<?php

use Illuminate\Database\Seeder;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\StockProductosModel;

class UpdateProductosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $filename = 'csv/inventario.csv'; //'csv/inventario_refacc.csv';
        $file_path = base_path('database/' . $filename);

        if (file_exists($file_path)) 
        {   
            $file = fopen($file_path, 'r');
            $cont = 0;

            while (($data = fgetcsv($file)) !== false) 
            {
                $cont++;

                $clave = trim($data[0]);
                $Gpo = trim($data[1]);
                $prefijo = trim($data[2]);
                $basico = trim($data[3]);
                $sufijo = trim($data[4]);
                $descripcion = trim($data[5]);
                $precio = trim($data[6]);
                $existencia = intval(trim($data[7]));
                $no_identificacion = "$Gpo$prefijo$basico$sufijo";

                $producto = ProductosModel::updateOrCreate([
                        ProductosModel::NO_IDENTIFICACION_FACTURA => $no_identificacion
                    ],
                    [
                        ProductosModel::GPO =>  $Gpo,
                        ProductosModel::BASICO =>  $basico,
                        ProductosModel::PREFIJO =>  $prefijo,
                        ProductosModel::SUFIJO =>  $sufijo,
                        ProductosModel::CLAVE_UNIDAD_FACTURA => "",
                        ProductosModel::CLAVE_PROD_SERV_FACTURA => "",
                        ProductosModel::DESCRIPCION => $descripcion,
                        ProductosModel::UNIDAD => "PIEZA",
                        ProductosModel::CANTIDAD => 0,
                        ProductosModel::VALOR_UNITARIO => $precio,
                        ProductosModel::PRECIO_FACTURA => $precio,
                        ProductosModel::TALLER_ID => 1,
                        ProductosModel::ALMACEN_ID => 1,
                        ProductosModel::UBICACION_PRODUCTO_ID => 1
                    ]);
                    
                StockProductosModel::updateOrCreate([
                    'producto_id' => $producto->id
                ], [
                    StockProductosModel::PRODUCTO_ID =>  $producto->id,
                    StockProductosModel::CANTIDAD_ACTUAL => $existencia < 0 ? 200 : $existencia, //$existencia,
                    StockProductosModel::CANTIDAD_ALMACEN_PRIMARIO => $existencia < 0 ? 200 : $existencia, // $existencia,
                    StockProductosModel::CANTIDAD_ALMACEN_SECUNDARIO => 0,
                    StockProductosModel::CANTIDAD_COMPRAS => 0,
                    StockProductosModel::CANTIDAD_VENTAS => 0,
                    StockProductosModel::TOTAL_PRECIO_COMPRAS => 0,
                    StockProductosModel::TOTAL_PRECIO_VENTAS => 0,                           
                ]);
                    
                echo "$cont,";

                // if($cont > 100)
                //     exit;
            }
        }
    }
}
