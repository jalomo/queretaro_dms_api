<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Refacciones\CatalogoClaveClienteModel;
use App\Models\Refacciones\ClaveClienteModel;

class CatalogoClaveClienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data =  [
            [CatalogoClaveClienteModel::NOMBRE => 'CLIENTE VENTA', CatalogoClaveClienteModel::CLAVE => 'CV'],
            [CatalogoClaveClienteModel::NOMBRE => 'CLIENTE SERVICIO', CatalogoClaveClienteModel::CLAVE => 'CS'],
            [CatalogoClaveClienteModel::NOMBRE => 'CLIENTE CRÉDITO', CatalogoClaveClienteModel::CLAVE => 'CC'],
            [CatalogoClaveClienteModel::NOMBRE => 'CLIENTE INTERNO', CatalogoClaveClienteModel::CLAVE => 'CI']
        ];

        foreach ($data as $key => $items) {
            DB::table(CatalogoClaveClienteModel::getTableName())->insert($items);
        }

        $claves_clientes =  [
            [ClaveClienteModel::CLIENTE_ID => 1, ClaveClienteModel::CLAVE_ID => 1],
            [ClaveClienteModel::CLIENTE_ID => 2, ClaveClienteModel::CLAVE_ID => 1],
            [ClaveClienteModel::CLIENTE_ID => 3, ClaveClienteModel::CLAVE_ID => 2],
            [ClaveClienteModel::CLIENTE_ID => 4, ClaveClienteModel::CLAVE_ID => 1]
        ];

        foreach ($claves_clientes as $key => $items) {
            DB::table(ClaveClienteModel::getTableName())
                ->insert($items);
        }
    }
}
