<?php

use App\Models\Usuarios\Menu\ModulosModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class ModulosMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $filename = 'csv/sistema/modulo.csv';
        $file_path = base_path('database/' . $filename);

        if (file_exists($file_path)) {
            $file = fopen($file_path, 'r');
            while (($data = fgetcsv($file)) !== false) {
                DB::table(ModulosModel::getTableName())->insert([
                    ModulosModel::NOMBRE => trim($data[0]) ? utf8_encode(trim($data[0])) : '',
                    ModulosModel::ICONO => trim($data[1]) ? trim($data[1]) : '',
                    ModulosModel::ORDEN => trim($data[2]) ? trim($data[2]) : '',
                    ModulosModel::DEFAULT => trim($data[3]) ? trim($data[3]) : 0
                ]);
            }
        }
    }
}
