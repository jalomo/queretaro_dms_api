<?php

use Illuminate\Database\Seeder;
use App\Models\Nomina\CaTipoSangreModel as Model;

class CaTipoSangreSeeder extends Seeder
{
    private function seedData(){
        return [
            [Model::ID => 1,Model::Descripcion => 'No Registrada',Model::Tipo=> ''],
            [Model::ID => 2,Model::Descripcion => 'Positiva',Model::Tipo=> 'A'],
            [Model::ID => 3,Model::Descripcion => 'Negativa',Model::Tipo=> 'A'],
            [Model::ID => 4,Model::Descripcion => 'Positiva',Model::Tipo=> 'B'],
            [Model::ID => 5,Model::Descripcion => 'Negativa',Model::Tipo=> 'B'],
            [Model::ID => 6,Model::Descripcion => 'Positiva',Model::Tipo=> 'O'],
            [Model::ID => 7,Model::Descripcion => 'Negativa',Model::Tipo=> 'O'],
            [Model::ID => 8,Model::Descripcion => 'Positivo',Model::Tipo=> 'AB'],
            [Model::ID => 9,Model::Descripcion => 'Negativo',Model::Tipo=> 'AB']
        ];
    }

    public function run()
    {
        foreach ($this->seedData() as $key => $items) {
            $exists = DB::connection(Model::connectionName())->table(Model::getTableName())->whereNotNull(MODEL::ID)->where(MODEL::ID, $items[MODEL::ID])->first();
            if($exists == false){
                DB::connection(Model::connectionName())->table(Model::getTableName())->insert($items);
            }
        }
    }
}
