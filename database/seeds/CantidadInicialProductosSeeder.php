<?php

use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\CantidadProductosInicialModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CantidadInicialProductosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */


    public function run()
    {
        $productos = ProductosModel::get();
        foreach ($productos as $val) {
            CantidadProductosInicialModel::create([
                CantidadProductosInicialModel::PRODUCTO_ID =>  $val->id,
                CantidadProductosInicialModel::CANTIDAD =>  5,
            ]);
        }
    }
}
