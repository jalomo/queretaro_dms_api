<?php
use App\Models\Refacciones\UbicacionLLavesModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UbicacionLlavesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                UbicacionLLavesModel::ID => 1,
                UbicacionLLavesModel::NOMBRE => 'Ubicación 1'
            ],
            [
                UbicacionLLavesModel::ID => 2,
                UbicacionLLavesModel::NOMBRE => 'Ubicación 2'
            ],
        );

        foreach ($data as $key => $items) {
            DB::table(UbicacionLLavesModel::getTableName())->insert($items);
        }
    }
}
