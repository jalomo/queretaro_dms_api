<?php

use App\Models\Refacciones\SucursalesModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SucursalesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                SucursalesModel::ID => 1,
                SucursalesModel::NOMBRE => 'QUERETARO'
            ],
            [
                SucursalesModel::ID => 2,
                SucursalesModel::NOMBRE => 'SAN JUAN'
            ]
        );

        foreach ($data as $key => $items) {
            DB::table(SucursalesModel::getTableName())->insert($items);
        }
    }
}
