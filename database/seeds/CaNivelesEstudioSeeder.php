<?php

use Illuminate\Database\Seeder;
use App\Models\Nomina\CaNivelesEstudioModel as Model;
use Illuminate\Support\Facades\DB;

class CaNivelesEstudioSeeder extends Seeder
{
    private function seedData(){
        return [
            [Model::ID => 1,Model::Descripcion => 'No Definido'],
            [Model::ID => 2,Model::Descripcion => 'Primaria'],
            [Model::ID => 3,Model::Descripcion => 'Secundaria'],
            [Model::ID => 4,Model::Descripcion => 'Media Superior'],
            [Model::ID => 5,Model::Descripcion => 'Técnico'],
            [Model::ID => 6,Model::Descripcion => 'Superior'],
            [Model::ID => 7,Model::Descripcion => 'Posgrado']
        ];
    }

    public function run()
    {
        foreach ($this->seedData() as $key => $items) {
            $exists = DB::connection(Model::connectionName())->table(Model::getTableName())->whereNotNull(MODEL::ID)->where(MODEL::ID, $items[MODEL::ID])->first();
            if($exists == false){
                DB::connection(Model::connectionName())->table(Model::getTableName())->insert($items);
            }
        }
    }
}
