<?php

use App\Models\Autos\UnidadesNuevas\CatCombustible;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class catCombustibleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                CatCombustible::DESCRIPCION => 'DIESEL'
            ],
            [
                CatCombustible::DESCRIPCION => 'GASOLINA'
            ],
            [
                CatCombustible::DESCRIPCION => 'ELÉCTRICO'
            ],
            [
                CatCombustible::DESCRIPCION => 'HÍBRIDO'
            ],
        );

        foreach ($data as $key => $items) {
            DB::table(CatCombustible::getTableName())->insert($items);
        }
    }
}
