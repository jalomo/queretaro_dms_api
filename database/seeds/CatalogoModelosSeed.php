<?php

use App\Models\Autos\CatModelosModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatalogoModelosSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $autos = [
            [CatModelosModel::CLAVE => '', CatModelosModel::NOMBRE => 'FIGO', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 1],
            [CatModelosModel::CLAVE => '', CatModelosModel::NOMBRE => 'FIESTA', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 1],
            [CatModelosModel::CLAVE => '', CatModelosModel::NOMBRE => 'FOCUS', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 1],
            [CatModelosModel::CLAVE => '', CatModelosModel::NOMBRE => 'FUSION', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 1],
            [CatModelosModel::CLAVE => '', CatModelosModel::NOMBRE => 'MUSTANG', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 1],
            [CatModelosModel::CLAVE => '', CatModelosModel::NOMBRE => 'RANGER', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 1],
            [CatModelosModel::CLAVE => '', CatModelosModel::NOMBRE => 'ECOSPORT', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 1],
            [CatModelosModel::CLAVE => '', CatModelosModel::NOMBRE => 'ESCAPE', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 1],
            [CatModelosModel::CLAVE => '', CatModelosModel::NOMBRE => 'EDGE', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 1],
            [CatModelosModel::CLAVE => '', CatModelosModel::NOMBRE => 'EXPLORER', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 1],
            [CatModelosModel::CLAVE => '', CatModelosModel::NOMBRE => 'EXPEDITION', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 1],
            [CatModelosModel::CLAVE => '', CatModelosModel::NOMBRE => 'LOBO', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 1],
            [CatModelosModel::CLAVE => '', CatModelosModel::NOMBRE => 'F150', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 1],
            [CatModelosModel::CLAVE => '', CatModelosModel::NOMBRE => 'F250', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 1],
            [CatModelosModel::CLAVE => '', CatModelosModel::NOMBRE => 'F350', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 1],
            [CatModelosModel::CLAVE => '', CatModelosModel::NOMBRE => 'F450', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 1],
            [CatModelosModel::CLAVE => '', CatModelosModel::NOMBRE => 'TRANSIT', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 1],
            [CatModelosModel::CLAVE => '', CatModelosModel::NOMBRE => 'RAPTOR', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 1],
            [CatModelosModel::CLAVE => '', CatModelosModel::NOMBRE => 'FREESTAR',CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 1],
            [CatModelosModel::CLAVE => '', CatModelosModel::NOMBRE => 'WINDSTAR',CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 1],
            [CatModelosModel::CLAVE => '', CatModelosModel::NOMBRE => 'EXCURSION',CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 1],
            [CatModelosModel::CLAVE => '', CatModelosModel::NOMBRE => 'MARINER',CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 1],
            [CatModelosModel::CLAVE => '', CatModelosModel::NOMBRE => 'CONTOUR',CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 1],
            [CatModelosModel::CLAVE => '', CatModelosModel::NOMBRE => 'SPORT TRACK',CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 1],
            [CatModelosModel::CLAVE => '', CatModelosModel::NOMBRE => 'COURIER',CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 1],
            [CatModelosModel::CLAVE => '', CatModelosModel::NOMBRE => 'F550',CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 1],
            [CatModelosModel::CLAVE => '', CatModelosModel::NOMBRE => 'BRONCO',CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 1],
            [CatModelosModel::CLAVE => '', CatModelosModel::NOMBRE => 'MAVERICK',CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 1],
            
            [CatModelosModel::CLAVE => 'F2A', CatModelosModel::NOMBRE => 'FIESTA 5-DR  SE A/T', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 2],
            [CatModelosModel::CLAVE => 'F2B', CatModelosModel::NOMBRE => 'FIESTA 5-DR SE M/T', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 2],
            [CatModelosModel::CLAVE => 'F2C', CatModelosModel::NOMBRE => 'FIESTA 4DR SE A/T', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 2],
            [CatModelosModel::CLAVE => 'F2D', CatModelosModel::NOMBRE => 'FIESTA SE M/T 4-DR', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 2],
            [CatModelosModel::CLAVE => 'F3B', CatModelosModel::NOMBRE => 'FIESTA 5 DR S M/T', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 2],
            [CatModelosModel::CLAVE => 'F3C', CatModelosModel::NOMBRE => 'FIESTA SEDAN S A/T', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 2],
            [CatModelosModel::CLAVE => 'F3D', CatModelosModel::NOMBRE => 'FIESTA S SEDAN M/T', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 2],
            [CatModelosModel::CLAVE => 'F4C', CatModelosModel::NOMBRE => 'FIESTA 4 DR TITANIUM', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 2],
            [CatModelosModel::CLAVE => 'F5B', CatModelosModel::NOMBRE => 'FIESTA 5 DR  ST', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 2],

            [CatModelosModel::CLAVE => 'Z0A', CatModelosModel::NOMBRE => 'FOCUS ST 5-DR HATCH M/T', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 3],
            [CatModelosModel::CLAVE => 'Z1B', CatModelosModel::NOMBRE => 'FOCUS DOOR SEDAN S M/T', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 3],
            [CatModelosModel::CLAVE => 'Z2A', CatModelosModel::NOMBRE => 'FOCUS 5-DOOR SPORT SE A/T', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 3],
            [CatModelosModel::CLAVE => 'Z2B', CatModelosModel::NOMBRE => 'FOCUS 5-DR HACTCHBACK SE A/T', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 3],
            [CatModelosModel::CLAVE => 'Z2D', CatModelosModel::NOMBRE => 'FOCUS 4-DOOR SEDAN SE A/T', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 3],
            [CatModelosModel::CLAVE => 'Z2E', CatModelosModel::NOMBRE => 'FOCUS 4-DOOR SEDAN SE', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 3],
            [CatModelosModel::CLAVE => 'Z2F', CatModelosModel::NOMBRE => 'FOCUS 5-DR HATCHBACK SE', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 3],
            [CatModelosModel::CLAVE => 'Z3A', CatModelosModel::NOMBRE => 'FOCUS 5-DR HATCH SE APPR A/T', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 3],
            [CatModelosModel::CLAVE => 'Z3B', CatModelosModel::NOMBRE => 'FOCUS 4-DR SEDAN SE APPR A/T', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 3],
            [CatModelosModel::CLAVE => 'Z3C', CatModelosModel::NOMBRE => 'FOCUS 4-DR SEDAN SE APPR', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 3],
            [CatModelosModel::CLAVE => 'Z3H', CatModelosModel::NOMBRE => 'FOCUS 5-DR HACTCH SE APPR', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 3],
            [CatModelosModel::CLAVE => 'Z3F', CatModelosModel::NOMBRE => 'FOCUS 4-DOOR SEDAN S M/T', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 3],
            [CatModelosModel::CLAVE => 'Z4C', CatModelosModel::NOMBRE => 'FOCUS 4-DOOR PLUS TITAN A/T', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 3],
            [CatModelosModel::CLAVE => 'Z5J', CatModelosModel::NOMBRE => 'FOCUS 5-DR HATCHBACK ST', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 3],
            [CatModelosModel::CLAVE => 'Z6D', CatModelosModel::NOMBRE => 'FOCUS AMBIENTE 4-DR SEDAN M/T', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 3],
            [CatModelosModel::CLAVE => 'Z7H', CatModelosModel::NOMBRE => 'FOCUS TREND 4-DR SEDAN A/T', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 3],
            [CatModelosModel::CLAVE => 'Z8E', CatModelosModel::NOMBRE => 'FOCUS TREND SPORT 4-DR SEDAN A/T', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 3],
            [CatModelosModel::CLAVE => 'ZRS', CatModelosModel::NOMBRE => 'FOCUS 5-DR HATCHBACK RS', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 3],
            [CatModelosModel::CLAVE => 'Z9B', CatModelosModel::NOMBRE => 'FOCUS TITANIUM 4-DR SEDAN A/TS', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 3],

            [CatModelosModel::CLAVE => 'C2C',  CatModelosModel::NOMBRE => 'FUSION SE', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 4],
            [CatModelosModel::CLAVE => 'C2E',  CatModelosModel::NOMBRE => 'FUSION SE LUX PLUS', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 4],
            [CatModelosModel::CLAVE => 'C2F',  CatModelosModel::NOMBRE => 'FUSION SE LUXURY', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 4],
            [CatModelosModel::CLAVE => 'C2G',  CatModelosModel::NOMBRE => 'FUSION SE NAVI', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 4],
            [CatModelosModel::CLAVE => 'C2J',  CatModelosModel::NOMBRE => 'FUSION SE HYBRID LUX', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 4],
            [CatModelosModel::CLAVE => 'C2K',  CatModelosModel::NOMBRE => 'FUSION SE HYBRID', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 4],
            [CatModelosModel::CLAVE => 'C2P',  CatModelosModel::NOMBRE => 'FUSION SEL HYBRID', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 4],
            [CatModelosModel::CLAVE => 'C1A',  CatModelosModel::NOMBRE => 'FUSION SEL', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 4],
            [CatModelosModel::CLAVE => 'C3B',  CatModelosModel::NOMBRE => 'FUSION S', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 4],
            [CatModelosModel::CLAVE => 'C4A',  CatModelosModel::NOMBRE => 'FUSION TITANIUM PLUS', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 4],

            [CatModelosModel::CLAVE => 'MAC', CatModelosModel::NOMBRE => 'MUSTANG GT COUPE PREM M/T', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 5],
            [CatModelosModel::CLAVE => 'MBD', CatModelosModel::NOMBRE => 'MUSTANG V6 COUPE TM', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 5],
            [CatModelosModel::CLAVE => 'MGE', CatModelosModel::NOMBRE => 'MUSTANG GT CONVERTIBLE TA', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 5],
            [CatModelosModel::CLAVE => 'MGF', CatModelosModel::NOMBRE => 'MUSTANG GT CONVERTIBLE', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 5],
            [CatModelosModel::CLAVE => 'MGL', CatModelosModel::NOMBRE => 'MUSTANG BULLITT ', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 5],
            [CatModelosModel::CLAVE => 'MPB', CatModelosModel::NOMBRE => 'MUSTANG GT PREMIUM COUPE', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 5],
            [CatModelosModel::CLAVE => 'MPD', CatModelosModel::NOMBRE => 'MUSTANG', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 5],
            [CatModelosModel::CLAVE => 'MPF', CatModelosModel::NOMBRE => 'MUSTANG GT PREMIUM COUPE', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 5],
            [CatModelosModel::CLAVE => 'MPG', CatModelosModel::NOMBRE => 'MUSTANG GT PREMIUM COUPE', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 5],
            [CatModelosModel::CLAVE => 'MPJ', CatModelosModel::NOMBRE => 'MUSTANG I4 ECOBOOST PREMIUM', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 5],
            [CatModelosModel::CLAVE => 'MT7', CatModelosModel::NOMBRE => 'SHELBY GT350 COUPE', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 5],

            [CatModelosModel::CLAVE => 'W7B', CatModelosModel::NOMBRE => 'ECOSPORT TITANIUM AT', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 6],
            [CatModelosModel::CLAVE => 'W8B', CatModelosModel::NOMBRE => 'ECOSPORT TREND AT', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 6],
            [CatModelosModel::CLAVE => 'W8D', CatModelosModel::NOMBRE => 'ECOSPORT TREND MANUAL', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 6],
            [CatModelosModel::CLAVE => 'W9D', CatModelosModel::NOMBRE => 'ECOSPORT ', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 6],

            [CatModelosModel::CLAVE => 'H1J', CatModelosModel::NOMBRE => 'ESCAPE SEL PQ. TITANIUM', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 7],
            [CatModelosModel::CLAVE => 'H2A', CatModelosModel::NOMBRE => 'ESCAPE SE PLUS', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 7],
            [CatModelosModel::CLAVE => 'H1H', CatModelosModel::NOMBRE => 'ESCAPE TITANIUM 2.5L', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 7],
            [CatModelosModel::CLAVE => 'H2H', CatModelosModel::NOMBRE => 'ESCAPE ADVANCE 2.5', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 7],
            [CatModelosModel::CLAVE => 'H2Y', CatModelosModel::NOMBRE => 'ESCAPE ADVANCE 2.0 ECOBOOST', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 7],
            [CatModelosModel::CLAVE => 'H3E', CatModelosModel::NOMBRE => 'ESCAPE S PLUS', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 7],
            [CatModelosModel::CLAVE => 'H3F', CatModelosModel::NOMBRE => 'ESCAPE S', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 7],
            [CatModelosModel::CLAVE => 'H5A', CatModelosModel::NOMBRE => 'ESCAPE FWD SE HYBRIDA ', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 7],
            [CatModelosModel::CLAVE => 'H5T', CatModelosModel::NOMBRE => 'ESCAPE TITANIUM FWD', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 7],
            [CatModelosModel::CLAVE => 'H3D', CatModelosModel::NOMBRE => 'ESCAPE S', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 7],

            [CatModelosModel::CLAVE => 'D1B', CatModelosModel::NOMBRE => 'EDGE SEL FWD', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 8],
            [CatModelosModel::CLAVE => 'D2B', CatModelosModel::NOMBRE => 'EDGE SE FWD', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 8],
            [CatModelosModel::CLAVE => 'D3B', CatModelosModel::NOMBRE => 'EDGE SEL FWD', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 8],
            [CatModelosModel::CLAVE => 'D3C', CatModelosModel::NOMBRE => 'EDGE SEL FWD PLUS', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 8],
            [CatModelosModel::CLAVE => 'D5A', CatModelosModel::NOMBRE => 'EDGE LIMITED FWD', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 8],
            [CatModelosModel::CLAVE => 'D6S', CatModelosModel::NOMBRE => 'EDGE SPORT FWD', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 8],

            [CatModelosModel::CLAVE => 'E1A', CatModelosModel::NOMBRE => 'EXPLORER LIMITED 4WD', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 9],
            [CatModelosModel::CLAVE => 'E1B', CatModelosModel::NOMBRE => 'EXPLORER FWD 4DR LTD', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 9],
            [CatModelosModel::CLAVE => 'E2C', CatModelosModel::NOMBRE => 'EXPLORER FWD 4DR XLT', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 9],
            [CatModelosModel::CLAVE => 'E2D', CatModelosModel::NOMBRE => 'EXPLORER FWD 4DR XLT', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 9],
            [CatModelosModel::CLAVE => 'E6S', CatModelosModel::NOMBRE => 'EXPLORER', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 9],
            [CatModelosModel::CLAVE => 'E5S', CatModelosModel::NOMBRE => 'EXPLORER 4DR4WD SPORT', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 9],

            [CatModelosModel::CLAVE => 'V2Z', CatModelosModel::NOMBRE => 'EXPEDITION EXP LIMITED MAX 4X2', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 10],
            [CatModelosModel::CLAVE => 'V6A', CatModelosModel::NOMBRE => 'EXPEDITION PLATINUM MAX 4X4', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 10],
            [CatModelosModel::CLAVE => 'V6B', CatModelosModel::NOMBRE => 'EXPEDITION PLATINUM 4X4', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 10],
            [CatModelosModel::CLAVE => 'V1Z', CatModelosModel::NOMBRE => 'EXPEDITION LIMITED 4X2', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 10],

            [CatModelosModel::CLAVE => 'J1D', CatModelosModel::NOMBRE => 'RANGER SA CREW CAB XLT', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 11],
            [CatModelosModel::CLAVE => 'J1E', CatModelosModel::NOMBRE => 'RANGER SA CREW CAB XLT', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 11],
            [CatModelosModel::CLAVE => 'J1F', CatModelosModel::NOMBRE => 'RANGER SA CREW CAB XL', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 11],
            [CatModelosModel::CLAVE => 'J1L', CatModelosModel::NOMBRE => 'RANGER SA CREW CAB XLT', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 11],
            [CatModelosModel::CLAVE => 'J1H', CatModelosModel::NOMBRE => 'RANGER', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 11],
            [CatModelosModel::CLAVE => 'J1R', CatModelosModel::NOMBRE => 'RANGER SA CREW CAB XL', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 11],
            [CatModelosModel::CLAVE => 'J1Q', CatModelosModel::NOMBRE => 'RANGER SA REG CAB', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 11],
            [CatModelosModel::CLAVE => 'J1W', CatModelosModel::NOMBRE => 'RANGER SA CREW CAB ', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 11],
            [CatModelosModel::CLAVE => 'J2E', CatModelosModel::NOMBRE => 'RANGER SA CREW XLT 4X2', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 11],
            [CatModelosModel::CLAVE => 'J3F', CatModelosModel::NOMBRE => 'RANGER SA REG CAB', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 11],
            [CatModelosModel::CLAVE => 'J5E', CatModelosModel::NOMBRE => 'RANGER SA CREW CAB LTD', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 11],
            [CatModelosModel::CLAVE => 'J5D', CatModelosModel::NOMBRE => 'RANGER SA CREW CAB LTD', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 11],
            [CatModelosModel::CLAVE => 'J9A', CatModelosModel::NOMBRE => 'RANGER SAFR CREW CB 4X2 XLT', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 11],
            [CatModelosModel::CLAVE => 'J9B', CatModelosModel::NOMBRE => 'RANGER SAFR CRW CAB 4X4 XLT', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 11],
            [CatModelosModel::CLAVE => 'J9D', CatModelosModel::NOMBRE => 'RANGER SAFR CREW CAB 4X4 XL', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 11],
            [CatModelosModel::CLAVE => 'J9F', CatModelosModel::NOMBRE => 'RANGER SAFR CREW CAB 4X2 XL', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 11],
            [CatModelosModel::CLAVE => 'J9G', CatModelosModel::NOMBRE => 'RANGER SAFR CREW CAB 4X2 XL', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 11],
            [CatModelosModel::CLAVE => 'JV5', CatModelosModel::NOMBRE => 'RANGER SA CREW CAB XL', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 11],

            [CatModelosModel::CLAVE => 'J7F', CatModelosModel::NOMBRE => 'F150 XL 4X2 REG CAB', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 12],
            [CatModelosModel::CLAVE => 'J6B', CatModelosModel::NOMBRE => 'F150 XL 4X2 SUPERCREW', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 12],
            [CatModelosModel::CLAVE => 'J6E', CatModelosModel::NOMBRE => 'F150 XL 4X4 REG CAB', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 12],
            [CatModelosModel::CLAVE => 'J6F', CatModelosModel::NOMBRE => 'F-150 XL 4X2 REG CAB', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 12],
            [CatModelosModel::CLAVE => 'J8A', CatModelosModel::NOMBRE => 'F-150 XL 4X4 SUPERCREW', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 12],
            [CatModelosModel::CLAVE => 'J8B', CatModelosModel::NOMBRE => 'F-150 XL 4X2 SUPERCREW', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 12],
            [CatModelosModel::CLAVE => 'J8F', CatModelosModel::NOMBRE => 'F150 XL 4X2 REG CAB', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 12],

            [CatModelosModel::CLAVE => 'K2A', CatModelosModel::NOMBRE => 'F250 SUPERDUTY SD PICK UP XLT', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 13],

            [CatModelosModel::CLAVE => 'K6D', CatModelosModel::NOMBRE => 'F350 XL PLUS CHAS CAB', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 14],
            [CatModelosModel::CLAVE => 'K6C', CatModelosModel::NOMBRE => 'F-350 SUPER DUTY XL PLUS CHAS CAB', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 14],
            [CatModelosModel::CLAVE => 'K6E', CatModelosModel::NOMBRE => 'F-350 XL PLUS CHAS CAB', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 14],
            [CatModelosModel::CLAVE => 'K6F', CatModelosModel::NOMBRE => 'F350 XL CHAS CAB', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 14],

            [CatModelosModel::CLAVE => 'K5E', CatModelosModel::NOMBRE => 'F450 XL REG CHAS CAB', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 15],
            [CatModelosModel::CLAVE => 'K5T', CatModelosModel::NOMBRE => 'F-450 XL REG CHAS CAB', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 15],

            [CatModelosModel::CLAVE => 'K8E', CatModelosModel::NOMBRE => 'F-550 XL REG CHAS CAB', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 16],
            [CatModelosModel::CLAVE => 'K8T', CatModelosModel::NOMBRE => 'F-550 XL REG CHAS CAB', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 16],

            [CatModelosModel::CLAVE => 'M1F', CatModelosModel::NOMBRE => 'TRANSIT VAN SWB AC', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 17],
            [CatModelosModel::CLAVE => 'M2A', CatModelosModel::NOMBRE => 'TRANSIT VAN MWB', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 17],
            [CatModelosModel::CLAVE => 'M2E', CatModelosModel::NOMBRE => 'TRANSIT', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 17],
            [CatModelosModel::CLAVE => 'M2G', CatModelosModel::NOMBRE => 'TRANSIT PASS MWB AC', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 17],
            [CatModelosModel::CLAVE => 'M3J', CatModelosModel::NOMBRE => 'TRANSIT CCMWBAC RWD', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 17],
            [CatModelosModel::CLAVE => 'M3K', CatModelosModel::NOMBRE => 'TRANSIT CC LWB AC RWD', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 17],
            [CatModelosModel::CLAVE => 'M3S', CatModelosModel::NOMBRE => 'TRANSIT CC LWB AC RWD', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 17],
            [CatModelosModel::CLAVE => 'M4A', CatModelosModel::NOMBRE => 'TRANSIT BUS ELWB AC', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 17],
            [CatModelosModel::CLAVE => 'M4M', CatModelosModel::NOMBRE => 'TRANSIT BUS ELWB AC', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 17],
            [CatModelosModel::CLAVE => 'M4B', CatModelosModel::NOMBRE => 'TRANSIT BUS ELWB AC', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 17],
            [CatModelosModel::CLAVE => 'M4H', CatModelosModel::NOMBRE => 'TRANSIT', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 17],
            [CatModelosModel::CLAVE => 'M6B', CatModelosModel::NOMBRE => 'TRANSIT 350 MR WAGON', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 17],
            [CatModelosModel::CLAVE => 'M7C', CatModelosModel::NOMBRE => 'TRANSIT 250 MR VAN', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 17],
            [CatModelosModel::CLAVE => 'N1A', CatModelosModel::NOMBRE => 'TRANSIT CUSTOM VAN SWB', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 17],
            [CatModelosModel::CLAVE => 'N2A', CatModelosModel::NOMBRE => 'TRANSIT CUST PASS LWB', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 17],
            [CatModelosModel::CLAVE => 'N2B', CatModelosModel::NOMBRE => 'TRANSIT', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 17],
            [CatModelosModel::CLAVE => 'N3A', CatModelosModel::NOMBRE => 'TRANSIT CUSTOM VAN WB', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 17],
            [CatModelosModel::CLAVE => 'N3B', CatModelosModel::NOMBRE => 'TRANSIT CUSTOM VAN WB', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 17],

            [CatModelosModel::CLAVE => 'G2B', CatModelosModel::NOMBRE => 'LOBO XLT 4X2 SUPERCREW', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 18],
            [CatModelosModel::CLAVE => 'G2A', CatModelosModel::NOMBRE => 'LOBO', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 18],
            [CatModelosModel::CLAVE => 'G2E', CatModelosModel::NOMBRE => 'LOBO XLT 4X4 REG CAB', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 18],
            [CatModelosModel::CLAVE => 'G2F', CatModelosModel::NOMBRE => 'LOBO XLT 4X4 REG CAB', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 18],
            [CatModelosModel::CLAVE => 'G3J', CatModelosModel::NOMBRE => 'LOBO TREMOR 4X4 C REG', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 18],
            [CatModelosModel::CLAVE => 'G4B', CatModelosModel::NOMBRE => 'LOBO LRT 4X2 SUPERCREW', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 18],
            [CatModelosModel::CLAVE => 'G4J', CatModelosModel::NOMBRE => 'LOBO LARIAT 4X4 CREW', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 18],
            [CatModelosModel::CLAVE => 'G4K', CatModelosModel::NOMBRE => 'LOBO 4X4 PLATINUM', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 18],
            [CatModelosModel::CLAVE => 'G4S', CatModelosModel::NOMBRE => 'LOBO', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 18],
            [CatModelosModel::CLAVE => 'G5J', CatModelosModel::NOMBRE => 'LOBO 4X4 KING RANCH', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 18],
            [CatModelosModel::CLAVE => 'G5L', CatModelosModel::NOMBRE => 'LOBO 4X4 LIMITED', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 18],
            [CatModelosModel::CLAVE => 'G5N', CatModelosModel::NOMBRE => 'LOBO', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 18],
            [CatModelosModel::CLAVE => 'G6J', CatModelosModel::NOMBRE => 'LOBO 4X4 PLATINUM', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 18],
            [CatModelosModel::CLAVE => 'G5P', CatModelosModel::NOMBRE => 'LOBO 4X4 RAPTOR', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 18],
            [CatModelosModel::CLAVE => 'G5R', CatModelosModel::NOMBRE => 'LOBO 4X4 RAPTOR', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 18],
            [CatModelosModel::CLAVE => 'G5T', CatModelosModel::NOMBRE => 'LOBO 4X4 RAPTOR', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 18],
           
            [CatModelosModel::CLAVE => 'LT', CatModelosModel::NOMBRE => 'Mark LT', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 33],
            [CatModelosModel::CLAVE => 'MKC', CatModelosModel::NOMBRE => 'MKC', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 33],
            [CatModelosModel::CLAVE => 'MKS', CatModelosModel::NOMBRE => 'MKS', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 33],
            [CatModelosModel::CLAVE => 'MKX', CatModelosModel::NOMBRE => 'MKX', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 33],
            [CatModelosModel::CLAVE => 'MKZ', CatModelosModel::NOMBRE => 'MKZ', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 33],
            [CatModelosModel::CLAVE => 'Navigator', CatModelosModel::NOMBRE => 'Navigator', CatModelosModel::TIEMPO_LAVADO => 0, CatModelosModel::ID_MARCA => 33],
     
        ];

        foreach ($autos as $key => $items) {
            DB::table(CatModelosModel::getTableName())->insert($items);
        }
    }
}


/* */
