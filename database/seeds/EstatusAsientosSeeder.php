<?php

use App\Models\CuentasPorCobrar\EstatusAsientosModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstatusAsientosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $estatus_abonos = [
            [
                EstatusAsientosModel::NOMBRE => 'PENDIENTE'
            ],
            [
                EstatusAsientosModel::NOMBRE => 'ENVIADO'
            ],
            [
                EstatusAsientosModel::NOMBRE => 'CANCELADO'
            ]
           
        ];

        foreach ($estatus_abonos as $value) {
            DB::table(EstatusAsientosModel::getTableName())->insert([
                EstatusAsientosModel::NOMBRE => $value[EstatusAsientosModel::NOMBRE]
            ]);
        }
    }
}
