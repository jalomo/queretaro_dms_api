<?php

use App\Models\CuentasPorPagar\CatEstatusAbonoModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstatusAbonosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $estatus_abonos = [
            [
                CatEstatusAbonoModel::ID => 1,
                CatEstatusAbonoModel::NOMBRE => 'PENDIENTE'
            ],
            [
                CatEstatusAbonoModel::ID => 2,
                CatEstatusAbonoModel::NOMBRE => 'MORATORIO'
            ],
            [
                CatEstatusAbonoModel::ID => 3,
                CatEstatusAbonoModel::NOMBRE => 'PAGADO'
            ],
            [
                CatEstatusAbonoModel::ID => 4,
                CatEstatusAbonoModel::NOMBRE => 'FACTURADO'
            ]
           
        ];

        foreach ($estatus_abonos as $value) {
            DB::table(CatEstatusAbonoModel::getTableName())->insert([
                CatEstatusAbonoModel::ID => $value[CatEstatusAbonoModel::ID],
                CatEstatusAbonoModel::NOMBRE => $value[CatEstatusAbonoModel::NOMBRE]
            ]);
        }
    }
}
