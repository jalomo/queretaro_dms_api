<?php

use App\Models\Usuarios\MenuSeccionesModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class MenuSeccionesSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $filename = 'csv/sistema/menu_secciones.csv';
        $file_path = base_path('database/' . $filename);
        if (file_exists($file_path)) {
            $file = fopen($file_path, 'r');

            while (($data = fgetcsv($file)) !== false) {
                DB::table(MenuSeccionesModel::getTableName())->insert([
                    MenuSeccionesModel::ID => trim($data[0]) ? utf8_encode(trim($data[0])) : '',
                    MenuSeccionesModel::NOMBRE => trim($data[1]) ? utf8_encode(trim($data[1])) : '',
                    MenuSeccionesModel::MODULO_ID => trim($data[2]) ? trim($data[2]) :  ''
                ]);
            }
        }
    }
}
