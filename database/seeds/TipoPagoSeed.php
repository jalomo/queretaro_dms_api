<?php

use App\Models\Refacciones\CatTipoPagoModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoPagoSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $almacenes = [
            [
                CatTipoPagoModel::NOMBRE => 'Efectivo',
                CatTipoPagoModel::CLAVE => '01',

            ],
            [
                CatTipoPagoModel::NOMBRE => 'Cheque nominativo',
                CatTipoPagoModel::CLAVE => '02',
            ],
            [
                CatTipoPagoModel::ID => 3,
                CatTipoPagoModel::NOMBRE => 'Transferencia Electrónica de fondos',
                CatTipoPagoModel::CLAVE => '03',
            ],
            [
                CatTipoPagoModel::NOMBRE => 'Tarjeta de crédito',
                CatTipoPagoModel::CLAVE => '04',
            ],
            [
                CatTipoPagoModel::NOMBRE => 'Monedero electrónico',
                CatTipoPagoModel::CLAVE => '05',
            ],
            [
                CatTipoPagoModel::NOMBRE => 'Dinero electrónico',
                CatTipoPagoModel::CLAVE => '06',
            ],
            [
                CatTipoPagoModel::NOMBRE => 'Vales de despensa',
                CatTipoPagoModel::CLAVE => '08',
            ],
            [
                CatTipoPagoModel::NOMBRE => 'Dación en pago',
                CatTipoPagoModel::CLAVE => '12',
            ],
            [
                CatTipoPagoModel::NOMBRE => 'Pago por subrogación',
                CatTipoPagoModel::CLAVE => '13',
            ],
            [
                CatTipoPagoModel::NOMBRE => 'Pago por consignación',
                CatTipoPagoModel::CLAVE => '14',
            ],
            [
                CatTipoPagoModel::NOMBRE => 'Condonación',
                CatTipoPagoModel::CLAVE => '15',
            ],
            [
                CatTipoPagoModel::NOMBRE => 'Compensación',
                CatTipoPagoModel::CLAVE => '17',
            ],
            [
                CatTipoPagoModel::NOMBRE => 'Novación',
                CatTipoPagoModel::CLAVE => '23',
            ],
            [
                CatTipoPagoModel::NOMBRE => 'Confusión',
                CatTipoPagoModel::CLAVE => '24',
            ],
            [
                CatTipoPagoModel::NOMBRE => 'Remisión de deuda',
                CatTipoPagoModel::CLAVE => '25',
            ],
            [
                CatTipoPagoModel::NOMBRE => 'Prescripción o caducidad',
                CatTipoPagoModel::CLAVE => '26',
            ],
            [
                CatTipoPagoModel::NOMBRE => 'A satisfacción del acreedor',
                CatTipoPagoModel::CLAVE => '27',
            ],
            [
                CatTipoPagoModel::NOMBRE => 'Tarjeta de débito',
                CatTipoPagoModel::CLAVE => '28',
            ],
            [
                CatTipoPagoModel::NOMBRE => 'Tarjeta de servicios',
                CatTipoPagoModel::CLAVE => '29',
            ],
            [
                CatTipoPagoModel::NOMBRE => 'Aplicación de anticipos',
                CatTipoPagoModel::CLAVE => '30',
            ],
            [
                CatTipoPagoModel::NOMBRE => 'Intermediario pagos',
                CatTipoPagoModel::CLAVE => '31',
            ],
            [
                CatTipoPagoModel::NOMBRE => 'Por definir',
                CatTipoPagoModel::CLAVE => '99',
            ],
        ];

        foreach ($almacenes as $key => $value) {
            DB::table(CatTipoPagoModel::getTableName())->insert([
                CatTipoPagoModel::NOMBRE => $value[CatTipoPagoModel::NOMBRE],
                CatTipoPagoModel::CLAVE => $value[CatTipoPagoModel::CLAVE],
            ]);
        }
    }
}
