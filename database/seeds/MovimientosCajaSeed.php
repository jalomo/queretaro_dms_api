<?php

use App\Models\Caja\MovimientoCajaModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MovimientosCajaSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                MovimientoCajaModel::ID => 1,
                MovimientoCajaModel::NOMBRE => 'ENTRADA'
            ],
            [
                MovimientoCajaModel::ID => 2,
                MovimientoCajaModel::NOMBRE => 'SALIDA'
            ]
        );

        foreach ($data as $key => $items) {
            DB::table(MovimientoCajaModel::getTableName())->insert($items);
        }
    }
}
