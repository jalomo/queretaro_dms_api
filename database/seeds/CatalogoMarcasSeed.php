<?php

use App\Models\Refacciones\CatalogoMarcasModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatalogoMarcasSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data =  [
            [CatalogoMarcasModel::NOMBRE => 'FORD'],
            [CatalogoMarcasModel::NOMBRE => 'MERCEDES BENZ'],
            [CatalogoMarcasModel::NOMBRE => 'HYUNDAI'],
            [CatalogoMarcasModel::NOMBRE => 'VOLVO'],
            [CatalogoMarcasModel::NOMBRE => 'TOYOTA'],
            [CatalogoMarcasModel::NOMBRE => 'VOLKSWAGEN'],
            [CatalogoMarcasModel::NOMBRE => 'NISSAN'],
            [CatalogoMarcasModel::NOMBRE => 'INTERNACIONAL'],
            [CatalogoMarcasModel::NOMBRE => 'AUDI'],
            [CatalogoMarcasModel::NOMBRE => 'CHEVROLET'],
            [CatalogoMarcasModel::NOMBRE => 'JEEP'],
            [CatalogoMarcasModel::NOMBRE => 'BMW'],
            [CatalogoMarcasModel::NOMBRE => 'MASA'],
            [CatalogoMarcasModel::NOMBRE => 'HIUNDAY'],
            [CatalogoMarcasModel::NOMBRE => 'FIGO'],
            [CatalogoMarcasModel::NOMBRE => 'FIESTA'],
            [CatalogoMarcasModel::NOMBRE => 'FOCUS'],
            [CatalogoMarcasModel::NOMBRE => 'FUSION'],
            [CatalogoMarcasModel::NOMBRE => 'MUSTANG'],
            [CatalogoMarcasModel::NOMBRE => 'ECOSPORT'],
            [CatalogoMarcasModel::NOMBRE => 'ESCAPE'],
            [CatalogoMarcasModel::NOMBRE => 'EDGE'],
            [CatalogoMarcasModel::NOMBRE => 'EXPLORE'],
            [CatalogoMarcasModel::NOMBRE => 'EXPEDITION'],
            [CatalogoMarcasModel::NOMBRE => 'RANGER'],
            [CatalogoMarcasModel::NOMBRE => 'F150'],
            [CatalogoMarcasModel::NOMBRE => 'F250'],
            [CatalogoMarcasModel::NOMBRE => 'F350'],
            [CatalogoMarcasModel::NOMBRE => 'F450'],
            [CatalogoMarcasModel::NOMBRE => 'F-550'],
            [CatalogoMarcasModel::NOMBRE => 'TRANSIT'],
            [CatalogoMarcasModel::NOMBRE => 'LOBO'],
            [CatalogoMarcasModel::NOMBRE => 'LINCON']
        ];

        foreach ($data as $key => $items) {
            DB::table(CatalogoMarcasModel::getTableName())
                ->insert($items);
        }
    }
}
