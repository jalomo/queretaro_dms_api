<?php

use App\Models\Refacciones\EstatusTraspaso;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstatusTraspasoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data[] = [ 'id' => 1, 'nombre' => 'GENERADO'];
        $data[] = [ 'id' => 2, 'nombre' => 'ACTUALIZADO'];
        $data[] = [ 'id' => 3, 'nombre' => 'PEDIDO'];
        $data[] = [ 'id' => 4, 'nombre' => 'PROCESANDO'];
        $data[] = [ 'id' => 5, 'nombre' => 'ENVIADO'];
        $data[] = [ 'id' => 6, 'nombre' => 'FINALIZADO'];
        $data[] = [ 'id' => 7, 'nombre' => 'CANCELADO'];
        $data[] = [ 'id' => 8, 'nombre' => 'PROCESANDO DEVOLUCION'];
        $data[] = [ 'id' => 9, 'nombre' => 'ENVIO EN DEVOLUCION'];
        $data[] = [ 'id' => 10, 'nombre' => 'DEVUELTO'];
        $data[] = [ 'id' => 11, 'nombre' => 'SOLICITUD TRASPASO FIRMADA'];
        $data[] = [ 'id' => 12, 'nombre' => 'TRASPASO AUTORIZADO'];
        $data[] = [ 'id' => 13, 'nombre' => 'SUBIR EVIDENCIA'];

        foreach ($data as $item) 
        {
            EstatusTraspaso::create($item);
        }
    }
}
