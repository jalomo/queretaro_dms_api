<?php

use App\Models\Refacciones\Cardex\MovimientosModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MovimientosProductosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                MovimientosModel::ID => 1,
                MovimientosModel::NOMBRE => 'TRASPASO',
                MovimientosModel::CLAVE => '00'
            ],
            [
                MovimientosModel::ID => 2,
                MovimientosModel::NOMBRE => 'VENTA MOSTRADOR',
                MovimientosModel::CLAVE => '00'
            ],
            [
                MovimientosModel::ID => 3,
                MovimientosModel::NOMBRE => 'VENTA TALLER',
                MovimientosModel::CLAVE => '00'
            ],
            [
                MovimientosModel::ID => 4,
                MovimientosModel::NOMBRE => 'DEVOLUCION PROVEEDOR',
                MovimientosModel::CLAVE => '00'
            ],
            [
                MovimientosModel::ID => 5,
                MovimientosModel::NOMBRE => 'COMPRAS',
                MovimientosModel::CLAVE => '00'
            ],
            [
                MovimientosModel::ID => 6,
                MovimientosModel::NOMBRE => 'DEV. VENTAS',
                MovimientosModel::CLAVE => '00'
            ]
        );

        foreach ($data as $key => $items) {
            DB::table(MovimientosModel::getTableName())->insert($items);
        }
    }
}
