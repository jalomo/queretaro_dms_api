<?php

use App\Models\Usuarios\MenuSubmenuModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenuSubmenusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
     {
         $filename = 'csv/sistema/menu_submenu.csv';
         $file_path = base_path('database/' . $filename);
         if (file_exists($file_path)) {
             $file = fopen($file_path, 'r');
 
             while (($data = fgetcsv($file)) !== false) {
                 DB::table(MenuSubmenuModel::getTableName())->insert([
                     MenuSubmenuModel::ID => trim($data[0]) ? utf8_encode(trim($data[0])) : '',
                     MenuSubmenuModel::NOMBRE => trim($data[1]) ? utf8_encode(trim($data[1])) : '',
                     MenuSubmenuModel::SECCION_ID => trim($data[2]) ? trim($data[2]) :  '',
                     MenuSubmenuModel::VISIBLE => trim($data[3]) ? trim($data[3]) :  ''
                 ]);
             }
         }
     }
}
