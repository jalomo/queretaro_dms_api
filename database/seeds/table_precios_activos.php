<?php

use App\Models\Autos\UnidadesNuevas\PreciosUnidadesNuevasModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class table_precios_activos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $precios = [
            [
                PreciosUnidadesNuevasModel::ID => 1,
                PreciosUnidadesNuevasModel::MODELO => '2021',
                PreciosUnidadesNuevasModel::DESCRIPCION => 'AUTOS 2021',
                PreciosUnidadesNuevasModel::FECHA_INICIO => '2021-09-01',
                PreciosUnidadesNuevasModel::FECHA_FIN => '2021-09-30',
                PreciosUnidadesNuevasModel::ACTIVO => 1
            ]
        ];

        foreach ($precios as $value) {
            DB::table(PreciosUnidadesNuevasModel::getTableName())->insert([
                PreciosUnidadesNuevasModel::ID => $value[PreciosUnidadesNuevasModel::ID],
                PreciosUnidadesNuevasModel::MODELO => $value[PreciosUnidadesNuevasModel::MODELO],
                PreciosUnidadesNuevasModel::DESCRIPCION => $value[PreciosUnidadesNuevasModel::DESCRIPCION],
                PreciosUnidadesNuevasModel::FECHA_INICIO => $value[PreciosUnidadesNuevasModel::FECHA_INICIO],
                PreciosUnidadesNuevasModel::FECHA_FIN => $value[PreciosUnidadesNuevasModel::FECHA_FIN],
                PreciosUnidadesNuevasModel::ACTIVO => $value[PreciosUnidadesNuevasModel::ACTIVO]
            ]);
        }
    }
}
