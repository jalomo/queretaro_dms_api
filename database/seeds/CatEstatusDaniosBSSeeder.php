<?php

use App\Models\Logistica\CatEstatusDaniosBodyshopModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatEstatusDaniosBSSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                CatEstatusDaniosBodyshopModel::ID => 1,
                CatEstatusDaniosBodyshopModel::ESTATUS => 'Reparado'
            ],
            [
                CatEstatusDaniosBodyshopModel::ID => 2,
                CatEstatusDaniosBodyshopModel::ESTATUS => 'En reparación'
            ],
        );

        foreach ($data as $key => $items) {
            DB::table(CatEstatusDaniosBodyshopModel::getTableName())->insert($items);
        }
    }
}
