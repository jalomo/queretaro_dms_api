<?php

use App\Models\CPVentas\CPVentasModel;
use Illuminate\Database\Seeder;

class ProactivoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $filename = 'csv/cp_ventas.csv';  
        $file_path = base_path('database/' . $filename);

        if (file_exists($file_path)) {
            $file = fopen($file_path, 'r');
            while (($data = fgetcsv($file)) !== false) {
                $NO_CUENTA = trim($data[1]);
                $VIN = trim($data[2]);
                $APELLIDO = trim($data[3]);
                $NOMBRE = trim($data[4]);
                $TIPO_CLIENTE = trim($data[5]);
                $TELEFONO_OFICINA = trim($data[6]);
                $TELEFONO_CASA = trim($data[7]);
                $PLAN_FINANCIAMIENTO = trim($data[8]);
                $ENGANCHE = trim($data[9]);
                $MESES_CONTRATO = trim($data[10]);
                $FECHA_INICIO_CONTRATO = trim($data[11]);
                $FECHA_TERMINO = trim($data[12]);
                $TASA_INTERES = trim($data[13]);
                $MENSUALIDAD = trim($data[14]);
                $MONTO_FINANCIADO = trim($data[15]);
                $MARCA = trim($data[16]);
                $MODELO = trim($data[17]);
                $LINEA = trim($data[18]);
                $TIPO_VEHICULO = trim($data[19]);
                $ANIO = trim($data[20]);
                $VENDEDOR_ASIGNADO = trim($data[21]);
                
                $ASIGNADO = trim($data[22]);
                $EMAIL = trim($data[23]);
                CPVentasModel::create([
                    CPVentasModel::NO_CUENTA => $NO_CUENTA,
                    CPVentasModel::VIN => $VIN,
                    CPVentasModel::APELLIDO => $APELLIDO,
                    CPVentasModel::NOMBRE => $NOMBRE,
                    CPVentasModel::TIPO_CLIENTE => $TIPO_CLIENTE,
                    CPVentasModel::TELEFONO_OFICINA => $TELEFONO_OFICINA,
                    CPVentasModel::TELEFONO_CASA => $TELEFONO_CASA,
                    CPVentasModel::ENGANCHE => $ENGANCHE,
                    CPVentasModel::MESES_CONTRATO => $MESES_CONTRATO,
                    CPVentasModel::FECHA_INICIO_CONTRATO =>  date('Y-M-d', strtotime($FECHA_INICIO_CONTRATO)),
                    CPVentasModel::FECHA_TERMINO => date('Y-M-d', strtotime($FECHA_TERMINO)),
                    CPVentasModel::TASA_INTERES => $TASA_INTERES,
                    CPVentasModel::MENSUALIDAD => $MENSUALIDAD,
                    CPVentasModel::MONTO_FINANCIADO => $MONTO_FINANCIADO,
                    CPVentasModel::MARCA => $MARCA,
                    CPVentasModel::MODELO => $MODELO,
                    CPVentasModel::LINEA => $LINEA,
                    CPVentasModel::TIPO_VEHICULO => $TIPO_VEHICULO,
                    CPVentasModel::ANIO => $ANIO,
                    CPVentasModel::VENDEDOR_ASIGNADO => $VENDEDOR_ASIGNADO,
                    CPVentasModel::PLAN_FINANCIAMIENTO => $PLAN_FINANCIAMIENTO,
                    CPVentasModel::ASIGNADO => $ASIGNADO,
                    CPVentasModel::EMAIL => $EMAIL
                ]);
            }
        }
    }
}
