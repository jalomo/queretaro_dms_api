<?php

use App\Models\Usuarios\MenuVistasModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenuVistasSeeder extends Seeder
{

    
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $filename = 'csv/sistema/menu_vistas.csv';
        $file_path = base_path('database/' . $filename);
        if (file_exists($file_path)) {
            $file = fopen($file_path, 'r');

            while (($data = fgetcsv($file)) !== false) {
                DB::table(MenuVistasModel::getTableName())->insert([
                    MenuVistasModel::ID => trim($data[0]) ? utf8_encode(trim($data[0])) : '',
                    MenuVistasModel::NOMBRE => trim($data[1]) ? utf8_encode(trim($data[1])) : '',
                    MenuVistasModel::ES_EXTERNA => trim($data[2]) ? trim($data[2]) :  FALSE,
                    MenuVistasModel::LINK => trim($data[3]) ? trim($data[3]) :  '',
                    MenuVistasModel::CONTROLADOR => trim($data[4]) ? trim($data[4]) :  '',
                    MenuVistasModel::MODULO => trim($data[5]) ? trim($data[5]) :  '',
                    MenuVistasModel::SUBMENU_ID => trim($data[6]) ? trim($data[6]) :  ''
                ]);
            }
        }
    }

}
