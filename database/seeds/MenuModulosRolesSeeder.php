<?php

use App\Models\Usuarios\Menu\ModulosModel;
use App\Models\Usuarios\MenuSeccionesModel;
use App\Models\Usuarios\MenuSubmenuModel;
use App\Models\Usuarios\MenuVistasModel;
use App\Models\Usuarios\RolesModulosModel;
use App\Models\Usuarios\RolesSeccionesModel;
use App\Models\Usuarios\RolesSubmenuModel;
use App\Models\Usuarios\RolesVistasModel;
use App\Models\Usuarios\ReRolesModulosModel;
use App\Models\Usuarios\RolModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenuModulosRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->modulosmodel = new ModulosModel();
        $this->rolessmodel = new RolModel();
        $this->menuSeccionesModel = new MenuSeccionesModel();
        $this->menuSubmenuModel = new MenuSubmenuModel();
        $this->menuVistasModel = new MenuVistasModel();
        $this->reRolesModulosModel = new ReRolesModulosModel();

        //$total_modulos = $this->modulosmodel->get();
        //$roles = $this->rolessmodel->get();

        // DB::table(RolesModulosModel::getTableName())->delete();
        // DB::table(RolesSeccionesModel::getTableName())->delete();
        // DB::table(RolesVistasModel::getTableName())->delete();
        foreach ($this->reRolesModulosModel->get() as $mod) {
            DB::table(RolesModulosModel::getTableName())->insert([
                RolesModulosModel::ROL_ID => $mod->rol_id,
                RolesModulosModel::MODULO_ID => $mod->modulo_id
            ]);

            $secciones = $this->menuSeccionesModel->where(MenuSeccionesModel::MODULO_ID, $mod->modulo_id)->get();
            foreach ($secciones as $seccion) {
                DB::table(RolesSeccionesModel::getTableName())->insert([
                    RolesSeccionesModel::ROL_ID => $mod->rol_id,
                    RolesSeccionesModel::SECCION_ID => $seccion->id
                ]);

                $submenus = $this->menuSubmenuModel->where(MenuSubmenuModel::SECCION_ID, $seccion->id)->get();
                foreach ($submenus as $submenu) {
                    DB::table(RolesSubmenuModel::getTableName())->insert([
                        RolesSubmenuModel::ROL_ID =>  $mod->rol_id,
                        RolesSubmenuModel::SUBMENU_ID => $submenu->id
                    ]);

                    $vistas = $this->menuVistasModel->where(MenuVistasModel::SUBMENU_ID, $submenu->id)->get();
                    foreach ($vistas as $vista) {
                        DB::table(RolesVistasModel::getTableName())->insert([
                            RolesVistasModel::ROL_ID =>  $mod->rol_id,
                            RolesVistasModel::VISTA_ID => $vista->id
                        ]);
                    }
                }
            }
        }
    }
}
