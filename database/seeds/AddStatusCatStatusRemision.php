<?php

use App\Models\Autos\UnidadesNuevas\CatStatusRemision;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AddStatusCatStatusRemision extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                CatStatusRemision::ID => 3,
                CatStatusRemision::ESTATUS => 'No Disponible'
            ],
        ];

        foreach ($data as $value) {
            DB::table(CatStatusRemision::getTableName())->insert($value);
        }
    }
}
