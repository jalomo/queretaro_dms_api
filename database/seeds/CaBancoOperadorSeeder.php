<?php

use Illuminate\Database\Seeder;
use App\Models\Nomina\CaBancoOperadorModel as Model;

class CaBancoOperadorSeeder extends Seeder
{
    private function seedData(){
        return [
            [Model::ID => 1, Model::Clave => '002',Model::Descripcion => 'BANAMEX'],
            [Model::ID => 2, Model::Clave => '006',Model::Descripcion => 'BANCOMEXT'],
            [Model::ID => 3, Model::Clave => '009',Model::Descripcion => 'BANOBRAS'],
            [Model::ID => 4, Model::Clave => '012',Model::Descripcion => 'BBVA BANCOMER'],
            [Model::ID => 5, Model::Clave => '014',Model::Descripcion => 'SANTANDER']
        ];
    }

    public function run()
    {
        foreach ($this->seedData() as $key => $items) {
            $exists = DB::connection(Model::connectionName())->table(Model::getTableName())->whereNotNull(MODEL::ID)->where(MODEL::ID, $items[MODEL::ID])->first();
            if($exists == false){
                DB::connection(Model::connectionName())->table(Model::getTableName())->insert($items);
            }
        }
    }
}
