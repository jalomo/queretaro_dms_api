<?php

use App\Models\Soporte\CatStatusPrioridadesModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatPrioridadesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                CatStatusPrioridadesModel::ID => 1,
                CatStatusPrioridadesModel::PRIORIDAD => 'Alta Prioridad'
            ],
            [
                CatStatusPrioridadesModel::ID => 2,
                CatStatusPrioridadesModel::PRIORIDAD => 'Moderada'
            ],
            [
                CatStatusPrioridadesModel::ID => 3,
                CatStatusPrioridadesModel::PRIORIDAD => 'Leve'
            ],
           
        );

        foreach ($data as $key => $items) {
            DB::table(CatStatusPrioridadesModel::getTableName())->insert($items);
        }
    }
}
