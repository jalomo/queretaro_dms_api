<?php

use App\Models\Contabilidad\CatalogoCuentasModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatalogoCuentasSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1,
                CatalogoCuentasModel::NO_CUENTA => 100000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAJA Y BANCOS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2,
                CatalogoCuentasModel::NO_CUENTA => 100001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAJA VENTAS QRO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 3,
                CatalogoCuentasModel::NO_CUENTA => 100002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAJA VENTAS  SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 4,
                CatalogoCuentasModel::NO_CUENTA => 100003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAJA VENTAS EZEQUIEL MONTES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 5,
                CatalogoCuentasModel::NO_CUENTA => 100004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAJA REFACC QRO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 6,
                CatalogoCuentasModel::NO_CUENTA => 100005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAJA SERV QRO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 7,
                CatalogoCuentasModel::NO_CUENTA => 100006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAJA GENERAL QRO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 8,
                CatalogoCuentasModel::NO_CUENTA => 100007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAJA ADMON QRO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 9,
                CatalogoCuentasModel::NO_CUENTA => 100008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAJA INTERCAMBIOS QRO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 10,
                CatalogoCuentasModel::NO_CUENTA => 100009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAJA LINCON",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 11,
                CatalogoCuentasModel::NO_CUENTA => 101000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FONDO FIJO",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 12,
                CatalogoCuentasModel::NO_CUENTA => 101001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAJA QUERETARO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 11
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 13,
                CatalogoCuentasModel::NO_CUENTA => 101002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAJA REFACCIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 11
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 14,
                CatalogoCuentasModel::NO_CUENTA => 101003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAJA SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 11
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 15,
                CatalogoCuentasModel::NO_CUENTA => 101004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ROSA ELVA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 11
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 16,
                CatalogoCuentasModel::NO_CUENTA => 101005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BANORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 11
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 17,
                CatalogoCuentasModel::NO_CUENTA => 101006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SANTANDER SERFIN",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 11
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 18,
                CatalogoCuentasModel::NO_CUENTA => 101007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BANORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 11
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 19,
                CatalogoCuentasModel::NO_CUENTA => 101008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BANCOMER CTA.EMPRESARIAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 11
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 20,
                CatalogoCuentasModel::NO_CUENTA => 101010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPOSITOS EN TRANSITO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 11
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 21,
                CatalogoCuentasModel::NO_CUENTA => 102000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BANCOS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 22,
                CatalogoCuentasModel::NO_CUENTA => 102001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BANAMEX",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 21
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 23,
                CatalogoCuentasModel::NO_CUENTA => 102002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BANCOMER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 21
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 24,
                CatalogoCuentasModel::NO_CUENTA => 103000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INVERSIONES EN VALORES",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 25,
                CatalogoCuentasModel::NO_CUENTA => 104000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPOSITOS EN TRANSITO",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 26,
                CatalogoCuentasModel::NO_CUENTA => 105000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BANCOS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 27,
                CatalogoCuentasModel::NO_CUENTA => 105001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BANCOMER CTA 1260",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 26
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 28,
                CatalogoCuentasModel::NO_CUENTA => 105002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BANCOMER CTA. 5037",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 26
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 29,
                CatalogoCuentasModel::NO_CUENTA => 105003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BANAMEX CTA 6406",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 26
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 30,
                CatalogoCuentasModel::NO_CUENTA => 105004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BANAMEX CTA 8714",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 26
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 31,
                CatalogoCuentasModel::NO_CUENTA => 105005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BANAMEX CTA 6133",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 26
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 32,
                CatalogoCuentasModel::NO_CUENTA => 105006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BANORTE CTA 9594",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 26
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 33,
                CatalogoCuentasModel::NO_CUENTA => 105007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HSBC CTA 7302",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 26
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 34,
                CatalogoCuentasModel::NO_CUENTA => 105008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SANTANDER CTA 8011",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 26
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 35,
                CatalogoCuentasModel::NO_CUENTA => 105009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SCOTIABANK CTA 5126",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 26
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 36,
                CatalogoCuentasModel::NO_CUENTA => 105010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BANORTE  0936",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 26
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 37,
                CatalogoCuentasModel::NO_CUENTA => 105011,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BBVA INVERSIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 26
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 38,
                CatalogoCuentasModel::NO_CUENTA => 105012,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BANAMEX 1904",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 26
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 39,
                CatalogoCuentasModel::NO_CUENTA => 110000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CUENTAS POR COBRAR",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 40,
                CatalogoCuentasModel::NO_CUENTA => 110001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CLIENTES AUTOS NUEVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 39
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 41,
                CatalogoCuentasModel::NO_CUENTA => 110002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CLIENTES AUTOS SEMINUEVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 39
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 42,
                CatalogoCuentasModel::NO_CUENTA => 110003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CLIENTES REFACCIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 39
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 43,
                CatalogoCuentasModel::NO_CUENTA => 110004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CLIENTES SERVICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 39
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 44,
                CatalogoCuentasModel::NO_CUENTA => 110007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DOCUMENTOS POR COBRAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 39
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 45,
                CatalogoCuentasModel::NO_CUENTA => 110008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GARANTIAS POR COBRAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 39
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 46,
                CatalogoCuentasModel::NO_CUENTA => 110009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FORD MOTOR 6BO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 39
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 47,
                CatalogoCuentasModel::NO_CUENTA => 110010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INCENTIVOS POR COBRAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 39
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 48,
                CatalogoCuentasModel::NO_CUENTA => 110011,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTRAS CUENTAS POR COBRAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 39
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 49,
                CatalogoCuentasModel::NO_CUENTA => 110012,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PUBLICIDAD A.M.D.F.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 39
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 50,
                CatalogoCuentasModel::NO_CUENTA => 110013,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HOLD BACK",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 39
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 51,
                CatalogoCuentasModel::NO_CUENTA => 111000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CLIENTES",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 52,
                CatalogoCuentasModel::NO_CUENTA => 111001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CLIENTES AUTOS NUEVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 51
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 53,
                CatalogoCuentasModel::NO_CUENTA => 111002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CLIENTES SEMINUEVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 51
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 54,
                CatalogoCuentasModel::NO_CUENTA => 111003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CLIENTES INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 51
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 55,
                CatalogoCuentasModel::NO_CUENTA => 111004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CLIENTES REFACCIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 51
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 56,
                CatalogoCuentasModel::NO_CUENTA => 111005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CLIENTES SERVICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 51
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 57,
                CatalogoCuentasModel::NO_CUENTA => 111006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DOCUMENTOS POR COBRAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 51
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 58,
                CatalogoCuentasModel::NO_CUENTA => 111007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RESERVA PARA CUENTAS INCOBRABLES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 51
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 59,
                CatalogoCuentasModel::NO_CUENTA => 111008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CLIENTES OTROS (POR DEPURAR)",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 51
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 60,
                CatalogoCuentasModel::NO_CUENTA => 111100,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UNIDADES CONTADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 51
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 61,
                CatalogoCuentasModel::NO_CUENTA => 111111,
                CatalogoCuentasModel::NOMBRE_CUENTA => 'SIN NOMBRE',
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 51
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 62,
                CatalogoCuentasModel::NO_CUENTA => 111200,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CLIENTES INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 51
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 63,
                CatalogoCuentasModel::NO_CUENTA => 112000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FORD CREDIT POR COBRAR",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 64,
                CatalogoCuentasModel::NO_CUENTA => 112001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FORD CREDIT UNIDADES USADAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 63
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 65,
                CatalogoCuentasModel::NO_CUENTA => 112002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FORD CREDIT UNIDADES NUEVAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 63
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 66,
                CatalogoCuentasModel::NO_CUENTA => 112003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FORD CREDIT APERTURAS DE CREDITO POR PAGAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 63
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 67,
                CatalogoCuentasModel::NO_CUENTA => 112004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FORD CREDIT HOLD BACK",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 63
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 68,
                CatalogoCuentasModel::NO_CUENTA => 112005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FORD CREDIT OTROS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 63
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 69,
                CatalogoCuentasModel::NO_CUENTA => 112010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "6B0 POR COBRAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 63
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 70,
                CatalogoCuentasModel::NO_CUENTA => 113000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FORD GARANTIAS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 71,
                CatalogoCuentasModel::NO_CUENTA => 113001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "6B0 POR COBRAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 70
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 72,
                CatalogoCuentasModel::NO_CUENTA => 113002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GARANTIAS FORD MOTOR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 70
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 73,
                CatalogoCuentasModel::NO_CUENTA => 114000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEUDORES DIVERSOS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 74,
                CatalogoCuentasModel::NO_CUENTA => 114001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FUNCIONARIOS Y EMPLEADOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 73
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 75,
                CatalogoCuentasModel::NO_CUENTA => 114002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEUDORES DIVERSOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 73
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 76,
                CatalogoCuentasModel::NO_CUENTA => 115000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CUENTAS POR COBRAR OTRAS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 77,
                CatalogoCuentasModel::NO_CUENTA => 115001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CUENTA POR COBRAR UDI`S",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 76
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 78,
                CatalogoCuentasModel::NO_CUENTA => 115002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AMDF PUBLICIDAD POR COBRAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 76
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 79,
                CatalogoCuentasModel::NO_CUENTA => 115005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IVA ACREDITABLE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 76
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 80,
                CatalogoCuentasModel::NO_CUENTA => 115010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEUD.DIVERS. RUIZ ALONSO BEATRIZ",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 76
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 81,
                CatalogoCuentasModel::NO_CUENTA => 115015,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IVA POR ACREDITAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 76
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 82,
                CatalogoCuentasModel::NO_CUENTA => 115020,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IVA A FAVOR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 76
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 83,
                CatalogoCuentasModel::NO_CUENTA => 115500,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SEGUROS POR COBRAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 76
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 84,
                CatalogoCuentasModel::NO_CUENTA => 116000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMPUESTOS POR ACREDITAR",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 85,
                CatalogoCuentasModel::NO_CUENTA => 116001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IVA POR ACREDITAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 84
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 86,
                CatalogoCuentasModel::NO_CUENTA => 116002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IVA REALMENTE PAGADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 84
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 87,
                CatalogoCuentasModel::NO_CUENTA => 116003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ISR RETENIDO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 84
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 88,
                CatalogoCuentasModel::NO_CUENTA => 116004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUBSIDIO PARA EL EMPLEO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 84
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 89,
                CatalogoCuentasModel::NO_CUENTA => 117000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CHEQUES DEVUELTOS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 90,
                CatalogoCuentasModel::NO_CUENTA => 117500,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CUENTAS POR COBRAR UDI'S",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 89
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 91,
                CatalogoCuentasModel::NO_CUENTA => 118000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ADMF-PUBLICIDAD",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 92,
                CatalogoCuentasModel::NO_CUENTA => 119000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GARANTIAS FORD MOTOR",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 93,
                CatalogoCuentasModel::NO_CUENTA => 119001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOMOCO 6B0",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 92
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 94,
                CatalogoCuentasModel::NO_CUENTA => 120000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INVENTARIO DE UNIDADES",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 95,
                CatalogoCuentasModel::NO_CUENTA => 121000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INVENTARIO DE UNIDADES NUEVAS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 96,
                CatalogoCuentasModel::NO_CUENTA => 121001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BRONCO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 95
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 97,
                CatalogoCuentasModel::NO_CUENTA => 121002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECONOLINE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 95
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 98,
                CatalogoCuentasModel::NO_CUENTA => 121003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPORT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 95
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 99,
                CatalogoCuentasModel::NO_CUENTA => 121004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EDGE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 95
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 100,
                CatalogoCuentasModel::NO_CUENTA => 121005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ESCAPE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 95
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 101,
                CatalogoCuentasModel::NO_CUENTA => 121006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPEDITION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 95
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 102,
                CatalogoCuentasModel::NO_CUENTA => 121007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPLORER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 95
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 103,
                CatalogoCuentasModel::NO_CUENTA => 121008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-150",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 95
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 104,
                CatalogoCuentasModel::NO_CUENTA => 121009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-250",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 95
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 105,
                CatalogoCuentasModel::NO_CUENTA => 121010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-350",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 95
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 106,
                CatalogoCuentasModel::NO_CUENTA => 121011,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-450",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 95
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 107,
                CatalogoCuentasModel::NO_CUENTA => 121012,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-550",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 95
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 108,
                CatalogoCuentasModel::NO_CUENTA => 121013,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIESTA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 95
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 109,
                CatalogoCuentasModel::NO_CUENTA => 121014,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOCUS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 95
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 110,
                CatalogoCuentasModel::NO_CUENTA => 121015,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FUSION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 95
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 111,
                CatalogoCuentasModel::NO_CUENTA => 121016,
                CatalogoCuentasModel::NOMBRE_CUENTA => "LOBO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 95
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 112,
                CatalogoCuentasModel::NO_CUENTA => 121017,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MUSTANG",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 95
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 113,
                CatalogoCuentasModel::NO_CUENTA => 121018,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RANGER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 95
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 114,
                CatalogoCuentasModel::NO_CUENTA => 121019,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SPORT TRAC",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 95
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 115,
                CatalogoCuentasModel::NO_CUENTA => 121020,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TRANSIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 95
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 116,
                CatalogoCuentasModel::NO_CUENTA => 121021,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIGO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 95
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 117,
                CatalogoCuentasModel::NO_CUENTA => 122000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INVENTARIO UNIDADES USADAS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 118,
                CatalogoCuentasModel::NO_CUENTA => 122001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INV. USADOS AUTOS EXENTOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 117
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 119,
                CatalogoCuentasModel::NO_CUENTA => 122002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INV. USADOS AUTOS GRAVADOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 117
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 120,
                CatalogoCuentasModel::NO_CUENTA => 122003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INV. USADOS CAMIONES EXENTOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 117
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 121,
                CatalogoCuentasModel::NO_CUENTA => 122004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INV. USADOS CAMIONES GRAVADOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 117
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 122,
                CatalogoCuentasModel::NO_CUENTA => 123000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FORD CREDIT OTROS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 123,
                CatalogoCuentasModel::NO_CUENTA => 123001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AUTOS USADOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 122
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 124,
                CatalogoCuentasModel::NO_CUENTA => 123002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FORD CREDIT UNIDADES NUEVAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 122
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 125,
                CatalogoCuentasModel::NO_CUENTA => 123003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FORD CREDIT APERTURAS DE CREDITO POR PAGAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 122
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 126,
                CatalogoCuentasModel::NO_CUENTA => 124000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FUNCIONARIOS Y EMPLEADOS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 127,
                CatalogoCuentasModel::NO_CUENTA => 124001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AUTOS USADOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 126
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 128,
                CatalogoCuentasModel::NO_CUENTA => 125000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ANTICIPOS DE COMISIONES",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 129,
                CatalogoCuentasModel::NO_CUENTA => 127000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RESERVA PARA CUENTAS POR COBRAR",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 130,
                CatalogoCuentasModel::NO_CUENTA => 128000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IVA ACREDITABLE",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 131,
                CatalogoCuentasModel::NO_CUENTA => 128001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IVA REALMENTE PAGADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 130
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 132,
                CatalogoCuentasModel::NO_CUENTA => 128002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IVA POR ACREDITAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 130
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 133,
                CatalogoCuentasModel::NO_CUENTA => 128003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IVA REALMENTE PAGADO 10 %",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 130
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 134,
                CatalogoCuentasModel::NO_CUENTA => 129000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMPUESTOS RETENIDOS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 135,
                CatalogoCuentasModel::NO_CUENTA => 129001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ISR RETENIDO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 136,
                CatalogoCuentasModel::NO_CUENTA => 129002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ISAN POR COBRAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 137,
                CatalogoCuentasModel::NO_CUENTA => 129500,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMPUESTOS POR RECUPERAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 138,
                CatalogoCuentasModel::NO_CUENTA => 129600,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMPUESTOS POR APLICAR (COMPENSACION)",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 139,
                CatalogoCuentasModel::NO_CUENTA => 130000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INVENTARIO DE REFACCIONES",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 140,
                CatalogoCuentasModel::NO_CUENTA => 130001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOCUS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 139
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 141,
                CatalogoCuentasModel::NO_CUENTA => 130002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SABLE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 139
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 142,
                CatalogoCuentasModel::NO_CUENTA => 130003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MONDEO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 139
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 143,
                CatalogoCuentasModel::NO_CUENTA => 130004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRAND MARQUIS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 139
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 144,
                CatalogoCuentasModel::NO_CUENTA => 130005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MUSTANG",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 139
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 145,
                CatalogoCuentasModel::NO_CUENTA => 130006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IKON",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 139
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 146,
                CatalogoCuentasModel::NO_CUENTA => 130007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "KA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 139
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 147,
                CatalogoCuentasModel::NO_CUENTA => 130008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIESTA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 139
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 148,
                CatalogoCuentasModel::NO_CUENTA => 130015,
                CatalogoCuentasModel::NOMBRE_CUENTA => "LOBO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 139
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 149,
                CatalogoCuentasModel::NO_CUENTA => 130016,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMIONES PESADOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 139
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 150,
                CatalogoCuentasModel::NO_CUENTA => 130017,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPLORER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 139
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 151,
                CatalogoCuentasModel::NO_CUENTA => 130018,
                CatalogoCuentasModel::NOMBRE_CUENTA => "WINDSTAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 139
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 152,
                CatalogoCuentasModel::NO_CUENTA => 130019,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TRANSIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 139
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 153,
                CatalogoCuentasModel::NO_CUENTA => 130020,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECONOLINE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 139
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 154,
                CatalogoCuentasModel::NO_CUENTA => 130021,
                CatalogoCuentasModel::NOMBRE_CUENTA => "THUNDERBIRD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 139
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 155,
                CatalogoCuentasModel::NO_CUENTA => 130022,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPORT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 139
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 156,
                CatalogoCuentasModel::NO_CUENTA => 130023,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIVE HUNDRED",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 139
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 157,
                CatalogoCuentasModel::NO_CUENTA => 130024,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FUSION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 139
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 158,
                CatalogoCuentasModel::NO_CUENTA => 131000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INV. REFACCIONES QUERETARO",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 159,
                CatalogoCuentasModel::NO_CUENTA => 132000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INVENTARIO REFACCIONES S.J.R.",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 160,
                CatalogoCuentasModel::NO_CUENTA => 133000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INV. REFACCIONES HYP",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 161,
                CatalogoCuentasModel::NO_CUENTA => 133021,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ESCAPE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 160
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 162,
                CatalogoCuentasModel::NO_CUENTA => 133022,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPEDITION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 160
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 163,
                CatalogoCuentasModel::NO_CUENTA => 133023,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXCURTION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 160
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 164,
                CatalogoCuentasModel::NO_CUENTA => 133024,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EDGE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 160
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 165,
                CatalogoCuentasModel::NO_CUENTA => 134000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INV. ACCESORIOS QUERETARO",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 166,
                CatalogoCuentasModel::NO_CUENTA => 135000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INV. ACCESORIOS S.J.R.",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 167,
                CatalogoCuentasModel::NO_CUENTA => 135001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOCUS SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 166
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 168,
                CatalogoCuentasModel::NO_CUENTA => 135002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SABLE SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 166
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 169,
                CatalogoCuentasModel::NO_CUENTA => 135003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MONDEO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 166
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 170,
                CatalogoCuentasModel::NO_CUENTA => 135004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRAND MARQUIZ",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 166
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 171,
                CatalogoCuentasModel::NO_CUENTA => 135005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MUSTANG SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 166
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 172,
                CatalogoCuentasModel::NO_CUENTA => 135006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IKON SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 166
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 173,
                CatalogoCuentasModel::NO_CUENTA => 135007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "KA SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 166
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 174,
                CatalogoCuentasModel::NO_CUENTA => 135008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIESTA SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 166
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 175,
                CatalogoCuentasModel::NO_CUENTA => 135009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPLORER SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 166
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 176,
                CatalogoCuentasModel::NO_CUENTA => 135010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FREESTART SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 166
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 177,
                CatalogoCuentasModel::NO_CUENTA => 135011,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECONOLINE SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 166
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 178,
                CatalogoCuentasModel::NO_CUENTA => 135012,
                CatalogoCuentasModel::NOMBRE_CUENTA => "THUNDERBIRD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 166
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 179,
                CatalogoCuentasModel::NO_CUENTA => 135013,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPORT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 166
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 180,
                CatalogoCuentasModel::NO_CUENTA => 135014,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIVEHUNDRED",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 166
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 181,
                CatalogoCuentasModel::NO_CUENTA => 135015,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ESCAPE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 166
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 182,
                CatalogoCuentasModel::NO_CUENTA => 135016,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPEDITION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 166
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 183,
                CatalogoCuentasModel::NO_CUENTA => 135017,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXCURSION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 166
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 184,
                CatalogoCuentasModel::NO_CUENTA => 135018,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COURIER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 166
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 185,
                CatalogoCuentasModel::NO_CUENTA => 135019,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FUSION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 166
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 186,
                CatalogoCuentasModel::NO_CUENTA => 135101,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-150 SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 166
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 187,
                CatalogoCuentasModel::NO_CUENTA => 135102,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-250 SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 166
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 188,
                CatalogoCuentasModel::NO_CUENTA => 135103,
                CatalogoCuentasModel::NOMBRE_CUENTA => "LOBO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 166
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 189,
                CatalogoCuentasModel::NO_CUENTA => 135104,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RANGER SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 166
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 190,
                CatalogoCuentasModel::NO_CUENTA => 135105,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COURIER SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 166
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 191,
                CatalogoCuentasModel::NO_CUENTA => 135200,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMIONES PESADOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 166
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 192,
                CatalogoCuentasModel::NO_CUENTA => 135201,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-350 SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 166
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 193,
                CatalogoCuentasModel::NO_CUENTA => 135202,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-450",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 166
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 194,
                CatalogoCuentasModel::NO_CUENTA => 135203,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-550",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 166
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 195,
                CatalogoCuentasModel::NO_CUENTA => 136000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INV. ACCESORIOS HYP",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 196,
                CatalogoCuentasModel::NO_CUENTA => 137000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INV. OBSOLETO REFACCIONES",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 197,
                CatalogoCuentasModel::NO_CUENTA => 140000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INV. TRABAJOS FUERA DE TALLER (TOT)",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 198,
                CatalogoCuentasModel::NO_CUENTA => 140001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AUTOMOVILES GRAVADOS EN PROCESO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 197
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 199,
                CatalogoCuentasModel::NO_CUENTA => 140002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AUTOMOVILES EXCENTOS EN PROCESO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 197
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 200,
                CatalogoCuentasModel::NO_CUENTA => 141000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INV. TOT QUERETARO",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 201,
                CatalogoCuentasModel::NO_CUENTA => 141001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AUTOMOVILES GRAVADOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 200
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 202,
                CatalogoCuentasModel::NO_CUENTA => 141002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AUTOMOVILES EXENTOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 200
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 203,
                CatalogoCuentasModel::NO_CUENTA => 142000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INV. TOT S.J.R.",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 204,
                CatalogoCuentasModel::NO_CUENTA => 142001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMIONES GRAVADOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 203
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 205,
                CatalogoCuentasModel::NO_CUENTA => 142002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMIONES EXENTOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 203
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 206,
                CatalogoCuentasModel::NO_CUENTA => 143000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INV. TOT HYP",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 207,
                CatalogoCuentasModel::NO_CUENTA => 143001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AUTOS SEMINUEVOS GRAVADOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 206
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 208,
                CatalogoCuentasModel::NO_CUENTA => 143002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AUTOS SEMINUEVOS EXENTOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 206
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 209,
                CatalogoCuentasModel::NO_CUENTA => 144001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INVENTARIO DE CAMIONES GRAVADOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 206
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 210,
                CatalogoCuentasModel::NO_CUENTA => 144002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INVENTARIO DE CAMIONES EXENTOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 206
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 211,
                CatalogoCuentasModel::NO_CUENTA => 150000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INVENTARIO EN PROCESO",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 212,
                CatalogoCuentasModel::NO_CUENTA => 150001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REFACCIONES - INVENTARIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 211
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 213,
                CatalogoCuentasModel::NO_CUENTA => 150002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ORDENES EN PROCESO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 211
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 214,
                CatalogoCuentasModel::NO_CUENTA => 150003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TRABAJOS FUERA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 211
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 215,
                CatalogoCuentasModel::NO_CUENTA => 150004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INVENTARIO DE HTAS TALLLER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 211
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 216,
                CatalogoCuentasModel::NO_CUENTA => 150005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ORDENES EN PROCESO OTROS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 211
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 217,
                CatalogoCuentasModel::NO_CUENTA => 150006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACCESORIOS HYP",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 211
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 218,
                CatalogoCuentasModel::NO_CUENTA => 150007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INVENTARIO DE HOJALATERIA Y PINTURA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 211
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 219,
                CatalogoCuentasModel::NO_CUENTA => 150008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACCESORIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 211
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 220,
                CatalogoCuentasModel::NO_CUENTA => 150009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ORDENES EN PROCESO ACC.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 211
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 221,
                CatalogoCuentasModel::NO_CUENTA => 150100,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REFACCIONES Y OTROS INVE SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 211
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 222,
                CatalogoCuentasModel::NO_CUENTA => 150101,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REFACCIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 211
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 223,
                CatalogoCuentasModel::NO_CUENTA => 150102,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ORDENES EN PROCESO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 211
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 224,
                CatalogoCuentasModel::NO_CUENTA => 150103,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TRABAJOS POR FUERA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 211
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 225,
                CatalogoCuentasModel::NO_CUENTA => 150104,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ORDENES EN PROCESO DE HYP",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 211
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 226,
                CatalogoCuentasModel::NO_CUENTA => 150105,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ORDENES EN PROCESO OTROS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 211
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 227,
                CatalogoCuentasModel::NO_CUENTA => 150107,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INVENTARIO DE HOJALAT Y PINTURA SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 211
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 228,
                CatalogoCuentasModel::NO_CUENTA => 150108,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACCESORIOS SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 211
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 229,
                CatalogoCuentasModel::NO_CUENTA => 150201,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ALMACEN VENTANILLA 1",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 211
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 230,
                CatalogoCuentasModel::NO_CUENTA => 151000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INV. PROCESO QUERETARO",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 231,
                CatalogoCuentasModel::NO_CUENTA => 152000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INV. PROCESO S.J.R.",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 232,
                CatalogoCuentasModel::NO_CUENTA => 153000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INC. PROCESOS HYP",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 233,
                CatalogoCuentasModel::NO_CUENTA => 154000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INV. PROCESO GARANTIAS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 234,
                CatalogoCuentasModel::NO_CUENTA => 155000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INV. PROCESO TOT QUERETARO",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 235,
                CatalogoCuentasModel::NO_CUENTA => 156000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INV. PROCESO TOT SJR",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 236,
                CatalogoCuentasModel::NO_CUENTA => 157000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INV. PROCESO TOT HYP",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 237,
                CatalogoCuentasModel::NO_CUENTA => 158000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INV. PROCESO TOT GARANTIAS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 238,
                CatalogoCuentasModel::NO_CUENTA => 159000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INV. PROCESO ACCESORIOS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 239,
                CatalogoCuentasModel::NO_CUENTA => 160000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACTIVO FIJO",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 240,
                CatalogoCuentasModel::NO_CUENTA => 160500,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RENTA EQ.COMPUTO PAG.POR ANTICIPADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 239
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 241,
                CatalogoCuentasModel::NO_CUENTA => 160501,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ANT. RENTA EQUIPO COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 239
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 242,
                CatalogoCuentasModel::NO_CUENTA => 161000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EDIFICIO",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 243,
                CatalogoCuentasModel::NO_CUENTA => 161001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EDIFICIO COSTO ORIGINAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 242
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 244,
                CatalogoCuentasModel::NO_CUENTA => 161002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EDIFICIO REEXPRESION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 242
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 245,
                CatalogoCuentasModel::NO_CUENTA => 162000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MAQUINARIA Y EQUIPO",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 246,
                CatalogoCuentasModel::NO_CUENTA => 162001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MAQ. Y EQUIPO COSTO ORIGINAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 245
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 247,
                CatalogoCuentasModel::NO_CUENTA => 162002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MAQ. Y EQUIPO REEXPRESION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 245
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 248,
                CatalogoCuentasModel::NO_CUENTA => 163000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EQUIPO DE OFICINA",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 249,
                CatalogoCuentasModel::NO_CUENTA => 163001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EQ. DE OFICINA COSTO ORIGINAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 248
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 250,
                CatalogoCuentasModel::NO_CUENTA => 163002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EQ. DE OFICINA REEXPRESION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 248
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 251,
                CatalogoCuentasModel::NO_CUENTA => 164000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EQUIPO DE TRANSPORTE",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 252,
                CatalogoCuentasModel::NO_CUENTA => 164001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EQ. DE TRANSPORTE COSTO ORIGINAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 251
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 253,
                CatalogoCuentasModel::NO_CUENTA => 164002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EQ. DE TRANSPORTE REEXPRESION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 251
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 254,
                CatalogoCuentasModel::NO_CUENTA => 164100,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ANTICIPOS A PROVEEDORES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 251
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 255,
                CatalogoCuentasModel::NO_CUENTA => 165000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 256,
                CatalogoCuentasModel::NO_CUENTA => 165001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EQ. DE COMPUTO COSTO ORIGINAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 255
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 257,
                CatalogoCuentasModel::NO_CUENTA => 165002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EQ. DE COMPUTO REEXPRESION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 255
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 258,
                CatalogoCuentasModel::NO_CUENTA => 166000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EQUIPO DE COMUNICACION Y TELEFONIA",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 259,
                CatalogoCuentasModel::NO_CUENTA => 166001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EQ. COMUNICACION COSTO ORIGINAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 258
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 260,
                CatalogoCuentasModel::NO_CUENTA => 166002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EQ. DE COMUNICACION REEXPRESION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 258
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 261,
                CatalogoCuentasModel::NO_CUENTA => 167000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MEJORAS A LOCALES ARRENDADOS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 262,
                CatalogoCuentasModel::NO_CUENTA => 167001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MEJORAS LOCAL ARRENDADO COSTO ORIGINAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 261
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 263,
                CatalogoCuentasModel::NO_CUENTA => 167002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MEJORAS LOCAL ARRENDADO REEXPRESION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 261
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 264,
                CatalogoCuentasModel::NO_CUENTA => 168000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ANUNCIOS LUMINOSOS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 265,
                CatalogoCuentasModel::NO_CUENTA => 168001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ANUNCIOS LUMINOSOS COSTO ORIGINAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 264
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 266,
                CatalogoCuentasModel::NO_CUENTA => 168002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ANUNCIOS LUMINOSOS REEXPRESION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 264
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 267,
                CatalogoCuentasModel::NO_CUENTA => 168003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OBRAS EN PROCESO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 264
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 268,
                CatalogoCuentasModel::NO_CUENTA => 169000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PAGOS ANTICIPADOS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 269,
                CatalogoCuentasModel::NO_CUENTA => 169001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ANTICIPO A PROVEEDORES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 268
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 270,
                CatalogoCuentasModel::NO_CUENTA => 169002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ANTICIPOS DE IMPUESTOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 268
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 271,
                CatalogoCuentasModel::NO_CUENTA => 169003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SEGUROS PAGADOS POR ANTICIPADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 268
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 272,
                CatalogoCuentasModel::NO_CUENTA => 169004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTROS ANTICIPOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 268
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 273,
                CatalogoCuentasModel::NO_CUENTA => 169005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CONSUMIBLES Y PAPELERIA PAGADOS POR ANT.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 268
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 274,
                CatalogoCuentasModel::NO_CUENTA => 169006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS PAGADOS POR ANTICIPADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 268
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 275,
                CatalogoCuentasModel::NO_CUENTA => 170000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION ACTIVO FIJO",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 276,
                CatalogoCuentasModel::NO_CUENTA => 170001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EDIFICIOS-COSTO ORIGINAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 275
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 277,
                CatalogoCuentasModel::NO_CUENTA => 170002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EDIFICIOS-REEXPRESION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 275
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 278,
                CatalogoCuentasModel::NO_CUENTA => 171000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION EDIFICIO",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 279,
                CatalogoCuentasModel::NO_CUENTA => 171001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COSTO ORIGINAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 278
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 280,
                CatalogoCuentasModel::NO_CUENTA => 171002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REEXPRESION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 278
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 281,
                CatalogoCuentasModel::NO_CUENTA => 172000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION MAQUINARIA Y EQUIPO",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 282,
                CatalogoCuentasModel::NO_CUENTA => 172001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COSTO ORIGINAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 281
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 283,
                CatalogoCuentasModel::NO_CUENTA => 172002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REEXPRESION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 281
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 284,
                CatalogoCuentasModel::NO_CUENTA => 173000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECACIOAN EQ. DE OFICINA",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 285,
                CatalogoCuentasModel::NO_CUENTA => 173001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COSTO ORIGINAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 284
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 286,
                CatalogoCuentasModel::NO_CUENTA => 173002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REEXPRESION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 284
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 287,
                CatalogoCuentasModel::NO_CUENTA => 174000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION EQ. DE TRANSPORTE",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 288,
                CatalogoCuentasModel::NO_CUENTA => 174001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COSTO ORIGINAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 287
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 289,
                CatalogoCuentasModel::NO_CUENTA => 174002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REEXPRESION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 287
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 290,
                CatalogoCuentasModel::NO_CUENTA => 174500,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EQ.DE COMUNICACIONES Y TELEFONIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 287
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 291,
                CatalogoCuentasModel::NO_CUENTA => 175000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECAICION EQ. DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 292,
                CatalogoCuentasModel::NO_CUENTA => 175001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COSTO ORIGINAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 291
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 293,
                CatalogoCuentasModel::NO_CUENTA => 175002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REEXPRESION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 291
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 294,
                CatalogoCuentasModel::NO_CUENTA => 175500,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ANUNCIOS LUMINOSOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 291
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 295,
                CatalogoCuentasModel::NO_CUENTA => 175501,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REEXPRESION ANUNCIOS LUMINOSOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 291
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 296,
                CatalogoCuentasModel::NO_CUENTA => 176000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION EQ. DE COMUNICACION Y TELEFONIA",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 297,
                CatalogoCuentasModel::NO_CUENTA => 176001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COSTO ORIGINAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 296
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 298,
                CatalogoCuentasModel::NO_CUENTA => 176002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REEXPRESION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 296
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 299,
                CatalogoCuentasModel::NO_CUENTA => 177000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION MEJORAS A LOCALES ARRENDADOS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 300,
                CatalogoCuentasModel::NO_CUENTA => 177001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COSTO ORIGINAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 299
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 301,
                CatalogoCuentasModel::NO_CUENTA => 177002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REEXPRESION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 299
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 302,
                CatalogoCuentasModel::NO_CUENTA => 178000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION ANUNCIOS LUMINOSOS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 303,
                CatalogoCuentasModel::NO_CUENTA => 178001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COSTO ORIGINAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 302
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 304,
                CatalogoCuentasModel::NO_CUENTA => 178002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REEXPRESION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 302
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 305,
                CatalogoCuentasModel::NO_CUENTA => 179000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AMORTIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 306,
                CatalogoCuentasModel::NO_CUENTA => 179001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AMORT. ACUMULADA MEJORAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 305
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 307,
                CatalogoCuentasModel::NO_CUENTA => 179002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AMORT. ACUMULADA ANUNCIOS LUMINOSOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 305
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 308,
                CatalogoCuentasModel::NO_CUENTA => 180000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTROS ACTIVOS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 309,
                CatalogoCuentasModel::NO_CUENTA => 180001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEP. ACUM.DE EDIFICIOS CTO. ORIGINAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 308
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 310,
                CatalogoCuentasModel::NO_CUENTA => 180002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEP. ACUM.DE EDIFICION REEXPRESION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 308
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 311,
                CatalogoCuentasModel::NO_CUENTA => 181000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPOSITOS EN GARANTIA",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 312,
                CatalogoCuentasModel::NO_CUENTA => 181001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEP.ACUM.DE MAQ. DE TALLER-REEXPRESION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 311
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 313,
                CatalogoCuentasModel::NO_CUENTA => 182000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INVERSIONES EN ACCIONES",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 314,
                CatalogoCuentasModel::NO_CUENTA => 182001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEP.ACUM.DE EQ. OFIC.CTO-REEXPRESION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 313
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 315,
                CatalogoCuentasModel::NO_CUENTA => 183000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INVERSIONES EN FILIALES",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 316,
                CatalogoCuentasModel::NO_CUENTA => 183001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEP.ACUM.DE EQ. TRANSP.-REEXPRESION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 315
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 317,
                CatalogoCuentasModel::NO_CUENTA => 184000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTROS ACTIVOS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 318,
                CatalogoCuentasModel::NO_CUENTA => 184001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REEXPRESION EQPO COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 317
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 319,
                CatalogoCuentasModel::NO_CUENTA => 184500,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPN EQUIPO DE COMUNIC. HISTORICO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 317
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 320,
                CatalogoCuentasModel::NO_CUENTA => 184501,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REEXPRESION EQPO COMUNICACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 317
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 321,
                CatalogoCuentasModel::NO_CUENTA => 184502,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEP ANUN LUM Y ACT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 317
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 322,
                CatalogoCuentasModel::NO_CUENTA => 184503,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION ACUM. ANUNCIOS - REEXPRE.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 317
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 323,
                CatalogoCuentasModel::NO_CUENTA => 185000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ISR DIFERIDO",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 324,
                CatalogoCuentasModel::NO_CUENTA => 185500,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AMORTIZACION DE ANUNCIOS LUMINOSOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 323
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 325,
                CatalogoCuentasModel::NO_CUENTA => 186000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ISR DIFERIDO",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 326,
                CatalogoCuentasModel::NO_CUENTA => 190000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTROS ACTIVOS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 327,
                CatalogoCuentasModel::NO_CUENTA => 190001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS PREOPERATIVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 326
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 328,
                CatalogoCuentasModel::NO_CUENTA => 190002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INVERSIONES POR APORTACIONES A FILIALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 326
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 329,
                CatalogoCuentasModel::NO_CUENTA => 191000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPOSITOS EN GARANTIAS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 330,
                CatalogoCuentasModel::NO_CUENTA => 192000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INVERSION EN ACCIONES RENTE",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 331,
                CatalogoCuentasModel::NO_CUENTA => 195000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTROS CUENTA PUENTE",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 332,
                CatalogoCuentasModel::NO_CUENTA => 199001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS PREOPERATIVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 331
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 333,
                CatalogoCuentasModel::NO_CUENTA => 200000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DOCUMENTOS POR PAGAR F.C.",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 334,
                CatalogoCuentasModel::NO_CUENTA => 200001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DOC.POR PAGAR UNIDADES F.C.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 333
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 335,
                CatalogoCuentasModel::NO_CUENTA => 200002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DOC.POR PAGAR REFACCIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 333
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 336,
                CatalogoCuentasModel::NO_CUENTA => 200003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DOC. POR PAGAR UNIDADES F.C. A\/F",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 333
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 337,
                CatalogoCuentasModel::NO_CUENTA => 200004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UNIDADES POR PAGAR FORD SEMINUEVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 333
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 338,
                CatalogoCuentasModel::NO_CUENTA => 200005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INTERES PLAN PISO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 333
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 339,
                CatalogoCuentasModel::NO_CUENTA => 200006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "C X P FORD OTROS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 333
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 340,
                CatalogoCuentasModel::NO_CUENTA => 200007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOMOCO GASTOS DE FIN DE MES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 333
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 341,
                CatalogoCuentasModel::NO_CUENTA => 200008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "APERTURAS DE CREDITO FORD CREDIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 333
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 342,
                CatalogoCuentasModel::NO_CUENTA => 201000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DOC. X PAGAR F.C. UNIDADES",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 343,
                CatalogoCuentasModel::NO_CUENTA => 201100,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PROVEEDORES AL CONTADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 342
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 344,
                CatalogoCuentasModel::NO_CUENTA => 201200,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PROVEEDORES SEMINUEVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 342
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 345,
                CatalogoCuentasModel::NO_CUENTA => 202000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DOC. X PAGAR F.C. REFACCIONES",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 346,
                CatalogoCuentasModel::NO_CUENTA => 202100,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMPORTACIONES SIAM",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 345
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 347,
                CatalogoCuentasModel::NO_CUENTA => 202400,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTRAS CUENTAS X PAGAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 345
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 348,
                CatalogoCuentasModel::NO_CUENTA => 202500,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FUNCIONARIOS Y EMPLEADOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 345
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 349,
                CatalogoCuentasModel::NO_CUENTA => 203000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DOC. X PAGAR F.C. UNIDADES ACTIVO FIJO",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 350,
                CatalogoCuentasModel::NO_CUENTA => 204000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INTERESES PLAN PISO",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 351,
                CatalogoCuentasModel::NO_CUENTA => 205000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTROS FORD CREDIT",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 352,
                CatalogoCuentasModel::NO_CUENTA => 210000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PROVEEDORES",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 353,
                CatalogoCuentasModel::NO_CUENTA => 210001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PROVEEDORES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 352
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 354,
                CatalogoCuentasModel::NO_CUENTA => 210002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PROVEEDORES CONTADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 352
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 355,
                CatalogoCuentasModel::NO_CUENTA => 210003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PROVEEDORES SEMINUEVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 352
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 356,
                CatalogoCuentasModel::NO_CUENTA => 210004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IVA RETENIDO 4%",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 352
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 357,
                CatalogoCuentasModel::NO_CUENTA => 210005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ISR RETENIDO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 352
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 358,
                CatalogoCuentasModel::NO_CUENTA => 210006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ISR DE SUELDOS Y SALARIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 352
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 359,
                CatalogoCuentasModel::NO_CUENTA => 210007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMPUESTO SUNTUARIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 352
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 360,
                CatalogoCuentasModel::NO_CUENTA => 210008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMPUESTO ESTATAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 352
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 361,
                CatalogoCuentasModel::NO_CUENTA => 210009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMSS RETENIDO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 352
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 362,
                CatalogoCuentasModel::NO_CUENTA => 210010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMSS PATRONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 352
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 363,
                CatalogoCuentasModel::NO_CUENTA => 210011,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 352
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 364,
                CatalogoCuentasModel::NO_CUENTA => 210012,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 352
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 365,
                CatalogoCuentasModel::NO_CUENTA => 210013,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AMORTIZACIONES INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 352
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 366,
                CatalogoCuentasModel::NO_CUENTA => 210014,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SIAM POR PAGAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 352
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 367,
                CatalogoCuentasModel::NO_CUENTA => 210015,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ISR PROVISIONAL Y ANUAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 352
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 368,
                CatalogoCuentasModel::NO_CUENTA => 210020,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ISR RET. HONS. P.FISICAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 352
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 369,
                CatalogoCuentasModel::NO_CUENTA => 210021,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ISR RET. ARREND. P.FISICAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 352
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 370,
                CatalogoCuentasModel::NO_CUENTA => 210028,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IVA RET POR COMIS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 352
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 371,
                CatalogoCuentasModel::NO_CUENTA => 210029,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IVA RET ARREND PERS FIS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 352
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 372,
                CatalogoCuentasModel::NO_CUENTA => 210030,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IVA RET. HONS. P.F.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 352
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 373,
                CatalogoCuentasModel::NO_CUENTA => 210031,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SIAM POR PAGAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 352
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 374,
                CatalogoCuentasModel::NO_CUENTA => 210040,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SIAM POR PAGAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 352
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 375,
                CatalogoCuentasModel::NO_CUENTA => 210101,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPOSITOS DE CLIENTES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 352
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 376,
                CatalogoCuentasModel::NO_CUENTA => 210500,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CHECAR CUENTA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 352
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 377,
                CatalogoCuentasModel::NO_CUENTA => 220000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACREEDORES",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 378,
                CatalogoCuentasModel::NO_CUENTA => 220001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACREEDORES DIVERSOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 377
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 379,
                CatalogoCuentasModel::NO_CUENTA => 220002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPOSITOS CLIENTES SERVICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 377
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 380,
                CatalogoCuentasModel::NO_CUENTA => 220003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPOSITOS PARA SEGUROS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 377
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 381,
                CatalogoCuentasModel::NO_CUENTA => 220004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES Y SUELDOS POR PAGAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 377
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 382,
                CatalogoCuentasModel::NO_CUENTA => 220005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INTERESES COB. POR ANTICIPADOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 377
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 383,
                CatalogoCuentasModel::NO_CUENTA => 220006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPOSITO DE CLIENTES SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 377
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 384,
                CatalogoCuentasModel::NO_CUENTA => 220007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPOSITOS DEDUCIBLES DE SEGUROS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 377
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 385,
                CatalogoCuentasModel::NO_CUENTA => 220008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INTEGRACION PASIVOS POR SUELDOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 377
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 386,
                CatalogoCuentasModel::NO_CUENTA => 220100,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPOSITOS SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 377
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 387,
                CatalogoCuentasModel::NO_CUENTA => 220101,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPOSITOS DE CLIENTES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 377
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 388,
                CatalogoCuentasModel::NO_CUENTA => 220102,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPOSITOS CLIENTES SAN JUAN DEL RIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 377
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 389,
                CatalogoCuentasModel::NO_CUENTA => 220103,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPOSITOS PARA SEGUROS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 377
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 390,
                CatalogoCuentasModel::NO_CUENTA => 220104,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPOSITO DE  CLIENTES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 377
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 391,
                CatalogoCuentasModel::NO_CUENTA => 220200,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPOSITO DE SEGUROS U. NUEVAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 377
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 392,
                CatalogoCuentasModel::NO_CUENTA => 221000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES VENTAS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 393,
                CatalogoCuentasModel::NO_CUENTA => 230000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTRAS CUENTAS POR PAGAR",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 394,
                CatalogoCuentasModel::NO_CUENTA => 230001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DOCUMENTOS POR PAGAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 393
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 395,
                CatalogoCuentasModel::NO_CUENTA => 230002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INTERESES POR PAGAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 393
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 396,
                CatalogoCuentasModel::NO_CUENTA => 230003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTRAS CUENTAS POR PAGAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 393
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 397,
                CatalogoCuentasModel::NO_CUENTA => 231000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RESERVA P\/SUBSIDIO DE SEGUROS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 398,
                CatalogoCuentasModel::NO_CUENTA => 240000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMPUESTOS POR PAGAR",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 399,
                CatalogoCuentasModel::NO_CUENTA => 240001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AMORTIZACIONES INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 398
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 400,
                CatalogoCuentasModel::NO_CUENTA => 240002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMPUESTO ESTATAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 398
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 401,
                CatalogoCuentasModel::NO_CUENTA => 240003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMSS PATRONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 398
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 402,
                CatalogoCuentasModel::NO_CUENTA => 240004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMSS RETENIDO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 398
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 403,
                CatalogoCuentasModel::NO_CUENTA => 240005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 398
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 404,
                CatalogoCuentasModel::NO_CUENTA => 240006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ISAN POR PAGAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 398
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 405,
                CatalogoCuentasModel::NO_CUENTA => 240007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ISR DE SUELDOS Y SALARIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 398
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 406,
                CatalogoCuentasModel::NO_CUENTA => 240008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ISR PROVISIONAL Y ANUAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 398
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 407,
                CatalogoCuentasModel::NO_CUENTA => 240009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ISR RET. ARRENDAMIENTO P. FISICAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 398
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 408,
                CatalogoCuentasModel::NO_CUENTA => 240010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ISR RET. HONORARIOS P. FISICAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 398
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 409,
                CatalogoCuentasModel::NO_CUENTA => 240011,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ISR RETENIDO (HAS)",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 398
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 410,
                CatalogoCuentasModel::NO_CUENTA => 240012,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IVA COBRADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 398
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 411,
                CatalogoCuentasModel::NO_CUENTA => 240013,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IVA FACTURADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 398
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 412,
                CatalogoCuentasModel::NO_CUENTA => 240014,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IVA RET. ARRENDAMIENTO P. FISICAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 398
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 413,
                CatalogoCuentasModel::NO_CUENTA => 240015,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECARGOS IETU POR PAGAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 398
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 414,
                CatalogoCuentasModel::NO_CUENTA => 240016,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IVA RET. HONORARIOS P. FISICAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 398
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 415,
                CatalogoCuentasModel::NO_CUENTA => 240017,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IVA RETENIDO DEL 4%",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 398
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 416,
                CatalogoCuentasModel::NO_CUENTA => 240018,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 398
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 417,
                CatalogoCuentasModel::NO_CUENTA => 240019,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IETU",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 398
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 418,
                CatalogoCuentasModel::NO_CUENTA => 250000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPOSITO DE CLIENTES",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 419,
                CatalogoCuentasModel::NO_CUENTA => 250001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEP. CLIENTES UNIDADES QRO.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 418
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 420,
                CatalogoCuentasModel::NO_CUENTA => 250002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPOSITOS DE CLIENTES NUEVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 418
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 421,
                CatalogoCuentasModel::NO_CUENTA => 250003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEP. DE CLIENTES SERVICIO QRO.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 418
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 422,
                CatalogoCuentasModel::NO_CUENTA => 250004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEP. DE CLIENTES SERVICIO S.J.R.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 418
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 423,
                CatalogoCuentasModel::NO_CUENTA => 250005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPOSITO DE CLIENTES USADOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 418
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 424,
                CatalogoCuentasModel::NO_CUENTA => 250006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEP. DE CLIENTES REFACCIONES S.J.R.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 418
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 425,
                CatalogoCuentasModel::NO_CUENTA => 250007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPOSITO PARA SEGUROS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 418
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 426,
                CatalogoCuentasModel::NO_CUENTA => 250008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPOSITOS RENTA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 418
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 427,
                CatalogoCuentasModel::NO_CUENTA => 251000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RESERVA LEGAL",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 428,
                CatalogoCuentasModel::NO_CUENTA => 251001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REEXPRESION DE LA RESERVA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 427
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 429,
                CatalogoCuentasModel::NO_CUENTA => 252000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RESULTADO DEL A¥O",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 430,
                CatalogoCuentasModel::NO_CUENTA => 252001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RESULTADO ACUMULADOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 429
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 431,
                CatalogoCuentasModel::NO_CUENTA => 252002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REEXPRESION DE RESULTADOS ACUM.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 429
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 432,
                CatalogoCuentasModel::NO_CUENTA => 252003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PERDIDA DEL EJERCICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 429
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 433,
                CatalogoCuentasModel::NO_CUENTA => 253000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RESULTADO POR TENENCIA ACT. NO MONETARIOS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 434,
                CatalogoCuentasModel::NO_CUENTA => 254000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXCESO O (INSUFICIENCIA) EN LA REEXPRESION",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 435,
                CatalogoCuentasModel::NO_CUENTA => 255000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ISR DIFERIDO",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 436,
                CatalogoCuentasModel::NO_CUENTA => 260000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUELDOS POR PAGAR",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 437,
                CatalogoCuentasModel::NO_CUENTA => 260001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES Y SUELDOS POR PAGAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 436
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 438,
                CatalogoCuentasModel::NO_CUENTA => 270000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RESERVAS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 439,
                CatalogoCuentasModel::NO_CUENTA => 270001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RESERVA PARA GRATIFICACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 438
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 440,
                CatalogoCuentasModel::NO_CUENTA => 270002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RESERVA PARA PTU",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 438
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 441,
                CatalogoCuentasModel::NO_CUENTA => 270003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RESERVA ISR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 438
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 442,
                CatalogoCuentasModel::NO_CUENTA => 270004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RESERVA IETU",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 438
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 443,
                CatalogoCuentasModel::NO_CUENTA => 280000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAPITAL CONTABLE",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 444,
                CatalogoCuentasModel::NO_CUENTA => 280001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAPITAL SOCIAL FIJO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 443
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 445,
                CatalogoCuentasModel::NO_CUENTA => 280002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAPITAL SOCIAL VARIABLE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 443
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 446,
                CatalogoCuentasModel::NO_CUENTA => 280003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REEXPRESION DEL CAPITAL SOCIAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 443
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 447,
                CatalogoCuentasModel::NO_CUENTA => 280004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "APORT. PARA FUTUROS AUMENTOS DE CAPITAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 443
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 448,
                CatalogoCuentasModel::NO_CUENTA => 280005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RESERVA LEGAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 443
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 449,
                CatalogoCuentasModel::NO_CUENTA => 280006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA EN SUSCRIPCION DE ACCIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 443
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 450,
                CatalogoCuentasModel::NO_CUENTA => 280007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RESULTADOS DEL AÑO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 443
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 451,
                CatalogoCuentasModel::NO_CUENTA => 280008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RESULTADOS ACUMULADOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 443
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 452,
                CatalogoCuentasModel::NO_CUENTA => 280009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REEXPRESION DE RESULTADOS ACUMULADOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 443
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 453,
                CatalogoCuentasModel::NO_CUENTA => 280010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PERDIDA DEL EJERCICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 443
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 454,
                CatalogoCuentasModel::NO_CUENTA => 280011,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RESULTADO POR TENECIA ACT. NO MONETARIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 443
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 455,
                CatalogoCuentasModel::NO_CUENTA => 280012,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXCESO O INSUFICIENCIA EN LA REEXPRESION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 443
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 456,
                CatalogoCuentasModel::NO_CUENTA => 280013,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ISR DIFERIDO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 443
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 457,
                CatalogoCuentasModel::NO_CUENTA => 300000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VENTA",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 458,
                CatalogoCuentasModel::NO_CUENTA => 300001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SABLE-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 459,
                CatalogoCuentasModel::NO_CUENTA => 300002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SABLE-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 460,
                CatalogoCuentasModel::NO_CUENTA => 300003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SABLE-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 461,
                CatalogoCuentasModel::NO_CUENTA => 300004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MUSTANG-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 462,
                CatalogoCuentasModel::NO_CUENTA => 300005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MUSTANG-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 463,
                CatalogoCuentasModel::NO_CUENTA => 300006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MUSTANG-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 464,
                CatalogoCuentasModel::NO_CUENTA => 300007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOCUS-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 465,
                CatalogoCuentasModel::NO_CUENTA => 300008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOCUS-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 466,
                CatalogoCuentasModel::NO_CUENTA => 300009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOCUS-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 467,
                CatalogoCuentasModel::NO_CUENTA => 300010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IKON-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 468,
                CatalogoCuentasModel::NO_CUENTA => 300011,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IKON-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 469,
                CatalogoCuentasModel::NO_CUENTA => 300012,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IKON FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 470,
                CatalogoCuentasModel::NO_CUENTA => 300013,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MONDEO-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 471,
                CatalogoCuentasModel::NO_CUENTA => 300014,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MONDEO-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 472,
                CatalogoCuentasModel::NO_CUENTA => 300015,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MONDEO-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 473,
                CatalogoCuentasModel::NO_CUENTA => 300016,
                CatalogoCuentasModel::NOMBRE_CUENTA => "KA-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 474,
                CatalogoCuentasModel::NO_CUENTA => 300017,
                CatalogoCuentasModel::NOMBRE_CUENTA => "KA-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 475,
                CatalogoCuentasModel::NO_CUENTA => 300018,
                CatalogoCuentasModel::NOMBRE_CUENTA => "KA-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 476,
                CatalogoCuentasModel::NO_CUENTA => 300019,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIESTA-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 477,
                CatalogoCuentasModel::NO_CUENTA => 300020,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIESTA-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 478,
                CatalogoCuentasModel::NO_CUENTA => 300021,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIESTA FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 479,
                CatalogoCuentasModel::NO_CUENTA => 300022,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRAN MARQUIS-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 480,
                CatalogoCuentasModel::NO_CUENTA => 300023,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRAN MARQUIS-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 481,
                CatalogoCuentasModel::NO_CUENTA => 300024,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRAN MARQUIS-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 482,
                CatalogoCuentasModel::NO_CUENTA => 300025,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRAN MARQUIS-INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 483,
                CatalogoCuentasModel::NO_CUENTA => 300026,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SABLE-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 484,
                CatalogoCuentasModel::NO_CUENTA => 300027,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MUSTANG-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 485,
                CatalogoCuentasModel::NO_CUENTA => 300028,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOCUS-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 486,
                CatalogoCuentasModel::NO_CUENTA => 300029,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IKON-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 487,
                CatalogoCuentasModel::NO_CUENTA => 300030,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MONDEO-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 488,
                CatalogoCuentasModel::NO_CUENTA => 300031,
                CatalogoCuentasModel::NOMBRE_CUENTA => "KA-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 489,
                CatalogoCuentasModel::NO_CUENTA => 300032,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIESTA-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 490,
                CatalogoCuentasModel::NO_CUENTA => 300033,
                CatalogoCuentasModel::NOMBRE_CUENTA => "THUNDERBIRD-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 491,
                CatalogoCuentasModel::NO_CUENTA => 300034,
                CatalogoCuentasModel::NOMBRE_CUENTA => "THUNDERBIRD-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 492,
                CatalogoCuentasModel::NO_CUENTA => 300035,
                CatalogoCuentasModel::NOMBRE_CUENTA => "THUNDERBIRD-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 493,
                CatalogoCuentasModel::NO_CUENTA => 300036,
                CatalogoCuentasModel::NOMBRE_CUENTA => "THUNDERBIRD-INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 494,
                CatalogoCuentasModel::NO_CUENTA => 300037,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPORT GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 495,
                CatalogoCuentasModel::NO_CUENTA => 300038,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPORT CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 496,
                CatalogoCuentasModel::NO_CUENTA => 300039,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPORT FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 497,
                CatalogoCuentasModel::NO_CUENTA => 300040,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPORT INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 498,
                CatalogoCuentasModel::NO_CUENTA => 300041,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPORT P\/LLENO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 499,
                CatalogoCuentasModel::NO_CUENTA => 300042,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIVE HUNDRED GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 500,
                CatalogoCuentasModel::NO_CUENTA => 300043,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIVE HUNDRED FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 501,
                CatalogoCuentasModel::NO_CUENTA => 300044,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIVE HUNDRED CANAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 502,
                CatalogoCuentasModel::NO_CUENTA => 300045,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIVE HUNDRED INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 503,
                CatalogoCuentasModel::NO_CUENTA => 300046,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FUSION GENERAL QRO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 504,
                CatalogoCuentasModel::NO_CUENTA => 300047,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FUSION CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 505,
                CatalogoCuentasModel::NO_CUENTA => 300048,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FUSION FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 506,
                CatalogoCuentasModel::NO_CUENTA => 300049,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FUSION INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 507,
                CatalogoCuentasModel::NO_CUENTA => 300050,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EDGE GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 508,
                CatalogoCuentasModel::NO_CUENTA => 300051,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EDGE CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 509,
                CatalogoCuentasModel::NO_CUENTA => 300052,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EDGE FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 510,
                CatalogoCuentasModel::NO_CUENTA => 300053,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EDGE INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 511,
                CatalogoCuentasModel::NO_CUENTA => 300100,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VENTAS UNIDADES SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 512,
                CatalogoCuentasModel::NO_CUENTA => 300101,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SABLE-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 513,
                CatalogoCuentasModel::NO_CUENTA => 300102,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SABLE-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 514,
                CatalogoCuentasModel::NO_CUENTA => 300103,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SABLE-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 515,
                CatalogoCuentasModel::NO_CUENTA => 300104,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MUSTANG-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 516,
                CatalogoCuentasModel::NO_CUENTA => 300105,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MUSTANG-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 517,
                CatalogoCuentasModel::NO_CUENTA => 300106,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MUSTANG-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 518,
                CatalogoCuentasModel::NO_CUENTA => 300107,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOCUS-GEJNERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 519,
                CatalogoCuentasModel::NO_CUENTA => 300108,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOCUS-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 520,
                CatalogoCuentasModel::NO_CUENTA => 300109,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOCUS-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 521,
                CatalogoCuentasModel::NO_CUENTA => 300110,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IKON-GENARAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 522,
                CatalogoCuentasModel::NO_CUENTA => 300111,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IKON-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 523,
                CatalogoCuentasModel::NO_CUENTA => 300112,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IKON-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 524,
                CatalogoCuentasModel::NO_CUENTA => 300113,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MONDEO-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 525,
                CatalogoCuentasModel::NO_CUENTA => 300114,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MONDEO-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 526,
                CatalogoCuentasModel::NO_CUENTA => 300115,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MONDEO-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 527,
                CatalogoCuentasModel::NO_CUENTA => 300116,
                CatalogoCuentasModel::NOMBRE_CUENTA => "KA-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 528,
                CatalogoCuentasModel::NO_CUENTA => 300117,
                CatalogoCuentasModel::NOMBRE_CUENTA => "KA-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 529,
                CatalogoCuentasModel::NO_CUENTA => 300118,
                CatalogoCuentasModel::NOMBRE_CUENTA => "KA-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 530,
                CatalogoCuentasModel::NO_CUENTA => 300119,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIESTA-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 531,
                CatalogoCuentasModel::NO_CUENTA => 300120,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIESTA-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 532,
                CatalogoCuentasModel::NO_CUENTA => 300121,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIESTA-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 533,
                CatalogoCuentasModel::NO_CUENTA => 300133,
                CatalogoCuentasModel::NOMBRE_CUENTA => "THUNDERBIRD-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 534,
                CatalogoCuentasModel::NO_CUENTA => 300134,
                CatalogoCuentasModel::NOMBRE_CUENTA => "THUNDERBIRD-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 535,
                CatalogoCuentasModel::NO_CUENTA => 300135,
                CatalogoCuentasModel::NOMBRE_CUENTA => "THUNDERBIRD-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 536,
                CatalogoCuentasModel::NO_CUENTA => 300136,
                CatalogoCuentasModel::NOMBRE_CUENTA => "THUNDERBIRD-INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 537,
                CatalogoCuentasModel::NO_CUENTA => 300201,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MUSTANG-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 538,
                CatalogoCuentasModel::NO_CUENTA => 300202,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MUSTANG-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 539,
                CatalogoCuentasModel::NO_CUENTA => 300203,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MUSTANG-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 540,
                CatalogoCuentasModel::NO_CUENTA => 300204,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MUSTANG-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 541,
                CatalogoCuentasModel::NO_CUENTA => 300301,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOCUS-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 542,
                CatalogoCuentasModel::NO_CUENTA => 300302,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOCUS-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 543,
                CatalogoCuentasModel::NO_CUENTA => 300303,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOCUS-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 544,
                CatalogoCuentasModel::NO_CUENTA => 300304,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOCUS-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 545,
                CatalogoCuentasModel::NO_CUENTA => 300401,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ESCAPE-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 546,
                CatalogoCuentasModel::NO_CUENTA => 300402,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ESCAPE-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 547,
                CatalogoCuentasModel::NO_CUENTA => 300403,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ESCAPE-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 548,
                CatalogoCuentasModel::NO_CUENTA => 300404,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ESCAPE-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 549,
                CatalogoCuentasModel::NO_CUENTA => 300501,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IKON-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 550,
                CatalogoCuentasModel::NO_CUENTA => 300502,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IKON-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 551,
                CatalogoCuentasModel::NO_CUENTA => 300503,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IKON-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 552,
                CatalogoCuentasModel::NO_CUENTA => 300504,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IKON-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 553,
                CatalogoCuentasModel::NO_CUENTA => 300601,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MONDEO-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 554,
                CatalogoCuentasModel::NO_CUENTA => 300602,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MODEO-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 555,
                CatalogoCuentasModel::NO_CUENTA => 300603,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MODEO-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 556,
                CatalogoCuentasModel::NO_CUENTA => 300604,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MONDEO-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 557,
                CatalogoCuentasModel::NO_CUENTA => 300701,
                CatalogoCuentasModel::NOMBRE_CUENTA => "KA-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 558,
                CatalogoCuentasModel::NO_CUENTA => 300702,
                CatalogoCuentasModel::NOMBRE_CUENTA => "KA-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 559,
                CatalogoCuentasModel::NO_CUENTA => 300703,
                CatalogoCuentasModel::NOMBRE_CUENTA => "KA-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 560,
                CatalogoCuentasModel::NO_CUENTA => 300704,
                CatalogoCuentasModel::NOMBRE_CUENTA => "KA-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 561,
                CatalogoCuentasModel::NO_CUENTA => 300801,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIESTA-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 562,
                CatalogoCuentasModel::NO_CUENTA => 300802,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIESTA CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 563,
                CatalogoCuentasModel::NO_CUENTA => 300803,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIESTA FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 564,
                CatalogoCuentasModel::NO_CUENTA => 300804,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIESTA INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 565,
                CatalogoCuentasModel::NO_CUENTA => 300901,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRAN MARQUIZ GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 566,
                CatalogoCuentasModel::NO_CUENTA => 300902,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRAN MARQUIZ CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 567,
                CatalogoCuentasModel::NO_CUENTA => 300903,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRAN MARQUIZ FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 568,
                CatalogoCuentasModel::NO_CUENTA => 300904,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRAN MARQUIZ INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 457
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 569,
                CatalogoCuentasModel::NO_CUENTA => 301000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VENTAS SJR",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 570,
                CatalogoCuentasModel::NO_CUENTA => 301001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOCUS GENERAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 571,
                CatalogoCuentasModel::NO_CUENTA => 301002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOCUS CONAUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 572,
                CatalogoCuentasModel::NO_CUENTA => 301003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOCUS FLOTILLA SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 573,
                CatalogoCuentasModel::NO_CUENTA => 301004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOCUS INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 574,
                CatalogoCuentasModel::NO_CUENTA => 301005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SABLE GENERAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 575,
                CatalogoCuentasModel::NO_CUENTA => 301006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SABLE CONAUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 576,
                CatalogoCuentasModel::NO_CUENTA => 301007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SABLE FLOTILLA SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 577,
                CatalogoCuentasModel::NO_CUENTA => 301008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SABLE INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 578,
                CatalogoCuentasModel::NO_CUENTA => 301009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MONDEOGENERAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 579,
                CatalogoCuentasModel::NO_CUENTA => 301010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MONDEOCONAUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 580,
                CatalogoCuentasModel::NO_CUENTA => 301011,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MONDEOFLOTILLA SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 581,
                CatalogoCuentasModel::NO_CUENTA => 301012,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MONDEOCONAUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 582,
                CatalogoCuentasModel::NO_CUENTA => 301013,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRAN M GENERAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 583,
                CatalogoCuentasModel::NO_CUENTA => 301014,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRAN M CONAUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 584,
                CatalogoCuentasModel::NO_CUENTA => 301015,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRAN M FLOTILLA SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 585,
                CatalogoCuentasModel::NO_CUENTA => 301016,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRAN M INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 586,
                CatalogoCuentasModel::NO_CUENTA => 301017,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MUSTANG GENERAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 587,
                CatalogoCuentasModel::NO_CUENTA => 301018,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MUSTANG CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 588,
                CatalogoCuentasModel::NO_CUENTA => 301019,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MUSTANG FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 589,
                CatalogoCuentasModel::NO_CUENTA => 301020,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MUSTANG INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 590,
                CatalogoCuentasModel::NO_CUENTA => 301021,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IKON GENERAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 591,
                CatalogoCuentasModel::NO_CUENTA => 301022,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IKON CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 592,
                CatalogoCuentasModel::NO_CUENTA => 301023,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IKON FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 593,
                CatalogoCuentasModel::NO_CUENTA => 301024,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IKON INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 594,
                CatalogoCuentasModel::NO_CUENTA => 301025,
                CatalogoCuentasModel::NOMBRE_CUENTA => "KA GENENAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 595,
                CatalogoCuentasModel::NO_CUENTA => 301026,
                CatalogoCuentasModel::NOMBRE_CUENTA => "KA CONAUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 596,
                CatalogoCuentasModel::NO_CUENTA => 301027,
                CatalogoCuentasModel::NOMBRE_CUENTA => "KA FLOTILLA SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 597,
                CatalogoCuentasModel::NO_CUENTA => 301028,
                CatalogoCuentasModel::NOMBRE_CUENTA => "KA INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 598,
                CatalogoCuentasModel::NO_CUENTA => 301029,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIESTA GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 599,
                CatalogoCuentasModel::NO_CUENTA => 301030,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIESTA CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 600,
                CatalogoCuentasModel::NO_CUENTA => 301031,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIESTA FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 601,
                CatalogoCuentasModel::NO_CUENTA => 301032,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIESTA INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 602,
                CatalogoCuentasModel::NO_CUENTA => 301033,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPLORER GENERAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 603,
                CatalogoCuentasModel::NO_CUENTA => 301034,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPLORER CONAUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 604,
                CatalogoCuentasModel::NO_CUENTA => 301035,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPLORER FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 605,
                CatalogoCuentasModel::NO_CUENTA => 301036,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPLORER INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 606,
                CatalogoCuentasModel::NO_CUENTA => 301037,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FREESTAR GENERAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 607,
                CatalogoCuentasModel::NO_CUENTA => 301038,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FREESTAR CONAUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 608,
                CatalogoCuentasModel::NO_CUENTA => 301039,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FREESTAR FLOTILLA SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 609,
                CatalogoCuentasModel::NO_CUENTA => 301040,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FREESTAR INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 610,
                CatalogoCuentasModel::NO_CUENTA => 301041,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECONOLINE GENERAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 611,
                CatalogoCuentasModel::NO_CUENTA => 301042,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECONOLINE CONAUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 612,
                CatalogoCuentasModel::NO_CUENTA => 301043,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECONOLINE FLOTILLA SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 613,
                CatalogoCuentasModel::NO_CUENTA => 301044,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECONOLINE INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 614,
                CatalogoCuentasModel::NO_CUENTA => 301045,
                CatalogoCuentasModel::NOMBRE_CUENTA => "THUNDERBIRD GENERAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 615,
                CatalogoCuentasModel::NO_CUENTA => 301046,
                CatalogoCuentasModel::NOMBRE_CUENTA => "THUNDERBIRD CONAUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 616,
                CatalogoCuentasModel::NO_CUENTA => 301047,
                CatalogoCuentasModel::NOMBRE_CUENTA => "THUNDERBIRD FLOTILLA SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 617,
                CatalogoCuentasModel::NO_CUENTA => 301048,
                CatalogoCuentasModel::NOMBRE_CUENTA => "THUNDERBIRD INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 618,
                CatalogoCuentasModel::NO_CUENTA => 301049,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPORT GENERAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 619,
                CatalogoCuentasModel::NO_CUENTA => 301050,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPOR CONAUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 620,
                CatalogoCuentasModel::NO_CUENTA => 301051,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPOR FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 621,
                CatalogoCuentasModel::NO_CUENTA => 301052,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPOR INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 622,
                CatalogoCuentasModel::NO_CUENTA => 301053,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIVE HUNDRED GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 623,
                CatalogoCuentasModel::NO_CUENTA => 301054,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIVE HUNDRED CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 624,
                CatalogoCuentasModel::NO_CUENTA => 301055,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIVE HUNDRED FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 625,
                CatalogoCuentasModel::NO_CUENTA => 301056,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIVE HUNDRED INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 626,
                CatalogoCuentasModel::NO_CUENTA => 301057,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ESCAPE GENERAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 627,
                CatalogoCuentasModel::NO_CUENTA => 301058,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ESCAPE CONAUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 628,
                CatalogoCuentasModel::NO_CUENTA => 301059,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ESCAPE FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 629,
                CatalogoCuentasModel::NO_CUENTA => 301060,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ESCAPE INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 630,
                CatalogoCuentasModel::NO_CUENTA => 301061,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPEDITION GENENRAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 631,
                CatalogoCuentasModel::NO_CUENTA => 301062,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPEDITION CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 632,
                CatalogoCuentasModel::NO_CUENTA => 301063,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPEDITION FLOTILLA SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 633,
                CatalogoCuentasModel::NO_CUENTA => 301064,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPEDITION INTECAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 634,
                CatalogoCuentasModel::NO_CUENTA => 301065,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXCURSION GENERAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 635,
                CatalogoCuentasModel::NO_CUENTA => 301066,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXCURSION COANUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 636,
                CatalogoCuentasModel::NO_CUENTA => 301067,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXCURSION FLOTILLA SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 637,
                CatalogoCuentasModel::NO_CUENTA => 301068,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXCURSION INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 638,
                CatalogoCuentasModel::NO_CUENTA => 301069,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FUSION GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 639,
                CatalogoCuentasModel::NO_CUENTA => 301070,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FUSION CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 640,
                CatalogoCuentasModel::NO_CUENTA => 301071,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FUSION FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 641,
                CatalogoCuentasModel::NO_CUENTA => 301072,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FUSION INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 642,
                CatalogoCuentasModel::NO_CUENTA => 301301,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-250 GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 569
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 643,
                CatalogoCuentasModel::NO_CUENTA => 302000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMIONES LIGEROS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 644,
                CatalogoCuentasModel::NO_CUENTA => 302101,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-150 GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 643
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 645,
                CatalogoCuentasModel::NO_CUENTA => 302102,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-150 CONAUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 643
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 646,
                CatalogoCuentasModel::NO_CUENTA => 302103,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-150 FLOTILLA SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 643
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 647,
                CatalogoCuentasModel::NO_CUENTA => 302104,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-150 INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 643
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 648,
                CatalogoCuentasModel::NO_CUENTA => 302105,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-250 GENERAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 643
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 649,
                CatalogoCuentasModel::NO_CUENTA => 302106,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-250 CONAUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 643
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 650,
                CatalogoCuentasModel::NO_CUENTA => 302107,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-250 FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 643
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 651,
                CatalogoCuentasModel::NO_CUENTA => 302108,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-250 INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 643
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 652,
                CatalogoCuentasModel::NO_CUENTA => 302109,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RANGER GENERAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 643
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 653,
                CatalogoCuentasModel::NO_CUENTA => 302110,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RANGER CONAUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 643
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 654,
                CatalogoCuentasModel::NO_CUENTA => 302111,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RANGER FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 643
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 655,
                CatalogoCuentasModel::NO_CUENTA => 302112,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RANGER INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 643
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 656,
                CatalogoCuentasModel::NO_CUENTA => 302113,
                CatalogoCuentasModel::NOMBRE_CUENTA => "LOBO GENERAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 643
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 657,
                CatalogoCuentasModel::NO_CUENTA => 302114,
                CatalogoCuentasModel::NOMBRE_CUENTA => "LOBO CONAUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 643
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 658,
                CatalogoCuentasModel::NO_CUENTA => 302115,
                CatalogoCuentasModel::NOMBRE_CUENTA => "LOBO FLOTILLA SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 643
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 659,
                CatalogoCuentasModel::NO_CUENTA => 302116,
                CatalogoCuentasModel::NOMBRE_CUENTA => "LOBO INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 643
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 660,
                CatalogoCuentasModel::NO_CUENTA => 302117,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COURIER GENERAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 643
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 661,
                CatalogoCuentasModel::NO_CUENTA => 302118,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COURIER CONAUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 643
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 662,
                CatalogoCuentasModel::NO_CUENTA => 302119,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COURIER FLOTILLA SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 643
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 663,
                CatalogoCuentasModel::NO_CUENTA => 302120,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COURIER INT, SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 643
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 664,
                CatalogoCuentasModel::NO_CUENTA => 302126,
                CatalogoCuentasModel::NOMBRE_CUENTA => "LOBO INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 643
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 665,
                CatalogoCuentasModel::NO_CUENTA => 302201,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-350 GENERAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 643
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 666,
                CatalogoCuentasModel::NO_CUENTA => 302202,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-350 CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 643
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 667,
                CatalogoCuentasModel::NO_CUENTA => 302203,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-350 FLOTILLA SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 643
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 668,
                CatalogoCuentasModel::NO_CUENTA => 302204,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-350 INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 643
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 669,
                CatalogoCuentasModel::NO_CUENTA => 302205,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F450 GENERAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 643
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 670,
                CatalogoCuentasModel::NO_CUENTA => 302206,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-450 CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 643
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 671,
                CatalogoCuentasModel::NO_CUENTA => 302207,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-450 FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 643
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 672,
                CatalogoCuentasModel::NO_CUENTA => 302208,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-450 INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 643
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 673,
                CatalogoCuentasModel::NO_CUENTA => 302209,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-550 GENERAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 643
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 674,
                CatalogoCuentasModel::NO_CUENTA => 302210,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-550 CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 643
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 675,
                CatalogoCuentasModel::NO_CUENTA => 302211,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-550 FLOTILLA SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 643
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 676,
                CatalogoCuentasModel::NO_CUENTA => 302212,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-550 INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 643
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 677,
                CatalogoCuentasModel::NO_CUENTA => 310000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VENTAS UNIDADES NUEVAS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 678,
                CatalogoCuentasModel::NO_CUENTA => 310001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BRONCO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 679,
                CatalogoCuentasModel::NO_CUENTA => 310002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECONOLINE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 680,
                CatalogoCuentasModel::NO_CUENTA => 310003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPORT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 681,
                CatalogoCuentasModel::NO_CUENTA => 310004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EDGE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 682,
                CatalogoCuentasModel::NO_CUENTA => 310005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ESCAPE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 683,
                CatalogoCuentasModel::NO_CUENTA => 310006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPEDITION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 684,
                CatalogoCuentasModel::NO_CUENTA => 310007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPLORER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 685,
                CatalogoCuentasModel::NO_CUENTA => 310008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-150",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 686,
                CatalogoCuentasModel::NO_CUENTA => 310009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-250",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 687,
                CatalogoCuentasModel::NO_CUENTA => 310010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-350",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 688,
                CatalogoCuentasModel::NO_CUENTA => 310011,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-450",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 689,
                CatalogoCuentasModel::NO_CUENTA => 310012,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-550",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 690,
                CatalogoCuentasModel::NO_CUENTA => 310013,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIESTA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 691,
                CatalogoCuentasModel::NO_CUENTA => 310014,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOCUS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 692,
                CatalogoCuentasModel::NO_CUENTA => 310015,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FUSION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 693,
                CatalogoCuentasModel::NO_CUENTA => 310016,
                CatalogoCuentasModel::NOMBRE_CUENTA => "LOBO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 694,
                CatalogoCuentasModel::NO_CUENTA => 310017,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MUSTANG",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 695,
                CatalogoCuentasModel::NO_CUENTA => 310018,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RANGER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 696,
                CatalogoCuentasModel::NO_CUENTA => 310019,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SPORT TRAC",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 697,
                CatalogoCuentasModel::NO_CUENTA => 310020,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TRANSIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 698,
                CatalogoCuentasModel::NO_CUENTA => 310021,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIGO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 699,
                CatalogoCuentasModel::NO_CUENTA => 310022,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. RANGER-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 700,
                CatalogoCuentasModel::NO_CUENTA => 310023,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. RANGER-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 701,
                CatalogoCuentasModel::NO_CUENTA => 310024,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. RANGER-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 702,
                CatalogoCuentasModel::NO_CUENTA => 310025,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. F350-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 703,
                CatalogoCuentasModel::NO_CUENTA => 310026,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. F350-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 704,
                CatalogoCuentasModel::NO_CUENTA => 310027,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. F350-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 705,
                CatalogoCuentasModel::NO_CUENTA => 310028,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. LOBO-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 706,
                CatalogoCuentasModel::NO_CUENTA => 310029,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. LOBO-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 707,
                CatalogoCuentasModel::NO_CUENTA => 310030,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. LOBO-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 708,
                CatalogoCuentasModel::NO_CUENTA => 310031,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. COURIER-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 709,
                CatalogoCuentasModel::NO_CUENTA => 310032,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. COURIER-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 710,
                CatalogoCuentasModel::NO_CUENTA => 310033,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. COURIER-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 711,
                CatalogoCuentasModel::NO_CUENTA => 310034,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXCURSION-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 712,
                CatalogoCuentasModel::NO_CUENTA => 310035,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXCURSION-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 713,
                CatalogoCuentasModel::NO_CUENTA => 310036,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXCURSION-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 714,
                CatalogoCuentasModel::NO_CUENTA => 310037,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPEDITION-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 715,
                CatalogoCuentasModel::NO_CUENTA => 310038,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPEDITION-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 716,
                CatalogoCuentasModel::NO_CUENTA => 310039,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPEDITION-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 717,
                CatalogoCuentasModel::NO_CUENTA => 310040,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ESCAPE-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 718,
                CatalogoCuentasModel::NO_CUENTA => 310041,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPLORER-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 719,
                CatalogoCuentasModel::NO_CUENTA => 310042,
                CatalogoCuentasModel::NOMBRE_CUENTA => "WINDSTAR-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 720,
                CatalogoCuentasModel::NO_CUENTA => 310043,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TRANSIT-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 721,
                CatalogoCuentasModel::NO_CUENTA => 310044,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECONOLINE-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 722,
                CatalogoCuentasModel::NO_CUENTA => 310045,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. F150-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 723,
                CatalogoCuentasModel::NO_CUENTA => 310046,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. F250-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 724,
                CatalogoCuentasModel::NO_CUENTA => 310047,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. RANGER-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 725,
                CatalogoCuentasModel::NO_CUENTA => 310048,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. F350-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 726,
                CatalogoCuentasModel::NO_CUENTA => 310049,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. LOBO-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 727,
                CatalogoCuentasModel::NO_CUENTA => 310050,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. COURIER-INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 728,
                CatalogoCuentasModel::NO_CUENTA => 310051,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXCURSION-INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 729,
                CatalogoCuentasModel::NO_CUENTA => 310052,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPEDITION-INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 730,
                CatalogoCuentasModel::NO_CUENTA => 310053,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPORT GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 731,
                CatalogoCuentasModel::NO_CUENTA => 310054,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPORT CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 732,
                CatalogoCuentasModel::NO_CUENTA => 310055,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPORT FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 733,
                CatalogoCuentasModel::NO_CUENTA => 310056,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPORT INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 734,
                CatalogoCuentasModel::NO_CUENTA => 310057,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPORT P\/LLENO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 735,
                CatalogoCuentasModel::NO_CUENTA => 310100,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VENTA DE CAMIONES SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 736,
                CatalogoCuentasModel::NO_CUENTA => 310101,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ESCAPE-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 737,
                CatalogoCuentasModel::NO_CUENTA => 310102,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ESCAPE-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 738,
                CatalogoCuentasModel::NO_CUENTA => 310103,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ESCAPE-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 739,
                CatalogoCuentasModel::NO_CUENTA => 310104,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPLORER-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 740,
                CatalogoCuentasModel::NO_CUENTA => 310105,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPLORER-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 741,
                CatalogoCuentasModel::NO_CUENTA => 310106,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ESPLORER-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 742,
                CatalogoCuentasModel::NO_CUENTA => 310107,
                CatalogoCuentasModel::NOMBRE_CUENTA => "WINDSTAR-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 743,
                CatalogoCuentasModel::NO_CUENTA => 310108,
                CatalogoCuentasModel::NOMBRE_CUENTA => "WINDSTAR-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 744,
                CatalogoCuentasModel::NO_CUENTA => 310109,
                CatalogoCuentasModel::NOMBRE_CUENTA => "WINDSTAR-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 745,
                CatalogoCuentasModel::NO_CUENTA => 310110,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CLUB WAGON-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 746,
                CatalogoCuentasModel::NO_CUENTA => 310111,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CLUB WAGON-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 747,
                CatalogoCuentasModel::NO_CUENTA => 310112,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CLUB WAGON-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 748,
                CatalogoCuentasModel::NO_CUENTA => 310113,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECONOLINE-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 749,
                CatalogoCuentasModel::NO_CUENTA => 310114,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECONOLINE-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 750,
                CatalogoCuentasModel::NO_CUENTA => 310115,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECONOLINE-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 751,
                CatalogoCuentasModel::NO_CUENTA => 310116,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. F150-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 752,
                CatalogoCuentasModel::NO_CUENTA => 310117,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. F150-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 753,
                CatalogoCuentasModel::NO_CUENTA => 310118,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. F150-FLOTOLLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 754,
                CatalogoCuentasModel::NO_CUENTA => 310119,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. F250-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 755,
                CatalogoCuentasModel::NO_CUENTA => 310120,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. F250-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 756,
                CatalogoCuentasModel::NO_CUENTA => 310121,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. F250-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 757,
                CatalogoCuentasModel::NO_CUENTA => 310122,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. RANGER-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 758,
                CatalogoCuentasModel::NO_CUENTA => 310123,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. RANGER-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 759,
                CatalogoCuentasModel::NO_CUENTA => 310124,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. RANGER-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 760,
                CatalogoCuentasModel::NO_CUENTA => 310125,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. F350-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 761,
                CatalogoCuentasModel::NO_CUENTA => 310126,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. F350-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 762,
                CatalogoCuentasModel::NO_CUENTA => 310127,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. F350-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 763,
                CatalogoCuentasModel::NO_CUENTA => 310128,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. LOBO-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 764,
                CatalogoCuentasModel::NO_CUENTA => 310129,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. LOBO-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 765,
                CatalogoCuentasModel::NO_CUENTA => 310130,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. LOBO-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 766,
                CatalogoCuentasModel::NO_CUENTA => 310131,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. COURIER-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 767,
                CatalogoCuentasModel::NO_CUENTA => 310132,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. COURIER-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 768,
                CatalogoCuentasModel::NO_CUENTA => 310133,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. CUORIER-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 769,
                CatalogoCuentasModel::NO_CUENTA => 310134,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXCURSION-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 770,
                CatalogoCuentasModel::NO_CUENTA => 310135,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXCURSION-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 771,
                CatalogoCuentasModel::NO_CUENTA => 310136,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXCURSION-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 772,
                CatalogoCuentasModel::NO_CUENTA => 310137,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPEDITION-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 773,
                CatalogoCuentasModel::NO_CUENTA => 310138,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPEDITION-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 774,
                CatalogoCuentasModel::NO_CUENTA => 310139,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPEDITION-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 775,
                CatalogoCuentasModel::NO_CUENTA => 310153,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPORT GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 776,
                CatalogoCuentasModel::NO_CUENTA => 310154,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPORT CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 777,
                CatalogoCuentasModel::NO_CUENTA => 310155,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPORT FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 778,
                CatalogoCuentasModel::NO_CUENTA => 310156,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPORT INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 779,
                CatalogoCuentasModel::NO_CUENTA => 310157,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPORT P\/LLENO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 780,
                CatalogoCuentasModel::NO_CUENTA => 310201,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CLUB WAGON-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 781,
                CatalogoCuentasModel::NO_CUENTA => 310202,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CLUB WAGON-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 782,
                CatalogoCuentasModel::NO_CUENTA => 310203,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CLUB WAGON-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 783,
                CatalogoCuentasModel::NO_CUENTA => 310204,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CLUB WAGON-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 784,
                CatalogoCuentasModel::NO_CUENTA => 310301,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECONOLINE-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 785,
                CatalogoCuentasModel::NO_CUENTA => 310302,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECONOLINE-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 786,
                CatalogoCuentasModel::NO_CUENTA => 310303,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECONOLINE-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 787,
                CatalogoCuentasModel::NO_CUENTA => 310304,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECONOLINE-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 788,
                CatalogoCuentasModel::NO_CUENTA => 310401,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.F150-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 789,
                CatalogoCuentasModel::NO_CUENTA => 310402,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.F150-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 790,
                CatalogoCuentasModel::NO_CUENTA => 310403,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.F150-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 791,
                CatalogoCuentasModel::NO_CUENTA => 310404,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.F150-INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 792,
                CatalogoCuentasModel::NO_CUENTA => 310501,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.F250-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 793,
                CatalogoCuentasModel::NO_CUENTA => 310502,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.F250-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 794,
                CatalogoCuentasModel::NO_CUENTA => 310503,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.F250-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 795,
                CatalogoCuentasModel::NO_CUENTA => 310504,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.F250-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 796,
                CatalogoCuentasModel::NO_CUENTA => 310601,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.RANGER-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 797,
                CatalogoCuentasModel::NO_CUENTA => 310602,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.RANGER-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 798,
                CatalogoCuentasModel::NO_CUENTA => 310603,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.RANGER-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 799,
                CatalogoCuentasModel::NO_CUENTA => 310604,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.RANGER-INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 800,
                CatalogoCuentasModel::NO_CUENTA => 310701,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.F350-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 801,
                CatalogoCuentasModel::NO_CUENTA => 310702,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.F350-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 802,
                CatalogoCuentasModel::NO_CUENTA => 310703,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.F350-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 803,
                CatalogoCuentasModel::NO_CUENTA => 310704,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.F350-INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 804,
                CatalogoCuentasModel::NO_CUENTA => 310801,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.LOBO-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 805,
                CatalogoCuentasModel::NO_CUENTA => 310802,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.LOBO-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 806,
                CatalogoCuentasModel::NO_CUENTA => 310803,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.LOBO-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 807,
                CatalogoCuentasModel::NO_CUENTA => 310804,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.LOBO-INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 808,
                CatalogoCuentasModel::NO_CUENTA => 310901,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.COURIER-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 809,
                CatalogoCuentasModel::NO_CUENTA => 310902,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.COURIER-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 810,
                CatalogoCuentasModel::NO_CUENTA => 310903,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.COURIER-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 811,
                CatalogoCuentasModel::NO_CUENTA => 310904,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.COURIER-INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 677
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 812,
                CatalogoCuentasModel::NO_CUENTA => 311000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DESCUENTO SOBRE VENTAS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 813,
                CatalogoCuentasModel::NO_CUENTA => 311001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXCURSION-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 812
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 814,
                CatalogoCuentasModel::NO_CUENTA => 311002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXCURSION-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 812
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 815,
                CatalogoCuentasModel::NO_CUENTA => 311003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXCURSION-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 812
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 816,
                CatalogoCuentasModel::NO_CUENTA => 311004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXCURSION-INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 812
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 817,
                CatalogoCuentasModel::NO_CUENTA => 311101,
                CatalogoCuentasModel::NOMBRE_CUENTA => "THUNDERBIRD GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 812
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 818,
                CatalogoCuentasModel::NO_CUENTA => 311102,
                CatalogoCuentasModel::NOMBRE_CUENTA => "THUNDERBIRD CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 812
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 819,
                CatalogoCuentasModel::NO_CUENTA => 311103,
                CatalogoCuentasModel::NOMBRE_CUENTA => "THUNDERBIR FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 812
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 820,
                CatalogoCuentasModel::NO_CUENTA => 311104,
                CatalogoCuentasModel::NOMBRE_CUENTA => "THUNDERBIR INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 812
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 821,
                CatalogoCuentasModel::NO_CUENTA => 311201,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIVE HUNDRED GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 812
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 822,
                CatalogoCuentasModel::NO_CUENTA => 311202,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIVE HUNDRED CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 812
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 823,
                CatalogoCuentasModel::NO_CUENTA => 311203,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIVE HUNDRED FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 812
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 824,
                CatalogoCuentasModel::NO_CUENTA => 311204,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIVE HUNDRED INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 812
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 825,
                CatalogoCuentasModel::NO_CUENTA => 312000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VENTAS INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 826,
                CatalogoCuentasModel::NO_CUENTA => 320000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VENTAS UNIDADES SEMINUEVAS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 827,
                CatalogoCuentasModel::NO_CUENTA => 320001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VENTAS AUTOS EXENTOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 826
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 828,
                CatalogoCuentasModel::NO_CUENTA => 320002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VENTAS AUTOS GRAVADOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 826
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 829,
                CatalogoCuentasModel::NO_CUENTA => 320003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VENTAS CAMIONES EXENTOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 826
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 830,
                CatalogoCuentasModel::NO_CUENTA => 320004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VENTAS CAMIONES GRAVADOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 826
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 831,
                CatalogoCuentasModel::NO_CUENTA => 320100,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMIONES PESADOS SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 826
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 832,
                CatalogoCuentasModel::NO_CUENTA => 320101,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION PES F450-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 826
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 833,
                CatalogoCuentasModel::NO_CUENTA => 320102,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION PES F450-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 826
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 834,
                CatalogoCuentasModel::NO_CUENTA => 320103,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION PES F-450-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 826
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 835,
                CatalogoCuentasModel::NO_CUENTA => 320104,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION PES F-450 INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 826
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 836,
                CatalogoCuentasModel::NO_CUENTA => 320201,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMIONES PES F-550 GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 826
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 837,
                CatalogoCuentasModel::NO_CUENTA => 320202,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION PES F-550 CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 826
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 838,
                CatalogoCuentasModel::NO_CUENTA => 320203,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION PES F-550 FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 826
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 839,
                CatalogoCuentasModel::NO_CUENTA => 320204,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION PES F-550 INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 826
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 840,
                CatalogoCuentasModel::NO_CUENTA => 330000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VENTAS REFACCIONES Y ACCESORIOS QUERETARO",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 841,
                CatalogoCuentasModel::NO_CUENTA => 330001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VENTAS MENUDEO REFACCIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 840
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 842,
                CatalogoCuentasModel::NO_CUENTA => 330002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VENTAS MENUDEO ACCESORIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 840
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 843,
                CatalogoCuentasModel::NO_CUENTA => 330003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REFACCIONES TALLER DE SERVICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 840
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 844,
                CatalogoCuentasModel::NO_CUENTA => 330004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REFACCIONES GARANTIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 840
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 845,
                CatalogoCuentasModel::NO_CUENTA => 330005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DESCUENTOS SOBRE VENTA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 840
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 846,
                CatalogoCuentasModel::NO_CUENTA => 330006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VENTA MAYOREO REFACCIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 840
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 847,
                CatalogoCuentasModel::NO_CUENTA => 330007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VENTA MAYOREO ACCESORIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 840
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 848,
                CatalogoCuentasModel::NO_CUENTA => 330100,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VTA CAMIONES SEMINUEVOS QRO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 840
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 849,
                CatalogoCuentasModel::NO_CUENTA => 330101,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRAVADOS PARA IVA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 840
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 850,
                CatalogoCuentasModel::NO_CUENTA => 330102,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXENTOS PARA IVA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 840
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 851,
                CatalogoCuentasModel::NO_CUENTA => 335000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VTA AUTOS SEMINUEVOS SJR",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 852,
                CatalogoCuentasModel::NO_CUENTA => 335001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRAVADOS PARA IVA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 851
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 853,
                CatalogoCuentasModel::NO_CUENTA => 335002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXENTAS PARA IVA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 851
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 854,
                CatalogoCuentasModel::NO_CUENTA => 335100,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VTA CAMIONES SEMINUEVOS SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 851
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 855,
                CatalogoCuentasModel::NO_CUENTA => 335101,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRAVADAS PARA IVA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 851
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 856,
                CatalogoCuentasModel::NO_CUENTA => 335102,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXENTAS PARA IVA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 851
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 857,
                CatalogoCuentasModel::NO_CUENTA => 340000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VENTAS REFACCIONES Y ACCESORIOS S.J.R.",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 858,
                CatalogoCuentasModel::NO_CUENTA => 340001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VENTAS MENUDEO REFACCIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 857
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 859,
                CatalogoCuentasModel::NO_CUENTA => 340002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VENTAS MENUDEO ACCESORIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 857
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 860,
                CatalogoCuentasModel::NO_CUENTA => 340003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REFACCIONES TALLER DE SERVICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 857
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 861,
                CatalogoCuentasModel::NO_CUENTA => 340004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REFACCIONES DE GARANTIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 857
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 862,
                CatalogoCuentasModel::NO_CUENTA => 340005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DESCUENTO SOBRE VENTA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 857
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 863,
                CatalogoCuentasModel::NO_CUENTA => 340006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REFACCINES GARANTIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 857
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 864,
                CatalogoCuentasModel::NO_CUENTA => 340007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DESCUENTOS SOBRE VENTA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 857
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 865,
                CatalogoCuentasModel::NO_CUENTA => 340008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VENTA MENUDEO ACC.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 857
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 866,
                CatalogoCuentasModel::NO_CUENTA => 340100,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VENTA REFACCIONES Y ACCESORIOS SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 857
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 867,
                CatalogoCuentasModel::NO_CUENTA => 340101,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VENTA MAYOREO REFACCIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 857
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 868,
                CatalogoCuentasModel::NO_CUENTA => 340102,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MENUDEO - VTAS REFACCS Y ACCS SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 857
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 869,
                CatalogoCuentasModel::NO_CUENTA => 340103,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TALLER DE SERVICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 857
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 870,
                CatalogoCuentasModel::NO_CUENTA => 340104,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TALLER DE GARANTIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 857
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 871,
                CatalogoCuentasModel::NO_CUENTA => 340105,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ORDENES INTERNAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 857
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 872,
                CatalogoCuentasModel::NO_CUENTA => 340106,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REFACCIONES GARANTIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 857
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 873,
                CatalogoCuentasModel::NO_CUENTA => 340107,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DESCUENTOS SOBRE VENTA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 857
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 874,
                CatalogoCuentasModel::NO_CUENTA => 346000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REFAC. GTIAS.",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 875,
                CatalogoCuentasModel::NO_CUENTA => 346100,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DESCUENTOS SERVICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 874
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 876,
                CatalogoCuentasModel::NO_CUENTA => 346101,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DESCUENTOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 874
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 877,
                CatalogoCuentasModel::NO_CUENTA => 350000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VENTAS TALLER DE SERVICIO QUERETARO",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 878,
                CatalogoCuentasModel::NO_CUENTA => 350001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANO DE OBRA TALLER SERVICIO QUERETARO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 877
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 879,
                CatalogoCuentasModel::NO_CUENTA => 350002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TALLER GARANTIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 877
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 880,
                CatalogoCuentasModel::NO_CUENTA => 350003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANO DE OBRA GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 877
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 881,
                CatalogoCuentasModel::NO_CUENTA => 350004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANOS DE OBRAS GARANTIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 877
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 882,
                CatalogoCuentasModel::NO_CUENTA => 350005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "T.O.T.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 877
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 883,
                CatalogoCuentasModel::NO_CUENTA => 350006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MATERIALES DIVERSOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 877
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 884,
                CatalogoCuentasModel::NO_CUENTA => 350007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DESCUENTOS MANO DE OBRA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 877
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 885,
                CatalogoCuentasModel::NO_CUENTA => 350008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DESCUENTOS SERVICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 877
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 886,
                CatalogoCuentasModel::NO_CUENTA => 350009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INCENTIVO FORD ORDENES DE SERVICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 877
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 887,
                CatalogoCuentasModel::NO_CUENTA => 350100,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VENTA TALLER DE SERVICIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 877
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 888,
                CatalogoCuentasModel::NO_CUENTA => 350101,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANO DE OBRA GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 877
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 889,
                CatalogoCuentasModel::NO_CUENTA => 350102,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANO DE OBRA GARANTIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 877
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 890,
                CatalogoCuentasModel::NO_CUENTA => 350103,
                CatalogoCuentasModel::NOMBRE_CUENTA => "T. O. T.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 877
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 891,
                CatalogoCuentasModel::NO_CUENTA => 350104,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MATERIALES DIVERSOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 877
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 892,
                CatalogoCuentasModel::NO_CUENTA => 350105,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DESCUENTOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 877
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 893,
                CatalogoCuentasModel::NO_CUENTA => 360000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VENTA TALLER DE SERVICIO S.J.R.",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 894,
                CatalogoCuentasModel::NO_CUENTA => 360001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANO DE OBRA TALLER DE SERVICIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 893
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 895,
                CatalogoCuentasModel::NO_CUENTA => 360002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TALLER GARANTIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 893
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 896,
                CatalogoCuentasModel::NO_CUENTA => 360003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANO DE OBRA GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 893
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 897,
                CatalogoCuentasModel::NO_CUENTA => 360004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANO DE OBRA GARANTIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 893
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 898,
                CatalogoCuentasModel::NO_CUENTA => 360005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "T.O.T.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 893
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 899,
                CatalogoCuentasModel::NO_CUENTA => 360006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MATERIALES DIVERSOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 893
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 900,
                CatalogoCuentasModel::NO_CUENTA => 360007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DESCUENTOS MANO DE OBRA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 893
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 901,
                CatalogoCuentasModel::NO_CUENTA => 360008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DESCUENTOS SERVICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 893
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 902,
                CatalogoCuentasModel::NO_CUENTA => 360100,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DESCUENTOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 893
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 903,
                CatalogoCuentasModel::NO_CUENTA => 361002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DAR NOMBRE DE CTA.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 893
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 904,
                CatalogoCuentasModel::NO_CUENTA => 362000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "M.O. GTIAS.",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 905,
                CatalogoCuentasModel::NO_CUENTA => 362001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "M.O. GARANTIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 904
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 906,
                CatalogoCuentasModel::NO_CUENTA => 364100,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DESCUENTOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 904
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 907,
                CatalogoCuentasModel::NO_CUENTA => 370000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VENTA HOJALATERIA Y PINTURA",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 908,
                CatalogoCuentasModel::NO_CUENTA => 370001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANO DE OBRA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 907
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 909,
                CatalogoCuentasModel::NO_CUENTA => 370002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PIEZAS Y ACCESORIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 907
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 910,
                CatalogoCuentasModel::NO_CUENTA => 370003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HOJALATERIA Y PINTURA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 907
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 911,
                CatalogoCuentasModel::NO_CUENTA => 370004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DESCUENTOS MANO DE OBRA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 907
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 912,
                CatalogoCuentasModel::NO_CUENTA => 371000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VENTA  HOJALATERIA Y PINTURA",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 913,
                CatalogoCuentasModel::NO_CUENTA => 371001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANO DE OBRA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 912
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 914,
                CatalogoCuentasModel::NO_CUENTA => 371002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PIEZAS Y ACCESORIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 912
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 915,
                CatalogoCuentasModel::NO_CUENTA => 371003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DESCUENTOS M.O.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 912
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 916,
                CatalogoCuentasModel::NO_CUENTA => 371004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HOJALATERIA Y PINTURA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 912
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 917,
                CatalogoCuentasModel::NO_CUENTA => 371100,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PINTURA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 912
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 918,
                CatalogoCuentasModel::NO_CUENTA => 381000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VENTA HOJALATERIA Y PINTURA SJR",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 919,
                CatalogoCuentasModel::NO_CUENTA => 381001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HOJALATERIA Y PINTURA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 918
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 920,
                CatalogoCuentasModel::NO_CUENTA => 390000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REEXPRESION VENTAS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 921,
                CatalogoCuentasModel::NO_CUENTA => 400000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COSTO DE VENTAS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 922,
                CatalogoCuentasModel::NO_CUENTA => 400001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SABLE-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 923,
                CatalogoCuentasModel::NO_CUENTA => 400002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SABLE CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 924,
                CatalogoCuentasModel::NO_CUENTA => 400003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SABLE-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 925,
                CatalogoCuentasModel::NO_CUENTA => 400004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MUSTANG-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 926,
                CatalogoCuentasModel::NO_CUENTA => 400005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MUSTANG-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 927,
                CatalogoCuentasModel::NO_CUENTA => 400006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MUSTANG-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 928,
                CatalogoCuentasModel::NO_CUENTA => 400007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOCUS GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 929,
                CatalogoCuentasModel::NO_CUENTA => 400008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOCUS-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 930,
                CatalogoCuentasModel::NO_CUENTA => 400009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOCUS-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 931,
                CatalogoCuentasModel::NO_CUENTA => 400010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IKON-GENRERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 932,
                CatalogoCuentasModel::NO_CUENTA => 400011,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IKON-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 933,
                CatalogoCuentasModel::NO_CUENTA => 400012,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IKON-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 934,
                CatalogoCuentasModel::NO_CUENTA => 400013,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MONDEO-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 935,
                CatalogoCuentasModel::NO_CUENTA => 400014,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MONDEO-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 936,
                CatalogoCuentasModel::NO_CUENTA => 400015,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MONDEO-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 937,
                CatalogoCuentasModel::NO_CUENTA => 400016,
                CatalogoCuentasModel::NOMBRE_CUENTA => "KA-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 938,
                CatalogoCuentasModel::NO_CUENTA => 400017,
                CatalogoCuentasModel::NOMBRE_CUENTA => "KA-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 939,
                CatalogoCuentasModel::NO_CUENTA => 400018,
                CatalogoCuentasModel::NOMBRE_CUENTA => "KA-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 940,
                CatalogoCuentasModel::NO_CUENTA => 400019,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIESTA-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 941,
                CatalogoCuentasModel::NO_CUENTA => 400020,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIESTA-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 942,
                CatalogoCuentasModel::NO_CUENTA => 400021,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIESTA FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 943,
                CatalogoCuentasModel::NO_CUENTA => 400022,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRAN MARQUIS-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 944,
                CatalogoCuentasModel::NO_CUENTA => 400023,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRAN MARQUIS-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 945,
                CatalogoCuentasModel::NO_CUENTA => 400024,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRAN MARQUIS-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 946,
                CatalogoCuentasModel::NO_CUENTA => 400025,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRAN MARQUIS-INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 947,
                CatalogoCuentasModel::NO_CUENTA => 400026,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SABLE-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 948,
                CatalogoCuentasModel::NO_CUENTA => 400027,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MUSTANG-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 949,
                CatalogoCuentasModel::NO_CUENTA => 400028,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOCUS-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 950,
                CatalogoCuentasModel::NO_CUENTA => 400029,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IKON-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 951,
                CatalogoCuentasModel::NO_CUENTA => 400030,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MONDEO-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 952,
                CatalogoCuentasModel::NO_CUENTA => 400031,
                CatalogoCuentasModel::NOMBRE_CUENTA => "KA-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 953,
                CatalogoCuentasModel::NO_CUENTA => 400032,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIESTA INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 954,
                CatalogoCuentasModel::NO_CUENTA => 400033,
                CatalogoCuentasModel::NOMBRE_CUENTA => "THUNDERBIRD-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 955,
                CatalogoCuentasModel::NO_CUENTA => 400034,
                CatalogoCuentasModel::NOMBRE_CUENTA => "THUNDERBIRD-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 956,
                CatalogoCuentasModel::NO_CUENTA => 400035,
                CatalogoCuentasModel::NOMBRE_CUENTA => "THUNDERBIRD-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 957,
                CatalogoCuentasModel::NO_CUENTA => 400036,
                CatalogoCuentasModel::NOMBRE_CUENTA => "THUNDERBIRD-INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 958,
                CatalogoCuentasModel::NO_CUENTA => 400037,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EDGE GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 959,
                CatalogoCuentasModel::NO_CUENTA => 400038,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EDGE CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 960,
                CatalogoCuentasModel::NO_CUENTA => 400039,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EDGE FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 961,
                CatalogoCuentasModel::NO_CUENTA => 400040,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EDGE INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 962,
                CatalogoCuentasModel::NO_CUENTA => 400100,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COSTO UNIDADES SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 963,
                CatalogoCuentasModel::NO_CUENTA => 400101,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SABLE-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 964,
                CatalogoCuentasModel::NO_CUENTA => 400102,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SABLE-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 965,
                CatalogoCuentasModel::NO_CUENTA => 400103,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SABLE-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 966,
                CatalogoCuentasModel::NO_CUENTA => 400104,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MUSTANG-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 967,
                CatalogoCuentasModel::NO_CUENTA => 400105,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MUSTANG-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 968,
                CatalogoCuentasModel::NO_CUENTA => 400106,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MUSTANG-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 969,
                CatalogoCuentasModel::NO_CUENTA => 400107,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOCUS-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 970,
                CatalogoCuentasModel::NO_CUENTA => 400108,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOCUS-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 971,
                CatalogoCuentasModel::NO_CUENTA => 400109,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOCUS-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 972,
                CatalogoCuentasModel::NO_CUENTA => 400110,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IKON-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 973,
                CatalogoCuentasModel::NO_CUENTA => 400111,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IKON-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 974,
                CatalogoCuentasModel::NO_CUENTA => 400112,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IKON-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 975,
                CatalogoCuentasModel::NO_CUENTA => 400113,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MONDEO-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 976,
                CatalogoCuentasModel::NO_CUENTA => 400114,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MONDEO-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 977,
                CatalogoCuentasModel::NO_CUENTA => 400115,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MONDEO-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 978,
                CatalogoCuentasModel::NO_CUENTA => 400116,
                CatalogoCuentasModel::NOMBRE_CUENTA => "KA-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 979,
                CatalogoCuentasModel::NO_CUENTA => 400117,
                CatalogoCuentasModel::NOMBRE_CUENTA => "KA-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 980,
                CatalogoCuentasModel::NO_CUENTA => 400118,
                CatalogoCuentasModel::NOMBRE_CUENTA => "KA-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 981,
                CatalogoCuentasModel::NO_CUENTA => 400119,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIESTA-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 982,
                CatalogoCuentasModel::NO_CUENTA => 400120,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIESTA-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 983,
                CatalogoCuentasModel::NO_CUENTA => 400121,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIESTA-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 984,
                CatalogoCuentasModel::NO_CUENTA => 400133,
                CatalogoCuentasModel::NOMBRE_CUENTA => "THUNDERBIRD-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 985,
                CatalogoCuentasModel::NO_CUENTA => 400134,
                CatalogoCuentasModel::NOMBRE_CUENTA => "THUNDERBIRD-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 986,
                CatalogoCuentasModel::NO_CUENTA => 400135,
                CatalogoCuentasModel::NOMBRE_CUENTA => "THUNDERBIRD-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 987,
                CatalogoCuentasModel::NO_CUENTA => 400136,
                CatalogoCuentasModel::NOMBRE_CUENTA => "THUNDERBIRD-INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 988,
                CatalogoCuentasModel::NO_CUENTA => 400201,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MUSTANG-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 989,
                CatalogoCuentasModel::NO_CUENTA => 400202,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MUSTANG-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 990,
                CatalogoCuentasModel::NO_CUENTA => 400203,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MUSTANG-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 991,
                CatalogoCuentasModel::NO_CUENTA => 400204,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MUSTANG-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 992,
                CatalogoCuentasModel::NO_CUENTA => 400301,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOCUS-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 993,
                CatalogoCuentasModel::NO_CUENTA => 400302,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOCUS-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 994,
                CatalogoCuentasModel::NO_CUENTA => 400303,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOCUS-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 995,
                CatalogoCuentasModel::NO_CUENTA => 400304,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOCUS-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 996,
                CatalogoCuentasModel::NO_CUENTA => 400401,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ESCAPE-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 997,
                CatalogoCuentasModel::NO_CUENTA => 400402,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ESCAPE-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 998,
                CatalogoCuentasModel::NO_CUENTA => 400403,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ESCAPE-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 999,
                CatalogoCuentasModel::NO_CUENTA => 400404,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ESCAPE-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1000,
                CatalogoCuentasModel::NO_CUENTA => 400501,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IKON-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1001,
                CatalogoCuentasModel::NO_CUENTA => 400502,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IKON-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1002,
                CatalogoCuentasModel::NO_CUENTA => 400503,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IKON-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1003,
                CatalogoCuentasModel::NO_CUENTA => 400504,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IKON-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1004,
                CatalogoCuentasModel::NO_CUENTA => 400601,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MONDEO-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1005,
                CatalogoCuentasModel::NO_CUENTA => 400602,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MODEO-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1006,
                CatalogoCuentasModel::NO_CUENTA => 400603,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MODEO-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1007,
                CatalogoCuentasModel::NO_CUENTA => 400604,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MONDEO-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1008,
                CatalogoCuentasModel::NO_CUENTA => 400701,
                CatalogoCuentasModel::NOMBRE_CUENTA => "KA-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1009,
                CatalogoCuentasModel::NO_CUENTA => 400702,
                CatalogoCuentasModel::NOMBRE_CUENTA => "KA-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1010,
                CatalogoCuentasModel::NO_CUENTA => 400703,
                CatalogoCuentasModel::NOMBRE_CUENTA => "KA-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1011,
                CatalogoCuentasModel::NO_CUENTA => 400704,
                CatalogoCuentasModel::NOMBRE_CUENTA => "KA-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1012,
                CatalogoCuentasModel::NO_CUENTA => 400801,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIESTA GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1013,
                CatalogoCuentasModel::NO_CUENTA => 400802,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIETS CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1014,
                CatalogoCuentasModel::NO_CUENTA => 400803,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIESTA FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1015,
                CatalogoCuentasModel::NO_CUENTA => 400804,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIESTA INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1016,
                CatalogoCuentasModel::NO_CUENTA => 400900,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIVE HUNDRED GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1017,
                CatalogoCuentasModel::NO_CUENTA => 400901,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIVE HUNDRED GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1018,
                CatalogoCuentasModel::NO_CUENTA => 400902,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIVE HUNDRED CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1019,
                CatalogoCuentasModel::NO_CUENTA => 400903,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIVE HUNDRED FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1020,
                CatalogoCuentasModel::NO_CUENTA => 400904,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIVE HUNDRED INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1021,
                CatalogoCuentasModel::NO_CUENTA => 400905,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOCUS INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1022,
                CatalogoCuentasModel::NO_CUENTA => 400906,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SABLE INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1023,
                CatalogoCuentasModel::NO_CUENTA => 400907,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MONDEO INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1024,
                CatalogoCuentasModel::NO_CUENTA => 400908,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IKON INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1025,
                CatalogoCuentasModel::NO_CUENTA => 400909,
                CatalogoCuentasModel::NOMBRE_CUENTA => "KA INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1026,
                CatalogoCuentasModel::NO_CUENTA => 400910,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPLORER INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1027,
                CatalogoCuentasModel::NO_CUENTA => 400911,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FREESTAR INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1028,
                CatalogoCuentasModel::NO_CUENTA => 400912,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPEDITION INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1029,
                CatalogoCuentasModel::NO_CUENTA => 400913,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FUSION GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1030,
                CatalogoCuentasModel::NO_CUENTA => 400914,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FUSION CONAUTI",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1031,
                CatalogoCuentasModel::NO_CUENTA => 400915,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FUSION FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1032,
                CatalogoCuentasModel::NO_CUENTA => 400916,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FUSION INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1033,
                CatalogoCuentasModel::NO_CUENTA => 401001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOCUS GENRAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1034,
                CatalogoCuentasModel::NO_CUENTA => 401002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOCUS CONAUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1035,
                CatalogoCuentasModel::NO_CUENTA => 401003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOCUS FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1036,
                CatalogoCuentasModel::NO_CUENTA => 401004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOCUS INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1037,
                CatalogoCuentasModel::NO_CUENTA => 401005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SABLE GENRAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1038,
                CatalogoCuentasModel::NO_CUENTA => 401006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SABLE COANUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1039,
                CatalogoCuentasModel::NO_CUENTA => 401007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SABLE CONAUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1040,
                CatalogoCuentasModel::NO_CUENTA => 401008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SABLE INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1041,
                CatalogoCuentasModel::NO_CUENTA => 401009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MONDEO GENERAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1042,
                CatalogoCuentasModel::NO_CUENTA => 401010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MONDEO CONAUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1043,
                CatalogoCuentasModel::NO_CUENTA => 401011,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MONDEO FLOTILLA SRJ",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1044,
                CatalogoCuentasModel::NO_CUENTA => 401012,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MONDEO INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1045,
                CatalogoCuentasModel::NO_CUENTA => 401013,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MUSTANG GENERAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1046,
                CatalogoCuentasModel::NO_CUENTA => 401014,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MUSTANG CONAUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1047,
                CatalogoCuentasModel::NO_CUENTA => 401015,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MUSTANG FLOTILLA SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1048,
                CatalogoCuentasModel::NO_CUENTA => 401016,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MUSTANG INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1049,
                CatalogoCuentasModel::NO_CUENTA => 401017,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IKON GENERAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1050,
                CatalogoCuentasModel::NO_CUENTA => 401018,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IKON CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1051,
                CatalogoCuentasModel::NO_CUENTA => 401019,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IKON FLOTILLA SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1052,
                CatalogoCuentasModel::NO_CUENTA => 401020,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IKON INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1053,
                CatalogoCuentasModel::NO_CUENTA => 401021,
                CatalogoCuentasModel::NOMBRE_CUENTA => "KA GENRAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1054,
                CatalogoCuentasModel::NO_CUENTA => 401022,
                CatalogoCuentasModel::NOMBRE_CUENTA => "KA CONAUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1055,
                CatalogoCuentasModel::NO_CUENTA => 401023,
                CatalogoCuentasModel::NOMBRE_CUENTA => "KA FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1056,
                CatalogoCuentasModel::NO_CUENTA => 401024,
                CatalogoCuentasModel::NOMBRE_CUENTA => "KA INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1057,
                CatalogoCuentasModel::NO_CUENTA => 401025,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIESTA GENERAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1058,
                CatalogoCuentasModel::NO_CUENTA => 401026,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIESTA CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1059,
                CatalogoCuentasModel::NO_CUENTA => 401027,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIESTA FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1060,
                CatalogoCuentasModel::NO_CUENTA => 401028,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIESTA INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1061,
                CatalogoCuentasModel::NO_CUENTA => 401029,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPLORER GENERAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1062,
                CatalogoCuentasModel::NO_CUENTA => 401030,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPLORER CONAUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1063,
                CatalogoCuentasModel::NO_CUENTA => 401031,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPLORER FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1064,
                CatalogoCuentasModel::NO_CUENTA => 401032,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPLORER INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1065,
                CatalogoCuentasModel::NO_CUENTA => 401033,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FREESTAR GENERAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1066,
                CatalogoCuentasModel::NO_CUENTA => 401034,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FREESTAR CONAUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1067,
                CatalogoCuentasModel::NO_CUENTA => 401035,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FREESTAR FLOTILLA SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1068,
                CatalogoCuentasModel::NO_CUENTA => 401036,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FREESTAR INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1069,
                CatalogoCuentasModel::NO_CUENTA => 401037,
                CatalogoCuentasModel::NOMBRE_CUENTA => " ECONOLINE GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1070,
                CatalogoCuentasModel::NO_CUENTA => 401038,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECONOLINE CONAUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1071,
                CatalogoCuentasModel::NO_CUENTA => 401039,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECONOLINE FLOTILLA SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1072,
                CatalogoCuentasModel::NO_CUENTA => 401040,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECONOLINE INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1073,
                CatalogoCuentasModel::NO_CUENTA => 401041,
                CatalogoCuentasModel::NOMBRE_CUENTA => "THUNDERBIRD GENERAL SRJ",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1074,
                CatalogoCuentasModel::NO_CUENTA => 401042,
                CatalogoCuentasModel::NOMBRE_CUENTA => "THUNDERBIRD COANUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1075,
                CatalogoCuentasModel::NO_CUENTA => 401043,
                CatalogoCuentasModel::NOMBRE_CUENTA => "THUNDERBIRD FLOTILLA SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1076,
                CatalogoCuentasModel::NO_CUENTA => 401044,
                CatalogoCuentasModel::NOMBRE_CUENTA => "THUNDERBIRD INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1077,
                CatalogoCuentasModel::NO_CUENTA => 401045,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPORT GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1078,
                CatalogoCuentasModel::NO_CUENTA => 401046,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPORT CONAUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1079,
                CatalogoCuentasModel::NO_CUENTA => 401047,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPORT FLOTILLA SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1080,
                CatalogoCuentasModel::NO_CUENTA => 401048,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPORT INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1081,
                CatalogoCuentasModel::NO_CUENTA => 401049,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIVEHUNDRED GENERAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1082,
                CatalogoCuentasModel::NO_CUENTA => 401050,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIVE HUNDRED CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1083,
                CatalogoCuentasModel::NO_CUENTA => 401051,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIVEHUNDRED FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1084,
                CatalogoCuentasModel::NO_CUENTA => 401052,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIVE HUNDRED INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1085,
                CatalogoCuentasModel::NO_CUENTA => 401053,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ESCAPE GENERAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1086,
                CatalogoCuentasModel::NO_CUENTA => 401054,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ESCAPE CONAUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1087,
                CatalogoCuentasModel::NO_CUENTA => 401055,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ESCAPE FLOTILLA SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1088,
                CatalogoCuentasModel::NO_CUENTA => 401056,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ESCAPE INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1089,
                CatalogoCuentasModel::NO_CUENTA => 401057,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPEDITION GENERAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1090,
                CatalogoCuentasModel::NO_CUENTA => 401058,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPEDITION CONAUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1091,
                CatalogoCuentasModel::NO_CUENTA => 401059,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPEDITION FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1092,
                CatalogoCuentasModel::NO_CUENTA => 401060,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPEDITION INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1093,
                CatalogoCuentasModel::NO_CUENTA => 401061,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXCURSION GENERAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1094,
                CatalogoCuentasModel::NO_CUENTA => 401062,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXCURSION CONAUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1095,
                CatalogoCuentasModel::NO_CUENTA => 401063,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXCURSION FLOTILLA SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1096,
                CatalogoCuentasModel::NO_CUENTA => 401064,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXCURSION INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1097,
                CatalogoCuentasModel::NO_CUENTA => 401065,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FUSION GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1098,
                CatalogoCuentasModel::NO_CUENTA => 401066,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FISION CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1099,
                CatalogoCuentasModel::NO_CUENTA => 401067,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FUSION FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1100,
                CatalogoCuentasModel::NO_CUENTA => 401068,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FUSION INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 921
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1101,
                CatalogoCuentasModel::NO_CUENTA => 402000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMIONES LIGEROS SJR",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1102,
                CatalogoCuentasModel::NO_CUENTA => 402001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-150 GENERAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1101
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1103,
                CatalogoCuentasModel::NO_CUENTA => 402002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-150 CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1101
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1104,
                CatalogoCuentasModel::NO_CUENTA => 402003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-150 FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1101
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1105,
                CatalogoCuentasModel::NO_CUENTA => 402004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-150 INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1101
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1106,
                CatalogoCuentasModel::NO_CUENTA => 402005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-250 GENERAL SRJ",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1101
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1107,
                CatalogoCuentasModel::NO_CUENTA => 402006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-250 CONAUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1101
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1108,
                CatalogoCuentasModel::NO_CUENTA => 402007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-250 FLOTILLA SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1101
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1109,
                CatalogoCuentasModel::NO_CUENTA => 402008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-250 INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1101
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1110,
                CatalogoCuentasModel::NO_CUENTA => 402009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RANGER GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1101
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1111,
                CatalogoCuentasModel::NO_CUENTA => 402010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RANGER CONAUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1101
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1112,
                CatalogoCuentasModel::NO_CUENTA => 402011,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RANGER FLOTILLA SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1101
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1113,
                CatalogoCuentasModel::NO_CUENTA => 402012,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RANGER INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1101
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1114,
                CatalogoCuentasModel::NO_CUENTA => 402013,
                CatalogoCuentasModel::NOMBRE_CUENTA => "LOBO GENERAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1101
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1115,
                CatalogoCuentasModel::NO_CUENTA => 402014,
                CatalogoCuentasModel::NOMBRE_CUENTA => "LOBO CONAUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1101
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1116,
                CatalogoCuentasModel::NO_CUENTA => 402015,
                CatalogoCuentasModel::NOMBRE_CUENTA => "LOBO FLOTILLA SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1101
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1117,
                CatalogoCuentasModel::NO_CUENTA => 402016,
                CatalogoCuentasModel::NOMBRE_CUENTA => "LOBO INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1101
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1118,
                CatalogoCuentasModel::NO_CUENTA => 402017,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COURIER GRAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1101
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1119,
                CatalogoCuentasModel::NO_CUENTA => 402018,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COURIER CONAUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1101
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1120,
                CatalogoCuentasModel::NO_CUENTA => 402019,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COURIER FLOTILLA SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1101
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1121,
                CatalogoCuentasModel::NO_CUENTA => 402020,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CUORIER INT SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1101
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1122,
                CatalogoCuentasModel::NO_CUENTA => 402101,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-350 GENERAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1101
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1123,
                CatalogoCuentasModel::NO_CUENTA => 402102,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-350 CONAUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1101
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1124,
                CatalogoCuentasModel::NO_CUENTA => 402103,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-350 FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1101
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1125,
                CatalogoCuentasModel::NO_CUENTA => 402104,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-350 INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1101
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1126,
                CatalogoCuentasModel::NO_CUENTA => 402105,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-450 GENERAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1101
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1127,
                CatalogoCuentasModel::NO_CUENTA => 402106,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-450 CONAUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1101
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1128,
                CatalogoCuentasModel::NO_CUENTA => 402107,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-450 FLOTILLA SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1101
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1129,
                CatalogoCuentasModel::NO_CUENTA => 402108,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-450 INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1101
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1130,
                CatalogoCuentasModel::NO_CUENTA => 402109,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-550 GENERAL SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1101
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1131,
                CatalogoCuentasModel::NO_CUENTA => 402110,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-550 CONAUTO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1101
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1132,
                CatalogoCuentasModel::NO_CUENTA => 402111,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-550 FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1101
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1133,
                CatalogoCuentasModel::NO_CUENTA => 402112,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-550 INTERCAMBIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1101
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1134,
                CatalogoCuentasModel::NO_CUENTA => 410000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COSTO UNIDADES NUEVAS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1135,
                CatalogoCuentasModel::NO_CUENTA => 410001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BRONCO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1136,
                CatalogoCuentasModel::NO_CUENTA => 410002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECONOLINE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1137,
                CatalogoCuentasModel::NO_CUENTA => 410003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPORT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1138,
                CatalogoCuentasModel::NO_CUENTA => 410004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EDGE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1139,
                CatalogoCuentasModel::NO_CUENTA => 410005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ESCAPE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1140,
                CatalogoCuentasModel::NO_CUENTA => 410006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPEDITION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1141,
                CatalogoCuentasModel::NO_CUENTA => 410007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPLORER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1142,
                CatalogoCuentasModel::NO_CUENTA => 410008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-150",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1143,
                CatalogoCuentasModel::NO_CUENTA => 410009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-250",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1144,
                CatalogoCuentasModel::NO_CUENTA => 410010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-350",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1145,
                CatalogoCuentasModel::NO_CUENTA => 410011,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-450",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1146,
                CatalogoCuentasModel::NO_CUENTA => 410012,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-550",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1147,
                CatalogoCuentasModel::NO_CUENTA => 410013,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIESTA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1148,
                CatalogoCuentasModel::NO_CUENTA => 410014,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FOCUS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1149,
                CatalogoCuentasModel::NO_CUENTA => 410015,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FUSION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1150,
                CatalogoCuentasModel::NO_CUENTA => 410016,
                CatalogoCuentasModel::NOMBRE_CUENTA => "LOBO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1151,
                CatalogoCuentasModel::NO_CUENTA => 410017,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MUSTANG",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1152,
                CatalogoCuentasModel::NO_CUENTA => 410018,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RANGER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1153,
                CatalogoCuentasModel::NO_CUENTA => 410019,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SPORT TRAC",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1154,
                CatalogoCuentasModel::NO_CUENTA => 410020,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TRANSIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1155,
                CatalogoCuentasModel::NO_CUENTA => 410021,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FIGO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1156,
                CatalogoCuentasModel::NO_CUENTA => 410022,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.RANGER-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1157,
                CatalogoCuentasModel::NO_CUENTA => 410023,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.RANGER-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1158,
                CatalogoCuentasModel::NO_CUENTA => 410024,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.RANGER-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1159,
                CatalogoCuentasModel::NO_CUENTA => 410025,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.F350-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1160,
                CatalogoCuentasModel::NO_CUENTA => 410026,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.F350-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1161,
                CatalogoCuentasModel::NO_CUENTA => 410027,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.F350-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1162,
                CatalogoCuentasModel::NO_CUENTA => 410028,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.LOBO.GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1163,
                CatalogoCuentasModel::NO_CUENTA => 410029,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.LOBO-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1164,
                CatalogoCuentasModel::NO_CUENTA => 410030,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.LOBO-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1165,
                CatalogoCuentasModel::NO_CUENTA => 410031,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.COURIER-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1166,
                CatalogoCuentasModel::NO_CUENTA => 410032,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.COURIER-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1167,
                CatalogoCuentasModel::NO_CUENTA => 410033,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.COURIER-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1168,
                CatalogoCuentasModel::NO_CUENTA => 410034,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXCURSION-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1169,
                CatalogoCuentasModel::NO_CUENTA => 410035,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXCURSION-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1170,
                CatalogoCuentasModel::NO_CUENTA => 410036,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXCURSION-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1171,
                CatalogoCuentasModel::NO_CUENTA => 410037,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPEDITION-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1172,
                CatalogoCuentasModel::NO_CUENTA => 410038,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPEDITION-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1173,
                CatalogoCuentasModel::NO_CUENTA => 410039,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPEDITION-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1174,
                CatalogoCuentasModel::NO_CUENTA => 410040,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ESCAPE-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1175,
                CatalogoCuentasModel::NO_CUENTA => 410041,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPLORER-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1176,
                CatalogoCuentasModel::NO_CUENTA => 410042,
                CatalogoCuentasModel::NOMBRE_CUENTA => "WINDSTAR-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1177,
                CatalogoCuentasModel::NO_CUENTA => 410043,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TRANSIT-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1178,
                CatalogoCuentasModel::NO_CUENTA => 410044,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECONOLINE-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1179,
                CatalogoCuentasModel::NO_CUENTA => 410045,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. F150-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1180,
                CatalogoCuentasModel::NO_CUENTA => 410046,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. F250-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1181,
                CatalogoCuentasModel::NO_CUENTA => 410047,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. RANGER-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1182,
                CatalogoCuentasModel::NO_CUENTA => 410048,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. F350-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1183,
                CatalogoCuentasModel::NO_CUENTA => 410049,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. LOBO-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1184,
                CatalogoCuentasModel::NO_CUENTA => 410050,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG. COURIER-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1185,
                CatalogoCuentasModel::NO_CUENTA => 410051,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXCURSION-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1186,
                CatalogoCuentasModel::NO_CUENTA => 410052,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPEDITION-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1187,
                CatalogoCuentasModel::NO_CUENTA => 410053,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPORT GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1188,
                CatalogoCuentasModel::NO_CUENTA => 410054,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPORT CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1189,
                CatalogoCuentasModel::NO_CUENTA => 410055,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPORT FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1190,
                CatalogoCuentasModel::NO_CUENTA => 410056,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPORT INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1191,
                CatalogoCuentasModel::NO_CUENTA => 410057,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPORT P\/LLENO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1192,
                CatalogoCuentasModel::NO_CUENTA => 410100,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COSTO DE CAMIONES SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1193,
                CatalogoCuentasModel::NO_CUENTA => 410101,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ESCAPE-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1194,
                CatalogoCuentasModel::NO_CUENTA => 410102,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ESCAPE-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1195,
                CatalogoCuentasModel::NO_CUENTA => 410103,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ESCAPE-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1196,
                CatalogoCuentasModel::NO_CUENTA => 410104,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPLORER-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1197,
                CatalogoCuentasModel::NO_CUENTA => 410105,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPLORER-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1198,
                CatalogoCuentasModel::NO_CUENTA => 410106,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPLORER-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1199,
                CatalogoCuentasModel::NO_CUENTA => 410107,
                CatalogoCuentasModel::NOMBRE_CUENTA => "WINDSTAR-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1200,
                CatalogoCuentasModel::NO_CUENTA => 410108,
                CatalogoCuentasModel::NOMBRE_CUENTA => "WINDSTAR-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1201,
                CatalogoCuentasModel::NO_CUENTA => 410109,
                CatalogoCuentasModel::NOMBRE_CUENTA => "WINDSTAR-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1202,
                CatalogoCuentasModel::NO_CUENTA => 410110,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CLUB WAGON-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1203,
                CatalogoCuentasModel::NO_CUENTA => 410111,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CLUB WAGON-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1204,
                CatalogoCuentasModel::NO_CUENTA => 410112,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CLUB WAGON-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1205,
                CatalogoCuentasModel::NO_CUENTA => 410113,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECONOLINE-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1206,
                CatalogoCuentasModel::NO_CUENTA => 410114,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECONOLINE-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1207,
                CatalogoCuentasModel::NO_CUENTA => 410115,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECONOLINE-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1208,
                CatalogoCuentasModel::NO_CUENTA => 410116,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.F150-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1209,
                CatalogoCuentasModel::NO_CUENTA => 410117,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.F150-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1210,
                CatalogoCuentasModel::NO_CUENTA => 410118,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.F150-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1211,
                CatalogoCuentasModel::NO_CUENTA => 410119,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.F250-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1212,
                CatalogoCuentasModel::NO_CUENTA => 410120,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.F250-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1213,
                CatalogoCuentasModel::NO_CUENTA => 410121,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.F250-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1214,
                CatalogoCuentasModel::NO_CUENTA => 410122,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.RANGER-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1215,
                CatalogoCuentasModel::NO_CUENTA => 410123,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.RANGER-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1216,
                CatalogoCuentasModel::NO_CUENTA => 410124,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.RANGER-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1217,
                CatalogoCuentasModel::NO_CUENTA => 410125,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.F350-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1218,
                CatalogoCuentasModel::NO_CUENTA => 410126,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.F350-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1219,
                CatalogoCuentasModel::NO_CUENTA => 410127,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.F350-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1220,
                CatalogoCuentasModel::NO_CUENTA => 410128,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.LOBO-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1221,
                CatalogoCuentasModel::NO_CUENTA => 410129,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.LOBO-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1222,
                CatalogoCuentasModel::NO_CUENTA => 410130,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.LOBO-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1223,
                CatalogoCuentasModel::NO_CUENTA => 410131,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.COURIER-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1224,
                CatalogoCuentasModel::NO_CUENTA => 410132,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.COURIER-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1225,
                CatalogoCuentasModel::NO_CUENTA => 410133,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.COURIER-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1226,
                CatalogoCuentasModel::NO_CUENTA => 410134,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXCURSION-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1227,
                CatalogoCuentasModel::NO_CUENTA => 410135,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXCURSION-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1228,
                CatalogoCuentasModel::NO_CUENTA => 410136,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXCURSION-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1229,
                CatalogoCuentasModel::NO_CUENTA => 410137,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPEDITION-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1230,
                CatalogoCuentasModel::NO_CUENTA => 410138,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPEDITION-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1231,
                CatalogoCuentasModel::NO_CUENTA => 410139,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXPEDITION-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1232,
                CatalogoCuentasModel::NO_CUENTA => 410153,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPORT GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1233,
                CatalogoCuentasModel::NO_CUENTA => 410154,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPORT CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1234,
                CatalogoCuentasModel::NO_CUENTA => 410155,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPORT FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1235,
                CatalogoCuentasModel::NO_CUENTA => 410156,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPORT INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1236,
                CatalogoCuentasModel::NO_CUENTA => 410157,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECOSPORT P\/LLENO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1237,
                CatalogoCuentasModel::NO_CUENTA => 410201,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CLUB WAGON-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1238,
                CatalogoCuentasModel::NO_CUENTA => 410202,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CLUB WAGON-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1239,
                CatalogoCuentasModel::NO_CUENTA => 410203,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CLUB WAGON-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1240,
                CatalogoCuentasModel::NO_CUENTA => 410204,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CLUB WAGON-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1241,
                CatalogoCuentasModel::NO_CUENTA => 410301,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECONOLINE-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1242,
                CatalogoCuentasModel::NO_CUENTA => 410302,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECONOLINE-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1243,
                CatalogoCuentasModel::NO_CUENTA => 410303,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECONOLINE-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1244,
                CatalogoCuentasModel::NO_CUENTA => 410304,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ECONOLINE-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1245,
                CatalogoCuentasModel::NO_CUENTA => 410401,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.F150-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1246,
                CatalogoCuentasModel::NO_CUENTA => 410402,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.F150-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1247,
                CatalogoCuentasModel::NO_CUENTA => 410403,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.F150-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1248,
                CatalogoCuentasModel::NO_CUENTA => 410404,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.F150-INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1249,
                CatalogoCuentasModel::NO_CUENTA => 410501,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.F250-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1250,
                CatalogoCuentasModel::NO_CUENTA => 410502,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.F250-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1251,
                CatalogoCuentasModel::NO_CUENTA => 410503,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.F250-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1252,
                CatalogoCuentasModel::NO_CUENTA => 410504,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.F250-INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1253,
                CatalogoCuentasModel::NO_CUENTA => 410601,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.RANGER-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1254,
                CatalogoCuentasModel::NO_CUENTA => 410602,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.RANGER-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1255,
                CatalogoCuentasModel::NO_CUENTA => 410603,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.RANGER-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1256,
                CatalogoCuentasModel::NO_CUENTA => 410604,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.RANGER-INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1257,
                CatalogoCuentasModel::NO_CUENTA => 410701,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.F350-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1258,
                CatalogoCuentasModel::NO_CUENTA => 410702,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.F350-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1259,
                CatalogoCuentasModel::NO_CUENTA => 410703,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.F350-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1260,
                CatalogoCuentasModel::NO_CUENTA => 410704,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.F350-INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1261,
                CatalogoCuentasModel::NO_CUENTA => 410801,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.LOBO-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1262,
                CatalogoCuentasModel::NO_CUENTA => 410802,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.LOBO-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1263,
                CatalogoCuentasModel::NO_CUENTA => 410803,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.LOBO-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1264,
                CatalogoCuentasModel::NO_CUENTA => 410804,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.LOBO-INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1265,
                CatalogoCuentasModel::NO_CUENTA => 410901,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.COURIER-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1266,
                CatalogoCuentasModel::NO_CUENTA => 410902,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.COURIER-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1267,
                CatalogoCuentasModel::NO_CUENTA => 410903,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.COURIER-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1268,
                CatalogoCuentasModel::NO_CUENTA => 410904,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION LIG.COURIER-INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1134
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1269,
                CatalogoCuentasModel::NO_CUENTA => 411000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DESCUENTOS SOBRE VENTAS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1270,
                CatalogoCuentasModel::NO_CUENTA => 411001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXCURSION-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1269
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1271,
                CatalogoCuentasModel::NO_CUENTA => 411002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXCURSION-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1269
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1272,
                CatalogoCuentasModel::NO_CUENTA => 411003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXCURSION-FLOTILLA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1269
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1273,
                CatalogoCuentasModel::NO_CUENTA => 411004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXCURSION-INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1269
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1274,
                CatalogoCuentasModel::NO_CUENTA => 412000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COSTO INTERCAMBIOS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1275,
                CatalogoCuentasModel::NO_CUENTA => 420000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COSTO UNIDADES SEMINUEVAS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1276,
                CatalogoCuentasModel::NO_CUENTA => 420001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COSTO AUTOS EXENTOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1275
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1277,
                CatalogoCuentasModel::NO_CUENTA => 420002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COSTO AUTOS GRAVADOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1275
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1278,
                CatalogoCuentasModel::NO_CUENTA => 420003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COSTO CAMION EXENTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1275
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1279,
                CatalogoCuentasModel::NO_CUENTA => 420004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COSTO CAMION GRAVADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1275
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1280,
                CatalogoCuentasModel::NO_CUENTA => 420100,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COSTO PESADOS SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1275
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1281,
                CatalogoCuentasModel::NO_CUENTA => 420101,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION PES.F450-GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1275
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1282,
                CatalogoCuentasModel::NO_CUENTA => 420102,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION PES.F450-CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1275
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1283,
                CatalogoCuentasModel::NO_CUENTA => 420103,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAMION PES.F450-FLOTILLAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1275
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1284,
                CatalogoCuentasModel::NO_CUENTA => 420104,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-450 INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1275
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1285,
                CatalogoCuentasModel::NO_CUENTA => 420201,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-550 GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1275
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1286,
                CatalogoCuentasModel::NO_CUENTA => 420202,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-550 CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1275
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1287,
                CatalogoCuentasModel::NO_CUENTA => 420203,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-550 FLOTILLA SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1275
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1288,
                CatalogoCuentasModel::NO_CUENTA => 420204,
                CatalogoCuentasModel::NOMBRE_CUENTA => "F-350 INTERCAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1275
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1289,
                CatalogoCuentasModel::NO_CUENTA => 421000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DESCUENTOS SOBRE VENTAS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1290,
                CatalogoCuentasModel::NO_CUENTA => 430000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COSTO REFACCIONES Y ACCESORIOS QUERETARO",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1291,
                CatalogoCuentasModel::NO_CUENTA => 430001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COSTO MENUDEO REFACCIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1290
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1292,
                CatalogoCuentasModel::NO_CUENTA => 430002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COSTO ACCESORIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1290
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1293,
                CatalogoCuentasModel::NO_CUENTA => 430003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COSTO MAYOREO REFACCIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1290
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1294,
                CatalogoCuentasModel::NO_CUENTA => 430004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COSTO MAYOREO ACCESORIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1290
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1295,
                CatalogoCuentasModel::NO_CUENTA => 430100,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COSTOS CAMIONES SEMINUEVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1290
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1296,
                CatalogoCuentasModel::NO_CUENTA => 430101,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRAVADAS PARA IVA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1290
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1297,
                CatalogoCuentasModel::NO_CUENTA => 430102,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXENTAS PARA IVA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1290
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1298,
                CatalogoCuentasModel::NO_CUENTA => 431000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DESCUENTOS SOBRE VENTAS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1299,
                CatalogoCuentasModel::NO_CUENTA => 435000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AUTOS SEMINUEVOS SJR",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1300,
                CatalogoCuentasModel::NO_CUENTA => 435001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRAVADAS 100% PARA IVA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1299
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1301,
                CatalogoCuentasModel::NO_CUENTA => 435002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXENTAS PARA IVA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1299
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1302,
                CatalogoCuentasModel::NO_CUENTA => 435100,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COSTOS CAMIONES SEMINUEVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1299
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1303,
                CatalogoCuentasModel::NO_CUENTA => 435101,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRAVADAS 100% PARA IVA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1299
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1304,
                CatalogoCuentasModel::NO_CUENTA => 435102,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXENTAS PARA IVA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1299
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1305,
                CatalogoCuentasModel::NO_CUENTA => 440000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COSTO REFACCIONES Y ACCESORIOS S.J.R.",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1306,
                CatalogoCuentasModel::NO_CUENTA => 440001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COSTO MENUDEO REFACCIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1305
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1307,
                CatalogoCuentasModel::NO_CUENTA => 440002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COSTO ACCESORIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1305
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1308,
                CatalogoCuentasModel::NO_CUENTA => 440003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TALLER DE SERVICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1305
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1309,
                CatalogoCuentasModel::NO_CUENTA => 440004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TALLER DE GARANTIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1305
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1310,
                CatalogoCuentasModel::NO_CUENTA => 440005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ORDENES INTERNAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1305
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1311,
                CatalogoCuentasModel::NO_CUENTA => 440006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REFAC.GTIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1305
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1312,
                CatalogoCuentasModel::NO_CUENTA => 440007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DESCUENTOS SOBRE VENTA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1305
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1313,
                CatalogoCuentasModel::NO_CUENTA => 440008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COSTO ACCESORIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1305
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1314,
                CatalogoCuentasModel::NO_CUENTA => 440100,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COSTO REFACCIONES Y ACCESORIOS SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1305
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1315,
                CatalogoCuentasModel::NO_CUENTA => 440101,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MAYOREO - CTO REFACCS Y ACCS SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1305
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1316,
                CatalogoCuentasModel::NO_CUENTA => 440102,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COSTO MAYOREO REFACCIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1305
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1317,
                CatalogoCuentasModel::NO_CUENTA => 440103,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TALLER DE SERVICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1305
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1318,
                CatalogoCuentasModel::NO_CUENTA => 440104,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TALLER DE GARANTIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1305
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1319,
                CatalogoCuentasModel::NO_CUENTA => 440105,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ORDENES INTERNAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1305
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1320,
                CatalogoCuentasModel::NO_CUENTA => 440106,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REFACCIONES GARANTIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1305
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1321,
                CatalogoCuentasModel::NO_CUENTA => 440107,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DESCUENTOS SOBRE VENTA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1305
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1322,
                CatalogoCuentasModel::NO_CUENTA => 441000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DESCUENTOS SOBRE VENTAS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1323,
                CatalogoCuentasModel::NO_CUENTA => 442000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MENUDEO",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1324,
                CatalogoCuentasModel::NO_CUENTA => 442002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AJUSTE AL INVENTARIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1323
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1325,
                CatalogoCuentasModel::NO_CUENTA => 443000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TALLER DE SERVICIO",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1326,
                CatalogoCuentasModel::NO_CUENTA => 446000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TALLER GARANTIAS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1327,
                CatalogoCuentasModel::NO_CUENTA => 447001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CARGOS INTERNOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1326
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1328,
                CatalogoCuentasModel::NO_CUENTA => 450000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COSTO TALLER DE SERVICIO QUERETARO",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1329,
                CatalogoCuentasModel::NO_CUENTA => 450001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TALLER DE SERVICIO (LAVADORES)",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1330,
                CatalogoCuentasModel::NO_CUENTA => 450002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TALLER DE GARANTIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1331,
                CatalogoCuentasModel::NO_CUENTA => 450003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANO DE OBRA GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1332,
                CatalogoCuentasModel::NO_CUENTA => 450004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANO DE OBRA GARANTIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1333,
                CatalogoCuentasModel::NO_CUENTA => 450005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "T.O.T.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1334,
                CatalogoCuentasModel::NO_CUENTA => 450006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MATERIALES DIVERSOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1335,
                CatalogoCuentasModel::NO_CUENTA => 450007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DESCUENTOS MANO DE OBRA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1336,
                CatalogoCuentasModel::NO_CUENTA => 450008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BONO POR EFICIENCIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1337,
                CatalogoCuentasModel::NO_CUENTA => 450009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1338,
                CatalogoCuentasModel::NO_CUENTA => 450010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HORAS EXTRAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1339,
                CatalogoCuentasModel::NO_CUENTA => 450011,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMSS PATRONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1340,
                CatalogoCuentasModel::NO_CUENTA => 450012,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INDEMNIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1341,
                CatalogoCuentasModel::NO_CUENTA => 450013,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1342,
                CatalogoCuentasModel::NO_CUENTA => 450014,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DE ANTIGUEDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1343,
                CatalogoCuentasModel::NO_CUENTA => 450015,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DOMINICAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1344,
                CatalogoCuentasModel::NO_CUENTA => 450016,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA VACACIONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1345,
                CatalogoCuentasModel::NO_CUENTA => 450017,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1346,
                CatalogoCuentasModel::NO_CUENTA => 450018,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VACACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1347,
                CatalogoCuentasModel::NO_CUENTA => 450019,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTROS INGRES S\/SUELDOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1348,
                CatalogoCuentasModel::NO_CUENTA => 450020,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DOMINICAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1349,
                CatalogoCuentasModel::NO_CUENTA => 450077,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1350,
                CatalogoCuentasModel::NO_CUENTA => 450078,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1351,
                CatalogoCuentasModel::NO_CUENTA => 450100,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COSTO TALLER DE SERVICIO SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1352,
                CatalogoCuentasModel::NO_CUENTA => 450101,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANO DE OBRA GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1353,
                CatalogoCuentasModel::NO_CUENTA => 450102,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANO DE OBRA GARANTIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1354,
                CatalogoCuentasModel::NO_CUENTA => 450103,
                CatalogoCuentasModel::NOMBRE_CUENTA => "T.O.T.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1355,
                CatalogoCuentasModel::NO_CUENTA => 450104,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MATERIALES DIVERSOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1356,
                CatalogoCuentasModel::NO_CUENTA => 450105,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DESCUENTOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1357,
                CatalogoCuentasModel::NO_CUENTA => 450106,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1358,
                CatalogoCuentasModel::NO_CUENTA => 450107,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HORAS EXTRA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1359,
                CatalogoCuentasModel::NO_CUENTA => 450108,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DOMINICAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1360,
                CatalogoCuentasModel::NO_CUENTA => 450109,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRATIFICACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1361,
                CatalogoCuentasModel::NO_CUENTA => 450112,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AGUINALDO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1362,
                CatalogoCuentasModel::NO_CUENTA => 450113,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VACACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1363,
                CatalogoCuentasModel::NO_CUENTA => 450114,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA VACACIONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1364,
                CatalogoCuentasModel::NO_CUENTA => 450115,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMSS PATRONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1365,
                CatalogoCuentasModel::NO_CUENTA => 450116,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DE ANTIGUEDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1366,
                CatalogoCuentasModel::NO_CUENTA => 450118,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CREDITO AL SALARIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1367,
                CatalogoCuentasModel::NO_CUENTA => 450119,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTROS ING. S\/SUELDOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1368,
                CatalogoCuentasModel::NO_CUENTA => 450177,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1369,
                CatalogoCuentasModel::NO_CUENTA => 450178,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFINAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1328
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1370,
                CatalogoCuentasModel::NO_CUENTA => 460000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COSTOS TALLER DE SERVICIO S.J.R.",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1371,
                CatalogoCuentasModel::NO_CUENTA => 460001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TALLER DE SERVICIO (LAVADORES)",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1370
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1372,
                CatalogoCuentasModel::NO_CUENTA => 460002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TALLER DE GARANTIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1370
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1373,
                CatalogoCuentasModel::NO_CUENTA => 460003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANO DE OBRA GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1370
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1374,
                CatalogoCuentasModel::NO_CUENTA => 460004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANO DE OBRA GARANTIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1370
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1375,
                CatalogoCuentasModel::NO_CUENTA => 460005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "T.O.T.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1370
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1376,
                CatalogoCuentasModel::NO_CUENTA => 460006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MATERIALES DIVERSOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1370
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1377,
                CatalogoCuentasModel::NO_CUENTA => 460007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DESCUENTOS MANO DE OBRA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1370
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1378,
                CatalogoCuentasModel::NO_CUENTA => 460008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BONO POR EFICIENCIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1370
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1379,
                CatalogoCuentasModel::NO_CUENTA => 460009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1370
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1380,
                CatalogoCuentasModel::NO_CUENTA => 460010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HORAS EXTRAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1370
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1381,
                CatalogoCuentasModel::NO_CUENTA => 460011,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMSS PATRONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1370
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1382,
                CatalogoCuentasModel::NO_CUENTA => 460012,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INDEMNIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1370
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1383,
                CatalogoCuentasModel::NO_CUENTA => 460013,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1370
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1384,
                CatalogoCuentasModel::NO_CUENTA => 460014,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DE ANTIGUEDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1370
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1385,
                CatalogoCuentasModel::NO_CUENTA => 460015,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DOMINICAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1370
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1386,
                CatalogoCuentasModel::NO_CUENTA => 460016,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA VACACIONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1370
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1387,
                CatalogoCuentasModel::NO_CUENTA => 460017,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1370
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1388,
                CatalogoCuentasModel::NO_CUENTA => 460018,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VACACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1370
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1389,
                CatalogoCuentasModel::NO_CUENTA => 460098,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AGUINALDO EMPLEADOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1370
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1390,
                CatalogoCuentasModel::NO_CUENTA => 462000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANO DE OBRA GARANTIAS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1391,
                CatalogoCuentasModel::NO_CUENTA => 465000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TRABAJOS FUERA",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1392,
                CatalogoCuentasModel::NO_CUENTA => 465001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COSTOS DE MAT.DIVERSOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1391
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1393,
                CatalogoCuentasModel::NO_CUENTA => 470000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COSTO TALLER HYP",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1394,
                CatalogoCuentasModel::NO_CUENTA => 470001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PIEZAS Y ACCESORIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1393
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1395,
                CatalogoCuentasModel::NO_CUENTA => 470002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HOJALATERIA Y PINTURA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1393
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1396,
                CatalogoCuentasModel::NO_CUENTA => 470003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANO DE OBRA GENERAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1393
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1397,
                CatalogoCuentasModel::NO_CUENTA => 470004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MATERIALES DIVERSOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1393
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1398,
                CatalogoCuentasModel::NO_CUENTA => 470005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "T.O.T.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1393
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1399,
                CatalogoCuentasModel::NO_CUENTA => 470006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DESCUENTO MANO DE OBRA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1393
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1400,
                CatalogoCuentasModel::NO_CUENTA => 470007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BONO POR EFICIENCIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1393
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1401,
                CatalogoCuentasModel::NO_CUENTA => 470008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1393
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1402,
                CatalogoCuentasModel::NO_CUENTA => 470009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HORAS EXTRAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1393
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1403,
                CatalogoCuentasModel::NO_CUENTA => 470010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMSS PATRONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1393
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1404,
                CatalogoCuentasModel::NO_CUENTA => 470011,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INDEMNIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1393
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1405,
                CatalogoCuentasModel::NO_CUENTA => 470012,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1393
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1406,
                CatalogoCuentasModel::NO_CUENTA => 470013,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DE ANTIGUEDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1393
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1407,
                CatalogoCuentasModel::NO_CUENTA => 470014,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DOMINICAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1393
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1408,
                CatalogoCuentasModel::NO_CUENTA => 470015,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA VACACIONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1393
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1409,
                CatalogoCuentasModel::NO_CUENTA => 470016,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1393
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1410,
                CatalogoCuentasModel::NO_CUENTA => 470017,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VACACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1393
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1411,
                CatalogoCuentasModel::NO_CUENTA => 471000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANO DE OBRA",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1412,
                CatalogoCuentasModel::NO_CUENTA => 471100,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PIEZAS Y ACCESORIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1411
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1413,
                CatalogoCuentasModel::NO_CUENTA => 472000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MATERIALES HYP",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1414,
                CatalogoCuentasModel::NO_CUENTA => 490000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACTUALIZACION COSTOS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1415,
                CatalogoCuentasModel::NO_CUENTA => 500000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE VENTA",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1416,
                CatalogoCuentasModel::NO_CUENTA => 500001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES VENDEDORES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1417,
                CatalogoCuentasModel::NO_CUENTA => 500002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES SEGUROS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1418,
                CatalogoCuentasModel::NO_CUENTA => 500003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTRAS COMISIONES VENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1419,
                CatalogoCuentasModel::NO_CUENTA => 500004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUELDOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1420,
                CatalogoCuentasModel::NO_CUENTA => 500005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRATIFICACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1421,
                CatalogoCuentasModel::NO_CUENTA => 500006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES (SUELDOS)",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1422,
                CatalogoCuentasModel::NO_CUENTA => 500007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TIEMPO EXTRA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1423,
                CatalogoCuentasModel::NO_CUENTA => 500008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DOMINICAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1424,
                CatalogoCuentasModel::NO_CUENTA => 500009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTROS ING.S\/SUELDOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1425,
                CatalogoCuentasModel::NO_CUENTA => 500010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DE ANTIGUEDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1426,
                CatalogoCuentasModel::NO_CUENTA => 500011,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INDEMNIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1427,
                CatalogoCuentasModel::NO_CUENTA => 500012,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AGUINALDO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1428,
                CatalogoCuentasModel::NO_CUENTA => 500013,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VACACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1429,
                CatalogoCuentasModel::NO_CUENTA => 500014,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA VACACIONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1430,
                CatalogoCuentasModel::NO_CUENTA => 500015,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMSS PATRONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1431,
                CatalogoCuentasModel::NO_CUENTA => 500016,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1432,
                CatalogoCuentasModel::NO_CUENTA => 500017,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1433,
                CatalogoCuentasModel::NO_CUENTA => 500018,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CREDITO AL SALARIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1434,
                CatalogoCuentasModel::NO_CUENTA => 500019,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UNIFORMES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1435,
                CatalogoCuentasModel::NO_CUENTA => 500020,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PREVISION SOCIAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1436,
                CatalogoCuentasModel::NO_CUENTA => 500021,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAPACITACION Y ENTRENAMIENTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1437,
                CatalogoCuentasModel::NO_CUENTA => 500022,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE EDIFICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1438,
                CatalogoCuentasModel::NO_CUENTA => 500023,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1439,
                CatalogoCuentasModel::NO_CUENTA => 500024,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE TRANSPORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1440,
                CatalogoCuentasModel::NO_CUENTA => 500025,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE TALLER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1441,
                CatalogoCuentasModel::NO_CUENTA => 500026,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE TALLER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1442,
                CatalogoCuentasModel::NO_CUENTA => 500027,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE OFICINA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1443,
                CatalogoCuentasModel::NO_CUENTA => 500028,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE TRANSPORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1444,
                CatalogoCuentasModel::NO_CUENTA => 500029,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1445,
                CatalogoCuentasModel::NO_CUENTA => 500030,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE COMUNICACION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1446,
                CatalogoCuentasModel::NO_CUENTA => 500031,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AMORTIZACION EDIFICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1447,
                CatalogoCuentasModel::NO_CUENTA => 500032,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASOLINA NUEVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1448,
                CatalogoCuentasModel::NO_CUENTA => 500033,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASOLINA UNIDADES EN USO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1449,
                CatalogoCuentasModel::NO_CUENTA => 500034,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE PREVIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1450,
                CatalogoCuentasModel::NO_CUENTA => 500035,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HERRAMIENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1451,
                CatalogoCuentasModel::NO_CUENTA => 500036,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PUBLICIDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1452,
                CatalogoCuentasModel::NO_CUENTA => 500037,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PROMOCIONES ESPECIALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1453,
                CatalogoCuentasModel::NO_CUENTA => 500038,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CORTESIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1454,
                CatalogoCuentasModel::NO_CUENTA => 500039,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CUOTAS Y SUSCRIPCIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1455,
                CatalogoCuentasModel::NO_CUENTA => 500040,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DONATIVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1456,
                CatalogoCuentasModel::NO_CUENTA => 500041,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUBSIDIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1457,
                CatalogoCuentasModel::NO_CUENTA => 500042,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUBSIDIO DE SEGUROS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1458,
                CatalogoCuentasModel::NO_CUENTA => 500043,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RENTA DE LOCALES P.F.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1459,
                CatalogoCuentasModel::NO_CUENTA => 500044,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RENTA DE LOCALES P.M.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1460,
                CatalogoCuentasModel::NO_CUENTA => 500045,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TELEFONOS, FAX Y CORREOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1461,
                CatalogoCuentasModel::NO_CUENTA => 500046,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VIGILANCIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1462,
                CatalogoCuentasModel::NO_CUENTA => 500047,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CONSUMIBLES EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1463,
                CatalogoCuentasModel::NO_CUENTA => 500048,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS NO DEDUCIBLES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1464,
                CatalogoCuentasModel::NO_CUENTA => 500049,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PAPELERIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1465,
                CatalogoCuentasModel::NO_CUENTA => 500050,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SEGUROS Y FIANZAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1466,
                CatalogoCuentasModel::NO_CUENTA => 500051,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMPUESTOS Y DERECHOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1467,
                CatalogoCuentasModel::NO_CUENTA => 500052,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MULTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1468,
                CatalogoCuentasModel::NO_CUENTA => 500053,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACTUALIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1469,
                CatalogoCuentasModel::NO_CUENTA => 500054,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECARGOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1470,
                CatalogoCuentasModel::NO_CUENTA => 500055,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE VIAJE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1471,
                CatalogoCuentasModel::NO_CUENTA => 500056,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REPARACION DA¥OS SERVICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1472,
                CatalogoCuentasModel::NO_CUENTA => 500057,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COPIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1473,
                CatalogoCuentasModel::NO_CUENTA => 500058,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRASA Y LUBRICANTES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1474,
                CatalogoCuentasModel::NO_CUENTA => 500059,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PASAJES Y ESTACIONAMIENTOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1475,
                CatalogoCuentasModel::NO_CUENTA => 500060,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UTILES DE ASEO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1476,
                CatalogoCuentasModel::NO_CUENTA => 500061,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS FIN DE A¥O",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1477,
                CatalogoCuentasModel::NO_CUENTA => 500062,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECLUTAMIENTO Y SELECCION DE PERSONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1478,
                CatalogoCuentasModel::NO_CUENTA => 500063,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REDONDEOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1479,
                CatalogoCuentasModel::NO_CUENTA => 500064,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HONORARIOS PERSONAS FISICAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1480,
                CatalogoCuentasModel::NO_CUENTA => 500065,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HONORARIOS PERSONAS MORALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1481,
                CatalogoCuentasModel::NO_CUENTA => 500066,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ASESORIA ADMINISTRATIVA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1482,
                CatalogoCuentasModel::NO_CUENTA => 500067,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ASESORIA EN VENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1483,
                CatalogoCuentasModel::NO_CUENTA => 500068,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AUDITORIA EXTERNA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1484,
                CatalogoCuentasModel::NO_CUENTA => 500069,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DIVERSOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1485,
                CatalogoCuentasModel::NO_CUENTA => 500070,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SEGUROS DE TRASLADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1486,
                CatalogoCuentasModel::NO_CUENTA => 500071,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ARRENDAMIENTO DE EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1487,
                CatalogoCuentasModel::NO_CUENTA => 500072,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE TRASLADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1488,
                CatalogoCuentasModel::NO_CUENTA => 500073,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ALMACENAMIENTO UNIDADES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1489,
                CatalogoCuentasModel::NO_CUENTA => 500074,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AGUA POTABLE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1490,
                CatalogoCuentasModel::NO_CUENTA => 500075,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ATENCION A CLIENTES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1491,
                CatalogoCuentasModel::NO_CUENTA => 500076,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FLETES Y MENSJERIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1492,
                CatalogoCuentasModel::NO_CUENTA => 500077,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1493,
                CatalogoCuentasModel::NO_CUENTA => 500078,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1494,
                CatalogoCuentasModel::NO_CUENTA => 500079,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BURO DE CREDITO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1495,
                CatalogoCuentasModel::NO_CUENTA => 500080,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUBSIDIO DE SEGUROS NO DEDUC.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1496,
                CatalogoCuentasModel::NO_CUENTA => 500100,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE VENTA SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1497,
                CatalogoCuentasModel::NO_CUENTA => 500101,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES VENDEDORES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1498,
                CatalogoCuentasModel::NO_CUENTA => 500102,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES SEGUROS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1499,
                CatalogoCuentasModel::NO_CUENTA => 500103,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTRAS COMISIONES VENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1500,
                CatalogoCuentasModel::NO_CUENTA => 500104,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUELDOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1501,
                CatalogoCuentasModel::NO_CUENTA => 500105,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRATIFICACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1502,
                CatalogoCuentasModel::NO_CUENTA => 500106,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES (SUELDOS)",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1503,
                CatalogoCuentasModel::NO_CUENTA => 500107,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TIEMPO EXTRA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1504,
                CatalogoCuentasModel::NO_CUENTA => 500108,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DOMINICAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1505,
                CatalogoCuentasModel::NO_CUENTA => 500109,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTROS ING. S\/SUELDOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1506,
                CatalogoCuentasModel::NO_CUENTA => 500110,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DE ANTIGUEDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1507,
                CatalogoCuentasModel::NO_CUENTA => 500111,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INDEMNIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1508,
                CatalogoCuentasModel::NO_CUENTA => 500112,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AGUINALDO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1509,
                CatalogoCuentasModel::NO_CUENTA => 500113,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VACACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1510,
                CatalogoCuentasModel::NO_CUENTA => 500114,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA VACACIONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1511,
                CatalogoCuentasModel::NO_CUENTA => 500115,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMSS PATRONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1512,
                CatalogoCuentasModel::NO_CUENTA => 500116,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1513,
                CatalogoCuentasModel::NO_CUENTA => 500117,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1514,
                CatalogoCuentasModel::NO_CUENTA => 500118,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CREDITO AL SALARIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1515,
                CatalogoCuentasModel::NO_CUENTA => 500119,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UNIFORMES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1516,
                CatalogoCuentasModel::NO_CUENTA => 500120,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PREVISION SOCIAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1517,
                CatalogoCuentasModel::NO_CUENTA => 500121,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAPACITACION Y ENTRENAMIENTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1518,
                CatalogoCuentasModel::NO_CUENTA => 500122,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EDIFICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1519,
                CatalogoCuentasModel::NO_CUENTA => 500123,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1520,
                CatalogoCuentasModel::NO_CUENTA => 500124,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE TRANSPORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1521,
                CatalogoCuentasModel::NO_CUENTA => 500125,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE TALLER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1522,
                CatalogoCuentasModel::NO_CUENTA => 500126,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE TALLER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1523,
                CatalogoCuentasModel::NO_CUENTA => 500127,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE OFICINA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1524,
                CatalogoCuentasModel::NO_CUENTA => 500128,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE TRANSPORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1525,
                CatalogoCuentasModel::NO_CUENTA => 500129,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1526,
                CatalogoCuentasModel::NO_CUENTA => 500130,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE COMUNICACION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1527,
                CatalogoCuentasModel::NO_CUENTA => 500131,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AMORTIZACION EDIFICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1528,
                CatalogoCuentasModel::NO_CUENTA => 500132,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASOLINA NUEVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1529,
                CatalogoCuentasModel::NO_CUENTA => 500133,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASOLINA UNIDADES EN USO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1530,
                CatalogoCuentasModel::NO_CUENTA => 500134,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE PREVIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1531,
                CatalogoCuentasModel::NO_CUENTA => 500135,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HERRAMIENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1532,
                CatalogoCuentasModel::NO_CUENTA => 500136,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PUBLICIDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1533,
                CatalogoCuentasModel::NO_CUENTA => 500137,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PROMOCIONES ESPECIALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1534,
                CatalogoCuentasModel::NO_CUENTA => 500138,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CORTESIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1535,
                CatalogoCuentasModel::NO_CUENTA => 500139,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CUOTAS Y SUSCRIPCIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1536,
                CatalogoCuentasModel::NO_CUENTA => 500140,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DONATIVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1537,
                CatalogoCuentasModel::NO_CUENTA => 500141,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUBSIDIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1538,
                CatalogoCuentasModel::NO_CUENTA => 500142,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUBSIDIO DE SEGUROS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1539,
                CatalogoCuentasModel::NO_CUENTA => 500143,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RENTA DE LOCALES P.F.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1540,
                CatalogoCuentasModel::NO_CUENTA => 500144,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RENTA DE LOCALES P.M.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1541,
                CatalogoCuentasModel::NO_CUENTA => 500145,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TELEFONOS, FAX Y CORREOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1542,
                CatalogoCuentasModel::NO_CUENTA => 500146,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VIGILANCIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1543,
                CatalogoCuentasModel::NO_CUENTA => 500147,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CONSUMIBLES EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1544,
                CatalogoCuentasModel::NO_CUENTA => 500148,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS NO DEDUCIBLES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1545,
                CatalogoCuentasModel::NO_CUENTA => 500149,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PAPELERIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1546,
                CatalogoCuentasModel::NO_CUENTA => 500150,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SEGUROS Y FIANZAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1547,
                CatalogoCuentasModel::NO_CUENTA => 500151,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMPUESTOS Y DERECHOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1548,
                CatalogoCuentasModel::NO_CUENTA => 500152,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MULTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1549,
                CatalogoCuentasModel::NO_CUENTA => 500153,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACTUALIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1550,
                CatalogoCuentasModel::NO_CUENTA => 500154,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECARGOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1551,
                CatalogoCuentasModel::NO_CUENTA => 500155,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE VIAJE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1552,
                CatalogoCuentasModel::NO_CUENTA => 500156,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REPARACION DA¥OS SERVICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1553,
                CatalogoCuentasModel::NO_CUENTA => 500157,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COPIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1554,
                CatalogoCuentasModel::NO_CUENTA => 500158,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRASA Y LUBRICANTES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1555,
                CatalogoCuentasModel::NO_CUENTA => 500159,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PASAJES Y ESTACIONAMIENTOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1556,
                CatalogoCuentasModel::NO_CUENTA => 500160,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UTILES DE ASEO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1557,
                CatalogoCuentasModel::NO_CUENTA => 500161,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS FIN DE A¥O",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1558,
                CatalogoCuentasModel::NO_CUENTA => 500162,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECLUTAMIENTO Y SELECCION DE PERSONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1559,
                CatalogoCuentasModel::NO_CUENTA => 500163,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REDONDEOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1560,
                CatalogoCuentasModel::NO_CUENTA => 500164,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HONORARIOS PERSONAS FISICAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1561,
                CatalogoCuentasModel::NO_CUENTA => 500165,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HONORARIOS PERSONAS MORALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1562,
                CatalogoCuentasModel::NO_CUENTA => 500166,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ASESORIA ADMINISTRATIVA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1563,
                CatalogoCuentasModel::NO_CUENTA => 500167,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ASESORIA EN VENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1564,
                CatalogoCuentasModel::NO_CUENTA => 500168,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AUDITORIA EXTERNA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1565,
                CatalogoCuentasModel::NO_CUENTA => 500169,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DIVERSOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1566,
                CatalogoCuentasModel::NO_CUENTA => 500170,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SEGUROS DE TRASLADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1567,
                CatalogoCuentasModel::NO_CUENTA => 500171,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ARRENDAMIENTO DE EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1568,
                CatalogoCuentasModel::NO_CUENTA => 500172,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE TRASLADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1569,
                CatalogoCuentasModel::NO_CUENTA => 500173,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AGUA POTABLE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1570,
                CatalogoCuentasModel::NO_CUENTA => 500174,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ENERGIA ELECTRICA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1571,
                CatalogoCuentasModel::NO_CUENTA => 500175,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ATENCION A CLIENTES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1572,
                CatalogoCuentasModel::NO_CUENTA => 500177,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1573,
                CatalogoCuentasModel::NO_CUENTA => 500178,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1574,
                CatalogoCuentasModel::NO_CUENTA => 500200,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS EZEQUIEL MONTES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1575,
                CatalogoCuentasModel::NO_CUENTA => 500201,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES VENDEDORES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1576,
                CatalogoCuentasModel::NO_CUENTA => 500202,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES SEGUROS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1577,
                CatalogoCuentasModel::NO_CUENTA => 500203,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTRAS COMISIONES VENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1578,
                CatalogoCuentasModel::NO_CUENTA => 500204,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUELOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1579,
                CatalogoCuentasModel::NO_CUENTA => 500205,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRATIFICACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1580,
                CatalogoCuentasModel::NO_CUENTA => 500206,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES (SUELDOS)",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1581,
                CatalogoCuentasModel::NO_CUENTA => 500207,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TIEMPO EXTRA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1582,
                CatalogoCuentasModel::NO_CUENTA => 500208,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DOMINICAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1583,
                CatalogoCuentasModel::NO_CUENTA => 500209,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTROS ING. S\/SUELDOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1584,
                CatalogoCuentasModel::NO_CUENTA => 500210,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DE ANTIGUEDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1585,
                CatalogoCuentasModel::NO_CUENTA => 500211,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INDEMNIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1586,
                CatalogoCuentasModel::NO_CUENTA => 500212,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AGUINALDO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1587,
                CatalogoCuentasModel::NO_CUENTA => 500213,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VACACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1588,
                CatalogoCuentasModel::NO_CUENTA => 500214,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA VACACIONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1589,
                CatalogoCuentasModel::NO_CUENTA => 500215,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMSS PATRONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1590,
                CatalogoCuentasModel::NO_CUENTA => 500216,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1591,
                CatalogoCuentasModel::NO_CUENTA => 500217,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1592,
                CatalogoCuentasModel::NO_CUENTA => 500218,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CREDITO AL SALARIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1593,
                CatalogoCuentasModel::NO_CUENTA => 500219,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UNIFORMES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1594,
                CatalogoCuentasModel::NO_CUENTA => 500220,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PREVISION SOCIAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1595,
                CatalogoCuentasModel::NO_CUENTA => 500221,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAPACITACION Y ENTRENAMIENTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1596,
                CatalogoCuentasModel::NO_CUENTA => 500222,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE EDIFICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1597,
                CatalogoCuentasModel::NO_CUENTA => 500223,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1598,
                CatalogoCuentasModel::NO_CUENTA => 500224,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE TRANSPORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1599,
                CatalogoCuentasModel::NO_CUENTA => 500225,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE TALLER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1600,
                CatalogoCuentasModel::NO_CUENTA => 500226,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE TALLER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1601,
                CatalogoCuentasModel::NO_CUENTA => 500227,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE OFICINA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1602,
                CatalogoCuentasModel::NO_CUENTA => 500228,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE TRANSPORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1603,
                CatalogoCuentasModel::NO_CUENTA => 500229,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1604,
                CatalogoCuentasModel::NO_CUENTA => 500230,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE COMUNICACION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1605,
                CatalogoCuentasModel::NO_CUENTA => 500231,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AMORTIZACION DE EDIFICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1606,
                CatalogoCuentasModel::NO_CUENTA => 500232,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASOLINA NUEVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1607,
                CatalogoCuentasModel::NO_CUENTA => 500233,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASOLINA UNIDADES EN USO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1608,
                CatalogoCuentasModel::NO_CUENTA => 500234,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE PREVIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1609,
                CatalogoCuentasModel::NO_CUENTA => 500235,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HERRAMIENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1610,
                CatalogoCuentasModel::NO_CUENTA => 500236,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PUBLICIDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1611,
                CatalogoCuentasModel::NO_CUENTA => 500237,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PROMOCIONES ESPECIALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1612,
                CatalogoCuentasModel::NO_CUENTA => 500238,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CORTESIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1613,
                CatalogoCuentasModel::NO_CUENTA => 500239,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CUOTAS Y SUSCRIPCIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1614,
                CatalogoCuentasModel::NO_CUENTA => 500240,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DONATIVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1615,
                CatalogoCuentasModel::NO_CUENTA => 500241,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUBSIDIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1616,
                CatalogoCuentasModel::NO_CUENTA => 500242,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUBSIDIO DE SEGUROS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1617,
                CatalogoCuentasModel::NO_CUENTA => 500243,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RENTA DE LOCALES P.F.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1618,
                CatalogoCuentasModel::NO_CUENTA => 500244,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RENTA DE LOCALES P.M.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1619,
                CatalogoCuentasModel::NO_CUENTA => 500245,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TELEFONOS, FAX Y CORREOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1620,
                CatalogoCuentasModel::NO_CUENTA => 500246,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VIGILANCIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1621,
                CatalogoCuentasModel::NO_CUENTA => 500247,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CONSUMIBLES EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1622,
                CatalogoCuentasModel::NO_CUENTA => 500248,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS NO DEDUCIBLES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1623,
                CatalogoCuentasModel::NO_CUENTA => 500249,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PAPELERIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1624,
                CatalogoCuentasModel::NO_CUENTA => 500250,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SEGUROS Y FIANZAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1625,
                CatalogoCuentasModel::NO_CUENTA => 500251,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMPUESTOS Y DERECHOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1626,
                CatalogoCuentasModel::NO_CUENTA => 500252,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MULTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1627,
                CatalogoCuentasModel::NO_CUENTA => 500253,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACTUALIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1628,
                CatalogoCuentasModel::NO_CUENTA => 500254,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECARGOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1629,
                CatalogoCuentasModel::NO_CUENTA => 500255,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE VIAJE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1630,
                CatalogoCuentasModel::NO_CUENTA => 500256,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REPARACION DA¤OS SERVICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1631,
                CatalogoCuentasModel::NO_CUENTA => 500257,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COPIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1632,
                CatalogoCuentasModel::NO_CUENTA => 500258,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRASA Y LUBRICANTES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1633,
                CatalogoCuentasModel::NO_CUENTA => 500259,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PASAJES Y ESTACIONAMIENTOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1634,
                CatalogoCuentasModel::NO_CUENTA => 500260,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UTILES DE ASEO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1635,
                CatalogoCuentasModel::NO_CUENTA => 500261,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS FIN DE A¤O",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1636,
                CatalogoCuentasModel::NO_CUENTA => 500262,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECLUTAMIENTO Y SELECCION DE PERSONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1637,
                CatalogoCuentasModel::NO_CUENTA => 500263,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REDONDEOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1638,
                CatalogoCuentasModel::NO_CUENTA => 500264,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HONORARIOS PERSONAS FISICAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1639,
                CatalogoCuentasModel::NO_CUENTA => 500265,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HONORARIOS PERSONAS MORALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1640,
                CatalogoCuentasModel::NO_CUENTA => 500266,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ASESORIA ADMINISTRATIVA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1641,
                CatalogoCuentasModel::NO_CUENTA => 500267,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ASESORIA EN VENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1642,
                CatalogoCuentasModel::NO_CUENTA => 500268,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AUDITORIA EXTERNA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1643,
                CatalogoCuentasModel::NO_CUENTA => 500269,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DIVERSOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1644,
                CatalogoCuentasModel::NO_CUENTA => 500270,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SEGUROS DE TRASLADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1645,
                CatalogoCuentasModel::NO_CUENTA => 500271,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ARRENDAMIENTO DE EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1646,
                CatalogoCuentasModel::NO_CUENTA => 500272,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE TRASLADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1647,
                CatalogoCuentasModel::NO_CUENTA => 500273,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ALMACENAMIENTO UNIDADES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1648,
                CatalogoCuentasModel::NO_CUENTA => 500274,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ENERGIA ELECTRICA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1649,
                CatalogoCuentasModel::NO_CUENTA => 500275,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ATENCION A CLIENTES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1650,
                CatalogoCuentasModel::NO_CUENTA => 500279,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BURO DE CREDITO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1651,
                CatalogoCuentasModel::NO_CUENTA => 500300,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1652,
                CatalogoCuentasModel::NO_CUENTA => 500301,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES VENDEDORES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1653,
                CatalogoCuentasModel::NO_CUENTA => 500302,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES SEGUROS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1654,
                CatalogoCuentasModel::NO_CUENTA => 500303,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTRAS COMISIONES VENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1655,
                CatalogoCuentasModel::NO_CUENTA => 500304,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUELDOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1656,
                CatalogoCuentasModel::NO_CUENTA => 500305,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRATIFICACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1657,
                CatalogoCuentasModel::NO_CUENTA => 500306,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES (SUELDOS)",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1658,
                CatalogoCuentasModel::NO_CUENTA => 500307,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TIEMPO EXTRA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1659,
                CatalogoCuentasModel::NO_CUENTA => 500308,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DOMINICAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1660,
                CatalogoCuentasModel::NO_CUENTA => 500309,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTROS ING. S\/SUELDOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1661,
                CatalogoCuentasModel::NO_CUENTA => 500310,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DE ANTIGUEDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1662,
                CatalogoCuentasModel::NO_CUENTA => 500311,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INDEMNIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1663,
                CatalogoCuentasModel::NO_CUENTA => 500312,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AGUINALDO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1664,
                CatalogoCuentasModel::NO_CUENTA => 500313,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VACACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1665,
                CatalogoCuentasModel::NO_CUENTA => 500314,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA VACACIONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1666,
                CatalogoCuentasModel::NO_CUENTA => 500315,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMSS PATRONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1667,
                CatalogoCuentasModel::NO_CUENTA => 500316,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1668,
                CatalogoCuentasModel::NO_CUENTA => 500317,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1669,
                CatalogoCuentasModel::NO_CUENTA => 500318,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CREDITO AL SALARIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1670,
                CatalogoCuentasModel::NO_CUENTA => 500319,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UNIFORMES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1671,
                CatalogoCuentasModel::NO_CUENTA => 500320,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PREVISION SOCIAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1672,
                CatalogoCuentasModel::NO_CUENTA => 500321,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAPACITACION Y ENTRENAMIENTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1673,
                CatalogoCuentasModel::NO_CUENTA => 500322,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE EDIFICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1674,
                CatalogoCuentasModel::NO_CUENTA => 500323,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1675,
                CatalogoCuentasModel::NO_CUENTA => 500324,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE TRANSPORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1676,
                CatalogoCuentasModel::NO_CUENTA => 500325,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE TALLER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1677,
                CatalogoCuentasModel::NO_CUENTA => 500326,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE TALLER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1678,
                CatalogoCuentasModel::NO_CUENTA => 500327,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE OFICINA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1679,
                CatalogoCuentasModel::NO_CUENTA => 500328,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE TRANSPORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1680,
                CatalogoCuentasModel::NO_CUENTA => 500329,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1681,
                CatalogoCuentasModel::NO_CUENTA => 500330,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE COMUNICACION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1682,
                CatalogoCuentasModel::NO_CUENTA => 500331,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AMORTIZACION EDIFICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1683,
                CatalogoCuentasModel::NO_CUENTA => 500332,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASOLINA NUEVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1684,
                CatalogoCuentasModel::NO_CUENTA => 500333,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASOLINA UNIDADES EN USO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1685,
                CatalogoCuentasModel::NO_CUENTA => 500334,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE PREVIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1686,
                CatalogoCuentasModel::NO_CUENTA => 500335,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HERRAMIENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1687,
                CatalogoCuentasModel::NO_CUENTA => 500336,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PUBLICIDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1688,
                CatalogoCuentasModel::NO_CUENTA => 500337,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PROMOCIONES ESPECIALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1689,
                CatalogoCuentasModel::NO_CUENTA => 500338,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CORTESIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1690,
                CatalogoCuentasModel::NO_CUENTA => 500339,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CUOTAS Y SUSCRIPCIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1691,
                CatalogoCuentasModel::NO_CUENTA => 500340,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DONATIVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1692,
                CatalogoCuentasModel::NO_CUENTA => 500341,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUBSIDIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1693,
                CatalogoCuentasModel::NO_CUENTA => 500342,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUBSIDIO DE SEGUROS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1694,
                CatalogoCuentasModel::NO_CUENTA => 500343,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RENTA DE LOCALES P.F.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1695,
                CatalogoCuentasModel::NO_CUENTA => 500344,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RENTA DE LOCALES P.M.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1696,
                CatalogoCuentasModel::NO_CUENTA => 500345,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TELEFONOS, FAX Y CORREOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1697,
                CatalogoCuentasModel::NO_CUENTA => 500346,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VIGILANCIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1698,
                CatalogoCuentasModel::NO_CUENTA => 500347,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CONSUMIBLES EQ DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1699,
                CatalogoCuentasModel::NO_CUENTA => 500348,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS NO DEDUCIBLES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1700,
                CatalogoCuentasModel::NO_CUENTA => 500349,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PAPELERIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1701,
                CatalogoCuentasModel::NO_CUENTA => 500350,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SEGUROS Y FIANZAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1702,
                CatalogoCuentasModel::NO_CUENTA => 500351,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMPUESTOS Y DERECHOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1703,
                CatalogoCuentasModel::NO_CUENTA => 500352,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MULTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1704,
                CatalogoCuentasModel::NO_CUENTA => 500353,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACTUALIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1705,
                CatalogoCuentasModel::NO_CUENTA => 500354,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECARGOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1706,
                CatalogoCuentasModel::NO_CUENTA => 500355,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE VIAJE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1707,
                CatalogoCuentasModel::NO_CUENTA => 500356,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REPARACION DA¤OS SERVICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1708,
                CatalogoCuentasModel::NO_CUENTA => 500357,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COPIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1709,
                CatalogoCuentasModel::NO_CUENTA => 500358,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRASA Y LUBRICANTES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1710,
                CatalogoCuentasModel::NO_CUENTA => 500359,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PASAJES Y ESTACIONAMIENTOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1711,
                CatalogoCuentasModel::NO_CUENTA => 500360,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UTILES DE ASEO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1712,
                CatalogoCuentasModel::NO_CUENTA => 500361,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE FIN DE A¤O",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1713,
                CatalogoCuentasModel::NO_CUENTA => 500362,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECLUTAMIENTO Y SELECCION DE PERSONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1714,
                CatalogoCuentasModel::NO_CUENTA => 500363,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REDONDEOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1715,
                CatalogoCuentasModel::NO_CUENTA => 500364,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HONORARIOS PERSONAS FISICAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1716,
                CatalogoCuentasModel::NO_CUENTA => 500365,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HONORARIOS PERSONAS MORALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1717,
                CatalogoCuentasModel::NO_CUENTA => 500366,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ASESORIA ADMINISTRATIVA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1718,
                CatalogoCuentasModel::NO_CUENTA => 500367,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ASESORIA EN VENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1719,
                CatalogoCuentasModel::NO_CUENTA => 500368,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AUDITORIA EXTERNA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1720,
                CatalogoCuentasModel::NO_CUENTA => 500369,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DIVERSOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1721,
                CatalogoCuentasModel::NO_CUENTA => 500370,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SEGUROS DE TRASLADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1722,
                CatalogoCuentasModel::NO_CUENTA => 500371,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ARRENDAMIENTO DE EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1723,
                CatalogoCuentasModel::NO_CUENTA => 500372,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE TRASLADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1724,
                CatalogoCuentasModel::NO_CUENTA => 500373,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ALMACENAMIENTO DE UNIDADES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1725,
                CatalogoCuentasModel::NO_CUENTA => 500374,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AGUA POTABLE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1726,
                CatalogoCuentasModel::NO_CUENTA => 500375,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ATENCION A CLIENTES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1727,
                CatalogoCuentasModel::NO_CUENTA => 500376,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ENERGIA ELECTRICA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1728,
                CatalogoCuentasModel::NO_CUENTA => 500377,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FLETES Y MENSAJERIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1729,
                CatalogoCuentasModel::NO_CUENTA => 500379,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BURO DE CREDITO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1415
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1730,
                CatalogoCuentasModel::NO_CUENTA => 510000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE VENTA QUERETARO",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1731,
                CatalogoCuentasModel::NO_CUENTA => 510001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUELDOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1732,
                CatalogoCuentasModel::NO_CUENTA => 510002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BONO EFICIENCIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1733,
                CatalogoCuentasModel::NO_CUENTA => 510003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1734,
                CatalogoCuentasModel::NO_CUENTA => 510004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TIEMPO EXTRA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1735,
                CatalogoCuentasModel::NO_CUENTA => 510005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DOMINICAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1736,
                CatalogoCuentasModel::NO_CUENTA => 510006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DE ANTIGUEDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1737,
                CatalogoCuentasModel::NO_CUENTA => 510007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INDEMNIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1738,
                CatalogoCuentasModel::NO_CUENTA => 510008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AGUINALDO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1739,
                CatalogoCuentasModel::NO_CUENTA => 510009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VACACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1740,
                CatalogoCuentasModel::NO_CUENTA => 510010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA VACACIONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1741,
                CatalogoCuentasModel::NO_CUENTA => 510011,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMSS PATRONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1742,
                CatalogoCuentasModel::NO_CUENTA => 510012,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1743,
                CatalogoCuentasModel::NO_CUENTA => 510013,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1744,
                CatalogoCuentasModel::NO_CUENTA => 510014,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CREDITO AL SALARIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1745,
                CatalogoCuentasModel::NO_CUENTA => 510015,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UNIFORMES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1746,
                CatalogoCuentasModel::NO_CUENTA => 510016,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PREVISION SOCIAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1747,
                CatalogoCuentasModel::NO_CUENTA => 510017,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAPACITACION Y ENTRENAMIENTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1748,
                CatalogoCuentasModel::NO_CUENTA => 510018,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EDIFICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1749,
                CatalogoCuentasModel::NO_CUENTA => 510019,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1750,
                CatalogoCuentasModel::NO_CUENTA => 510020,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE TRANSPORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1751,
                CatalogoCuentasModel::NO_CUENTA => 510021,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE TALLER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1752,
                CatalogoCuentasModel::NO_CUENTA => 510022,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE TALLER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1753,
                CatalogoCuentasModel::NO_CUENTA => 510023,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE OFICINA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1754,
                CatalogoCuentasModel::NO_CUENTA => 510024,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DERECIACION DE EQUIPO DE TRANSPORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1755,
                CatalogoCuentasModel::NO_CUENTA => 510025,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1756,
                CatalogoCuentasModel::NO_CUENTA => 510026,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE COMUNICACION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1757,
                CatalogoCuentasModel::NO_CUENTA => 510027,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AMORTIZACION EDIFICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1758,
                CatalogoCuentasModel::NO_CUENTA => 510028,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASOLINA NUEVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1759,
                CatalogoCuentasModel::NO_CUENTA => 510029,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASOLINA UNIDADES EN USO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1760,
                CatalogoCuentasModel::NO_CUENTA => 510030,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE PREVIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1761,
                CatalogoCuentasModel::NO_CUENTA => 510031,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HERRAMIENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1762,
                CatalogoCuentasModel::NO_CUENTA => 510032,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PUBLICIDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1763,
                CatalogoCuentasModel::NO_CUENTA => 510033,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PROMOCIONES ESPECIALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1764,
                CatalogoCuentasModel::NO_CUENTA => 510034,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CORTESIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1765,
                CatalogoCuentasModel::NO_CUENTA => 510035,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CUOTAS Y SUSCRIPCIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1766,
                CatalogoCuentasModel::NO_CUENTA => 510036,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DONATIVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1767,
                CatalogoCuentasModel::NO_CUENTA => 510037,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RENTA DE LOCALES P.F.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1768,
                CatalogoCuentasModel::NO_CUENTA => 510038,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RENTA DE LOCALES P.M.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1769,
                CatalogoCuentasModel::NO_CUENTA => 510039,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TELEFONOS, FAX Y CORREOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1770,
                CatalogoCuentasModel::NO_CUENTA => 510040,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VIGILANCIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1771,
                CatalogoCuentasModel::NO_CUENTA => 510041,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CONSUMIBLES EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1772,
                CatalogoCuentasModel::NO_CUENTA => 510042,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS NO DEDUCIBLES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1773,
                CatalogoCuentasModel::NO_CUENTA => 510043,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PAPELERIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1774,
                CatalogoCuentasModel::NO_CUENTA => 510044,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SEGUROS Y FIANZAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1775,
                CatalogoCuentasModel::NO_CUENTA => 510045,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMPUESTOS Y DERECHOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1776,
                CatalogoCuentasModel::NO_CUENTA => 510046,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MULTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1777,
                CatalogoCuentasModel::NO_CUENTA => 510047,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACTUALIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1778,
                CatalogoCuentasModel::NO_CUENTA => 510048,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECARGOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1779,
                CatalogoCuentasModel::NO_CUENTA => 510049,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE VIAJE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1780,
                CatalogoCuentasModel::NO_CUENTA => 510050,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REPARACIONES DAÑOS SERVICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1781,
                CatalogoCuentasModel::NO_CUENTA => 510051,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COPIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1782,
                CatalogoCuentasModel::NO_CUENTA => 510052,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRASAS Y LUBRICANTES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1783,
                CatalogoCuentasModel::NO_CUENTA => 510053,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PASAJES Y ESTACIONAMIENTOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1784,
                CatalogoCuentasModel::NO_CUENTA => 510054,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UTILES DE ASEO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1785,
                CatalogoCuentasModel::NO_CUENTA => 510055,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE FIN DE AÑO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1786,
                CatalogoCuentasModel::NO_CUENTA => 510056,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECLUTAMIENTO Y SELECCION DE PERSONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1787,
                CatalogoCuentasModel::NO_CUENTA => 510057,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REDONDEOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1788,
                CatalogoCuentasModel::NO_CUENTA => 510058,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HONORARIOS PERSONAS FISICAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1789,
                CatalogoCuentasModel::NO_CUENTA => 510059,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HONORARIOS PERSONAS MORALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1790,
                CatalogoCuentasModel::NO_CUENTA => 510060,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ASESORIA ADMINISTRATIVA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1791,
                CatalogoCuentasModel::NO_CUENTA => 510061,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ASESORIA EN VENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1792,
                CatalogoCuentasModel::NO_CUENTA => 510062,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REVOCACION INCENTIVOS FORD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1793,
                CatalogoCuentasModel::NO_CUENTA => 510063,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DIVERSOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1794,
                CatalogoCuentasModel::NO_CUENTA => 510064,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SEGUROS DE TRASLADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1795,
                CatalogoCuentasModel::NO_CUENTA => 510065,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ARRENDAMIENTO DE EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1796,
                CatalogoCuentasModel::NO_CUENTA => 510066,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE TRASLADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1797,
                CatalogoCuentasModel::NO_CUENTA => 510067,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ALMACENAMIENTO DE UNIDADES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1798,
                CatalogoCuentasModel::NO_CUENTA => 510068,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AGUA POTABLE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1799,
                CatalogoCuentasModel::NO_CUENTA => 510069,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ATENCION A CLIENTES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1800,
                CatalogoCuentasModel::NO_CUENTA => 510070,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FLETES Y MENSAJERIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1801,
                CatalogoCuentasModel::NO_CUENTA => 510071,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ENERGIA ELECTRICA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1802,
                CatalogoCuentasModel::NO_CUENTA => 510072,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1803,
                CatalogoCuentasModel::NO_CUENTA => 510073,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BURO DE CREDITO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1804,
                CatalogoCuentasModel::NO_CUENTA => 510074,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUBSIDIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1805,
                CatalogoCuentasModel::NO_CUENTA => 510075,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACTUALIZACION DE GASTOS (REEXPRESION)",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1730
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1806,
                CatalogoCuentasModel::NO_CUENTA => 520000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE VENTA SAN JUAN DEL RIO",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1807,
                CatalogoCuentasModel::NO_CUENTA => 520001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUELDOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1808,
                CatalogoCuentasModel::NO_CUENTA => 520002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRATIFICACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1809,
                CatalogoCuentasModel::NO_CUENTA => 520003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1810,
                CatalogoCuentasModel::NO_CUENTA => 520004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TIEMPO EXTRA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1811,
                CatalogoCuentasModel::NO_CUENTA => 520005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DOMINICAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1812,
                CatalogoCuentasModel::NO_CUENTA => 520006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DE ANTIGUEDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1813,
                CatalogoCuentasModel::NO_CUENTA => 520007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INDEMNIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1814,
                CatalogoCuentasModel::NO_CUENTA => 520008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AGUINALDO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1815,
                CatalogoCuentasModel::NO_CUENTA => 520009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VACACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1816,
                CatalogoCuentasModel::NO_CUENTA => 520010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA VACACIONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1817,
                CatalogoCuentasModel::NO_CUENTA => 520011,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMSS PATRONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1818,
                CatalogoCuentasModel::NO_CUENTA => 520012,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1819,
                CatalogoCuentasModel::NO_CUENTA => 520013,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1820,
                CatalogoCuentasModel::NO_CUENTA => 520014,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CREDITO AL SALARIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1821,
                CatalogoCuentasModel::NO_CUENTA => 520015,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UNIFORMES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1822,
                CatalogoCuentasModel::NO_CUENTA => 520016,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PREVISION SOCIAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1823,
                CatalogoCuentasModel::NO_CUENTA => 520017,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAPACITACION Y ENTRENAMIENTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1824,
                CatalogoCuentasModel::NO_CUENTA => 520018,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EDIFICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1825,
                CatalogoCuentasModel::NO_CUENTA => 520019,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1826,
                CatalogoCuentasModel::NO_CUENTA => 520020,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE TRANSPORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1827,
                CatalogoCuentasModel::NO_CUENTA => 520021,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE TALLER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1828,
                CatalogoCuentasModel::NO_CUENTA => 520022,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE TALLER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1829,
                CatalogoCuentasModel::NO_CUENTA => 520023,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE OFICINA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1830,
                CatalogoCuentasModel::NO_CUENTA => 520024,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE TRANSPORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1831,
                CatalogoCuentasModel::NO_CUENTA => 520025,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1832,
                CatalogoCuentasModel::NO_CUENTA => 520026,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE COMUNICACION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1833,
                CatalogoCuentasModel::NO_CUENTA => 520027,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AMORTIZACION DE EDIFICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1834,
                CatalogoCuentasModel::NO_CUENTA => 520028,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASOLINA NUEVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1835,
                CatalogoCuentasModel::NO_CUENTA => 520029,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASOLINA UNIDADES EN USO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1836,
                CatalogoCuentasModel::NO_CUENTA => 520030,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE PREVIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1837,
                CatalogoCuentasModel::NO_CUENTA => 520031,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HERRAMIENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1838,
                CatalogoCuentasModel::NO_CUENTA => 520032,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PUBLICIDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1839,
                CatalogoCuentasModel::NO_CUENTA => 520033,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PROMOCIONES ESPECIALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1840,
                CatalogoCuentasModel::NO_CUENTA => 520034,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CORTESIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1841,
                CatalogoCuentasModel::NO_CUENTA => 520035,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CUOTAS Y SUSCRIPCIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1842,
                CatalogoCuentasModel::NO_CUENTA => 520036,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DONATIVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1843,
                CatalogoCuentasModel::NO_CUENTA => 520037,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RENTA DE LOCALES P.F.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1844,
                CatalogoCuentasModel::NO_CUENTA => 520038,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RENTA DE LOCALES P.M.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1845,
                CatalogoCuentasModel::NO_CUENTA => 520039,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TELEFONOS, FAX Y CORREOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1846,
                CatalogoCuentasModel::NO_CUENTA => 520040,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VIGILANCIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1847,
                CatalogoCuentasModel::NO_CUENTA => 520041,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CONSUMIBLES EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1848,
                CatalogoCuentasModel::NO_CUENTA => 520042,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS NO DEDUCIBLES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1849,
                CatalogoCuentasModel::NO_CUENTA => 520043,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PAPELERIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1850,
                CatalogoCuentasModel::NO_CUENTA => 520044,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SEGUROS Y FIANZAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1851,
                CatalogoCuentasModel::NO_CUENTA => 520045,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMPUESTOS Y DERECHOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1852,
                CatalogoCuentasModel::NO_CUENTA => 520046,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MULTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1853,
                CatalogoCuentasModel::NO_CUENTA => 520047,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACTUALIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1854,
                CatalogoCuentasModel::NO_CUENTA => 520048,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECARGOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1855,
                CatalogoCuentasModel::NO_CUENTA => 520049,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE VIAJE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1856,
                CatalogoCuentasModel::NO_CUENTA => 520050,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REPARACION DE DAÑOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1857,
                CatalogoCuentasModel::NO_CUENTA => 520051,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COPIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1858,
                CatalogoCuentasModel::NO_CUENTA => 520052,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRASA Y LUBRICANTES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1859,
                CatalogoCuentasModel::NO_CUENTA => 520053,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PASAJES Y ESTACIONAMIENTOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1860,
                CatalogoCuentasModel::NO_CUENTA => 520054,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UTILES DE ASEO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1861,
                CatalogoCuentasModel::NO_CUENTA => 520055,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS FIN DE AÑO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1862,
                CatalogoCuentasModel::NO_CUENTA => 520056,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECLUTAMIENTO Y SELECCION DE PERSONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1863,
                CatalogoCuentasModel::NO_CUENTA => 520057,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REDONDEOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1864,
                CatalogoCuentasModel::NO_CUENTA => 520058,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HONORARIOS PERSONAS FISICAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1865,
                CatalogoCuentasModel::NO_CUENTA => 520060,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ASESORIA ADMINISTRATIVA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1866,
                CatalogoCuentasModel::NO_CUENTA => 520061,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ASESORIA EN VENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1867,
                CatalogoCuentasModel::NO_CUENTA => 520062,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AUDITORIA EXTERNA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1868,
                CatalogoCuentasModel::NO_CUENTA => 520063,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DIVERSOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1869,
                CatalogoCuentasModel::NO_CUENTA => 520064,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SEGUROS DE TRASLADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1870,
                CatalogoCuentasModel::NO_CUENTA => 520065,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ARRENDAMIENTO DE EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1871,
                CatalogoCuentasModel::NO_CUENTA => 520066,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE TRASLADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1872,
                CatalogoCuentasModel::NO_CUENTA => 520067,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ALMACENAMIENTO UNIDADES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1873,
                CatalogoCuentasModel::NO_CUENTA => 520068,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AGUA POTABLE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1874,
                CatalogoCuentasModel::NO_CUENTA => 520069,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ATENCION A CLIENTES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1875,
                CatalogoCuentasModel::NO_CUENTA => 520070,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FLETES Y MENSAJERIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1876,
                CatalogoCuentasModel::NO_CUENTA => 520071,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ENERGIA ELECTRICA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1877,
                CatalogoCuentasModel::NO_CUENTA => 520072,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1878,
                CatalogoCuentasModel::NO_CUENTA => 520073,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BURO DE CREDITO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1879,
                CatalogoCuentasModel::NO_CUENTA => 520074,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUBSIDIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1880,
                CatalogoCuentasModel::NO_CUENTA => 520075,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACTUALIZACION DE GASTOS (REEXPRESION)",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1806
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1881,
                CatalogoCuentasModel::NO_CUENTA => 52259,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HONORARIOS PERSONAS MORALES",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1882,
                CatalogoCuentasModel::NO_CUENTA => 530000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE VENTA CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1883,
                CatalogoCuentasModel::NO_CUENTA => 530001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUELDOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1884,
                CatalogoCuentasModel::NO_CUENTA => 530002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRATIFICACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1885,
                CatalogoCuentasModel::NO_CUENTA => 530003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1886,
                CatalogoCuentasModel::NO_CUENTA => 530004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TIEMPO EXTRA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1887,
                CatalogoCuentasModel::NO_CUENTA => 530005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DOMINICAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1888,
                CatalogoCuentasModel::NO_CUENTA => 530006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DE ANTIGUEDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1889,
                CatalogoCuentasModel::NO_CUENTA => 530007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INDEMNIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1890,
                CatalogoCuentasModel::NO_CUENTA => 530008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AGUINALDO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1891,
                CatalogoCuentasModel::NO_CUENTA => 530009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VACACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1892,
                CatalogoCuentasModel::NO_CUENTA => 530010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA VACACIONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1893,
                CatalogoCuentasModel::NO_CUENTA => 530011,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMSS PATRONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1894,
                CatalogoCuentasModel::NO_CUENTA => 530012,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1895,
                CatalogoCuentasModel::NO_CUENTA => 530013,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1896,
                CatalogoCuentasModel::NO_CUENTA => 530014,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CREDITO AL SALARIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1897,
                CatalogoCuentasModel::NO_CUENTA => 530015,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UNIFORMES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1898,
                CatalogoCuentasModel::NO_CUENTA => 530016,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PREVISION SOCIAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1899,
                CatalogoCuentasModel::NO_CUENTA => 530017,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAPACITACION Y ENTRENAMIENTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1900,
                CatalogoCuentasModel::NO_CUENTA => 530018,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EDIFICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1901,
                CatalogoCuentasModel::NO_CUENTA => 530019,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1902,
                CatalogoCuentasModel::NO_CUENTA => 530020,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE TRANSPORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1903,
                CatalogoCuentasModel::NO_CUENTA => 530021,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE TALLER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1904,
                CatalogoCuentasModel::NO_CUENTA => 530022,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE TALLER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1905,
                CatalogoCuentasModel::NO_CUENTA => 530023,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE OFICINA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1906,
                CatalogoCuentasModel::NO_CUENTA => 530024,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE TRANSPORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1907,
                CatalogoCuentasModel::NO_CUENTA => 530025,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1908,
                CatalogoCuentasModel::NO_CUENTA => 530026,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE COMUNICACION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1909,
                CatalogoCuentasModel::NO_CUENTA => 530027,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AMORTIZACION EDFICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1910,
                CatalogoCuentasModel::NO_CUENTA => 530028,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASOLINA NUEVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1911,
                CatalogoCuentasModel::NO_CUENTA => 530029,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASOLINA UNIDADES EN USO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1912,
                CatalogoCuentasModel::NO_CUENTA => 530030,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE PREVIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1913,
                CatalogoCuentasModel::NO_CUENTA => 530031,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HERRAMIENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1914,
                CatalogoCuentasModel::NO_CUENTA => 530032,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PUBLICIDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1915,
                CatalogoCuentasModel::NO_CUENTA => 530033,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PROMOCIONES ESPECIALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1916,
                CatalogoCuentasModel::NO_CUENTA => 530034,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CORTESIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1917,
                CatalogoCuentasModel::NO_CUENTA => 530035,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CUOTAS Y SUSCRIPCION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1918,
                CatalogoCuentasModel::NO_CUENTA => 530036,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DONATIVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1919,
                CatalogoCuentasModel::NO_CUENTA => 530037,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RENTA DE LOCALES P.F.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1920,
                CatalogoCuentasModel::NO_CUENTA => 530038,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RENTA DE LOCALES P.M.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1921,
                CatalogoCuentasModel::NO_CUENTA => 530039,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TELEFONOS, FAX Y CORREOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1922,
                CatalogoCuentasModel::NO_CUENTA => 530040,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VIGILANCIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1923,
                CatalogoCuentasModel::NO_CUENTA => 530041,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CONSUMIBLES EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1924,
                CatalogoCuentasModel::NO_CUENTA => 530042,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS NO DEDUCIBLES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1925,
                CatalogoCuentasModel::NO_CUENTA => 530043,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PAPELERIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1926,
                CatalogoCuentasModel::NO_CUENTA => 530044,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SEGUROS Y FIANZAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1927,
                CatalogoCuentasModel::NO_CUENTA => 530045,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMPUESTOS Y DERECHOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1928,
                CatalogoCuentasModel::NO_CUENTA => 530046,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MULTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1929,
                CatalogoCuentasModel::NO_CUENTA => 530047,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACTUALIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1930,
                CatalogoCuentasModel::NO_CUENTA => 530048,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECARGOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1931,
                CatalogoCuentasModel::NO_CUENTA => 530049,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE VIAJE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1932,
                CatalogoCuentasModel::NO_CUENTA => 530050,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REPARACION DAÑOS SERVICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1933,
                CatalogoCuentasModel::NO_CUENTA => 530051,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COPIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1934,
                CatalogoCuentasModel::NO_CUENTA => 530052,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRASAS Y LUBRICANTES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1935,
                CatalogoCuentasModel::NO_CUENTA => 530053,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PASAJES Y ESTACIONAMIENTOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1936,
                CatalogoCuentasModel::NO_CUENTA => 530054,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UTILES DE ASEO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1937,
                CatalogoCuentasModel::NO_CUENTA => 530055,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS FIN DE AÑO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1938,
                CatalogoCuentasModel::NO_CUENTA => 530056,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECLUTAMIENTO Y SELECCION DE PERSONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1939,
                CatalogoCuentasModel::NO_CUENTA => 530057,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REDONDEOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1940,
                CatalogoCuentasModel::NO_CUENTA => 530058,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HONORARIOS PERSONAS FISICAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1941,
                CatalogoCuentasModel::NO_CUENTA => 530059,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HONORARIOS PERSONAS MORALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1942,
                CatalogoCuentasModel::NO_CUENTA => 530060,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ASESORIA ADMINISTRATIVA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1943,
                CatalogoCuentasModel::NO_CUENTA => 530061,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ASESORIA EN VENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1944,
                CatalogoCuentasModel::NO_CUENTA => 530062,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AUDITORIA EXTERNA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1945,
                CatalogoCuentasModel::NO_CUENTA => 530063,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DIVERSOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1946,
                CatalogoCuentasModel::NO_CUENTA => 530064,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SEGUROS DE TRASLADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1947,
                CatalogoCuentasModel::NO_CUENTA => 530065,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ARRENDAMIENTO DE EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1948,
                CatalogoCuentasModel::NO_CUENTA => 530066,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE TRASLADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1949,
                CatalogoCuentasModel::NO_CUENTA => 530067,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ALMACENAMIENTO DE UNIDADES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1950,
                CatalogoCuentasModel::NO_CUENTA => 530068,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AGUA POTABLE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1951,
                CatalogoCuentasModel::NO_CUENTA => 530069,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ATENCION A CLIENTES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1952,
                CatalogoCuentasModel::NO_CUENTA => 530070,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FLETES Y MENSAJERIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1953,
                CatalogoCuentasModel::NO_CUENTA => 530071,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ENERGIA ELECTRICA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1954,
                CatalogoCuentasModel::NO_CUENTA => 530072,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1955,
                CatalogoCuentasModel::NO_CUENTA => 530073,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BURO DE CREDITO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1956,
                CatalogoCuentasModel::NO_CUENTA => 530074,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUBSIDIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1957,
                CatalogoCuentasModel::NO_CUENTA => 530075,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACTUALIZACION DE GASTOS (REEXPRESION)",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1882
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1958,
                CatalogoCuentasModel::NO_CUENTA => 540000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE VENTA SEMINUEVOS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1959,
                CatalogoCuentasModel::NO_CUENTA => 540001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUELDOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1958
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1960,
                CatalogoCuentasModel::NO_CUENTA => 540002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRATIFICACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1958
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1961,
                CatalogoCuentasModel::NO_CUENTA => 540003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1958
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1962,
                CatalogoCuentasModel::NO_CUENTA => 540004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TIEMPO EXTRA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1958
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1963,
                CatalogoCuentasModel::NO_CUENTA => 540005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DOMINICAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1958
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1964,
                CatalogoCuentasModel::NO_CUENTA => 540006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DE ANTIGUEDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1958
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1965,
                CatalogoCuentasModel::NO_CUENTA => 540007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INDEMNIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1958
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1966,
                CatalogoCuentasModel::NO_CUENTA => 540008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AGUINALDO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1958
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1967,
                CatalogoCuentasModel::NO_CUENTA => 540009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VACACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1958
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1968,
                CatalogoCuentasModel::NO_CUENTA => 540010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA VACACIONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1958
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1969,
                CatalogoCuentasModel::NO_CUENTA => 540011,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMSS PATRONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1958
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1970,
                CatalogoCuentasModel::NO_CUENTA => 540012,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1958
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1971,
                CatalogoCuentasModel::NO_CUENTA => 540013,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1958
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1972,
                CatalogoCuentasModel::NO_CUENTA => 540014,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CREDITO AL SALARIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1958
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1973,
                CatalogoCuentasModel::NO_CUENTA => 540015,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UNIFORMES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1958
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1974,
                CatalogoCuentasModel::NO_CUENTA => 540016,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PREVISION SOCIAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1958
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1975,
                CatalogoCuentasModel::NO_CUENTA => 540017,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAPACITACION Y ENTRENAMIENTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1958
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1976,
                CatalogoCuentasModel::NO_CUENTA => 540018,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PUBLICIDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1958
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1977,
                CatalogoCuentasModel::NO_CUENTA => 540019,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PROMOCIONES ESPECIALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1958
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1978,
                CatalogoCuentasModel::NO_CUENTA => 540020,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CORTESIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1958
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1979,
                CatalogoCuentasModel::NO_CUENTA => 540021,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CUOTAS Y SUSCRIPCIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1958
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1980,
                CatalogoCuentasModel::NO_CUENTA => 540022,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS NO DEDUCIBLES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1958
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1981,
                CatalogoCuentasModel::NO_CUENTA => 540023,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PAPELERIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1958
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1982,
                CatalogoCuentasModel::NO_CUENTA => 540024,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE VIAJE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1958
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1983,
                CatalogoCuentasModel::NO_CUENTA => 540025,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECLUTAMIENTO Y SELECCION DE PERSONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1958
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1984,
                CatalogoCuentasModel::NO_CUENTA => 540026,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DIVERSOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1958
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1985,
                CatalogoCuentasModel::NO_CUENTA => 540027,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ATENCION A CLIENTES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1958
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1986,
                CatalogoCuentasModel::NO_CUENTA => 540028,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE ACONDICIONAMIENTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1958
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1987,
                CatalogoCuentasModel::NO_CUENTA => 550000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UNIDADES SEMINUEVAS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1988,
                CatalogoCuentasModel::NO_CUENTA => 550001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUELDOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1987
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1989,
                CatalogoCuentasModel::NO_CUENTA => 550002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES VENDEDORES EXTERNOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1987
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1990,
                CatalogoCuentasModel::NO_CUENTA => 550003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES A EMPLEADOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1987
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1991,
                CatalogoCuentasModel::NO_CUENTA => 550004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES EXTERNOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1987
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1992,
                CatalogoCuentasModel::NO_CUENTA => 550005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1987
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1993,
                CatalogoCuentasModel::NO_CUENTA => 550006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMSS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1987
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1994,
                CatalogoCuentasModel::NO_CUENTA => 550007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA VACACIONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1987
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1995,
                CatalogoCuentasModel::NO_CUENTA => 550008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTROS BENEFICIOS A EMPLEADOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1987
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1996,
                CatalogoCuentasModel::NO_CUENTA => 550009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1987
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1997,
                CatalogoCuentasModel::NO_CUENTA => 550010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1987
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1998,
                CatalogoCuentasModel::NO_CUENTA => 550011,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BONO POR EFICIENCIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1987
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 1999,
                CatalogoCuentasModel::NO_CUENTA => 550012,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ENTRENAMIENTO Y CAPACITACION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1987
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2000,
                CatalogoCuentasModel::NO_CUENTA => 550013,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PUBLICIDAD Y PROMOCION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1987
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2001,
                CatalogoCuentasModel::NO_CUENTA => 550014,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE TRASLADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1987
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2002,
                CatalogoCuentasModel::NO_CUENTA => 550015,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PASAJES Y ESTACIONAMIENTOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1987
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2003,
                CatalogoCuentasModel::NO_CUENTA => 550016,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMPUESTOS Y DERECHOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1987
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2004,
                CatalogoCuentasModel::NO_CUENTA => 550017,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CORTESIA A CLIENTES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1987
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2005,
                CatalogoCuentasModel::NO_CUENTA => 550018,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASOLINA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1987
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2006,
                CatalogoCuentasModel::NO_CUENTA => 550019,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INTERESES PLAN PISO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1987
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2007,
                CatalogoCuentasModel::NO_CUENTA => 550020,
                CatalogoCuentasModel::NOMBRE_CUENTA => "1% APERTURA PAGADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1987
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2008,
                CatalogoCuentasModel::NO_CUENTA => 550021,
                CatalogoCuentasModel::NOMBRE_CUENTA => "1% APERTURA COBRADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1987
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2009,
                CatalogoCuentasModel::NO_CUENTA => 550022,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MTTO. EPO. DE TRANSPORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1987
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2010,
                CatalogoCuentasModel::NO_CUENTA => 550023,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BONO POR EFICIENCIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1987
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2011,
                CatalogoCuentasModel::NO_CUENTA => 550025,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AGUINALDO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1987
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2012,
                CatalogoCuentasModel::NO_CUENTA => 550026,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VACACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1987
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2013,
                CatalogoCuentasModel::NO_CUENTA => 550027,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PAPELERIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1987
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2014,
                CatalogoCuentasModel::NO_CUENTA => 550028,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AGUINALDO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1987
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2015,
                CatalogoCuentasModel::NO_CUENTA => 550029,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VACACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1987
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2016,
                CatalogoCuentasModel::NO_CUENTA => 550030,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA VACACIONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1987
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2017,
                CatalogoCuentasModel::NO_CUENTA => 550031,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DE ANTIGUEDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1987
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2018,
                CatalogoCuentasModel::NO_CUENTA => 550043,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RENTA DE LOCALES P.F.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1987
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2019,
                CatalogoCuentasModel::NO_CUENTA => 557003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MTTO. U. SERVICIO LINCOLN",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 1987
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2020,
                CatalogoCuentasModel::NO_CUENTA => 560000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REEXPRESION GTOS VTAS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2021,
                CatalogoCuentasModel::NO_CUENTA => 600000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE VENTA REFACCIONES",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2022,
                CatalogoCuentasModel::NO_CUENTA => 600001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUELDOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2021
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2023,
                CatalogoCuentasModel::NO_CUENTA => 600002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES VENDEDORES EXTERNOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2021
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2024,
                CatalogoCuentasModel::NO_CUENTA => 600003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES A EMPLEADOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2021
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2025,
                CatalogoCuentasModel::NO_CUENTA => 600004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES EXTERNOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2021
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2026,
                CatalogoCuentasModel::NO_CUENTA => 600005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2021
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2027,
                CatalogoCuentasModel::NO_CUENTA => 600006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMSS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2021
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2028,
                CatalogoCuentasModel::NO_CUENTA => 600007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA VACACIONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2021
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2029,
                CatalogoCuentasModel::NO_CUENTA => 600008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTROS BENEFICIOS A EMPLEADOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2021
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2030,
                CatalogoCuentasModel::NO_CUENTA => 600009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2021
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2031,
                CatalogoCuentasModel::NO_CUENTA => 600010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2021
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2032,
                CatalogoCuentasModel::NO_CUENTA => 600011,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HONORARIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2021
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2033,
                CatalogoCuentasModel::NO_CUENTA => 600012,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PUBLICIDAD Y PROMOCION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2021
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2034,
                CatalogoCuentasModel::NO_CUENTA => 600013,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASOLINA Y LUBRICANTES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2021
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2035,
                CatalogoCuentasModel::NO_CUENTA => 600014,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CORTESIA A CLIENTES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2021
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2036,
                CatalogoCuentasModel::NO_CUENTA => 600015,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMPTO. 1% S\/ADQUIS. VEH. USADOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2021
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2037,
                CatalogoCuentasModel::NO_CUENTA => 600016,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MTTO. EPO. DE TRANSPORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2021
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2038,
                CatalogoCuentasModel::NO_CUENTA => 600017,
                CatalogoCuentasModel::NOMBRE_CUENTA => "1% APERTURA DE CREDITO PAGADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2021
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2039,
                CatalogoCuentasModel::NO_CUENTA => 600018,
                CatalogoCuentasModel::NOMBRE_CUENTA => "1% APERTURA DE CREDITO COBRADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2021
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2040,
                CatalogoCuentasModel::NO_CUENTA => 600019,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MULTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2021
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2041,
                CatalogoCuentasModel::NO_CUENTA => 600020,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UTILES Y HERRAMIENTAS USADOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2021
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2042,
                CatalogoCuentasModel::NO_CUENTA => 600025,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DIVERSOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2021
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2043,
                CatalogoCuentasModel::NO_CUENTA => 600026,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TELEFONO, TELEGRAFO Y CORREO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2021
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2044,
                CatalogoCuentasModel::NO_CUENTA => 600049,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UNIFORMES PARA PERSONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2021
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2045,
                CatalogoCuentasModel::NO_CUENTA => 610000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE VENTA REFACCIONES QUERETARO",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2046,
                CatalogoCuentasModel::NO_CUENTA => 610001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUELDOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2047,
                CatalogoCuentasModel::NO_CUENTA => 610002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BONO EFICIENCIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2048,
                CatalogoCuentasModel::NO_CUENTA => 610003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2049,
                CatalogoCuentasModel::NO_CUENTA => 610004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TIEMPO EXTRA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2050,
                CatalogoCuentasModel::NO_CUENTA => 610005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DOMINICAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2051,
                CatalogoCuentasModel::NO_CUENTA => 610006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DE ANTIGUEDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2052,
                CatalogoCuentasModel::NO_CUENTA => 610007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INDEMNIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2053,
                CatalogoCuentasModel::NO_CUENTA => 610008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AGUINALDO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2054,
                CatalogoCuentasModel::NO_CUENTA => 610009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VACACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2055,
                CatalogoCuentasModel::NO_CUENTA => 610010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA VACACIONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2056,
                CatalogoCuentasModel::NO_CUENTA => 610011,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMSS PATRONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2057,
                CatalogoCuentasModel::NO_CUENTA => 610012,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2058,
                CatalogoCuentasModel::NO_CUENTA => 610013,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2059,
                CatalogoCuentasModel::NO_CUENTA => 610014,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CREDITO AL SALARIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2060,
                CatalogoCuentasModel::NO_CUENTA => 610015,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UNIFORMES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2061,
                CatalogoCuentasModel::NO_CUENTA => 610016,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PREVISION SOCIAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2062,
                CatalogoCuentasModel::NO_CUENTA => 610017,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAPACITACION Y ENTRENAMIENTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2063,
                CatalogoCuentasModel::NO_CUENTA => 610018,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EDIFICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2064,
                CatalogoCuentasModel::NO_CUENTA => 610019,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2065,
                CatalogoCuentasModel::NO_CUENTA => 610020,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE TRANSPORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2066,
                CatalogoCuentasModel::NO_CUENTA => 610021,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE TALLER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2067,
                CatalogoCuentasModel::NO_CUENTA => 610022,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE TALLER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2068,
                CatalogoCuentasModel::NO_CUENTA => 610023,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECAICION DE EQUIPO DE OFICINA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2069,
                CatalogoCuentasModel::NO_CUENTA => 610024,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE TRANSPORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2070,
                CatalogoCuentasModel::NO_CUENTA => 610025,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2071,
                CatalogoCuentasModel::NO_CUENTA => 610026,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE COMUNICACION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2072,
                CatalogoCuentasModel::NO_CUENTA => 610027,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AMORTIZACION EDIFICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2073,
                CatalogoCuentasModel::NO_CUENTA => 610028,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASOLINA NUEVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2074,
                CatalogoCuentasModel::NO_CUENTA => 610029,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASOLINA UNIDADES EN USO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2075,
                CatalogoCuentasModel::NO_CUENTA => 610030,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE PREVIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2076,
                CatalogoCuentasModel::NO_CUENTA => 610031,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HERRAMIENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2077,
                CatalogoCuentasModel::NO_CUENTA => 610032,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PUBLICIDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2078,
                CatalogoCuentasModel::NO_CUENTA => 610033,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PROMOCIONES ESPECIALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2079,
                CatalogoCuentasModel::NO_CUENTA => 610034,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CORTESIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2080,
                CatalogoCuentasModel::NO_CUENTA => 610035,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CUOTAS Y SUSCRIPCIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2081,
                CatalogoCuentasModel::NO_CUENTA => 610036,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DONATIVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2082,
                CatalogoCuentasModel::NO_CUENTA => 610037,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RENTA DE LOCALES P.F.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2083,
                CatalogoCuentasModel::NO_CUENTA => 610038,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RENTA DE LOCALES P.M.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2084,
                CatalogoCuentasModel::NO_CUENTA => 610039,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TELEFONOS, FAX Y CORREOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2085,
                CatalogoCuentasModel::NO_CUENTA => 610040,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VIGILANCIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2086,
                CatalogoCuentasModel::NO_CUENTA => 610041,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CONSUMIBLES EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2087,
                CatalogoCuentasModel::NO_CUENTA => 610042,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS NO DEDUCIBLES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2088,
                CatalogoCuentasModel::NO_CUENTA => 610043,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PAPELERIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2089,
                CatalogoCuentasModel::NO_CUENTA => 610044,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SEGUROS Y FIANZAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2090,
                CatalogoCuentasModel::NO_CUENTA => 610045,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMPUESTOS Y DERECHOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2091,
                CatalogoCuentasModel::NO_CUENTA => 610046,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MULTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2092,
                CatalogoCuentasModel::NO_CUENTA => 610047,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACTUALIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2093,
                CatalogoCuentasModel::NO_CUENTA => 610048,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECARGOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2094,
                CatalogoCuentasModel::NO_CUENTA => 610049,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE VIAJE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2095,
                CatalogoCuentasModel::NO_CUENTA => 610050,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REPARACION DAÑOS SERVICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2096,
                CatalogoCuentasModel::NO_CUENTA => 610051,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COPIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2097,
                CatalogoCuentasModel::NO_CUENTA => 610052,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRASAS Y LUBRICANTES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2098,
                CatalogoCuentasModel::NO_CUENTA => 610053,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PASAJES Y ESTACIONAMIENTOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2099,
                CatalogoCuentasModel::NO_CUENTA => 610054,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UTILES DE ASEO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2100,
                CatalogoCuentasModel::NO_CUENTA => 610055,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE FIN DE AÑO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2101,
                CatalogoCuentasModel::NO_CUENTA => 610056,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECLUTAMIENTO Y SELECCION DE PERSONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2102,
                CatalogoCuentasModel::NO_CUENTA => 610057,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REDONDEOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2103,
                CatalogoCuentasModel::NO_CUENTA => 610058,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HNORARIOS PERSONAS FISICAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2104,
                CatalogoCuentasModel::NO_CUENTA => 610059,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HONORARIOS PERSONAS MORALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2105,
                CatalogoCuentasModel::NO_CUENTA => 610060,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ASESORIA ADMINISTRATIVA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2106,
                CatalogoCuentasModel::NO_CUENTA => 610061,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ASESORIA EN VENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2107,
                CatalogoCuentasModel::NO_CUENTA => 610062,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AUDITORIA EXTERNA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2108,
                CatalogoCuentasModel::NO_CUENTA => 610063,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DIVERSOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2109,
                CatalogoCuentasModel::NO_CUENTA => 610064,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SEGUROS DE TRASLADOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2110,
                CatalogoCuentasModel::NO_CUENTA => 610065,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ARRENDAMIENTO DE EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2111,
                CatalogoCuentasModel::NO_CUENTA => 610066,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE TRASLADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2112,
                CatalogoCuentasModel::NO_CUENTA => 610067,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ALMACENAMIENTO UNIDADES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2113,
                CatalogoCuentasModel::NO_CUENTA => 610068,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AGUA POTABLE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2114,
                CatalogoCuentasModel::NO_CUENTA => 610069,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ATENCION A CLIENTES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2115,
                CatalogoCuentasModel::NO_CUENTA => 610070,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FLETES Y MENSAJERIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2116,
                CatalogoCuentasModel::NO_CUENTA => 610071,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ENERGIA ELECTRICA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2117,
                CatalogoCuentasModel::NO_CUENTA => 610072,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2118,
                CatalogoCuentasModel::NO_CUENTA => 610073,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BURO DE CREDITO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2119,
                CatalogoCuentasModel::NO_CUENTA => 610074,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACTUALIZACION DE GASTOS (REEXPRESION)",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2045
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2120,
                CatalogoCuentasModel::NO_CUENTA => 620000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE VENTA REFACCIONES QUERETARO",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2121,
                CatalogoCuentasModel::NO_CUENTA => 620001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUELDOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2122,
                CatalogoCuentasModel::NO_CUENTA => 620002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BONO EFICIENCIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2123,
                CatalogoCuentasModel::NO_CUENTA => 620003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2124,
                CatalogoCuentasModel::NO_CUENTA => 620004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TIEPO EXTRA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2125,
                CatalogoCuentasModel::NO_CUENTA => 620005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DOMINICAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2126,
                CatalogoCuentasModel::NO_CUENTA => 620006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DE ANTIGUEDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2127,
                CatalogoCuentasModel::NO_CUENTA => 620007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INDEMNIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2128,
                CatalogoCuentasModel::NO_CUENTA => 620008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AGUINALDO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2129,
                CatalogoCuentasModel::NO_CUENTA => 620009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VACACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2130,
                CatalogoCuentasModel::NO_CUENTA => 620010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PROMA VACACIONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2131,
                CatalogoCuentasModel::NO_CUENTA => 620011,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMSS PATRONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2132,
                CatalogoCuentasModel::NO_CUENTA => 620012,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2133,
                CatalogoCuentasModel::NO_CUENTA => 620013,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2134,
                CatalogoCuentasModel::NO_CUENTA => 620014,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CREDITO AL SALARIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2135,
                CatalogoCuentasModel::NO_CUENTA => 620015,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UNIFORMES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2136,
                CatalogoCuentasModel::NO_CUENTA => 620016,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PREVISION SOCIAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2137,
                CatalogoCuentasModel::NO_CUENTA => 620017,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAPACITACION Y ENTRENAMIENTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2138,
                CatalogoCuentasModel::NO_CUENTA => 620018,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EDIFICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2139,
                CatalogoCuentasModel::NO_CUENTA => 620019,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2140,
                CatalogoCuentasModel::NO_CUENTA => 620020,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE TRANSPORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2141,
                CatalogoCuentasModel::NO_CUENTA => 620021,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE TALLER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2142,
                CatalogoCuentasModel::NO_CUENTA => 620022,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE TALLER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2143,
                CatalogoCuentasModel::NO_CUENTA => 620023,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE OFICINA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2144,
                CatalogoCuentasModel::NO_CUENTA => 620024,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE TRANSPORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2145,
                CatalogoCuentasModel::NO_CUENTA => 620025,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2146,
                CatalogoCuentasModel::NO_CUENTA => 620026,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE COMUNICACION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2147,
                CatalogoCuentasModel::NO_CUENTA => 620027,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AMORTIZACION EDIFICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2148,
                CatalogoCuentasModel::NO_CUENTA => 620028,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASOLINA NUEVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2149,
                CatalogoCuentasModel::NO_CUENTA => 620029,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASOLINA UNIDADES EN USO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2150,
                CatalogoCuentasModel::NO_CUENTA => 620030,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE PREVIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2151,
                CatalogoCuentasModel::NO_CUENTA => 620031,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HERRAMIENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2152,
                CatalogoCuentasModel::NO_CUENTA => 620032,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PUBLICIDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2153,
                CatalogoCuentasModel::NO_CUENTA => 620033,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PROMOCIONES ESPECIALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2154,
                CatalogoCuentasModel::NO_CUENTA => 620034,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CORTESIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2155,
                CatalogoCuentasModel::NO_CUENTA => 620035,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CUOTAS Y SUSCRIPCIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2156,
                CatalogoCuentasModel::NO_CUENTA => 620036,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DONATIVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2157,
                CatalogoCuentasModel::NO_CUENTA => 620038,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RENTA DE LOCALES P.M.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2158,
                CatalogoCuentasModel::NO_CUENTA => 620039,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TELEFONOS, FAX Y CORREOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2159,
                CatalogoCuentasModel::NO_CUENTA => 620040,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VIGILANCIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2160,
                CatalogoCuentasModel::NO_CUENTA => 620041,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CONSUMIBLES EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2161,
                CatalogoCuentasModel::NO_CUENTA => 620042,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS NO DEDUCIBLES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2162,
                CatalogoCuentasModel::NO_CUENTA => 620043,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PAPELERIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2163,
                CatalogoCuentasModel::NO_CUENTA => 620044,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SEGUROS Y FIANZAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2164,
                CatalogoCuentasModel::NO_CUENTA => 620045,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMPUESTOS Y DERECHOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2165,
                CatalogoCuentasModel::NO_CUENTA => 620046,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MULTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2166,
                CatalogoCuentasModel::NO_CUENTA => 620047,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACTUALIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2167,
                CatalogoCuentasModel::NO_CUENTA => 620048,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECARGOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2168,
                CatalogoCuentasModel::NO_CUENTA => 620049,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE VIAJE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2169,
                CatalogoCuentasModel::NO_CUENTA => 620050,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REPARACION DAÑOS SERVICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2170,
                CatalogoCuentasModel::NO_CUENTA => 620051,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COPIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2171,
                CatalogoCuentasModel::NO_CUENTA => 620052,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRASAS Y LUBRICANTES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2172,
                CatalogoCuentasModel::NO_CUENTA => 620053,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PASAJES Y ESTACIONAMIENTOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2173,
                CatalogoCuentasModel::NO_CUENTA => 620054,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UTILES DE ASEO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2174,
                CatalogoCuentasModel::NO_CUENTA => 620055,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE FIN DE AÑO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2175,
                CatalogoCuentasModel::NO_CUENTA => 620056,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECLUTAMIENTO Y SELECCION DE PERSONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2176,
                CatalogoCuentasModel::NO_CUENTA => 620057,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REDONDEOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2177,
                CatalogoCuentasModel::NO_CUENTA => 620058,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HONORARIOS PERSONAS FISICAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2178,
                CatalogoCuentasModel::NO_CUENTA => 620059,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HONORARIOS PERSONAS MORALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2179,
                CatalogoCuentasModel::NO_CUENTA => 620060,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ASESORIA ADMINISTRATIVA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2180,
                CatalogoCuentasModel::NO_CUENTA => 620061,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AESEORIA EN VENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2181,
                CatalogoCuentasModel::NO_CUENTA => 620062,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AUDITORIA EXTERNA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2182,
                CatalogoCuentasModel::NO_CUENTA => 620063,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DIVERSOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2183,
                CatalogoCuentasModel::NO_CUENTA => 620064,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SEGUROS DE TRASLADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2184,
                CatalogoCuentasModel::NO_CUENTA => 620065,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ARRENDAMIENTO DE EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2185,
                CatalogoCuentasModel::NO_CUENTA => 620066,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE TRASLADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2186,
                CatalogoCuentasModel::NO_CUENTA => 620067,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ALMACENAMIENTO DE UNIDADES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2187,
                CatalogoCuentasModel::NO_CUENTA => 620068,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AGUA POTABLE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2188,
                CatalogoCuentasModel::NO_CUENTA => 620069,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ATENCION A CLIENTES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2189,
                CatalogoCuentasModel::NO_CUENTA => 620070,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FLETES Y MENSAJERIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2190,
                CatalogoCuentasModel::NO_CUENTA => 620071,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ENERGIA ELECTRICA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2191,
                CatalogoCuentasModel::NO_CUENTA => 620072,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2192,
                CatalogoCuentasModel::NO_CUENTA => 620073,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BURO DE CREDITO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2193,
                CatalogoCuentasModel::NO_CUENTA => 620074,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACTUALIZACION DE GASTOS (REEXPRESION)",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2120
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2194,
                CatalogoCuentasModel::NO_CUENTA => 700000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE VENTA DE TALLER SERVICIO",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2195,
                CatalogoCuentasModel::NO_CUENTA => 700001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES VENDEDORES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2196,
                CatalogoCuentasModel::NO_CUENTA => 700002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES SEGUROS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2197,
                CatalogoCuentasModel::NO_CUENTA => 700003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTRAS COMISIONES VENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2198,
                CatalogoCuentasModel::NO_CUENTA => 700004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUELDOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2199,
                CatalogoCuentasModel::NO_CUENTA => 700005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRATIFICACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2200,
                CatalogoCuentasModel::NO_CUENTA => 700006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES (SUELDOS)",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2201,
                CatalogoCuentasModel::NO_CUENTA => 700007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TIEMPO EXTRA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2202,
                CatalogoCuentasModel::NO_CUENTA => 700008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DOMINICAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2203,
                CatalogoCuentasModel::NO_CUENTA => 700009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTROS ING. S\/SUELDOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2204,
                CatalogoCuentasModel::NO_CUENTA => 700010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DE ANTIGUEDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2205,
                CatalogoCuentasModel::NO_CUENTA => 700011,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INDEMNIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2206,
                CatalogoCuentasModel::NO_CUENTA => 700012,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AGUINALDO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2207,
                CatalogoCuentasModel::NO_CUENTA => 700013,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VACACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2208,
                CatalogoCuentasModel::NO_CUENTA => 700014,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA VACACIONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2209,
                CatalogoCuentasModel::NO_CUENTA => 700015,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMSS PATRONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2210,
                CatalogoCuentasModel::NO_CUENTA => 700016,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2211,
                CatalogoCuentasModel::NO_CUENTA => 700017,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2212,
                CatalogoCuentasModel::NO_CUENTA => 700018,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CREDITO AL SALARIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2213,
                CatalogoCuentasModel::NO_CUENTA => 700019,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UNIFORMES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2214,
                CatalogoCuentasModel::NO_CUENTA => 700020,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PREVISION SOCIAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2215,
                CatalogoCuentasModel::NO_CUENTA => 700021,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAPACITACION Y ENTRENAMIENTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2216,
                CatalogoCuentasModel::NO_CUENTA => 700022,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE EDIFICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2217,
                CatalogoCuentasModel::NO_CUENTA => 700023,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2218,
                CatalogoCuentasModel::NO_CUENTA => 700024,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE TRANSPORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2219,
                CatalogoCuentasModel::NO_CUENTA => 700025,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE TALLER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2220,
                CatalogoCuentasModel::NO_CUENTA => 700026,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE TALLER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2221,
                CatalogoCuentasModel::NO_CUENTA => 700027,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE OFICINA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2222,
                CatalogoCuentasModel::NO_CUENTA => 700028,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE TRANSPORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2223,
                CatalogoCuentasModel::NO_CUENTA => 700029,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2224,
                CatalogoCuentasModel::NO_CUENTA => 700030,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE COMUNICACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2225,
                CatalogoCuentasModel::NO_CUENTA => 700031,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AMORTIZACION EDIFICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2226,
                CatalogoCuentasModel::NO_CUENTA => 700032,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASOLINA NUEVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2227,
                CatalogoCuentasModel::NO_CUENTA => 700033,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASOLINA UNIDADES EN USO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2228,
                CatalogoCuentasModel::NO_CUENTA => 700034,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE PREVIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2229,
                CatalogoCuentasModel::NO_CUENTA => 700035,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HERRAMIENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2230,
                CatalogoCuentasModel::NO_CUENTA => 700036,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PUBLICIDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2231,
                CatalogoCuentasModel::NO_CUENTA => 700037,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PROMOCIONES ESPECIALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2232,
                CatalogoCuentasModel::NO_CUENTA => 700038,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CORTESIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2233,
                CatalogoCuentasModel::NO_CUENTA => 700039,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CUOTAS Y SUSCRIPCIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2234,
                CatalogoCuentasModel::NO_CUENTA => 700040,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DONATIVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2235,
                CatalogoCuentasModel::NO_CUENTA => 700041,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUBSIDIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2236,
                CatalogoCuentasModel::NO_CUENTA => 700042,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUBSIDIO DE SEGUROS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2237,
                CatalogoCuentasModel::NO_CUENTA => 700043,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RENTA DE LOCALES P.F.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2238,
                CatalogoCuentasModel::NO_CUENTA => 700044,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RENTA DE LOCALES P.M.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2239,
                CatalogoCuentasModel::NO_CUENTA => 700045,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TELEFONOS, FAX Y CORREOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2240,
                CatalogoCuentasModel::NO_CUENTA => 700046,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VIGILANCIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2241,
                CatalogoCuentasModel::NO_CUENTA => 700047,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CONSUMIBLES EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2242,
                CatalogoCuentasModel::NO_CUENTA => 700048,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS NO DEDUCIBLES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2243,
                CatalogoCuentasModel::NO_CUENTA => 700049,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PAPELERIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2244,
                CatalogoCuentasModel::NO_CUENTA => 700050,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SEGUROS Y FIANZAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2245,
                CatalogoCuentasModel::NO_CUENTA => 700051,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMPUESTOS Y DERECHOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2246,
                CatalogoCuentasModel::NO_CUENTA => 700052,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MULTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2247,
                CatalogoCuentasModel::NO_CUENTA => 700053,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACTUALIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2248,
                CatalogoCuentasModel::NO_CUENTA => 700054,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS 6B0 FOMOCO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2249,
                CatalogoCuentasModel::NO_CUENTA => 700055,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE VIAJE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2250,
                CatalogoCuentasModel::NO_CUENTA => 700056,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REPARACION DA¥OS SERVICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2251,
                CatalogoCuentasModel::NO_CUENTA => 700057,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REFACCIONES VARIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2252,
                CatalogoCuentasModel::NO_CUENTA => 700058,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRASA Y LUBRICANTES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2253,
                CatalogoCuentasModel::NO_CUENTA => 700059,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PASAJES Y ESTACIONAMIENTOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2254,
                CatalogoCuentasModel::NO_CUENTA => 700060,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UTILES DE ASEO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2255,
                CatalogoCuentasModel::NO_CUENTA => 700061,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS FIN DE A¥O",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2256,
                CatalogoCuentasModel::NO_CUENTA => 700062,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECLUTAMIENTO Y SELECCION DE PERSONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2257,
                CatalogoCuentasModel::NO_CUENTA => 700063,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REDONDEOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2258,
                CatalogoCuentasModel::NO_CUENTA => 700064,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HONORARIOS PERSONAS FISICAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2259,
                CatalogoCuentasModel::NO_CUENTA => 700065,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HONORARIOS PERSONAS MORALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2260,
                CatalogoCuentasModel::NO_CUENTA => 700066,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ASESORIA ADMINISTRATIVA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2261,
                CatalogoCuentasModel::NO_CUENTA => 700067,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ASESORIA EN VENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2262,
                CatalogoCuentasModel::NO_CUENTA => 700068,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AUDITORIA EXTERNA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2263,
                CatalogoCuentasModel::NO_CUENTA => 700069,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DIVERSOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2264,
                CatalogoCuentasModel::NO_CUENTA => 700070,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SEGUROS DE TRASLADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2265,
                CatalogoCuentasModel::NO_CUENTA => 700071,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ARRENDAMIENTO DE EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2266,
                CatalogoCuentasModel::NO_CUENTA => 700072,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE TRASLADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2267,
                CatalogoCuentasModel::NO_CUENTA => 700076,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FLETES Y MENSAJERIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2268,
                CatalogoCuentasModel::NO_CUENTA => 700077,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2269,
                CatalogoCuentasModel::NO_CUENTA => 700078,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2270,
                CatalogoCuentasModel::NO_CUENTA => 700100,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE REFACCIONES SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2271,
                CatalogoCuentasModel::NO_CUENTA => 700101,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES VENDEDORES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2272,
                CatalogoCuentasModel::NO_CUENTA => 700102,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES SEGUROS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2273,
                CatalogoCuentasModel::NO_CUENTA => 700103,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTRAS COMISIONES VENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2274,
                CatalogoCuentasModel::NO_CUENTA => 700104,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUELDOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2275,
                CatalogoCuentasModel::NO_CUENTA => 700105,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRATIFICACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2276,
                CatalogoCuentasModel::NO_CUENTA => 700106,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES (SUELDOS)",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2277,
                CatalogoCuentasModel::NO_CUENTA => 700107,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TIEMPO EXTRA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2278,
                CatalogoCuentasModel::NO_CUENTA => 700108,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DOMINICAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2279,
                CatalogoCuentasModel::NO_CUENTA => 700109,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTROS ING.S\/SUELDOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2280,
                CatalogoCuentasModel::NO_CUENTA => 700110,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DE ANTIGUEDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2281,
                CatalogoCuentasModel::NO_CUENTA => 700111,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INDEMNIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2282,
                CatalogoCuentasModel::NO_CUENTA => 700112,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AGUINALDO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2283,
                CatalogoCuentasModel::NO_CUENTA => 700113,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VACACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2284,
                CatalogoCuentasModel::NO_CUENTA => 700114,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA VACACIONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2285,
                CatalogoCuentasModel::NO_CUENTA => 700115,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMSS PATRONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2286,
                CatalogoCuentasModel::NO_CUENTA => 700116,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2287,
                CatalogoCuentasModel::NO_CUENTA => 700117,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2288,
                CatalogoCuentasModel::NO_CUENTA => 700118,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CREDITO AL SALARIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2289,
                CatalogoCuentasModel::NO_CUENTA => 700119,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UNIFORMES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2290,
                CatalogoCuentasModel::NO_CUENTA => 700120,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PREVISION SOCIAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2291,
                CatalogoCuentasModel::NO_CUENTA => 700121,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAPACITACION Y ENTRENAMIENTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2292,
                CatalogoCuentasModel::NO_CUENTA => 700122,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE EDIFICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2293,
                CatalogoCuentasModel::NO_CUENTA => 700123,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2294,
                CatalogoCuentasModel::NO_CUENTA => 700124,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE TRANSPORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2295,
                CatalogoCuentasModel::NO_CUENTA => 700125,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE TALLER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2296,
                CatalogoCuentasModel::NO_CUENTA => 700126,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE TALLER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2297,
                CatalogoCuentasModel::NO_CUENTA => 700127,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE OFICINA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2298,
                CatalogoCuentasModel::NO_CUENTA => 700128,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE TRANSPORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2299,
                CatalogoCuentasModel::NO_CUENTA => 700129,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2300,
                CatalogoCuentasModel::NO_CUENTA => 700130,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE COMUNICACION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2301,
                CatalogoCuentasModel::NO_CUENTA => 700131,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AMORTIZACION EDIFICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2302,
                CatalogoCuentasModel::NO_CUENTA => 700132,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASOLINA NUEVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2303,
                CatalogoCuentasModel::NO_CUENTA => 700133,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASOLINA UNIDADES EN USO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2304,
                CatalogoCuentasModel::NO_CUENTA => 700134,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE PREVIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2305,
                CatalogoCuentasModel::NO_CUENTA => 700135,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HERRAMIENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2306,
                CatalogoCuentasModel::NO_CUENTA => 700136,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PUBLICIDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2307,
                CatalogoCuentasModel::NO_CUENTA => 700137,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PROMOCIONES ESPECIALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2308,
                CatalogoCuentasModel::NO_CUENTA => 700138,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CORTESIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2309,
                CatalogoCuentasModel::NO_CUENTA => 700139,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CUOTAS Y SUSCRIPCIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2310,
                CatalogoCuentasModel::NO_CUENTA => 700140,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DONATIVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2311,
                CatalogoCuentasModel::NO_CUENTA => 700141,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUBSIDIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2312,
                CatalogoCuentasModel::NO_CUENTA => 700142,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUBSIDIO DE SEGUROS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2313,
                CatalogoCuentasModel::NO_CUENTA => 700143,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RENTA DE LOCALES P.F.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2314,
                CatalogoCuentasModel::NO_CUENTA => 700144,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RENTA DE LOCALES P.M.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2315,
                CatalogoCuentasModel::NO_CUENTA => 700145,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TELEFONOS, FAX Y CORREOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2316,
                CatalogoCuentasModel::NO_CUENTA => 700146,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VIGILANCIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2317,
                CatalogoCuentasModel::NO_CUENTA => 700147,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CONSUMIBLES EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2318,
                CatalogoCuentasModel::NO_CUENTA => 700148,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS NO DEDUCIBLES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2319,
                CatalogoCuentasModel::NO_CUENTA => 700149,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PAPELERIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2320,
                CatalogoCuentasModel::NO_CUENTA => 700150,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SEGUROS Y FIANZAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2321,
                CatalogoCuentasModel::NO_CUENTA => 700151,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMPUESTOS Y DERECHOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2322,
                CatalogoCuentasModel::NO_CUENTA => 700152,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MULTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2323,
                CatalogoCuentasModel::NO_CUENTA => 700153,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACTUALIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2324,
                CatalogoCuentasModel::NO_CUENTA => 700154,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECARGOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2325,
                CatalogoCuentasModel::NO_CUENTA => 700155,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE VIAJE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2326,
                CatalogoCuentasModel::NO_CUENTA => 700156,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REPARACION DA¥OS SERVICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2327,
                CatalogoCuentasModel::NO_CUENTA => 700157,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COPIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2328,
                CatalogoCuentasModel::NO_CUENTA => 700158,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRASAS Y LUBRICANTES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2329,
                CatalogoCuentasModel::NO_CUENTA => 700159,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PASAJES Y ESTACIONAMIENTOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2330,
                CatalogoCuentasModel::NO_CUENTA => 700160,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UTILES DE ASEO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2331,
                CatalogoCuentasModel::NO_CUENTA => 700161,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE FIN DE A¥O",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2332,
                CatalogoCuentasModel::NO_CUENTA => 700162,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECLUTAMIENTO Y SELECCION DE PERSONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2333,
                CatalogoCuentasModel::NO_CUENTA => 700163,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REDONDEOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2334,
                CatalogoCuentasModel::NO_CUENTA => 700164,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HONORARIOS PERSONAS FISICAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2335,
                CatalogoCuentasModel::NO_CUENTA => 700165,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HONORARIOS PERSONAS MORALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2336,
                CatalogoCuentasModel::NO_CUENTA => 700166,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ASESORIAS ADMINISTRATIVAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2337,
                CatalogoCuentasModel::NO_CUENTA => 700167,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ADSESORIA EN VENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2338,
                CatalogoCuentasModel::NO_CUENTA => 700168,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AUDITORIA EXTERNA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2339,
                CatalogoCuentasModel::NO_CUENTA => 700169,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DIVERSOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2340,
                CatalogoCuentasModel::NO_CUENTA => 700170,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SEGUROS DE TRASLADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2341,
                CatalogoCuentasModel::NO_CUENTA => 700177,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2342,
                CatalogoCuentasModel::NO_CUENTA => 700178,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2194
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2343,
                CatalogoCuentasModel::NO_CUENTA => 710000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE VENTA DE TALLER SERVICIO QUERETARO",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2344,
                CatalogoCuentasModel::NO_CUENTA => 710001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUELDOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2345,
                CatalogoCuentasModel::NO_CUENTA => 710002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BONO EFICIENCIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2346,
                CatalogoCuentasModel::NO_CUENTA => 710003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2347,
                CatalogoCuentasModel::NO_CUENTA => 710004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TIEMPO EXTRA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2348,
                CatalogoCuentasModel::NO_CUENTA => 710005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DOMINICAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2349,
                CatalogoCuentasModel::NO_CUENTA => 710006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DE ANTIGUEDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2350,
                CatalogoCuentasModel::NO_CUENTA => 710007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INDEMNIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2351,
                CatalogoCuentasModel::NO_CUENTA => 710008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AGUINALDO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2352,
                CatalogoCuentasModel::NO_CUENTA => 710009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VACACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2353,
                CatalogoCuentasModel::NO_CUENTA => 710010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA VACACIONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2354,
                CatalogoCuentasModel::NO_CUENTA => 710011,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMSS PATRONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2355,
                CatalogoCuentasModel::NO_CUENTA => 710012,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2356,
                CatalogoCuentasModel::NO_CUENTA => 710013,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2357,
                CatalogoCuentasModel::NO_CUENTA => 710014,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CREDITO AL SALARIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2358,
                CatalogoCuentasModel::NO_CUENTA => 710015,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UNIFORMES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2359,
                CatalogoCuentasModel::NO_CUENTA => 710016,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PREVISION SOCIAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2360,
                CatalogoCuentasModel::NO_CUENTA => 710017,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAPACITACION Y ENTRENAMIENTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2361,
                CatalogoCuentasModel::NO_CUENTA => 710018,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EDIFICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2362,
                CatalogoCuentasModel::NO_CUENTA => 710019,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2363,
                CatalogoCuentasModel::NO_CUENTA => 710020,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE TRANSPORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2364,
                CatalogoCuentasModel::NO_CUENTA => 710021,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE TALLER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2365,
                CatalogoCuentasModel::NO_CUENTA => 710022,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECAICION DE EQUIPO DE TALLER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2366,
                CatalogoCuentasModel::NO_CUENTA => 710023,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE OFICINA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2367,
                CatalogoCuentasModel::NO_CUENTA => 710024,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE TRANSPORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2368,
                CatalogoCuentasModel::NO_CUENTA => 710025,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2369,
                CatalogoCuentasModel::NO_CUENTA => 710026,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE COMUNICACION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2370,
                CatalogoCuentasModel::NO_CUENTA => 710027,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AMORTIZACION EDIFICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2371,
                CatalogoCuentasModel::NO_CUENTA => 710028,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASOLINA NUEVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2372,
                CatalogoCuentasModel::NO_CUENTA => 710029,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASOLINA UNIDADES EN USO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2373,
                CatalogoCuentasModel::NO_CUENTA => 710030,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE PREVIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2374,
                CatalogoCuentasModel::NO_CUENTA => 710031,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HERRAMIENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2375,
                CatalogoCuentasModel::NO_CUENTA => 710032,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PUBLICIDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2376,
                CatalogoCuentasModel::NO_CUENTA => 710033,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PROMOCIONES ESPECIALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2377,
                CatalogoCuentasModel::NO_CUENTA => 710034,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CORTESIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2378,
                CatalogoCuentasModel::NO_CUENTA => 710035,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CUOTAS Y SUSCRIPCIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2379,
                CatalogoCuentasModel::NO_CUENTA => 710036,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DONATIVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2380,
                CatalogoCuentasModel::NO_CUENTA => 710037,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RENTA DE LOCALES P.F.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2381,
                CatalogoCuentasModel::NO_CUENTA => 710038,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RENTA DE LOCALES P.M.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2382,
                CatalogoCuentasModel::NO_CUENTA => 710039,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TELEFONOS, FAX Y CORREOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2383,
                CatalogoCuentasModel::NO_CUENTA => 710040,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VIGILANCIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2384,
                CatalogoCuentasModel::NO_CUENTA => 710041,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CONSUMIBLES EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2385,
                CatalogoCuentasModel::NO_CUENTA => 710042,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS NO DEDUCIBLES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2386,
                CatalogoCuentasModel::NO_CUENTA => 710043,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PAPELERIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2387,
                CatalogoCuentasModel::NO_CUENTA => 710044,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SEGUROS Y FIANZAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2388,
                CatalogoCuentasModel::NO_CUENTA => 710045,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMPUESTOS Y DERECHOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2389,
                CatalogoCuentasModel::NO_CUENTA => 710046,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MULTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2390,
                CatalogoCuentasModel::NO_CUENTA => 710047,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACTUALIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2391,
                CatalogoCuentasModel::NO_CUENTA => 710048,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECARGOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2392,
                CatalogoCuentasModel::NO_CUENTA => 710049,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE VIAJE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2393,
                CatalogoCuentasModel::NO_CUENTA => 710050,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REPARACION DAÑOS SERVICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2394,
                CatalogoCuentasModel::NO_CUENTA => 710051,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COPIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2395,
                CatalogoCuentasModel::NO_CUENTA => 710052,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRASAS Y LUBRICANTES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2396,
                CatalogoCuentasModel::NO_CUENTA => 710053,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PASAJES Y ESTACIONAMIENTOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2397,
                CatalogoCuentasModel::NO_CUENTA => 710054,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UTILES DE ASEO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2398,
                CatalogoCuentasModel::NO_CUENTA => 710055,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS FIN DE AÑO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2399,
                CatalogoCuentasModel::NO_CUENTA => 710056,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECLUTAMIENTO Y SELECCION DE PERSONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2400,
                CatalogoCuentasModel::NO_CUENTA => 710057,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REDONDEOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2401,
                CatalogoCuentasModel::NO_CUENTA => 710058,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HONORARIOS PERSONAS FISICAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2402,
                CatalogoCuentasModel::NO_CUENTA => 710059,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HONORARIOS PERSONAS MORALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2403,
                CatalogoCuentasModel::NO_CUENTA => 710060,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ASESORIA ADMINISTRATIVA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2404,
                CatalogoCuentasModel::NO_CUENTA => 710061,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ASESORIA EN VENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2405,
                CatalogoCuentasModel::NO_CUENTA => 710062,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AUDITORIA EXTERNA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2406,
                CatalogoCuentasModel::NO_CUENTA => 710063,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DIVERSOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2407,
                CatalogoCuentasModel::NO_CUENTA => 710064,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SEGUROS DE TRASLADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2408,
                CatalogoCuentasModel::NO_CUENTA => 710065,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ARRENDAMIENTO DE EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2409,
                CatalogoCuentasModel::NO_CUENTA => 710066,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE TRASLADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2410,
                CatalogoCuentasModel::NO_CUENTA => 710067,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ALMACENAMIENTO DE UNIDADES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2411,
                CatalogoCuentasModel::NO_CUENTA => 710068,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AGUA POTABLE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2412,
                CatalogoCuentasModel::NO_CUENTA => 710069,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ATENCION A CLIENTES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2413,
                CatalogoCuentasModel::NO_CUENTA => 710070,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FLETES Y MENSAJERIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2414,
                CatalogoCuentasModel::NO_CUENTA => 710071,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ENERGIA ELECTRICA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2415,
                CatalogoCuentasModel::NO_CUENTA => 710072,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2416,
                CatalogoCuentasModel::NO_CUENTA => 710073,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BURO DE CREDITO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2417,
                CatalogoCuentasModel::NO_CUENTA => 710074,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACTUALIZACION DE GASTOS (REEXPRESION)",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2418,
                CatalogoCuentasModel::NO_CUENTA => 710075,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUELDO ADMON. GARANTIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2343
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2419,
                CatalogoCuentasModel::NO_CUENTA => 720000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE VENTA DE TALLER S.J.R.",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2420,
                CatalogoCuentasModel::NO_CUENTA => 720001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUELDOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2421,
                CatalogoCuentasModel::NO_CUENTA => 720002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRATIFICACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2422,
                CatalogoCuentasModel::NO_CUENTA => 720003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2423,
                CatalogoCuentasModel::NO_CUENTA => 720004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TIEMPO EXTRA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2424,
                CatalogoCuentasModel::NO_CUENTA => 720005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DOMINICAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2425,
                CatalogoCuentasModel::NO_CUENTA => 720006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DE ANTIGUEDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2426,
                CatalogoCuentasModel::NO_CUENTA => 720007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INDEMNIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2427,
                CatalogoCuentasModel::NO_CUENTA => 720008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AGUINALDO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2428,
                CatalogoCuentasModel::NO_CUENTA => 720009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VACACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2429,
                CatalogoCuentasModel::NO_CUENTA => 720010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA VACACIONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2430,
                CatalogoCuentasModel::NO_CUENTA => 720011,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMSS PATRONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2431,
                CatalogoCuentasModel::NO_CUENTA => 720012,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2432,
                CatalogoCuentasModel::NO_CUENTA => 720013,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2433,
                CatalogoCuentasModel::NO_CUENTA => 720014,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CREDITO AL SALARIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2434,
                CatalogoCuentasModel::NO_CUENTA => 720015,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UNIFORMES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2435,
                CatalogoCuentasModel::NO_CUENTA => 720016,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PREVISION SOCIAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2436,
                CatalogoCuentasModel::NO_CUENTA => 720017,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAPACITACION Y ENTRENAMIENTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2437,
                CatalogoCuentasModel::NO_CUENTA => 720018,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EDIFICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2438,
                CatalogoCuentasModel::NO_CUENTA => 720019,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2439,
                CatalogoCuentasModel::NO_CUENTA => 720020,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE TRANSPORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2440,
                CatalogoCuentasModel::NO_CUENTA => 720021,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE TALLER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2441,
                CatalogoCuentasModel::NO_CUENTA => 720022,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE TALLER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2442,
                CatalogoCuentasModel::NO_CUENTA => 720023,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE OFICINA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2443,
                CatalogoCuentasModel::NO_CUENTA => 720024,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE TRANSPORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2444,
                CatalogoCuentasModel::NO_CUENTA => 720025,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2445,
                CatalogoCuentasModel::NO_CUENTA => 720026,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE COMUNICACION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2446,
                CatalogoCuentasModel::NO_CUENTA => 720027,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AMORTIZACION EDIFICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2447,
                CatalogoCuentasModel::NO_CUENTA => 720028,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASOLINA NUEVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2448,
                CatalogoCuentasModel::NO_CUENTA => 720029,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASOLINA UNIDADES EN USO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2449,
                CatalogoCuentasModel::NO_CUENTA => 720030,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE PREVIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2450,
                CatalogoCuentasModel::NO_CUENTA => 720031,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HERRAMIENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2451,
                CatalogoCuentasModel::NO_CUENTA => 720032,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PUBLICIDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2452,
                CatalogoCuentasModel::NO_CUENTA => 720033,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PROMOCIONES ESPECIALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2453,
                CatalogoCuentasModel::NO_CUENTA => 720034,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CORTESIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2454,
                CatalogoCuentasModel::NO_CUENTA => 720035,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CUOTAS Y SUSCRIPCIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2455,
                CatalogoCuentasModel::NO_CUENTA => 720036,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DONATIVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2456,
                CatalogoCuentasModel::NO_CUENTA => 720037,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RENTA DE LOCALES P.F.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2457,
                CatalogoCuentasModel::NO_CUENTA => 720038,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RENTA DE LOCALES P.M.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2458,
                CatalogoCuentasModel::NO_CUENTA => 720039,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TELEFONOS, FAX Y CORREOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2459,
                CatalogoCuentasModel::NO_CUENTA => 720040,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VIGILANCIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2460,
                CatalogoCuentasModel::NO_CUENTA => 720041,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CONSUMIBLES EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2461,
                CatalogoCuentasModel::NO_CUENTA => 720042,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS NO DEDUCIBLES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2462,
                CatalogoCuentasModel::NO_CUENTA => 720043,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PAPELERIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2463,
                CatalogoCuentasModel::NO_CUENTA => 720044,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SEGUROS Y FIANZAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2464,
                CatalogoCuentasModel::NO_CUENTA => 720045,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMPUESTOS Y DERECHOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2465,
                CatalogoCuentasModel::NO_CUENTA => 720046,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MULTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2466,
                CatalogoCuentasModel::NO_CUENTA => 720047,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACTUALIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2467,
                CatalogoCuentasModel::NO_CUENTA => 720048,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECARGOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2468,
                CatalogoCuentasModel::NO_CUENTA => 720049,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE VIAJE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2469,
                CatalogoCuentasModel::NO_CUENTA => 720050,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REPARACION DAÑOS SERVICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2470,
                CatalogoCuentasModel::NO_CUENTA => 720051,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COPIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2471,
                CatalogoCuentasModel::NO_CUENTA => 720052,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRASAS Y LUBRICANTES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2472,
                CatalogoCuentasModel::NO_CUENTA => 720053,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PASAJES Y ESTACIONAMIENTOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2473,
                CatalogoCuentasModel::NO_CUENTA => 720054,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UTILES DE ASEO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2474,
                CatalogoCuentasModel::NO_CUENTA => 720055,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS FIN DE AÑO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2475,
                CatalogoCuentasModel::NO_CUENTA => 720056,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECLUTAMIENTO Y SELECCION DE PERSONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2476,
                CatalogoCuentasModel::NO_CUENTA => 720057,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REDONDEOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2477,
                CatalogoCuentasModel::NO_CUENTA => 720058,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HONORARIOS PERSONAS FISICAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2478,
                CatalogoCuentasModel::NO_CUENTA => 720059,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HONORARIOS PERSONAS MORALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2479,
                CatalogoCuentasModel::NO_CUENTA => 720060,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ASESORIA ADMINISTRATIVA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2480,
                CatalogoCuentasModel::NO_CUENTA => 720061,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ASESORIA EN VENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2481,
                CatalogoCuentasModel::NO_CUENTA => 720062,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AUDITORIA EXTERNA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2482,
                CatalogoCuentasModel::NO_CUENTA => 720063,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DIVERSOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2483,
                CatalogoCuentasModel::NO_CUENTA => 720064,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SEGUROS DE TRASLADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2484,
                CatalogoCuentasModel::NO_CUENTA => 720065,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ARRENDAMIENTO DE EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2485,
                CatalogoCuentasModel::NO_CUENTA => 720066,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE TRASLADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2486,
                CatalogoCuentasModel::NO_CUENTA => 720067,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ALMACENAMIENTO UNIDADES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2487,
                CatalogoCuentasModel::NO_CUENTA => 720068,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AGUA POTABLE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2488,
                CatalogoCuentasModel::NO_CUENTA => 720069,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ATENCION A CLIENTES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2489,
                CatalogoCuentasModel::NO_CUENTA => 720070,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FLETES Y MENSAJERIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2490,
                CatalogoCuentasModel::NO_CUENTA => 720071,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ENERGIA ELECTRICA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2491,
                CatalogoCuentasModel::NO_CUENTA => 720072,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2492,
                CatalogoCuentasModel::NO_CUENTA => 720073,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BURO DE CREDITO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2493,
                CatalogoCuentasModel::NO_CUENTA => 720074,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACTUALIZACION DE GASTOS (REEXPRESION)",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2494,
                CatalogoCuentasModel::NO_CUENTA => 720075,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUELDO ADMON. GARANTIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2419
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2495,
                CatalogoCuentasModel::NO_CUENTA => 800000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE ADMINISTRACION",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2496,
                CatalogoCuentasModel::NO_CUENTA => 800001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES VENDEDORES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2497,
                CatalogoCuentasModel::NO_CUENTA => 800002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES SEGUROS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2498,
                CatalogoCuentasModel::NO_CUENTA => 800003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTRAS COMISIONES SOBRE VENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2499,
                CatalogoCuentasModel::NO_CUENTA => 800004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUELDOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2500,
                CatalogoCuentasModel::NO_CUENTA => 800005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRATIFICACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2501,
                CatalogoCuentasModel::NO_CUENTA => 800006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES (SUELDOS)",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2502,
                CatalogoCuentasModel::NO_CUENTA => 800007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TIEMPO EXTRA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2503,
                CatalogoCuentasModel::NO_CUENTA => 800008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DOMINICAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2504,
                CatalogoCuentasModel::NO_CUENTA => 800009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTROS INGRESOS (SUELDOS)",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2505,
                CatalogoCuentasModel::NO_CUENTA => 800010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DE ANTIGUEDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2506,
                CatalogoCuentasModel::NO_CUENTA => 800011,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INDEMNIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2507,
                CatalogoCuentasModel::NO_CUENTA => 800012,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AGUINALDO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2508,
                CatalogoCuentasModel::NO_CUENTA => 800013,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VACACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2509,
                CatalogoCuentasModel::NO_CUENTA => 800014,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA VACACIONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2510,
                CatalogoCuentasModel::NO_CUENTA => 800015,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMSS PATRONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2511,
                CatalogoCuentasModel::NO_CUENTA => 800016,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2512,
                CatalogoCuentasModel::NO_CUENTA => 800017,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2513,
                CatalogoCuentasModel::NO_CUENTA => 800018,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CREDITO AL SALARIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2514,
                CatalogoCuentasModel::NO_CUENTA => 800019,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UNIFORMES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2515,
                CatalogoCuentasModel::NO_CUENTA => 800020,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PREVISION SOCIAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2516,
                CatalogoCuentasModel::NO_CUENTA => 800021,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAPACITACION Y ENTRENAMIENTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2517,
                CatalogoCuentasModel::NO_CUENTA => 800022,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE EDIFICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2518,
                CatalogoCuentasModel::NO_CUENTA => 800023,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2519,
                CatalogoCuentasModel::NO_CUENTA => 800024,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE TRANSPORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2520,
                CatalogoCuentasModel::NO_CUENTA => 800025,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE TALLER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2521,
                CatalogoCuentasModel::NO_CUENTA => 800026,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE TALLER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2522,
                CatalogoCuentasModel::NO_CUENTA => 800027,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION EQIPO DE OFICINA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2523,
                CatalogoCuentasModel::NO_CUENTA => 800028,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION EQUIPO DE TRANSPORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2524,
                CatalogoCuentasModel::NO_CUENTA => 800029,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2525,
                CatalogoCuentasModel::NO_CUENTA => 800030,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE COMUNICACION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2526,
                CatalogoCuentasModel::NO_CUENTA => 800031,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AMORTIZACION DE EDIFICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2527,
                CatalogoCuentasModel::NO_CUENTA => 800032,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASOLINA NUEVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2528,
                CatalogoCuentasModel::NO_CUENTA => 800033,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASOLINA UNIDADES EN USO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2529,
                CatalogoCuentasModel::NO_CUENTA => 800034,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE PREVIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2530,
                CatalogoCuentasModel::NO_CUENTA => 800035,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HERRAMIENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2531,
                CatalogoCuentasModel::NO_CUENTA => 800036,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PUBLICIDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2532,
                CatalogoCuentasModel::NO_CUENTA => 800037,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PROMOCIONES ESPECIALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2533,
                CatalogoCuentasModel::NO_CUENTA => 800038,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CORTESIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2534,
                CatalogoCuentasModel::NO_CUENTA => 800039,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CUOTAS Y SUSCRIPCIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2535,
                CatalogoCuentasModel::NO_CUENTA => 800040,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DONATIVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2536,
                CatalogoCuentasModel::NO_CUENTA => 800041,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUBSIDIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2537,
                CatalogoCuentasModel::NO_CUENTA => 800042,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUBSIDIO DE SEGUROS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2538,
                CatalogoCuentasModel::NO_CUENTA => 800043,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RENTA DE LOCALES P. FISICAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2539,
                CatalogoCuentasModel::NO_CUENTA => 800044,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RENTA LOCALES P. MORALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2540,
                CatalogoCuentasModel::NO_CUENTA => 800045,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TELEFONO FAX Y CORREOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2541,
                CatalogoCuentasModel::NO_CUENTA => 800046,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VIGILANCIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2542,
                CatalogoCuentasModel::NO_CUENTA => 800047,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CONSUMIBLES EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2543,
                CatalogoCuentasModel::NO_CUENTA => 800048,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS NO DEDUCIBLES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2544,
                CatalogoCuentasModel::NO_CUENTA => 800049,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PAPELERIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2545,
                CatalogoCuentasModel::NO_CUENTA => 800050,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SEGUROS Y FIANZAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2546,
                CatalogoCuentasModel::NO_CUENTA => 800051,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMPUESTOS Y DERECHOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2547,
                CatalogoCuentasModel::NO_CUENTA => 800052,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MULTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2548,
                CatalogoCuentasModel::NO_CUENTA => 800053,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACTUALIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2549,
                CatalogoCuentasModel::NO_CUENTA => 800054,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECARGOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2550,
                CatalogoCuentasModel::NO_CUENTA => 800055,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE VIAJE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2551,
                CatalogoCuentasModel::NO_CUENTA => 800056,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REPARACION DE DA¥OS SERVICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2552,
                CatalogoCuentasModel::NO_CUENTA => 800057,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COPIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2553,
                CatalogoCuentasModel::NO_CUENTA => 800058,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRASAS Y LUBRICACNTES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2554,
                CatalogoCuentasModel::NO_CUENTA => 800059,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PASAJES Y ESTACIONAMIENTOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2555,
                CatalogoCuentasModel::NO_CUENTA => 800060,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UTILES Y ASEO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2556,
                CatalogoCuentasModel::NO_CUENTA => 800061,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECLUTAMIENTO Y SELECCION DE PERSONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2557,
                CatalogoCuentasModel::NO_CUENTA => 800062,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REFACCIONES VARIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2558,
                CatalogoCuentasModel::NO_CUENTA => 800064,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HONORARIOS PERSONAS FISICAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2559,
                CatalogoCuentasModel::NO_CUENTA => 800065,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HONORARIOS PERSONAS MORALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2560,
                CatalogoCuentasModel::NO_CUENTA => 800066,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ASESORIA ADMINISTRATIVA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2561,
                CatalogoCuentasModel::NO_CUENTA => 800067,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ASESORIA EN VENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2562,
                CatalogoCuentasModel::NO_CUENTA => 800068,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AUDITORIA EXTERNA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2563,
                CatalogoCuentasModel::NO_CUENTA => 800069,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DIVERSOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2564,
                CatalogoCuentasModel::NO_CUENTA => 800070,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SEGUROS DE TRASLADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2565,
                CatalogoCuentasModel::NO_CUENTA => 800071,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ARRENDAMIENTO DE EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2566,
                CatalogoCuentasModel::NO_CUENTA => 800072,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE TRASLADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2567,
                CatalogoCuentasModel::NO_CUENTA => 800073,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MATERIALES Y EQUIPOS DE SEGURIDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2568,
                CatalogoCuentasModel::NO_CUENTA => 800075,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ATENCION A CLIENTES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2569,
                CatalogoCuentasModel::NO_CUENTA => 800076,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FLETES Y MENSAJERIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2570,
                CatalogoCuentasModel::NO_CUENTA => 800077,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EQ, DE SEGURIDAD E HIGIENE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2571,
                CatalogoCuentasModel::NO_CUENTA => 800078,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RENTA DE AUTOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2572,
                CatalogoCuentasModel::NO_CUENTA => 800080,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANO DE OBRA - HOJALATERIA Y PINTURA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2573,
                CatalogoCuentasModel::NO_CUENTA => 800098,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE FIN DE AñO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2574,
                CatalogoCuentasModel::NO_CUENTA => 800100,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS TALLER SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2575,
                CatalogoCuentasModel::NO_CUENTA => 800101,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES VENDEDORES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2576,
                CatalogoCuentasModel::NO_CUENTA => 800102,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES SEGUROS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2577,
                CatalogoCuentasModel::NO_CUENTA => 800103,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTRAS COMISIONES VENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2578,
                CatalogoCuentasModel::NO_CUENTA => 800104,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUELDOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2579,
                CatalogoCuentasModel::NO_CUENTA => 800105,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRATIFICACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2580,
                CatalogoCuentasModel::NO_CUENTA => 800106,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES (SUELDOS)",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2581,
                CatalogoCuentasModel::NO_CUENTA => 800107,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TIEMPO EXTRA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2582,
                CatalogoCuentasModel::NO_CUENTA => 800108,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DOMINICAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2583,
                CatalogoCuentasModel::NO_CUENTA => 800109,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTROS ING. (SUELDOS)",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2584,
                CatalogoCuentasModel::NO_CUENTA => 800110,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DE ANTIGUEDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2585,
                CatalogoCuentasModel::NO_CUENTA => 800111,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INDEMNIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2586,
                CatalogoCuentasModel::NO_CUENTA => 800112,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AGUINALDO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2587,
                CatalogoCuentasModel::NO_CUENTA => 800113,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VACACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2588,
                CatalogoCuentasModel::NO_CUENTA => 800114,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA VACACIONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2589,
                CatalogoCuentasModel::NO_CUENTA => 800115,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMSS PATRONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2590,
                CatalogoCuentasModel::NO_CUENTA => 800116,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2591,
                CatalogoCuentasModel::NO_CUENTA => 800117,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2592,
                CatalogoCuentasModel::NO_CUENTA => 800118,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CREDITO AL SALARIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2593,
                CatalogoCuentasModel::NO_CUENTA => 800119,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UNIFORMES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2594,
                CatalogoCuentasModel::NO_CUENTA => 800120,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PREVISION SOCIAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2595,
                CatalogoCuentasModel::NO_CUENTA => 800121,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAPACITACION Y ENTRENAMIENTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2596,
                CatalogoCuentasModel::NO_CUENTA => 800122,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE EDIFICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2597,
                CatalogoCuentasModel::NO_CUENTA => 800123,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2598,
                CatalogoCuentasModel::NO_CUENTA => 800124,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE TRANSPORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2599,
                CatalogoCuentasModel::NO_CUENTA => 800125,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE TALLER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2600,
                CatalogoCuentasModel::NO_CUENTA => 800126,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE TALLER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2601,
                CatalogoCuentasModel::NO_CUENTA => 800127,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE OFICINA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2602,
                CatalogoCuentasModel::NO_CUENTA => 800128,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE TRANSPORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2603,
                CatalogoCuentasModel::NO_CUENTA => 800129,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2604,
                CatalogoCuentasModel::NO_CUENTA => 800130,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE COMUNICACION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2605,
                CatalogoCuentasModel::NO_CUENTA => 800131,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AMORTIZACION DE EDIFICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2606,
                CatalogoCuentasModel::NO_CUENTA => 800132,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASOLINA NUEVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2607,
                CatalogoCuentasModel::NO_CUENTA => 800133,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASOLINA UNIDADES EN USO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2608,
                CatalogoCuentasModel::NO_CUENTA => 800134,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE PREVIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2609,
                CatalogoCuentasModel::NO_CUENTA => 800135,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HERRAMIENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2610,
                CatalogoCuentasModel::NO_CUENTA => 800136,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PUBLICIDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2611,
                CatalogoCuentasModel::NO_CUENTA => 800137,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PROMOCIONES ESPECIALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2612,
                CatalogoCuentasModel::NO_CUENTA => 800138,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CORTESIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2613,
                CatalogoCuentasModel::NO_CUENTA => 800139,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CUOTAS Y SUSCRIPCIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2614,
                CatalogoCuentasModel::NO_CUENTA => 800140,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DONATIVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2615,
                CatalogoCuentasModel::NO_CUENTA => 800141,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUBSIDIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2616,
                CatalogoCuentasModel::NO_CUENTA => 800142,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUBSIDIO DE SEGUROS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2617,
                CatalogoCuentasModel::NO_CUENTA => 800143,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RENTA DE LOCALES P.F.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2618,
                CatalogoCuentasModel::NO_CUENTA => 800144,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RENTA DE LOCALES P.M.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2619,
                CatalogoCuentasModel::NO_CUENTA => 800145,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TELEFONOS, FAX Y CORREOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2620,
                CatalogoCuentasModel::NO_CUENTA => 800146,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VIGILANCIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2621,
                CatalogoCuentasModel::NO_CUENTA => 800147,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CONSUMIBLES EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2622,
                CatalogoCuentasModel::NO_CUENTA => 800148,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS NO DEDUCIBLES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2623,
                CatalogoCuentasModel::NO_CUENTA => 800149,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PAPELERIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2624,
                CatalogoCuentasModel::NO_CUENTA => 800150,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SEGUROS Y FIANZAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2625,
                CatalogoCuentasModel::NO_CUENTA => 800151,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMPUESTOS Y DERECHOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2626,
                CatalogoCuentasModel::NO_CUENTA => 800152,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MULTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2627,
                CatalogoCuentasModel::NO_CUENTA => 800153,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACTUALIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2628,
                CatalogoCuentasModel::NO_CUENTA => 800154,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECARGOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2629,
                CatalogoCuentasModel::NO_CUENTA => 800155,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE VIAJE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2630,
                CatalogoCuentasModel::NO_CUENTA => 800156,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REPARACION DA¥OS Y SERVICIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2631,
                CatalogoCuentasModel::NO_CUENTA => 800157,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COPIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2632,
                CatalogoCuentasModel::NO_CUENTA => 800158,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRASA Y LUBRICANTES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2633,
                CatalogoCuentasModel::NO_CUENTA => 800159,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PASAJES Y ESTACIONAMIENTOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2634,
                CatalogoCuentasModel::NO_CUENTA => 800160,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UTILES DE ASEO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2635,
                CatalogoCuentasModel::NO_CUENTA => 800161,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS FIN DE A¥O",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2636,
                CatalogoCuentasModel::NO_CUENTA => 800162,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECLUTAMIENTO Y SELECCION DE PERSONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2637,
                CatalogoCuentasModel::NO_CUENTA => 800163,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REDONDEOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2638,
                CatalogoCuentasModel::NO_CUENTA => 800164,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HONORARIOS PERSONAS FISICAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2639,
                CatalogoCuentasModel::NO_CUENTA => 800165,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HONORARIOS PERSONAS MORALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2640,
                CatalogoCuentasModel::NO_CUENTA => 800166,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ASESORIA ADMINISTRATIVA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2641,
                CatalogoCuentasModel::NO_CUENTA => 800167,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ASESORIA EN VENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2642,
                CatalogoCuentasModel::NO_CUENTA => 800168,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AUDITORIA EXTERNA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2643,
                CatalogoCuentasModel::NO_CUENTA => 800169,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DIVERSOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2644,
                CatalogoCuentasModel::NO_CUENTA => 800170,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SEGUROS DE TRASLADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2645,
                CatalogoCuentasModel::NO_CUENTA => 800171,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REFACCIONES VARIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2646,
                CatalogoCuentasModel::NO_CUENTA => 800172,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE TRASLADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2647,
                CatalogoCuentasModel::NO_CUENTA => 800177,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2648,
                CatalogoCuentasModel::NO_CUENTA => 800178,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2649,
                CatalogoCuentasModel::NO_CUENTA => 800179,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REPARACIONES VARIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2495
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2650,
                CatalogoCuentasModel::NO_CUENTA => 810000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE ADMINISTRACION",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2651,
                CatalogoCuentasModel::NO_CUENTA => 810001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUELDOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2652,
                CatalogoCuentasModel::NO_CUENTA => 810002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BONO EFICIENCIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2653,
                CatalogoCuentasModel::NO_CUENTA => 810003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2654,
                CatalogoCuentasModel::NO_CUENTA => 810004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DOMINICAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2655,
                CatalogoCuentasModel::NO_CUENTA => 810005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DE ANTIGUEDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2656,
                CatalogoCuentasModel::NO_CUENTA => 810006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INDEMNIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2657,
                CatalogoCuentasModel::NO_CUENTA => 810007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AGUINALDO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2658,
                CatalogoCuentasModel::NO_CUENTA => 810008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VACACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2659,
                CatalogoCuentasModel::NO_CUENTA => 810009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA VACACIONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2660,
                CatalogoCuentasModel::NO_CUENTA => 810010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMSS PATRONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2661,
                CatalogoCuentasModel::NO_CUENTA => 810011,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2662,
                CatalogoCuentasModel::NO_CUENTA => 810012,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2663,
                CatalogoCuentasModel::NO_CUENTA => 810013,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CREDITO AL SALARIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2664,
                CatalogoCuentasModel::NO_CUENTA => 810014,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UNIFORMES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2665,
                CatalogoCuentasModel::NO_CUENTA => 810015,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HONORARIOS ASIMILABLES A SALARIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2666,
                CatalogoCuentasModel::NO_CUENTA => 810016,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAPACITACION Y ENTRENAMIENTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2667,
                CatalogoCuentasModel::NO_CUENTA => 810017,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO DE EDIFICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2668,
                CatalogoCuentasModel::NO_CUENTA => 810018,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2669,
                CatalogoCuentasModel::NO_CUENTA => 810019,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE TRANSPORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2670,
                CatalogoCuentasModel::NO_CUENTA => 810020,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE TALLER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2671,
                CatalogoCuentasModel::NO_CUENTA => 810021,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE TALLER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2672,
                CatalogoCuentasModel::NO_CUENTA => 810022,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE OFICINA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2673,
                CatalogoCuentasModel::NO_CUENTA => 810023,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE TRANPORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2674,
                CatalogoCuentasModel::NO_CUENTA => 810024,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2675,
                CatalogoCuentasModel::NO_CUENTA => 810025,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECAICION DE EQUIPO DE COMUNICACION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2676,
                CatalogoCuentasModel::NO_CUENTA => 810026,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AMORTIZACION EDIFICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2677,
                CatalogoCuentasModel::NO_CUENTA => 810027,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASOLINA NUEVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2678,
                CatalogoCuentasModel::NO_CUENTA => 810028,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASOLINA UNIDADES EN USO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2679,
                CatalogoCuentasModel::NO_CUENTA => 810029,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE PREVIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2680,
                CatalogoCuentasModel::NO_CUENTA => 810030,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HERRAMIENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2681,
                CatalogoCuentasModel::NO_CUENTA => 810031,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PUBLICIDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2682,
                CatalogoCuentasModel::NO_CUENTA => 810032,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PROMOCIONES ESPECIALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2683,
                CatalogoCuentasModel::NO_CUENTA => 810033,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CORTESIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2684,
                CatalogoCuentasModel::NO_CUENTA => 810034,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CUOTAS Y SUSCRIPCIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2685,
                CatalogoCuentasModel::NO_CUENTA => 810035,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DONATIVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2686,
                CatalogoCuentasModel::NO_CUENTA => 810036,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RENTA DE LOCALES Y TERRENOS P.F.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2687,
                CatalogoCuentasModel::NO_CUENTA => 810037,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RENTA DE LOCALES Y TERRENOS P.M.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2688,
                CatalogoCuentasModel::NO_CUENTA => 810038,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TELEFONOS, FAX Y CORREOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2689,
                CatalogoCuentasModel::NO_CUENTA => 810039,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VIGILANCIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2690,
                CatalogoCuentasModel::NO_CUENTA => 810040,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CONSUMIBLES EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2691,
                CatalogoCuentasModel::NO_CUENTA => 810041,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS NO DEDUCIBLES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2692,
                CatalogoCuentasModel::NO_CUENTA => 810042,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PAPELERIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2693,
                CatalogoCuentasModel::NO_CUENTA => 810043,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SEGUROS Y FIANZAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2694,
                CatalogoCuentasModel::NO_CUENTA => 810044,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMPUESTOS Y DERECHOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2695,
                CatalogoCuentasModel::NO_CUENTA => 810045,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MULTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2696,
                CatalogoCuentasModel::NO_CUENTA => 810046,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACTUALIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2697,
                CatalogoCuentasModel::NO_CUENTA => 810047,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECARGOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2698,
                CatalogoCuentasModel::NO_CUENTA => 810048,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE VIAJE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2699,
                CatalogoCuentasModel::NO_CUENTA => 810049,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PROYECTO MYBSA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2700,
                CatalogoCuentasModel::NO_CUENTA => 810050,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COPIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2701,
                CatalogoCuentasModel::NO_CUENTA => 810051,
                CatalogoCuentasModel::NOMBRE_CUENTA => "LIBROS Y REVISTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2702,
                CatalogoCuentasModel::NO_CUENTA => 810052,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PASAJES Y ESTACIONAMIENTOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2703,
                CatalogoCuentasModel::NO_CUENTA => 810053,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UTILES DE ASEO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2704,
                CatalogoCuentasModel::NO_CUENTA => 810054,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS FIN DE AÑO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2705,
                CatalogoCuentasModel::NO_CUENTA => 810055,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECLUTAMIENTO Y SELECCION DE PERSONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2706,
                CatalogoCuentasModel::NO_CUENTA => 810056,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REDONDEOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2707,
                CatalogoCuentasModel::NO_CUENTA => 810057,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HONORARIOS PERSONAS FISICAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2708,
                CatalogoCuentasModel::NO_CUENTA => 810058,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HONORARIOS PERSONAS MORALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2709,
                CatalogoCuentasModel::NO_CUENTA => 810059,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ASESORIA ADMINISTRATIVA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2710,
                CatalogoCuentasModel::NO_CUENTA => 810060,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ASESORIA EN VENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2711,
                CatalogoCuentasModel::NO_CUENTA => 810061,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS CAFETERIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2712,
                CatalogoCuentasModel::NO_CUENTA => 810062,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DIVERSOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2713,
                CatalogoCuentasModel::NO_CUENTA => 810063,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SEGUROS DE TRASLADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2714,
                CatalogoCuentasModel::NO_CUENTA => 810064,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ARRENDAMIENTO DE EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2715,
                CatalogoCuentasModel::NO_CUENTA => 810065,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AUDITORIA EXTERNA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2716,
                CatalogoCuentasModel::NO_CUENTA => 810066,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ENERGIA ELECTRICA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2717,
                CatalogoCuentasModel::NO_CUENTA => 810067,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AGUA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2718,
                CatalogoCuentasModel::NO_CUENTA => 810068,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECOLECCION DE VALORES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2719,
                CatalogoCuentasModel::NO_CUENTA => 810069,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AMORTIZACION LICENCIAS P\/COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2720,
                CatalogoCuentasModel::NO_CUENTA => 810071,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2721,
                CatalogoCuentasModel::NO_CUENTA => 810072,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE OFICINA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2722,
                CatalogoCuentasModel::NO_CUENTA => 810073,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS MEDICOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2723,
                CatalogoCuentasModel::NO_CUENTA => 810074,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS FIN DE AÑO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2724,
                CatalogoCuentasModel::NO_CUENTA => 810075,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACTUALIZACION DE GASTOS (REEXPRESION)",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2725,
                CatalogoCuentasModel::NO_CUENTA => 810077,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUELDO ADMON. BODYSHOP",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2650
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2726,
                CatalogoCuentasModel::NO_CUENTA => 850000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUELDOS ADMINISTRATIVOS SJR",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2727,
                CatalogoCuentasModel::NO_CUENTA => 850001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUELDOS ADMON. SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2726
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2728,
                CatalogoCuentasModel::NO_CUENTA => 850002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "BONO ADMON. SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2726
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2729,
                CatalogoCuentasModel::NO_CUENTA => 850003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VACACIONES ADMON. SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2726
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2730,
                CatalogoCuentasModel::NO_CUENTA => 850004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA VACACIONAL ADMON. SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2726
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2731,
                CatalogoCuentasModel::NO_CUENTA => 850005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AGUINALDO ADMON. SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2726
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2732,
                CatalogoCuentasModel::NO_CUENTA => 850006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INDEMNIZACIONES ADMON. SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2726
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2733,
                CatalogoCuentasModel::NO_CUENTA => 850007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DE ANTIGUEDAD ADMON. SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2726
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2734,
                CatalogoCuentasModel::NO_CUENTA => 850008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMSS PATRONAL ADMON. SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2726
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2735,
                CatalogoCuentasModel::NO_CUENTA => 850009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SAR ADMON. SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2726
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2736,
                CatalogoCuentasModel::NO_CUENTA => 850010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT ADMON. SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2726
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2737,
                CatalogoCuentasModel::NO_CUENTA => 850011,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VARIOS HOJALATERIA Y PINTURA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2726
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2738,
                CatalogoCuentasModel::NO_CUENTA => 850012,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OXIGENO INDUSTRIAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2726
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2739,
                CatalogoCuentasModel::NO_CUENTA => 850013,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ROYOS FOTOGRAFICOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2726
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2740,
                CatalogoCuentasModel::NO_CUENTA => 850014,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS NO DEDUCIBLES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2726
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2741,
                CatalogoCuentasModel::NO_CUENTA => 850015,
                CatalogoCuentasModel::NOMBRE_CUENTA => "FERRETERIA Y TLAPALERIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2726
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2742,
                CatalogoCuentasModel::NO_CUENTA => 850016,
                CatalogoCuentasModel::NOMBRE_CUENTA => "NO DEDUCIBLES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2726
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2743,
                CatalogoCuentasModel::NO_CUENTA => 850017,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DIVERSOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2726
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2744,
                CatalogoCuentasModel::NO_CUENTA => 856003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PASAJES Y ESTACIONAMIENTO H. Y P.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2726
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2745,
                CatalogoCuentasModel::NO_CUENTA => 860000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REEXPRESION GTOS TALLER",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2746,
                CatalogoCuentasModel::NO_CUENTA => 900000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTROS GASTOS Y PRODUCTOS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2747,
                CatalogoCuentasModel::NO_CUENTA => 900001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES VENDEDORES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2748,
                CatalogoCuentasModel::NO_CUENTA => 900002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES SEGUROS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2749,
                CatalogoCuentasModel::NO_CUENTA => 900003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTRAS COMISIONES VENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2750,
                CatalogoCuentasModel::NO_CUENTA => 900004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUELDOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2751,
                CatalogoCuentasModel::NO_CUENTA => 900005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GRATIFICACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2752,
                CatalogoCuentasModel::NO_CUENTA => 900006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES (SUELDOS)",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2753,
                CatalogoCuentasModel::NO_CUENTA => 900007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TIEMPO EXTRA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2754,
                CatalogoCuentasModel::NO_CUENTA => 900008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DOMINICAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2755,
                CatalogoCuentasModel::NO_CUENTA => 900009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTROS ING.S\/SUELDOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2756,
                CatalogoCuentasModel::NO_CUENTA => 900010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA DE ANTIGUEDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2757,
                CatalogoCuentasModel::NO_CUENTA => 900011,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INDEMNIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2758,
                CatalogoCuentasModel::NO_CUENTA => 900012,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AGUINALDO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2759,
                CatalogoCuentasModel::NO_CUENTA => 900013,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VACACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2760,
                CatalogoCuentasModel::NO_CUENTA => 900014,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRIMA VACACIONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2761,
                CatalogoCuentasModel::NO_CUENTA => 900015,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMSS PATRONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2762,
                CatalogoCuentasModel::NO_CUENTA => 900016,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2763,
                CatalogoCuentasModel::NO_CUENTA => 900017,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2764,
                CatalogoCuentasModel::NO_CUENTA => 900018,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CREDITO AL SALARIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2765,
                CatalogoCuentasModel::NO_CUENTA => 900019,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UNIFORMES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2766,
                CatalogoCuentasModel::NO_CUENTA => 900020,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PREVISION SOCIAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2767,
                CatalogoCuentasModel::NO_CUENTA => 900021,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CAPACITACION Y ENTRENAMIENTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2768,
                CatalogoCuentasModel::NO_CUENTA => 900022,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE EDIFICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2769,
                CatalogoCuentasModel::NO_CUENTA => 900023,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2770,
                CatalogoCuentasModel::NO_CUENTA => 900024,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE TRANSPORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2771,
                CatalogoCuentasModel::NO_CUENTA => 900025,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE TALLER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2772,
                CatalogoCuentasModel::NO_CUENTA => 900026,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE TALLER",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2773,
                CatalogoCuentasModel::NO_CUENTA => 900027,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE OFICINA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2774,
                CatalogoCuentasModel::NO_CUENTA => 900028,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE TRANSPORTE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2775,
                CatalogoCuentasModel::NO_CUENTA => 900029,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2776,
                CatalogoCuentasModel::NO_CUENTA => 900030,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEPRECIACION DE EQUIPO DE COMUNICACION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2777,
                CatalogoCuentasModel::NO_CUENTA => 900031,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AMORTIZACION EDIFICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2778,
                CatalogoCuentasModel::NO_CUENTA => 900032,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASOLINA NUEVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2779,
                CatalogoCuentasModel::NO_CUENTA => 900033,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASOLINA UNIDADES EN USO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2780,
                CatalogoCuentasModel::NO_CUENTA => 900034,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE PREVIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2781,
                CatalogoCuentasModel::NO_CUENTA => 900035,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HERRAMIENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2782,
                CatalogoCuentasModel::NO_CUENTA => 900036,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PUBLICIDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2783,
                CatalogoCuentasModel::NO_CUENTA => 900037,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PROMOCIONES ESPECIALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2784,
                CatalogoCuentasModel::NO_CUENTA => 900038,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CORTESIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2785,
                CatalogoCuentasModel::NO_CUENTA => 900039,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CUOTAS Y SUSCRIPCIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2786,
                CatalogoCuentasModel::NO_CUENTA => 900040,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DONATIVOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2787,
                CatalogoCuentasModel::NO_CUENTA => 900041,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUBSIDIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2788,
                CatalogoCuentasModel::NO_CUENTA => 900042,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SUBSIDIO DE SEGUROS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2789,
                CatalogoCuentasModel::NO_CUENTA => 900043,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RENTA DE LOCALES Y TERRENOS P.F.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2790,
                CatalogoCuentasModel::NO_CUENTA => 900044,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RENTA DE LOCALES Y TERRENOS P.M.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2791,
                CatalogoCuentasModel::NO_CUENTA => 900045,
                CatalogoCuentasModel::NOMBRE_CUENTA => "TELEFONOS, FAX Y CORREOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2792,
                CatalogoCuentasModel::NO_CUENTA => 900046,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VIGILANCIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2793,
                CatalogoCuentasModel::NO_CUENTA => 900047,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CONSUMIBLES EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2794,
                CatalogoCuentasModel::NO_CUENTA => 900048,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS NO DEDUCIBLES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2795,
                CatalogoCuentasModel::NO_CUENTA => 900049,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PAPELERIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2796,
                CatalogoCuentasModel::NO_CUENTA => 900050,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SEGUROS Y FIANZAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2797,
                CatalogoCuentasModel::NO_CUENTA => 900051,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IMPUESTOS Y DERECHOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2798,
                CatalogoCuentasModel::NO_CUENTA => 900052,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MULTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2799,
                CatalogoCuentasModel::NO_CUENTA => 900053,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACTUALIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2800,
                CatalogoCuentasModel::NO_CUENTA => 900054,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECARGOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2801,
                CatalogoCuentasModel::NO_CUENTA => 900055,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE VIAJE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2802,
                CatalogoCuentasModel::NO_CUENTA => 900056,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REPARACION DA¤OS SERVICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2803,
                CatalogoCuentasModel::NO_CUENTA => 900057,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COPIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2804,
                CatalogoCuentasModel::NO_CUENTA => 900058,
                CatalogoCuentasModel::NOMBRE_CUENTA => "LIBROS Y REVISTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2805,
                CatalogoCuentasModel::NO_CUENTA => 900059,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PASAJES Y ESTACIONAMIENTOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2806,
                CatalogoCuentasModel::NO_CUENTA => 900060,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UTILES DE ASEO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2807,
                CatalogoCuentasModel::NO_CUENTA => 900061,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS FIN DE A¤O",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2808,
                CatalogoCuentasModel::NO_CUENTA => 900062,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECLUTAMIENTO Y SELECCION DE PERSONAL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2809,
                CatalogoCuentasModel::NO_CUENTA => 900063,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REDONDEOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2810,
                CatalogoCuentasModel::NO_CUENTA => 900064,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HONORARIOS PERSONAS FISICAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2811,
                CatalogoCuentasModel::NO_CUENTA => 900065,
                CatalogoCuentasModel::NOMBRE_CUENTA => "HONORARIOS PERSONAS MORALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2812,
                CatalogoCuentasModel::NO_CUENTA => 900066,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ASESORIA ADMINISTRATIVA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2813,
                CatalogoCuentasModel::NO_CUENTA => 900067,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ASESORIA EN VENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2814,
                CatalogoCuentasModel::NO_CUENTA => 900068,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AUDITORIA EXTERNA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2815,
                CatalogoCuentasModel::NO_CUENTA => 900069,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DIVERSOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2816,
                CatalogoCuentasModel::NO_CUENTA => 900070,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SEGUROS DE TRASLADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2817,
                CatalogoCuentasModel::NO_CUENTA => 900071,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ARRENDAMIENTO DE EQUIPO DE COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2818,
                CatalogoCuentasModel::NO_CUENTA => 900072,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AUDITORIA EXTERNA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2819,
                CatalogoCuentasModel::NO_CUENTA => 900073,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ENERGIA ELECTRICA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2820,
                CatalogoCuentasModel::NO_CUENTA => 900074,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AGUA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2821,
                CatalogoCuentasModel::NO_CUENTA => 900075,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECOLECCION DE VALORES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2822,
                CatalogoCuentasModel::NO_CUENTA => 900076,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AMORTIZACION LICENCIAS P\/COMPUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2823,
                CatalogoCuentasModel::NO_CUENTA => 900077,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SAR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2824,
                CatalogoCuentasModel::NO_CUENTA => 900078,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INFONAVIT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2825,
                CatalogoCuentasModel::NO_CUENTA => 900079,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MANTENIMIENTO EQUIPO DE OFICINA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2826,
                CatalogoCuentasModel::NO_CUENTA => 900080,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EVENTOS DISTRIBUIDORES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2827,
                CatalogoCuentasModel::NO_CUENTA => 900090,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS MEDICOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2828,
                CatalogoCuentasModel::NO_CUENTA => 900097,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS FIN DE A¥O",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2829,
                CatalogoCuentasModel::NO_CUENTA => 900098,
                CatalogoCuentasModel::NOMBRE_CUENTA => "AGUINALDO EMPLEADOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2830,
                CatalogoCuentasModel::NO_CUENTA => 900099,
                CatalogoCuentasModel::NOMBRE_CUENTA => "SERVICIO DE GRUAS ARRASTRE Y MANIOBRAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2831,
                CatalogoCuentasModel::NO_CUENTA => 900100,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DEP'N EQUIPO DE COMUNICACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2832,
                CatalogoCuentasModel::NO_CUENTA => 900101,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ENERGIA ELECTRICA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2746
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2833,
                CatalogoCuentasModel::NO_CUENTA => 910000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTROS GASTOS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2834,
                CatalogoCuentasModel::NO_CUENTA => 910001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES BANCARIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2833
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2835,
                CatalogoCuentasModel::NO_CUENTA => 910002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTROS GASTOS CON IVA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2833
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2836,
                CatalogoCuentasModel::NO_CUENTA => 910003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTROS GASTOS SIN IVA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2833
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2837,
                CatalogoCuentasModel::NO_CUENTA => 910004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECUPERACION GASTOS PUBLICIDAD AMDF",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2833
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2838,
                CatalogoCuentasModel::NO_CUENTA => 910005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACTUALIZACION OTRSO GASTOS (REEXPRESION)",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2833
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2839,
                CatalogoCuentasModel::NO_CUENTA => 911006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RESERVA PARA CUENTAS INCOBRABLES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2833
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2840,
                CatalogoCuentasModel::NO_CUENTA => 912000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REPOMO",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2841,
                CatalogoCuentasModel::NO_CUENTA => 920000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTROS PRODUCTOS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2842,
                CatalogoCuentasModel::NO_CUENTA => 920001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INGRESOS POR ACONDICIONAMIENTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2841
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2843,
                CatalogoCuentasModel::NO_CUENTA => 920002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INGRESOS POR COMISIONES COBRADAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2841
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2844,
                CatalogoCuentasModel::NO_CUENTA => 920003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INGRESOS POR UDIS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2841
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2845,
                CatalogoCuentasModel::NO_CUENTA => 920004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INGRESOS OTROS GRAVADOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2841
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2846,
                CatalogoCuentasModel::NO_CUENTA => 920005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INGRESOS OTROS NO GRAVADOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2841
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2847,
                CatalogoCuentasModel::NO_CUENTA => 920006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INGRESOS EXTENSION DE GARANTIA FORD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2841
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2848,
                CatalogoCuentasModel::NO_CUENTA => 920007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INGRESOS POR APERTURAS DE CREDITO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2841
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2849,
                CatalogoCuentasModel::NO_CUENTA => 920008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACTUALIZACION OTROS PRODUCTOS (REEXPRESION)",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2841
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2850,
                CatalogoCuentasModel::NO_CUENTA => 930000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS FINANCIEROS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2851,
                CatalogoCuentasModel::NO_CUENTA => 930001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INTERESES BANCARIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2850
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2852,
                CatalogoCuentasModel::NO_CUENTA => 930002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INTERESES PAGADOS P. FISICAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2850
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2853,
                CatalogoCuentasModel::NO_CUENTA => 930003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INTERESES PAGADOS P. MORALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2850
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2854,
                CatalogoCuentasModel::NO_CUENTA => 930004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INTERESES PLAN PISO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2850
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2855,
                CatalogoCuentasModel::NO_CUENTA => 930005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PERDIDA CAMBIARIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2850
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2856,
                CatalogoCuentasModel::NO_CUENTA => 930006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACTUAL. GASTOS FINANCIEROS (REEXPRESION)",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2850
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2857,
                CatalogoCuentasModel::NO_CUENTA => 940000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRODUCTOS FINANCIEROS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2858,
                CatalogoCuentasModel::NO_CUENTA => 940001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INTERESE BANCARIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2857
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2859,
                CatalogoCuentasModel::NO_CUENTA => 940002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INTERESES COBRADOS P. FISICAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2857
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2860,
                CatalogoCuentasModel::NO_CUENTA => 940003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INTERESES COBRADOS P. MORALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2857
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2861,
                CatalogoCuentasModel::NO_CUENTA => 940004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INTERESES COBRADOS A FORD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2857
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2862,
                CatalogoCuentasModel::NO_CUENTA => 940005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UTILIDAD CAMBIARIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2857
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2863,
                CatalogoCuentasModel::NO_CUENTA => 940006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACTUAL. PROD. FINANCIEROS (REEXPRESION)",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2857
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2864,
                CatalogoCuentasModel::NO_CUENTA => 950000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UNIDADES EN TRASPASO",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2865,
                CatalogoCuentasModel::NO_CUENTA => 950001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VENTAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2864
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2866,
                CatalogoCuentasModel::NO_CUENTA => 950002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PTU",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2864
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2867,
                CatalogoCuentasModel::NO_CUENTA => 950003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IETU",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2864
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2868,
                CatalogoCuentasModel::NO_CUENTA => 960000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RESULTADOS EN LA SUBSIDIARIA",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2869,
                CatalogoCuentasModel::NO_CUENTA => 960001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PARTICIPACION EN RESULTADOS DE SUBSIDIARIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2868
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2870,
                CatalogoCuentasModel::NO_CUENTA => 971000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTROS PRODUCTOS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2871,
                CatalogoCuentasModel::NO_CUENTA => 971001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INGRESOS POR APERTURA DE CREDITO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2870
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2872,
                CatalogoCuentasModel::NO_CUENTA => 971002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INGRESOS POR ACONDICIONAMIENTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2870
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2873,
                CatalogoCuentasModel::NO_CUENTA => 971003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INGRESOS POR COMISION COBRADA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2870
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2874,
                CatalogoCuentasModel::NO_CUENTA => 971004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INGRESOS POR REEXPRESION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2870
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2875,
                CatalogoCuentasModel::NO_CUENTA => 971005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INGRESOS POR UDIïS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2870
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2876,
                CatalogoCuentasModel::NO_CUENTA => 971006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INGRESOS POR ACTUALIZACIONES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2870
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2877,
                CatalogoCuentasModel::NO_CUENTA => 971007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INGRESOS OTROS GRAVADOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2870
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2878,
                CatalogoCuentasModel::NO_CUENTA => 971008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INGRESOS OTROS NO GRAVADOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2870
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2879,
                CatalogoCuentasModel::NO_CUENTA => 971009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECUPERACION DE PUBLICIDAD (AMDF)",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2870
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2880,
                CatalogoCuentasModel::NO_CUENTA => 971010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INGRESOS POR RECUPERACION CONAUTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2870
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2881,
                CatalogoCuentasModel::NO_CUENTA => 971011,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INGRESOS CAFETERIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2870
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2882,
                CatalogoCuentasModel::NO_CUENTA => 971100,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTROS PRODUCTOS SJR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2870
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2883,
                CatalogoCuentasModel::NO_CUENTA => 971101,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INGRESOS POR APERTURA DE CREDITO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2870
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2884,
                CatalogoCuentasModel::NO_CUENTA => 971102,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INGRESOS POR ACONDICIONAMIENTO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2870
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2885,
                CatalogoCuentasModel::NO_CUENTA => 971103,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INGRESOS POR COMISION COBRADA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2870
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2886,
                CatalogoCuentasModel::NO_CUENTA => 972000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTROS GASTOS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2887,
                CatalogoCuentasModel::NO_CUENTA => 972001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "1% APERTURA DE CREDITO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2886
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2888,
                CatalogoCuentasModel::NO_CUENTA => 972002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES BANCARIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2886
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2889,
                CatalogoCuentasModel::NO_CUENTA => 972003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTROS GASTOS CON IVA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2886
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2890,
                CatalogoCuentasModel::NO_CUENTA => 972004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "OTROS GASTOS SIN IVA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2886
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2891,
                CatalogoCuentasModel::NO_CUENTA => 972005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ISR DIFERIDO RESULTADOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2886
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2892,
                CatalogoCuentasModel::NO_CUENTA => 972006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PUBLICIDAD Y ACCESORIOS (AMDF)",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2886
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2893,
                CatalogoCuentasModel::NO_CUENTA => 972009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACTUALIZACION OTROS GASTOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2886
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2894,
                CatalogoCuentasModel::NO_CUENTA => 972500,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INTERESES POR INVERSIONES EN V",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2886
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2895,
                CatalogoCuentasModel::NO_CUENTA => 973000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRODUCTOS FINANCIEROS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2896,
                CatalogoCuentasModel::NO_CUENTA => 973001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INTERESES BANCARIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2895
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2897,
                CatalogoCuentasModel::NO_CUENTA => 973002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INTERESES COB. P.FISICAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2895
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2898,
                CatalogoCuentasModel::NO_CUENTA => 973003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INTERESES COB. P. MORALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2895
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2899,
                CatalogoCuentasModel::NO_CUENTA => 973004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INTERESES COB. FORD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2895
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2900,
                CatalogoCuentasModel::NO_CUENTA => 973005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UTILIDAD EN TIPO DE CAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2895
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2901,
                CatalogoCuentasModel::NO_CUENTA => 974000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS FINANCIEROS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2902,
                CatalogoCuentasModel::NO_CUENTA => 974001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INTERESES BANCARIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2901
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2903,
                CatalogoCuentasModel::NO_CUENTA => 974002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INTERESES PAGADOS P. FISICAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2901
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2904,
                CatalogoCuentasModel::NO_CUENTA => 974003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INTERESES PAGADOS P. MORALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2901
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2905,
                CatalogoCuentasModel::NO_CUENTA => 974004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INTERESES PLAN PISO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2901
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2906,
                CatalogoCuentasModel::NO_CUENTA => 974005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PERDIDA EN TIPO DE CAMBIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2901
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2907,
                CatalogoCuentasModel::NO_CUENTA => 974006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INGRESOS POR ACT. DE IMPUESTOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2901
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2908,
                CatalogoCuentasModel::NO_CUENTA => 974007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DIVIDENDOS COBRADOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2901
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2909,
                CatalogoCuentasModel::NO_CUENTA => 974008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "MEDIACION MERCANTIL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2901
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2910,
                CatalogoCuentasModel::NO_CUENTA => 974009,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UTILIDAD CAMBIARIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2901
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2911,
                CatalogoCuentasModel::NO_CUENTA => 974010,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PROD.DIV.SEGURO DE UNIDAD O.R.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2901
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2912,
                CatalogoCuentasModel::NO_CUENTA => 974011,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PROD.DIV.INT.GAN.CRED.PLAN S\/RIESGO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2901
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2913,
                CatalogoCuentasModel::NO_CUENTA => 974012,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXENTO P\/IVA PROD. DIV. VERIF. CONTAM.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2901
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2914,
                CatalogoCuentasModel::NO_CUENTA => 974013,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PROD. X RECUPERACION DE DIF. PA-293",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2901
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2915,
                CatalogoCuentasModel::NO_CUENTA => 974014,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PRODUCTOS DIV. OTROS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2901
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2916,
                CatalogoCuentasModel::NO_CUENTA => 974015,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INGRESOS DIVERSOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2901
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2917,
                CatalogoCuentasModel::NO_CUENTA => 974016,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PROD. DIV. IMPUESTO DE IMPORT",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2901
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2918,
                CatalogoCuentasModel::NO_CUENTA => 974017,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PROD.DIV.ING.X ACT.DE IMP.(NO ACUM.)P\/ISR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2901
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2919,
                CatalogoCuentasModel::NO_CUENTA => 974018,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PROD.DIV.ING. NO ACUM.PARA ISR",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2901
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2920,
                CatalogoCuentasModel::NO_CUENTA => 974019,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PROD. DIVERSOS DIVIDENDOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2901
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2921,
                CatalogoCuentasModel::NO_CUENTA => 974020,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PROD. DIV. MEDIACION MERCANTIL",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2901
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2922,
                CatalogoCuentasModel::NO_CUENTA => 974021,
                CatalogoCuentasModel::NOMBRE_CUENTA => "1% APERTURA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2901
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2923,
                CatalogoCuentasModel::NO_CUENTA => 974022,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UTILIDAD O PERDIDA X VTA.ACT.FIJO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2901
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2924,
                CatalogoCuentasModel::NO_CUENTA => 974023,
                CatalogoCuentasModel::NOMBRE_CUENTA => "UTILIDAD CAMBIARIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2901
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2925,
                CatalogoCuentasModel::NO_CUENTA => 974024,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PROD. X REMBOLSO MTTO. SERVICIO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2901
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2926,
                CatalogoCuentasModel::NO_CUENTA => 974025,
                CatalogoCuentasModel::NOMBRE_CUENTA => "REEXPRESION DE OTROS PRODUCTOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2901
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2927,
                CatalogoCuentasModel::NO_CUENTA => 974026,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PROD. DIV. RENAVE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2901
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2928,
                CatalogoCuentasModel::NO_CUENTA => 974027,
                CatalogoCuentasModel::NOMBRE_CUENTA => "METODO DE PARTICIPACION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2901
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2929,
                CatalogoCuentasModel::NO_CUENTA => 974028,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXENTO PROD. DIV. 1% APERT. DE CRED.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2901
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2930,
                CatalogoCuentasModel::NO_CUENTA => 974029,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RECUPERACION PUBLICIDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2901
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2931,
                CatalogoCuentasModel::NO_CUENTA => 974030,
                CatalogoCuentasModel::NOMBRE_CUENTA => "INGRESOS UDI'S",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2901
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2932,
                CatalogoCuentasModel::NO_CUENTA => 974031,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISION POR COBRO CON TARJETA CREDITO.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2901
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2933,
                CatalogoCuentasModel::NO_CUENTA => 974039,
                CatalogoCuentasModel::NOMBRE_CUENTA => "ACTUALIZACION GTOS FINANCIEROS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2901
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2934,
                CatalogoCuentasModel::NO_CUENTA => 974101,
                CatalogoCuentasModel::NOMBRE_CUENTA => "VENTAS PLAN GANE.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2901
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2935,
                CatalogoCuentasModel::NO_CUENTA => 975000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DESCUENTOS CONCEDIDOS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2936,
                CatalogoCuentasModel::NO_CUENTA => 975001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GTOS. TRAMITES LEGALES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2935
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2937,
                CatalogoCuentasModel::NO_CUENTA => 975002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RESERVA P\/CTAS. INCOBRABLES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2935
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2938,
                CatalogoCuentasModel::NO_CUENTA => 975003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS. NO DEDUCIBLES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2935
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2939,
                CatalogoCuentasModel::NO_CUENTA => 975004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "COMISIONES BANCARIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2935
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2940,
                CatalogoCuentasModel::NO_CUENTA => 975005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PERDIDA CAMBIARIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2935
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2941,
                CatalogoCuentasModel::NO_CUENTA => 975006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE FIN DE A¥O",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2935
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2942,
                CatalogoCuentasModel::NO_CUENTA => 975007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "IVA NO ACREDITABLE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2935
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2943,
                CatalogoCuentasModel::NO_CUENTA => 975012,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DIVERSOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2935
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2944,
                CatalogoCuentasModel::NO_CUENTA => 976000,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DIVERSOS",
                CatalogoCuentasModel::PRINCIPAL => 1,
                CatalogoCuentasModel::SUBCUENTA => 0
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2945,
                CatalogoCuentasModel::NO_CUENTA => 976001,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXENTO P\/IVA GTOS.DIV.TRAMITES LEG.UN",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2944
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2946,
                CatalogoCuentasModel::NO_CUENTA => 976002,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXENTO P\/IVA GASTOS DIVERSOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2944
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2947,
                CatalogoCuentasModel::NO_CUENTA => 976003,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GTOS.DIV.TRAMITES LEGALES U.N.",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2944
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2948,
                CatalogoCuentasModel::NO_CUENTA => 976004,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DIVERSOS RENAVE",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2944
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2949,
                CatalogoCuentasModel::NO_CUENTA => 976005,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GTOS. DIV. EQPO. OPCIONAL MULTIVA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2944
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2950,
                CatalogoCuentasModel::NO_CUENTA => 976006,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GTOS. DIV. COBRANZA E INF. DE ",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2944
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2951,
                CatalogoCuentasModel::NO_CUENTA => 976007,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GTOS. MAT.DE REFACCS. OBSOLETO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2944
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2952,
                CatalogoCuentasModel::NO_CUENTA => 976008,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GTOS. RESERVA PRIMA DE ANTIGUEDAD",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2944
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2953,
                CatalogoCuentasModel::NO_CUENTA => 976013,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RESERVA P\/CTAS. INCOBRABLES",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2944
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2954,
                CatalogoCuentasModel::NO_CUENTA => 976014,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GTOS. DIV. NO DEDUCIBLES P\/I.S",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2944
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2955,
                CatalogoCuentasModel::NO_CUENTA => 976015,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GTOS. DIV. COMIÿIONES BANCARIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2944
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2956,
                CatalogoCuentasModel::NO_CUENTA => 976016,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GTOS. DIVERSOS OTROS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2944
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2957,
                CatalogoCuentasModel::NO_CUENTA => 976017,
                CatalogoCuentasModel::NOMBRE_CUENTA => "RVA.PARA INVENTARIOS OBSOLETOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2944
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2958,
                CatalogoCuentasModel::NO_CUENTA => 976018,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GTOS. POR TRASLADO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2944
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2959,
                CatalogoCuentasModel::NO_CUENTA => 976019,
                CatalogoCuentasModel::NOMBRE_CUENTA => "G. D. IMPUESTOS DE IMPORTACION",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2944
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2960,
                CatalogoCuentasModel::NO_CUENTA => 976020,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PERDIDA CAMBIARIA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2944
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2961,
                CatalogoCuentasModel::NO_CUENTA => 976021,
                CatalogoCuentasModel::NOMBRE_CUENTA => "DIFERENCIA EN INVENTARIOS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2944
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2962,
                CatalogoCuentasModel::NO_CUENTA => 976022,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS NO DEDUCIBLES (POR COMIDAS)",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2944
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2963,
                CatalogoCuentasModel::NO_CUENTA => 976023,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GASTOS DE FIN DE A¥O",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2944
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2964,
                CatalogoCuentasModel::NO_CUENTA => 976024,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXENTO P\/IVA GTOS.DIV.COMIS.BANCARIAS",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2944
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2965,
                CatalogoCuentasModel::NO_CUENTA => 976025,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXENTO P\/IVA GASTOS DE FIN DE A¥O",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2944
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2966,
                CatalogoCuentasModel::NO_CUENTA => 976026,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CUENTA DEPURADORA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2944
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2967,
                CatalogoCuentasModel::NO_CUENTA => 976027,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GTOS. DIV. TRASP. I.V.A. X PRORRATEO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2944
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2968,
                CatalogoCuentasModel::NO_CUENTA => 976028,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PART.RESULTADOS ACUM.DE LA ASOCIADA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2944
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2969,
                CatalogoCuentasModel::NO_CUENTA => 976025,
                CatalogoCuentasModel::NOMBRE_CUENTA => "EXENTO P\/IVA GASTOS DE FIN DE AÑO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2944
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2970,
                CatalogoCuentasModel::NO_CUENTA => 976026,
                CatalogoCuentasModel::NOMBRE_CUENTA => "CUENTA DEPURADORA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2944
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2971,
                CatalogoCuentasModel::NO_CUENTA => 976027,
                CatalogoCuentasModel::NOMBRE_CUENTA => "GTOS. DIV. TRASP. I.V.A. X PRORRATEO",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2944
            ],
            [
                CatalogoCuentasModel::ID_MOVIMIENTO => 2972,
                CatalogoCuentasModel::NO_CUENTA => 976028,
                CatalogoCuentasModel::NOMBRE_CUENTA => "PART.RESULTADOS ACUM.DE LA ASOCIADA",
                CatalogoCuentasModel::PRINCIPAL => 0,
                CatalogoCuentasModel::SUBCUENTA => 2944
            ]
        ];

        foreach ($data as $key => $items) {
            DB::table(CatalogoCuentasModel::getTableName())->insert($items);
        }
    }
}
