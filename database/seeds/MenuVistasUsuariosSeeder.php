<?php

use App\Models\Usuarios\MenuUsuariosVistasModel;
use App\Models\Usuarios\UsuariosModulosModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenuVistasUsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $filename = 'csv/menu_usuario_vista.csv';
        $file_path = base_path('database/' . $filename);

        if (file_exists($file_path)) {
            $file = fopen($file_path, 'r');
            while (($data = fgetcsv($file)) !== false) {

                $usuario_id = trim($data[1]);
                $seccion_id = trim($data[2]);
                $submenu_id = trim($data[3]);
                $vista_id = trim($data[4]);

                MenuUsuariosVistasModel::create([
                    MenuUsuariosVistasModel::USUARIO_ID => $usuario_id,
                    MenuUsuariosVistasModel::SECCION_ID => $seccion_id,
                    MenuUsuariosVistasModel::SUBMENU_ID => $submenu_id,
                    MenuUsuariosVistasModel::VISTA_ID => $vista_id
                ]);
            }
        }
    }
}
