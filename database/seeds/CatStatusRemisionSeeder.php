<?php

use App\Models\Autos\UnidadesNuevas\CatStatusRemision;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatStatusRemisionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                CatStatusRemision::ID => 1,
                CatStatusRemision::ESTATUS => 'Disponible'
            ],
            [
                CatStatusRemision::ID => 2,
                CatStatusRemision::ESTATUS => 'Apartada'
            ]
        ];

        foreach ($data as $value) {
            DB::table(CatStatusRemision::getTableName())->insert($value);
        }
    }
}
