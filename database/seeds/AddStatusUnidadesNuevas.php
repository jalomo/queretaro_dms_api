<?php

use App\Models\Autos\UnidadesNuevas\CatEstatusUNModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AddStatusUnidadesNuevas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                CatEstatusUNModel::ID => 2,
                CatEstatusUNModel::ESTATUS => 'TRÁNSITO'
            ],
            [
                CatEstatusUNModel::ID => 3,
                CatEstatusUNModel::ESTATUS => 'EXHIBICIÓN'
            ],
            [
                CatEstatusUNModel::ID => 4,
                CatEstatusUNModel::ESTATUS => 'DEMO'
            ],
            [
                CatEstatusUNModel::ID => 5,
                CatEstatusUNModel::ESTATUS => 'EN TALLER'
            ],
            [
                CatEstatusUNModel::ID => 6,
                CatEstatusUNModel::ESTATUS => 'PUNTO DE VENTA'
            ],

        ];

        foreach ($data as $value) {
            DB::table(CatEstatusUNModel::getTableName())->insert($value);
        }
    }
}
