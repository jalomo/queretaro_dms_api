<?php

use App\Models\Refacciones\Almacenes;
use App\Models\Refacciones\ClientesModel;
use App\Models\Refacciones\EstatusCompra;
use App\Models\Refacciones\Talleres;
use App\Models\Refacciones\VendedorModel;
use App\Models\Refacciones\ClaveClienteModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GlobalSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table(VendedorModel::getTableName())->insert([
            VendedorModel::NOMBRE => 'Vendedor '
        ]);
        
       

        $talleres = [
            [
                Talleres::ID => 1,
                Talleres::NOMBRE => 'Taller 1',
                Talleres::UBICACION => 'colima'
            ],
            [
                Talleres::ID => 2,
                Talleres::NOMBRE => 'Taller 2',
                Talleres::UBICACION => 'colima'
            ],
            [
                Talleres::ID => 3,
                Talleres::NOMBRE => 'Taller 3',
                Talleres::UBICACION => 'colima'
            ]
        ];

        foreach ($talleres as $key => $taller) {
            DB::table(Talleres::getTableName())->insert([
                Talleres::NOMBRE => $taller[Talleres::NOMBRE],
                Talleres::UBICACION => $taller[Talleres::UBICACION]
            ]);
        }

        $almacenes = [
            [
                Almacenes::ID => 1,
                Almacenes::NOMBRE => 'Almacen queretaro',
                Almacenes::UBICACION => 'Queretaro',
                Almacenes::CODIGO_ALMACEN => 'Qrt'
            ],
            [
                Almacenes::ID => 2,
                Almacenes::NOMBRE => 'Almacen San Juan del Rio',
                Almacenes::UBICACION => 'San Juan del Rio',
                Almacenes::CODIGO_ALMACEN => 'SJR'
            ]
        ];

        foreach ($almacenes as $key => $value) {
            DB::table(Almacenes::getTableName())->insert([
                Almacenes::CODIGO_ALMACEN => $value[Almacenes::CODIGO_ALMACEN],
                Almacenes::NOMBRE => $value[Almacenes::NOMBRE],
                Almacenes::UBICACION => $value[Almacenes::UBICACION]
            ]);
        }
    }
}
