<?php

use App\Models\Autos\EstatusSalidaUnidadModel;
use App\Models\Autos\UnidadesModel;
use App\Models\Autos\UnidadesNuevas\CatCombustible;
use App\Models\Autos\UnidadesNuevas\CatLineas;
use App\Models\Autos\UnidadesNuevas\CatProveedoresUN;
use App\Models\Autos\UnidadesNuevas\DetalleCostosRemisionModel;
use App\Models\Autos\UnidadesNuevas\DetalleRemisionModel;
use App\Models\Autos\UnidadesNuevas\FacturacionModel;
use App\Models\Autos\UnidadesNuevas\MemoCompraModel;
use App\Models\Autos\UnidadesNuevas\RemisionModel;
use App\Models\Refacciones\CatalogoUbicacionModel;
use App\Models\Refacciones\UbicacionLLavesModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatalogoUnidadesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $cat_estatus_salida =  [
            [EstatusSalidaUnidadModel::NOMBRE => 'DISPONIBLE'],
            [EstatusSalidaUnidadModel::NOMBRE => 'APARTADA'],
            [EstatusSalidaUnidadModel::NOMBRE => 'VENDIDA']
        ];

        DB::table(EstatusSalidaUnidadModel::getTableName())
            ->insert($cat_estatus_salida);

        $cat_linea =  [
            CatLineas::MODELO => date('Y'),
            CatLineas::DESCRIPCION => 'Explorer',
            CatLineas::LINEA => 'A'
        ];
        DB::table(CatLineas::getTableName())->insert($cat_linea);
        $cat_linea =  [
            CatLineas::MODELO => date('Y'),
            CatLineas::DESCRIPCION => 'LOBO',
            CatLineas::LINEA => 'B'
        ];
        DB::table(CatLineas::getTableName())->insert($cat_linea);
        $cat_linea =  [
            CatLineas::MODELO => 2022,
            CatLineas::DESCRIPCION => 'RANGER',
            CatLineas::LINEA => 'R'
        ];
        DB::table(CatLineas::getTableName())->insert($cat_linea);
        $cat_linea =  [
            CatLineas::MODELO => date('Y'),
            CatLineas::DESCRIPCION => 'MUSTANG',
            CatLineas::LINEA => 'MU'
        ];
        DB::table(CatLineas::getTableName())->insert($cat_linea);
        


        $cat_proveedor_unidades =  [
            CatProveedoresUN::CLAVE => "Ford",
            CatProveedoresUN::NOMBRE => 'Proveedor ford'
        ];

        DB::table(CatProveedoresUN::getTableName())->insert($cat_proveedor_unidades);

        //Unidades
        $remision = [
            [
                RemisionModel::LINEA_ID => 1,
                RemisionModel::UNIDAD_DESCRIPCION => "Descripcion larga unidad",
                RemisionModel::ECONOMICO => 15505454872,
                RemisionModel::UNIDAD_IMPORTADA => 1,
                RemisionModel::CAT => 1,
                RemisionModel::CLAVE_VEHICULAR => 'clave',
                RemisionModel::TIPO_AUTO => 1,
                RemisionModel::CLAVE_ISAN => 1,
                RemisionModel::CTA_MENUDO => 1,
                RemisionModel::CTA_FLOTILLA => 1,
                RemisionModel::CTA_CONAUTO => 1,
                RemisionModel::CTA_INTERCAMBIO => 1,
                RemisionModel::CTA_PLLENO => 1,
                RemisionModel::CTA_INVENTARIO => 1,
                RemisionModel::VTA_MENUDEO => 1,
                RemisionModel::VTA_FLOTILLA => 1,
                RemisionModel::VTA_CONAUTO => 1,
                RemisionModel::VTA_INTERCAMBIO => 1,
                RemisionModel::VTA_PLLENO => 1,
                RemisionModel::USERID => 1,
                RemisionModel::C_SUBTOTAL => 600000,
                RemisionModel::C_TOTAL => 700000,
                RemisionModel::C_IVA => 1,
                RemisionModel::FECHA_PEDIMENTO => date('Y-m-d'),
                RemisionModel::PEDIMENTO => 1,
                RemisionModel::FECHA_REMISION => date('Y-m-d'),
                RemisionModel::SERIE => 1,
                RemisionModel::SERIE_CORTA => 1,
                RemisionModel::MOTOR => 1,
                RemisionModel::INTERCAMBIO => 1,
                RemisionModel::PROVEEDOR_ID => 1,
                RemisionModel::ESTATUS_ID => 1,
                RemisionModel::ID_STATUS_UNIDAD => 1,
                RemisionModel::UBICACION_ID => 1,
                RemisionModel::UBICACION_LLAVES_ID => 1
            ]
        ];

        foreach ($remision as $key => $item) {
            DB::table(RemisionModel::getTableName())->insert([
                RemisionModel::LINEA_ID => $item[RemisionModel::LINEA_ID],
                RemisionModel::ECONOMICO => $item[RemisionModel::ECONOMICO],
                RemisionModel::CAT => $item[RemisionModel::CAT],
                RemisionModel::CLAVE_VEHICULAR => $item[RemisionModel::CLAVE_VEHICULAR],
                RemisionModel::UNIDAD_DESCRIPCION => $item[RemisionModel::UNIDAD_DESCRIPCION],
                RemisionModel::UNIDAD_IMPORTADA => $item[RemisionModel::UNIDAD_IMPORTADA],
                RemisionModel::TIPO_AUTO => $item[RemisionModel::TIPO_AUTO],
                RemisionModel::CLAVE_ISAN => $item[RemisionModel::CLAVE_ISAN],
                RemisionModel::CTA_MENUDO => $item[RemisionModel::CTA_MENUDO],
                RemisionModel::CTA_FLOTILLA => $item[RemisionModel::CTA_FLOTILLA],
                RemisionModel::CTA_CONAUTO => $item[RemisionModel::CTA_CONAUTO],
                RemisionModel::CTA_INTERCAMBIO => $item[RemisionModel::CTA_INTERCAMBIO],
                RemisionModel::CTA_PLLENO => $item[RemisionModel::CTA_PLLENO],
                RemisionModel::CTA_INVENTARIO => $item[RemisionModel::CTA_INVENTARIO],
                RemisionModel::VTA_MENUDEO => $item[RemisionModel::VTA_MENUDEO],
                RemisionModel::VTA_FLOTILLA => $item[RemisionModel::VTA_FLOTILLA],
                RemisionModel::VTA_CONAUTO => $item[RemisionModel::VTA_CONAUTO],
                RemisionModel::VTA_INTERCAMBIO => $item[RemisionModel::VTA_INTERCAMBIO],
                RemisionModel::VTA_PLLENO => $item[RemisionModel::VTA_PLLENO],
                RemisionModel::USERID => $item[RemisionModel::USERID],
                RemisionModel::C_SUBTOTAL => $item[RemisionModel::C_SUBTOTAL],
                RemisionModel::C_TOTAL => $item[RemisionModel::C_TOTAL],
                RemisionModel::C_IVA => $item[RemisionModel::C_IVA],
                RemisionModel::FECHA_PEDIMENTO => $item[RemisionModel::FECHA_PEDIMENTO],
                RemisionModel::PEDIMENTO => $item[RemisionModel::PEDIMENTO],
                RemisionModel::FECHA_REMISION => $item[RemisionModel::FECHA_REMISION],
                RemisionModel::SERIE => $item[RemisionModel::SERIE],
                RemisionModel::SERIE_CORTA => $item[RemisionModel::SERIE_CORTA],
                RemisionModel::MOTOR => $item[RemisionModel::MOTOR],
                RemisionModel::INTERCAMBIO => $item[RemisionModel::INTERCAMBIO],
                RemisionModel::PROVEEDOR_ID => $item[RemisionModel::PROVEEDOR_ID],
                RemisionModel::ESTATUS_ID => $item[RemisionModel::ESTATUS_ID],
                RemisionModel::ID_STATUS_UNIDAD => $item[RemisionModel::ID_STATUS_UNIDAD],
                RemisionModel::UBICACION_ID => $item[RemisionModel::UBICACION_ID],
                RemisionModel::UBICACION_LLAVES_ID => $item[RemisionModel::UBICACION_LLAVES_ID]
            ]);
        }

        $detalle_resmision =  [
            DetalleRemisionModel::REMISIONID => 1,
            DetalleRemisionModel::PUERTAS => 4,
            DetalleRemisionModel::CILINDROS => 8,
            DetalleRemisionModel::TRANSMISION => 'Automatica',
            DetalleRemisionModel::COMBUSTIBLE_ID => 1,
            DetalleRemisionModel::CAPACIDAD => '4 personas',
            DetalleRemisionModel::COLORINTID => 1,
            DetalleRemisionModel::COLOREXTID => 1,
        ];
        DB::table(DetalleRemisionModel::getTableName())->insert($detalle_resmision);

        $detalle_costo_resmision =  [
            DetalleCostosRemisionModel::REMISIONID => 1,
            DetalleCostosRemisionModel::C_VALOR_UNIDAD => 650000,
            DetalleCostosRemisionModel::C_EQUIPO_BASE => 1,
            DetalleCostosRemisionModel::C_TOTAL_BASE => 1,
            DetalleCostosRemisionModel::C_DEDUCCION_FORD => 1,
            DetalleCostosRemisionModel::C_SEG_TRASLADO => 1,
            DetalleCostosRemisionModel::C_GASTOS_TRASLADO => 1,
            DetalleCostosRemisionModel::C_IMP_IMPORT => 1,
            DetalleCostosRemisionModel::C_FLETES_EXT => 1,
            // DetalleCostosRemisionModel::C_ISAN => 1,
            DetalleCostosRemisionModel::C_HOLDBACK => 1,
            DetalleCostosRemisionModel::C_CUENTA_PUB => 1,
            DetalleCostosRemisionModel::C_DONATIVO_CCF => 1,
            DetalleCostosRemisionModel::C_PLAN_PISO => 1
        ];

        DB::table(DetalleCostosRemisionModel::getTableName())->insert($detalle_costo_resmision);

        $memo_compra =  [
            MemoCompraModel::REMISION_ID => 1,
            MemoCompraModel::CM => 1,
            MemoCompraModel::MES => 1,
            MemoCompraModel::NO_PRODUCTO => 1,
            MemoCompraModel::MEMO => 1,
            MemoCompraModel::COSTO_REMISION => 1,
            MemoCompraModel::COMPRAR => 1,
            MemoCompraModel::MES_COMPRA => 1,
            MemoCompraModel::FP => 1,
            MemoCompraModel::DIAS => 1
        ];

        DB::table(MemoCompraModel::getTableName())->insert($memo_compra);

        $facturacion_unidades =  [
            FacturacionModel::REMISION_ID => 1,
            FacturacionModel::PROCEDENCIA => 1,
            FacturacionModel::VENDEDOR => 1,
            FacturacionModel::ADUANA_ID => 1,
            FacturacionModel::REPUVE => 1
        ];

        DB::table(FacturacionModel::getTableName())->insert($facturacion_unidades);
    }
}
