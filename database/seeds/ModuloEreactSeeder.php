<?php

use App\Models\Usuarios\Menu\ModulosModel;
use App\Models\Usuarios\MenuSeccionesModel;
use App\Models\Usuarios\MenuSubmenuModel;
use App\Models\Usuarios\MenuVistasModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ModuloEreactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $moudulo_id = DB::table(ModulosModel::getTableName())->insertGetId([
            ModulosModel::NOMBRE =>  'Reportes',
            ModulosModel::ICONO =>  'fas fa-file-signature',
            ModulosModel::ORDEN => '25',
            ModulosModel::DEFAULT =>  0
        ]);

        $seccion_id = DB::table(MenuSeccionesModel::getTableName())->insertGetId([
            MenuSeccionesModel::NOMBRE => 'Ereact',
            MenuSeccionesModel::MODULO_ID => $moudulo_id
        ]);
		
		$salidas_ereact_id = DB::table(MenuSubmenuModel::getTableName())->insertGetId([
            MenuSubmenuModel::NOMBRE => "Exportar ereact",
            MenuSubmenuModel::SECCION_ID => $seccion_id,
            MenuSubmenuModel::VISIBLE =>  1
        ]);

        $vistas = [
            [
                MenuVistasModel::NOMBRE => "Salidas",
                MenuVistasModel::ES_EXTERNA =>  FALSE,
                MenuVistasModel::LINK =>  'salidas',
                MenuVistasModel::CONTROLADOR =>  'ereact',
                MenuVistasModel::MODULO => 'ereact',
                MenuVistasModel::SUBMENU_ID => $salidas_ereact_id
            ],
            [
                MenuVistasModel::NOMBRE => 'Compras',
                MenuVistasModel::ES_EXTERNA =>  FALSE,
                MenuVistasModel::LINK =>  'compras',
                MenuVistasModel::CONTROLADOR =>  'ereact',
                MenuVistasModel::MODULO => 'ereact',
                MenuVistasModel::SUBMENU_ID => $salidas_ereact_id
            ],
            [
                MenuVistasModel::NOMBRE => 'Inventario',
                MenuVistasModel::ES_EXTERNA =>  FALSE,
                MenuVistasModel::LINK =>  'inventario',
                MenuVistasModel::CONTROLADOR =>  'ereact',
                MenuVistasModel::MODULO => 'ereact',
                MenuVistasModel::SUBMENU_ID => $salidas_ereact_id
            ],
        ];

        foreach ($vistas as $key => $items) {
            DB::table(MenuVistasModel::getTableName())->insert($items);
        }
    }
}
