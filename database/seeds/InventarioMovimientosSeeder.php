<?php

use Illuminate\Database\Seeder;
use App\Models\Refacciones\TipoMovimiento;

class InventarioMovimientosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $movimientos[] = ['id' => 1, 'nombre' => 'COMPRA'];
        $movimientos[] = ['id' => 2, 'nombre' => 'VENTA'];
        $movimientos[] = ['id' => 3, 'nombre' => 'DEVOLUCION VENTA MOSTRADOR'];
        $movimientos[] = ['id' => 4, 'nombre' => 'TRASPASO'];
        $movimientos[] = ['id' => 5, 'nombre' => 'CANCELACION_TRASPASO'];
        $movimientos[] = ['id' => 6, 'nombre' => 'PEDIDO'];
        $movimientos[] = ['id' => 7, 'nombre' => 'DEVOLUCION_PEDIDO'];
        $movimientos[] = ['id' => 8, 'nombre' => 'APARTADO'];
        $movimientos[] = ['id' => 9, 'nombre' => 'DESAPARTADO'];
        $movimientos[] = ['id' => 10, 'nombre' => 'VENTA DETALLES'];
        $movimientos[] = ['id' => 11, 'nombre' => 'DEVOLUCION VENTA DETALLES'];
        $movimientos[] = ['id' => 12, 'nombre' => 'AJUSTE AUDITORIA INVENTARIO AUMENTO'];
        $movimientos[] = ['id' => 13, 'nombre' => 'AJUSTE AUDITORIA INVENTARIO DISMINUCIÓN'];
        $movimientos[] = ['id' => 14, 'nombre' => 'EN PROCESO VENTA MOSTRADOR'];
        $movimientos[] = ['id' => 15, 'nombre' => 'EN PROCESO VENTA DETALLES'];
        $movimientos[] = ['id' => 16, 'nombre' => 'EN PROCESO DEVOLUCION DETALLES'];
        $movimientos[] = ['id' => 17, 'nombre' => 'EN PROCESO APARTAR'];
        $movimientos[] = ['id' => 18, 'nombre' => 'EN PROCESO AUDITORIA DISMINUCION'];
        $movimientos[] = ['id' => 19, 'nombre' => 'EN PROCESO TRASPASO'];

        foreach($movimientos as $movimiento)
        {
            TipoMovimiento::create($movimiento);
        }
    }
}
