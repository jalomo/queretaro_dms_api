<?php

use App\Models\Refacciones\CatalogoUbicacionProductoModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatalogoUbicacionesProductoSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$data = [
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A97T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02A3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01I3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A57B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL125"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "HYP1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A58B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A49D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A70T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV09E4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A60T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL1-51"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A96B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05A1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A22T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A11L2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL111"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A65C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A105E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A27D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL134"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A61T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A61A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A105A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A09H6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14G5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02A6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A75E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A105B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01A2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A84F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A58E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03C2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A07G5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A66F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01I4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A87F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A58T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A31F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03F5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A26A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV01C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05E5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A07H2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01F2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A10L2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A33A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A57F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A07L3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A73T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A75A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A10G5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A39A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01I6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A76F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A98A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02G6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A07D5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A09I4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A100D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A11H5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A09K1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL128"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A85E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A40F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL1-55"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A11F6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A59E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01I1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL1-56"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01B2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A74F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A59T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A51D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A42C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A07H4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A40C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A16C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01H6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A92D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04F2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A35T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A92C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A06D5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A93A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05C1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A10F5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A33E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL141"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A09D4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A41T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A52T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01E5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03D3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A11A1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A74D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A88T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV04A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A55E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A46A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A104E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A71C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01D2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV09A2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A28E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A75F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A74T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A54E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04I3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A41A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A09I3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A55B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A06B2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV09C1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A19C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A78A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A42B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05C4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A35D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A62B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A84B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV09G4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A56C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A07J2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A09H2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A67D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "JAULAD"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14C3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A33T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL1-49"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A54T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A75B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A91E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A19E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A57C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05F6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01J2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A06G4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV09B3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01E6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A48T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "JAULAT"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A56B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV02A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A07E4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01F4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SHEILA"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03E5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A10D1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A15A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A35B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03E2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV03A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A90A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "B55D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A09E2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A76E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A40E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A56A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "MOSTREF"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV05C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A07F5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A95B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "BATER"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03J5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A99C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01A01"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A11J4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02E5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A103T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A83T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A64A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03F3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01M2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A31E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A06F3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05C5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A54A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A16T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A06D3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01G2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV09C4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A30D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A11D4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01J3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04D6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A55A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05A5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A33D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "JAULAB"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14K5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A17F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A102E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A07C5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A75C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A49A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A104D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV07D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A24T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL120"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04G4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01J6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV07B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03E4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A07D3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03B4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05I5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A83B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A09H4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A87C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A71B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A103B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A10C1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01A3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "L93"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A57A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A79E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A17D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV09K3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03E3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A92A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV06B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A07L1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A11E1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV02T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04C1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04D2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14K4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV06E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A09H3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14I5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A70F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04F1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A76C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03J6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A09B1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A81B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14C2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A52E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A53B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A70B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A90T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A51E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL143"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL133"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A06E5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A34C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03L1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A38T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05L3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A51D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A28F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV06C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02F4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02A1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A50F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A34E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV09G6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A61E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02F3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01K4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL150"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02B1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A84T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A80F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "CJ5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A61D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A29E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A10L3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A26F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL51"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01F5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A76B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A18A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A11E2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A103D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A76D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01H4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14D1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14E6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03J4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A23D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A10D3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A60F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A43A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A89T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A07B2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A25A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A09E5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A07A3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A94D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A61C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A09M1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => " A28A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A92B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A64E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04B3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A07H3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV08F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A60A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02D3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04E1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A09C4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A45B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01K6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A10F4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL118"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01L1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A06M1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03G5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04K4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A93T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03F4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A55D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02C2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A10H6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A11E5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL149"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02E6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A06D2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV09G5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A67B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01H1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL126"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A45T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A65D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14B2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A36B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A43T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A101E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A26D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A40B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV01D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A54F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A105C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV05A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05D1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01D4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A52A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A76A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02B6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV01A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A54C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A58F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01F3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A30E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03F1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A16F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14D5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A48D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A28D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV01F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A39C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01G1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV09J3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A80C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01C5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A78D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A07G4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A25E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A30C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A31A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A06J6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV08D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03D1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A91C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A56E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A68D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A58C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01M1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "HYP3JAULA"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A53E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A72F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02B2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14E4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "HYP2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A82A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A94E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV09L3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01C2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A102T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A39D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A77F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A26B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02K6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A11G4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A11M1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A64D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV07A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A79C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A48A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01L3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A100A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05I12"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A37F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01G6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A10J3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A06A4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A63C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A29T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03F2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02J4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A41C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A98D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A15C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A09C1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A86A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A24F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01E3|"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01I5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A09J3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A95F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A39T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A62A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A84E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A28A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14J3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV09B6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03D4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A72B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A47E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A18F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A43D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A71T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A24A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV03E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A06C3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14I6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A51T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A82B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A66B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A25B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A11F4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01L2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "JAULA D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A66E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A10K3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A40A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A45F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A45C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "CJ3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05C3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14H5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A69A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02E1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A79B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A36A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A09H1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01E2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV04E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03K4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02F5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV09D6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A30B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A104C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A06E1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A46C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A68B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A07K6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A06E2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02H5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A98C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A98F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A57E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A59C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A58A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A06F2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A85C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A29A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A27B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03D2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A77C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A10I4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02H2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02C1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04E2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "E.MOST2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV04B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A102A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A74A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14F3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04A4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14H2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A66T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV03B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV09A4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A07E1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A09D5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01C6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14I1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02F6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A83E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A24C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A11C4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A16A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "MOSTRADOR"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A50A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03H5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05G6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02G5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A53A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A45D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A94F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A62T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A06G3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A98E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A101B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A50B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A31C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A89C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A56D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A47B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05C6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A102F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A72C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04G5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A07C6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A09G4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A42T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A11C6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A22A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A27A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A21C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14B4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A06B3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV01E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A74B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A22F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A103C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A87B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV06A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A67C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05I6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A07G3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A77B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04H2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A41F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A09A3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A95A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A10C4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => " A16A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A105F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A47C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A10H4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A78B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01H2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A94T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A62D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "BUJIA"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A71E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A70E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A11E3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A28B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A49E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14F2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A41B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => " A17B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03C1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "PISO A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05D4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A10H3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A19F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A21D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A71F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A60D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A06A2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03G2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A68F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A44T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A104F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A96F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14G3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A09B6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A37E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A103E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL1-52"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL151"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "E.MOST3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A24B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A37D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A35C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL115"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL104"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A37T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A10J2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A64B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A09F2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A80B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02I3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV08A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A28T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A34F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A34B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A66A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05B2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL53"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A09I2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A46T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05F3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV08C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14E3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14G1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05H3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A17B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A16E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02D2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A46E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A51A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A11D6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14G4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A79A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14D3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A104A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A93D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV09A5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A09G1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A99B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A72A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL145"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A91T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02C5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL110"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A06J5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A11G2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A06E6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "JAULAC"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A56T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A105D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV02B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => " A60A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03C4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A34A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14E2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A11E4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02D5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04B1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL139"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL138"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A09J1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV04C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A59F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A44C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV02D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A09E4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A58D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02C4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A27E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A63T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A97A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A53F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A94A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A10E2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A89D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A10G4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A90B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL55"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01J4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A46F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV09C2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A06C2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A97E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A104B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A55C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A33D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A73E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A51C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A71A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV09C3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A06E4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05D2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02E3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A62F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01A1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A100B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A30T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A48E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03K2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A47A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV09A1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A11K2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A70D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A49B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01K3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A62E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A29C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03I3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A10C2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV04D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV0"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01B4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A70A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01D1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL129"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14J5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A54D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03K1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A99E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A69C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A011C5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A06H6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A38D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A21B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "DEVOLUCION"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A51F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A86C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03L3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A80D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A96D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL107"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01H5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03A4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV07C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A69B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV03F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A81A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A103F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A93B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03K5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A59A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL112"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04E3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL136"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A27T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A54B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A34T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A11L3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A35E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A09E6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A85F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A32E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03H1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02F1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A11F1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14M1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A34D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A46B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A63A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A10G1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A43F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04A3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A97F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01K2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A20E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03G3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A72T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV07E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A97C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04K1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "BATERIA"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A07D2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A78F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03C6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A42F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04F3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A91D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14G2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A73D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A80E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05H4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A09J2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05G4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A99D|"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A32C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "PLUMAS"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A37C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A18D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A09L3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14K1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A66C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A38A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "JAULAA"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A06G5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A63D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => " HYP3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL148"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A35A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A81C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SEV09I2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14D6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04E5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A99D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A22B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A20C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A09F5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A18B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A43B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04H3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV09G2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A07G2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV09D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A49F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A87E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL106"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A69D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02C3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03D5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A19A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01E3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A76T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A75D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A29F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV09D4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL1 52"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "ACEITES"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A48B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A62C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A27F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14E5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A68A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A99A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A17A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A11G3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01B3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01K5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02I5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A80T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A002D4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14B3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A37B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A51B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A81E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "HYPP2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A10E6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A10L1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A20B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05G3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL119"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14H6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14A3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03E6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A20D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A32A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A21A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV04F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A101F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A48C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03J3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03A2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05K5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05E3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14D2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14K3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL114"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05B6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A47D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "JAZ"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A78T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01E4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A23A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A78E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A11H3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A41E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A11C2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "BUJIAS"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A92T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A38T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02E2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A76C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A50C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A60E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A09G6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A99T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A10D2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A55F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14F1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04I1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A85T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02I2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01K1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A07J3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05A4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A11A2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A25T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05L2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A22C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A19B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A94B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A65B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A33C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A104T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03F6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A64C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02J6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A23C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A28C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A79T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A11E6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A84C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A07K3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A50E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A97T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A40T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A52B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A97B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "PISOA"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL101"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14L1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02E4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03C5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A22E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A65A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A31T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04H4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03J2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A61B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A23B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV09E3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03H2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14C4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => " A41A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A85A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A81F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A103A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A94C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A36C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A29B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A20A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A83D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A73F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A96E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02A4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A59D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A99F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A10F6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04B2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A69T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14J6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A33B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "CJ2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A07J4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A38C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL1-54"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01J5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A50D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A102B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A77E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04D5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A32B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A61F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV09H5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A07F4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05A3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A31D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A102C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03G1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04F4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A52C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A87B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => " A24B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A16D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "HERIB"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A07C3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A44A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A38B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "CJ13"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A82E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A45A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01I2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A96A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL146"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A24E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A33F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A10G6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A89B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04A5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A11L1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A63E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A83A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A15F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03G6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A101D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04D4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL103"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A98B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "B25T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A09G5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A53C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL124"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A86D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A96T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "JAULA B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A11I1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A90C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03B5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A26C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A93E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A44E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL102"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "E.MOST"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A36T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A73A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A19D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A97D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A100C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A11K5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04F5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A11F5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A31B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A67A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05I3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "ASESOR"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A53T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV08B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A73B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A36D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A15D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14B6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A57D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A68C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04D3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A67T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A85B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A07E2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "ACEIT"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01G3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV09H4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV01B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03I2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A06G2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL1-53"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05I1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A35F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05E2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01C1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "CJ6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A42E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A52D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A42D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A44B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A100F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A101C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A68E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01G5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "PLUMA"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A55T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A46D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01A5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A18E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A17A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05M2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04E4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV09I3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14G6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A11C3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A11H4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A32D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A77T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A06D6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01C4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05F4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A32F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "CJ11"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A06D4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05B3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05G2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A30A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A09I1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A10E1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01C3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02G4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A10M2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A25C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05F5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04K3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV09A3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04I5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03H3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A09L1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A30F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02G3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A25F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02H6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A77D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A63B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05E4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A49C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A24D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL105"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL117"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A42A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A78C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A23F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A43E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A15B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A57T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03G4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A47F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A15E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A11M2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A95T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV06D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A101T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04C5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14D4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A32T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02D1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03B2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A50T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A88C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14I4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04L2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A10F2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A72D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A80A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02H3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A101A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A17E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A06F6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV09F1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04C4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A11D5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A17T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A48F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04I6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A23T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14E1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL127"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A10G2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A18C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A41D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A29D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14H4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "MOS G"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A37A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV07F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => " A42A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => " A59B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01F6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A06J1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV09C6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A83C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A59B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A102D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL130"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL109"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02G2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A39B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A73A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A77A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A40D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV02C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL56"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02F2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A88D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A60C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04E6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01D3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01E1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV05B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14L2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV09H3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A90D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03E1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A25D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A60B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL1 50"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL1-48"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A84A"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A82C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04G2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A14J4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A81T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A11J6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05B4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A89E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL121"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A100T|"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A06H2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A09A2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV08E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A11D2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A95D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01G4"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A71D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A03K3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01B1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A100E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A16B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A105T"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A10K2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV09B5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01F1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04G6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A56F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV02E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04G3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A70C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "COL132"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => " A57B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A74C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05D3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A06J2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A84D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A72E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A06F1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "SERV02F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A53D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04K2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A01D5"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A17C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A07B3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A43C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A10G3"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A04F6"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A79D"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A02G1"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A88B"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A91F"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A27C"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A05J2"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A26E"
			],
			[
				CatalogoUbicacionProductoModel::NOMBRE => "A93C"
			]
		];

		foreach ($data as $key => $items) {
			DB::table(CatalogoUbicacionProductoModel::getTableName())->insert($items);
		}
	}
}
