<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ModulosMenuSeeder::class);
        $this->call(MenuSeccionesSeeder::class);
        $this->call(MenuSubmenusSeeder::class);
        $this->call(MenuVistasSeeder::class);
        $this->call(ModuloEreactSeeder::class);
        
        $this->call(CatalogoUbicacionesProductoSeeder::class);
        $this->call(RolesSeeder::class);
        $this->call(ReRolesModulosSeed::class);
       
        $this->call(CatalogoMesesSeed::class);

        $this->call(PreciosSeeder::class);
        $this->call(table_cat_ubicaciones::class);
        $this->call(CfdiSeeder::class);
        $this->call(TipoPagoSeed::class);
        $this->call(TipoClienteSeed::class);
        $this->call(UsuariosSeeder::class);
        $this->call(MovimientosProductosSeeder::class);
        $this->call(CatTipoPolizaSeeder::class);
        $this->call(CatalogoVestidurasSeeder::class);
        $this->call(ClientesSeeder::class);
        $this->call(CatalogoClaveClienteSeeder::class);
        $this->call(GlobalSeeders::class);
        $this->call(CatalogoCuentasSeed::class);
        $this->call(TipoPrecioSeeder::class);
        $this->call(EstatusCompraSeed::class);
        $this->call(EstatusVentaSeed::class);
        $this->call(EstatusFacturaSeed::class);
        $this->call(EstatusTraspasoSeeder::class);
        $this->call(TipoFormaPagoSeed::class);
        $this->call(TipoDocumentoSeeder::class);
        $this->call(PlazoCreditoSeeder::class);
        $this->call(CatalogoColoresSeed::class);
        $this->call(CatalogoAniosSeed::class);
        $this->call(CatalogoMarcasSeed::class);
        $this->call(CatalogoModelosSeed::class);
        $this->call(EstatusCuentaSeed::class);
        $this->call(MovimientosCajaSeed::class);
        $this->call(EstatusVentaAutosSeed::class);
        $this->call(catCombustibleSeeder::class);
        $this->call(CatStausUNSeeder::class);
        $this->call(CatStatusRecepcionUNSeeder::class);
        $this->call(UbicacionLlavesSeeder::class);
        $this->call(tableAduanasSeeder::class);
        $this->call(CatalogoUnidadesSeeder::class);
        // $this->call(UnidadesCostosSeeder::class); 
        $this->call(TipoAbonosSeeder::class);
        $this->call(EstatusAbonosSeeder::class);

        //Relaciones menu usarios
        $this->call(MenuModulosRolesSeeder::class);
        $this->call(MenuModulosUsuariosSeeder::class);
        // $this->call(MenuVistasUsuariosSeeder::class); 
        $this->call(OrigenSeeder::class);
        $this->call(EstatusFinanciamientosSeeder::class);
        $this->call(catFinancierasSeeder::class);
        $this->call(CatPropuestasSeeder::class);
        $this->call(CatPerfilesFinanciamientoSeeder::class);
        $this->call(ComisionFinanciamientosSeeder::class);
        $this->call(tipoSeguroSeeder::class);
        $this->call(CompaniasSeguroSSeeder::class);
        $this->call(UDISeeder::class);
        $this->call(EstatusPlanPisoSeeder::class);
        $this->call(CatUsoSeguroSeeder::class);
        $this->call(EstatusAdmvosSeeder::class);
        $this->call(CatEstatusDaniosBSSeeder::class);
        $this->call(CatAgenciaSeeder::class);
        $this->call(CatAreaReparacionSeeder::class);
        $this->call(CatTipoSeveridadSeeder::class);
        $this->call(CatEstatusSasSegurosSeeder::class);
        $this->call(CatModosSeeder::class);
        $this->call(CatCarrilesSeeder::class);
        $this->call(EstatusInventarioSeeder::class);
        $this->call(CatCajasSeeder::class);
        // $this->call(ProductoSeeder::class);
        $this->call(CatTipoPedidoSeeder::class);
        $this->call(ProveedoresSeeder::class);
        $this->call(TipoDocumentosVentaSeed::class);
        $this->call(CaBodyshopProyectosSeeder::class);
        $this->call(CatTipoAsientoSeeder::class);
        $this->call(CatalogoProcesosSeed::class);
        $this->call(EstatusAsientosSeeder::class);
        $this->call(table_precios_activos::class);
        $this->call(Table_lista_precios_un::class);
        $this->call(CatEstatusCPSeeder::class);
        // $this->call(ProactivoSeeder::class);
        $this->call(cpVentasSeed::class);
        $this->call(CatPrioridadesSeeder::class);
        $this->call(CatStatusTicketsSeeder::class);
        $this->call(telefonos_usuarios::class);
        $this->call(TipoPolizaSeeder::class);
        $this->call(roles_soporte::class);
        $this->call(EstatusAnticiposSeed::class);
        $this->call(CatalogoBancosSeeder::class);
        $this->call(CatStatusRemisionSeeder::class);
        $this->call(InventarioMovimientosSeeder::class);
        $this->call(AddStatusUnidadesNuevas::class);
        $this->call(AddStatusCatStatusRemision::class);
        
    }
}
