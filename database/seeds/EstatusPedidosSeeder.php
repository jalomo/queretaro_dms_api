<?php

use Illuminate\Database\Seeder;
use App\Models\Refacciones\EstatusPedidos;

class EstatusPedidosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data[] = [ 'id' => 1, 'nombre' => 'SIN AUTORIZAR'];
        $data[] = [ 'id' => 2, 'nombre' => 'ACTUALIZADO'];
        $data[] = [ 'id' => 3, 'nombre' => 'PEDIDO'];
        $data[] = [ 'id' => 4, 'nombre' => 'AUTORIZADO'];
        $data[] = [ 'id' => 5, 'nombre' => 'PEDIDO CARGADO'];

        foreach ($data as $item) 
        {
            EstatusPedidos::create($item);
        }
    }
}
