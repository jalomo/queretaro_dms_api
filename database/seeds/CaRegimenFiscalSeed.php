<?php

use App\Models\Refacciones\CaRegimenFiscalModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CaRegimenFiscalSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $regimenesFiscales = array(
            array(CaRegimenFiscalModel::CLAVE => '601', CaRegimenFiscalModel::NOMBRE => 'General de Ley Personas Morales', CaRegimenFiscalModel::PERSONA_FISICA => false, CaRegimenFiscalModel::PERSONA_MORAL => true),
            array(CaRegimenFiscalModel::CLAVE => '603', CaRegimenFiscalModel::NOMBRE => 'Personas Morales con Fines no Lucrativos', CaRegimenFiscalModel::PERSONA_FISICA => false, CaRegimenFiscalModel::PERSONA_MORAL => true),
            array(CaRegimenFiscalModel::CLAVE => '605', CaRegimenFiscalModel::NOMBRE => 'Sueldos y Salarios e Ingresos Asimilados a Salarios', CaRegimenFiscalModel::PERSONA_FISICA => true, CaRegimenFiscalModel::PERSONA_MORAL => false),
            array(CaRegimenFiscalModel::CLAVE => '606', CaRegimenFiscalModel::NOMBRE => 'Arrendamiento', CaRegimenFiscalModel::PERSONA_FISICA => true, CaRegimenFiscalModel::PERSONA_MORAL => false),
            array(CaRegimenFiscalModel::CLAVE => '607', CaRegimenFiscalModel::NOMBRE => 'Régimen de Enajenación o Adquisición de Bienes', CaRegimenFiscalModel::PERSONA_FISICA => true, CaRegimenFiscalModel::PERSONA_MORAL => false),
            array(CaRegimenFiscalModel::CLAVE => '608', CaRegimenFiscalModel::NOMBRE => 'Demás ingresos', CaRegimenFiscalModel::PERSONA_FISICA => true, CaRegimenFiscalModel::PERSONA_MORAL => false),
            array(CaRegimenFiscalModel::CLAVE => '610', CaRegimenFiscalModel::NOMBRE => 'Residentes en el Extranjero sin Establecimiento Permanente en México', CaRegimenFiscalModel::PERSONA_FISICA => true, CaRegimenFiscalModel::PERSONA_MORAL => true),
            array(CaRegimenFiscalModel::CLAVE => '611', CaRegimenFiscalModel::NOMBRE => 'Ingresos por Dividendos (socios y accionistas)', CaRegimenFiscalModel::PERSONA_FISICA => true, CaRegimenFiscalModel::PERSONA_MORAL => false),
            array(CaRegimenFiscalModel::CLAVE => '612', CaRegimenFiscalModel::NOMBRE => 'Personas Físicas con Actividades Empresariales y Profesionales', CaRegimenFiscalModel::PERSONA_FISICA => true, CaRegimenFiscalModel::PERSONA_MORAL => false),
            array(CaRegimenFiscalModel::CLAVE => '614', CaRegimenFiscalModel::NOMBRE => 'Ingresos por intereses', CaRegimenFiscalModel::PERSONA_FISICA => true, CaRegimenFiscalModel::PERSONA_MORAL => false),
            array(CaRegimenFiscalModel::CLAVE => '615', CaRegimenFiscalModel::NOMBRE => 'Régimen de los ingresos por obtención de premios', CaRegimenFiscalModel::PERSONA_FISICA => true, CaRegimenFiscalModel::PERSONA_MORAL => false),
            array(CaRegimenFiscalModel::CLAVE => '616', CaRegimenFiscalModel::NOMBRE => 'Sin obligaciones fiscales', CaRegimenFiscalModel::PERSONA_FISICA => true, CaRegimenFiscalModel::PERSONA_MORAL => false),
            array(CaRegimenFiscalModel::CLAVE => '620', CaRegimenFiscalModel::NOMBRE => 'Sociedades Cooperativas de Producción que optan por diferir sus ingresos', CaRegimenFiscalModel::PERSONA_FISICA => false, CaRegimenFiscalModel::PERSONA_MORAL => true),
            array(CaRegimenFiscalModel::CLAVE => '621', CaRegimenFiscalModel::NOMBRE => 'Incorporación Fiscal', CaRegimenFiscalModel::PERSONA_FISICA => true, CaRegimenFiscalModel::PERSONA_MORAL => false),
            array(CaRegimenFiscalModel::CLAVE => '622', CaRegimenFiscalModel::NOMBRE => 'Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras', CaRegimenFiscalModel::PERSONA_FISICA => false, CaRegimenFiscalModel::PERSONA_MORAL => true),
            array(CaRegimenFiscalModel::CLAVE => '623', CaRegimenFiscalModel::NOMBRE => 'Opcional para Grupos de Sociedades', CaRegimenFiscalModel::PERSONA_FISICA => false, CaRegimenFiscalModel::PERSONA_MORAL => true),
            array(CaRegimenFiscalModel::CLAVE => '624', CaRegimenFiscalModel::NOMBRE => 'Coordinados', CaRegimenFiscalModel::PERSONA_FISICA => false, CaRegimenFiscalModel::PERSONA_MORAL => true),
            array(CaRegimenFiscalModel::CLAVE => '625', CaRegimenFiscalModel::NOMBRE => 'Régimen de las Actividades Empresariales con ingresos a través de Plataformas Tecnológicas', CaRegimenFiscalModel::PERSONA_FISICA => true, CaRegimenFiscalModel::PERSONA_MORAL => false),
            array(CaRegimenFiscalModel::CLAVE => '626', CaRegimenFiscalModel::NOMBRE => 'Régimen Simplificado de Confianza', CaRegimenFiscalModel::PERSONA_FISICA => true, CaRegimenFiscalModel::PERSONA_MORAL => true)
        );

        foreach ($regimenesFiscales as $key => $value) {
            DB::table(CaRegimenFiscalModel::getTableName())->insert([
                CaRegimenFiscalModel::NOMBRE => $value[CaRegimenFiscalModel::NOMBRE],
                CaRegimenFiscalModel::CLAVE => $value[CaRegimenFiscalModel::CLAVE],
                CaRegimenFiscalModel::PERSONA_FISICA => $value[CaRegimenFiscalModel::PERSONA_FISICA],
                CaRegimenFiscalModel::PERSONA_MORAL => $value[CaRegimenFiscalModel::PERSONA_MORAL]
            ]);
        }
    }
}
