<?php

use App\Models\Caja\CatalogoBancosModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatalogoBancosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $catalogo_bancos = [
            [
                CatalogoBancosModel::NOMBRE => 'BANAMEX'
            ],
            [
                CatalogoBancosModel::NOMBRE => 'BANCOMEXT'
            ],
            [
                CatalogoBancosModel::NOMBRE => 'BANOBRAS'
            ],
            [
                CatalogoBancosModel::NOMBRE => 'BBVA BANCOMER'
            ],
            [
                CatalogoBancosModel::NOMBRE => 'SANTANDER'
            ],
            [
                CatalogoBancosModel::NOMBRE => 'BANJERCITO'
            ],
            [
                CatalogoBancosModel::NOMBRE => 'HSBC'
            ],
            [
                CatalogoBancosModel::NOMBRE => 'BAJIO'
            ],
            [
                CatalogoBancosModel::NOMBRE => 'IXE'
            ],
            [
                CatalogoBancosModel::NOMBRE => 'INBURSA'
            ],
            [
                CatalogoBancosModel::NOMBRE => 'INTERACCIONES'
            ],
            [
                CatalogoBancosModel::NOMBRE => 'MIFEL'
            ],
            [
                CatalogoBancosModel::NOMBRE => 'SCOTIABANK'
            ],
            [
                CatalogoBancosModel::NOMBRE => 'BANREGIO'
            ],
            [
                CatalogoBancosModel::NOMBRE => 'INVEX'
            ],
            [
                CatalogoBancosModel::NOMBRE => 'BANSI'
            ],
            [
                CatalogoBancosModel::NOMBRE => 'AFIRME'
            ],
            [
                CatalogoBancosModel::NOMBRE => 'BANORTE/IXE'
            ],
            [
                CatalogoBancosModel::NOMBRE => 'THE ROYAL BANK'
            ],
            [
                CatalogoBancosModel::NOMBRE => 'AMERICAN EXPRESS'
            ],
            [
                CatalogoBancosModel::NOMBRE => 'BAMSA'
            ],
            [
                CatalogoBancosModel::NOMBRE => 'TOKYO'
            ],
            [
                CatalogoBancosModel::NOMBRE => 'JP MORGAN'
            ],
            [
                CatalogoBancosModel::NOMBRE => 'BMONEX'
            ],
            [
                CatalogoBancosModel::NOMBRE => 'VE POR MAS'
            ],
            [
                CatalogoBancosModel::NOMBRE => 'DEUTSCHE'
            ],
            [
                CatalogoBancosModel::NOMBRE => 'AZTECA'
            ],
            [
                CatalogoBancosModel::NOMBRE => 'WAL-MART'
            ],
            [
                CatalogoBancosModel::NOMBRE => 'BANCOPPEL'
            ],
            [
                CatalogoBancosModel::NOMBRE => 'PROGRESO'
            ],
            [
                CatalogoBancosModel::NOMBRE => 'TELECOMM'
            ],
            [
                CatalogoBancosModel::NOMBRE => 'HDI SEGUROS'
            ],
            [
                CatalogoBancosModel::NOMBRE => 'INDEVAL'
            ],
           
        ];

        foreach ($catalogo_bancos as $value) {
            DB::table(CatalogoBancosModel::getTableName())->insert([
                CatalogoBancosModel::NOMBRE => $value[CatalogoBancosModel::NOMBRE]
            ]);
        }
    }
}
