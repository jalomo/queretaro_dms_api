<?php

use Illuminate\Database\Seeder;
use App\Models\Nomina\CaRegimenContratoModel as Model;

class CaRegimenContratoSeeder extends Seeder
{
    private function seedData(){
        return [
            [Model::ID=>1, Model::Clave => '02', Model::Descripcion => 'Sueldos'],
            [Model::ID=>2, Model::Clave => '03', Model::Descripcion => 'Jubilados'],
            [Model::ID=>3, Model::Clave => '04', Model::Descripcion => 'Pensionados'],
            [Model::ID=>4, Model::Clave => '05', Model::Descripcion => 'Asimilados miembros sociedades cooperativas producción'],
            [Model::ID=>5, Model::Clave => '06', Model::Descripcion => 'Asimilados integrantes sociedades asociaciones civiles'],
            [Model::ID=>6, Model::Clave => '07', Model::Descripcion => 'Asimilados miembros consejos'],
            [Model::ID=>7, Model::Clave => '08', Model::Descripcion => 'Asimilados comisionistas'],
            [Model::ID=>8, Model::Clave => '09', Model::Descripcion => 'Asimilados honorarios'],
            [Model::ID=>9, Model::Clave => '10', Model::Descripcion => 'Asimilados acciones'],
            [Model::ID=>10, Model::Clave => '11', Model::Descripcion => 'Asimilados otros'],
            [Model::ID=>11, Model::Clave => '99', Model::Descripcion => 'Otro régimen']
        ];
    }

    public function run()
    {
        foreach ($this->seedData() as $key => $items) {
            $exists = DB::connection(Model::connectionName())->table(Model::getTableName())->whereNotNull(MODEL::ID)->where(MODEL::ID, $items[MODEL::ID])->first();
            if($exists == false){
                DB::connection(Model::connectionName())->table(Model::getTableName())->insert($items);
            }
        }
    }
}
