<?php

use App\Models\Autos\UnidadesNuevas\CatStatusRecepcionUNModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatStatusRecepcionUNSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                CatStatusRecepcionUNModel::ID => 1,
                CatStatusRecepcionUNModel::ESTATUS => 'Unidades en tránsito'
            ],
            [
                CatStatusRecepcionUNModel::ID => 2,
                CatStatusRecepcionUNModel::ESTATUS => 'DEMO'
            ],
            [
                CatStatusRecepcionUNModel::ID => 3,
                CatStatusRecepcionUNModel::ESTATUS => 'Intercambio'
            ],
            [
                CatStatusRecepcionUNModel::ID => 4,
                CatStatusRecepcionUNModel::ESTATUS => 'Planta'
            ],
            [
                CatStatusRecepcionUNModel::ID => 5,
                CatStatusRecepcionUNModel::ESTATUS => 'Recepción'
            ],
            [
                CatStatusRecepcionUNModel::ID => 6,
                CatStatusRecepcionUNModel::ESTATUS => 'Previa'
            ],
            [
                CatStatusRecepcionUNModel::ID => 7,
                CatStatusRecepcionUNModel::ESTATUS => 'Almacén'
            ],
            [
                CatStatusRecepcionUNModel::ID => 8,
                CatStatusRecepcionUNModel::ESTATUS => 'Mantenimiento'
            ],
            [
                CatStatusRecepcionUNModel::ID => 9,
                CatStatusRecepcionUNModel::ESTATUS => 'Programado'
            ],
            [
                CatStatusRecepcionUNModel::ID => 10,
                CatStatusRecepcionUNModel::ESTATUS => 'PDI'
            ],
            [
                CatStatusRecepcionUNModel::ID => 11,
                CatStatusRecepcionUNModel::ESTATUS => 'Accesorio'
            ],
            [
                CatStatusRecepcionUNModel::ID => 12,
                CatStatusRecepcionUNModel::ESTATUS => 'Entrega'
            ],
            [
                CatStatusRecepcionUNModel::ID => 13,
                CatStatusRecepcionUNModel::ESTATUS => 'Revisión'
            ],
            [
                CatStatusRecepcionUNModel::ID => 14,
                CatStatusRecepcionUNModel::ESTATUS => 'Reparación mecánica'
            ],
            [
                CatStatusRecepcionUNModel::ID => 15,
                CatStatusRecepcionUNModel::ESTATUS => 'Faltante'
            ],
            [
                CatStatusRecepcionUNModel::ID => 16,
                CatStatusRecepcionUNModel::ESTATUS => 'Reparación H Y P'
            ],
            [
                CatStatusRecepcionUNModel::ID => 17,
                CatStatusRecepcionUNModel::ESTATUS => 'Exhibición'
            ],
            [
                CatStatusRecepcionUNModel::ID => 18,
                CatStatusRecepcionUNModel::ESTATUS => 'Prueba'
            ],
            [
                CatStatusRecepcionUNModel::ID => 19,
                CatStatusRecepcionUNModel::ESTATUS => 'BODY'
            ],
            [
                CatStatusRecepcionUNModel::ID => 20,
                CatStatusRecepcionUNModel::ESTATUS => 'TRÁNSITO INTERCAMBIO'
            ],
            [
                CatStatusRecepcionUNModel::ID => 21,
                CatStatusRecepcionUNModel::ESTATUS => ' TRANSITO PLANTA'
            ],
           
        ];

        foreach ($data as $value) {
            DB::table(CatStatusRecepcionUNModel::getTableName())->insert($value);
        }
    }
}
