<?php

use App\Models\Autos\ContratoVentaAutoModel;
use App\Models\Autos\VentasAutosModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaContratoVentaAuto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ContratoVentaAutoModel::getTableName(), function (Blueprint $table) {
            $table->increments(ContratoVentaAutoModel::ID);

            $table->text(ContratoVentaAutoModel::FIRMA_CLIENTE)->nullable();
            $table->text(ContratoVentaAutoModel::FIRMA_GERENTE_CREDITO)->nullable();
            $table->text(ContratoVentaAutoModel::FIRMA_GERENTE_VENTAS)->nullable();
            
            $table->unsignedInteger(ContratoVentaAutoModel::ID_VENTA_AUTO);
            $table->foreign(ContratoVentaAutoModel::ID_VENTA_AUTO)
                ->references(VentasAutosModel::ID)
                ->on(VentasAutosModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(ContratoVentaAutoModel::getTableName());
        //
    }
}
