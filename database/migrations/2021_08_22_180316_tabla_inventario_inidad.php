<?php

// use App\Autos\Inventario\InventarioModel;

use App\Models\Autos\Inventario\InventarioModel;
use App\Models\Autos\UnidadesModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaInventarioInidad extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(InventarioModel::getTableName(), function (Blueprint $table) {
            $table->increments(InventarioModel::ID);
            $table->string(InventarioModel::DIAS_HABILES)->nullable();
            $table->text(InventarioModel::FIRMA_ELABORA)->nullable();
            $table->date(InventarioModel::FECHA_ELABORACION)->nullable();
            $table->date(InventarioModel::FECHA_ACEPTACION)->nullable();
            $table->text(InventarioModel::FIRMA_CONSUMIDOR)->nullable();
            
            $table->unsignedInteger(InventarioModel::ID_UNIDAD);
            $table->foreign(InventarioModel::ID_UNIDAD)
                ->references(UnidadesModel::ID)
                ->on(UnidadesModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(InventarioModel::getTableName());
    }
}
