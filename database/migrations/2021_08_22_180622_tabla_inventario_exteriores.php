<?php

use App\Models\Autos\Inventario\ExterioresModel;
use App\Models\Autos\Inventario\InventarioModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaInventarioExteriores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create(ExterioresModel::getTableName(), function (Blueprint $table) {
            $table->increments(ExterioresModel::ID);
            $table->string(ExterioresModel::TAPONES_RUEDA)->nullable();
            $table->string(ExterioresModel::GOMA_LIMPIADORES)->nullable();
            $table->string(ExterioresModel::ANTENA)->nullable();
            $table->string(ExterioresModel::TAPON_GASOLINA)->nullable();
            $table->unsignedInteger(ExterioresModel::ID_INVENTARIO);
            $table->foreign(ExterioresModel::ID_INVENTARIO)
                ->references(InventarioModel::ID)
                ->on(InventarioModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop(ExterioresModel::getTableName());
    }
}
