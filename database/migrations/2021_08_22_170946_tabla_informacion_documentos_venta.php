<?php

use App\Models\Autos\InformacionDocumentosVentaModel;
use App\Models\Autos\UnidadesModel;
use App\Models\Autos\VentasAutosModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaInformacionDocumentosVenta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(InformacionDocumentosVentaModel::getTableName(), function (Blueprint $table) {
            $table->increments(InformacionDocumentosVentaModel::ID);
            $table->date(InformacionDocumentosVentaModel::FECHA_FACTURA)->nullable();
            $table->date(InformacionDocumentosVentaModel::FECHA_ULTIMO_INGRESO)->nullable();
            $table->date(InformacionDocumentosVentaModel::FECHA_CONTRATO)->nullable();
            $table->date(InformacionDocumentosVentaModel::FECHA_CARTA_CONAUTO)->nullable();
            $table->date(InformacionDocumentosVentaModel::FECHA_VENTA_SMART)->nullable();
            $table->date(InformacionDocumentosVentaModel::FECHA_ORDEN_COMPRA)->nullable();
            $table->date(InformacionDocumentosVentaModel::FECHA_INGRESO_SMART)->nullable();

            $table->text(InformacionDocumentosVentaModel::FIRMA_GERENTE)->nullable();
            $table->text(InformacionDocumentosVentaModel::FIRMA_OTROS)->nullable();
            
            $table->unsignedInteger(InformacionDocumentosVentaModel::ID_VENTA_UNIDAD);
            $table->foreign(InformacionDocumentosVentaModel::ID_VENTA_UNIDAD)
                ->references(VentasAutosModel::ID)
                ->on(VentasAutosModel::getTableName());

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(InformacionDocumentosVentaModel::getTableName());
    }
}
