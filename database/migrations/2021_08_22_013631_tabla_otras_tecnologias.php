<?php

use App\Models\Autos\SalidaUnidades\OtrasTecnologiasModel;
use App\Models\Autos\SalidaUnidades\SalidaUnidadesModel;
use App\Models\Autos\VentasAutosModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaOtrasTecnologias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(OtrasTecnologiasModel::getTableName(), function (Blueprint $table) {
            $table->increments(OtrasTecnologiasModel::ID);
            $table->string(OtrasTecnologiasModel::EASY_FUEL)->nullable();
            $table->string(OtrasTecnologiasModel::TRACK_APPS)->nullable();
            $table->string(OtrasTecnologiasModel::AUTO_START_STOP)->nullable();
            $table->string(OtrasTecnologiasModel::SISTEMA_CTR_TERRENO)->nullable();
            $table->unsignedInteger(OtrasTecnologiasModel::ID_VENTA_AUTO)->nullable();
            $table->foreign(OtrasTecnologiasModel::ID_VENTA_AUTO)->references(VentasAutosModel::ID)->on(VentasAutosModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(OtrasTecnologiasModel::getTableName());
    }
}
