<?php

use App\Models\Autos\UnidadesNuevas\ListPreciosUNModel;
use App\Models\Autos\UnidadesNuevas\PreciosUnidadesNuevasModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableListaPreciosUnidadesNuevas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ListPreciosUNModel::getTableName(), function (Blueprint $table) {
            $table->increments(ListPreciosUNModel::ID);
            $table->string(ListPreciosUNModel::CAT);
            $table->string(ListPreciosUNModel::CLAVE_VEHICULAR);
            $table->string(ListPreciosUNModel::DESCRIPCION);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(ListPreciosUNModel::getTableName());
    }
}
