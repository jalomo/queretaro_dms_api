<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TraspasoDirecto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('traspasos', function (Blueprint $table) {
            $table->boolean('traspaso_directo')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('traspasos', function (Blueprint $table) {
            $table->dropColumn(['traspaso_directo']);
        });
    }
}
