<?php

use App\Models\Logistica\CatTipoSeveridadModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableCatTipoSeveridad extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatTipoSeveridadModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatTipoSeveridadModel::ID);
            $table->string(CatTipoSeveridadModel::TIPO_SEVERIDAD);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatTipoSeveridadModel::getTableName());
    }
}
