<?php

use App\Models\Bodyshop\CatProyectoModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaBodyshopCatProyectos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create(CatProyectoModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatProyectoModel::ID);
            $table->string(CatProyectoModel::PROYECTO);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop(CatProyectoModel::getTableName());
    }
}
