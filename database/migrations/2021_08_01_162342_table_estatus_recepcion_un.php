<?php

use App\Models\Autos\UnidadesNuevas\CatStatusRecepcionUNModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableEstatusRecepcionUn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatStatusRecepcionUNModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatStatusRecepcionUNModel::ID);
            $table->string(CatStatusRecepcionUNModel::ESTATUS);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatStatusRecepcionUNModel::getTableName());
    }
}
