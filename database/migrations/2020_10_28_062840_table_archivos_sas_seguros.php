<?php

use App\Models\Logistica\FilesSasSegurosModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableArchivosSasSeguros extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(FilesSasSegurosModel::getTableName(), function (Blueprint $table) {
            $table->increments(FilesSasSegurosModel::ID);
            $table->string(FilesSasSegurosModel::ARCHIVO);
            $table->string(FilesSasSegurosModel::PATH);
            $table->string(FilesSasSegurosModel::SEGURO_ID);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(FilesSasSegurosModel::getTableName());
    }
}
