<?php

use App\Models\Autos\EstatusSalidaUnidadModel;
use App\Models\Autos\UnidadesNuevas\CatEstatusUNModel;
use App\Models\Autos\UnidadesNuevas\CatLineas;
use App\Models\Autos\UnidadesNuevas\CatProveedoresUN;
use App\Models\Autos\UnidadesNuevas\CatStatusRecepcionUNModel;
use App\Models\Autos\UnidadesNuevas\RemisionModel;
use App\Models\Refacciones\CatalogoUbicacionModel;
use App\Models\Refacciones\UbicacionLLavesModel;
use App\Models\Usuarios\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableRemisiones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(RemisionModel::getTableName(), function (Blueprint $table) {
            $table->increments(RemisionModel::ID);
            $table->unsignedInteger(RemisionModel::LINEA_ID);
            $table->foreign(RemisionModel::LINEA_ID)
                ->references(CatLineas::ID)
                ->on(CatLineas::getTableName());
            $table->string(RemisionModel::UNIDAD_DESCRIPCION);
            $table->string(RemisionModel::CAT);
            $table->string(RemisionModel::CLAVE_VEHICULAR);
            $table->string(RemisionModel::ECONOMICO);

            $table->boolean(RemisionModel::UNIDAD_IMPORTADA);
            $table->string(RemisionModel::TIPO_AUTO);
            $table->string(RemisionModel::CLAVE_ISAN);
            $table->string(RemisionModel::CTA_MENUDO);
            $table->string(RemisionModel::CTA_FLOTILLA);
            $table->string(RemisionModel::CTA_CONAUTO);
            $table->string(RemisionModel::CTA_INTERCAMBIO);
            $table->string(RemisionModel::CTA_PLLENO);
            $table->string(RemisionModel::CTA_INVENTARIO);
            $table->string(RemisionModel::VTA_MENUDEO);
            $table->string(RemisionModel::VTA_FLOTILLA);
            $table->string(RemisionModel::VTA_CONAUTO);
            $table->string(RemisionModel::VTA_INTERCAMBIO);
            $table->string(RemisionModel::VTA_PLLENO);
            $table->unsignedInteger(RemisionModel::USERID);
            $table->foreign(RemisionModel::USERID)
                ->references(User::ID)
                ->on(User::getTableName());
            $table->double(RemisionModel::C_SUBTOTAL);
            $table->double(RemisionModel::C_TOTAL);
            $table->double(RemisionModel::C_IVA);
            $table->date(RemisionModel::FECHA_PEDIMENTO)->nullable();
            $table->string(RemisionModel::PEDIMENTO)->nullable();
            $table->date(RemisionModel::FECHA_REMISION);
            $table->string(RemisionModel::SERIE);
            $table->string(RemisionModel::SERIE_CORTA)->nullable();
            $table->string(RemisionModel::MOTOR);
            $table->boolean(RemisionModel::INTERCAMBIO);

            $table->unsignedInteger(RemisionModel::PROVEEDOR_ID);
            $table->foreign(RemisionModel::PROVEEDOR_ID)
                ->references(CatProveedoresUN::ID)
                ->on(CatProveedoresUN::getTableName());
            $table->unsignedInteger(RemisionModel::ESTATUS_ID)->default(EstatusSalidaUnidadModel::ESTATUS_SALIDA_DISPONIBLE);
            $table->foreign(RemisionModel::ESTATUS_ID)
                ->references(EstatusSalidaUnidadModel::ID)
                ->on(EstatusSalidaUnidadModel::getTableName());
            $table->unsignedInteger(RemisionModel::UBICACION_ID)->default(1);
            $table->foreign(RemisionModel::UBICACION_ID)
                ->references(CatalogoUbicacionModel::ID)
                ->on(CatalogoUbicacionModel::getTableName());

            $table->date(RemisionModel::FECHA_RECEPCION)->nullable();

            $table->unsignedInteger(RemisionModel::UBICACION_LLAVES_ID)->nullable();
            $table->foreign(RemisionModel::UBICACION_LLAVES_ID)
                ->references(UbicacionLLavesModel::ID)
                ->on(UbicacionLLavesModel::getTableName());

            $table->text(RemisionModel::COMENTARIO)->nullable();
            $table->text(RemisionModel::LEYENDA_DCTO)->nullable();
            $table->date(RemisionModel::ULTIMO_SERVICIO)->nullable();
            
            $table->unsignedInteger(RemisionModel::ID_STATUS_UNIDAD)->default(1);
            $table->foreign(RemisionModel::ID_STATUS_UNIDAD)
                ->references(CatStatusRecepcionUNModel::ID)
                ->on(CatStatusRecepcionUNModel::getTableName());

            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(RemisionModel::getTableName());
    }
}
