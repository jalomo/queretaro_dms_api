<?php

use App\Models\Autos\SalidaUnidades\IluminacionModel;
use App\Models\Autos\SalidaUnidades\SalidaUnidadesModel;
use App\Models\Autos\VentasAutosModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaIluminacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(IluminacionModel::getTableName(), function (Blueprint $table) {
            $table->increments(IluminacionModel::ID);
            $table->string(IluminacionModel::ILUMINACION_AMBIENTAL)->nullable();
            $table->string(IluminacionModel::SISTEMA_ILUMINACION)->nullable();
            $table->string(IluminacionModel::FAROS_BIXENON)->nullable();
            $table->string(IluminacionModel::HID)->nullable();
            $table->string(IluminacionModel::LED)->nullable();
            $table->unsignedInteger(IluminacionModel::ID_VENTA_AUTO);
            $table->foreign(IluminacionModel::ID_VENTA_AUTO)->references(VentasAutosModel::ID)->on(VentasAutosModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(IluminacionModel::getTableName());
    }
}
