<?php

use App\Models\Soporte\EvidenciaTicketModel;
use App\Models\Usuarios\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserEvidenciaTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(EvidenciaTicketModel::getTableName(), function (Blueprint $table) {
            $table->unsignedInteger(EvidenciaTicketModel::USER_ID)->nullable();
            $table->foreign(EvidenciaTicketModel::USER_ID)
                ->references(User::ID)
                ->on(User::getTableName());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(EvidenciaTicketModel::getTableName(), function (Blueprint $table) {
            $table->dropColumn(EvidenciaTicketModel::USER_ID);
        });
    }
}
