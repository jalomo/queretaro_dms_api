<?php

use App\Models\Autos\UnidadesNuevas\DetalleRemisionModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCapacidadRemision extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(DetalleRemisionModel::getTableName(), function (Blueprint $table) {
            $table->float(DetalleRemisionModel::CAPACIDAD_KG)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(DetalleRemisionModel::getTableName());
    }
}
