<?php

use App\Models\Autos\CatModelosModel;
use App\Models\Bodyshop\BodyshopModel;
use App\Models\Bodyshop\CatProyectoModel;
use App\Models\Logistica\CatEstatusDaniosBodyshopModel;
use App\Models\Refacciones\CatalogoAnioModel;
use App\Models\Refacciones\CatalogoColoresModel;
use App\Models\Usuarios\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableBodyshop extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(BodyshopModel::getTableName(), function (Blueprint $table) {
            $table->increments(BodyshopModel::ID);
            $table->unsignedInteger(BodyshopModel::ID_PROYECTO);
            $table->foreign(BodyshopModel::ID_PROYECTO)
                ->references(CatProyectoModel::ID)
                ->on(CatProyectoModel::getTableName());
            $table->string(BodyshopModel::DESCRIPCION);
            $table->string(BodyshopModel::NUMERO_SINIESTRO);
            $table->string(BodyshopModel::NUMERO_POLIZA);
            $table->unsignedInteger(BodyshopModel::ID_ANIO);
            $table->foreign(BodyshopModel::ID_ANIO)->references(CatalogoAnioModel::ID)->on(CatalogoAnioModel::getTableName());
            $table->string(BodyshopModel::PLACAS);
            $table->unsignedInteger(BodyshopModel::ID_COLOR);
            $table->foreign(BodyshopModel::ID_COLOR)->references(CatalogoColoresModel::ID)->on(CatalogoColoresModel::getTableName());
            $table->unsignedInteger(BodyshopModel::ID_MODELO);
            $table->foreign(BodyshopModel::ID_MODELO)->references(CatModelosModel::ID)->on(CatModelosModel::getTableName());
            $table->string(BodyshopModel::SERIE);
            $table->boolean(BodyshopModel::CITA);
            $table->unsignedInteger(BodyshopModel::ID_ASESOR);
            $table->foreign(BodyshopModel::ID_ASESOR)->references(User::ID)->on(User::getTableName());
            $table->date(BodyshopModel::FECHA_CITA);
            $table->time(BodyshopModel::HORA_CITA);
            $table->unsignedInteger(BodyshopModel::ID_STATUS);
            $table->foreign(BodyshopModel::ID_STATUS)->references(CatEstatusDaniosBodyshopModel::ID)->on(CatEstatusDaniosBodyshopModel::getTableName());
            $table->date(BodyshopModel::FECHA_INICIO);
            $table->date(BodyshopModel::FECHA_FIN);
            $table->string(BodyshopModel::TIPO_GOLPE);
            $table->text(BodyshopModel::COMENTARIOS);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(BodyshopModel::getTableName());
    }
}
