<?php

use App\Models\Autos\UnidadesNuevas\RemisionModel;
use App\Models\Logistica\CatEstatusDaniosBodyshopModel;
use App\Models\Logistica\SasSeguroModel;
use App\Models\Refacciones\CatalogoColoresModel;
use App\Models\Usuarios\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableSasSegurosBodyshop extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(SasSeguroModel::getTableName(), function (Blueprint $table) {
            $table->increments(SasSeguroModel::ID);
            $table->string(SasSeguroModel::RECLAMO);
            $table->string(SasSeguroModel::TIPO);
            $table->unsignedInteger(SasSeguroModel::ID_UNIDAD)->nullable();
            $table->foreign(SasSeguroModel::ID_UNIDAD)->references(RemisionModel::ID)->on(RemisionModel::getTableName());
            $table->unsignedInteger(SasSeguroModel::ID_COLOR)->nullable();
            $table->foreign(SasSeguroModel::ID_COLOR)->references(CatalogoColoresModel::ID)->on(CatalogoColoresModel::getTableName());
            $table->unsignedInteger(SasSeguroModel::ID_ANIO)->nullable();
            $table->foreign(SasSeguroModel::ID_ANIO)->references(CatalogoColoresModel::ID)->on(CatalogoColoresModel::getTableName());
            $table->string(SasSeguroModel::SERIE);
            $table->text(SasSeguroModel::OBSERVACIONES)->nullable();
            $table->string(SasSeguroModel::DANIO);
            $table->date(SasSeguroModel::FECHA_ALTA);
            $table->date(SasSeguroModel::FECHA_ENTREGA_ASESOR);
            $table->unsignedInteger(SasSeguroModel::ID_ESTATUS)->nullable();
            $table->foreign(SasSeguroModel::ID_ESTATUS)->references(CatEstatusDaniosBodyshopModel::ID)->on(CatEstatusDaniosBodyshopModel::getTableName());
            $table->boolean(SasSeguroModel::FINALIZADO);
            $table->unsignedInteger(SasSeguroModel::ID_ASESOR_VENTA)->nullable();
            $table->foreign(SasSeguroModel::ID_ASESOR_VENTA)->references(User::ID)->on(User::getTableName());
            $table->string(SasSeguroModel::FOTOS);
            $table->string(SasSeguroModel::DOCUMENTOS);
            $table->string(SasSeguroModel::PRESUPUESTOS);
            $table->string(SasSeguroModel::RECUPERADO);
            $table->string(SasSeguroModel::COBRADO);
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(SasSeguroModel::getTableName());
    }
}
