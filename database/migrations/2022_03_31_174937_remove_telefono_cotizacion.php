<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Refacciones\CotizadorRefaccionesModel;

class RemoveTelefonoCotizacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(CotizadorRefaccionesModel::getTableName(), function (Blueprint $table) {
            $table->dropColumn(CotizadorRefaccionesModel::TELEFONO);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(CotizadorRefaccionesModel::getTableName(), function (Blueprint $table) {
            $table->dropColumn(CotizadorRefaccionesModel::TELEFONO);
        });
    }
}
