<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Pedidos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('pedidos', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('autorizado')->nullable();
            $table->timestamps();
        });

        Schema::create('pedidos_productos', function(Blueprint $table){
            $table->bigIncrements('id')->unsigned();
            $table->integer('producto_id')->unsigned();
            $table->bigInteger('pedido_id')->unsigned();
            $table->integer('cantidad')->default(0);

            $table->foreign('producto_id')->references('id')->on('producto');
            $table->foreign('pedido_id')->references('id')->on('pedidos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('pedidos_productos');
        Schema::drop('pedidos');
    }
}
