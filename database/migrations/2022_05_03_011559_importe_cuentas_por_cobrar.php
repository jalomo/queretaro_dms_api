<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ImporteCuentasPorCobrar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('cuentas_por_cobrar', function (Blueprint $table) {
            $table->decimal('importe', 10, 2)->change();
            $table->decimal('total', 10, 2)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('cuentas_por_cobrar', function (Blueprint $table) {
            $table->decimal('importe', 8, 2)->change();
            $table->decimal('total', 8, 2)->change();
        });
    }
}
