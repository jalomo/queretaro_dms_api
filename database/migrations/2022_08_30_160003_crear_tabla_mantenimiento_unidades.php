<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaMantenimientoUnidades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mantenimiento_unidades', function (Blueprint $table) {
            $table->id();

            $table->integer('remision_id')->unsigned()->nullable();
            $table->string('tipo_unidad')->nullable(); 
            $table->integer('tipo_unidad_id')->nullable(); 
            $table->integer('tecnico_mantenimiento_id')->nullable();
            $table->string('tecnico_mantenimiento')->nullable();
            $table->string('no_orden_reparacion')->nullable();
            $table->date('fecha_recibo_unidad')->nullable();
            $table->date('fecha_mantenimiento')->nullable();
            $table->string('estado_bateria')->nullable();
            $table->string('estatus_voltaje_bateria', 12)->nullable();
            $table->string('voltaje_bateria_inicial', 12)->nullable();
            $table->string('voltaje_bateria_final', 12)->nullable();
            $table->string('estado_bateria_electrico')->nullable();
            $table->string('estatus_voltaje_bateria_electrico', 12)->nullable();
            $table->string('voltaje_bateria_inicial_electrico', 12)->nullable();
            $table->string('voltaje_bateria_final_electrico', 12)->nullable();
            $table->string('temperatura_normal', 12)->nullable();
            $table->string('estereo_apagado', 12)->nullable();
            $table->string('aire_apagado', 12)->nullable();
            $table->string('cambios_transmision', 12)->nullable();
            $table->string('rotacion_llantas', 12)->nullable();
            $table->string('estado_elevadores', 12)->nullable();
            $table->string('fugas_fluidos', 12)->nullable();
            $table->string('protectores_asientos', 12)->nullable();
            $table->string('presion_llanta_dd', 12)->nullable();
            $table->string('presion_llanta_di', 12)->nullable();
            $table->string('presion_llanta_ti', 12)->nullable();
            $table->string('presion_llanta_td', 12)->nullable();
            $table->string('caducidad_wrap', 12)->nullable();
            $table->string('lavado_ford', 12)->nullable();
            $table->string('estado_pintura', 12)->nullable();
            $table->string('cera_ford', 12)->nullable();
            $table->string('descontaminacion_pintura', 12)->nullable();
            $table->string('gomas_limpiaparabrisas', 12)->nullable();
            $table->string('nivel_gasolina', 12)->nullable();
            $table->string('parte_baja_vieculo', 12)->nullable();
            $table->string('nivel_anticongelante', 12)->nullable();
            $table->string('nivel_aceite_motor', 12)->nullable();
            $table->string('nivel_liquido_frenos', 12)->nullable();
            $table->string('nivel_aceite_transmicion', 12)->nullable();
            $table->string('nivel_chisgueteros', 12)->nullable();
            $table->string('nivel_aceite_dieccion', 12)->nullable();
            $table->string('luces_delanteras', 12)->nullable();
            $table->string('direccionales', 12)->nullable();
            $table->string('luces_traceras', 12)->nullable();
            $table->string('luz_toldo', 12)->nullable();
            $table->string('luz_emergencia', 12)->nullable();
            $table->string('luz_vanidad', 12)->nullable();
            $table->string('luz_frenos', 12)->nullable();
            $table->string('luz_reversa', 12)->nullable();
            $table->string('luz_placa', 12)->nullable();
            $table->string('luz_guantera', 12)->nullable();
            $table->string('kilometraje_madrina', 12)->nullable();
            $table->string('kilometraje_intercambio', 12)->nullable();
            $table->string('kilometraje_traslado', 12)->nullable();
            $table->string('kilometraje_prueba', 12)->nullable();
            $table->string('papeleria_completa', 12)->nullable();
            $table->date('fecha_venta')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mantenimiento_unidades');
    }
}
