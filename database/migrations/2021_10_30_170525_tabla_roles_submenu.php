<?php

use App\Models\Usuarios\MenuSubmenuModel;
use App\Models\Usuarios\RolesSubmenuModel;
use App\Models\Usuarios\RolModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaRolesSubmenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(RolesSubmenuModel::getTableName(), function (Blueprint $table) {
            $table->increments(RolesSubmenuModel::ID);
            $table->unsignedInteger(RolesSubmenuModel::ROL_ID);
            $table->foreign(RolesSubmenuModel::ROL_ID)
                ->references(RolModel::ID)
                ->on(RolModel::getTableName());

            $table->unsignedInteger(RolesSubmenuModel::SUBMENU_ID)->nullable();
            $table->foreign(RolesSubmenuModel::SUBMENU_ID)
                ->references(MenuSubmenuModel::ID)
                ->on(MenuSubmenuModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(RolesSubmenuModel::getTableName());
    }
}
