<?php

use App\Models\Autos\UnidadesNuevas\CatStatusRemision;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableStatusRemision extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatStatusRemision::getTableName(), function (Blueprint $table) {
            $table->increments(CatStatusRemision::ID);
            $table->string(CatStatusRemision::ESTATUS);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatStatusRemision::getTableName());
    }
}
