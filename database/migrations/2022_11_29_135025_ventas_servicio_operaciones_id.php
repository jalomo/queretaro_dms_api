<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class VentasServicioOperacionesId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('venta_servicio', function(Blueprint $table){
            $table->bigInteger('id_index_requisicion')->unsigned()->nullable();
            $table->boolean('bodyshop')->default(false);
        });

        Schema::table('venta_producto', function(Blueprint $table){
            $table->boolean('bodyshop')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('venta_servicio', function(Blueprint $table){
            $table->dropColumn('id_index_requisicion', 'bodyshop');
        });

        Schema::table('venta_producto', function(Blueprint $table){
            $table->dropColumn('bodyshop');
        });
    }
}
