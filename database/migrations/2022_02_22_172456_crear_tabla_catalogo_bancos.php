<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Caja\CatalogoBancosModel;


class CrearTablaCatalogoBancos extends Migration
{

    public function up()
    {
        Schema::create(CatalogoBancosModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatalogoBancosModel::ID);
            $table->string(CatalogoBancosModel::NOMBRE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatalogoBancosModel::getTableName());
    }
}
