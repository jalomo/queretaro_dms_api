<?php

use App\Models\Autos\EstatusVentaAutosModel;
use App\Models\Autos\UnidadesModel;
use App\Models\Autos\UnidadesNuevas\CatLineas;
use App\Models\Autos\UnidadesNuevas\RemisionModel;
use App\Models\Autos\VentasAutosModel;
use App\Models\CuentasPorCobrar\TipoFormaPagoModel;
use App\Models\Refacciones\FoliosModel;
use App\Models\Refacciones\CatTipoPagoModel;
use App\Models\Refacciones\ClientesModel;
use App\Models\Refacciones\EstatusCompra;
use App\Models\Usuarios\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaVentaUnidades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create(VentasAutosModel::getTableName(), function (Blueprint $table) {
            $table->increments(VentasAutosModel::ID);
            $table->unsignedInteger(VentasAutosModel::ID_UNIDAD)->nullable();
            $table->foreign(VentasAutosModel::ID_UNIDAD)
                ->references(RemisionModel::ID)
                ->on(RemisionModel::getTableName());

            $table->unsignedInteger(VentasAutosModel::ID_FOLIO);
            $table->foreign(VentasAutosModel::ID_FOLIO)
                ->references(FoliosModel::ID)
                ->on(FoliosModel::getTableName());

            $table->unsignedInteger(VentasAutosModel::ID_CLIENTE);
            $table->foreign(VentasAutosModel::ID_CLIENTE)
                ->references(ClientesModel::ID)
                ->on(ClientesModel::getTableName());

            $table->unsignedInteger(VentasAutosModel::ID_ESTATUS);
            $table->foreign(VentasAutosModel::ID_ESTATUS)
                ->references(EstatusVentaAutosModel::ID)
                ->on(EstatusVentaAutosModel::getTableName());

            $table->integer(VentasAutosModel::ID_TIPO_AUTO); //nuevo - seminuevo 
            $table->string(VentasAutosModel::NO_ECONOMICO)->nullable();

            $table->unsignedInteger(VentasAutosModel::ID_ASESOR);
            $table->foreign(VentasAutosModel::ID_ASESOR)
                ->references(User::ID)
                ->on(User::getTableName());

            $table->text(VentasAutosModel::DESCRIPCION_MODELO)->nullable();

            $table->string(VentasAutosModel::MONEDA);

            $table->integer(VentasAutosModel::SERVICIO_CITA_ID)->nullable(); //No se para que es -> servicio

            $table->string(VentasAutosModel::IMPUESTO_ISAN)->default(0);
            $table->string(VentasAutosModel::DESCUENTO)->default(0);
            $table->string(VentasAutosModel::IVA)->default(0);
            $table->text(VentasAutosModel::FIRMA_CLIENTE)->nullable();
            $table->text(VentasAutosModel::FIRMA_ASESOR)->nullable();
            $table->text(VentasAutosModel::FIRMA_CONTADORA)->nullable();
            $table->text(VentasAutosModel::FIRMA_CREDITO)->nullable();

            $table->string(VentasAutosModel::NO_SERIE)->nullable();

            $table->integer(VentasAutosModel::CREDITO_APROVADO)->default(0);
            $table->integer(VentasAutosModel::SOLICITA_FACTURACION)->default(0);
            $table->integer(VentasAutosModel::TIPO_VENTA)->default(TipoFormaPagoModel::FORMA_CONTADO);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(VentasAutosModel::getTableName());
    }
}
