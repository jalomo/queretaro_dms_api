<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\CuentasPorCobrar\EstatusAsientosModel;

class CrearTablaEstatusAsientos extends Migration
{
 /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(EstatusAsientosModel::getTableName(), function (Blueprint $table) {
            $table->increments(EstatusAsientosModel::ID);
            $table->string(EstatusAsientosModel::NOMBRE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(EstatusAsientosModel::getTableName());
    }
}
