<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Caja\MovimientoCajaModel;


class CrearTablaMovimientosCaja extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(MovimientoCajaModel::getTableName(), function (Blueprint $table) {
            $table->increments(MovimientoCajaModel::ID);
            $table->string(MovimientoCajaModel::NOMBRE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(MovimientoCajaModel::getTableName());
    }
}
