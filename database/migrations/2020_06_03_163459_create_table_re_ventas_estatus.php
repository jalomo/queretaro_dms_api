<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Refacciones\ReVentasEstatusModel;
use App\Models\Refacciones\EstatusVentaModel;
use App\Models\Refacciones\VentasRealizadasModel as VentasModel;

class CreateTableReVentasEstatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ReVentasEstatusModel::getTableName(), function (Blueprint $table) {
            $table->increments(ReVentasEstatusModel::ID);
            $table->unsignedInteger(ReVentasEstatusModel::VENTA_ID);
            $table->unsignedInteger(ReVentasEstatusModel::ESTATUS_VENTA_ID);
            //$table->unsignedInteger(ReVentasEstatusModel::STOCK);
            $table->boolean(ReVentasEstatusModel::ACTIVO)->default(1);
            $table->unsignedInteger(ReVentasEstatusModel::USER_ID);
            $table->foreign(ReVentasEstatusModel::ESTATUS_VENTA_ID)->references(EstatusVentaModel::ID)->on(EstatusVentaModel::getTableName());
            $table->foreign(ReVentasEstatusModel::VENTA_ID)->references(VentasModel::ID)->on(VentasModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(ReVentasEstatusModel::getTableName());
    }
}
