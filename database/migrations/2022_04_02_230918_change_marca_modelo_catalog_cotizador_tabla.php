<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Refacciones\CotizadorRefaccionesModel;
use App\Models\Refacciones\CatalogoMarcasModel;
use App\Models\Autos\CatModelosModel;

class ChangeMarcaModeloCatalogCotizadorTabla extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(CotizadorRefaccionesModel::getTableName(), function (Blueprint $table) {
            $table->dropColumn(CotizadorRefaccionesModel::MARCA);
            $table->dropColumn(CotizadorRefaccionesModel::MODELO);
            $table->unsignedInteger(CotizadorRefaccionesModel::MARCA_ID);
            $table->foreign(CotizadorRefaccionesModel::MARCA_ID)->references(CatalogoMarcasModel::ID)->on(CatalogoMarcasModel::getTableName());
            $table->unsignedInteger(CotizadorRefaccionesModel::MODELO_ID);
            $table->foreign(CotizadorRefaccionesModel::MODELO_ID)->references(CatModelosModel::ID)->on(CatModelosModel::getTableName());

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(CotizadorRefaccionesModel::getTableName(), function (Blueprint $table) {
            $table->dropColumn(CotizadorRefaccionesModel::MODELO);
        });
    }
}
