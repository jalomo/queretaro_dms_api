<?php

use App\Models\Refacciones\InventarioModel;
use App\Models\Usuarios\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MaTablaInventario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(InventarioModel::getTableName(), function (Blueprint $table) {
            $table->increments(InventarioModel::ID);
            $table->unsignedInteger(InventarioModel::ESTATUS_INVENTARIO_ID);
            $table->unsignedInteger(InventarioModel::USUARIO_REGISTRO);
            $table->foreign(InventarioModel::USUARIO_REGISTRO)->references(User::ID)->on(User::getTableName());
            $table->unsignedInteger(InventarioModel::USUARIO_ACTUALIZO)->nullable();
            $table->text(InventarioModel::JUSTIFICACION)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(InventarioModel::getTableName());
    }
}
