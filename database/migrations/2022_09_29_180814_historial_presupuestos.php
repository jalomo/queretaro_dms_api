<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class HistorialPresupuestos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('historial_presupuestos', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->bigInteger('venta_id')->unsigned();
            $table->bigInteger('venta_producto_id')->unsigned();
            $table->bigInteger('producto_id')->unsigned();
            $table->bigInteger('producto_id_latest')->unsigned();
            $table->integer('cantidad');
            $table->integer('cantidad_anterior');
            $table->decimal('precio_refaccion', 10, 2);
            $table->decimal('precio_refaccion_anterior', 10, 2);
            $table->decimal('costo_mano_obra', 10, 2);
            $table->decimal('costo_mano_obra_anterior', 10, 2);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('historial_presupuestos');
    }
}
