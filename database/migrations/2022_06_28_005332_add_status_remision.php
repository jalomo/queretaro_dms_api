<?php

use App\Models\Autos\UnidadesNuevas\CatStatusRemision;
use App\Models\Autos\UnidadesNuevas\RemisionModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusRemision extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(RemisionModel::getTableName(), function (Blueprint $table) {
            $table->unsignedInteger(RemisionModel::ESTATUS_REMISION_ID)->nullable();
            $table->foreign(RemisionModel::ESTATUS_REMISION_ID)
                ->references(CatStatusRemision::ID)
                ->on(CatStatusRemision::getTableName());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(RemisionModel::getTableName());
    }
}
