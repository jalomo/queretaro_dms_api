<?php

use App\Models\Autos\UnidadesNuevas\RemisionModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableAddLineaContabilidad extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(RemisionModel::getTableName(), function (Blueprint $table) {
            $table->integer(RemisionModel::LINEA_ID_CONTABILIDAD)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(RemisionModel::getTableName());
    }
}
