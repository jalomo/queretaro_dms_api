<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagosFacturaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pagos_factura', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('cuenta_por_cobrar_id')->nullable();
            $table->foreign('cuenta_por_cobrar_id')->references('id')->on('cuentas_por_cobrar');

            $table->unsignedInteger('tipo_pago_id')->nullable();
            $table->foreign('tipo_pago_id')->references('id')->on('cat_tipo_pago');

            $table->unsignedInteger('cfdi_id')->nullable();
            $table->foreign('cfdi_id')->references('id')->on('catalogo_cfdi');

            $table->string('factura_id')->nullable();
            $table->decimal('importe', 10, 2);
            $table->boolean('facturado')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pagos_factura');
    }
}
