<?php

use App\Models\Autos\UnidadesNuevas\DetalleCostosRemisionModel;
use App\Models\Autos\UnidadesNuevas\RemisionModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableDetalleCostosRemision extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(DetalleCostosRemisionModel::getTableName(), function (Blueprint $table) {
            $table->increments(DetalleCostosRemisionModel::ID);
            $table->unsignedInteger(DetalleCostosRemisionModel::REMISIONID);
            $table->foreign(DetalleCostosRemisionModel::REMISIONID)
                ->references(RemisionModel::ID)
                ->on(RemisionModel::getTableName());
            $table->double(DetalleCostosRemisionModel::C_VALOR_UNIDAD);
            $table->double(DetalleCostosRemisionModel::C_EQUIPO_BASE);
            $table->double(DetalleCostosRemisionModel::C_TOTAL_BASE)->nullable();
            $table->double(DetalleCostosRemisionModel::C_DEDUCCION_FORD)->nullable();
            $table->double(DetalleCostosRemisionModel::C_SEG_TRASLADO)->nullable();
            $table->double(DetalleCostosRemisionModel::C_GASTOS_TRASLADO);
            $table->double(DetalleCostosRemisionModel::C_IMP_IMPORT)->nullable();
            $table->double(DetalleCostosRemisionModel::C_FLETES_EXT)->nullable();
            $table->double(DetalleCostosRemisionModel::C_ISAN);
            $table->double(DetalleCostosRemisionModel::C_HOLDBACK);
            $table->double(DetalleCostosRemisionModel::C_DONATIVO_CCF);
            $table->double(DetalleCostosRemisionModel::C_PLAN_PISO);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(DetalleCostosRemisionModel::getTableName());
    }
}
