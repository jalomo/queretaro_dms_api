<?php

use App\Models\Refacciones\EstatusInventarioModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaEstatusInventario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(EstatusInventarioModel::getTableName(), function (Blueprint $table) {
            $table->increments(EstatusInventarioModel::ID);
            $table->string(EstatusInventarioModel::NOMBRE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(EstatusInventarioModel::getTableName());
    }
}
