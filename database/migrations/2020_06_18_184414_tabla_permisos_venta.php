<?php

use App\Models\Refacciones\permisoVentaModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaPermisosVenta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(permisoVentaModel::getTableName(), function (Blueprint $table) {
            $table->increments(permisoVentaModel::ID);
            $table->string(permisoVentaModel::USUARIO_ID);
            $table->string(permisoVentaModel::VENTA_ID);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(permisoVentaModel::getTableName());
    }
}
