<?php

use App\Models\CuentasPorCobrar\TipoFormaPagoModel;
use App\Models\Nomina\CaFormaPagoModel;
use App\Models\Refacciones\CatCfdiModel;
use App\Models\Refacciones\CatTipoPagoModel;
use App\Models\Refacciones\ProveedorRefacciones;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaProveedorRefacciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ProveedorRefacciones::getTableName(), function (Blueprint $table) {
            $table->increments(ProveedorRefacciones::ID);
            $table->string(ProveedorRefacciones::PROVEEDOR_NOMBRE);
            $table->string(ProveedorRefacciones::APELLIDO_MATERNO)->nullable();
            $table->string(ProveedorRefacciones::APELLIDO_PATERNO)->nullable();
            $table->string(ProveedorRefacciones::PROVEEDOR_RFC);
            $table->string(ProveedorRefacciones::PROVEEDOR_CALLE)->nullable();
            $table->string(ProveedorRefacciones::PROVEEDOR_NUMERO)->nullable();
            $table->string(ProveedorRefacciones::PROVEEDOR_COLONIA)->nullable();
            $table->string(ProveedorRefacciones::PROVEEDOR_ESTADO)->nullable();
            $table->string(ProveedorRefacciones::PROVEEDOR_PAIS)->nullable();
            $table->string(ProveedorRefacciones::PROVEEDOR_LOCALIDAD)->nullable();
            $table->string(ProveedorRefacciones::CODIGO_POSTAL)->nullable();
            $table->string(ProveedorRefacciones::CLAVE_IDENTIFICADOR)->nullable();
            $table->string(ProveedorRefacciones::RAZON_SOCIAL)->nullable();
            $table->string(ProveedorRefacciones::CORREO)->nullable();
            $table->string(ProveedorRefacciones::CORREO_SECUNDARIO)->nullable();
            $table->string(ProveedorRefacciones::TELEFONO_SECUNDARIO)->nullable();
            $table->string(ProveedorRefacciones::TELEFONO)->nullable();
            $table->text(ProveedorRefacciones::NOTAS)->nullable();

            $table->unsignedInteger(ProveedorRefacciones::CFDI_ID)->nullable();
            $table->foreign(ProveedorRefacciones::CFDI_ID)
                ->references(CatCfdiModel::ID)
                ->on(CatCfdiModel::getTableName());
            $table->unsignedInteger(ProveedorRefacciones::METODO_PAGO_ID)->nullable();
            $table->foreign(ProveedorRefacciones::METODO_PAGO_ID)
                ->references(CatTipoPagoModel::ID)
                ->on(CatTipoPagoModel::getTableName());
            $table->string(ProveedorRefacciones::SALDO)->nullable();
            $table->string(ProveedorRefacciones::LIMITE_CREDITO)->nullable();

            $table->unsignedInteger(ProveedorRefacciones::FORMA_PAGO_ID)->nullable();
            $table->foreign(ProveedorRefacciones::FORMA_PAGO_ID)
                ->references(TipoFormaPagoModel::ID)
                ->on(TipoFormaPagoModel::getTableName());
                
            $table->string(ProveedorRefacciones::TIPO_REGISTRO)->nullable();
            $table->smallInteger(ProveedorRefacciones::TIPO_PROVEEDOR)->default(2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(ProveedorRefacciones::getTableName());
    }
}
