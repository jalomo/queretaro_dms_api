<?php

use App\Models\Facturas\Factura;
use App\Models\Facturas\TimbreFiscal;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTimbreFiscal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(TimbreFiscal::getTableName(), function (Blueprint $table) {
            $table->increments(TimbreFiscal::ID);
            $table->text(TimbreFiscal::SELLO_CFD);
            $table->string(TimbreFiscal::NO_CERTIFICADO_SAT);
            $table->string(TimbreFiscal::RFC_PROV_CERTIF);
            $table->string(TimbreFiscal::FECHA_TIMBRADO);
            $table->text(TimbreFiscal::SELLO_SAT);
            $table->string(TimbreFiscal::UUID);
            $table->unsignedInteger(TimbreFiscal::FACTURA_ID);
            $table->foreign(TimbreFiscal::FACTURA_ID)->references(Factura::ID)->on(Factura::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(TimbreFiscal::getTableName());
    }
}
