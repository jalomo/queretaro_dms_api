<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Refacciones\VentaProductoModel;

class ChangeColumnasVentaProducto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table(VentaProductoModel::getTableName(), function (Blueprint $table) {
            $table->decimal(VentaProductoModel::TOTAL_VENTA, 10, 4)->change();
            $table->decimal(VentaProductoModel::VALOR_UNITARIO, 10, 4)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(VentaProductoModel::getTableName(), function (Blueprint $table) {
            $table->float(VentaProductoModel::TOTAL_VENTA)->change();
            $table->float(VentaProductoModel::VALOR_UNITARIO)->change();
        });
    }
}
