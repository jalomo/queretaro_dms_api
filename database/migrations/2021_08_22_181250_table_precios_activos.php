<?php

use App\Models\Autos\UnidadesNuevas\PreciosUnidadesNuevasModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablePreciosActivos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(PreciosUnidadesNuevasModel::getTableName(), function (Blueprint $table) {
            $table->increments(PreciosUnidadesNuevasModel::ID);
            $table->integer(PreciosUnidadesNuevasModel::MODELO);
            $table->string(PreciosUnidadesNuevasModel::DESCRIPCION);
            $table->date(PreciosUnidadesNuevasModel::FECHA_INICIO);
            $table->date(PreciosUnidadesNuevasModel::FECHA_FIN);
            $table->boolean(PreciosUnidadesNuevasModel::ACTIVO)->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(PreciosUnidadesNuevasModel::getTableName());
    }
}
