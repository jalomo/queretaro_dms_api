<?php

use App\Models\Refacciones\InventarioModel;
use App\Models\Refacciones\InventarioProductosModel;
use App\Models\Refacciones\ProductosModel;
use App\Models\Usuarios\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaInventarioProductos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(InventarioProductosModel::getTableName(), function (Blueprint $table) {
            $table->increments(InventarioProductosModel::ID);
            $table->unsignedInteger(InventarioProductosModel::PRODUCTO_ID);
            $table->foreign(InventarioProductosModel::PRODUCTO_ID)
                ->references(ProductosModel::ID)
                ->on(ProductosModel::getTableName());

            $table->integer(InventarioProductosModel::CANTIDAD_STOCK);
            $table->integer(InventarioProductosModel::CANTIDAD_INVENTARIO);
            
            $table->unsignedInteger(InventarioProductosModel::USUARIO_ID);
            $table->foreign(InventarioProductosModel::USUARIO_ID)
                ->references(User::ID)
                ->on(User::getTableName());

            $table->unsignedInteger(InventarioProductosModel::INVENTARIO_ID);
            $table->foreign(InventarioProductosModel::INVENTARIO_ID)
                ->references(InventarioModel::ID)
                ->on(InventarioModel::getTableName());

            $table->float(InventarioProductosModel::VALOR_UNITARIO);
            $table->string(InventarioProductosModel::DIFERENCIA);
            $table->unsignedInteger(InventarioProductosModel::FALTANTES)->nullable();
            $table->unsignedInteger(InventarioProductosModel::SOBRANTES)->nullable();
            $table->string(InventarioProductosModel::VALOR_RESIDUO);
            $table->text(InventarioProductosModel::OBSERVACIONES)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(InventarioProductosModel::getTableName());
    }
}
