<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Refacciones\VentasRealizadasModel;


class AddColumnsVentasRealizadas extends Migration
{
    public function up()
    {
        Schema::table(VentasRealizadasModel::getTableName(), function (Blueprint $table) {
            $table->double(VentasRealizadasModel::SUBTOTAL)->nullable();
            $table->double(VentasRealizadasModel::IVA)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(VentasRealizadasModel::getTableName(), function (Blueprint $table) {
            $table->dropColumn(VentasRealizadasModel::SUBTOTAL);
            $table->dropColumn(VentasRealizadasModel::IVA);
        });
    }
}
