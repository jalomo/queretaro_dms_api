<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Refacciones\CotizadorRefaccionesModel;
use App\Models\Refacciones\VentasRealizadasModel;


class ModifyTableCotizacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(CotizadorRefaccionesModel::getTableName(), function (Blueprint $table) {
            $table->unsignedInteger(CotizadorRefaccionesModel::VENTA_ID);
            $table->foreign(CotizadorRefaccionesModel::VENTA_ID)->references(VentasRealizadasModel::ID)->on(VentasRealizadasModel::getTableName());
            $table->dropColumn(CotizadorRefaccionesModel::NOMBRE_CONTACTO);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(CotizadorRefaccionesModel::getTableName(), function (Blueprint $table) {
            $table->dropColumn(CotizadorRefaccionesModel::NOMBRE_CONTACTO);
        });
    }
}
