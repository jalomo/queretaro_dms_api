<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Caja\DetalleCorteModel;
use App\Models\Caja\CorteCajaModel;
use App\Models\Refacciones\CatTipoPagoModel;

class CrearTablaDetalleCorte extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(DetalleCorteModel::getTableName(), function (Blueprint $table) {
            $table->increments(DetalleCorteModel::ID);
            $table->unsignedInteger(DetalleCorteModel::CORTE_CAJA_ID);
            $table->foreign(DetalleCorteModel::CORTE_CAJA_ID)->references(CorteCajaModel::ID)->on(CorteCajaModel::getTableName());
            $table->unsignedInteger(DetalleCorteModel::TIPO_PAGO_ID);
            $table->foreign(DetalleCorteModel::TIPO_PAGO_ID)->references(CatTipoPagoModel::ID)->on(CatTipoPagoModel::getTableName());
            $table->float(DetalleCorteModel::TOTAL_CANTIDAD_CAJA);
            $table->float(DetalleCorteModel::TOTAL_CANTIDAD_REPORTADA)->nullable();
            $table->float(DetalleCorteModel::FALTANTES)->nullable();
            $table->float(DetalleCorteModel::SOBRANTES)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CorteCajaModel::getTableName());
    }
}
