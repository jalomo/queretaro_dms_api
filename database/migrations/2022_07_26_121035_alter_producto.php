<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterProducto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('producto', function (Blueprint $table) {
            $table->bigInteger('reemplazo_id')->unsigned()->nullable();
            $table->foreign('reemplazo_id')->on('reemplazos')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('producto', function (Blueprint $table) {
            $table->dropColumn(['reemplazo_id']);
        });
    }
}
