<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Refacciones\VentasRealizadasModel;

class AddColumnsTipoClienteVentasRealizadas extends Migration
{
    public function up()
    {
        Schema::table(VentasRealizadasModel::getTableName(), function (Blueprint $table) {
            $table->integer(VentasRealizadasModel::TIPO_CLIENTE_ID)->nullable();
        });
    }

    public function down()
    {
        Schema::table(VentasRealizadasModel::getTableName(), function (Blueprint $table) {
            $table->dropColumn(VentasRealizadasModel::TIPO_CLIENTE_ID);
        });
    }
}
