<?php

use App\Models\Autos\UnidadesNuevas\RemisionModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTableRemision extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(RemisionModel::getTableName(), function (Blueprint $table) {
            $table->string(RemisionModel::CLAVE_ISAN)->nullable()->change();
            $table->string(RemisionModel::CTA_MENUDO)->nullable()->change();
            $table->string(RemisionModel::CTA_FLOTILLA)->nullable()->change();
            $table->string(RemisionModel::CTA_CONAUTO)->nullable()->change();
            $table->string(RemisionModel::CTA_INTERCAMBIO)->nullable()->change();
            $table->string(RemisionModel::CTA_PLLENO)->nullable()->change();
            $table->string(RemisionModel::CTA_INVENTARIO)->nullable()->change();
            $table->string(RemisionModel::VTA_MENUDEO)->nullable()->change();
            $table->string(RemisionModel::VTA_FLOTILLA)->nullable()->change();
            $table->string(RemisionModel::VTA_CONAUTO)->nullable()->change();
            $table->string(RemisionModel::VTA_INTERCAMBIO)->nullable()->change();
            $table->string(RemisionModel::VTA_PLLENO)->nullable()->change();
            
            $table->string(RemisionModel::NO_INVENTARIO)->nullable();
            $table->string(RemisionModel::CLAVE_SAT)->nullable();
            $table->string(RemisionModel::UNIDAD)->nullable();
            $table->string(RemisionModel::MODELO)->nullable();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(RemisionModel::getTableName());
    }
}
