<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ConsecutivoHistorialEreact extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('historial_ereact', function(Blueprint $table){
            $table->integer('consecutivo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('historial_ereact', function(Blueprint $table){
            $table->dropColumn('consecutivo');
        });
    }
}
