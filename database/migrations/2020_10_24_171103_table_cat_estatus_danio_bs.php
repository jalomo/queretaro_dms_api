<?php

use App\Models\Logistica\CatEstatusDaniosBodyshopModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableCatEstatusDanioBs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatEstatusDaniosBodyshopModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatEstatusDaniosBodyshopModel::ID);
            $table->string(CatEstatusDaniosBodyshopModel::ESTATUS);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatEstatusDaniosBodyshopModel::getTableName());
    }
}
