<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Auditoria extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('auditorias', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->nullable();
            $table->timestamp('inicio');
            $table->timestamp('terminada')->nullable();
            $table->timestamp('cancelada')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('auditoria_producto', function (Blueprint $table) {
            $table->id();
            $table->foreignId('auditoria_id')->constrained();
            $table->integer('producto_id')->unsigned()->constrained('producto');
            $table->string('ubicacion', 10)->default('GEN.');
            $table->decimal('existencia_sistema', 10, 2)->default(0);
            $table->decimal('existencia_fisico', 10, 2)->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('auditoria_producto');
        Schema::dropIfExists('auditorias');
    }
}
