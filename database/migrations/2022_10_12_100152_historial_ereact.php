<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class HistorialEreact extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('historial_ereact', function(Blueprint $table){
            $table->id();
            $table->string('nombre_archivo');
            $table->timestamp('fecha_transmision')->nullable();
            $table->timestamp('completo')->nullable();
            $table->json('filters');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('historial_ereact');
    }
}
