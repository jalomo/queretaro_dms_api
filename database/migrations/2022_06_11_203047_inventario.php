<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Inventario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::dropIfExists('inventario_productos');

        Schema::create('tipo_movimiento', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('nombre');
        });

        Schema::create('inventario_productos', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->integer('producto_id')->unsigned();
            $table->bigInteger('tipo_movimiento_id')->unsigned();
            $table->integer('cantidad')->unsigned();
            $table->integer('existia')->unsigned();
            $table->integer('existe')->unsigned();
            $table->timestamps();

            $table->foreign('producto_id')->references('id')->on('producto');
            $table->foreign('tipo_movimiento_id')->references('id')->on('tipo_movimiento');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('inventario_productos');
        Schema::drop('tipo_movimiento');
    }
}
