<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReemplazos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reemplazos', function (Blueprint $table) {
            $table->id();
            $table->integer('producto_id')->unsigned();
            $table->foreign('producto_id')->on('producto')->references('id');

            $table->integer('reemplazo_id')->unsigned();
            $table->foreign('reemplazo_id')->on('producto')->references('id');

            $table->double('precio_producto')->nullable();
            $table->double('precio_reemplazo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reemplazos');
    }
}
