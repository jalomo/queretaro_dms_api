<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TraspasoCantidadDevueltoEntregado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('traspaso_productos', function (Blueprint $table) {
            $table->integer('cantidad_entregada')->default(0);
            $table->integer('cantidad_devuelta')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('traspaso_productos', function (Blueprint $table) {
            $table->dropColumn(['cantidad_entregada', 'cantidad_devuelta']);
        });
    }
}
