<?php

use App\Models\Logistica\CatModosModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableCatModos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatModosModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatModosModel::ID);
            $table->string(CatModosModel::MODO);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatModosModel::getTableName());
    }
}
