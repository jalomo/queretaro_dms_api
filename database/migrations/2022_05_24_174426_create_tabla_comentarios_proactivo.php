<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Refacciones\ComentariosProactivosModel;
use App\Models\Refacciones\CotizadorRefaccionesModel;

class CreateTablaComentariosProactivo extends Migration
{
    public function up()
    {
        Schema::create(ComentariosProactivosModel::getTableName(), function (Blueprint $table) {
            $table->increments(ComentariosProactivosModel::ID);
            $table->text(ComentariosProactivosModel::DESCRIPCION);
            $table->unsignedInteger(ComentariosProactivosModel::COTIZADOR_REFACCION_ID);
            $table->foreign(ComentariosProactivosModel::COTIZADOR_REFACCION_ID)->references(CotizadorRefaccionesModel::ID)->on(CotizadorRefaccionesModel::getTableName());
            $table->unsignedInteger(ComentariosProactivosModel::USUARIO_ID);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(ComentariosProactivosModel::getTableName());
    }
}
