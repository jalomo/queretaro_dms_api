<?php

use App\Models\Refacciones\EstatusCompra;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EstatusCompras extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(EstatusCompra::getTableName(), function (Blueprint $table) {
            $table->increments(EstatusCompra::ID);
            $table->string(EstatusCompra::NOMBRE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(EstatusCompra::getTableName());
    }
}
