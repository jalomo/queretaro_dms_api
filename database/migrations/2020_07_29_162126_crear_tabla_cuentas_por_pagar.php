<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\CuentasPorPagar\CuentasPorPagarModel;
use App\Models\CuentasPorCobrar\PlazoCreditoModel;
use App\Models\CuentasPorCobrar\EstatusCuentaModel;
use App\Models\CuentasPorCobrar\TipoFormaPagoModel;
use App\Models\Refacciones\CatTipoPagoModel;
use App\Models\Refacciones\FoliosModel;
use App\Models\Refacciones\ProveedorRefacciones as ProveedorModel;


class CrearTablaCuentasPorPagar extends Migration
{
    public function up()
    {
        Schema::create(CuentasPorPagarModel::getTableName(), function (Blueprint $table) {
            $table->increments(CuentasPorPagarModel::ID);
            $table->unsignedInteger(CuentasPorPagarModel::FOLIO_ID);
            $table->foreign(CuentasPorPagarModel::FOLIO_ID)->references(FoliosModel::ID)->on(FoliosModel::getTableName());
            $table->unsignedInteger(CuentasPorPagarModel::PROVEEDOR_ID);
            $table->foreign(CuentasPorPagarModel::PROVEEDOR_ID)->references(ProveedorModel::ID)->on(ProveedorModel::getTableName());
            $table->unsignedInteger(CuentasPorPagarModel::ESTATUS_CUENTA_ID);
            $table->foreign(CuentasPorPagarModel::ESTATUS_CUENTA_ID)->references(EstatusCuentaModel::ID)->on(EstatusCuentaModel::getTableName());
            $table->text(CuentasPorPagarModel::CONCEPTO);
            $table->unsignedInteger(CuentasPorPagarModel::TIPO_FORMA_PAGO_ID)->nullable();
            $table->foreign(CuentasPorPagarModel::TIPO_FORMA_PAGO_ID)->references(TipoFormaPagoModel::ID)->on(TipoFormaPagoModel::getTableName());
            $table->unsignedInteger(CuentasPorPagarModel::TIPO_PAGO_ID)->nullable();
            $table->foreign(CuentasPorPagarModel::TIPO_PAGO_ID)->references(CatTipoPagoModel::ID)->on(CatTipoPagoModel::getTableName());
            $table->unsignedInteger(CuentasPorPagarModel::PLAZO_CREDITO_ID)->nullable();
            $table->foreign(CuentasPorPagarModel::PLAZO_CREDITO_ID)->references(PlazoCreditoModel::ID)->on(PlazoCreditoModel::getTableName());
            $table->float(CuentasPorPagarModel::IMPORTE);
            $table->float(CuentasPorPagarModel::TOTAL);
            $table->float(CuentasPorPagarModel::ENGANCHE)->nullable();
            $table->float(CuentasPorPagarModel::TASA_INTERES)->nullable();
            $table->float(CuentasPorPagarModel::INTERESES)->nullable();
            $table->text(CuentasPorPagarModel::COMENTARIOS)->nullable();
            $table->date(CuentasPorPagarModel::FECHA);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CuentasPorPagarModel::getTableName());
    }
}
