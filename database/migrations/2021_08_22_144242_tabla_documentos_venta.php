<?php

use App\Models\Autos\DocumentosVentaModel;
use App\Models\Autos\TipoDocumentosVentaModel;
use App\Models\Autos\UnidadesModel;
use App\Models\Autos\VentasAutosModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaDocumentosVenta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(DocumentosVentaModel::getTableName(), function (Blueprint $table) {
            $table->increments(DocumentosVentaModel::ID);
            $table->string(DocumentosVentaModel::NOMBRE_ARCHIVO);
            $table->string(DocumentosVentaModel::RUTA_ARCHIVO);
            $table->unsignedInteger(DocumentosVentaModel::ID_VENTA_UNIDAD);
            $table->foreign(DocumentosVentaModel::ID_VENTA_UNIDAD)
                ->references(VentasAutosModel::ID)
                ->on(VentasAutosModel::getTableName());

            $table->unsignedInteger(DocumentosVentaModel::ID_TIPO_DOCUMENTO);
            $table->foreign(DocumentosVentaModel::ID_TIPO_DOCUMENTO)
                ->references(TipoDocumentosVentaModel::ID)
                ->on(TipoDocumentosVentaModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(DocumentosVentaModel::getTableName());
    }
}
