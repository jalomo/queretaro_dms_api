<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AumentarValorUnitarioA10 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try{
            // MYSQL y MARIADB 
            DB::beginTransaction();
            DB::statement('ALTER TABLE producto MODIFY COLUMN `valor_unitario` DOUBLE(10,2) DEFAULT 0');
            DB::statement('ALTER TABLE producto MODIFY COLUMN `precio_factura` DOUBLE(10,2) DEFAULT 0');
            DB::commit();
        } catch(Exception $e){
            // POSGRESS
            DB::rollBack();
            // DB::statement('ALTER TABLE producto ALTER COLUMN valor_unitario TYPE DECIMAL(10,2)');
            // DB::statement('ALTER TABLE producto ALTER COLUMN precio_factura TYPE DECIMAL(10,2)');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        try{
            DB::beginTransaction();
            // MYSQL y MARIADB 
            DB::statement('ALTER TABLE producto MODIFY COLUMN `valor_unitario` FLOAT(8,2) DEFAULT 0');
            DB::statement('ALTER TABLE producto MODIFY COLUMN `precio_factura` FLOAT(8,2) DEFAULT 0');
            DB::commit();
        } catch(Exception $e){
            DB::rollBack();
            // POSGRESS
            // DB::statement('ALTER TABLE producto ALTER COLUMN valor_unitario TYPE FLOAT(8,2)');
            // DB::statement('ALTER TABLE producto ALTER COLUMN precio_factura TYPE FLOAT(8,2)');
        }
        
    }
}
