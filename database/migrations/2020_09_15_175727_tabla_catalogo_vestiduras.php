<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Autos\CatalogoVestidurasModel;

class TablaCatalogoVestiduras extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatalogoVestidurasModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatalogoVestidurasModel::ID);
            $table->string(CatalogoVestidurasModel::NOMBRE);
            $table->string(CatalogoVestidurasModel::CLAVE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatalogoVestidurasModel::getTableName());
    }
}
