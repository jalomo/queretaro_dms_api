<?php

use App\Models\Refacciones\CatalogoColoresModel;
use App\Models\Autos\UnidadesNuevas\CatCombustible;
use App\Models\Autos\UnidadesNuevas\DetalleRemisionModel;
use App\Models\Autos\UnidadesNuevas\RemisionModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableDetalleRemision extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(DetalleRemisionModel::getTableName(), function (Blueprint $table) {
            $table->increments(DetalleRemisionModel::ID);
            $table->unsignedInteger(DetalleRemisionModel::REMISIONID);
            $table->foreign(DetalleRemisionModel::REMISIONID)
                ->references(RemisionModel::ID)
                ->on(RemisionModel::getTableName());
            $table->string(DetalleRemisionModel::PUERTAS);
            $table->string(DetalleRemisionModel::CILINDROS);
            $table->string(DetalleRemisionModel::TRANSMISION);
            $table->string(DetalleRemisionModel::CAPACIDAD);
            $table->unsignedInteger(DetalleRemisionModel::COMBUSTIBLE_ID);
            $table->foreign(DetalleRemisionModel::COMBUSTIBLE_ID)
                ->references(CatCombustible::ID)
                ->on(CatCombustible::getTableName());
            $table->unsignedInteger(DetalleRemisionModel::COLORINTID);
            $table->foreign(DetalleRemisionModel::COLORINTID)
                ->references(CatalogoColoresModel::ID)
                ->on(CatalogoColoresModel::getTableName());
            $table->unsignedInteger(DetalleRemisionModel::COLOREXTID);
            $table->foreign(DetalleRemisionModel::COLOREXTID)
                ->references(CatalogoColoresModel::ID)
                ->on(CatalogoColoresModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(DetalleRemisionModel::getTableName());
    }
}
