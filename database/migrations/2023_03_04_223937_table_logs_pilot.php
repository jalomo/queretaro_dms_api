<?php

use App\Models\Logs\Pilot\LogsPilotModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableLogsPilot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(LogsPilotModel::getTableName(), function (Blueprint $table) {
            $table->increments(LogsPilotModel::ID);
            $table->integer(LogsPilotModel::TYPE);
            $table->text(LogsPilotModel::POSTDATA);
            $table->text(LogsPilotModel::RESPONSEDATA);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(LogsPilotModel::getTableName());
    }
}
