<?php

use App\Models\Autos\UnidadesNuevas\RemisionModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterRemisionesPilotId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(RemisionModel::getTableName(), function (Blueprint $table) {
            $table->text(RemisionModel::PILOT_ID)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(RemisionModel::getTableName(), function (Blueprint $table) {
            $table->float(RemisionModel::PILOT_ID);
        });
    }
}
