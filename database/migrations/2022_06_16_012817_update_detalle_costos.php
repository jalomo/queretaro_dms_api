<?php

use App\Models\Autos\UnidadesNuevas\DetalleCostosRemisionModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateDetalleCostos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(DetalleCostosRemisionModel::getTableName(), function (Blueprint $table) {
            $table->dropColumn(DetalleCostosRemisionModel::C_ISAN);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(DetalleCostosRemisionModel::getTableName());
    }
}
