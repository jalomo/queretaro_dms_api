<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Refacciones\VentasRealizadasModel;

class ChangeColumnasTipoVentas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table(VentasRealizadasModel::getTableName(), function (Blueprint $table) {
            $table->decimal(VentasRealizadasModel::SUBTOTAL, 10, 4)->change();
            $table->decimal(VentasRealizadasModel::IVA, 10, 4)->change();
            $table->decimal(VentasRealizadasModel::VENTA_TOTAL, 10, 4)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(VentasRealizadasModel::getTableName(), function (Blueprint $table) {
            $table->float(VentasRealizadasModel::SUBTOTAL)->change();
            $table->float(VentasRealizadasModel::IVA)->change();
            $table->float(VentasRealizadasModel::VENTA_TOTAL)->change();
        });
    }
}
