<?php

use App\Models\Autos\UnidadesNuevas\RemisionModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Adddisponibleremision extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(RemisionModel::getTableName(), function (Blueprint $table) {
            $table->boolean(RemisionModel::DISPONIBLE)->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(RemisionModel::getTableName(), function (Blueprint $table) {
            $table->boolean(RemisionModel::DISPONIBLE);
        });
    }
}
