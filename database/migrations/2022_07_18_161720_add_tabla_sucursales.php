<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Refacciones\SucursalesModel;

class AddTablaSucursales extends Migration
{
       /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(SucursalesModel::getTableName(), function (Blueprint $table) {
            $table->increments(SucursalesModel::ID);
            $table->string(SucursalesModel::NOMBRE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(SucursalesModel::getTableName());
    }
}
