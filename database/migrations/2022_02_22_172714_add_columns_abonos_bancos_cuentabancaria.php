<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\CuentasPorCobrar\AbonosModel;
use App\Models\Caja\CatalogoBancosModel;

class AddColumnsAbonosBancosCuentabancaria extends Migration
{

    public function up()
    {
        Schema::table(AbonosModel::getTableName(), function (Blueprint $table) {
            $table->unsignedInteger(AbonosModel::BANCO_PAGAR_ID)->nullable();
            $table->foreign(AbonosModel::BANCO_PAGAR_ID)->references(CatalogoBancosModel::ID)->on(CatalogoBancosModel::getTableName());
            $table->string(AbonosModel::REFERENCIA_TARJETA)->nullable();
            $table->string(AbonosModel::REFERENCIA_CHEQUE)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(AbonosModel::getTableName(), function (Blueprint $table) {
            $table->dropForeign([AbonosModel::BANCO_PAGAR_ID]);
            $table->dropColumn(AbonosModel::BANCO_PAGAR_ID);
            $table->dropColumn(AbonosModel::REFERENCIA_TARJETA);
            $table->dropColumn(AbonosModel::REFERENCIA_CHEQUE);
        });
    }
}
