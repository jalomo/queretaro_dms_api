<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ClaveSat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('pedidos_productos', function (Blueprint $table) {
            $table->string('clave_sat')->nullable();
            $table->string('unidad_sat')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('pedidos_productos', function (Blueprint $table) {
            $table->dropColumn(['clave_sat', 'unidad_sat']);
        });
    }
}
