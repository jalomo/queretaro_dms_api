<?php

use App\Models\Refacciones\OperacionesPiezasModel;
use App\Models\Refacciones\ProductosModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaOperacionesPiezas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(OperacionesPiezasModel::getTableName(), function (Blueprint $table) {
            $table->increments(OperacionesPiezasModel::ID);
            $table->text(OperacionesPiezasModel::NO_ORDEN);
            $table->text(OperacionesPiezasModel::NO_OPERACION);
            $table->float(OperacionesPiezasModel::TOTAL);
            $table->unsignedInteger(OperacionesPiezasModel::ID_REFACCION);
            $table->foreign(OperacionesPiezasModel::ID_REFACCION)
                ->references(ProductosModel::ID)
                ->on(ProductosModel::getTableName());
                
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(OperacionesPiezasModel::getTableName());
    }
}
