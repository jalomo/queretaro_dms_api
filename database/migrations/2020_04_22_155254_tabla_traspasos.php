<?php

use App\Models\Refacciones\FoliosModel;
use App\Models\Refacciones\Almacenes;
use App\Models\Refacciones\Traspasos;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaTraspasos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('traspasos', function (Blueprint $table) {
            $table->increments('id');
            
            $table->unsignedInteger('almacen_origen_id');
            $table->foreign('almacen_origen_id')->references('id')->on('traspasos');

            $table->unsignedInteger('almacen_destino_id');
            $table->foreign('almacen_destino_id')->references('id')->on('traspasos');
            
            $table->unsignedInteger('folio_id');
            $table->foreign('folio_id')->references(FoliosModel::ID)->on(FoliosModel::getTableName());

            $table->integer('estatus')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('traspasos');
    }
}
