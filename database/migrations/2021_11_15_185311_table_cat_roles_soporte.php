<?php

use App\Models\Soporte\CatRolesSoporteModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableCatRolesSoporte extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatRolesSoporteModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatRolesSoporteModel::ID);
            $table->text(CatRolesSoporteModel::ROL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatRolesSoporteModel::getTableName());
    }
}
