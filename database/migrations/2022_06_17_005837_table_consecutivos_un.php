<?php

use App\Models\Autos\UnidadesNuevas\ConsecutivosModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableConsecutivosUn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ConsecutivosModel::getTableName(), function (Blueprint $table) {
            $table->increments(ConsecutivosModel::ID);
            $table->integer(ConsecutivosModel::ANIO);
            $table->string(ConsecutivosModel::CAT_CONSECUTIVO);
            $table->integer(ConsecutivosModel::CONSECUTIVO);
            $table->integer(ConsecutivosModel::TIPO);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(ConsecutivosModel::getTableName());
    }
}
