<?php

use App\Models\Usuarios\MenuSeccionesModel;
use App\Models\Usuarios\RolesSeccionesModel;
use App\Models\Usuarios\RolModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaRolesSeccion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(RolesSeccionesModel::getTableName(), function (Blueprint $table) {
            $table->increments(RolesSeccionesModel::ID);
            $table->unsignedInteger(RolesSeccionesModel::ROL_ID);
            $table->foreign(RolesSeccionesModel::ROL_ID)
                ->references(RolModel::ID)
                ->on(RolModel::getTableName());

            $table->unsignedInteger(RolesSeccionesModel::SECCION_ID)->nullable();
            $table->foreign(RolesSeccionesModel::SECCION_ID)
                ->references(MenuSeccionesModel::ID)
                ->on(MenuSeccionesModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(RolesSeccionesModel::getTableName());
    }
}
