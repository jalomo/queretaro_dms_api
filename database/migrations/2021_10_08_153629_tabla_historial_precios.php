<?php

use App\Models\Refacciones\HistorialPreciosRefaccionesModel;
use App\Models\Refacciones\ProductosModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaHistorialPrecios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(HistorialPreciosRefaccionesModel::getTableName(), function (Blueprint $table) {
            $table->increments(HistorialPreciosRefaccionesModel::ID);
            $table->text(HistorialPreciosRefaccionesModel::PRECIO);
            $table->text(HistorialPreciosRefaccionesModel::PRECIO_PUBLICO);
            $table->unsignedInteger(HistorialPreciosRefaccionesModel::PRODUCTO_ID);
            $table->foreign(HistorialPreciosRefaccionesModel::PRODUCTO_ID)
                ->references(ProductosModel::ID)
                ->on(ProductosModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(HistorialPreciosRefaccionesModel::getTableName());
    }
}
