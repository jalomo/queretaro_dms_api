<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Traspasos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::dropIfExists('detalle_traspaso_producto_almacen');
        Schema::dropIfExists('re_estatus_traspaso');
        Schema::dropIfExists('traspasos');
        Schema::dropIfExists('estatus_traspaso');

        Schema::create('traspasos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('folio')->nullable();
            $table->integer('venta_id')->unsigned()->nullable();
            $table->foreign('venta_id')->references('id')->on('ventas');
            $table->bigInteger('origen_almacen_id')->unsigned();
            $table->bigInteger('destino_almacen_id')->unsigned();
            $table->timestamps();
        });

        Schema::create('estatus_traspaso', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('nombre')->nullable();
            $table->timestamps();
        });

        Schema::create('re_estatus_traspaso', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('traspasos_id')->unsigned();
            $table->foreign('traspasos_id')->references('id')->on('traspasos');
            $table->bigInteger('estatus_id')->unsigned();
            $table->foreign('estatus_id')->references('id')->on('estatus_traspaso');
            $table->timestamps();
        });

        Schema::create('traspaso_productos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('traspasos_id')->unsigned();
            $table->foreign('traspasos_id')->references('id')->on('traspasos');
            $table->string('no_identificacion');
            $table->integer('cantidad');
            $table->double('valor_unitario')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('re_estatus_traspaso');
        Schema::dropIfExists('traspaso_productos');
        Schema::dropIfExists('estatus_traspaso');
        Schema::dropIfExists('traspasos');
    }
}
