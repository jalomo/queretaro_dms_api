<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EstatusPedidos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('estatus_pedidos', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('nombre');
            $table->timestamps();
        });

        Schema::table('pedidos', function (Blueprint $table) {
            $table->bigInteger('actual')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('estatus_pedidos');
        Schema::table('pedidos', function (Blueprint $table) {
            $table->dropColumn(['actual']);
        });
    }
}
