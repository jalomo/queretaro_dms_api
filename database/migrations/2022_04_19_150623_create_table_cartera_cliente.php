<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Contabilidad\CarteraClienteModel;
use App\Models\Refacciones\ClientesModel;
use App\Models\CuentasPorCobrar\PlazoCreditoModel;

class CreateTableCarteraCliente extends Migration
{
    public function up()
    {
        Schema::create(CarteraClienteModel::getTableName(), function (Blueprint $table) {
            $table->increments(CarteraClienteModel::ID);
            $table->unsignedInteger(CarteraClienteModel::CLIENTE_ID);
            $table->foreign(CarteraClienteModel::CLIENTE_ID)->references(ClientesModel::ID)->on(ClientesModel::getTableName());
            $table->unsignedInteger(CarteraClienteModel::PLAZO_CREDITO_ID)->nullable();
            $table->foreign(CarteraClienteModel::PLAZO_CREDITO_ID)->references(PlazoCreditoModel::ID)->on(PlazoCreditoModel::getTableName());
            $table->float(CarteraClienteModel::MONTO_ACTUAL);
            $table->date(CarteraClienteModel::FECHA_OPERACION);
            $table->text(CarteraClienteModel::COMENTARIOS)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CarteraClienteModel::getTableName());
    }
}
