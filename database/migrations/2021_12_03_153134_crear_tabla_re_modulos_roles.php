<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Usuarios\ReRolesModulosModel;
use App\Models\Usuarios\Menu\ModulosModel;
use App\Models\Usuarios\RolModel;

class CrearTablaReModulosRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ReRolesModulosModel::getTableName(), function (Blueprint $table) {
            $table->increments(ReRolesModulosModel::ID);
            $table->unsignedInteger(ReRolesModulosModel::ROL_ID);
            $table->foreign(ReRolesModulosModel::ROL_ID)->references(RolModel::ID)->on(RolModel::getTableName());
            $table->unsignedInteger(ReRolesModulosModel::MODULO_ID);
            $table->foreign(ReRolesModulosModel::MODULO_ID)->references(ModulosModel::ID)->on(ModulosModel::getTableName());
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop(ReRolesModulosModel::getTableName());
    }

}
