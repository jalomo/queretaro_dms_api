<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Refacciones\DetalleCotizadorModel;
use App\Models\Refacciones\CotizadorRefaccionesModel;


class CreateTablaDetalleCotizadorRefacciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(DetalleCotizadorModel::getTableName(), function (Blueprint $table) {
            $table->increments(DetalleCotizadorModel::ID);
            $table->unsignedInteger(DetalleCotizadorModel::COTIZADOR_ID);
            $table->foreign(DetalleCotizadorModel::COTIZADOR_ID)->references(CotizadorRefaccionesModel::ID)->on(CotizadorRefaccionesModel::getTableName());
            $table->integer(DetalleCotizadorModel::CANTIDAD);
            $table->string(DetalleCotizadorModel::DESCRIPCION)->nullable();
            $table->string(DetalleCotizadorModel::NUMERO_PARTE)->nullable();
            $table->float(DetalleCotizadorModel::PRECIO_NETO);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(DetalleCotizadorModel::getTableName());
    }
}
