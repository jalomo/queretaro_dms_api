<?php

use App\Models\Autos\UnidadesNuevas\RemisionModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Autos\UnidadesNuevas\CatEstatusUNModel;

class AddEstatusPilotUnidad extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(RemisionModel::getTableName(), function (Blueprint $table) {
            $table->unsignedInteger(RemisionModel::ESTATUS_ID_PILOT)->default(1);
            $table->foreign(RemisionModel::ESTATUS_ID_PILOT)
                ->references(CatEstatusUNModel::ID)
                ->on(CatEstatusUNModel::getTableName());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
