<?php

use App\Models\Autos\UnidadesNuevas\EquipoEspecialRemisionModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableEquipoEspecialRemision extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(EquipoEspecialRemisionModel::getTableName(), function (Blueprint $table) {
            $table->increments(EquipoEspecialRemisionModel::ID);
            $table->unsignedInteger(EquipoEspecialRemisionModel::REMISION_ID);
            $table->foreign(EquipoEspecialRemisionModel::REMISION_ID)
                ->references(EquipoEspecialRemisionModel::ID)
                ->on(EquipoEspecialRemisionModel::getTableName());

            $table->text(EquipoEspecialRemisionModel::DESCRIPCION);
            $table->float(EquipoEspecialRemisionModel::IMPORTE);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(FacturacionModel::getTableName());
    }
}
