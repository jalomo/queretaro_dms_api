<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Caja\RealizarCorteCajaModel;
use App\Models\Refacciones\CatTipoPagoModel;

class CreateTableRealizarCorteCaja extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(RealizarCorteCajaModel::getTableName(), function (Blueprint $table) {
            $table->increments(RealizarCorteCajaModel::ID);
            $table->string(RealizarCorteCajaModel::NUMERO_DOCUMENTO)->nullable();
            $table->text(RealizarCorteCajaModel::DESCRIPCION)->nullable();
            $table->unsignedInteger(RealizarCorteCajaModel::TIPO_PAGO_ID);
            $table->foreign(RealizarCorteCajaModel::TIPO_PAGO_ID)->references(CatTipoPagoModel::ID)->on(CatTipoPagoModel::getTableName());
            $table->float(RealizarCorteCajaModel::CANTIDAD);
            $table->date(RealizarCorteCajaModel::FECHA_REGISTRO);
            $table->float(RealizarCorteCajaModel::CANTIDAD_SISTEMA)->nullable();
            $table->float(RealizarCorteCajaModel::FALTANTES)->nullable();
            $table->float(RealizarCorteCajaModel::SOBRANTES)->nullable();
            $table->boolean(RealizarCorteCajaModel::AUTORIZADO)->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('realizar_corte_caja');
    }
}
