<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Refacciones\StockProductosModel;
use App\Models\Refacciones\ProductosModel;

class CreateTablaStockProductos extends Migration
{
   /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(StockProductosModel::getTableName(), function (Blueprint $table) {
            $table->increments(StockProductosModel::ID);
            $table->unsignedInteger(StockProductosModel::PRODUCTO_ID);
            $table->foreign(StockProductosModel::PRODUCTO_ID)->references(ProductosModel::ID)->on(ProductosModel::getTableName());
            $table->unsignedInteger(StockProductosModel::CANTIDAD_ACTUAL);
            $table->unsignedInteger(StockProductosModel::CANTIDAD_ALMACEN_PRIMARIO)->nullable();
            $table->unsignedInteger(StockProductosModel::CANTIDAD_ALMACEN_SECUNDARIO)->nullable();
            $table->unsignedInteger(StockProductosModel::CANTIDAD_COMPRAS)->nullable();
            $table->unsignedInteger(StockProductosModel::CANTIDAD_VENTAS)->nullable();
            $table->float(StockProductosModel::TOTAL_PRECIO_COMPRAS)->nullable();
            $table->float(StockProductosModel::TOTAL_PRECIO_VENTAS)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(StockProductosModel::getTableName());

    }
}
