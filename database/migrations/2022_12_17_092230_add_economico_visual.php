<?php

use App\Models\Autos\UnidadesNuevas\RemisionModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEconomicoVisual extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(RemisionModel::getTableName(), function (Blueprint $table) {
            $table->string(RemisionModel::ECONOMICO_VISUAL)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(RemisionModel::getTableName(), function (Blueprint $table) {
            $table->dropColumn(RemisionModel::ECONOMICO_VISUAL);
        });
    }
}
