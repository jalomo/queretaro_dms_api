<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Refacciones\DevolucionVentaProductoModel;

class DevolucionVentaProductoVentaId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table(DevolucionVentaProductoModel::getTableName(), function(Blueprint $table){
            $table->integer(DevolucionVentaProductoModel::VENTA_ID)->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table(DevolucionVentaProductoModel::getTableName(), function(Blueprint $table){
            $table->dropColumn(DevolucionVentaProductoModel::VENTA_ID);
        });
    }
}
