<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Refacciones\DevolucionPiezaModel;
use App\Models\Refacciones\ProductosModel;

class CrearTablaDevolucionPieza extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(DevolucionPiezaModel::getTableName(), function (Blueprint $table) {
            $table->increments(DevolucionPiezaModel::ID);
            $table->unsignedInteger(DevolucionPiezaModel::PRODUCTO_ID);
            $table->foreign(DevolucionPiezaModel::PRODUCTO_ID)->references(ProductosModel::ID)->on(ProductosModel::getTableName());
            $table->unsignedInteger(DevolucionPiezaModel::ORDEN_COMPRA_ID);
            $table->unsignedInteger(DevolucionPiezaModel::CANTIDAD);
            $table->float(DevolucionPiezaModel::TOTAL);
            $table->string(DevolucionPiezaModel::NOTA_CREDITO)->nullable();
            $table->text(DevolucionPiezaModel::OBSERVACIONES)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(DevolucionPiezaModel::getTableName());
    }
}
