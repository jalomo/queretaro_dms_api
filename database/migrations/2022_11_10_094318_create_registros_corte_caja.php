<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Caja\RegistroCorteCajaModel;
use App\Models\Refacciones\CatTipoPagoModel;
use App\Models\Refacciones\FoliosModel;

class CreateRegistrosCorteCaja extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(RegistroCorteCajaModel::getTableName(), function (Blueprint $table) {
            $table->increments(RegistroCorteCajaModel::ID);
            $table->date(RegistroCorteCajaModel::FECHA_REGISTRO);
            $table->text(RegistroCorteCajaModel::DESCRIPCION);
            $table->unsignedInteger(RegistroCorteCajaModel::FOLIO_ID)->nullable();
            $table->foreign(RegistroCorteCajaModel::FOLIO_ID)->references(FoliosModel::ID)->on(FoliosModel::getTableName());  
            $table->unsignedInteger(RegistroCorteCajaModel::TIPO_PAGO_ID);
            $table->foreign(RegistroCorteCajaModel::TIPO_PAGO_ID)->references(CatTipoPagoModel::ID)->on(CatTipoPagoModel::getTableName());
            $table->float(RegistroCorteCajaModel::TOTAL_CANTIDAD_CAJA);
            $table->float(RegistroCorteCajaModel::TOTAL_CANTIDAD_REPORTADA)->nullable();
            $table->float(RegistroCorteCajaModel::FALTANTES)->nullable();
            $table->float(RegistroCorteCajaModel::SOBRANTES)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registros_corte_caja');
    }
}
