<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CostoPromedioHistorial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('costo_promedio', function(Blueprint $table){
            $table->id();
            $table->integer('movimiento_id');
            $table->integer('producto_id')->unsigned()->constrained('producto');
            $table->integer('entrada')->unsigned();
            $table->integer('existencia')->unsigned();
            $table->decimal('costo_existencia', 10, 4);
            $table->decimal('costo_movimiento', 10, 4);
            $table->decimal('costo_promedio_existencia', 10, 4);
            $table->decimal('costo_promedio_movimiento', 10, 4);
            $table->decimal('costo_promedio', 10, 4);
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('costo_promedio');
    }
}
