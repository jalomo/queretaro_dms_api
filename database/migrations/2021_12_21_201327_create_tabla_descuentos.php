<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Caja\DescuentosModel;
use App\Models\Refacciones\FoliosModel;

class CreateTablaDescuentos extends Migration
{
    /**
     * @return void
     */
    public function up()
    {
        Schema::create(DescuentosModel::getTableName(), function (Blueprint $table) {
            $table->increments(DescuentosModel::ID);
            $table->unsignedInteger(DescuentosModel::FOLIO_ID);
            $table->foreign(DescuentosModel::FOLIO_ID)->references(FoliosModel::ID)->on(FoliosModel::getTableName());
            $table->smallInteger(DescuentosModel::TIPO_DESCUENTO);
            $table->float(DescuentosModel::TOTAL_APLICAR);
            $table->float(DescuentosModel::CANTIDAD_DESCONTADA);
            $table->float(DescuentosModel::PORCENTAJE);
            $table->float(DescuentosModel::TOTAL_DESCUENTO);
            $table->date(DescuentosModel::FECHA);
            $table->text(DescuentosModel::COMENTARIO)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(DescuentosModel::getTableName());
    }
}
