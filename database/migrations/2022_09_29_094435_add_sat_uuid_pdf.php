<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSatUuidPdf extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pagos_factura', function (Blueprint $table) {
            $table->string('sat_uuid',50)->nullable();
            $table->string('sat_pdf')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pagos_factura', function (Blueprint $table) {
            $table->dropColumn(['sat_uuid']);
            $table->dropColumn(['sat_pdf']);
        });
    }
}
