<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ValorUnitarioCostoPromedio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('costo_promedio', function(Blueprint $table){
            $table->decimal('valor_unitario_existencia', 10, 4)->nullable();
            $table->decimal('valor_unitario_movimiento', 10, 4)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('costo_promedio', function(Blueprint $table){
            $table->dropColumn('valor_unitario_existencia', 'valor_unitario_movimiento');
        });
    }
}
