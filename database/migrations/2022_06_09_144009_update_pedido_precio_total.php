<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdatePedidoPrecioTotal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('pedidos_productos', function(Blueprint $table){
            $table->integer('cantidad_entregada')->default(0);
            $table->double('precio', 10, 2)->nullable();
            $table->double('total', 10, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('pedidos_productos', function (Blueprint $table) {
            $table->dropColumn(['cantidad_entregada', 'precio', 'total']);
        });
    }
}
