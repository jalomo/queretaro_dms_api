<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Caja\AnticiposModel;
use App\Models\Caja\EstatusAnticiposModel;
use App\Models\Refacciones\ClientesModel;
use App\Models\Contabilidad\CatalogoProcesosModel;
use App\Models\Refacciones\FoliosModel;

class CreateTablaAnticipos extends Migration
{
    /**
     * @return void
     */
    public function up()
    {
        Schema::create(AnticiposModel::getTableName(), function (Blueprint $table) {
            $table->increments(AnticiposModel::ID);
            $table->float(AnticiposModel::TOTAL);
            $table->unsignedInteger(AnticiposModel::CLIENTE_ID);
            $table->foreign(AnticiposModel::CLIENTE_ID)->references(ClientesModel::ID)->on(ClientesModel::getTableName());
            $table->unsignedInteger(AnticiposModel::TIPO_PROCESO_ID);
            $table->foreign(AnticiposModel::TIPO_PROCESO_ID)->references(CatalogoProcesosModel::ID)->on(CatalogoProcesosModel::getTableName());
            $table->date(AnticiposModel::FECHA);
            $table->unsignedInteger(AnticiposModel::ESTATUS_ID)->default(1);
            $table->foreign(AnticiposModel::ESTATUS_ID)->references(EstatusAnticiposModel::ID)->on(EstatusAnticiposModel::getTableName());
            $table->unsignedInteger(AnticiposModel::FOLIO_ID)->nullable();
            $table->foreign(AnticiposModel::FOLIO_ID)->references(FoliosModel::ID)->on(FoliosModel::getTableName());
            $table->text(AnticiposModel::COMENTARIO)->nullable();
            $table->unsignedInteger(AnticiposModel::FOLIO_APLICADO)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(AnticiposModel::getTableName());
    }
}
