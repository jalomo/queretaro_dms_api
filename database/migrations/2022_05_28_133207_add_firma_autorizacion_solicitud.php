<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFirmaAutorizacionSolicitud extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('traspasos', function (Blueprint $table) {
            $table->binary('firma_solicitud')->nullable();
            $table->binary('firma_autorizacion')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('traspasos', function (Blueprint $table) {
            $table->dropColumn(['firma_solicitud', 'firma_autorizacion']);
        });
    }
}
