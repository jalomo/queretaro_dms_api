<?php

use App\Models\CPVentas\CatStatusCPModel;
use App\Models\CPVentas\CPVentasModel;
use App\Models\Usuarios\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableContactoProactivoVentas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CPVentasModel::getTableName(), function (Blueprint $table) {
            $table->increments(CPVentasModel::ID);
            $table->string(CPVentasModel::NO_CUENTA);
            $table->string(CPVentasModel::VIN);
            $table->string(CPVentasModel::APELLIDO);
            $table->string(CPVentasModel::NOMBRE);
            $table->string(CPVentasModel::TIPO_CLIENTE);
            $table->string(CPVentasModel::TELEFONO_OFICINA);
            $table->string(CPVentasModel::TELEFONO_CASA);
            $table->string(CPVentasModel::ENGANCHE);
            $table->integer(CPVentasModel::MESES_CONTRATO);
            $table->date(CPVentasModel::FECHA_INICIO_CONTRATO);
            $table->date(CPVentasModel::FECHA_TERMINO);
            $table->float(CPVentasModel::TASA_INTERES);
            $table->string(CPVentasModel::MENSUALIDAD);
            $table->string(CPVentasModel::MONTO_FINANCIADO);
            $table->string(CPVentasModel::MARCA);
            $table->string(CPVentasModel::MODELO);
            $table->string(CPVentasModel::LINEA);
            $table->string(CPVentasModel::TIPO_VEHICULO);
            $table->year(CPVentasModel::ANIO);
            $table->string(CPVentasModel::PLAN_FINANCIAMIENTO);
            $table->string(CPVentasModel::VENDEDOR_ASIGNADO);
            $table->unsignedInteger(CPVentasModel::ASIGNADO);
            $table->foreign(CPVentasModel::ASIGNADO)
                ->references(User::ID)
                ->on(User::getTableName());
            $table->string(CPVentasModel::EMAIL);
            $table->integer(CPVentasModel::INTENTOS)->default(0);
            $table->unsignedInteger(CPVentasModel::ESTATUS_ID)->nullable();
            $table->foreign(CPVentasModel::ESTATUS_ID)
                    ->references(CatStatusCPModel::ID)
                    ->on(CatStatusCPModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CPVentasModel::getTableName());
    }
}
