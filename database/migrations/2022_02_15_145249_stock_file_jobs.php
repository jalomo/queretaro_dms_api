<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class StockFileJobs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('stock_file_jobs', function (Blueprint $table){
            $table->bigIncrements('id')->unsignedInteger();
            $table->integer('usuario_id')->unsignedInteger()->nullable();
            $table->string('archivo');
            $table->string('nombre');
            $table->string('estatus')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('stock_file_jobs');
    }
}
