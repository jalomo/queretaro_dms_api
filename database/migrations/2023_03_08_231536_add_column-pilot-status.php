<?php

use App\Models\Logs\Pilot\LogsPilotModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnPilotStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(LogsPilotModel::getTableName(), function (Blueprint $table) {
            $table->string(LogsPilotModel::STATUS)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(LogsPilotModel::getTableName(), function (Blueprint $table) {
            $table->string(LogsPilotModel::STATUS);
        });
    }
}
