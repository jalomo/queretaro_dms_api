<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Refacciones\CotizadorRefaccionesModel;

class CreateTablaCotizadorRefacciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CotizadorRefaccionesModel::getTableName(), function (Blueprint $table) {
            $table->increments(CotizadorRefaccionesModel::ID);
            $table->string(CotizadorRefaccionesModel::NOMBRE_CONTACTO);
            $table->string(CotizadorRefaccionesModel::TELEFONO)->nullable();
            $table->string(CotizadorRefaccionesModel::MARCA)->nullable();
            $table->string(CotizadorRefaccionesModel::MODELO)->nullable();
            $table->integer(CotizadorRefaccionesModel::ANIO)->nullable();
            $table->string(CotizadorRefaccionesModel::NO_SERIE)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(CotizadorRefaccionesModel::getTableName());
    }
}
