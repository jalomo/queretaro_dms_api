<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'oasis/master/'], function () {
    Route::post('/get-all', 'Oasis\OasisController@getAll');
});
Route::resource('oasis', 'Oasis\OasisController');
Route::post('/guardar-mantenimiento', 'Oasis\MantenimientoUnidadesController@guardarMantenimiento');
Route::get('/historial-mantenimiento/{id}', 'Oasis\MantenimientoUnidadesController@historial');
Route::get('/historial-mantenimiento-id/{id}', 'Oasis\MantenimientoUnidadesController@historialId');
Route::post('/historial-mantenimientos', 'Oasis\MantenimientoUnidadesController@historialMantenimientosUnidad');

