<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('usuarios/login', 'Usuarios\UsuariosController@login');
Route::post('usuarios/registrar', 'Usuarios\UsuariosController@registro');

// Route::middleware('auth:api')->group(function () {

Route::group(['prefix' => 'usuarios'], function () {
    Route::post('/modulos', 'Usuarios\UsuariosController@storeUsuariosModulos');
    Route::get('/modulos-by-usuario', 'Usuarios\UsuariosController@usuariosModulos');
    Route::post('/logout', 'Usuarios\UsuariosController@logout');
    Route::put('/cambiar-password/{id}', 'Usuarios\UsuariosController@changePassword');
    Route::post('/usuario-by-rol', 'Usuarios\UsuariosController@getUsuariosByParametros');
});
Route::resource('usuarios', 'Usuarios\UsuariosController');

Route::group(['prefix' => 'menu'], function () {
    Route::get('/', 'Usuarios\Menu\MenuController@menu');
    Route::get('/modulos-userid/{user_id}', 'Usuarios\Menu\ModulosMenuController@getModulosByUserID');
    Route::get('/modulos', 'Usuarios\Menu\ModulosMenuController@index');
    Route::post('/modulos', 'Usuarios\Menu\ModulosMenuController@store');
    Route::get('/modulos/{id}', 'Usuarios\Menu\ModulosMenuController@show');
    Route::put('/modulos/{id}', 'Usuarios\Menu\ModulosMenuController@update');
    Route::delete('/modulos/{id}', 'Usuarios\Menu\ModulosMenuController@destroy');
});

Route::group(['prefix' => 'permisos'], function () {
    Route::get('/', 'Administrador\PermisosController@permisos');
    Route::post('/', 'Administrador\PermisosController@insertPermiso');
    Route::delete('/eliminar-permiso-rol', 'Administrador\PermisosController@deletePermiso');
});

Route::group(['prefix' => 'roles'], function () {
    Route::get('/', 'Usuarios\RolesController@index');
    Route::post('/', 'Usuarios\RolesController@store');
    Route::post('/modulos', 'Usuarios\RolesController@storerolesModulos');
    Route::get('/modulos-by-rol', 'Usuarios\RolesController@rolesModulos');
    Route::get('/{id}', 'Usuarios\RolesController@show');
    Route::put('/{id}', 'Usuarios\RolesController@update');
    Route::delete('/{id}', 'Usuarios\RolesController@destroy');
});

Route::post('/pdf', 'PdfGenerator@index');
Route::post('/upload-factura', 'Facturacion\Facturacion@uploadXmlFactura');
Route::get('/getfileview', 'Core\ManejoArchivosController@returnFileView');
Route::get('/getimg', 'Core\ManejoArchivosController@returnFileImg');
Route::get('/downloadFile', 'Core\ManejoArchivosController@downloadFile');
Route::get('/file', 'Core\ManejoArchivosController@getFile');


Route::group(['prefix' => 'facturas'], function () {
    Route::post('/upload', 'Facturacion\Facturacion@uploadXmlFactura');
    Route::get('/', 'Facturacion\Facturacion@index');
    Route::get('/{id}', 'Facturacion\Facturacion@getByIdFactura');
    Route::get('/descargar/{string}', 'Facturacion\Facturacion@downloadFactura');
});


Route::group(['prefix' => 'productos'], function () {
    Route::get('/', 'Refacciones\ProductosController@getAllProductos');
    Route::get('/listado', 'Refacciones\ProductosController@listado');
    Route::get('/stockActual', 'Refacciones\ProductosController@getAllStockProducto');
    Route::get('/stockByProductoId/{producto_id}', 'Refacciones\ProductosController@getStockByProductoId');
    Route::get('/stockByProductoNoParteAlmacenSecundario/{producto_id}', 'Refacciones\ProductosController@stockByProductoNoParteAlmacenSecundario');
    Route::get('/listadoStock', 'Refacciones\ProductosController@listadoStockProductos');
    Route::get('/listadoStockAlmacenSecundario', 'Refacciones\ProductosController@listadoStockProductosAlmacenSecundario');
    Route::get('/refaccion', 'Productos\ProductosController@show');
    Route::get('/{id}', 'Refacciones\ProductosController@getProductoById');
    // Route::post('/', 'Refacciones\ProductosController@storeproductos');
    Route::post('/upload-factura-productos', 'Refacciones\ProductosController@uploadFacturaProductos');
    // Route::put('/{id}', 'Refacciones\ProductosController@update');
    Route::put('/ubicacion/{id}', 'Refacciones\ProductosController@updateProductoUbicacion');
    // Route::delete('/{id}', 'Refacciones\ProductosController@destroy');
    Route::post('/buscar-piezas', 'Refacciones\ProductosController@getByNumeroPiezaDescripcion');

    // Route::post('/descontar-producto', 'Refacciones\ProductosController@descontarProductos');
    // Route::post('/aumentar-producto', 'Refacciones\ProductosController@aumentarProductos');
    Route::post('/validar-descuento-producto', 'Refacciones\ProductosController@validarCantidadProducto');
    Route::post('/actualiza-precios', 'Refacciones\ProductosController@updateProductsPrecio');
});

Route::resource('productos', 'Refacciones\ProductosController');

Route::group(['prefix' => 'almacen'], function () {
    Route::get('/', 'Refacciones\AlmacenesController@index');
    Route::post('/', 'Refacciones\AlmacenesController@store');
    Route::get('/{id}', 'Refacciones\AlmacenesController@show');
    Route::put('/{id}', 'Refacciones\AlmacenesController@update');
    Route::delete('/{id}', 'Refacciones\AlmacenesController@destroy');
});

Route::group(['prefix' => 'menu-secciones'], function () {
    Route::get('/', 'Usuarios\Menu\MenuSeccionesController@index');
    Route::get('/catalogo', 'Usuarios\Menu\MenuSeccionesController@getCatalogo');
    Route::get('/permisos', 'Usuarios\Menu\MenuSeccionesController@getSeccionesByModulo');
    Route::get('/byrol', 'Usuarios\Menu\MenuSeccionesController@getSeccionesByRol');
    Route::post('/store-secciones-rol', 'Usuarios\Menu\MenuSeccionesController@storeSeccionesByRole');
    Route::post('/', 'Usuarios\Menu\MenuSeccionesController@store');
    Route::get('/{id}', 'Usuarios\Menu\MenuSeccionesController@show');
    Route::put('/{id}', 'Usuarios\Menu\MenuSeccionesController@update');
    Route::delete('/{id}', 'Usuarios\Menu\MenuSeccionesController@destroy');
});

Route::group(['prefix' => 'menu-submenu'], function () {
    Route::get('/', 'Usuarios\Menu\MenuSubmenuController@index');
    Route::get('/catalogo', 'Usuarios\Menu\MenuSubmenuController@getCatalogo');
    Route::get('/submenu', 'Usuarios\Menu\MenuSubmenuController@getsubmenusBySeccion');
    Route::post('/', 'Usuarios\Menu\MenuSubmenuController@store');
    Route::get('/byrol', 'Usuarios\Menu\MenuSubmenuController@getSubmenuByRol');
    Route::post('/store-submenu-rol', 'Usuarios\Menu\MenuSubmenuController@storeSubmenuByRole');
    Route::get('/{id}', 'Usuarios\Menu\MenuSubmenuController@show');
    Route::put('/{id}', 'Usuarios\Menu\MenuSubmenuController@update');
    Route::delete('/{id}', 'Usuarios\Menu\MenuSubmenuController@destroy');
});

Route::group(['prefix' => 'menu-vistas'], function () {
    Route::get('/', 'Usuarios\Menu\MenuVistasController@index');
    Route::get('/vistas', 'Usuarios\Menu\MenuVistasController@getVistasBySubmenu');
    Route::post('/store-by-roles', 'Usuarios\Menu\MenuVistasController@saveVistasRoles');
    Route::get('/byrol', 'Usuarios\Menu\MenuVistasController@getVistaByRol');
    Route::get('/get-by-usuario', 'Usuarios\Menu\MenuVistasController@getUsuariosVistasByUserId');
    Route::post('/store-by-usuarios', 'Usuarios\Menu\MenuVistasController@saveVistasUsuarios');
    Route::post('/', 'Usuarios\Menu\MenuVistasController@store');
    Route::get('/{id}', 'Usuarios\Menu\MenuVistasController@show');
    Route::put('/{id}', 'Usuarios\Menu\MenuVistasController@update');
    Route::delete('/{id}', 'Usuarios\Menu\MenuVistasController@destroy');
});

Route::group(['prefix' => 'requisiciones'], function () {
    Route::get('/', 'Refacciones\RequisicionesController@index');
    Route::post('/', 'Refacciones\RequisicionesController@store');
    Route::get('/{id}', 'Refacciones\RequisicionesController@getDetalleRequisicion');
    Route::put('/{id}', 'Refacciones\RequisicionesController@update');
    Route::delete('/{id}', 'Refacciones\RequisicionesController@destroy');
});


Route::group(['prefix' => 'clientes'], function () {
    Route::get('/busqueda', 'Refacciones\ClientesController@getClientes');
    Route::get('/ultimo-registro', 'Refacciones\ClientesController@getLastRecord');
    Route::get('/tipo-clave', 'Refacciones\ClientesController@getClienteByclave');
    Route::get('/informacion-cliente', 'Refacciones\ClientesController@getInformacionCliente');
    Route::get('/select-catalogo', 'Refacciones\ClientesController@selectCatalogo');
    Route::post('/tiene-credito', 'Refacciones\ClientesController@tieneCredito');
    Route::post('/buscar-cliente', 'Refacciones\ClientesController@searchNombreCliente');
    Route::post('/buscar-existencia', 'Refacciones\ClientesController@searchCliente');
    Route::post('/numero-cliente', 'Refacciones\ClientesController@searchNumeroCliente');
    Route::put('/{id}', 'Refacciones\ClientesController@update');
    Route::put('/actualizaCredito/{id}', 'Refacciones\ClientesController@actualizarAplicaCredito');
    Route::put('/actualizaPlazoCredito/{id}', 'Refacciones\ClientesController@actualizarPlazoCredito');
    Route::put('/actualizar-datos-cliente/{id}', 'Refacciones\ClientesController@updateDatosCliente');
    Route::post('/crear-regresar', 'Refacciones\ClientesController@crearRegresarCliente');
    Route::post('/crear-cliente-modal', 'Refacciones\ClientesController@createClienteModal');
    Route::put('/update-cliente-modal/{id}', 'Refacciones\ClientesController@updateClienteModal');
    Route::post('/buscar-rfc', 'Refacciones\ClientesController@getByRFC');
    Route::get('/datos-credito', 'Refacciones\ClientesController@creditoByRFC');
});

Route::resource('clientes', 'Refacciones\ClientesController');

Route::group(['prefix' => 'contactos'], function () {
    Route::get('/', 'Refacciones\ContactoController@index');
    Route::get('lista-cliente/{id}', 'Refacciones\ContactoController@getContactosByClienteId');
    Route::post('/', 'Refacciones\ContactoController@store');
    Route::get('/{id}', 'Refacciones\ContactoController@show');
    Route::put('/{id}', 'Refacciones\ContactoController@update');
    Route::delete('/{id}', 'Refacciones\ContactoController@destroy');
});

Route::group(['prefix' => 'vehiculos-clientes'], function () {
    Route::get('/', 'Refacciones\VehiculosClientesController@index');
    Route::get('lista-cliente/{id}', 'Refacciones\VehiculosClientesController@getVehiculosByClienteId');
    Route::post('/', 'Refacciones\VehiculosClientesController@store');
    Route::get('/{id}', 'Refacciones\VehiculosClientesController@show');
    Route::put('/{id}', 'Refacciones\VehiculosClientesController@update');
    Route::delete('/{id}', 'Refacciones\VehiculosClientesController@destroy');
    Route::post('/buscar-serie', 'Refacciones\VehiculosClientesController@searchSerieCliente');
});

Route::group(['prefix' => 'cfdi'], function () {
    Route::get('/', 'Refacciones\CfdiController@index');
    Route::post('/', 'Refacciones\CfdiController@store');
    Route::get('/{id}', 'Refacciones\CfdiController@show');
    Route::put('/{id}', 'Refacciones\CfdiController@update');
    Route::delete('/{id}', 'Refacciones\CfdiController@destroy');
});

Route::group(['prefix' => 'precios'], function () {
    Route::get('/', 'Refacciones\PreciosController@filtrarPrecios');
    Route::post('/', 'Refacciones\PreciosController@store');
    Route::get('/{id}', 'Refacciones\PreciosController@show');
    Route::put('/{id}', 'Refacciones\PreciosController@update');
    Route::delete('/{id}', 'Refacciones\PreciosController@destroy');
});

Route::group(['prefix' => 'talleres'], function () {
    Route::get('/', 'Refacciones\TallerController@index');
    Route::post('/', 'Refacciones\TallerController@store');
    Route::get('/{id}', 'Refacciones\TallerController@show');
    Route::put('/{id}', 'Refacciones\TallerController@update');
    Route::delete('/{id}', 'Refacciones\TallerController@destroy');
});

Route::group(['prefix' => 'vendedor'], function () {
    Route::get('/', 'Refacciones\VendedorController@index');
    Route::post('/', 'Refacciones\VendedorController@store');
    Route::get('/{id}', 'Refacciones\VendedorController@show');
    Route::put('/{id}', 'Refacciones\VendedorController@update');
    Route::delete('/{id}', 'Refacciones\VendedorController@destroy');
});

Route::group(['prefix' => 'folio'], function () {
    Route::get('/', 'Refacciones\FoliosController@index');
    Route::get('/get-by-folio/{folio}', 'Refacciones\FoliosController@getByFolio');
    Route::post('/generar-folio', 'Refacciones\FoliosController@generarFolio');
    Route::get('/muestra-folio', 'Refacciones\FoliosController@getFolioByString');
    Route::get('/{id}', 'Refacciones\FoliosController@show');
    Route::post('/', 'Refacciones\FoliosController@store');
});


Route::group(['prefix' => 'venta-producto'], function () {
    Route::get('/', 'Refacciones\VentaProductoController@index');
    Route::get('/detalle-venta', 'Refacciones\VentaProductoController@detalleVenta');
    // Route::get('/detalle-venta-by-folio/{folio}', 'Refacciones\VentaProductoController@getDetalleVentaByFolio');
    Route::get('/detalle-by-id/{id}', 'Refacciones\VentaProductoController@getDetalleVentaId');
    Route::get('/total-venta-by-folio/{folio}', 'Refacciones\VentaProductoController@totalVentaByFolio');
    Route::get('/{id}', 'Refacciones\VentaProductoController@show');
    Route::post('/', 'Refacciones\VentaProductoController@store');
    Route::post('/detalle', 'Refacciones\VentaProductoController@storeVentaDetalle');
    Route::post('/validarprecioapartado', 'Refacciones\VentaProductoController@validaCambioPreciosApartado');
    Route::post('/autorizar', 'Refacciones\VentaProductoController@autorizarVenta');
    Route::delete('/{id}', 'Refacciones\VentaProductoController@destroy');
    Route::post('/remove-mpm-item/{id}', 'Refacciones\VentaProductoController@deleteMpmitem');
    Route::patch('/{id}/actualizar', 'Refacciones\VentaProductoController@updateVenta');
    Route::patch('/{id}/actualizar-presupuesto', 'Refacciones\VentaProductoController@updateVentaPresupuesto');
    Route::delete('/{refaccion}/eliminar-presupuesto', 'Refacciones\VentaProductoController@deleteRefaccionPresupuesto')->middleware('auth:api');
});

Route::group(['prefix' => 'operaciones-piezas'], function () {
    Route::post('/verificar-total', 'Refacciones\OperacionesPiezasController@verificartotales');
});
Route::resource('operaciones-piezas', 'Refacciones\OperacionesPiezasController');

Route::group(['prefix' => 'ventas'], function () {
    Route::post('/generar-archivo-ford', 'Refacciones\VentasRealizadasController@getVentas6MesesAtras');
    Route::post('/apartar', 'Refacciones\VentasRealizadasController@apartarVenta');
    Route::get('/listado-ordenes', 'Refacciones\VentasRealizadasController@getOrdenesAbiertas');
    Route::get('/taller/numero-orden/{numero_orden}', 'Refacciones\VentasRealizadasController@getRefaccionesVentanillaTaller');
    Route::get('/taller/orden/{numero_orden}', 'Refacciones\VentasRealizadasController@getRefaccionesDeTaller');
    Route::get('/taller/info-vehiculo/{numero_orden}', 'Refacciones\VentasRealizadasController@curlGetVehiculoInfo');
    Route::get('/', 'Refacciones\VentasRealizadasController@index');
    Route::get('/venta-by-folio/{folio}', 'Refacciones\VentasRealizadasController@ventasByFolioId');
    Route::get('/busqueda-ventas', 'Refacciones\VentasRealizadasController@getBusquedaVentas');
    Route::get('/reporte-ventas', 'Refacciones\VentasRealizadasController@downloadReporteVentas');
    Route::get('/busqueda-all-ventas', 'Refacciones\VentasRealizadasController@getAllVentas');
    Route::get('/busqueda-devoluciones', 'Refacciones\VentasRealizadasController@getBusquedaVentasDevoluciones');
    Route::get('/ventas-agrupadas-producto', 'Refacciones\VentasRealizadasController@getVentasPorMesByProducto');
    Route::post('/', 'Refacciones\VentasRealizadasController@storeVenta');
    Route::post('/generar-venta', 'Refacciones\VentasRealizadasController@createVenta');
    Route::post('/mpm', 'Refacciones\VentasRealizadasController@ventaMpm');
    Route::post('/mpm-finalizar', 'Refacciones\VentasRealizadasController@FinalizarVentaServicio');
    Route::post('/detalle-operacion', 'Refacciones\VentasRealizadasController@detalleOperacion');
    Route::put('/finalizar/{id}', 'Refacciones\VentasRealizadasController@finalizarVenta');
    Route::put('/mpmdescontar/{id}', 'Refacciones\VentasRealizadasController@descontarVentaMpm');
    Route::put('/editar-venta/{id}', 'Refacciones\VentasRealizadasController@editarVenta');
    Route::get('/detalle-mpm/{id}', 'Refacciones\VentasRealizadasController@detalleVentampmById');
    Route::get('/detalle-mpm/{numero_orden}/numero-orden', 'Refacciones\VentasRealizadasController@detalleVentaByNumeroOrden');
    Route::post('/contabilidad-numero-orden/{numero_orden}', 'Refacciones\VentasRealizadasController@contabilidad_numero_orden');
    Route::get('/contabilidad-numero-orden/{numero_orden}/presupuestos', 'Refacciones\VentasRealizadasController@contabilidad_numero_orden_presupuestos');
    Route::get('/contabilidad-numero-orden/{numero_orden}/refaccion/{num_pieza}', 'Refacciones\VentasRealizadasController@contabilidad_numero_orden_y_refaccion');
    Route::get('/total/{id}', 'Refacciones\VentasRealizadasController@totalventa');
    Route::get('/{id}', 'Refacciones\VentasRealizadasController@show');
    Route::get('/porusuario/{id}', 'Refacciones\VentasRealizadasController@ventasByUser');
    Route::get('/busqueda-venta/{id}', 'Refacciones\VentasRealizadasController@showByVentaID');
    Route::get('/venta-totales/{id}', 'Refacciones\VentasRealizadasController@getTotales');
    Route::put('/editar-solo-venta/{id}', 'Refacciones\VentasRealizadasController@updateOnlyVenta');
    Route::put('/update-tipo-venta/{id}', 'Refacciones\VentasRealizadasController@updateTipoVenta');
    Route::get('/venta-cotizacion-por-cliente/{cliente_id}', 'Refacciones\VentasRealizadasController@getByVentaCotizadorPorClienteId');
    Route::post('/numero_orden/{id}/folio', 'Refacciones\VentasRealizadasController@getFolio');
    Route::post('/completar/{venta}', 'Refacciones\VentasRealizadasController@completar');
    Route::get('/total-servicios-refacciones/{numero_orden}', 'Refacciones\VentasRealizadasController@getTotalesServiciosRefacciones');
    Route::post('/actualizar/{venta}/refacciones-operaciones', 'Refacciones\VentasRealizadasController@actualizar_refacciones_operaciones');
    Route::post('/actualizar/{numero_orden}/refacciones-operaciones-por-orden', 'Refacciones\VentasRealizadasController@actualizar_refacciones_operaciones_orden');
    Route::post('/{venta}/abrir-orden-completa', 'Refacciones\VentasRealizadasController@reabrir_orden_completa');
    Route::get('/{numero_orden}/resumen-detalles', 'Refacciones\VentasRealizadasController@resumen_detalles');
    Route::post('/{numero_orden}/habilitar-firma-requisicion', 'Refacciones\VentasRealizadasController@habilitar_firma_requisicion');
    Route::post('/consulta-vendedor', 'Refacciones\VentasRealizadasController@getVendedorPorFolio');
});

Route::group(['prefix' => 'producto-traspaso-almacen'], function () {
    Route::get('/', 'Refacciones\TraspasoProductoController@index');
    Route::get('/getby-idproducto/{id}', 'Refacciones\TraspasoProductoController@getbyidproducto');
    Route::post('/', 'Refacciones\TraspasoProductoController@store');
    Route::post('/confirmar-traspaso', 'Refacciones\TraspasoProductoController@confirmarTraspaso');
    Route::put('/{id}', 'Refacciones\TraspasoProductoController@update');
    Route::get('/get-by-id-traspaso/{id}', 'Refacciones\TraspasoProductoController@getByIdTraspaso');
    Route::get('/get-detalle-traspaso-by-id/{id}', 'Refacciones\TraspasoProductoController@getDetalleTraspasoAlmacen');
    Route::delete('/{id}', 'Refacciones\TraspasoProductoController@destroy');
});

Route::group(['prefix' => 'producto-almacen'], function () {
    Route::get('/', 'Refacciones\ProductoAlmacenController@index');
    Route::get('/{id}', 'Refacciones\ProductoAlmacenController@show');
    Route::post('/', 'Refacciones\ProductoAlmacenController@store');
    Route::put('/{id}', 'Refacciones\ProductoAlmacenController@update');
    Route::post('/get-cantidad-almacen-producto', 'Refacciones\ProductoAlmacenController@getCantidadAlmacenProducto');

    Route::post('/descontar', 'Refacciones\ProductoAlmacenController@descontar');
});

// Route::group(['prefix' => 'traspasos'], function () {
//     Route::get('/poliza/venta-total/{id}', 'Refacciones\TraspasoProductoController@polizaVentaTotalByTraspasoId');
//     Route::get('/reporte/{traspaso_id}', 'Refacciones\TraspasosController@reportetraspaso');
// });

// Route::resource('traspasos', 'Refacciones\TraspasosController');

Route::group(['prefix' => 'producto-remplazo-almacen'], function () {
    Route::get('/', 'Refacciones\RemplazoProductoController@index');
    Route::post('/', 'Refacciones\RemplazoProductoController@actualizarProductoAlmacen');
    Route::put('/{id}', 'Refacciones\RemplazoProductoController@update');
    Route::get('/getby-idproducto/{id}', 'Refacciones\TraspasoProductoController@getbyidproducto');
});

Route::group(['prefix' => 'catalogo-proveedor'], function () {
    Route::get('/buscar-rfc', 'Refacciones\ProveedorRefaccionesController@getProveedorByRFC');
});
Route::resource('catalogo-proveedor', 'Refacciones\ProveedorRefaccionesController');

Route::group(['prefix' => 'catalogo-tipo-cliente'], function () {
    Route::get('/', 'Refacciones\TipoClienteController@index');
    Route::get('/{id}', 'Refacciones\TipoClienteController@show');
    Route::post('/', 'Refacciones\TipoClienteController@store');
    Route::put('/{id}', 'Refacciones\TipoClienteController@update');
    Route::delete('/{id}', 'Refacciones\TipoClienteController@destroy');
});

Route::group(['prefix' => 'devoluciones'], function () {
    Route::get('/', 'Refacciones\DevolucionProveedorController@index');
    Route::get('/{id}', 'Refacciones\DevolucionProveedorController@show');
    Route::get('/get-by-compra_id/{id}', 'Refacciones\DevolucionProveedorController@showByOrdenCompraId');
    Route::post('/', 'Refacciones\DevolucionProveedorController@store');
    Route::put('/{id}', 'Refacciones\DevolucionProveedorController@update');
    Route::delete('/{id}', 'Refacciones\DevolucionProveedorController@destroy');
});

Route::group(['prefix' => 'devolucion-venta'], function () {
    Route::get('/', 'Refacciones\DevolucionVentasController@index');
    Route::get('/{id}', 'Refacciones\DevolucionVentasController@show');
    Route::get('/get-by-venta-id/{id}', 'Refacciones\DevolucionVentasController@showByVentaID');
    Route::post('/', 'Refacciones\DevolucionVentasController@storeDevolucion');
    Route::post('/servicio', 'Refacciones\DevolucionVentasController@devolucionByServicio');
    Route::put('/{id}', 'Refacciones\DevolucionVentasController@update');
    Route::delete('/{id}', 'Refacciones\DevolucionVentasController@destroy');
});


Route::group(['prefix' => 'orden-compra'], function () {
    Route::put('/update-inventario-por-orden/{orden_compra_id}', 'Refacciones\OrdenCompraController@actualizarInventarioByOrdenCompra');
    Route::get('/facturas-listado', 'Refacciones\OrdenCompraController@facturasListado');
    Route::get('/', 'Refacciones\OrdenCompraController@index');
    Route::post('/', 'Refacciones\OrdenCompraController@store');
    Route::put('/{id}', 'Refacciones\OrdenCompraController@update');
    Route::delete('/{id}', 'Refacciones\OrdenCompraController@destroy');
    Route::get('/lista-productos-carrito/{id}', 'Refacciones\OrdenCompraController@listadoProductosCarrito');
    Route::post('/factura', 'Refacciones\OrdenCompraController@facturaOrdenCompra');
    Route::post('/csv', 'Refacciones\OrdenCompraController@csvOrdenCompra');
    Route::post('/cancelar-factura', 'Refacciones\OrdenCompraController@cancelarFactura');
    Route::get('/busqueda-compras', 'Refacciones\OrdenCompraController@getBusquedaOrdenCompra');
    Route::get('/busqueda-devoluciones', 'Refacciones\OrdenCompraController@getBusquedaDevoluciones');
    Route::get('/{id}', 'Refacciones\OrdenCompraController@show');
});

Route::group(['prefix' => 'productos-orden-compra'], function () {
    Route::get('/', 'Refacciones\ListaProductosOrdenCompraController@index');
    Route::get('/{id}', 'Refacciones\ListaProductosOrdenCompraController@listadoProductosCarrito');
    Route::post('/', 'Refacciones\ListaProductosOrdenCompraController@store');
    Route::put('/{id}', 'Refacciones\ListaProductosOrdenCompraController@update');
    Route::delete('/{id}', 'Refacciones\ListaProductosOrdenCompraController@destroy');
    Route::get('/total-by-orden-compra/{id}', 'Refacciones\ListaProductosOrdenCompraController@totalByOrdenCompra');
});


Route::group(['prefix' => 'tipo-pago'], function () {
    Route::get('/', 'Refacciones\CatTipoPagoController@index');
    Route::get('/{id}', 'Refacciones\CatTipoPagoController@show');
    Route::post('/', 'Refacciones\CatTipoPagoController@store');
    Route::put('/{id}', 'Refacciones\CatTipoPagoController@update');
    Route::delete('/{id}', 'Refacciones\CatTipoPagoController@destroy');
});

Route::group(['prefix' => 're-estatus-compra'], function () {
    Route::get('/', 'Refacciones\ReOrdenCompraEstatus@index');
    Route::get('/{id}', 'Refacciones\ReOrdenCompraEstatus@show');
    Route::post('/', 'Refacciones\ReOrdenCompraEstatus@store');
    Route::put('/{id}', 'Refacciones\ReOrdenCompraEstatus@update');
    Route::delete('/{id}', 'Refacciones\ReOrdenCompraEstatus@destroy');
});

Route::group(['prefix' => 're-estatus-venta'], function () {
    Route::get('/', 'Refacciones\ReVentasEstatus@index');
    Route::get('/{id}', 'Refacciones\ReVentasEstatus@show');
    Route::post('/', 'Refacciones\ReVentasEstatus@store');
    Route::put('/{id}', 'Refacciones\ReVentasEstatus@update');
    Route::delete('/{id}', 'Refacciones\ReVentasEstatus@destroy');
});

// 
Route::group(['prefix' => 'catalogo-anio'], function () {
    Route::post('/buscar-id', 'Refacciones\CatalogoAnioController@buscar_id');
});
Route::resource('catalogo-anio', 'Refacciones\CatalogoAnioController');

Route::group(['prefix' => 'catalogo-colores'], function () {
    Route::post('/buscar-id', 'Refacciones\CatalogoColoresController@buscar_id');
});
Route::resource('catalogo-colores', 'Refacciones\CatalogoColoresController');

Route::group(['prefix' => 'catalogo-marcas'], function () {
    Route::post('/buscar-id', 'Refacciones\MarcasController@buscar_id');
    Route::get('/catalogo', 'Refacciones\MarcasController@getCatalogo');
});

Route::resource('catalogo-marcas', 'Refacciones\MarcasController');
Route::resource('catalogo-ubicacion', 'Refacciones\UbicacionController');

Route::group(['prefix' => 'catalogo-ubicacion-llaves'], function () {
    Route::get('/', 'Refacciones\UbicacionLLavesController@index');
    Route::get('/{id}', 'Refacciones\UbicacionLLavesController@show');
    Route::post('/', 'Refacciones\UbicacionLLavesController@store');
    Route::put('/{id}', 'Refacciones\UbicacionLLavesController@update');
    Route::delete('/{id}', 'Refacciones\UbicacionLLavesController@destroy');
});


// Route::group(['prefix' => 'recepcion-unidades'], function () {
//     Route::get('/ultimo-registro', 'Refacciones\RecepcionUnidadesController@getLastRecord');    
// });

// dataformulariorecepcion
// Route::resource('recepcion-unidades', 'Refacciones\RecepcionUnidadesController');

Route::resource('catalogo-estatus-ventas', 'Refacciones\CatalogoEstatusVentaController');
Route::resource('catalogo-estatus-compras', 'Refacciones\CatalogoEstatusCompraController');
Route::resource('catalogo-estatus-inventario', 'Refacciones\CatalogoEstatusInventarioController');

Route::group(['prefix' => 'desglose-producto'], function () {
    Route::get('/', 'Refacciones\DesgloseProductosController@index');
    Route::get('/actualizaStockByProducto', 'Refacciones\DesgloseProductosController@actualizaStockByProducto');
    Route::get('/getStockByProducto', 'Refacciones\DesgloseProductosController@getStockByProducto');
    Route::get('/{id}', 'Refacciones\DesgloseProductosController@show');
    Route::post('/', 'Refacciones\DesgloseProductosController@store');
    Route::put('/{id}', 'Refacciones\DesgloseProductosController@update');
    Route::delete('/{id}', 'Refacciones\DesgloseProductosController@destroy');
});

Route::group(['prefix' => 'devolucion-pieza'], function () {
    Route::get('/', 'Refacciones\DevolucionPiezaController@index');
    Route::get('/{id}', 'Refacciones\DevolucionPiezaController@show');
    Route::get('/por-orden-compra/{orden_compra_id}', 'Refacciones\DevolucionPiezaController@getByOrdenCompraId');
    Route::post('/', 'Refacciones\DevolucionPiezaController@store');
    Route::put('/{id}', 'Refacciones\DevolucionPiezaController@update');
    Route::delete('/{id}', 'Refacciones\DevolucionPiezaController@destroy');
});


Route::resource('catalogo-clave-cliente', 'Refacciones\CatalogoClaveClienteController');

Route::group(['prefix' => 'inventario-producto'], function () {
    Route::get('/productos', 'Refacciones\InventarioProductosController@inventarioProductos');
    Route::get('/buscar-productos', 'Refacciones\InventarioProductosController@getInventarioByProductoAttr');
    Route::get('/inventario-completo/{id_inventario}', 'Refacciones\InventarioProductosController@validarInventarioCompleto');
});
Route::resource('inventario-producto', 'Refacciones\InventarioProductosController');

Route::group(['prefix' => 'inventario'], function () {
    Route::get('/validaractivo', 'Refacciones\InventarioController@validarInventarioActivo');
    Route::get('/validarid/{id}', 'Refacciones\InventarioController@ultimoidinventario');
});
Route::group(['prefix' => 'masterpedidoproducto'], function () {
    Route::post('/filtrar', 'Refacciones\MaProductoPedidoController@filtrarPedidos');
});
Route::resource('masterpedidoproducto', 'Refacciones\MaProductoPedidoController');

Route::group(['prefix' => 'pedidoproducto'], function () {
    Route::get('/listado', 'Refacciones\ProductoPedidosController@getpedidoproductos');
});
Route::resource('pedidoproducto', 'Refacciones\ProductoPedidosController');
Route::resource('inventario', 'Refacciones\InventarioController');
Route::resource('tipopedido', 'Refacciones\CatTipoPedidoController');
Route::resource('enviopedido', 'Refacciones\EnvioPedidoDetalleController');

Route::group(['prefix' => 'clave-cliente'], function () {
    Route::get('/', 'Refacciones\ClaveClienteController@index');
    Route::get('/{id}', 'Refacciones\ClaveClienteController@show');
    Route::post('/', 'Refacciones\ClaveClienteController@store');
    Route::put('/{id}', 'Refacciones\ClaveClienteController@update');
    Route::delete('/{id}', 'Refacciones\ClaveClienteController@destroy');
    Route::get('/cliente-lista/{id}', 'Refacciones\ClaveClienteController@searchClienteId');
});

Route::resource('catalogo-ubicacion-producto', 'Refacciones\UbicacionProductoController');
Route::post('/notificaciones', 'Sistemas\NotificacionesController@setNotificacion');
Route::get('/notificaciones', 'Sistemas\NotificacionesController@getNotificaciones');
Route::get('/chat', 'Sistemas\NotificacionesController@getChat');

//Bodyshop
Route::resource('catalogo-proyectos-bodyshop', 'Bodyshop\CatProyectosController');

Route::get('stock', 'Refacciones\StockController@index');
Route::post('stock/csv', 'Refacciones\StockController@csv');
Route::post('stock/text', 'Refacciones\StockController@text');
Route::post('clientes/csv', 'Refacciones\StockController@clientes');

Route::get('orden/refacciones-facturadas', 'Orden\RefaccionesFacturadasController@index');

Route::group(['prefix' => 'cotizador-refacciones'], function () {
    Route::get('/buscar-por-venta', 'Refacciones\CotizadorRefaccionesController@getByIdVenta');
});
Route::resource('cotizador-refacciones', 'Refacciones\CotizadorRefaccionesController');

Route::group(['prefix' => 'detalle-cotizador-refacciones'], function () {
    Route::get('/buscar-por-cotizacion', 'Refacciones\DetalleCotizadorController@getByIdCotizacion');
});
Route::resource('detalle-cotizador-refacciones', 'Refacciones\DetalleCotizadorController');

Route::post('cajas/cliente-cuentas', 'Caja\ClienteCuentasController@store');

Route::get('refacciones/venta/{id}', 'Ventas\Refacciones\RefaccionesController@show');
Route::put('refacciones/venta', 'Ventas\Refacciones\VentaController@store');
Route::post('refacciones/venta/{id}/update', 'Ventas\Refacciones\VentaController@update');
Route::post('refacciones/venta/{id}/finalizar', 'Ventas\Refacciones\VentaController@finalizar');

Route::get('traspasos', 'Traspasos\TraspasoController@index');
Route::post('traspaso', 'Traspasos\TraspasoController@storeTraspaso');
Route::get('traspaso/refacciones-ventas', 'Traspasos\TraspasoController@refacciones_ventas');
Route::post('traspaso/solicitud', 'Traspasos\TraspasoController@solicitud');
Route::get('traspaso/{id}', 'Traspasos\TraspasoController@show');
Route::post('traspaso/{venta_id}/crear', 'Traspasos\TraspasoController@store');
Route::post('traspaso/{traspaso}/pedido', 'Traspasos\TraspasoController@send');
Route::post('traspaso/{traspaso}/finalizar', 'Traspasos\TraspasoController@finalizar');
Route::get('traspaso/{id}/refacciones', 'Traspasos\TraspasoController@refacciones');
Route::delete('traspaso/{traspaso}/cancelacion', 'Traspasos\TraspasoController@cancel');
Route::delete('traspaso/cancelacion-sucursal', 'Traspasos\TraspasoController@cancel_folio');
Route::post('traspaso/{traspaso}/envio', 'Traspasos\TraspasoController@enviado');
Route::post('traspaso/envio-sucursal', 'Traspasos\TraspasoController@enviado_folio');
Route::post('traspaso/{traspaso}/devolucion', 'Traspasos\TraspasoController@devolucion');
Route::post('traspaso/devolucion-sucursal', 'Traspasos\TraspasoController@devolucion_folio');
Route::post('traspaso/{traspaso}/devuelto', 'Traspasos\TraspasoController@devuelto');
Route::post('traspaso/devuelto-sucursal', 'Traspasos\TraspasoController@devuelto_folio');
Route::get('traspaso/{traspaso}/poliza', 'Traspasos\TraspasoController@poliza_traspaso');
Route::post('traspaso/{traspaso}/autorizar', 'Traspasos\TraspasoController@autorizar');
Route::post('traspaso/autorizado', 'Traspasos\TraspasoController@autorizado');
Route::post('traspaso/{traspaso}/subir-evidencia', 'Traspasos\TraspasoController@subir_evidencia');
Route::get('traspaso/{traspaso}/descargar-evidencia', 'Traspasos\TraspasoController@descargar_evidencia');
Route::post('traspaso/{id}/aumentar', 'Traspasos\TraspasoController@aumentar');
Route::post('traspaso/{id}/disminuir', 'Traspasos\TraspasoController@disminuir');
Route::post('traspaso/{id}/aumentar/entregado', 'Traspasos\TraspasoController@aumentar_entregado');
Route::post('traspaso/{id}/disminuir/entregado', 'Traspasos\TraspasoController@disminuir_entregado');
Route::post('traspaso/{id}/aumentar/devuelto', 'Traspasos\TraspasoController@aumentar_devuelto');
Route::post('traspaso/{id}/disminuir/devuelto', 'Traspasos\TraspasoController@disminuir_devuelto');
Route::post('traspaso/{id}/contabilidad', 'Traspasos\TraspasoController@contabilidad');

Route::group(['prefix' => 'cartera-cliente'], function () {
    Route::get('/buscar-por-cliente', 'Contabilidad\CarteraClienteController@getByClienteId');
});
Route::resource('cartera-cliente', 'Contabilidad\CarteraClienteController');

Route::group(['prefix' => 'comentario-proactivo'], function () {
    Route::get('/buscar-por-cotizador', 'Refacciones\ComentariosProactivoController@getByCotizadorRefaccionesID');
});
Route::resource('comentario-proactivo', 'Refacciones\ComentariosProactivoController');


Route::get('pedidos', 'Pedidos\PedidosController@index');
Route::post('pedido', 'Pedidos\PedidosController@store');
Route::post('pedido/cargamento', 'Pedidos\PedidosController@cargamento');
Route::get('pedido/{pedido}', 'Pedidos\PedidosController@productos');
Route::get('pedido/{pedido}/cargamento', 'Pedidos\PedidosController@productos_cargamento');
Route::post('pedido/{pedido}/editar', 'Pedidos\PedidosController@update');
Route::delete('pedido/{pedido}/eliminar', 'Pedidos\PedidosController@destroy');
Route::post('pedido/{pedido}/autorizar', 'Pedidos\PedidosController@autorizar');
Route::post('pedido/cargar-pedido/csv', 'Pedidos\PedidosController@csv');
Route::post('pedido/cargar-pedido/xml', 'Pedidos\PedidosController@xml');
Route::get('pedido/{pedido}/csv', 'Pedidos\PedidosController@descargar_csv');
Route::get('pedido/{venta}/ventas', 'Pedidos\PedidosController@ventas');
Route::post('pedido/del-dia', 'Pedidos\PedidosController@pedido_del_dia');
Route::get('pedido/{pedido}/cargamento/pdf', 'Pedidos\PedidosController@descargar_pdf_cargamento');
Route::get('pedido/{pedido}/cargamento/contabilidad', 'Pedidos\PedidosController@productos_cargamento_contabilidad');
Route::post('pedido/{pedido}/producto', 'Pedidos\PedidosController@cambiar_producto');
Route::post('pedido/{pedido}/subir-factura', 'Pedidos\PedidosController@subir_factura');
Route::post('pedido/{producto}/devolver', 'Pedidos\PedidosController@devolver_refaccion_pedido');
Route::get('pedido/{numero_folio}/factura', 'Pedidos\PedidosController@getFacturaByFolio');

Route::post('inventario/{numero_orden}/descontar', 'Inventario\InventarioController@descontar_inventario_venta');


Route::get('estadisticas/ventas', 'Estadisticas\VentasController@index');
Route::get('estadisticas/ventas/descargar', 'Estadisticas\VentasController@download');
Route::get('estadisticas/pedidos/cargamento', 'Estadisticas\PedidosController@index');
Route::get('estadisticas/pedidos/cargamento/descargar', 'Estadisticas\PedidosController@download');
Route::get('estadisticas/devoluciones/ventas', 'Estadisticas\DevolucionesController@index');
Route::get('estadisticas/devoluciones/ventas/descargar', 'Estadisticas\DevolucionesController@download');
Route::get('estadisticas/devoluciones/ventas', 'Estadisticas\DevolucionesController@index');
Route::get('estadisticas/traspasos/descargar', 'Estadisticas\TraspasoController@download');
Route::get('estadisticas/traspasos', 'Estadisticas\TraspasoController@index');
Route::get('estadisticas/ereact', 'Estadisticas\EreactController@index');
Route::get('estadisticas/ereact/descargar', 'Estadisticas\EreactController@download');

Route::get('kardex/productos', 'Kardex\KardexController@productos');
Route::get('kardex/detalle/{producto}', 'Kardex\KardexController@detalles_producto');

Route::post('envia-correo', 'CuentasPorCobrar\CuentasPorCobrarController@envioCorreo');
Route::resource('sucursales', 'Refacciones\SucursalesController');

Route::get('reemplazos', 'Reemplazo\ReemplazoController@index');
Route::post('reemplazo', 'Reemplazo\ReemplazoController@store');
Route::delete('reemplazo/{reemplazo}/delete', 'Reemplazo\ReemplazoController@destroy');

Route::group(['prefix' => 'ereact'], function () {
    Route::get('/ventas', 'Refacciones\VentaProductoController@getEreactVentas');
    Route::get('/inventario', 'Estadisticas\InventarioController@index');
    Route::get('/inventario/download', 'Estadisticas\InventarioController@download');
    Route::get('/compras', 'Estadisticas\ComprasController@index');
    Route::get('/compras/download', 'Estadisticas\ComprasController@download');
    Route::get('/genera-ereact-ventas', 'Refacciones\VentaProductoController@generateEreactVentasFile');
});


Route::group(['prefix' => 'refacciones'], function(){
    Route::get('list', 'Ventas\Detalles\DetallesController@index');
    Route::post('save', 'Ventas\Detalles\DetallesController@store');
    Route::post('update', 'Ventas\Detalles\DetallesController@update');
    Route::delete('destroy', 'Ventas\Detalles\DetallesController@destroy');
});


Route::get('auditorias', 'Auditoria\AuditoriaController@index');
Route::post('auditoria', 'Auditoria\AuditoriaController@store');
Route::get('auditoria/refacciones', 'Auditoria\RefaccionesController@index');
Route::get('auditoria/activa', 'Auditoria\AuditoriaController@activa');
Route::get('auditoria/{auditoria}', 'Auditoria\AuditoriaController@show');
Route::post('auditoria/{auditoria}', 'Auditoria\AuditoriaController@update');
Route::delete('auditoria/{auditoria}', 'Auditoria\AuditoriaController@destroy');
Route::post('auditoria/{auditoria}/completar', 'Auditoria\AuditoriaController@completar');
Route::post('auditoria/{auditoria}/cancelar', 'Auditoria\AuditoriaController@cancelar');
Route::post('auditoria/{auditoria}/aplicar-ajustes', 'Auditoria\AuditoriaController@aplicar_ajustes');

Route::post('auditoria/refaccion/{refaccion}/update', 'Auditoria\RefaccionesController@update');
Route::delete('auditoria/refaccion/{refaccion}/delete', 'Auditoria\RefaccionesController@destroy');
Route::post('auditoria/{auditoria}/cargar-refacciones', 'Auditoria\RefaccionesController@cargar_refacciones');
Route::post('auditoria/{auditoria}/refaccion', 'Auditoria\RefaccionesController@store');

// });


Route::group(['prefix' => 'detalles', 'middleware' => 'auth:api'] , function(){
    Route::post('venta/{numero_orden}', 'Detalles\VentaDetallesController@store');
    Route::post('venta/{venta}/completar', 'Detalles\VentaDetallesController@completar');
    Route::post('venta/{venta}/reabrir', 'Detalles\VentaDetallesController@reabrir_orden_completada');
    Route::post('venta/{numero_orden}/actualizar-operaciones', 'Detalles\VentaDetallesController@updateOperaciones');
    Route::get('recuperar-operaciones/{numero_orden}', 'Detalles\VentaDetallesController@updateOperaciones');
    Route::get('presupuesto/{numero_orden}', 'Detalles\VentaDetallesController@consultar_presupuesto');


    Route::get('refacciones', 'Detalles\RefaccionesController@index');
    Route::post('refaccion', 'Detalles\RefaccionesController@store');
    Route::patch('refaccion/{refaccion}', 'Detalles\RefaccionesController@update');
    Route::delete('refaccion/{refaccion}', 'Detalles\RefaccionesController@delete');
    
    Route::post('servicio', 'Detalles\ServiciosController@store');
    Route::patch('servicio/{servicio}', 'Detalles\ServiciosController@update');
    Route::delete('servicio/{servicio}', 'Detalles\ServiciosController@delete');

    Route::get('{numero_orden}', 'Detalles\VentaDetallesController@index');
});

Route::get('detalles/{numero_orden}/servicios', 'Detalles\VentaDetallesController@index');
Route::patch('detalles/refaccion/{refaccion}/entregar', 'Detalles\RefaccionesController@entregar');
Route::post('detalles/venta/{numero_orden}/actualizar', 'Detalles\VentaDetallesController@updateOperaciones');
Route::post('detalles/venta-desde-servicios/{numero_orden}', 'Detalles\VentaDetallesController@storeFromServicios');
Route::get('detalles/refacciones-por-firmar/{numero_orden}', 'Detalles\VentaDetallesController@refacciones_por_firmar');
Route::get('detalles/venta/{numero_orden}/completa', 'Detalles\VentaDetallesController@orden_completa');
Route::get('detalles/venta/{numero_orden}/venta-folio-contabilidad', 'Detalles\VentaDetallesController@venta_folio_contabilidad');

Route::get('refaccion-contabilidad', 'Detalles\RefaccionesController@refaccion_contabilidad');
Route::resource('regimen-fiscal', 'Refacciones\CaRegimenFiscalController');

Route::group(['prefix' => 'regimen-fiscal'], function () {
    Route::get('/', 'Refacciones\CaRegimenFiscalController@getBusqueda');
});