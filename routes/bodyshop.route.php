<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'bodyshop/master/'], function () {
    Route::post('/get-all', 'Bodyshop\BodyshopController@getByParametros');
    Route::post('/get-horarios', 'Bodyshop\BodyshopController@getHorarios');
    Route::post('/horarios-ocupados', 'Bodyshop\BodyshopController@horariosOCupados');
    Route::post('/existe-reservacion', 'Bodyshop\BodyshopController@existeReservacion');
});
Route::resource('bodyshop', 'Bodyshop\BodyshopController');