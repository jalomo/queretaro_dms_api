<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| MODULO AUTOS Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'catalogo-tipo-forma-pago'], function () {
    Route::get('/', 'CuentasPorCobrar\CatTipoFormaPagoController@index');
    Route::get('/{id}', 'CuentasPorCobrar\CatTipoFormaPagoController@show');
    Route::post('/', 'CuentasPorCobrar\CatTipoFormaPagoController@store');
    Route::put('/{id}', 'CuentasPorCobrar\CatTipoFormaPagoController@update');
    Route::delete('/{id}', 'CuentasPorCobrar\CatTipoFormaPagoController@destroy');
});

Route::group(['prefix' => 'cuentas-por-cobrar'], function () {

    Route::get('/', 'CuentasPorCobrar\CuentasPorCobrarController@listado');
    Route::get('/getkardexpagos', 'CuentasPorCobrar\CuentasPorCobrarController@getKardexPagos');
    Route::get('/getCierreCaja', 'CuentasPorCobrar\CuentasPorCobrarController@getCierreCaja');
    Route::get('/verifica-pagos-morosos', 'CuentasPorCobrar\CuentasPorCobrarController@getVerificaCuentasMorosas');
    Route::get('/aviso-pago', 'CuentasPorCobrar\CuentasPorCobrarController@envioAvisoPago');
    Route::get('/comprobante-fiscal', 'CuentasPorCobrar\CuentasPorCobrarController@getComprobanteFiscal');
    Route::get('/cancela-factura', 'CuentasPorCobrar\CuentasPorCobrarController@cancelarFactura');
    Route::post('/descargar-complemento', 'CuentasPorCobrar\CuentasPorCobrarController@getDescargarComplemento');
    Route::get('/{id}', 'CuentasPorCobrar\CuentasPorCobrarController@show');
    Route::get('buscar-por-folio-id/{folio_id}', 'CuentasPorCobrar\CuentasPorCobrarController@showByFolioId');
    Route::post('muestra-folio', 'CuentasPorCobrar\CuentasPorCobrarController@showByFolioIdNumeroOrden');
    Route::get('buscar-numero-orden/{numero_orden}', 'CuentasPorCobrar\CuentasPorCobrarController@showByNumeroOrden');
    Route::get('verifica-numero-orden/{numero_orden}', 'CuentasPorCobrar\CuentasPorCobrarController@getByNumeroOrden');
    // Route::get('/cuentas-proceso-cliente', 'CuentasPorCobrar\CuentasPorCobrarController@getCuentasPorCliente');
    Route::post('/', 'CuentasPorCobrar\CuentasPorCobrarController@store');
    Route::post('/registrar-servicio', 'CuentasPorCobrar\CuentasPorCobrarController@registrarCxcServicio');
    Route::post('/aplicar-factura', 'CuentasPorCobrar\CuentasPorCobrarController@aplicarFactura');
    Route::post('/aplicar-factura-credito', 'CuentasPorCobrar\CuentasPorCobrarController@aplicarFacturaCredito');
    Route::post('/set-complemento-cxc', 'CuentasPorCobrar\CuentasPorCobrarController@setCxcComplemento');
    Route::put('/{id}', 'CuentasPorCobrar\CuentasPorCobrarController@update');
    Route::put('/updatestatus/{id}', 'CuentasPorCobrar\CuentasPorCobrarController@updatestatus');
    Route::put('/actualizar-cliente/{id}', 'CuentasPorCobrar\CuentasPorCobrarController@updateClienteByCuentaPorCobrar');
    Route::put('/cancelar-cuenta/{id}', 'CuentasPorCobrar\CuentasPorCobrarController@cancelarCuentaById');
    Route::delete('/{id}', 'CuentasPorCobrar\CuentasPorCobrarController@destroy');
    Route::post('/crear-cuenta-venta', 'CuentasPorCobrar\CuentasPorCobrarController@crearCuentaVenta');
    Route::put('/update-cuenta-venta/{id}', 'CuentasPorCobrar\CuentasPorCobrarController@updateCuentaVenta');
    Route::post('/elimina-numero-orden', 'CuentasPorCobrar\CuentasPorCobrarController@eliminaNumeroOrden');
    Route::post('/send-comprobante-fiscal', 'CuentasPorCobrar\CuentasPorCobrarController@sendComprobanteFiscal');
    Route::get('/date/prueba', 'CuentasPorCobrar\CuentasPorCobrarController@showDate');
    Route::post('/genera-complemento', 'CuentasPorCobrar\CuentasPorCobrarController@generaFacturaComplemento');
    Route::post('/aplicar-factura-multiple', 'CuentasPorCobrar\CuentasPorCobrarController@aplicarFacturaMultiple');
    Route::post('/ereact-por-numero_orden', 'CuentasPorCobrar\CuentasPorCobrarController@getDataEreactByNumeroOrden');

});

Route::group(['prefix' => 'abonos-por-cobrar'], function () {
    Route::get('abonos-by-orden-entrada', 'CuentasPorCobrar\AbonosController@showByIdOrdenEntrada');
    Route::get('listado-abonos-by-orden-entrada', 'CuentasPorCobrar\AbonosController@listadoAbonosByOrdenEntrada');
    Route::get('verifica-abonos-pendientes', 'CuentasPorCobrar\AbonosController@getVerificaAbonosPendientes');
    Route::get('abono-por-folio', 'CuentasPorCobrar\AbonosController@getAbonoByFolio');
    Route::get('abonos-pagados-fecha', 'CuentasPorCobrar\AbonosController@getAbonosPagadosPorFecha');
    Route::get('/', 'CuentasPorCobrar\AbonosController@index');
    Route::get('/{id}', 'CuentasPorCobrar\AbonosController@show');
    Route::post('/', 'CuentasPorCobrar\AbonosController@store');
    Route::put('/{id}', 'CuentasPorCobrar\AbonosController@update');
    Route::put('/updateEstatus/{id}', 'CuentasPorCobrar\AbonosController@updateEstatus');
    Route::delete('/{id}', 'CuentasPorCobrar\AbonosController@destroy');
});

Route::group(['prefix' => 'solicitud-credito'], function () {
    Route::get('/', 'CuentasPorCobrar\SolicitudCreditoController@index');
    Route::get('/{id}', 'CuentasPorCobrar\SolicitudCreditoController@show');
    Route::post('/', 'CuentasPorCobrar\SolicitudCreditoController@store');
    Route::put('/{id}', 'CuentasPorCobrar\SolicitudCreditoController@update');
    Route::delete('/{id}', 'CuentasPorCobrar\SolicitudCreditoController@destroy');
    Route::get('/descargar/{id}/{string}', 'CuentasPorCobrar\SolicitudCreditoController@downloadArchivosSolicitudCredito');
});

Route::group(['prefix' => 'solicitud/tipo-documentos'], function () {
    Route::get('/', 'CuentasPorCobrar\TipoDocumentosController@index');
    Route::get('/{id}', 'CuentasPorCobrar\TipoDocumentosController@show');
    Route::post('/', 'CuentasPorCobrar\TipoDocumentosController@store');
    Route::put('/{id}', 'CuentasPorCobrar\TipoDocumentosController@update');
    Route::delete('/{id}', 'CuentasPorCobrar\TipoDocumentosController@destroy');
});

Route::group(['prefix' => 'catalogo-plazo-credito'], function () {
    Route::get('/', 'CuentasPorCobrar\CatPlazoCreditoController@index');
    Route::get('/{id}', 'CuentasPorCobrar\CatPlazoCreditoController@show');
    Route::post('/', 'CuentasPorCobrar\CatPlazoCreditoController@store');
    Route::put('/{id}', 'CuentasPorCobrar\CatPlazoCreditoController@update');
    Route::delete('/{id}', 'CuentasPorCobrar\CatPlazoCreditoController@destroy');
});

Route::group(['prefix' => 'estatus-cuentas'], function () {
    Route::get('/', 'CuentasPorCobrar\CatEstatusCuentasController@index');
    Route::get('/{id}', 'CuentasPorCobrar\CatEstatusCuentasController@show');
    Route::post('/', 'CuentasPorCobrar\CatEstatusCuentasController@store');
    Route::put('/{id}', 'CuentasPorCobrar\CatEstatusCuentasController@update');
    Route::delete('/{id}', 'CuentasPorCobrar\CatEstatusCuentasController@destroy');
});

Route::group(['prefix' => 'cuentas-morosas'], function () {
    Route::get('/', 'CuentasPorCobrar\CuentasMorosasController@index');
    Route::get('/buscar-filtro', 'CuentasPorCobrar\CuentasMorosasController@getBusqueda');
    Route::get('/{id}', 'CuentasPorCobrar\CuentasMorosasController@show');
    Route::post('/', 'CuentasPorCobrar\CuentasMorosasController@store');
    Route::put('/{id}', 'CuentasPorCobrar\CuentasMorosasController@update');
    Route::delete('/{id}', 'CuentasPorCobrar\CuentasMorosasController@destroy');
});
Route::group(['prefix' => 'pago-factura'], function () {
    Route::post('/guardar', 'CuentasPorCobrar\PagoFacturaController@guardar');
    Route::put('/actualizar/{id}', 'CuentasPorCobrar\PagoFacturaController@actualizar');
    Route::delete('/eliminar/{id}', 'CuentasPorCobrar\PagoFacturaController@eliminar');
    Route::get('/cuenta-id/{id}', 'CuentasPorCobrar\PagoFacturaController@GetByCuentaPorCobrarId');
    Route::post('/facturado', 'CuentasPorCobrar\PagoFacturaController@facturado');
});

Route::group(['prefix' => 'asientos'], function () {
    Route::post('/poliza-hojalateria', 'CuentasPorCobrar\AsientosController@registrarPolizaHojalateria');
    Route::post('/poliza-servicio', 'CuentasPorCobrar\AsientosController@registrarPolizaServicio');
    Route::post('/poliza-garantias', 'CuentasPorCobrar\AsientosController@registrarPolizaGarantias');
    Route::post('/poliza-venta-mostrador', 'CuentasPorCobrar\AsientosController@registrarPolizaVentaMostrador');
    Route::post('/poliza-venta-mostrador-credito', 'CuentasPorCobrar\AsientosController@registrarPolizaVentaMostradorCredito');
    Route::post('/poliza-servicio-interno', 'CuentasPorCobrar\AsientosController@registrarPolizaServicioInterno');
    Route::post('/poliza-refaccion-taller', 'CuentasPorCobrar\AsientosController@registrarPolizaRefaccionTaller');
    Route::post('/poliza-compra-fordPlanta', 'CuentasPorCobrar\AsientosController@registrarPolizaCompraFordPlanta');
    Route::post('/poliza-compra-proveedorExterno', 'CuentasPorCobrar\AsientosController@registrarPolizaCompraProveedorExterno');
    Route::post('/poliza-actualiza', 'CuentasPorCobrar\AsientosController@registrarActualizaPoliza');
    Route::post('/contabilidad', 'CuentasPorCobrar\AsientosController@setAsientoContabilidad');
    Route::post('/aplicar-asientos', 'CuentasPorCobrar\AsientosController@aplicaAsientosCuentas');
    Route::post('/poliza-devolucion-servicio', 'CuentasPorCobrar\AsientosController@registrarPolizaDevolucionServicio');
    Route::post('/poliza-devolucion-mostrador', 'CuentasPorCobrar\AsientosController@registrarPolizaDevolucionMostrador');

});

Route::resource('tipo-asiento', 'CuentasPorCobrar\TipoAsientoController');
