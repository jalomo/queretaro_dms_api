<?php

namespace App\Policies;

use App\Models\Auditoria\Auditoria;
use App\Models\Usuarios\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AuditoriaPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function create(?User $user)
    {
        $auditorias_activas = Auditoria::whereNull('terminada')->count();

        return ! ($auditorias_activas > 0)? $this->allow() : $this->deny(__('Existen auditorias en proceso solo se puede crear una a la vez.'));
    }

    public function cargar_refacciones(?User $user, Auditoria $auditoria)
    {
        return $auditoria->terminada == null? $this->allow() : $this->deny(__('La auditoria se encuentra terminada no es posible cargar mas refacciones.'));
    }

    public function delete(?User $user, Auditoria $auditoria)
    {
        return $auditoria->terminada == null? $this->allow() : $this->deny(__('La auditoria se encuentra terminada no se puede eliminar.'));
    }

    public function cancelar(?User $user, Auditoria $auditoria)
    {
        return $auditoria->terminada == null? $this->allow() : $this->deny(__('La auditoria se encuentra terminada no se puede cancelar.'));
    }

    public function completar(?User $user, Auditoria $auditoria)
    {
        return $auditoria->cancelada == null? $this->allow() : $this->deny(__('La auditoria se encuentra cancelada no se puede completar.'));
    }

    public function aplicar_ajustes(?User $user, Auditoria $auditoria)
    {
        if($auditoria->terminada != null && $auditoria->aprobada_por == null)
        {
            return $this->allow();
        }

        return $this->deny(__('La auditoria se encuentra terminada o ya se aplicaron los ajustes.'));
    }
}
