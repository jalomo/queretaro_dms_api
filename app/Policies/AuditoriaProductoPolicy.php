<?php

namespace App\Policies;

use App\Models\Auditoria\Auditoria;
use App\Models\Auditoria\AuditoriaProducto;
use App\Models\Usuarios\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AuditoriaProductoPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    public function update(?User $user, AuditoriaProducto $producto)
    {
        return $producto->auditoria->terminada == null? $this->allow() : $this->deny(__('La auditoria se encuentra concluida.'));
    }

    public function create(?User $user, Auditoria $auditoria)
    {
        return $auditoria->terminada == null? $this->allow() : $this->deny(__('La auditoria se encuentra concluida.'));
    }

    public function delete(?User $user, AuditoriaProducto $producto)
    {
        return $producto->auditoria->terminada == null? $this->allow() : $this->deny(__('La auditoria se encuentra concluida.'));
    }
}
