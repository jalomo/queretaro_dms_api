<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Refacciones\StockFileJobs;
use App\Models\Refacciones\ProductosModel;
use Illuminate\Support\Facades\DB;
use Throwable;

class StockTxtUploadJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $stock;
    protected $cont = 0;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(StockFileJobs $stock)
    {
        //
        $this->stock = $stock;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        logger()->info($this->stock->archivo);

        $this->stock->estatus = 'Procesando...';
        $this->stock->save();

        $file_path = storage_path($this->stock->archivo);

        $file = fopen($file_path, 'r');
        $productos = [];

        // Columnas unicas
        $columns = [
            ProductosModel::NO_IDENTIFICACION_FACTURA,
        ];
        
        // Columnas a actualizar
        $update = [
            ProductosModel::GPO,
            ProductosModel::BASICO,
            ProductosModel::PREFIJO,
            ProductosModel::SUFIJO,
            ProductosModel::CLAVE_UNIDAD_FACTURA,
            ProductosModel::CLAVE_PROD_SERV_FACTURA,
            ProductosModel::DESCRIPCION,
            ProductosModel::UNIDAD,
            ProductosModel::CANTIDAD,
            ProductosModel::VALOR_UNITARIO,
            ProductosModel::PRECIO_FACTURA,
            ProductosModel::COSTO_FORD,
            ProductosModel::TALLER_ID,
            ProductosModel::ALMACEN_ID,
            ProductosModel::UBICACION_PRODUCTO_ID,
        ];

        while (($data = fgets($file)) !== false) 
        {
            $this->cont++;

            $descripcion = utf8_encode(trim(substr($data, 23, 40)));
            $costo_ford = floatval(trim(substr($data, 68, 13)));
            $precio = floatval(trim(substr($data, 172, 13)));
            $Gpo = "FM";
            $prefijo = trim(substr($data,2, 6)); // Fix: algunas piezas ya traen el gpo
            $basico = trim(substr($data, 8, 8));
            $sufijo = trim(substr($data, 16, 7));
            $no_identificacion = preg_replace('/\s/', '', "$Gpo$prefijo$basico$sufijo"); // algunos traen espacios

            $productos[] = [
                ProductosModel::NO_IDENTIFICACION_FACTURA => $no_identificacion,
                ProductosModel::GPO =>  $Gpo,
                ProductosModel::BASICO =>  $basico,
                ProductosModel::PREFIJO =>  $prefijo,
                ProductosModel::SUFIJO =>  $sufijo,
                ProductosModel::CLAVE_UNIDAD_FACTURA => "",
                ProductosModel::CLAVE_PROD_SERV_FACTURA => "",
                ProductosModel::DESCRIPCION => $descripcion,
                ProductosModel::UNIDAD => "PIEZA",
                ProductosModel::CANTIDAD => 0,
                ProductosModel::VALOR_UNITARIO => $precio,
                ProductosModel::PRECIO_FACTURA => $precio,
                ProductosModel::COSTO_FORD => $costo_ford,
                ProductosModel::TALLER_ID => 1,
                ProductosModel::ALMACEN_ID => 1,
                ProductosModel::UBICACION_PRODUCTO_ID => 1
            ];

            if($this->cont % 500 == 0)
            {
                ProductosModel::upsert($productos, $columns, $update);
                $productos = [];

                $this->stock->estatus = "{$this->cont} registros procesados.";
                $this->stock->save();
            }
        }

        // Fix: insertar o actualizar los ultimos valores despues de que se recorrio
        // el final del archivo
        if(! empty($productos))
        {
            ProductosModel::upsert($productos, $columns, $update);
            $productos = [];
        }

        // Insertar los productos del universo de partes en stock productos que no contienen 
        // inventario en 0 para que no falle en mostrador y detalles excellence si no tiene
        // stock
        DB::statement('INSERT INTO stock_productos (producto_id, cantidad_actual, cantidad_almacen_primario, cantidad_almacen_secundario, cantidad_compras, cantidad_ventas, total_precio_compras, total_precio_ventas)
        SELECT
            producto.id AS producto_id,
            COALESCE ( stock_productos.cantidad_actual, 0 ) AS cantidad_actual,
            COALESCE ( stock_productos.cantidad_almacen_primario, 0 ) AS cantidad_almacen_primario,
            COALESCE(stock_productos.cantidad_almacen_secundario, 0) as cantidad_almacen_secundario,
            COALESCE(stock_productos.cantidad_compras, 0) as cantidad_compras,
            COALESCE(stock_productos.cantidad_ventas, 0) as cantidad_ventas,
            COALESCE(stock_productos.total_precio_compras, 0) as total_precio_compras,
            COALESCE(stock_productos.total_precio_ventas, 0) as total_precio_ventas
        FROM
            producto
            LEFT JOIN stock_productos ON producto.id = stock_productos.producto_id 
        WHERE
            producto_id IS NULL');

        $this->stock->estatus = 'Exito. Se proceso correctamente el archivo.';
        $this->stock->save();

        // Borramos el archivo procesado
        unlink($file_path);
    }

    public function failed(Throwable $exception)
    {
        // Registramos en el log
        logger()->warning($exception->getMessage(), [ 'file' => $exception->getFile(), 'line' => $exception->getLine(), 'fila' => $this->cont - 1]);

        // Registramos en la base de datos
        $this->stock->estatus = "Error: se procesaron " . ($this->cont - 1);
        $this->stock->save();

        // Borramos el archivo en caso de que exista y ocurrio un error
        $filepath = storage_path($this->stock->archivo);

        if(file_exists($filepath))
        {
            unlink($filepath);
        }
    }
}
