<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Refacciones\StockFileJobs;
use Illuminate\Support\Facades\Log;
use App\Models\Refacciones\ClientesModel;
use App\Models\CuentasPorCobrar\CuentasPorCobrarModel;
use Carbon\Carbon;
use App\Servicios\Refacciones\ServicioFolios;
use Illuminate\Support\Facades\Http;

class UploadClientesCSV implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $stock;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(StockFileJobs $stock)
    {
        //
        $this->stock = $stock;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ServicioFolios $servicio)
    {
        //
        Log::info($this->stock->archivo);

        $this->stock->estatus = 'Procesando...';
        $this->stock->save();

        $file_path = storage_path($this->stock->archivo);

        try{
            $file = fopen($file_path, 'r');
            $cont = 0;

            while (($data = fgetcsv($file)) !== false) 
            {
                $cont++;

                if($cont < 5)
                    continue; // Saltamos primeras lineas del excel
                
                $clave = trim($data[0]);
                $nombre_cliente = utf8_encode(trim($data[1]));
                $cuenta_refacciones = doubleval(trim($data[2]));
                $cuenta_servicios = doubleval(trim($data[3]));
                $cuenta_unidades = doubleval(trim($data[4]));
                $departamento_refacciones = env('ALMACEN_PRIMARIO')? 13 : 13;
                $departamento_servicios = env('ALMACEN_PRIMARIO')? 7 : 15;
                $departamento_unidades = env('ALMACEN_PRIMARIO')? 14 : 16;

                $cliente = ClientesModel::updateOrCreate([
                    'numero_cliente' => $clave
                ], [
                    'nombre' => $nombre_cliente,
                    'nombre_empresa' => $nombre_cliente,
                ]);
                
                // Generamos folio para el cliente
                $folio = $servicio->generarFolio(8); // Tipo servicio
                
                // Generamos la cuenta por cobrar y mandamos el saldo a contabilidad para refacciones
                if($cuenta_refacciones > 0)
                {
                    CuentasPorCobrarModel::create([
                        'folio_id' => $folio->id,
                        'cliente_id' => $cliente->id,
                        'estatus_cuenta_id' => 5, // PAGO EN PROCESO
                        'concepto' => 'CUENTA POR COBRAR REFACCIONES INICIAL CLIENTE '. $cliente->nombre,
                        'importe' => $cuenta_refacciones,
                        'total' => $cuenta_refacciones,
                        'fecha' => Carbon::now(),
                    ]);

                    $this->contabilidad($cuenta_refacciones, $cliente->id, $folio->id, 1, $departamento_refacciones);
                }
                else
                {
                    $this->contabilidad($cuenta_refacciones, $cliente->id, $folio->id, 2, $departamento_refacciones);
                }

                // Generamos la cuenta por cobrar y mandamos el saldo a contabilidad para servicios
                if($cuenta_servicios > 0)
                {
                    CuentasPorCobrarModel::create([
                        'folio_id' => $folio->id,
                        'cliente_id' => $cliente->id,
                        'estatus_cuenta_id' => 5, // PAGO EN PROCESO
                        'concepto' => 'CUENTA POR COBRAR SERVICIOS INICIAL CLIENTE '. $cliente->nombre,
                        'importe' => $cuenta_servicios,
                        'total' => $cuenta_servicios,
                        'fecha' => Carbon::now(),
                    ]);

                    $this->contabilidad($cuenta_servicios, $cliente->id, $folio->id, 1, $departamento_servicios);
                }
                else
                {
                    $this->contabilidad($cuenta_servicios, $cliente->id, $folio->id, 2, $departamento_servicios);
                }

                // Generamos la cuenta por cobrar y mandamos el saldo a contabilidad para las unidades
                if($cuenta_unidades > 0)
                {
                    CuentasPorCobrarModel::create([
                        'folio_id' => $folio->id,
                        'cliente_id' => $cliente->id,
                        'estatus_cuenta_id' => 5, // PAGO EN PROCESO
                        'concepto' => 'CUENTA POR COBRAR UNIDADES INICIAL CLIENTE '. $cliente->nombre,
                        'importe' => $cuenta_unidades,
                        'total' => $cuenta_unidades,
                        'fecha' => Carbon::now(),
                    ]);

                    $this->contabilidad($cuenta_unidades, $cliente->id, $folio->id, 1, $departamento_unidades);
                }
                else
                {
                    $this->contabilidad($cuenta_unidades, $cliente->id, $folio->id, 2, $departamento_unidades);
                }
                

                if($cont % 100 == 0)
                {
                    $this->stock->estatus = ($cont - 1) . " registros procesados.";
                    $this->stock->save();
                }
            }

            $this->stock->estatus = 'Exito. Se proceso correctamente el archivo clientes.';
            $this->stock->save();

        } catch(\Exception $e){
            $this->stock->estatus = "Error: se procesaron " . ($cont - 1);
            $this->stock->save();

            Log::emergency($e->getMessage());
        }

        // Borramos el archivo procesado
        unlink($file_path);
    }

    /**
     * Llamada al webservice de contabilidad
     * 
     * @param float $total
     * @param int $cliente
     * @param int $folio
     * @param int $tipo 1=Abono 2=Cargo
     * @param int $departamento 7=Servicios QRT, 13=Refacciones, 14=Unidades QRT, 15=Servicios SNJ, 16=Unidades SNJ
     * @return boolean
     */
    public function contabilidad($total, $cliente, $folio, $tipo, $departamento)
    {
        $contabilidad = Http::asForm()->post(env('URL_CONTABILIDAD'). 'asientos/api/agregar', [
            'clave_poliza' => 'DO',
            'tipo' => $tipo, // 1 Abono, 2 Cargo
            'cuenta' => env('ALMACEN_PRIMARIO') == 'QUERETARO'? 158 : 159, // 158 Queretaro, 159 San Juan
            'total' => $total,
            'concepto' => $tipo == 1? 'CUENTA INICIAL - ABONO '. env('ALMACEN_PRIMARIO') : 'CUENTA INICIAL - CARGO '. env('ALMACEN_PRIMARIO'),
            'departamento' => $departamento,
            'estatus' => 'APLICADO',
            'folio' => env('ALMACEN_PRIMARIO') == 'QUERETARO'? 'DMS-QRT-'.  $folio : 'DMS-SNJ-' . $folio,
            'cliente_id' => $cliente,
            'sucursal_id' => env('ALMACEN_PRIMARIO') == 'QUERETARO'? 1 : 2, // 1 Queretaro, 2 San Juan
        ]);

        return $contabilidad->successful();
    }
}
