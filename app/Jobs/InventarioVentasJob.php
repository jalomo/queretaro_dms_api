<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Refacciones\InventarioProductos;
use App\Models\Refacciones\TipoMovimiento;
use App\Models\Refacciones\VentaProductoModel;

class InventarioVentasJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $movimientos = [
        TipoMovimiento::VENTA_DETALLES,
        TipoMovimiento::VENTA_MOSTRADOR,
        TipoMovimiento::EN_PROCESO_VENTA_DETALLES,
        TipoMovimiento::EN_PROCESO_VENTA_MOSTRADOR,
        TipoMovimiento::DEVOLUCION_VENTA_DETALLES,
        TipoMovimiento::DEVOLUCION_MOSTRADOR,
    ];

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //Obtenemos todos los model_id de las ventas donde la factura o folio sean vacios
        $refacciones = InventarioProductos::
            whereIn('tipo_movimiento_id', $this->movimientos)
            ->where(function($q){
                $q->whereNull('factura');
                $q->orWhereNull('folio');
            })
            ->get();

        if($refacciones->isNotEmpty())
        {
            // Todas las ventas de las refacciones que coincidan con el model_id en inventario
            $ventas = VentaProductoModel::
                with('folio', 'venta')
                ->whereIn('id', $refacciones->pluck('model_id'))
                ->get();
    
            $refacciones->each(function($refaccion) use ($ventas){

                // Aqui va la actualizacion de la factura y el folio o numero de orden

                $ventas = $ventas->keyBy('id');

                if(isset($ventas[$refaccion->model_id]))
                {
                    $refaccion->factura = '';
                    $refaccion->folio = $ventas[$refaccion->model_id]->venta->numero_orden?? $ventas[$refaccion->model_id]->folio->folio?? null;
                    $refaccion->save();
                }
            });
        }
    }
}
