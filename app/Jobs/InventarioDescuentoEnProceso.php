<?php

namespace App\Jobs;

use App\Models\Refacciones\InventarioProductos;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\TipoMovimiento;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class InventarioDescuentoEnProceso implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $movimientos = [
        TipoMovimiento::EN_PROCESO_VENTA_DETALLES => TipoMovimiento::VENTA_DETALLES,
        TipoMovimiento::EN_PROCESO_VENTA_MOSTRADOR => TipoMovimiento::VENTA_MOSTRADOR,
        TipoMovimiento::EN_PROCESO_DEVOLUCION_PEDIDO => TipoMovimiento::DEVOLUCION_PEDIDO,
        TipoMovimiento::EN_PROCESO_APARTAR => TipoMovimiento::APARTADO,
        TipoMovimiento::EN_PROCESO_AJUSTE_AUDITORIA_DISMINUCION => TipoMovimiento::AJUSTE_AUDITORIA_DISMINUCION,
    ];
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $refacciones = InventarioProductos::whereIn('tipo_movimiento_id', $this->getMovimientosProceso())->whereNull('procesado')->get();

        $refacciones->each(function($refaccion){
            $producto = ProductosModel::find($refaccion->producto_id);
            
            // Si no existe inventario para el producto 
            // continuamos con la siguiente iteracion
            if($producto == null)
            {
                return true;
            }

            $stock = $producto->desglose_producto()->first();

            if($stock->cantidad_actual >= $refaccion->cantidad)
            {
                $existe = $stock->cantidad_actual - $refaccion->cantidad;

                // Creamos movimiento decremento de lo que existe en inventario
                $producto->inventario()->create([
                    'tipo_movimiento_id' => $this->getMovimiento($refaccion->tipo_movimiento_id),
                    'model_id' => $refaccion->model_id,
                    'cantidad' => $refaccion->cantidad,
                    'existe' => $existe,
                    'existia' => $stock->cantidad_actual,
                    'precio' => $refaccion->precio,
                    'costo_promedio' => $producto->costo_promedio,
                ]);

                $stock->cantidad_actual = $existe;
                $stock->cantidad_almacen_primario = $existe;
                $stock->save();

                $producto->cantidad = $existe;
                $producto->save();
    
                $refaccion->procesado = now();
                $refaccion->save();
            }
            elseif($stock->cantidad_actual > 0 && $stock->cantidad_actual < $refaccion->cantidad)
            {
                $existe = $stock->cantidad_actual;
                $cantidad_proceso = $refaccion->cantidad - $stock->cantidad_actual;

                // Creamos movimiento decremento de lo que se puede descontar del inventario
                $producto->inventario()->create([
                    'tipo_movimiento_id' => $this->getMovimiento($refaccion->tipo_movimiento_id),
                    'model_id' => $refaccion->model_id,
                    'cantidad' => $existe,
                    'existe' => $stock->cantidad_actual - $existe,
                    'existia' => $stock->cantidad_actual,
                    'precio' => $refaccion->precio,
                    'costo_promedio' => $producto->costo_promedio,
                ]);

                // Creamos un nuevo movimiento en proceso de lo que resta por descontar
                $producto->inventario()->create([
                    'tipo_movimiento_id' => $refaccion->tipo_movimiento_id,
                    'model_id' => $refaccion->model_id,
                    'cantidad' => $cantidad_proceso,
                    'existe' => $stock->cantidad_actual - $existe,
                    'existia' => $stock->cantidad_actual,
                    'precio' => $refaccion->precio,
                    'costo_promedio' => $producto->costo_promedio,
                ]);

                $stock->cantidad_actual = $existe;
                $stock->cantidad_almacen_primario = $existe;
                $stock->save();

                $producto->cantidad = $existe;
                $producto->save();
    
                $refaccion->procesado = now();
                $refaccion->save();
            }
            
        });
    }

    public function getMovimientosProceso()
    {
        return array_keys($this->movimientos);
    }

    public function getMovimiento(int $tipo_movimiento_id) : int
    {
        return $this->movimientos[$tipo_movimiento_id];
    }
}
