<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Refacciones\StockFileJobs;
use App\Models\Refacciones\CatalogoUbicacionProductoModel;
use App\Servicios\Producto\CrearProductoService;
use RuntimeException;
use Throwable;

class StockUploadJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $stock;
    protected $cont = 0;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(StockFileJobs $stock)
    {
        //
        $this->stock = $stock;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        logger()->info($this->stock->archivo);

        $this->stock->estatus = 'Procesando...';
        $this->stock->save();

        $file_path = storage_path($this->stock->archivo);
        
        $file = fopen($file_path, 'r');

        while (($data = fgetcsv($file)) !== false) 
        {
            $this->cont++;

            // Saltamos la primera linea que corresponde
            // al Header del archivo .csv que se sube
            if($this->cont == 1)
            {
                $this->validate_columns($data);
                $this->exists('Parte', $data[0]);
                $this->exists('Grupo', $data[1]);
                $this->exists('Prefijo', $data[2]);
                $this->exists('Basico', $data[3]);
                $this->exists('Sufijo', $data[4]);
                $this->exists('Descripcion', $data[5]);
                $this->exists('F.U.E', $data[6]);
                $this->exists('P.Lista', $data[7]);
                $this->exists('P.Mayoreo', $data[8]);
                $this->exists('C.Prom', $data[9]);
                $this->exists('C.Ford', $data[10]);
                $this->exists('Existencia', $data[11]);
                $this->exists('Ubicacion', $data[12]);

                continue;
            }

            $Gpo = trim($data[1]);
            $prefijo = trim($data[2]);
            $basico = trim($data[3]);
            $sufijo = trim($data[4]);
            $descripcion = utf8_encode(trim($data[5]));
            $precio = trim($data[7]);
            $costo_promedio = trim($data[9]);
            $cantidad = intval(trim($data[11])) > 0 ? intval(trim($data[11])) : 0;
            $no_identificacion = trim(preg_replace('/\s/', '', "$Gpo$prefijo$basico$sufijo"));
            $ubicacion = utf8_encode(trim($data[12]));

            $catalogo_ubicacion = CatalogoUbicacionProductoModel::where('nombre', $ubicacion)->first();

            $producto = CrearProductoService::create([
                'grupo' => $Gpo,
                'prefijo' => $prefijo,
                'basico' => $basico,
                'sufijo' => $sufijo,
                'descripcion' => $descripcion,
                'ubicacion_producto_id' => $catalogo_ubicacion->id?? null,
                'unidad' => 'PIEZA',
                'cantidad' => $cantidad,
                'valor_unitario' => $precio,
                'costo_promedio' => $costo_promedio,
            ]);

            // Actualizamos valores
            $producto->update([
                'valor_unitario' => $precio,
                'cantidad' => $cantidad,
                'costo_promedio' => $costo_promedio,
            ]);

            if($this->cont % 100 == 0)
            {
                $this->stock->estatus = ($this->cont - 1) . " registros procesados.";
                $this->stock->save();
            }
        }

        $this->stock->estatus = 'Exito. Se proceso correctamente el archivo.';
        $this->stock->save();
        
        // Borramos el archivo procesado
        unlink($file_path);
    }

    public function exists($clave, $parametro)
    {
        // $clave = mb_detect_encoding($clave, 'UTF-8', true) == false? mb_convert_encoding($clave, 'UTF-8') : $clave;
        // $parametro = mb_detect_encoding($parametro, 'UTF-8', true) == false? mb_convert_encoding($parametro, 'UTF-8') : $parametro;

        // if($clave !== $parametro)
        // {
        //     throw new RuntimeException("No existe la columna $clave se ingresa $parametro");
        // }
    }

    public function validate_columns(array $data)
    {
        if($columns = count($data) == 12)
        {
            throw new RuntimeException("El número de columnas en el formato no coinciden, ingresadas: $columns, requeridas: 12.");
        }
    }

    public function failed(Throwable $exception)
    {
        // Registramos en el log
        logger()->warning($exception->getMessage(), [ 'file' => $exception->getFile(), 'line' => $exception->getLine(), 'fila' => $this->cont - 1]);

        // Registramos en la base de datos
        $this->stock->estatus = "Error: se procesaron " . ($this->cont - 1) ."; ". $exception->getMessage();
        $this->stock->save();

        // Borramos el archivo en caso de que exista y ocurrio un error
        $filepath = storage_path($this->stock->archivo);

        if(file_exists($filepath))
        {
            unlink($filepath);
        }
    }
}
