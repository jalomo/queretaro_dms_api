<?php

namespace App\Servicios\Usuarios\Menu;

use App\Servicios\Core\ServicioDB;
use App\Models\Usuarios\MenuSeccionesModel;
use App\Models\Usuarios\RolesSeccionesModel;
use App\Models\Usuarios\RolModel;
use DB;

class ServicioSeccionesMenu extends ServicioDB
{
    public function __construct()
    {
        $this->modelo = new MenuSeccionesModel();
        $this->rolesSeccionesModel = new RolesSeccionesModel();
        $this->recurso = 'secciones';
    }

    public function getReglasGuardar()
    {
        return [
            MenuSeccionesModel::NOMBRE => 'required|unique:modulo,nombre',
            MenuSeccionesModel::MODULO_ID => 'required|exists:modulo,id'
        ];
    }

    public function getReglasUpdate()
    {
        return [
            MenuSeccionesModel::NOMBRE => 'required|unique:modulo,nombre',
            MenuSeccionesModel::MODULO_ID => 'required|exists:modulo,id'
        ];
    }

    public function getReglasSecciones()
    {
        return [
            MenuSeccionesModel::MODULO_ID => 'required|exists:modulo,id'
        ];
    }

    public function buscarTodos()
    {
        $query = $this->modelo->select(
            'menu_secciones.id as id',
            'menu_secciones.nombre as seccion_nombre',
            'modulo.nombre as modulo_nombre',
            'modulo.id as modulo_id'
        );
        $query->join('modulo', 'modulo.id', '=', 'menu_secciones.modulo_id');

        if (isset($parametros[MenuSeccionesModel::MODULO_ID])) {
            $query->where(MenuSeccionesModel::MODULO_ID, '=', $parametros[MenuSeccionesModel::MODULO_ID]);
        }

        return  $query->get();
    }

    public function getSecciones($parametros)
    {
        return $this->modelo
            ->where(MenuSeccionesModel::MODULO_ID, '=', $parametros[MenuSeccionesModel::MODULO_ID])
            ->get();
    }

    public function seccionesByModulos($parametros)
    {
        $query = $this->modelo->select(
            'menu_secciones.id as seccion_id',
            'menu_secciones.nombre as seccion_nombre',
            'modulo.nombre as modulo_nombre',
            'modulo.id as modulo_id'
        );
        $query->join('modulo', 'modulo.id', '=', 'menu_secciones.modulo_id');
        $query->join('det_roles_secciones', 'det_roles_secciones.seccion_id', '=', 'menu_secciones.id');

        if (isset($parametros[MenuSeccionesModel::MODULO_ID])) {
            $query->where(MenuSeccionesModel::MODULO_ID, '=', $parametros[MenuSeccionesModel::MODULO_ID]);
        }

        if (isset($parametros['rol_id'])) {
            $query->where('det_roles_secciones.rol_id', '=', $parametros['rol_id']);
        }

        return  $query->get();
    }

    public function seccionesByModulosPermisos($parametros)
    {
        $query = $this->modelo->select(
            'menu_secciones.id as seccion_id',
            'menu_secciones.nombre as seccion_nombre',
            'menu_secciones.modulo_id'
        );
        $query->join('menu_usuario_vista', 'menu_usuario_vista.seccion_id', '=', 'menu_secciones.id');
        $query->join('menu_submenu', 'menu_submenu.seccion_id', '=', 'menu_secciones.id');

        if (isset($parametros[MenuSeccionesModel::MODULO_ID])) {
            $query->where(MenuSeccionesModel::MODULO_ID, '=', $parametros[MenuSeccionesModel::MODULO_ID]);
        }

        return  $query->groupBy(
            'menu_secciones.id',
            'menu_secciones.nombre',
            'menu_secciones.modulo_id'
        )->dd();
    }

    public function getSeccionesByRol($parametros)
    {
        $query = $this->modelo->select(
            'menu_secciones.id as seccion_id',
            'menu_secciones.nombre as seccion_nombre'
        );
        $query->join('det_roles_secciones', 'det_roles_secciones.seccion_id', '=', 'menu_secciones.id');
        $query->join('roles', 'det_roles_secciones.rol_id', '=', 'roles.id');

        if (isset($parametros[RolesSeccionesModel::ROL_ID])) {
            $query->where(RolModel::getTableName() . '.' . RolModel::ID, '=', $parametros[RolesSeccionesModel::ROL_ID]);
        }
        return  $query->get();
    }

    public function storeSeccionesByRole($parametros)
    {
        $secciones = $parametros[RolesSeccionesModel::SECCION_ID];
        $this->rolesSeccionesModel
            ->where(RolesSeccionesModel::ROL_ID, '=',  $parametros[RolesSeccionesModel::ROL_ID])
            ->delete();

        foreach ($secciones as $key => $item) {
            $this->rolesSeccionesModel->create([
                RolesSeccionesModel::ROL_ID => $parametros[RolesSeccionesModel::ROL_ID],
                RolesSeccionesModel::SECCION_ID => $item,
            ]);
        }
    }

    public function getCatalogo()
    {
        return $this->modelo->select(
            MenuSeccionesModel::getTableName() . '.' . MenuSeccionesModel::ID,
            DB::raw("CONCAT(modulo.nombre,' - ',menu_secciones.nombre) as nombre"),
        )->join('modulo', 'modulo.id', '=', 'menu_secciones.modulo_id')->get();
    }
}
