<?php

namespace App\Servicios\Usuarios\Menu;

use App\Servicios\Core\ServicioDB;
use App\Models\Usuarios\Menu\ModulosModel;
use App\Models\Usuarios\MenuSeccionesModel;
use App\Models\Usuarios\MenuSubmenuModel;
use App\Models\Usuarios\MenuVistasModel;

class ServicioModulos extends ServicioDB
{
    public function __construct()
    {
        $this->modelo = new ModulosModel();
        $this->recurso = 'modulos';
    }

    public function getReglasGuardar()
    {
        return [
            ModulosModel::NOMBRE => 'required|unique:modulo,nombre',
            ModulosModel::ORDEN => 'required|numeric',
            ModulosModel::ICONO => 'required',
        ];
    }

    public function getReglasUpdate()
    {
        return [
            ModulosModel::NOMBRE => 'required',
            ModulosModel::ORDEN => 'required|numeric',
            ModulosModel::ICONO => 'required'
        ];
    }

    public function buscarTodos()
    {
        return $this->modelo->select(
            ModulosModel::ID,
            ModulosModel::NOMBRE,
            ModulosModel::ORDEN,
            ModulosModel::ICONO,
            ModulosModel::DEFAULT
        )->orderBy(ModulosModel::ORDEN, 'asc')->get();
    }

    public function getModulosPermisos($request)
    {
        $table = ModulosModel::getTableName();
        $tableSecciones = MenuSeccionesModel::getTableName();
        $tableSubMenu = MenuSubmenuModel::getTableName();
        $tableVistas = MenuVistasModel::getTableName();

        $consulta = $this->modelo
            ->select(
                $table . '.' . ModulosModel::ID . ' as modulo_id',
                $table . '.' . ModulosModel::NOMBRE . ' as modulo',
                $tableSecciones . '.' . MenuSeccionesModel::ID . ' as seccion_id',
                $tableSecciones . '.' . MenuSeccionesModel::NOMBRE . ' as seccion',
                $tableSubMenu . '.' . MenuSubmenuModel::ID . ' as submenu_id',
                $tableSubMenu . '.' . MenuSubmenuModel::NOMBRE . ' as submenu',
                $tableVistas . '.' . MenuVistasModel::ID . ' as vista_id',
                $tableVistas . '.' . MenuVistasModel::NOMBRE . ' as vista',
            )
            ->join(
                $tableSecciones,
                $table . '.' . ModulosModel::ID,
                '=',
                $tableSecciones . '.' . MenuSeccionesModel::MODULO_ID
            )->join(
                $tableSubMenu,
                $tableSecciones . '.' . MenuSeccionesModel::ID,
                '=',
                $tableSubMenu . '.' . MenuSubmenuModel::SECCION_ID
            )->join(
                $tableVistas,
                $tableSubMenu . '.' . MenuSubmenuModel::ID,
                '=',
                $tableVistas . '.' . MenuVistasModel::SUBMENU_ID
            );
        if (isset($request->modulo_id)) {
            $consulta->where($table . '.' . ModulosModel::ID, $request->modulo_id);
        }
        if (isset($request->seccion_id)) {
            $consulta->where($tableSecciones . '.' . MenuSeccionesModel::ID, $request->seccion_id);
        }
        if (isset($request->submenu_id)) {
            $consulta->where($tableSubMenu . '.' . MenuSubmenuModel::ID, $request->submenu_id);
        }
        if (isset($request->vista_id)) {
            $consulta->where($tableVistas . '.' . MenuVistasModel::ID, $request->vista_id);
        }
        return $consulta->get();
    }
}
