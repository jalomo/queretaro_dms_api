<?php

namespace App\Servicios\Usuarios\Menu;

use App\Models\Usuarios\RolModel;
use App\Servicios\Core\ServicioDB;
use App\Models\Usuarios\User;
use App\Models\Usuarios\UsuariosModulosModel;

class ServicioUsuariosModulos extends ServicioDB
{
    public function __construct()
    {
        $this->modelo = new UsuariosModulosModel();
        $this->modeloUsuarios = new User();
        $this->modeloRoles = new RolModel();
        $this->recurso = 'usuarios_modulos';
    }

    public function getReglasGuardar()
    {
        return [
            UsuariosModulosModel::USUARIO_ID => 'required|exists:usuarios,id',
            UsuariosModulosModel::MODULO_ID => 'required|exists:modulo,id'
        ];
    }

    public function getReglasUpdate()
    {
        return [
            UsuariosModulosModel::USUARIO_ID => 'required|exists:usuarios,id',
            UsuariosModulosModel::MODULO_ID => 'required|exists:modulo,id'
        ];
    }

    public function reglasStoreUsuariosModulos()
    {
        return [
            UsuariosModulosModel::MODULO_ID => 'required|array',
            UsuariosModulosModel::USUARIO_ID => 'required|exists:usuarios,id',
        ];
    }

    public function storeUsuariosModulos($parametros)
    {
        $modulos = $parametros[UsuariosModulosModel::MODULO_ID];
        $this->modelo
            ->where(UsuariosModulosModel::USUARIO_ID, '=',  $parametros[UsuariosModulosModel::USUARIO_ID])
            ->delete();

        foreach ($modulos as $key => $item) {
            $this->crear([
                UsuariosModulosModel::USUARIO_ID => $parametros[UsuariosModulosModel::USUARIO_ID],
                UsuariosModulosModel::MODULO_ID => $item,
            ]);
        }
    }

    public function getReglasGetModulosByUsuarios()
    {
        return [
            UsuariosModulosModel::USUARIO_ID => 'required|exists:usuarios,id'
        ];
    }

    public function usuariosAndModulos($parametros)
    {
        $tabla_roles = RolModel::getTableName();
        
        $query = $this->modeloRoles->select(
            'det_roles_modulos.id',
            'det_roles_modulos.rol_id',
            'det_roles_modulos.modulo_id',
            'modulo.orden',
            'modulo.nombre as nombre_modulo',
            'modulo.icono',
        );

        $query->join('det_roles_modulos', 'det_roles_modulos.rol_id', '=', 'roles.id');
        $query->join('modulo', 'modulo.id', '=', 'det_roles_modulos.modulo_id');

        if (isset($parametros['rol_id'])) {
            $query->where($tabla_roles . '.' . RolModel::ID, '=', $parametros['rol_id']);
        }

        return  $query->orderBy('modulo.orden', 'ASC')->get();

    }
}
