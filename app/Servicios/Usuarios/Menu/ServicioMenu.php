<?php

namespace App\Servicios\Usuarios\Menu;

use App\Servicios\Core\ServicioDB;
use App\Models\Usuarios\MenuSeccionesModel;
use App\Models\Usuarios\MenuSubmenuModel;
use App\Models\Usuarios\MenuVistasModel;
use App\Models\Usuarios\UsuariosModulosModel;
use App\Models\Usuarios\RolesModulosModel;
use App\Models\Usuarios\RolesSeccionesModel;
use App\Models\Usuarios\RolesSubmenuModel;
use App\Models\Usuarios\RolesVistasModel;
use App\Servicios\Usuarios\Menu\ServicioUsuariosModulos;
use App\Servicios\Usuarios\ServicioUsuarios;
use DB;

class ServicioMenu extends ServicioDB
{
    private $permisos;

    public function __construct()
    {
        $this->servicioModulos = new ServicioModulos();
        $this->servicioRoles = new ServicioRoles();
        $this->servicioUsuariosModulos = new ServicioUsuariosModulos();
        $this->servicioSeccionesMenu = new ServicioSeccionesMenu();
        $this->servicioSubmenus = new ServicioSubmenus();
        $this->serviciomenuVistas = new ServiciomenuVistas();
        $this->servicioMenuUsuariosVistas = new ServicioMenuUsuariosVistas();
        $this->servicioRolesVistas = new ServicioRolesVistas();
        $this->servicioUsuarios = new ServicioUsuarios();
        $this->recurso = 'menu';
        $this->rol_id = null;
        $this->menuModulosModel = new RolesModulosModel();
        $this->menuSeccionesModel = new RolesSeccionesModel();
        $this->menuSubMenuModel = new RolesSubmenuModel();
        $this->menusVistasModel = new RolesVistasModel();
    }

    public function getReglasMenu()
    {
        return [
            UsuariosModulosModel::USUARIO_ID => 'required|exists:usuarios,id'
        ];
    }

    public function getReglasGuardar()
    {
        return [
            RolesModulosModel::ROL_ID => 'required|exists:roles,id',
            RolesModulosModel::MODULO_ID => 'required|exists:modulo,id',
            RolesSeccionesModel::SECCION_ID => 'required|exists:menu_secciones,id',
            RolesSubmenuModel::SUBMENU_ID => 'required|exists:menu_submenu,id',
            RolesVistasModel::VISTA_ID => 'required|exists:menu_vistas,id',
        ];
    }

    public function getReglasUpdate()
    {
        return [];
    }

    public function setPermisos($array_permisos)
    {
        return $this->permisos = $array_permisos;
    }

    public function getMenu_2($parametros)
    {
        $query = $this->servicioRolesVistas->getVistasRoles(['id' => $parametros['usuario_id']]);
        $menu_array = [];
        foreach ($query as $key => $item) {
            $menu_array["modulo_$item->modulo_id"] = [
                'id' => $item->modulo_id,
                'nombre_modulo' => $item->nombre_modulo
            ];
        }
        return $menu_array;
    }

    public function getmenu($parametros)
    {
        $usuario = $this->servicioUsuarios->getById($parametros['usuario_id']);
        $this->rol_id = $usuario->rol_id;
        $modulos_usuarios = $this->servicioUsuariosModulos->usuariosAndModulos(['rol_id' => $usuario->rol_id]);
        $array_menu = [];
        foreach ($modulos_usuarios as $key => $modulo) {
            $item = new \stdClass;
            $item->modulo_id = $modulo->modulo_id;
            $item->modulo = $modulo->nombre_modulo;
            $item->icono = $modulo->icono;
            $item->orden = $modulo->orden;
            $seccion = $this->getmenuSeccion([
                MenuSeccionesModel::MODULO_ID => $modulo->modulo_id
            ]);
            $item->contenido_seccion = $seccion;
            if (count($seccion) > 0 || $modulo->modulo_id == 1) {
                array_push($array_menu, $item);
            }
        }

        return $array_menu;
    }




    public function getmenuSeccion($parametros)
    {

        $secciones = $this->servicioSeccionesMenu->seccionesByModulos([
            MenuSeccionesModel::MODULO_ID => $parametros[MenuSeccionesModel::MODULO_ID],
            'rol_id' => $this->rol_id
        ]);

        $array_menu = [];
        foreach ($secciones as $key => $seccion) {
            $item = new \stdClass;
            $item->seccion_id = $seccion->seccion_id;
            $item->seccion_nombre = $seccion->seccion_nombre;
            $item->modulo_id = $seccion->modulo_id;

            $submenus = $this->getSubmenus([
                MenuSubmenuModel::SECCION_ID => $seccion->seccion_id
            ]);

            $item->contenido_submenus = $submenus;
            if (count($submenus) > 0) {
                array_push($array_menu, $item);
            }
        }

        return $array_menu;
    }

    public function getSubmenus($parametros)
    {

        $submenus = $this->servicioSubmenus->subMenuBySeccion([
            MenuSubmenuModel::SECCION_ID => $parametros[MenuSubmenuModel::SECCION_ID],
            'rol_id' => $this->rol_id
        ]);
        $array_menu = [];
        foreach ($submenus as $key => $submenu) {
            $item = new \stdClass;
            $item->submenu_id = $submenu->submenu_id;
            $item->nombre_submenu = $submenu->nombre_submenu;
            $item->seccion_nombre = $submenu->seccion_nombre;
            $item->seccion_id = $submenu->seccion_id;
            $item->visible = $submenu->visible;

            $vistas = $this->getVistas([
                MenuVistasModel::SUBMENU_ID => $submenu->submenu_id
            ]);

            $item->vistas = $vistas;
            if (count($vistas) > 0) {
                array_push($array_menu, $item);
            }
        }
        return $array_menu;
    }

    public function getVistas($parametros)
    {
        $vistas = $this->serviciomenuVistas->vistasBySubmenuId([
            MenuVistasModel::SUBMENU_ID => $parametros[MenuVistasModel::SUBMENU_ID],
            'rol_id' => $this->rol_id
        ]);

        $array_menu = [];
        foreach ($vistas as $key => $vista) {
            $item = new \stdClass;
            $item->vista_id = $vista->vista_id;
            $item->nombre_vista = $vista->nombre_vista;
            $item->link = $vista->link;
            $item->controlador = $vista->controlador;
            $item->submenu_id = $vista->submenu_id;
            $item->nombre_submenu = $vista->nombre_submenu;
            $item->modulo = $vista->modulo;

            array_push($array_menu, $item);
        }

        return $array_menu;
    }
}
