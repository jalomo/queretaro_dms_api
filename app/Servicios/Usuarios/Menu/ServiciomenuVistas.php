<?php

namespace App\Servicios\Usuarios\Menu;

use App\Servicios\Core\ServicioDB;
use App\Models\Usuarios\MenuVistasModel;
use App\Models\Usuarios\RolesVistasModel;
use App\Models\Usuarios\RolModel;

class ServiciomenuVistas extends ServicioDB
{
    public function __construct()
    {
        $this->modelo = new MenuVistasModel();
        $this->rolesVistasModel = new RolesVistasModel();
        $this->recurso = 'submenus';
    }

    public function getReglasGuardar()
    {
        return [
            MenuVistasModel::CONTROLADOR => 'required',
            MenuVistasModel::NOMBRE => 'required',
            MenuVistasModel::SUBMENU_ID => 'required|exists:menu_submenu,id',
            MenuVistasModel::LINK => 'nullable',
            MenuVistasModel::MODULO => 'required'
        ];
    }

    public function getReglasUpdate()
    {
        return [
            MenuVistasModel::CONTROLADOR => 'required',
            MenuVistasModel::NOMBRE => 'required',
            MenuVistasModel::SUBMENU_ID => 'required|exists:menu_submenu,id',
            MenuVistasModel::LINK => 'nullable',
            MenuVistasModel::MODULO => 'required'
        ];
    }

    public function getReglasMenuVista()
    {
        return [
            MenuVistasModel::SUBMENU_ID => 'required|exists:menu_submenu,id'
        ];
    }
    public function buscarTodos() {
        $query = $this->modelo->select(
            'menu_vistas.id',
            'modulo.id as modulo_id',
            'menu_secciones.id as seccion_id',
            'menu_submenu.id as submenu_id',
            'modulo.nombre as modulo_nombre',
            'menu_secciones.nombre as seccion_nombre',
            'menu_submenu.nombre_submenu',
            'menu_vistas.nombre as nombre_vista',
            'menu_vistas.link',
            'menu_vistas.controlador',
            'menu_vistas.modulo',
        );

        $query->join('menu_submenu', 'menu_vistas.submenu_id', '=', 'menu_submenu.id');
        $query->join('menu_secciones', 'menu_submenu.seccion_id', '=', 'menu_secciones.id');
        $query->join('modulo', 'modulo.id', '=', 'menu_secciones.modulo_id');

        if (isset($parametros[MenuVistasModel::SUBMENU_ID])) {
            $query->where(MenuVistasModel::SUBMENU_ID, '=', $parametros[MenuVistasModel::SUBMENU_ID]);
        }

        return  $query->get();
    }

    public function vistasBySubmenuId($parametros)
    {
        $query = $this->modelo->select(
            'menu_vistas.id as vista_id',
            'menu_vistas.nombre as nombre_vista',
            'menu_vistas.link',
            'menu_vistas.controlador',
            'menu_vistas.modulo',
            'menu_submenu.id as submenu_id',
            'menu_submenu.nombre_submenu'
        );

        $query->join('menu_submenu', 'menu_vistas.submenu_id', '=', 'menu_submenu.id');
        $query->join('det_rol_vista', 'det_rol_vista.vista_id', '=', 'menu_vistas.id');


        if (isset($parametros[MenuVistasModel::SUBMENU_ID])) {
            $query->where(MenuVistasModel::SUBMENU_ID, '=', $parametros[MenuVistasModel::SUBMENU_ID]);
        }

        if (isset($parametros['rol_id'])) {
            $query->where('det_rol_vista.rol_id', '=', $parametros['rol_id']);
        }

        return  $query->get();
    }

    public function getReglasVistas()
    {
        return [
            MenuVistasModel::SUBMENU_ID => 'required|exists:menu_submenu,id'
        ];
    }

    public function getVistas($parametros)
    {
        return $this->modelo
            ->where(MenuVistasModel::SUBMENU_ID, '=', $parametros[MenuVistasModel::SUBMENU_ID])
            ->get();
    }

    public function getVistasByRol($parametros)
    {
        $query = $this->modelo->select(
            'menu_vistas.id as vista_id',
            'menu_vistas.nombre as nombre_vista',
        );
        $query->join('det_rol_vista', 'det_rol_vista.vista_id', '=', 'menu_vistas.id');
        $query->join('roles', 'det_rol_vista.rol_id', '=', 'roles.id');

        if (isset($parametros[RolesVistasModel::ROL_ID])) {
            $query->where(RolModel::getTableName() . '.' . RolModel::ID, '=', $parametros[RolesVistasModel::ROL_ID]);
        }
        return  $query->get();
    }

    public function storeVistasByRole($parametros)
    {
        $roles = $parametros[RolesVistasModel::VISTA_ID];
        $this->rolesVistasModel
            ->where(RolesVistasModel::ROL_ID, '=',  $parametros[RolesVistasModel::ROL_ID])
            ->delete();

        foreach ($roles as $key => $item) {
            $this->rolesVistasModel->create([
                RolesVistasModel::ROL_ID => $parametros[RolesVistasModel::ROL_ID],
                RolesVistasModel::VISTA_ID => $item,
            ]);
        }
    }
}
