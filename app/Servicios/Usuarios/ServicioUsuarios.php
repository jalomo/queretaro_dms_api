<?php

namespace App\Servicios\Usuarios;

use App\Exceptions\ParametroHttpInvalidoException;
use App\Servicios\Core\ServicioDB;
use App\Servicios\Usuarios\Log\ServicioLogSessiones;
use App\Models\Usuarios\Log\LogSessionesModel;
use App\Models\Usuarios\User as Usuarios;
use DB;

use Carbon\Carbon;
use Exception;

use Illuminate\Support\Facades\Hash;

class ServicioUsuarios extends ServicioDB
{
    public function __construct()
    {
        $this->modelo = new Usuarios();
        $this->recurso = 'usuarios';
        $this->servicioLogSessiones = new ServicioLogSessiones();
    }

    public function getReglasGuardar()
    {
        return [
            Usuarios::NOMBRE => 'required|max:100',
            Usuarios::APELLIDO_PATERNO => 'required|max:100',
            Usuarios::APELLIDO_MATERNO => 'nullable|max:100',
            Usuarios::TELEFONO => 'nullable',
            Usuarios::EMAIL => 'required|email|unique:usuarios,email',
            Usuarios::RFC => 'required|alpha_num|max:14|unique:usuarios,rfc',
            Usuarios::USUARIO => 'required|unique:usuarios,usuario',
            Usuarios::PASSWORD => 'required|min:6',
            Usuarios::ROL_ID => 'required|exists:roles,id',
            Usuarios::CLAVE_VENDEDOR => 'nullable',
            Usuarios::CLAVE_STARS => 'nullable'
        ];
    }

    public function getReglasUpdate()
    {
        return [
            Usuarios::NOMBRE => 'required|max:100',
            Usuarios::APELLIDO_PATERNO => 'required|max:100',
            Usuarios::APELLIDO_MATERNO => 'nullable|max:100',
            Usuarios::TELEFONO => 'required|numeric',
            Usuarios::EMAIL => 'required|email|unique:usuarios,email',
            Usuarios::RFC => 'nullable|unique:usuarios,rfc',
            Usuarios::ROL_ID => 'nullable|exists:roles,id',
            Usuarios::CLAVE_VENDEDOR => 'nullable',
            Usuarios::CLAVE_STARS => 'nullable'
        ];
    }
    public function getReglaUpdateUser($id) {
        return [
            Usuarios::NOMBRE => 'required|max:100',
            Usuarios::APELLIDO_PATERNO => 'required|max:100',
            Usuarios::APELLIDO_MATERNO => 'nullable|max:100',
            Usuarios::TELEFONO => 'required|numeric',
            Usuarios::EMAIL => 'required|email|unique:usuarios,email,' . $id . ',id',
            Usuarios::RFC => 'nullable|unique:usuarios,rfc,' . $id . ',id',
            Usuarios::ROL_ID => 'nullable|exists:roles,id',
            Usuarios::CLAVE_VENDEDOR => 'nullable',
            Usuarios::CLAVE_STARS => 'nullable'
        ];
    }
    public function getReglaPassword() {
        return [
            Usuarios::PASSWORD => 'required',
        ];
    }

    public function getReglasLogin()
    {
        return [
            Usuarios::USUARIO => "required|string|max:20|exists:usuarios,usuario",
            Usuarios::PASSWORD => "required|string|max:20",
            LogSessionesModel::DIRECCION_IP => "nullable"
        ];
    }

    public function validarCredenciales($credenciales)
    {
        $usuario = $this->existeUsuario($credenciales);
        // if (isset($usuario) && Hash::check($credenciales[Usuarios::PASSWORD], $usuario->password)) {
        //     return $usuario;
        // }

        if (isset($usuario)) {

            $this->servicioLogSessiones->crear([
                LogSessionesModel::USUARIO_ID => $usuario->id,
                LogSessionesModel::DIRECCION_IP => isset($credenciales[LogSessionesModel::DIRECCION_IP]) ? $credenciales[LogSessionesModel::DIRECCION_IP] : '',
                LogSessionesModel::CREATED_AT => date("Y-m-d H:i:s"),
                LogSessionesModel::DESCRIPCION => "INICIO DE SESION"
            ]);

            $tokenResult  = $usuario->createToken('Personal Access Token');
            $token = $tokenResult->token;

            $usuario->expires_at = Carbon::parse($token->expires_at)->toDateTimeString();
            $usuario->access_token = $tokenResult->accessToken;
            $usuario->type_token = 'Bearer';
            return $usuario;
        }


        throw new ParametroHttpInvalidoException([
            'mensaje' => self::$E0003_WRONG_CREDENTIALS
        ]);
    }

    public function update($parametros, $id)
    {
        $data = [
            Usuarios::NOMBRE => $parametros[Usuarios::NOMBRE],
            Usuarios::APELLIDO_PATERNO => $parametros[Usuarios::APELLIDO_PATERNO],
            Usuarios::APELLIDO_MATERNO  => $parametros[Usuarios::APELLIDO_MATERNO],
            Usuarios::TELEFONO  => $parametros[Usuarios::TELEFONO],
            Usuarios::EMAIL  => $parametros[Usuarios::EMAIL],
            Usuarios::RFC => $parametros[Usuarios::RFC],
            Usuarios::ROL_ID => $parametros[Usuarios::ROL_ID],
            Usuarios::CLAVE_VENDEDOR => $parametros[Usuarios::CLAVE_VENDEDOR],
            Usuarios::CLAVE_STARS => $parametros[Usuarios::CLAVE_STARS]
        ];
        return $this->modelo->where(Usuarios::ID, $id)->update($data);
    }

    public function existeUsuario($credenciales)
    {
        return $this->modelo
            ->where(Usuarios::USUARIO, $credenciales[Usuarios::USUARIO])
            ->where(Usuarios::PASSWORD, $credenciales[Usuarios::PASSWORD])
            ->first();
    }

    public function getAll()
    {
        return $this->createQuery([]);
    }
    public function getUserByRoles($parametros){
        return $this->createQuery(['rol_id'=>$parametros['rol_id']]);
    }
    public function createQuery($parametros){
        $query = $this->modelo->select(
            'usuarios.id',
            'usuarios.nombre',
            'usuarios.apellido_paterno',
            'usuarios.apellido_materno',
            'usuarios.telefono',
            'usuarios.usuario',
            'usuarios.email',
            'usuarios.estatus_id',
            'usuarios.rfc',
            'usuarios.activo',
            'usuarios.ultimo_acceso',
            'usuarios.email_verified_at',
            'usuarios.api_token',
            'usuarios.ip_adress',
            'usuarios.session_activa',
            'usuarios.remember_token',
            'usuarios.rol_id',
            'roles.rol'
        )->join('roles', 'usuarios.rol_id', '=', 'roles.id');
        if(isset($parametros['rol_id'])){
            $query->where(Usuarios::ROL_ID,$parametros['rol_id']);
        }
        return $query->get();
    }
}
