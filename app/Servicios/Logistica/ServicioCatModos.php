<?php
namespace App\Servicios\Logistica;
use App\Servicios\Core\ServicioDB;
use App\Models\Logistica\CatModosModel;

class ServicioCatModos extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catálogo modos logística';
        $this->modelo = new CatModosModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatModosModel::MODO => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatModosModel::MODO => 'required'
        ];
    }
}


