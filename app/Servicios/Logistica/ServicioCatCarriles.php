<?php
namespace App\Servicios\Logistica;
use App\Servicios\Core\ServicioDB;
use App\Models\Logistica\CatCarrilesModel;

class ServicioCatCarriles extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catálogo carriles';
        $this->modelo = new CatCarrilesModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatCarrilesModel::CARRIL => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatCarrilesModel::CARRIL => 'required'
        ];
    }
}


