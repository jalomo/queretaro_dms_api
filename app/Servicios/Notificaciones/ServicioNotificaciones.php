<?php

namespace App\Servicios\Notificaciones;

use App\Models\Notificaciones\NotificacionesModel;
use App\Models\Usuarios\User;
use App\Servicios\Core\ServicioDB;

class ServicioNotificaciones extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'notificaciones';
        $this->modelo = new NotificacionesModel();
    }

    public function getReglasGuardar()
    {
        return [
            NotificacionesModel::FECHA_NOTIFICACION => 'required',
            NotificacionesModel::ASUNTO => 'required',
            NotificacionesModel::TEXTO => 'required',
            NotificacionesModel::LEIDA => 'nullable',
            NotificacionesModel::ELIMINADA => 'nullable',
            NotificacionesModel::USER_ID => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            NotificacionesModel::FECHA_NOTIFICACION => 'required',
            NotificacionesModel::ASUNTO => 'required',
            NotificacionesModel::TEXTO => 'required',
            NotificacionesModel::LEIDA => 'nullable',
            NotificacionesModel::ELIMINADA => 'nullable',
            NotificacionesModel::USER_ID => 'required'
        ];
    }
    public function getAll($parametros)
    {
        $query = $this->modelo->select(
            NotificacionesModel::getTableName() . '.' . NotificacionesModel::FECHA_NOTIFICACION,
            NotificacionesModel::getTableName() . '.' . NotificacionesModel::ASUNTO,
            NotificacionesModel::getTableName() . '.' . NotificacionesModel::TEXTO,
            NotificacionesModel::getTableName() . '.' . NotificacionesModel::LEIDA,
            NotificacionesModel::getTableName() . '.' . NotificacionesModel::ELIMINADA,
            NotificacionesModel::getTableName() . '.' . NotificacionesModel::USER_ID,
            NotificacionesModel::getTableName() . '.' . NotificacionesModel::CREATED_AT,
            User::getTableName() . '.' . User::NOMBRE . ' AS nombre_usuario',
            User::getTableName() . '.' . User::APELLIDO_PATERNO . ' AS ap_usuario',
            User::getTableName() . '.' . User::APELLIDO_MATERNO . ' AS am_usuario',
            User::getTableName() . '.' . User::EMAIL . ' AS email_usuario',
        )
            ->join(
                User::getTableName(),
                User::getTableName() . '.' . User::ID,
                '=',
                NotificacionesModel::getTableName() . '.' . NotificacionesModel::USER_ID
            )
            ->orderBy(
                NotificacionesModel::getTableName() . '.' . NotificacionesModel::CREATED_AT,
                'desc',
            )
            ->where(
                NotificacionesModel::getTableName() . '.' . NotificacionesModel::ELIMINADA,
                0
            );
        if (isset($parametros[NotificacionesModel::ID])) {
            $query->where(
                NotificacionesModel::ID,
                $parametros[NotificacionesModel::ID]
            );
        }
        if (isset($parametros[NotificacionesModel::USER_ID])) {
            $query->where(
                NotificacionesModel::USER_ID,
                $parametros[NotificacionesModel::USER_ID]
            );
        }
        if (isset($parameters['fecha_inicio']) && isset($parameters['fecha_fin'])) {
            $query->where(
                NotificacionesModel::getTableName() . '.' . NotificacionesModel::CREATED_AT,
                '>=',
                $parameters['fecha_inicio']
            );
            $query->where(
                NotificacionesModel::getTableName() . '.' . NotificacionesModel::CREATED_AT,
                '<=',
                $parameters['fecha_fin']
            );
        }
        return $query->get();
    }
}
