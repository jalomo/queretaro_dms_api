<?php

namespace App\Servicios\Autos\UnidadesNuevas;

use App\Models\Autos\UnidadesNuevas\EquipoEspecialRemisionModel;
use App\Servicios\Core\ServicioDB;

class ServicioEquipoEspecial extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'equipo especial remision';
        $this->modelo = new EquipoEspecialRemisionModel();
    }

    public function getReglasGuardar()
    {
        return [
            EquipoEspecialRemisionModel::DESCRIPCION => 'required',
            EquipoEspecialRemisionModel::IMPORTE => 'required',
            EquipoEspecialRemisionModel::REMISION_ID => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            EquipoEspecialRemisionModel::DESCRIPCION => 'required',
            EquipoEspecialRemisionModel::IMPORTE => 'required',
            EquipoEspecialRemisionModel::REMISION_ID => 'required'
        ];
    }
    public function getByRemision($id){
        return $this->getWhere(EquipoEspecialRemisionModel::REMISION_ID, $id);
    }
}
