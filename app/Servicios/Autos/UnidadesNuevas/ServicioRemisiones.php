<?php

namespace App\Servicios\Autos\UnidadesNuevas;

use App\Models\Autos\EstatusSalidaUnidadModel;
use App\Models\Autos\UnidadesNuevas\CatAduanasUNModel;
use App\Models\Autos\UnidadesNuevas\CatCombustible;
use App\Models\Autos\UnidadesNuevas\CatEstatusUNModel;
use App\Models\Autos\UnidadesNuevas\CatLineas;
use App\Models\Autos\UnidadesNuevas\CatStatusRemision;
use App\Models\Autos\UnidadesNuevas\ConsecutivosModel;
use App\Models\Autos\UnidadesNuevas\DetalleCostosRemisionModel;
use App\Models\Autos\UnidadesNuevas\DetalleRemisionModel;
use App\Models\Autos\UnidadesNuevas\FacturacionModel;
use App\Models\Autos\UnidadesNuevas\MemoCompraModel;
use App\Models\Autos\UnidadesNuevas\RemisionModel;
use App\Models\Logs\Pilot\LogsPilotModel;
use App\Models\Refacciones\CatalogoColoresModel;
use App\Models\Refacciones\CatalogoUbicacionModel;
use App\Models\Refacciones\UbicacionLLavesModel;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\ServicioDB;
use App\Servicios\Facturas\ServicioXmlFactura;
use App\Servicios\Pilot\ServicioPilot;
use App\Servicios\Refacciones\ServicioCatalogoColores;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use SimpleXMLElement;

class ServicioRemisiones extends ServicioDB
{

    public function __construct()
    {
        $this->recurso = 'unidades';
        $this->modelo = new RemisionModel();
        $this->modeloDetalleRemisionModel = new DetalleRemisionModel();
        $this->modeloCatColores = new CatalogoColoresModel();
        $this->modeloCatCombustible = new CatCombustible();
        $this->modeloDetalleCostosRemision = new DetalleCostosRemisionModel();
        $this->modelEstatusSalidaUnidad = new EstatusSalidaUnidadModel();
        $this->modelMemoCompras = new MemoCompraModel();
        $this->modelFacturacion = new FacturacionModel();
        $this->modelUbicaciones = new CatalogoUbicacionModel();
        $this->modelUbicacionesLlaves = new UbicacionLLavesModel();
        $this->modelAduana = new CatAduanasUNModel();
        $this->modelStatusRemision = new CatStatusRemision();

        $this->servicio_detalle_costos = new ServicioDetalleCostosRemisiones();
        $this->servicio_detalle_remision = new ServicioDetalleRemision();
        $this->servicio_memocompras = new ServicioMemoCompra();
        $this->servicio_facturacion = new ServicioFacturacion();
        $this->servicio_consecutivos = new ServicioConsecutivosUN();
        $this->servicio_aduana = new ServicioCatAduanas();
        $this->modeloLineas = new CatLineas();
        $this->modelo_estatus = new CatEstatusUNModel();
        $this->servicio_colores = new ServicioCatalogoColores();
        $this->servicioXml = new ServicioXmlFactura();
        $this->servicioCatLineas = new ServicioCatLineas();
    }

    public function getReglasGuardar()
    {
        return [
            RemisionModel::LINEA_ID => 'required',
            RemisionModel::UNIDAD_DESCRIPCION => 'required',
            RemisionModel::CAT => 'required',
            RemisionModel::CLAVE_VEHICULAR => 'required',
            RemisionModel::ECONOMICO => 'nullable',
            RemisionModel::ECONOMICO_VISUAL => 'nullable',
            RemisionModel::UNIDAD_IMPORTADA => 'required',
            RemisionModel::TIPO_AUTO => 'required',
            RemisionModel::CLAVE_ISAN => 'nullable',
            RemisionModel::CTA_MENUDO => 'nullable',
            RemisionModel::CTA_FLOTILLA => 'nullable',
            RemisionModel::CTA_CONAUTO => 'nullable',
            RemisionModel::CTA_INTERCAMBIO => 'nullable',
            RemisionModel::CTA_PLLENO => 'nullable',
            RemisionModel::CTA_INVENTARIO => 'nullable',
            RemisionModel::VTA_MENUDEO => 'nullable',
            RemisionModel::VTA_FLOTILLA => 'nullable',
            RemisionModel::VTA_CONAUTO => 'nullable',
            RemisionModel::VTA_INTERCAMBIO => 'nullable',
            RemisionModel::VTA_PLLENO => 'nullable',
            RemisionModel::USERID => 'required',
            RemisionModel::C_SUBTOTAL => 'required',
            RemisionModel::C_TOTAL => 'required',
            RemisionModel::C_IVA => 'required',
            RemisionModel::FECHA_PEDIMENTO => 'nullable',
            RemisionModel::PEDIMENTO => 'nullable',
            RemisionModel::FECHA_REMISION => 'required',
            RemisionModel::SERIE => 'required',
            RemisionModel::SERIE_CORTA => 'required',
            RemisionModel::MOTOR => 'required',
            RemisionModel::INTERCAMBIO => 'required',
            RemisionModel::PROVEEDOR_ID => 'required',
            RemisionModel::ESTATUS_ID => 'nullable',
            RemisionModel::UBICACION_ID => 'required',
            RemisionModel::LEYENDA_DCTO => 'required',
            RemisionModel::NO_INVENTARIO => 'nullable',
            RemisionModel::CLAVE_SAT => 'nullable',
            RemisionModel::UNIDAD => 'nullable',
            RemisionModel::MODELO => 'nullable',
            RemisionModel::CUENTA_INVENTARIO_UNIDAD => 'required',
            RemisionModel::NACIONAL_IMPORTADO => 'required',
            RemisionModel::LINEA_ID_CONTABILIDAD => 'required',
            RemisionModel::UNIQUE => 'nullable',
            // DETALLE COSTOS REMISIONES
            DetalleCostosRemisionModel::C_VALOR_UNIDAD => 'required',
            DetalleCostosRemisionModel::C_EQUIPO_BASE => 'required',
            DetalleCostosRemisionModel::C_TOTAL_BASE => 'nullable',
            DetalleCostosRemisionModel::C_DEDUCCION_FORD => 'nullable',
            DetalleCostosRemisionModel::C_SEG_TRASLADO => 'nullable',
            DetalleCostosRemisionModel::C_GASTOS_TRASLADO => 'required',
            DetalleCostosRemisionModel::C_IMP_IMPORT => 'nullable',
            DetalleCostosRemisionModel::C_FLETES_EXT => 'nullable',
            DetalleCostosRemisionModel::C_ISAN => 'nullable',
            DetalleCostosRemisionModel::C_HOLDBACK => 'required',
            DetalleCostosRemisionModel::C_CUENTA_PUB => 'required',
            DetalleCostosRemisionModel::C_DONATIVO_CCF => 'required',
            DetalleCostosRemisionModel::C_PLAN_PISO => 'required',
            DetalleCostosRemisionModel::C_CUENTA_PUB => 'required',
            DetalleCostosRemisionModel::C_DONATIVO_CCF => 'required',
            DetalleCostosRemisionModel::C_PLAN_PISO => 'required',
            //Detalle remisión
            DetalleRemisionModel::PUERTAS => 'required',
            DetalleRemisionModel::CILINDROS => 'required',
            DetalleRemisionModel::TRANSMISION => 'required',
            DetalleRemisionModel::COMBUSTIBLE_ID => 'required',
            DetalleRemisionModel::CAPACIDAD => 'required',
            DetalleRemisionModel::CAPACIDAD_KG => 'required',
            DetalleRemisionModel::COLORINTID => 'required',
            DetalleRemisionModel::COLOREXTID => 'required',
            //Memo compras
            // MemoCompraModel::MES => 'required',
            // MemoCompraModel::NO_PRODUCTO => 'required',
            // MemoCompraModel::MEMO => 'required',
            // MemoCompraModel::COSTO_REMISION => 'required',
            // MemoCompraModel::COMPRAR => 'required',
            // MemoCompraModel::MES_COMPRA => 'required',
            // MemoCompraModel::FP => 'required',
            // MemoCompraModel::DIAS => 'required',
            // MemoCompraModel::CM => 'required',
            //Facturación
            FacturacionModel::PROCEDENCIA => 'required',
            FacturacionModel::VENDEDOR => 'required',
            FacturacionModel::ADUANA_ID => 'required',
            FacturacionModel::REPUVE => 'required',
            //Consecutivos
            ConsecutivosModel::ANIO => 'required',
            ConsecutivosModel::CAT_CONSECUTIVO => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            RemisionModel::LINEA_ID => 'required',
            RemisionModel::UNIDAD_DESCRIPCION => 'required',
            RemisionModel::CAT => 'required',
            RemisionModel::CLAVE_VEHICULAR => 'required',
            RemisionModel::ECONOMICO => 'nullable',
            RemisionModel::ECONOMICO_VISUAL => 'nullable',
            RemisionModel::UNIDAD_IMPORTADA => 'required',
            RemisionModel::TIPO_AUTO => 'required',
            RemisionModel::CLAVE_ISAN => 'nullable',
            RemisionModel::CTA_MENUDO => 'nullable',
            RemisionModel::CTA_FLOTILLA => 'nullable',
            RemisionModel::CTA_CONAUTO => 'nullable',
            RemisionModel::CTA_INTERCAMBIO => 'nullable',
            RemisionModel::CTA_PLLENO => 'nullable',
            RemisionModel::CTA_INVENTARIO => 'nullable',
            RemisionModel::VTA_MENUDEO => 'nullable',
            RemisionModel::VTA_FLOTILLA => 'nullable',
            RemisionModel::VTA_CONAUTO => 'nullable',
            RemisionModel::VTA_INTERCAMBIO => 'nullable',
            RemisionModel::VTA_PLLENO => 'nullable',
            RemisionModel::USERID => 'required',
            RemisionModel::C_SUBTOTAL => 'required',
            RemisionModel::C_TOTAL => 'required',
            RemisionModel::C_IVA => 'required',
            RemisionModel::FECHA_PEDIMENTO => 'nullable',
            RemisionModel::PEDIMENTO => 'nullable',
            RemisionModel::FECHA_REMISION => 'required',
            RemisionModel::SERIE => 'required',
            RemisionModel::SERIE_CORTA => 'required',
            RemisionModel::MOTOR => 'required',
            RemisionModel::INTERCAMBIO => 'required',
            RemisionModel::PROVEEDOR_ID => 'required',
            RemisionModel::ESTATUS_ID => 'nullable',
            RemisionModel::UBICACION_ID => 'required',
            RemisionModel::LEYENDA_DCTO => 'required',
            RemisionModel::NO_INVENTARIO => 'nullable',
            RemisionModel::CLAVE_SAT => 'nullable',
            RemisionModel::UNIDAD => 'nullable',
            RemisionModel::MODELO => 'nullable',
            RemisionModel::CUENTA_INVENTARIO_UNIDAD => 'required',
            RemisionModel::NACIONAL_IMPORTADO => 'required',
            RemisionModel::LINEA_ID_CONTABILIDAD => 'required',
            RemisionModel::UNIQUE => 'nullable',
            // DETALLE COSTOS REMISIONES
            DetalleCostosRemisionModel::C_VALOR_UNIDAD => 'required',
            DetalleCostosRemisionModel::C_EQUIPO_BASE => 'required',
            DetalleCostosRemisionModel::C_TOTAL_BASE => 'nullable',
            DetalleCostosRemisionModel::C_DEDUCCION_FORD => 'nullable',
            DetalleCostosRemisionModel::C_SEG_TRASLADO => 'nullable',
            DetalleCostosRemisionModel::C_GASTOS_TRASLADO => 'required',
            DetalleCostosRemisionModel::C_IMP_IMPORT => 'nullable',
            DetalleCostosRemisionModel::C_FLETES_EXT => 'nullable',
            DetalleCostosRemisionModel::C_ISAN => 'nullable',
            DetalleCostosRemisionModel::C_HOLDBACK => 'required',
            DetalleCostosRemisionModel::C_CUENTA_PUB => 'required',
            DetalleCostosRemisionModel::C_DONATIVO_CCF => 'required',
            DetalleCostosRemisionModel::C_PLAN_PISO => 'required',
            DetalleCostosRemisionModel::C_CUENTA_PUB => 'required',
            DetalleCostosRemisionModel::C_DONATIVO_CCF => 'required',
            DetalleCostosRemisionModel::C_PLAN_PISO => 'required',
            //Detalle remisión
            DetalleRemisionModel::PUERTAS => 'required',
            DetalleRemisionModel::CILINDROS => 'required',
            DetalleRemisionModel::TRANSMISION => 'required',
            DetalleRemisionModel::COMBUSTIBLE_ID => 'required',
            DetalleRemisionModel::CAPACIDAD => 'required',
            DetalleRemisionModel::CAPACIDAD_KG => 'required',
            DetalleRemisionModel::COLORINTID => 'required',
            DetalleRemisionModel::COLOREXTID => 'required',
            //Memo compras
            // MemoCompraModel::MES => 'required',
            // MemoCompraModel::NO_PRODUCTO => 'required',
            // MemoCompraModel::MEMO => 'required',
            // MemoCompraModel::COSTO_REMISION => 'required',
            // MemoCompraModel::COMPRAR => 'required',
            // MemoCompraModel::MES_COMPRA => 'required',
            // MemoCompraModel::FP => 'required',
            // MemoCompraModel::DIAS => 'required',
            // MemoCompraModel::CM => 'required',
            //Facturación
            FacturacionModel::PROCEDENCIA => 'required',
            FacturacionModel::VENDEDOR => 'required',
            FacturacionModel::ADUANA_ID => 'required',
            FacturacionModel::REPUVE => 'required',
            //Consecutivos
            ConsecutivosModel::ANIO => 'required',
            ConsecutivosModel::CAT_CONSECUTIVO => 'required'
        ];
    }
    public function getConsecutivosInventario(Request $request)
    {
        $consecutivo = $this->servicio_consecutivos->getConsecutivo($request->all(), 1);

        $arrayCat = explode('-', $request->get(ConsecutivosModel::CAT_CONSECUTIVO));
        $cat_consecutivo_explode = isset($arrayCat[0]) ? $arrayCat[0] : $request->get(ConsecutivosModel::CAT_CONSECUTIVO);
        // //Crear consecutivo
        $consecutivoCreated = $this->servicio_consecutivos->crear([
            ConsecutivosModel::ANIO => $request->get(RemisionModel::MODELO),
            ConsecutivosModel::CAT_CONSECUTIVO => $cat_consecutivo_explode,
            ConsecutivosModel::TIPO => 1,
            ConsecutivosModel::CONSECUTIVO => $consecutivo,
        ]);
        $lastIdConsecutivo = $consecutivoCreated->id;
        $cat_sub = substr($cat_consecutivo_explode, 0, 2);
        switch (strlen($consecutivo)) {
            case 1:
                $consecutivo = '00000' . $consecutivo;
                break;
            case 2:
                $consecutivo = '0000' . $consecutivo;
                break;
            case 3:
                $consecutivo = '000' . $consecutivo;
                break;
            case 4:
                $consecutivo = '00' . $consecutivo;
                break;
            case 5:
                $consecutivo = '0' . $consecutivo;
                break;

            default:
                # code...
                break;
        }
        $no_inventario = $request->get(RemisionModel::MODELO) . $cat_sub . $consecutivo . $lastIdConsecutivo;
        return $no_inventario;
    }
    public function getConsecutivosEconomico(Request $request)
    {
        $consecutivo = $this->servicio_consecutivos->getConsecutivo($request->all(), 2);

        $arrayCat = explode('-', $request->get(ConsecutivosModel::CAT_CONSECUTIVO));
        $cat_consecutivo_explode = isset($arrayCat[0]) ? $arrayCat[0] : $request->get(ConsecutivosModel::CAT_CONSECUTIVO);
        // //Crear consecutivo
        $consecutivoCreated = $this->servicio_consecutivos->crear([
            ConsecutivosModel::ANIO => $request->get(RemisionModel::MODELO),
            ConsecutivosModel::CAT_CONSECUTIVO => $cat_consecutivo_explode,
            ConsecutivosModel::TIPO => 2,
            ConsecutivosModel::CONSECUTIVO => $consecutivo,
        ]);
        $lastIdConsecutivo = $consecutivoCreated->id;
        $cat_sub = substr($cat_consecutivo_explode, 0, 2);
        switch (strlen($consecutivo)) {
            case 1:
                $consecutivo = '00' . $consecutivo;
                break;
            case 2:
                $consecutivo = '00' . $consecutivo;
                break;
            case 3:
                $consecutivo = '00' . $consecutivo;
                break;
            case 4:
                $consecutivo = '00' . $consecutivo;
                break;
            case 5:
                $consecutivo = '00' . $consecutivo;
                break;

            default:
                return $consecutivo;
                break;
        }
        $economico =  $cat_sub . $consecutivo . $lastIdConsecutivo;
        return $economico;
    }
    public function  store(Request $request)
    {
        ParametrosHttpValidador::validar($request, $this->getReglasGuardar());
        $no_inventario = $this->getConsecutivosInventario($request);
        $no_economico = $this->getConsecutivosEconomico($request);
        // $economico = $consecutivos[0];
        // $no_inventario = $consecutivos[1];
        $remision = $this->crear([
            RemisionModel::LINEA_ID => $request->get(RemisionModel::LINEA_ID),
            RemisionModel::UNIDAD_DESCRIPCION => str_replace('\"', '', $request->get(RemisionModel::UNIDAD_DESCRIPCION)),
            RemisionModel::CAT => $request->get(RemisionModel::CAT),
            RemisionModel::CLAVE_VEHICULAR => $request->get(RemisionModel::CLAVE_VEHICULAR),
            RemisionModel::ECONOMICO => $no_economico,
            RemisionModel::ECONOMICO_VISUAL => $request->get(RemisionModel::ECONOMICO_VISUAL),
            RemisionModel::UNIDAD_IMPORTADA => $request->get(RemisionModel::UNIDAD_IMPORTADA),
            RemisionModel::TIPO_AUTO => $request->get(RemisionModel::TIPO_AUTO),
            RemisionModel::CLAVE_ISAN => $request->get(RemisionModel::CLAVE_ISAN),
            RemisionModel::CTA_MENUDO => $request->get(RemisionModel::CTA_MENUDO),
            RemisionModel::CTA_FLOTILLA => $request->get(RemisionModel::CTA_FLOTILLA),
            RemisionModel::CTA_CONAUTO => $request->get(RemisionModel::CTA_CONAUTO),
            RemisionModel::CTA_INTERCAMBIO => $request->get(RemisionModel::CTA_INTERCAMBIO),
            RemisionModel::CTA_PLLENO => $request->get(RemisionModel::CTA_PLLENO),
            RemisionModel::CTA_INVENTARIO => $request->get(RemisionModel::CTA_INVENTARIO),
            RemisionModel::VTA_MENUDEO => $request->get(RemisionModel::VTA_MENUDEO),
            RemisionModel::VTA_FLOTILLA => $request->get(RemisionModel::VTA_FLOTILLA),
            RemisionModel::VTA_CONAUTO => $request->get(RemisionModel::VTA_CONAUTO),
            RemisionModel::VTA_INTERCAMBIO => $request->get(RemisionModel::VTA_INTERCAMBIO),
            RemisionModel::VTA_PLLENO => $request->get(RemisionModel::VTA_PLLENO),
            RemisionModel::USERID => $request->get(RemisionModel::USERID),
            RemisionModel::C_SUBTOTAL => $request->get(RemisionModel::C_SUBTOTAL),
            RemisionModel::C_TOTAL => $request->get(RemisionModel::C_TOTAL),
            RemisionModel::C_IVA => $request->get(RemisionModel::C_IVA),
            RemisionModel::FECHA_PEDIMENTO => $request->get(RemisionModel::FECHA_PEDIMENTO),
            RemisionModel::PEDIMENTO => $request->get(RemisionModel::PEDIMENTO),
            RemisionModel::FECHA_REMISION => $request->get(RemisionModel::FECHA_REMISION),
            RemisionModel::SERIE => $request->get(RemisionModel::SERIE),
            RemisionModel::SERIE_CORTA => $request->get(RemisionModel::SERIE_CORTA),
            RemisionModel::MOTOR => $request->get(RemisionModel::MOTOR),
            RemisionModel::INTERCAMBIO => $request->get(RemisionModel::INTERCAMBIO),
            RemisionModel::PROVEEDOR_ID => $request->get(RemisionModel::PROVEEDOR_ID),
            RemisionModel::ESTATUS_ID => $request->get(RemisionModel::ESTATUS_ID),
            RemisionModel::UBICACION_ID => $request->get(RemisionModel::UBICACION_ID),
            RemisionModel::LEYENDA_DCTO => $request->get(RemisionModel::LEYENDA_DCTO),
            RemisionModel::NO_INVENTARIO => $no_inventario,
            RemisionModel::CLAVE_SAT => $request->get(RemisionModel::CLAVE_SAT),
            RemisionModel::UNIDAD => $request->get(RemisionModel::UNIDAD),
            RemisionModel::MODELO => $request->get(RemisionModel::MODELO),
            RemisionModel::CUENTA_INVENTARIO_UNIDAD => $request->get(RemisionModel::CUENTA_INVENTARIO_UNIDAD),
            RemisionModel::NACIONAL_IMPORTADO => $request->get(RemisionModel::NACIONAL_IMPORTADO),
            RemisionModel::LINEA_ID_CONTABILIDAD => $request->get(RemisionModel::LINEA_ID_CONTABILIDAD),
            RemisionModel::UNIQUE => $request->get(RemisionModel::UNIQUE),
        ]);

        //Costos de la remisión
        $this->servicio_detalle_costos->crear([
            DetalleCostosRemisionModel::C_VALOR_UNIDAD => $request->get(DetalleCostosRemisionModel::C_VALOR_UNIDAD),
            DetalleCostosRemisionModel::C_EQUIPO_BASE => $request->get(DetalleCostosRemisionModel::C_EQUIPO_BASE),
            DetalleCostosRemisionModel::C_TOTAL_BASE => $request->get(DetalleCostosRemisionModel::C_TOTAL_BASE),
            DetalleCostosRemisionModel::C_DEDUCCION_FORD => $request->get(DetalleCostosRemisionModel::C_DEDUCCION_FORD),
            DetalleCostosRemisionModel::C_SEG_TRASLADO => $request->get(DetalleCostosRemisionModel::C_SEG_TRASLADO),
            DetalleCostosRemisionModel::C_GASTOS_TRASLADO => $request->get(DetalleCostosRemisionModel::C_GASTOS_TRASLADO),
            DetalleCostosRemisionModel::C_IMP_IMPORT => $request->get(DetalleCostosRemisionModel::C_IMP_IMPORT),
            DetalleCostosRemisionModel::C_FLETES_EXT => $request->get(DetalleCostosRemisionModel::C_FLETES_EXT),
            // DetalleCostosRemisionModel::C_ISAN => $request->get(DetalleCostosRemisionModel::C_ISAN),
            DetalleCostosRemisionModel::C_HOLDBACK => $request->get(DetalleCostosRemisionModel::C_HOLDBACK),
            DetalleCostosRemisionModel::C_CUENTA_PUB => $request->get(DetalleCostosRemisionModel::C_CUENTA_PUB),
            DetalleCostosRemisionModel::C_DONATIVO_CCF => $request->get(DetalleCostosRemisionModel::C_DONATIVO_CCF),
            DetalleCostosRemisionModel::C_PLAN_PISO => $request->get(DetalleCostosRemisionModel::C_PLAN_PISO),
            DetalleCostosRemisionModel::REMISIONID => $remision->id
        ]);

        //Detalle de la remisión
        $this->servicio_detalle_remision->crear([
            DetalleRemisionModel::PUERTAS => $request->get(DetalleRemisionModel::PUERTAS),
            DetalleRemisionModel::CILINDROS => $request->get(DetalleRemisionModel::CILINDROS),
            DetalleRemisionModel::TRANSMISION => $request->get(DetalleRemisionModel::TRANSMISION),
            DetalleRemisionModel::COMBUSTIBLE_ID => $request->get(DetalleRemisionModel::COMBUSTIBLE_ID),
            DetalleRemisionModel::CAPACIDAD => $request->get(DetalleRemisionModel::CAPACIDAD),
            DetalleRemisionModel::CAPACIDAD_KG => $request->get(DetalleRemisionModel::CAPACIDAD_KG),
            DetalleRemisionModel::COLORINTID => $request->get(DetalleRemisionModel::COLORINTID),
            DetalleRemisionModel::COLOREXTID => $request->get(DetalleRemisionModel::COLOREXTID),
            DetalleRemisionModel::REMISIONID => $remision->id
        ]);
        //Memo compras 
        // $this->servicio_memocompras->crear([
        //     MemoCompraModel::MES => $request->get(MemoCompraModel::MES),
        //     MemoCompraModel::NO_PRODUCTO => $request->get(MemoCompraModel::NO_PRODUCTO),
        //     MemoCompraModel::MEMO => $request->get(MemoCompraModel::MEMO),
        //     MemoCompraModel::COSTO_REMISION => $request->get(MemoCompraModel::COSTO_REMISION),
        //     MemoCompraModel::COMPRAR => $request->get(MemoCompraModel::COMPRAR),
        //     MemoCompraModel::MES_COMPRA => $request->get(MemoCompraModel::MES_COMPRA),
        //     MemoCompraModel::FP => $request->get(MemoCompraModel::FP),
        //     MemoCompraModel::DIAS => $request->get(MemoCompraModel::DIAS),
        //     MemoCompraModel::CM => $request->get(MemoCompraModel::CM),
        //     MemoCompraModel::REMISION_ID => $remision->id
        // ]);
        //Facturación
        $this->servicio_facturacion->crear([
            FacturacionModel::PROCEDENCIA => $request->get(FacturacionModel::PROCEDENCIA),
            FacturacionModel::VENDEDOR => $request->get(FacturacionModel::VENDEDOR),
            FacturacionModel::ADUANA_ID => $request->get(FacturacionModel::ADUANA_ID),
            FacturacionModel::REPUVE => $request->get(FacturacionModel::REPUVE),
            FacturacionModel::REMISION_ID => $remision->id
        ]);

        //Pilot

        $color = $this->servicio_colores->getById($request->get(DetalleRemisionModel::COLOREXTID));
        $linea = $this->servicioCatLineas->getById($request->get(RemisionModel::LINEA_ID));
        $data = [
            'factory_request_number' => $no_economico,
            'integration_reference_code' => (string)$remision->id,
            'availability_status_code' => '1',
            'type_code' => 'vn',
            'brand' => str_replace('\"', '', $request->get(RemisionModel::UNIDAD_DESCRIPCION)),
            'model' => $linea->descripcion,
            'version' => 'N/A',
            'factory_code' => $request->get(RemisionModel::CAT),
            'vin' => $request->get(RemisionModel::SERIE),
            'color' => $color->nombre,
            'year' => $request->get('anio'),
            'received_flag' => 0 //Flag que identifica si el vehículo se encuentra físicamente en stock. Valores posibles: 0 = Vehículo no recibido ; 1 = Vehículo físico
        ];
        //guardar id pilot "id": "9AACF9D9-CA56-4C81-9CA3-2281C3CB74AB",
        $servicioPilot = new ServicioPilot();
        $response = $servicioPilot->CreateUnidad($data, LogsPilotModel::TYPE_CREATE_UNIDAD);
        $response = json_decode($response);
        if ($response->result->status == 'success') {
            $this->massUpdateWhereId(RemisionModel::ID, $remision->id, [
                RemisionModel::PILOT_ID => $response->result->entitydata->id,
            ]);
        }
        return $remision;
    }
    public function  update(Request $request, $id)
    {

        ParametrosHttpValidador::validar($request, $this->getReglasUpdate());
        $remision = $this->massUpdateWhereId(RemisionModel::ID, $id, [
            RemisionModel::LINEA_ID => $request->get(RemisionModel::LINEA_ID),
            RemisionModel::UNIDAD_DESCRIPCION => str_replace('\"', '', $request->get(RemisionModel::UNIDAD_DESCRIPCION)),
            RemisionModel::CAT => $request->get(RemisionModel::CAT),
            RemisionModel::CLAVE_VEHICULAR => $request->get(RemisionModel::CLAVE_VEHICULAR),
            RemisionModel::ECONOMICO_VISUAL => $request->get(RemisionModel::ECONOMICO_VISUAL),
            RemisionModel::UNIDAD_IMPORTADA => $request->get(RemisionModel::UNIDAD_IMPORTADA),
            RemisionModel::TIPO_AUTO => $request->get(RemisionModel::TIPO_AUTO),
            RemisionModel::CLAVE_ISAN => $request->get(RemisionModel::CLAVE_ISAN),
            RemisionModel::CTA_MENUDO => $request->get(RemisionModel::CTA_MENUDO),
            RemisionModel::CTA_FLOTILLA => $request->get(RemisionModel::CTA_FLOTILLA),
            RemisionModel::CTA_CONAUTO => $request->get(RemisionModel::CTA_CONAUTO),
            RemisionModel::CTA_INTERCAMBIO => $request->get(RemisionModel::CTA_INTERCAMBIO),
            RemisionModel::CTA_PLLENO => $request->get(RemisionModel::CTA_PLLENO),
            RemisionModel::CTA_INVENTARIO => $request->get(RemisionModel::CTA_INVENTARIO),
            RemisionModel::VTA_MENUDEO => $request->get(RemisionModel::VTA_MENUDEO),
            RemisionModel::VTA_FLOTILLA => $request->get(RemisionModel::VTA_FLOTILLA),
            RemisionModel::VTA_CONAUTO => $request->get(RemisionModel::VTA_CONAUTO),
            RemisionModel::VTA_INTERCAMBIO => $request->get(RemisionModel::VTA_INTERCAMBIO),
            RemisionModel::VTA_PLLENO => $request->get(RemisionModel::VTA_PLLENO),
            RemisionModel::USERID => $request->get(RemisionModel::USERID),
            RemisionModel::C_SUBTOTAL => $request->get(RemisionModel::C_SUBTOTAL),
            RemisionModel::C_IVA => $request->get(RemisionModel::C_IVA),
            RemisionModel::C_TOTAL => $request->get(RemisionModel::C_TOTAL),
            RemisionModel::FECHA_PEDIMENTO => $request->get(RemisionModel::FECHA_PEDIMENTO),
            RemisionModel::PEDIMENTO => $request->get(RemisionModel::PEDIMENTO),
            RemisionModel::FECHA_REMISION => $request->get(RemisionModel::FECHA_REMISION),
            RemisionModel::SERIE => $request->get(RemisionModel::SERIE),
            RemisionModel::SERIE_CORTA => $request->get(RemisionModel::SERIE_CORTA),
            RemisionModel::MOTOR => $request->get(RemisionModel::MOTOR),
            RemisionModel::INTERCAMBIO => $request->get(RemisionModel::INTERCAMBIO),
            RemisionModel::PROVEEDOR_ID => $request->get(RemisionModel::PROVEEDOR_ID),
            RemisionModel::ESTATUS_ID => $request->get(RemisionModel::ESTATUS_ID),
            RemisionModel::UBICACION_ID => $request->get(RemisionModel::UBICACION_ID),
            RemisionModel::LEYENDA_DCTO => $request->get(RemisionModel::LEYENDA_DCTO),
            RemisionModel::CLAVE_SAT => $request->get(RemisionModel::CLAVE_SAT),
            RemisionModel::UNIDAD => $request->get(RemisionModel::UNIDAD),
            RemisionModel::MODELO => $request->get(RemisionModel::MODELO),
            RemisionModel::CUENTA_INVENTARIO_UNIDAD => $request->get(RemisionModel::CUENTA_INVENTARIO_UNIDAD),
            RemisionModel::NACIONAL_IMPORTADO => $request->get(RemisionModel::NACIONAL_IMPORTADO),
            RemisionModel::LINEA_ID_CONTABILIDAD => $request->get(RemisionModel::LINEA_ID_CONTABILIDAD),
            RemisionModel::UNIQUE => $request->get(RemisionModel::UNIQUE),
        ]);
        //Actualizar costos remisión
        $this->servicio_detalle_costos->massUpdateWhereId(DetalleCostosRemisionModel::REMISIONID, $id, [
            DetalleCostosRemisionModel::C_VALOR_UNIDAD => $request->get(DetalleCostosRemisionModel::C_VALOR_UNIDAD),
            DetalleCostosRemisionModel::C_EQUIPO_BASE => $request->get(DetalleCostosRemisionModel::C_EQUIPO_BASE),
            DetalleCostosRemisionModel::C_TOTAL_BASE => $request->get(DetalleCostosRemisionModel::C_TOTAL_BASE),
            DetalleCostosRemisionModel::C_DEDUCCION_FORD => $request->get(DetalleCostosRemisionModel::C_DEDUCCION_FORD),
            DetalleCostosRemisionModel::C_SEG_TRASLADO => $request->get(DetalleCostosRemisionModel::C_SEG_TRASLADO),
            DetalleCostosRemisionModel::C_GASTOS_TRASLADO => $request->get(DetalleCostosRemisionModel::C_GASTOS_TRASLADO),
            DetalleCostosRemisionModel::C_IMP_IMPORT => $request->get(DetalleCostosRemisionModel::C_IMP_IMPORT),
            DetalleCostosRemisionModel::C_FLETES_EXT => $request->get(DetalleCostosRemisionModel::C_FLETES_EXT),
            // DetalleCostosRemisionModel::C_ISAN => $request->get(DetalleCostosRemisionModel::C_ISAN),
            DetalleCostosRemisionModel::C_HOLDBACK => $request->get(DetalleCostosRemisionModel::C_HOLDBACK),
            DetalleCostosRemisionModel::C_CUENTA_PUB => $request->get(DetalleCostosRemisionModel::C_CUENTA_PUB),
            DetalleCostosRemisionModel::C_DONATIVO_CCF => $request->get(DetalleCostosRemisionModel::C_DONATIVO_CCF),
            DetalleCostosRemisionModel::C_PLAN_PISO => $request->get(DetalleCostosRemisionModel::C_PLAN_PISO)
        ]);

        $this->servicio_detalle_remision->massUpdateWhereId(DetalleRemisionModel::REMISIONID, $id, [
            DetalleRemisionModel::PUERTAS => $request->get(DetalleRemisionModel::PUERTAS),
            DetalleRemisionModel::CILINDROS => $request->get(DetalleRemisionModel::CILINDROS),
            DetalleRemisionModel::TRANSMISION => $request->get(DetalleRemisionModel::TRANSMISION),
            DetalleRemisionModel::COMBUSTIBLE_ID => $request->get(DetalleRemisionModel::COMBUSTIBLE_ID),
            DetalleRemisionModel::CAPACIDAD => $request->get(DetalleRemisionModel::CAPACIDAD),
            DetalleRemisionModel::CAPACIDAD_KG => $request->get(DetalleRemisionModel::CAPACIDAD_KG),
            DetalleRemisionModel::COLORINTID => $request->get(DetalleRemisionModel::COLORINTID),
            DetalleRemisionModel::COLOREXTID => $request->get(DetalleRemisionModel::COLOREXTID)
        ]);
        //Memo compras 
        // $this->servicio_memocompras->massUpdateWhereId(MemoCompraModel::REMISION_ID, $id, [
        //     MemoCompraModel::MES => $request->get(MemoCompraModel::MES),
        //     MemoCompraModel::NO_PRODUCTO => $request->get(MemoCompraModel::NO_PRODUCTO),
        //     MemoCompraModel::MEMO => $request->get(MemoCompraModel::MEMO),
        //     MemoCompraModel::COSTO_REMISION => $request->get(MemoCompraModel::COSTO_REMISION),
        //     MemoCompraModel::COMPRAR => $request->get(MemoCompraModel::COMPRAR),
        //     MemoCompraModel::MES_COMPRA => $request->get(MemoCompraModel::MES_COMPRA),
        //     MemoCompraModel::FP => $request->get(MemoCompraModel::FP),
        //     MemoCompraModel::DIAS => $request->get(MemoCompraModel::DIAS),
        //     MemoCompraModel::CM => $request->get(MemoCompraModel::CM)
        // ]);

        $this->servicio_facturacion->massUpdateWhereId(FacturacionModel::REMISION_ID, $id, [
            FacturacionModel::PROCEDENCIA => $request->get(FacturacionModel::PROCEDENCIA),
            FacturacionModel::VENDEDOR => $request->get(FacturacionModel::VENDEDOR),
            FacturacionModel::ADUANA_ID => $request->get(FacturacionModel::ADUANA_ID),
            FacturacionModel::REPUVE => $request->get(FacturacionModel::REPUVE)
        ]);
        $currentUnidad = $this->getById($id);
        // Pilot
        $color = $this->servicio_colores->getById($request->get(DetalleRemisionModel::COLOREXTID));
        $linea = $this->servicioCatLineas->getById($request->get(RemisionModel::LINEA_ID));
        $data = [
            'brand' => str_replace('\"', '', $request->get(RemisionModel::UNIDAD_DESCRIPCION)),
            'model' => $linea->descripcion,
            'version' => 'N/A',
            'factory_code' => $request->get(RemisionModel::CAT),
            'vin' => $request->get(RemisionModel::SERIE),
            'color' => $color->nombre,
            'year' => $request->get('anio'),
            'received_flag' => 0 //Flag que identifica si el vehículo se encuentra físicamente en stock. Valores posibles: 0 = Vehículo no recibido ; 1 = Vehículo físico
        ];
        if ($currentUnidad->pilot_id) {
            $data['id'] = $currentUnidad->pilot_id;
            $servicioPilot = new ServicioPilot();
            $response = $servicioPilot->UpdateUnidad($data, LogsPilotModel::TYPE_UPDATE_UNIDAD);
            $response = json_decode($response);
            if ($response->result->status == 'success') {
                $this->massUpdateWhereId(RemisionModel::ID, $id, [
                    RemisionModel::PILOT_ID => $response->result->entitydata->id,
                ]);
            }
        } else {

            $data['factory_request_number'] = $request->get(RemisionModel::ECONOMICO);
            $data['integration_reference_code'] = (string)$id;
            $data['availability_status_code'] = '1';
            $data['type_code'] = 'vn';
            $servicioPilot = new ServicioPilot();
            $response = $servicioPilot->CreateUnidad($data, LogsPilotModel::TYPE_CREATE_UNIDAD);
            $response = json_decode($response);
            if ($response->result->status == 'success') {
                $this->massUpdateWhereId(RemisionModel::ID, $id, [
                    RemisionModel::PILOT_ID => $response->result->entitydata->id,
                ]);
            }
        }
        //Facturación
        return $remision;
    }
    //Update only some fields 
    public function updateRemision($request, $id)
    {
        $data = [];
        if (isset($request[RemisionModel::FECHA_RECEPCION])) {
            $data[RemisionModel::FECHA_RECEPCION] = $request[RemisionModel::FECHA_RECEPCION];
        }
        if (isset($request[RemisionModel::ESTATUS_ID])) {
            $data[RemisionModel::ESTATUS_ID] = $request[RemisionModel::ESTATUS_ID];
        }
        if (isset($request[RemisionModel::UBICACION_ID])) {
            $data[RemisionModel::UBICACION_ID] = $request[RemisionModel::UBICACION_ID];
        }
        if (isset($request[RemisionModel::UBICACION_LLAVES_ID])) {
            $data[RemisionModel::UBICACION_LLAVES_ID] = $request[RemisionModel::UBICACION_LLAVES_ID];
        }
        if (isset($request[RemisionModel::COMENTARIO])) {
            $data[RemisionModel::COMENTARIO] = $request[RemisionModel::COMENTARIO];
        }
        if (isset($request[RemisionModel::ULTIMO_SERVICIO])) {
            $data[RemisionModel::ULTIMO_SERVICIO] = $request[RemisionModel::ULTIMO_SERVICIO];
        }
        if (isset($request[RemisionModel::ESTATUS_REMISION_ID])) {
            $data[RemisionModel::ESTATUS_REMISION_ID] = $request[RemisionModel::ESTATUS_REMISION_ID];
        }
        return $this->massUpdateWhereId(RemisionModel::ID, $id, $data);
    }
    public function getUnidades(Request $request)
    {
        // $parametros->merge(['page' => floor($parametros->get('start') / ($parametros->get('length') ?: 1)) + 1]);
        // $request->merge(['page' => floor($request->get('start') / ($request->get('length') ?: 1)) + 1]);
        $request->merge(['page' => floor($request->start / ($request->length ?: 1)) + 1]);

        $tabla_unidades = $this->modelo->getTable();
        $tabla_lineas = $this->modeloLineas->getTable();
        $tabla_detalleRemision = $this->modeloDetalleRemisionModel->getTable();
        $tabla_cat_colores = $this->modeloCatColores->getTable();
        $tabla_cat_combustible = $this->modeloCatCombustible->getTable();
        $tabla_detalle_costo_remision = $this->modeloDetalleCostosRemision->getTable();
        $tabla_estatus_remision = $this->modelo_estatus->getTable();
        $tabla_estatus_salida_unidad = $this->modelEstatusSalidaUnidad->getTable();
        // $tabla_memo_compras = $this->modelMemoCompras->getTable();
        $tabla_facturacion = $this->modelFacturacion->getTable();
        $tabla_ubicaciones = $this->modelUbicaciones->getTable();
        $tabla_ubicacion_llaves = $this->modelUbicacionesLlaves->getTable();
        $tabla_aduana = $this->modelAduana->getTable();
        $tabla_status_remision = $this->modelStatusRemision->getTable();

        //
        //servicio_aduana

        // DetalleCostosRemisionModel
        //aqui poner los joins
        $query = $this->modelo->select(
            $tabla_unidades . '.' . RemisionModel::ID,
            $tabla_lineas . '.' . CatLineas::ID . ' as modelo_id',
            $tabla_lineas . '.' . CatLineas::MODELO,
            $tabla_lineas . '.' . CatLineas::DESCRIPCION . ' as modelo_descripcion',
            $tabla_lineas . '.' . CatLineas::LINEA,
            // $tabla_estatus_remision. '.' . CatEstatusUNModel::ESTATUS,
            $tabla_unidades . '.' . str_replace('\"', '', RemisionModel::UNIDAD_DESCRIPCION),
            $tabla_unidades . '.' . RemisionModel::CAT,
            $tabla_unidades . '.' . RemisionModel::CLAVE_VEHICULAR,
            $tabla_unidades . '.' . RemisionModel::ECONOMICO,
            $tabla_unidades . '.' . RemisionModel::ECONOMICO_VISUAL,
            $tabla_unidades . '.' . RemisionModel::UNIDAD_IMPORTADA,
            $tabla_unidades . '.' . RemisionModel::TIPO_AUTO,
            $tabla_unidades . '.' . RemisionModel::CLAVE_ISAN,
            $tabla_unidades . '.' . RemisionModel::CTA_MENUDO,
            $tabla_unidades . '.' . RemisionModel::CTA_FLOTILLA,
            $tabla_unidades . '.' . RemisionModel::CTA_CONAUTO,
            $tabla_unidades . '.' . RemisionModel::CTA_INTERCAMBIO,
            $tabla_unidades . '.' . RemisionModel::CTA_PLLENO,
            $tabla_unidades . '.' . RemisionModel::CTA_INVENTARIO,
            $tabla_unidades . '.' . RemisionModel::VTA_MENUDEO,
            $tabla_unidades . '.' . RemisionModel::VTA_FLOTILLA,
            $tabla_unidades . '.' . RemisionModel::VTA_CONAUTO,
            $tabla_unidades . '.' . RemisionModel::VTA_INTERCAMBIO,
            $tabla_unidades . '.' . RemisionModel::VTA_PLLENO,
            $tabla_unidades . '.' . RemisionModel::USERID,
            $tabla_unidades . '.' . RemisionModel::C_SUBTOTAL,
            $tabla_unidades . '.' . RemisionModel::C_TOTAL,
            $tabla_unidades . '.' . RemisionModel::C_IVA,
            $tabla_unidades . '.' . RemisionModel::ULTIMO_SERVICIO,
            $tabla_unidades . '.' . RemisionModel::LINEA_ID,
            $tabla_unidades . '.' . RemisionModel::CREATED_AT,
            $tabla_unidades . '.' . RemisionModel::DISPONIBLE,
            $tabla_unidades . '.' . RemisionModel::UNIQUE,
            $tabla_detalle_costo_remision . '.' . DetalleCostosRemisionModel::C_VALOR_UNIDAD . ' as precio_costo',
            $tabla_detalle_costo_remision . '.' . DetalleCostosRemisionModel::C_VALOR_UNIDAD,
            $tabla_detalle_costo_remision . '.' . DetalleCostosRemisionModel::C_EQUIPO_BASE,
            $tabla_detalle_costo_remision . '.' . DetalleCostosRemisionModel::C_TOTAL_BASE,
            $tabla_detalle_costo_remision . '.' . DetalleCostosRemisionModel::C_DEDUCCION_FORD,
            $tabla_detalle_costo_remision . '.' . DetalleCostosRemisionModel::C_SEG_TRASLADO,
            $tabla_detalle_costo_remision . '.' . DetalleCostosRemisionModel::C_GASTOS_TRASLADO,
            $tabla_detalle_costo_remision . '.' . DetalleCostosRemisionModel::C_IMP_IMPORT,
            $tabla_detalle_costo_remision . '.' . DetalleCostosRemisionModel::C_FLETES_EXT,
            $tabla_detalle_costo_remision . '.' . DetalleCostosRemisionModel::C_HOLDBACK,
            $tabla_detalle_costo_remision . '.' . DetalleCostosRemisionModel::C_CUENTA_PUB,
            $tabla_detalle_costo_remision . '.' . DetalleCostosRemisionModel::C_DONATIVO_CCF,
            $tabla_detalle_costo_remision . '.' . DetalleCostosRemisionModel::C_PLAN_PISO,
            $tabla_unidades . '.' . RemisionModel::C_TOTAL . ' as precio_venta',
            $tabla_unidades . '.' . RemisionModel::FECHA_PEDIMENTO,
            $tabla_unidades . '.' . RemisionModel::PEDIMENTO,
            $tabla_unidades . '.' . RemisionModel::FECHA_REMISION,
            $tabla_unidades . '.' . RemisionModel::SERIE,
            $tabla_unidades . '.' . RemisionModel::SERIE_CORTA,
            $tabla_unidades . '.' . RemisionModel::MOTOR,
            $tabla_unidades . '.' . RemisionModel::INTERCAMBIO,
            $tabla_unidades . '.' . RemisionModel::PROVEEDOR_ID,
            $tabla_unidades . '.' . RemisionModel::UBICACION_ID,
            $tabla_unidades . '.' . RemisionModel::UBICACION_LLAVES_ID,
            $tabla_unidades . '.' . RemisionModel::ESTATUS_ID,
            $tabla_unidades . '.' . RemisionModel::COMENTARIO,
            $tabla_unidades . '.' . RemisionModel::LEYENDA_DCTO,
            $tabla_unidades . '.' . RemisionModel::NO_INVENTARIO,
            $tabla_unidades . '.' . RemisionModel::CLAVE_SAT,
            $tabla_unidades . '.' . RemisionModel::UNIDAD,
            $tabla_unidades . '.' . RemisionModel::MODELO,
            $tabla_unidades . '.' . RemisionModel::FECHA_RECEPCION,
            $tabla_unidades . '.' . RemisionModel::LINEA_ID_CONTABILIDAD,
            $tabla_unidades . '.' . RemisionModel::CUENTA_INVENTARIO_UNIDAD,
            $tabla_estatus_salida_unidad . '.' . EstatusSalidaUnidadModel::NOMBRE . ' as estatus_unidad',

            $tabla_detalleRemision . '.' . DetalleRemisionModel::PUERTAS,
            $tabla_detalleRemision . '.' . DetalleRemisionModel::CILINDROS,
            $tabla_detalleRemision . '.' . DetalleRemisionModel::TRANSMISION,

            // $tabla_detalleRemision . '.' . DetalleRemisionModel::COMBUSTIBLE_ID,
            $tabla_detalleRemision . '.' . DetalleRemisionModel::CAPACIDAD,
            $tabla_detalleRemision . '.' . DetalleRemisionModel::CAPACIDAD_KG,
            $tabla_cat_combustible . '.' . CatCombustible::DESCRIPCION . ' as combustible',

            'color_interior' . '.' . CatalogoColoresModel::ID . ' as id_color_interior',
            'color_interior' . '.' . CatalogoColoresModel::NOMBRE . ' as color_interior',
            'color_exterior' . '.' . CatalogoColoresModel::ID . ' as id_color_exterior',
            'color_exterior' . '.' . CatalogoColoresModel::NOMBRE . ' as color_exterior',
            // $tabla_memo_compras . '.' . MemoCompraModel::CM,
            // $tabla_memo_compras . '.' . MemoCompraModel::MES,
            // $tabla_memo_compras . '.' . MemoCompraModel::NO_PRODUCTO,
            // $tabla_memo_compras . '.' . MemoCompraModel::MEMO,
            // $tabla_memo_compras . '.' . MemoCompraModel::COSTO_REMISION,
            // $tabla_memo_compras . '.' . MemoCompraModel::COMPRAR,
            // $tabla_memo_compras . '.' . MemoCompraModel::MES_COMPRA,
            // $tabla_memo_compras . '.' . MemoCompraModel::FP,
            // $tabla_memo_compras . '.' . MemoCompraModel::DIAS,
            $tabla_facturacion . '.' . FacturacionModel::PROCEDENCIA,
            $tabla_facturacion . '.' . FacturacionModel::VENDEDOR,
            $tabla_facturacion . '.' . FacturacionModel::ADUANA_ID,
            $tabla_facturacion . '.' . FacturacionModel::REPUVE,
            $tabla_ubicaciones . '.' . CatalogoUbicacionModel::NOMBRE . ' as ubicacion',
            $tabla_ubicacion_llaves . '.' . CatalogoUbicacionModel::NOMBRE . ' as ubicacion_llaves',
            $tabla_aduana . '.' . CatAduanasUNModel::ADUANA . ' as aduana',
            $tabla_status_remision . '.' . CatStatusRemision::ESTATUS . ' as estatus_remision',
            $tabla_unidades . '.' . RemisionModel::NACIONAL_IMPORTADO,
            $tabla_unidades . '.' . RemisionModel::DISPONIBLE   . ' as unidad_disponible',
            $tabla_unidades . '.' . RemisionModel::ESTATUS_ID_PILOT   . ' as estatus_id_pilot',
            $tabla_estatus_remision . '.' . CatEstatusUNModel::ESTATUS   . ' as estatus_unidad_pilot',
            $tabla_unidades . '.' . RemisionModel::PILOT_ID   . ' as pilot_id',


        )->from($tabla_unidades);
        $query->Leftjoin($tabla_lineas, $tabla_lineas . '.id', '=', $tabla_unidades . '.' . RemisionModel::LINEA_ID);
        $query->Leftjoin($tabla_estatus_remision, $tabla_estatus_remision . '.id', '=', $tabla_unidades . '.' . RemisionModel::ESTATUS_ID_PILOT);
        $query->Leftjoin($tabla_detalleRemision, $tabla_detalleRemision . '.' . DetalleRemisionModel::REMISIONID, '=', $tabla_unidades . '.' . RemisionModel::ID);
        $query->Leftjoin($tabla_detalle_costo_remision, $tabla_detalle_costo_remision . '.' . DetalleCostosRemisionModel::REMISIONID, '=', $tabla_unidades . '.' . RemisionModel::ID);

        $query->Leftjoin($tabla_cat_colores . ' as color_interior', 'color_interior.' . CatalogoColoresModel::ID, '=', $tabla_detalleRemision . '.' . DetalleRemisionModel::COLORINTID);
        $query->Leftjoin($tabla_cat_colores . ' as color_exterior', 'color_exterior.' . CatalogoColoresModel::ID, '=', $tabla_detalleRemision . '.' . DetalleRemisionModel::COLOREXTID);
        $query->Leftjoin($tabla_cat_combustible, $tabla_cat_combustible . '.' . CatCombustible::ID, '=', $tabla_detalleRemision . '.' . DetalleRemisionModel::COMBUSTIBLE_ID);
        $query->Leftjoin($tabla_estatus_salida_unidad, $tabla_estatus_salida_unidad . '.' . EstatusSalidaUnidadModel::ID, '=', $tabla_unidades . '.' . RemisionModel::ESTATUS_ID);
        // $query->Leftjoin($tabla_memo_compras, $tabla_memo_compras . '.' . MemoCompraModel::REMISION_ID, '=', $tabla_unidades . '.' . RemisionModel::ID);
        $query->Leftjoin($tabla_facturacion, $tabla_facturacion . '.' . FacturacionModel::REMISION_ID, '=', $tabla_unidades . '.' . RemisionModel::ID);
        $query->Leftjoin($tabla_ubicaciones, $tabla_ubicaciones . '.' . CatalogoUbicacionModel::ID, '=', $tabla_unidades . '.' . RemisionModel::UBICACION_ID);
        $query->Leftjoin($tabla_ubicacion_llaves, $tabla_ubicacion_llaves . '.' . UbicacionLLavesModel::ID, '=', $tabla_unidades . '.' . RemisionModel::UBICACION_LLAVES_ID);
        $query->Leftjoin($tabla_aduana, $tabla_aduana . '.' . CatAduanasUNModel::ID, '=', $tabla_facturacion . '.' . FacturacionModel::ADUANA_ID);
        $query->Leftjoin($tabla_status_remision, $tabla_status_remision . '.' . CatStatusRemision::ID, '=', $tabla_unidades . '.' . RemisionModel::ESTATUS_REMISION_ID);

        if ($request->get(RemisionModel::SERIE)) {
            $query->where(RemisionModel::SERIE, $request->get(RemisionModel::SERIE));
        }
        if ($request->get(RemisionModel::ECONOMICO)) {
            $query->where(RemisionModel::ECONOMICO, $request->get(RemisionModel::ECONOMICO));
        }
        if ($request->get(RemisionModel::ECONOMICO_VISUAL)) {
            $query->where(RemisionModel::ECONOMICO_VISUAL, $request->get(RemisionModel::ECONOMICO_VISUAL));
        }
        if ($request->get('recepcion')) {
            $query->where(RemisionModel::FECHA_RECEPCION, '<>', NULL);
        }

        if ($request->get(RemisionModel::ESTATUS_ID)) {
            $query->where($tabla_unidades . '.' . RemisionModel::ESTATUS_ID, '=', $request->get(RemisionModel::ESTATUS_ID));
        }
        if ($request->get(RemisionModel::ESTATUS_REMISION_ID)) {
            $query->where($tabla_unidades . '.' . RemisionModel::ESTATUS_REMISION_ID, '=', $request->get(RemisionModel::ESTATUS_REMISION_ID));
        }
        // if (isset($parametros[RemisionModel::FECHA_RECEPCION])) {
        //     $query->orWhere(function($orquery) {
        //         $orquery->where(RemisionModel::FECHA_RECEPCION,'<>',NULL)
        //                 ->where(RemisionModel::FECHA_RECEPCION,'2021-09-01');
        //     });
        // }
        if ($request->get(RemisionModel::ID)) {
            $query->where($tabla_unidades . '.' . RemisionModel::ID, $request->get(RemisionModel::ID));
        }
        if ($request->get(RemisionModel::PILOT_ID)) {
            $query->where($tabla_unidades . '.' . RemisionModel::PILOT_ID, $request->get(RemisionModel::PILOT_ID));
        }
        // $query->limit(200);
        if ($request->get('pagination')) {
            $consulta =  $query->paginate(10);
        } else {
            $consulta =  $query->get();
        }
        return $consulta;
    }

    public function getunidadesNuevas($parameters)
    {
        return $this->getUnidades(new Request);
    }

    public function getunidadesSemiNuevas()
    {
        /*return $this->getUnidades([
            // 'id_estado' => UnidadesModel::TIPO_SEMI_NUEVO
        ]);*/
    }

    public function getUnidadSeminuevaById($id)
    {
        /*return $this->getUnidades([
            'id' => $id,
            // 'id_estado' => UnidadesModel::TIPO_SEMI_NUEVO
        ]);*/
    }

    public function getUnidadById($id)
    {
        $request = new Request([
            RemisionModel::ID => $id
        ]);
        return $this->getUnidades($request);
    }

    public function getTotalUnidades($parametros)
    {
        $tabla_unidades = $this->modelo->getTable();
        $tabla_detalleRemision = $this->modeloDetalleRemisionModel->getTable();
        $tabla_detalle_costo_remision = $this->modeloDetalleCostosRemision->getTable();

        $query = $this->modelo->select(
            DB::raw('sum(' . $tabla_detalle_costo_remision . '.' . DetalleCostosRemisionModel::C_VALOR_UNIDAD . ') as total_precio_costo'),
            DB::raw('sum(' . $tabla_unidades . '.' . RemisionModel::C_TOTAL . ') as total_precio_venta')
        )->from($tabla_unidades);
        $query->join($tabla_detalleRemision, $tabla_detalleRemision . '.' . DetalleRemisionModel::ID, '=', $tabla_unidades . '.' . RemisionModel::ID);
        $query->join($tabla_detalle_costo_remision, $tabla_detalle_costo_remision . '.' . DetalleCostosRemisionModel::REMISIONID, '=', $tabla_unidades . '.' . RemisionModel::ID);

        if (isset($parametros['id_estado'])) {
            // $query->where('unidades.id_estado', $parametros['id_estado']);
        }

        return $query->get();
    }

    public function getTotalUnidadesNuevas()
    {
        return $this->getTotalUnidades([
            // 'id_estado' => UnidadesModel::TIPO_NUEVO
        ]);
    }

    public function getTotalUnidadesSemiNuevas()
    {
        return $this->getTotalUnidades([
            // 'id_estado' => UnidadesModel::TIPO_SEMI_NUEVO
        ]);
    }

    public function handleDataXmlRemision($path_factura = '')
    {
        if ($path_factura == '') {
            return [];
        }
        $real_path = realpath('../storage/app' . $path_factura);
        $xml = $this->getRemisionXml($real_path);
        $emisor_factura_auto = $xml->xpath('//fdd:AddendaEmisorFactAuto')[0]->attributes();
        // $domicilio_receptor = $xml->xpath('//adddomrec:AddendaDomicilioReceptor')[0]->attributes();
        $concepto0 = $xml->xpath('//c:Concepto')[0]->attributes();
        $concepto1 = $xml->xpath('//c:Concepto')[1]->attributes();
        $concepto2 = $xml->xpath('//c:Concepto')[2]->attributes();
        $concepto3 = $xml->xpath('//c:Concepto')[3]->attributes();
        $comprobante = $xml->xpath('//c:Comprobante')[0]->attributes();
        $traslado0 = $xml->xpath('//cfdi:Traslado')[0]->attributes();
        $traslado1 = $xml->xpath('//cfdi:Traslado')[1]->attributes();
        $traslado2 = $xml->xpath('//cfdi:Traslado')[2]->attributes();
        if ($xml->xpath('//ventavehiculos:InformacionAduanera')) {
            $pedimento = $xml->xpath('//ventavehiculos:InformacionAduanera')[0]->attributes();
        } else {
            $pedimento = [];
        }
        $Complementos = $xml->xpath('//ventavehiculos:Parte');
        $Conceptos = $xml->xpath('//c:Concepto');
        $total_impuestos = 0;
        foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Impuestos') as $Impuestos) {
            $total_impuestos = $Impuestos['TotalImpuestosTrasladados'];
        }

        $valor_unidad = '';
        $seguro_plan_piso = '';
        $motor = '';
        $transmision = '';

        foreach ($Complementos as $c => $complemento) {
            if ($complemento['descripcion'] == 'VALOR DE LA UNIDAD') {
                $valor_unidad = $complemento['valorUnitario'];
            }
            if ($complemento['descripcion'] == 'SEGURO DE PLAN PISO') {
                $seguro_plan_piso = $complemento['valorUnitario'];
            }
            if (str_contains(strtoupper($complemento['descripcion']), 'MOTOR') == true) {
                $motor = $complemento['descripcion'];
            }
            if (str_contains(strtoupper($complemento['descripcion']), 'TRANSMISION') == true || str_contains(strtoupper($complemento['descripcion']), 'TRANS') == true) {
                $transmision = $complemento['descripcion'];
            }
        }

        $gastos_traslado = '';
        $holdback = '';
        $donativo_ccf = '';
        foreach ($Conceptos as $c => $con) {
            if ($con['ClaveProdServ'] == '81141601') {
                $gastos_traslado = $con['Importe'];
            }
            if ($con['ClaveProdServ'] == '94101500') {
                $holdback = $con['Importe'];
            }
            if ($con['ClaveProdServ'] == '84101600') {
                $donativo_ccf = $con['Importe'];
            }
        }
        $rdata =  [
            'emisor' => $this->getAttrsEmisor($emisor_factura_auto),
            // 'domicilio_receptor' => $this->getAttrsDomicilioReceptor($domicilio_receptor),
            'concepto0' => $this->getAttrsConceptos($concepto0),
            'concepto1' => $this->getAttrsConceptos($concepto1),
            'concepto2' => $this->getAttrsConceptos($concepto2),
            'concepto3' => $this->getAttrsConceptos($concepto3),
            'traslado0' => $this->getAttrsTraslados($traslado0),
            'traslado1' => $this->getAttrsTraslados($traslado1),
            'traslado2' => $this->getAttrsTraslados($traslado2),
            'comprobante' => $this->getComprobante($comprobante),
            'valor_unidad' => $valor_unidad,
            'seguro_plan_piso' => $seguro_plan_piso,
            'transmision' => $transmision,
            'motor' => $motor,
            'gastos_traslado' => $gastos_traslado,
            'holdback' => $holdback,
            'donativo_ccf' => $donativo_ccf,
            'total_impuestos' => $total_impuestos,
        ];
        if (!empty($pedimento)) {
            $rdata['pedimento'] = $this->getPedimento($pedimento);
        }
        return $rdata;
        // return $xml;
    }

    public function getRemisionxml($path_factura = '')
    {
        if ($path_factura == '') {
            return [];
        }

        $xml = simplexml_load_file($path_factura);
        $ns = $xml->getNamespaces(true);
        $xml->registerXPathNamespace('c', $ns['cfdi']);
        $xml->registerXPathNamespace('fdd', $ns['FORDADD']);
        $xml->registerXPathNamespace('venta_vehiculos', $ns['ventavehiculos']);
        $xml->registerXPathNamespace('tfd', $ns['tfd']);
        if (isset($ns['terceros'])) {
            $xml->registerXPathNamespace('terceros', $ns['terceros']);
        }
        if (isset($ns['impuestos'])) {
            $xml->registerXPathNamespace('impuestos', $ns['impuestos']);
        }
        // $xml->registerXPathNamespace('adddomrec', $ns['adddomrec']);

        return $xml;
    }

    public function getAttrsEmisor($parameters)
    {

        return [
            "bid" => $this->servicioXml->validateAndCastItem($parameters->BID),
            "anioModelo" => $this->servicioXml->validateAndCastItem($parameters->anioModelo),
            "claveVehicular" => $this->servicioXml->validateAndCastItem($parameters->claveVehicular),
            "colorExterior" => $this->validarColor($this->servicioXml->validateAndCastItem($parameters->colorExterior)),
            "colorInterior" => $this->validarColor($this->servicioXml->validateAndCastItem($parameters->colorInterior)),
            "cuotaPubHdbackAMDF" => $this->servicioXml->validateAndCastItem($parameters->cuotaPubHdbackAMDF),
            "detalleUnidadAccesorios" => $this->servicioXml->validateAndCastItem($parameters->detalleUnidadAccesorios),
            "donativoCCFyAMDF" => $this->servicioXml->validateAndCastItem($parameters->donativoCCFyAMDF),
            "financiera" => $this->servicioXml->validateAndCastItem($parameters->financiera),
            "leyendaDocto" => $this->servicioXml->validateAndCastItem($parameters->leyendaDocto),
            "moneda" => $this->servicioXml->validateAndCastItem($parameters->moneda),
            "montoLetra" => $this->servicioXml->validateAndCastItem($parameters->montoLetra),
            "noPedido" => $this->servicioXml->validateAndCastItem($parameters->noPedido),
            "servAdmonLogistica" => $this->servicioXml->validateAndCastItem($parameters->servAdmonLogistica),
            "tipoCompra" => $this->servicioXml->validateAndCastItem($parameters->tipoCompra),
            "vendedor" => $this->servicioXml->validateAndCastItem($parameters->vendedor)
        ];
    }

    public function getAttrsDomicilioReceptor($parameters)
    {
        return [
            "calle" => $this->servicioXml->validateAndCastItem($parameters["calle"]),
            "noExterior" => $this->servicioXml->validateAndCastItem($parameters["noExterior"]),
            "localidad" => $this->servicioXml->validateAndCastItem($parameters["localidad"]),
            "municipio" => $this->servicioXml->validateAndCastItem($parameters["municipio"]),
            "estado" => $this->servicioXml->validateAndCastItem($parameters["estado"]),
            "pais" => $this->servicioXml->validateAndCastItem($parameters["pais"]),
            "codigoPostal" => $this->servicioXml->validateAndCastItem($parameters["codigoPostal"])
        ];
    }

    public function getAttrsDomicilioReceptor2($parameters)
    {
        return [
            "calle" => $this->servicioXml->validateAndCastItem($parameters["calle"]),
            "noExterior" => $this->servicioXml->validateAndCastItem($parameters["noExterior"]),
            "localidad" => $this->servicioXml->validateAndCastItem($parameters["localidad"]),
            "municipio" => $this->servicioXml->validateAndCastItem($parameters["municipio"]),
            "estado" => $this->servicioXml->validateAndCastItem($parameters["estado"]),
            "pais" => $this->servicioXml->validateAndCastItem($parameters["pais"]),
            "codigoPostal" => $this->servicioXml->validateAndCastItem($parameters["codigoPostal"])
        ];
    }
    public function getAttrsConceptos($parameters)
    {
        return [
            "ClaveProdServ" => $this->servicioXml->validateAndCastItem($parameters["ClaveProdServ"]),
            "NoIdentificacion" => $this->servicioXml->validateAndCastItem($parameters["NoIdentificacion"]),
            "Cantidad" => $this->servicioXml->validateAndCastItem($parameters["Cantidad"]),
            "ClaveUnidad" => $this->servicioXml->validateAndCastItem($parameters["ClaveUnidad"]),
            "Unidad" => $this->servicioXml->validateAndCastItem($parameters["Unidad"]),
            "Descripcion" => $this->servicioXml->validateAndCastItem($parameters["Descripcion"]),
            "ValorUnitario" => $this->servicioXml->validateAndCastItem($parameters["ValorUnitario"]),
            "Importe" => $this->servicioXml->validateAndCastItem($parameters["Importe"])
        ];
    }
    public function getComprobante($parameters)
    {
        return [
            "Version" => $this->servicioXml->validateAndCastItem($parameters["Version"]),
            "Serie" => $this->servicioXml->validateAndCastItem($parameters["Serie"]),
            "Fecha" => $this->servicioXml->validateAndCastItem($parameters["Fecha"]),
            "Moneda" => $this->servicioXml->validateAndCastItem($parameters["Moneda"]),
            "TipoCambio" => $this->servicioXml->validateAndCastItem($parameters["TipoCambio"]),
            "SubTotal" => $this->servicioXml->validateAndCastItem($parameters["SubTotal"]),
            "Total" => $this->servicioXml->validateAndCastItem($parameters["Total"]),
            "FormaPago" => $this->servicioXml->validateAndCastItem($parameters["FormaPago"]),
            "CondicionesDePago" => $this->servicioXml->validateAndCastItem($parameters["CondicionesDePago"]),
            "TipoDeComprobante" => $this->servicioXml->validateAndCastItem($parameters["TipoDeComprobante"]),
            "MetodoPago" => $this->servicioXml->validateAndCastItem($parameters["MetodoPago"]),
            "LugarExpedicion" => $this->servicioXml->validateAndCastItem($parameters["LugarExpedicion"]),
            "NoCertificado" => $this->servicioXml->validateAndCastItem($parameters["NoCertificado"]),
            "Folio" => $this->servicioXml->validateAndCastItem($parameters["Folio"])
        ];
    }
    public function getPedimento($parameters)
    {
        return [
            "numero" => $this->servicioXml->validateAndCastItem($parameters["numero"]),
            "fecha" => $this->servicioXml->validateAndCastItem($parameters["fecha"]),
        ];
    }
    public function getImpuestos($parameters)
    {
        print_r($parameters["TotalImpuestosTrasladados"]);

        return [
            "total_impuestos" => isset($parameters[3]) ?  $this->servicioXml->validateAndCastItem($parameters[3]["TotalImpuestosTrasladados"]) : 0
        ];
    }
    public function getAttrsTraslados($parameters)
    {
        return [
            "Base" => $this->servicioXml->validateAndCastItem($parameters["Base"]),
            "Impuesto" => $this->servicioXml->validateAndCastItem($parameters["Impuesto"]),
            "TipoFactor" => $this->servicioXml->validateAndCastItem($parameters["TipoFactor"]),
            "TasaOCuota" => $this->servicioXml->validateAndCastItem($parameters["TasaOCuota"]),
            "Importe" => $this->servicioXml->validateAndCastItem($parameters["Importe"])
        ];
    }
    //Función para saber si existe un color
    public function validarColor($colorSave)
    {
        $color = $this->modeloCatColores->where(CatalogoColoresModel::NOMBRE, $colorSave)
            ->get();
        $existe = 1;
        if (count($color) == 0) {
            $existe = 0;
            //Crear color
            $color = $this->servicio_colores->crear([
                CatalogoColoresModel::NOMBRE => $colorSave,
                CatalogoColoresModel::CLAVE => $colorSave
            ]);
        } else {
            $color = $color[0];
        }
        return [
            "id" => $color[CatalogoColoresModel::ID],
            "existe" => $existe,
            "color" => $color[CatalogoColoresModel::NOMBRE],
            "clave" => $color[CatalogoColoresModel::CLAVE]
        ];
    }
    public function pilot_unidades_usadas(Request $request)
    {
        $data = [
            'factory_request_number' => $request->get('factory_request_number'),
            'integration_reference_code' => $request->get('integration_reference_code'),
            'brand' => $request->get('brand'),
            'model' => $request->get('model'),
            'version' => $request->get('version'),
            'factory_code' => $request->get('factory_code'),
            'vin' => $request->get('vin'),
            'color' => $request->get('color'),
            'year' => $request->get('year'),
            'odometer' => $request->get('odometer'),
            'availability_status_code' => $request->get('availability_status_code'),
            'type_code' => 'vo',
            'received_flag' => $request->get('received_flag') //Flag que identifica si el vehículo se encuentra físicamente en stock. Valores posibles: 0 = Vehículo no recibido ; 1 = Vehículo físico
        ];
        if ($request->get('status_code')) {
            $data['status_code'] = $request->get('status_code');
        }
        if ($request->get('location')) {
            $data['location'] = $request->get('location');
        }
        if ($request->get('pilot_id')) {
            //create
            $data['id'] = $request->get('pilot_id');
            $servicioPilot = new ServicioPilot();
            $response = $servicioPilot->UpdateUnidad($data, LogsPilotModel::TYPE_UPDATE_UNIDAD_USADO);
        } else {
            $servicioPilot = new ServicioPilot();
            $response = $servicioPilot->CreateUnidad($data, LogsPilotModel::TYPE_CREATE_UNIDAD_USADO);
        }
        return json_decode($response);
    }
    public function cambiar_disponibilidad_unidad(Request $request)
    {


        $data = [
            'id' => $request->get('pilot_id'),
            'availability_status_code' => $request->get('availability_status_code'),
        ];
        if ($request->get('status_code')) {
            $data['status_code'] = $request->get('status_code');
        }
        if ($request->get('unidad_id') != null) {
            //cambiar de unidad nueva
            $tipo = LogsPilotModel::TYPE_CAMBIAR_DISPONIBILIDAD_NUEVO;
            if ($request->get('availability_status_code')) {
                $estatusRemisionId = 1;
            } else {
                $estatusRemisionId = 3;
            }
            $this->massUpdateWhereId(RemisionModel::ID, $request->get('unidad_id'), [
                RemisionModel::ID_STATUS_UNIDAD => $request->get('estatus_id'),
                RemisionModel::DISPONIBLE => $request->get('availability_status_code'),
                RemisionModel::ESTATUS_REMISION_ID => $estatusRemisionId, //cat_estatus_remision
                RemisionModel::ESTATUS_ID_PILOT => $request->get('estatus_id'), //catalogo_estatus_un
            ]);
        } else {
            $tipo = LogsPilotModel::TYPE_CAMBIAR_DISPONIBILIDAD_USADO;
        }
        $servicioPilot = new ServicioPilot();
        $response = $servicioPilot->UpdateUnidad($data, $tipo);
        return json_decode($response);
    }
    public function envio_multiple_pilot(Request $request)
    {
        $tabla_unidades = $this->modelo->getTable();
        $tabla_detalleRemision = $this->modeloDetalleRemisionModel->getTable();
        $tabla_cat_colores = $this->modeloCatColores->getTable();
        $tabla_lineas = $this->modeloLineas->getTable();

        $query = $this->modelo->select(
            $tabla_unidades . '.' . RemisionModel::ID,
            // $tabla_estatus_remision. '.' . CatEstatusUNModel::ESTATUS,
            $tabla_unidades . '.' . str_replace('\"', '', RemisionModel::UNIDAD_DESCRIPCION),
            $tabla_unidades . '.' . RemisionModel::CAT,
            $tabla_unidades . '.' . RemisionModel::CLAVE_VEHICULAR,
            $tabla_unidades . '.' . RemisionModel::ECONOMICO,
            $tabla_unidades . '.' . RemisionModel::ECONOMICO_VISUAL,
            $tabla_unidades . '.' . RemisionModel::UNIDAD_IMPORTADA,
            $tabla_unidades . '.' . RemisionModel::TIPO_AUTO,
            $tabla_unidades . '.' . RemisionModel::CLAVE_ISAN,
            $tabla_unidades . '.' . RemisionModel::CTA_MENUDO,
            $tabla_unidades . '.' . RemisionModel::CTA_FLOTILLA,
            $tabla_unidades . '.' . RemisionModel::CTA_CONAUTO,
            $tabla_unidades . '.' . RemisionModel::CTA_INTERCAMBIO,
            $tabla_unidades . '.' . RemisionModel::CTA_PLLENO,
            $tabla_unidades . '.' . RemisionModel::CTA_INVENTARIO,
            $tabla_unidades . '.' . RemisionModel::VTA_MENUDEO,
            $tabla_unidades . '.' . RemisionModel::VTA_FLOTILLA,
            $tabla_unidades . '.' . RemisionModel::VTA_CONAUTO,
            $tabla_unidades . '.' . RemisionModel::VTA_INTERCAMBIO,
            $tabla_unidades . '.' . RemisionModel::VTA_PLLENO,
            $tabla_unidades . '.' . RemisionModel::USERID,
            $tabla_unidades . '.' . RemisionModel::C_SUBTOTAL,
            $tabla_unidades . '.' . RemisionModel::C_TOTAL,
            $tabla_unidades . '.' . RemisionModel::C_IVA,
            $tabla_unidades . '.' . RemisionModel::ULTIMO_SERVICIO,
            $tabla_unidades . '.' . RemisionModel::LINEA_ID,
            $tabla_unidades . '.' . RemisionModel::CREATED_AT,
            $tabla_unidades . '.' . RemisionModel::DISPONIBLE,
            $tabla_unidades . '.' . RemisionModel::UNIQUE,
            $tabla_unidades . '.' . RemisionModel::C_TOTAL . ' as precio_venta',
            $tabla_unidades . '.' . RemisionModel::FECHA_PEDIMENTO,
            $tabla_unidades . '.' . RemisionModel::PEDIMENTO,
            $tabla_unidades . '.' . RemisionModel::FECHA_REMISION,
            $tabla_unidades . '.' . RemisionModel::SERIE,
            $tabla_unidades . '.' . RemisionModel::SERIE_CORTA,
            $tabla_unidades . '.' . RemisionModel::MOTOR,
            $tabla_unidades . '.' . RemisionModel::INTERCAMBIO,
            $tabla_unidades . '.' . RemisionModel::PROVEEDOR_ID,
            $tabla_unidades . '.' . RemisionModel::UBICACION_ID,
            $tabla_unidades . '.' . RemisionModel::UBICACION_LLAVES_ID,
            $tabla_unidades . '.' . RemisionModel::ESTATUS_ID,
            $tabla_unidades . '.' . RemisionModel::COMENTARIO,
            $tabla_unidades . '.' . RemisionModel::LEYENDA_DCTO,
            $tabla_unidades . '.' . RemisionModel::NO_INVENTARIO,
            $tabla_unidades . '.' . RemisionModel::CLAVE_SAT,
            $tabla_unidades . '.' . RemisionModel::UNIDAD,
            $tabla_unidades . '.' . RemisionModel::MODELO,
            $tabla_unidades . '.' . RemisionModel::FECHA_RECEPCION,
            $tabla_unidades . '.' . RemisionModel::LINEA_ID_CONTABILIDAD,
            $tabla_unidades . '.' . RemisionModel::CUENTA_INVENTARIO_UNIDAD,
            $tabla_unidades . '.' . RemisionModel::PILOT_ID,

            $tabla_detalleRemision . '.' . DetalleRemisionModel::PUERTAS,
            $tabla_detalleRemision . '.' . DetalleRemisionModel::CILINDROS,
            $tabla_detalleRemision . '.' . DetalleRemisionModel::TRANSMISION,

            // $tabla_detalleRemision . '.' . DetalleRemisionModel::COMBUSTIBLE_ID,
            $tabla_detalleRemision . '.' . DetalleRemisionModel::CAPACIDAD,
            $tabla_detalleRemision . '.' . DetalleRemisionModel::CAPACIDAD_KG,

            'color_interior' . '.' . CatalogoColoresModel::ID . ' as id_color_interior',
            'color_interior' . '.' . CatalogoColoresModel::NOMBRE . ' as color_interior',
            'color_exterior' . '.' . CatalogoColoresModel::ID . ' as id_color_exterior',
            'color_exterior' . '.' . CatalogoColoresModel::NOMBRE . ' as color_exterior',
            $tabla_lineas . '.' . CatLineas::ID . ' as modelo_id',
            $tabla_lineas . '.' . CatLineas::MODELO,
            $tabla_lineas . '.' . CatLineas::DESCRIPCION . ' as modelo_descripcion',
            $tabla_lineas . '.' . CatLineas::LINEA,
        );

        $query->Leftjoin($tabla_lineas, $tabla_lineas . '.id', '=', $tabla_unidades . '.' . RemisionModel::LINEA_ID);

        $query->Leftjoin($tabla_detalleRemision, $tabla_detalleRemision . '.' . DetalleRemisionModel::REMISIONID, '=', $tabla_unidades . '.' . RemisionModel::ID);

        $query->Leftjoin($tabla_cat_colores . ' as color_interior', 'color_interior.' . CatalogoColoresModel::ID, '=', $tabla_detalleRemision . '.' . DetalleRemisionModel::COLORINTID);

        $query->Leftjoin($tabla_cat_colores . ' as color_exterior', 'color_exterior.' . CatalogoColoresModel::ID, '=', $tabla_detalleRemision . '.' . DetalleRemisionModel::COLOREXTID);

        $todo = $request->get('todo');

        if (!$todo) {
            $query->where('pilot_id', '!=', '');
        }

        $remisiones = $query->get();
        $errores = false;
        $array_errores = [];
        $array_actualizados = [];
        foreach ($remisiones as $key => $remision) {

            $data = [
                'brand' => str_replace('\"', '', $remision->unidad_descripcion),
                'model' => $remision->linea,
                'version' => 'N/A',
                'factory_code' => $remision->cat,
                'vin' => $remision->serie,
                'color' => $remision->color_interior,
                'year' => (string)$remision->modelo,
                'received_flag' => 0 //Flag que identifica si el vehículo se encuentra físicamente en stock. Valores posibles: 0 = Vehículo no recibido ; 1 = Vehículo físico
            ];
            
            if ($remision->pilot_id) {
                $data['id'] = $remision->pilot_id;
                $servicioPilot = new ServicioPilot();
                $response = $servicioPilot->UpdateUnidad($data, LogsPilotModel::TYPE_UPDATE_UNIDAD);
                $response = json_decode($response);
                if ($response->result->status == 'success') {
                    $this->massUpdateWhereId(RemisionModel::ID, $remision->id, [
                        RemisionModel::PILOT_ID => $response->result->entitydata->id,
                    ]);
                    $array_actualizados[] = [
                        'remision' => $remision->id,
                        'pilot_id' => $response->result->entitydata->id
                    ];
                } else {
                    $errores = true;
                    $array_errores[] = [
                        'remision' => $remision->id,
                        'message' => $response->result->message
                    ];
                }
            } else {

                $data['factory_request_number'] = $remision->economico;
                $data['integration_reference_code'] = (string)$remision->id;
                $data['availability_status_code'] = '1';
                $data['type_code'] = 'vn';
                $servicioPilot = new ServicioPilot();
                $response = $servicioPilot->CreateUnidad($data, LogsPilotModel::TYPE_CREATE_UNIDAD);
                $response = json_decode($response);
                if ($response->result->status == 'success') {
                    $this->massUpdateWhereId(RemisionModel::ID, $remision->id, [
                        RemisionModel::PILOT_ID => $response->result->entitydata->id,
                    ]);
                    $array_actualizados[] = [
                        'remision' => $remision->id,
                        'pilot_id' => $response->result->entitydata->id
                    ];
                } else {
                    $errores = true;
                    $array_errores[] = [
                        'remision' => $remision->id,
                        'message' => $response->result->message
                    ];
                }
            }
        }
        return [
            'exito' => !$errores ? true : false,
            'actualizados' => $array_actualizados,
            'errores' => $array_errores,
        ];
    }
}
