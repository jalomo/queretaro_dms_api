<?php

namespace App\Servicios\Autos\UnidadesNuevas;

use App\Models\Autos\UnidadesNuevas\ConsecutivosModel;
use App\Servicios\Core\ServicioDB;
use Illuminate\Http\Request;
class ServicioConsecutivosUN extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'detalle costos remision';
        $this->modelo = new ConsecutivosModel();
    }

    public function getReglasGuardar()
    {
        return [
            ConsecutivosModel::ANIO => 'required',
            ConsecutivosModel::CAT_CONSECUTIVO => 'required',
            ConsecutivosModel::TIPO => 'required',
            ConsecutivosModel::CONSECUTIVO => 'required',
        ];
    }
    public function getReglasUpdate()
    {
        return [
            ConsecutivosModel::ANIO => 'required',
            ConsecutivosModel::CAT_CONSECUTIVO => 'required',
            ConsecutivosModel::TIPO => 'required',
            ConsecutivosModel::CONSECUTIVO => 'required',
        ];
    }
    public function getConsecutivo($parametros,$tipo){
        
        $anio = $parametros[ConsecutivosModel::ANIO];
        
        $arrayCat = explode('-', $parametros[ConsecutivosModel::CAT_CONSECUTIVO]);
        
        $cat = isset($arrayCat[0]) ? $arrayCat[0] : $parametros[ConsecutivosModel::CAT_CONSECUTIVO];
        $consecutivo = $this->modelo 
                                    ->where(ConsecutivosModel::ANIO, $anio)
                                     ->where(ConsecutivosModel::CAT_CONSECUTIVO, $cat)
                                     ->where(ConsecutivosModel::TIPO, $tipo)
                                     ->orderBy(ConsecutivosModel::ID,'desc')
                                     ->limit(1)
                                     ->get();
        if (count($consecutivo) == 0) {
            return 1;
        }else{
            $consecutivo = $consecutivo[0][ConsecutivosModel::CONSECUTIVO];
            return (int)$consecutivo + 1;
        }
    }
}
