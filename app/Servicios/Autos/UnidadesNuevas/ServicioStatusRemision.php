<?php

namespace App\Servicios\Autos\UnidadesNuevas;

use App\Models\Autos\UnidadesNuevas\CatStatusRemision;
use App\Servicios\Core\ServicioDB;

class ServicioStatusRemision extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catálogo estatus remisión';
        $this->modelo = new CatStatusRemision();
    }

    public function getReglasGuardar()
    {
        return [
            CatStatusRemision::ESTATUS => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatStatusRemision::ESTATUS => 'required'
        ];
    }
}
