<?php

namespace App\Servicios\Autos\UnidadesNuevas;

use App\Models\Autos\UnidadesNuevas\DetalleCostosRemisionModel;
use App\Servicios\Core\ServicioDB;

class ServicioDetalleCostosRemisiones extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'detalle costos remision';
        $this->modelo = new DetalleCostosRemisionModel();
    }

    public function getReglasGuardar()
    {
        return [
            DetalleCostosRemisionModel::REMISIONID => 'required',
            DetalleCostosRemisionModel::C_VALOR_UNIDAD => 'required',
            DetalleCostosRemisionModel::C_EQUIPO_BASE => 'required',
            DetalleCostosRemisionModel::C_TOTAL_BASE => 'required',
            DetalleCostosRemisionModel::C_DEDUCCION_FORD => 'required',
            DetalleCostosRemisionModel::C_SEG_TRASLADO => 'required',
            DetalleCostosRemisionModel::C_GASTOS_TRASLADO => 'required',
            DetalleCostosRemisionModel::C_IMP_IMPORT => 'required',
            DetalleCostosRemisionModel::C_FLETES_EXT => 'required',
            DetalleCostosRemisionModel::C_ISAN => 'nullable',
            DetalleCostosRemisionModel::C_HOLDBACK => 'required',
            DetalleCostosRemisionModel::C_CUENTA_PUB => 'required',
            DetalleCostosRemisionModel::C_DONATIVO_CCF => 'required',
            DetalleCostosRemisionModel::C_PLAN_PISO => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            DetalleCostosRemisionModel::REMISIONID => 'required',
            DetalleCostosRemisionModel::C_VALOR_UNIDAD => 'required',
            DetalleCostosRemisionModel::C_EQUIPO_BASE => 'required',
            DetalleCostosRemisionModel::C_TOTAL_BASE => 'required',
            DetalleCostosRemisionModel::C_DEDUCCION_FORD => 'required',
            DetalleCostosRemisionModel::C_SEG_TRASLADO => 'required',
            DetalleCostosRemisionModel::C_GASTOS_TRASLADO => 'required',
            DetalleCostosRemisionModel::C_IMP_IMPORT => 'required',
            DetalleCostosRemisionModel::C_FLETES_EXT => 'required',
            DetalleCostosRemisionModel::C_ISAN => 'nullable',
            DetalleCostosRemisionModel::C_HOLDBACK => 'required',
            DetalleCostosRemisionModel::C_CUENTA_PUB => 'required',
            DetalleCostosRemisionModel::C_DONATIVO_CCF => 'required',
            DetalleCostosRemisionModel::C_PLAN_PISO => 'required',
        ];
    }
    public function getByRemision($id){
        return $this->getWhere(DetalleCostosRemisionModel::REMISIONID, $id)[0];
    }
}
