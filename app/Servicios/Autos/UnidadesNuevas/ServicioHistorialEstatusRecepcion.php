<?php

namespace App\Servicios\Autos\UnidadesNuevas;

use App\Models\Autos\UnidadesNuevas\CatStatusRecepcionUNModel;
use App\Models\Autos\UnidadesNuevas\HistorialStatusUnidadesModel;
use App\Models\Autos\UnidadesNuevas\RemisionModel;
use App\Models\Refacciones\CatalogoUbicacionModel;
use App\Models\Refacciones\UbicacionLLavesModel;
use App\Servicios\Core\ServicioDB;

class ServicioHistorialEstatusRecepcion extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'historial estatus recepcion unidades nuevas';
        $this->servicioRemision = new ServicioRemisiones();
        $this->modelo = new HistorialStatusUnidadesModel();

        $this->modelEstatus = new CatStatusRecepcionUNModel();
        $this->modelUnidades = new RemisionModel();
        $this->modelUbicaciones = new CatalogoUbicacionModel();
        $this->modelUbicacionLlaves = new UbicacionLLavesModel();
    }

    public function getReglasGuardar()
    {
        return [
            HistorialStatusUnidadesModel::ID_STATUS_ACTUAL => 'required',
            HistorialStatusUnidadesModel::ID_STATUS_NUEVO => 'required',
            HistorialStatusUnidadesModel::ID_USUARIO => 'required',
            HistorialStatusUnidadesModel::ID_UNIDAD => 'required',
            HistorialStatusUnidadesModel::COMENTARIO => 'nullable',
            HistorialStatusUnidadesModel::IMAGEN => 'nullable|mimes:jpg,jpeg,png|max:5120',
            HistorialStatusUnidadesModel::ID_UBICACION => 'required',
            HistorialStatusUnidadesModel::ID_UBICACION_LLAVES => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            HistorialStatusUnidadesModel::ID_STATUS_ACTUAL => 'required',
            HistorialStatusUnidadesModel::ID_STATUS_NUEVO => 'required',
            HistorialStatusUnidadesModel::ID_USUARIO => 'required',
            HistorialStatusUnidadesModel::ID_UNIDAD => 'required',
            HistorialStatusUnidadesModel::COMENTARIO => 'nullable',
            HistorialStatusUnidadesModel::IMAGEN => 'nullable|mimes:jpg,jpeg,png|max:5120',
            HistorialStatusUnidadesModel::ID_UBICACION => 'required',
            HistorialStatusUnidadesModel::ID_UBICACION_LLAVES => 'required'
        ];
    }
    public function saveHistorialUnidad($imagen, $info)
    {
        //Actualizar los nuevos datos en la remisión
        if ($info[HistorialStatusUnidadesModel::ID_STATUS_NUEVO] == 5) {
            //Si el estatus nuevo es igual a recepción, actualizar el ultimo_servicio
            $this->servicioRemision->massUpdateWhereId(RemisionModel::ID, $info[HistorialStatusUnidadesModel::ID_UNIDAD], [
                RemisionModel::ULTIMO_SERVICIO => date('Y-m-d')
            ]);
        }
        $this->servicioRemision->massUpdateWhereId(RemisionModel::ID, $info[HistorialStatusUnidadesModel::ID_UNIDAD], [
            RemisionModel::ID_STATUS_UNIDAD => $info[HistorialStatusUnidadesModel::ID_STATUS_NUEVO],
            RemisionModel::UBICACION_ID => $info[HistorialStatusUnidadesModel::ID_UBICACION],
            RemisionModel::UBICACION_LLAVES_ID => $info[HistorialStatusUnidadesModel::ID_UBICACION_LLAVES]
        ]);
        return $this->crear([
            HistorialStatusUnidadesModel::ID_STATUS_ACTUAL => $info[HistorialStatusUnidadesModel::ID_STATUS_ACTUAL],
            HistorialStatusUnidadesModel::ID_STATUS_NUEVO => $info[HistorialStatusUnidadesModel::ID_STATUS_NUEVO],
            HistorialStatusUnidadesModel::ID_USUARIO => $info[HistorialStatusUnidadesModel::ID_USUARIO],
            HistorialStatusUnidadesModel::ID_UNIDAD => $info[HistorialStatusUnidadesModel::ID_UNIDAD],
            HistorialStatusUnidadesModel::ID_UBICACION => $info[HistorialStatusUnidadesModel::ID_UBICACION],
            HistorialStatusUnidadesModel::COMENTARIO => $info[HistorialStatusUnidadesModel::COMENTARIO],
            HistorialStatusUnidadesModel::ID_UBICACION_LLAVES => $info[HistorialStatusUnidadesModel::ID_UBICACION_LLAVES],
            HistorialStatusUnidadesModel::IMAGEN => $imagen
        ]);
    }
    public function getHistorialId($id_unidad = '')
    {
        $tabla_historial = $this->modelo->getTable();
        $tabla_estatus = $this->modelEstatus->getTable();
        $tabla_unidades = $this->modelUnidades->getTable();
        $tabla_ubicacion = $this->modelUbicaciones->getTable();
        $tabla_ubicacion_llaves = $this->modelUbicacionLlaves->getTable();
        $query = $this->modelo->select(
            $tabla_historial . '.' . HistorialStatusUnidadesModel::COMENTARIO,
            $tabla_historial . '.' . HistorialStatusUnidadesModel::IMAGEN,
            $tabla_historial . '.' . HistorialStatusUnidadesModel::CREATED_AT,
            'estatus_actual' . '.' . CatStatusRecepcionUNModel::ESTATUS . ' as estatus_anterior',
            'estatus_nuevo' . '.' . CatStatusRecepcionUNModel::ESTATUS . ' as estatus_nuevo',
            $tabla_unidades . '.' . RemisionModel::UNIDAD_DESCRIPCION,
            $tabla_unidades . '.' . RemisionModel::SERIE,
            $tabla_unidades . '.' . RemisionModel::SERIE_CORTA,
            $tabla_ubicacion . '.' . CatalogoUbicacionModel::NOMBRE . ' as ubicacion',
            $tabla_ubicacion_llaves . '.' . UbicacionLLavesModel::NOMBRE . ' as ubicacion_llaves',

        )
            ->from($tabla_historial);
        $query->join($tabla_unidades, $tabla_unidades . '.' . RemisionModel::ID, '=', $tabla_historial . '.' . HistorialStatusUnidadesModel::ID_UNIDAD);
        $query->join($tabla_ubicacion, $tabla_ubicacion . '.' . CatalogoUbicacionModel::ID, '=', $tabla_historial . '.' . HistorialStatusUnidadesModel::ID_UBICACION);
        $query->join($tabla_ubicacion_llaves, $tabla_ubicacion_llaves . '.' . UbicacionLLavesModel::ID, '=', $tabla_historial . '.' . HistorialStatusUnidadesModel::ID_UBICACION_LLAVES);
        $query->join($tabla_estatus . ' as estatus_actual', 'estatus_actual.' . CatStatusRecepcionUNModel::ID, '=', $tabla_historial . '.' . HistorialStatusUnidadesModel::ID_STATUS_ACTUAL);
        $query->join($tabla_estatus . ' as estatus_nuevo', 'estatus_nuevo.' . CatStatusRecepcionUNModel::ID, '=', $tabla_historial . '.' . HistorialStatusUnidadesModel::ID_STATUS_NUEVO);
        $query->orderBy($tabla_historial . '.' . HistorialStatusUnidadesModel::CREATED_AT, 'desc');
        return $query->get();
    }
}
