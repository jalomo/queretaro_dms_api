<?php

namespace App\Servicios\Autos;

use App\Servicios\Core\ServicioDB;
use App\Models\Autos\CatModelosModel;

class ServicioCatModelos extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'modelos';
        $this->modelo = new CatModelosModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatModelosModel::NOMBRE => 'required',
            CatModelosModel::CLAVE => 'required',
            CatModelosModel::ID_MARCA => 'required',
            CatModelosModel::TIEMPO_LAVADO => 'nullable'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatModelosModel::NOMBRE => 'required',
            CatModelosModel::CLAVE => 'required',
            CatModelosModel::ID_MARCA => 'required',
            CatModelosModel::TIEMPO_LAVADO => 'nullable'
        ];
    }

    public function getReglasBusquedaModelo()
    {
        return [
            CatModelosModel::NOMBRE => 'required'
        ];
    }
    public function getReglasByMarca()
    {
        return [
            CatModelosModel::ID_MARCA => 'required|numeric|exists:catalogo_marcas,id'
        ];
    }

    public function searchIdModelo($request)
    {
        $data = $this->modelo->where(CatModelosModel::NOMBRE, $request->get(CatModelosModel::NOMBRE))
            ->select(CatModelosModel::ID)->limit(1)->get();

        return $data;
    }

    public function getByMarcaId($request)
    {
        return $this->modelo->where(CatModelosModel::ID_MARCA, $request->get(CatModelosModel::ID_MARCA))->get();

    }
}
