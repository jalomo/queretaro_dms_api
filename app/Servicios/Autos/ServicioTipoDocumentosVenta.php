<?php

namespace App\Servicios\Autos;

use App\Models\Autos\TipoDocumentosVentaModel;
use App\Servicios\Core\ServicioDB;

class ServicioTipoDocumentosVenta extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'tipo documento';
        $this->modelo = new TipoDocumentosVentaModel();
    }

    public function getReglasGuardar()
    {
        return [
            TipoDocumentosVentaModel::NOMBRE => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            TipoDocumentosVentaModel::NOMBRE => 'required'
        ];
    }

    
}
