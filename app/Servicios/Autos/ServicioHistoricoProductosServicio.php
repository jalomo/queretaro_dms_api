<?php

namespace App\Servicios\Autos;

use App\Exceptions\ParametroHttpInvalidoException;
use App\Servicios\Core\ServicioDB;
use App\Models\Autos\HistoricoProductosServicioModel;

class ServicioHistoricoProductosServicio extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'productos servicio';
        $this->modelo = new HistoricoProductosServicioModel();
    }

    public function getReglasGuardar()
    {
        return [
            HistoricoProductosServicioModel::PRODUCTO_ID => 'required|exists:producto,id',
            HistoricoProductosServicioModel::PRODUCTO_SERVICIO_ID => 'required|numeric',
            HistoricoProductosServicioModel::NO_IDENTIFICACION => 'required',
            HistoricoProductosServicioModel::CANTIDAD => 'required',
            HistoricoProductosServicioModel::NUMERO_ORDEN => 'required',
        ];
    }

    public function getReglasUpdate()
    {
        return [
            HistoricoProductosServicioModel::PRODUCTO_ID => 'nullable|exists:producto,id',
            HistoricoProductosServicioModel::PRODUCTO_SERVICIO_ID => 'nullable|numeric',
            HistoricoProductosServicioModel::NO_IDENTIFICACION => 'nullable',
            HistoricoProductosServicioModel::CANTIDAD => 'required',
            HistoricoProductosServicioModel::NUMERO_ORDEN
        ];
    }

    public function store($parametros)
    {
        $validar = $this->validarProductos($parametros); 
        if (isset($validar)) {
            // throw new ParametroHttpInvalidoException([
            //     'msg' => "El producto ya se registro previamente."
            // ]);
            return  $validar;
        }

        $array_parametros = $parametros->all();
        $array_parametros[HistoricoProductosServicioModel::CANTIDAD] = round($parametros->get(HistoricoProductosServicioModel::CANTIDAD));
        return $this->crear($array_parametros);
    }

    public function validarProductos($parametros)
    {
        //checar validacion
        return $this->modelo
            ->where(HistoricoProductosServicioModel::PRODUCTO_ID, '=', $parametros->get(HistoricoProductosServicioModel::PRODUCTO_ID))
            ->where(HistoricoProductosServicioModel::PRODUCTO_SERVICIO_ID, '=', $parametros->get(HistoricoProductosServicioModel::PRODUCTO_SERVICIO_ID))
            ->where(HistoricoProductosServicioModel::NO_IDENTIFICACION, '=', $parametros->get(HistoricoProductosServicioModel::NO_IDENTIFICACION))
            ->get()
            ->first();
    }
}
