<?php

namespace App\Servicios\Autos;

use App\Models\Autos\EncuestaSatisfaccionModel;
use App\Models\Autos\VentasAutosModel;
use App\Models\Refacciones\ClientesModel;
use App\Models\Usuarios\User;
use App\Servicios\Core\ServicioDB;

class ServicioEncuestaSatisfaccion extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'encuesta satisfaccion';
        $this->modelo = new EncuestaSatisfaccionModel();
        $this->modeloVentaAutos = new VentasAutosModel();
        $this->modeloUsuario = new User();
        $this->modeloCliente = new ClientesModel();
    }

    public function getReglasGuardar()
    {
        return [
            EncuestaSatisfaccionModel::ID_VENTA_UNIDAD => 'required|exists:' . VentasAutosModel::getTableName() . ',id',
            EncuestaSatisfaccionModel::ATENCION_ASESOR => 'required',
            EncuestaSatisfaccionModel::EXPERIENCIA_VENTAS => 'required',
            EncuestaSatisfaccionModel::EXPERIENCIA_FINANCIAMIENTO => 'required',
            EncuestaSatisfaccionModel::EXPERIENCIA_ENTREGA_VEHICULO => 'required',
            EncuestaSatisfaccionModel::SEGUIMIENTO_COMPROMISOS => 'required',
            EncuestaSatisfaccionModel::COMENTARIOS_MEJORAS => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            EncuestaSatisfaccionModel::ID_VENTA_UNIDAD => 'nullable|exists:' . VentasAutosModel::getTableName() . ',id',
            EncuestaSatisfaccionModel::ATENCION_ASESOR => 'nullable',
            EncuestaSatisfaccionModel::EXPERIENCIA_VENTAS => 'nullable',
            EncuestaSatisfaccionModel::EXPERIENCIA_FINANCIAMIENTO => 'nullable',
            EncuestaSatisfaccionModel::EXPERIENCIA_ENTREGA_VEHICULO => 'nullable',
            EncuestaSatisfaccionModel::SEGUIMIENTO_COMPROMISOS => 'nullable',
            EncuestaSatisfaccionModel::COMENTARIOS_MEJORAS => 'nullable'
        ];
    }

    public function getdatosencuestaventa($id_venta = '')
    {
        $tabla_encuesta = EncuestaSatisfaccionModel::getTableName(); //$this->modeloVentaAutos->getTableName();
        $tabla_venta = VentasAutosModel::getTableName();
        $tabla_asesor = User::getTableName();
        $tabla_cliente = ClientesModel::getTableName();

        $query = $this->modeloVentaAutos->select(
            $tabla_encuesta . '.*',
            $tabla_venta . '.' . VentasAutosModel::ID,
            $tabla_cliente . '.' . ClientesModel::NOMBRE,
            $tabla_cliente . '.' . ClientesModel::APELLIDO_PATERNO,
            $tabla_cliente . '.' . ClientesModel::APELLIDO_MATERNO,
            $tabla_cliente . '.' . ClientesModel::CORREO_ELECTRONICO,
            $tabla_cliente . '.' . ClientesModel::TELEFONO,
            $tabla_asesor . '.' . User::NOMBRE . ' as nombre_asesor',
            $tabla_asesor . '.' . User::APELLIDO_MATERNO . ' as apellido_materno_asesor',
            $tabla_asesor . '.' . User::APELLIDO_PATERNO . ' as apellido_paterno_asesor',
        );
        $query->leftjoin($tabla_encuesta, $tabla_encuesta . '.' . EncuestaSatisfaccionModel::ID_VENTA_UNIDAD, '=', $tabla_venta . '.' . VentasAutosModel::ID);
        $query->join($tabla_asesor, $tabla_asesor . '.' . User::ID, '=', $tabla_venta . '.' . VentasAutosModel::ID_ASESOR);
        $query->join($tabla_cliente, $tabla_cliente . '.' . ClientesModel::ID, '=', VentasAutosModel::ID_CLIENTE);

        if ($id_venta != '') {
            $query->where($tabla_venta . '.' . VentasAutosModel::ID, $id_venta);
        }
        return $query->first();
    }
}
