<?php

namespace App\Servicios\CuentasPorCobrar;

use App\Servicios\Core\ServicioDB;
use App\Models\CuentasPorCobrar\PlazoCreditoModel;

class ServicioPlazoCredito extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'plazo credito';
        $this->modelo = new PlazoCreditoModel();
    }

    public function getReglasGuardar()
    {
        return [
            PlazoCreditoModel::CANTIDAD_MES => 'required|numeric|unique:' . $this->modelo->getTable() . ',cantidad_mes',
            PlazoCreditoModel::NOMBRE => 'required|string|regex:/^[\pL\s]+$/u|max:50|unique:' . $this->modelo->getTable() . ',nombre',

        ];
    }
    public function getReglasUpdate()
    {
        return [
            PlazoCreditoModel::CANTIDAD_MES => 'nullable|numeric|unique:' . $this->modelo->getTable() . ',cantidad_mes',
            PlazoCreditoModel::NOMBRE => 'nullable|string|regex:/^[\pL\s]+$/u|max:50|unique:' . $this->modelo->getTable() . ',nombre',
        ];
    }
}
