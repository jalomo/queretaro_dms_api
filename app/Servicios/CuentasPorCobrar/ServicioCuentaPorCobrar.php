<?php

namespace App\Servicios\CuentasPorCobrar;

use App\Servicios\Core\ServicioDB;
use App\Servicios\Refacciones\ServicioFolios;
use App\Servicios\CuentasPorCobrar\ServicioAsientos;
use App\Models\CuentasPorCobrar\CuentasPorCobrarModel;
use App\Models\CuentasPorCobrar\EstatusCuentaModel;
use App\Models\CuentasPorCobrar\AbonosModel;
use App\Models\Refacciones\VentasRealizadasModel;
use App\Models\Refacciones\FoliosModel;
use App\Models\CuentasPorCobrar\PlazoCreditoModel;
use App\Models\Refacciones\ClientesModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Resources\Cuentas\CuentasResource;
use Illuminate\Database\Eloquent\Collection;

class ServicioCuentaPorCobrar extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'cuentas por cobrar';
        $this->modelo = new CuentasPorCobrarModel();
        $this->servicioFolio = new ServicioFolios();
        $this->servicio_abonos = new ServicioAbono();
        $this->modelo_plazo_credito = new PlazoCreditoModel();
        $this->servicioAsientos = new ServicioAsientos();
    }


    public function getReglasByCuentaID()
    {
        return [
            CuentasPorCobrarModel::CUENTA_POR_COBRAR_ID => 'required|numeric|exists:cuentas_por_cobrar,id',
        ];
    }

    public function getReglasByCuentaIDAndCliente()
    {
        return [
            CuentasPorCobrarModel::CUENTA_POR_COBRAR_ID => 'required|numeric|exists:cuentas_por_cobrar,id',
            CuentasPorCobrarModel::CLIENTE_ID => 'required|exists:clientes,id'
        ];
    }

    public function getReglasByCliente()
    {
        return [
            CuentasPorCobrarModel::CLIENTE_ID => 'required|exists:clientes,id'
        ];
    }

    public function getReglasNumeroOrden()
    {
        return [
            CuentasPorCobrarModel::NUMERO_ORDEN => 'required|exists:cuentas_por_cobrar,numero_orden'
        ];
    }

    public function getReglasGuardar()
    {
        return [
            CuentasPorCobrarModel::FOLIO_ID => 'nullable|exists:folios,id',
            CuentasPorCobrarModel::CLIENTE_ID => 'required|exists:clientes,id',
            CuentasPorCobrarModel::ESTATUS_CUENTA_ID => 'required|exists:estatus_cuenta,id',
            CuentasPorCobrarModel::CONCEPTO => 'required|string',
            CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID => 'required|numeric|exists:tipo_forma_pago,id',
            CuentasPorCobrarModel::TIPO_PAGO_ID => 'required|numeric|exists:cat_tipo_pago,id',
            CuentasPorCobrarModel::PLAZO_CREDITO_ID => 'required|numeric|exists:plazos_credito,id',
            CuentasPorCobrarModel::TOTAL => 'nullable|numeric',
            CuentasPorCobrarModel::ENGANCHE => 'nullable|numeric',
            CuentasPorCobrarModel::TASA_INTERES => 'nullable|numeric',
            CuentasPorCobrarModel::COMENTARIOS => 'nullable|json',
            CuentasPorCobrarModel::USUARIO_GESTOR_ID => 'nullable|numeric|exists:usuarios,id',
            CuentasPorCobrarModel::FECHA => 'nullable|date',
            VentasRealizadasModel::VENTA_TOTAL  => 'required',
            CuentasPorCobrarModel::TIPO_PROCESO => 'required|numeric|exists:catalogo_cuentas,id',
            CuentasPorCobrarModel::NUMERO_ORDEN => 'nullable',
        ];
    }

    public function getReglasUpdate()
    {
        return [
            CuentasPorCobrarModel::FOLIO_ID => 'nullable|exists:folios,id',
            CuentasPorCobrarModel::CLIENTE_ID => 'nullable|exists:clientes,id',
            CuentasPorCobrarModel::ESTATUS_CUENTA_ID => 'required|exists:estatus_cuenta,id',
            CuentasPorCobrarModel::CONCEPTO => 'nullable|string',
            CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID => 'nullable|numeric|exists:tipo_forma_pago,id',
            CuentasPorCobrarModel::TIPO_PAGO_ID => 'nullable|numeric|exists:cat_tipo_pago,id',
            CuentasPorCobrarModel::PLAZO_CREDITO_ID => 'nullable|numeric|exists:plazos_credito,id',
            CuentasPorCobrarModel::TOTAL => 'nullable|numeric',
            CuentasPorCobrarModel::ENGANCHE => 'nullable|numeric',
            CuentasPorCobrarModel::TASA_INTERES => 'nullable|numeric',
            CuentasPorCobrarModel::COMENTARIOS => 'nullable|json',
            CuentasPorCobrarModel::USUARIO_GESTOR_ID => 'nullable|numeric|exists:usuarios,id',
            CuentasPorCobrarModel::FECHA => 'nullable|date',
            VentasRealizadasModel::VENTA_TOTAL  => 'nullable|numeric',
            CuentasPorCobrarModel::TIPO_PROCESO => 'nullable|numeric|exists:catalogo_cuentas,id',
            CuentasPorCobrarModel::NUMERO_ORDEN => 'nullable'
        ];
    }

    public function getReglasCreateCuenta()
    {
        return [
            CuentasPorCobrarModel::FOLIO_ID => 'nullable|exists:folios,id',
            CuentasPorCobrarModel::CLIENTE_ID => 'required|exists:clientes,id',
            CuentasPorCobrarModel::ESTATUS_CUENTA_ID => 'required|exists:estatus_cuenta,id',
            CuentasPorCobrarModel::CONCEPTO => 'required|string',
            CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID => 'required|numeric|exists:tipo_forma_pago,id',
            CuentasPorCobrarModel::PLAZO_CREDITO_ID => 'required|numeric|exists:plazos_credito,id',
            CuentasPorCobrarModel::TIPO_PAGO_ID => 'required|numeric',
            CuentasPorCobrarModel::TOTAL => 'required|numeric',
            CuentasPorCobrarModel::IMPORTE => 'required|numeric',
            CuentasPorCobrarModel::FECHA => 'required|date',
            CuentasPorCobrarModel::TIPO_PROCESO => 'nullable|numeric|exists:catalogo_cuentas,id',
            CuentasPorCobrarModel::COMENTARIOS => 'nullable|json',
            CuentasPorCobrarModel::NUMERO_ORDEN => 'nullable',
            CuentasPorCobrarModel::OBSERVACIONES => 'nullable',
            CuentasPorCobrarModel::VENTA_TOTAL_DESCUENTO => 'nullable'
        ];
    }
    public function getReglasUpdateCuenta()
    {
        return [
            CuentasPorCobrarModel::FOLIO_ID => 'required|exists:folios,id',
            CuentasPorCobrarModel::CLIENTE_ID => 'required|exists:clientes,id',
            CuentasPorCobrarModel::CONCEPTO => 'required|string',
            CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID => 'required|numeric|exists:tipo_forma_pago,id',
            CuentasPorCobrarModel::PLAZO_CREDITO_ID => 'required|numeric|exists:plazos_credito,id',
            CuentasPorCobrarModel::TOTAL => 'required|numeric',
            CuentasPorCobrarModel::IMPORTE => 'required|numeric',
            CuentasPorCobrarModel::FECHA => 'required|date',
        ];
    }

    public function getReglasPolizaServicioHojalateria()
    {
        return [
            CuentasPorCobrarModel::TOTAL => 'required|numeric',
            CuentasPorCobrarModel::VENTA_TOTAL_DESCUENTO => 'nullable|numeric',
            CuentasPorCobrarModel::NUMERO_ORDEN => 'required'
        ];
    }

    public function getReglasFacturaComplemento()
    {
        return [
            CuentasPorCobrarModel::CUENTA_POR_COBRAR_ID => 'required|exists:cuentas_por_cobrar,id',
            AbonosModel::ABONO_ID => 'required|exists:abonos_por_cobrar,id',
            AbonosModel::PARCIALIDAD => 'required|numeric',
        ];
    }
    public function getReglasDescargarComplemento()
    {
        return [
            CuentasPorCobrarModel::CUENTA_POR_COBRAR_ID => 'required|exists:cuentas_por_cobrar,id',
            AbonosModel::ABONO_ID => 'required|exists:abonos_por_cobrar,id',
        ];
    }

    public function getReglasValidateJSON()
    {
        return [
            CuentasPorCobrarModel::COMENTARIOS => 'required|json',
        ];
    }

    public function getReglasValidateComplementoPago()
    {
        return [
            "uuid" => 'required',
            "seriep" => 'required',
            "foliop" => 'required',
            "moneda_pe" => 'required',
            "tipo_cambio_p" => 'required',
            "metodo_pago_p" => 'required',
            "numero_parcial" => 'required',
            "deuda_pagar" => 'required',
            "importe_pagado" => 'required',
            "nuevo_saldo" => 'required',
            "receptor_nombre" => 'required',
            "lugarExpedicion" => 'required',
            "fecha_pago" => 'required',
            "receptor_email" => 'required',
            "factura_folio" => 'required',
            "factura_serie" => 'required',
            "comentario" => 'required',
            "receptorRFC" => 'required',
            "complemento_fecha" => 'required',
            "complemento_forma_pago" => 'required',
            "complemento_tipoCambio" => 'required',
            "complemento_totalPago" => 'required',
            "complemento_no_operacion" => 'required',
            "complemento_banco_ordenante" => 'required',
            "complemento_cta_ordenante" => 'required',
            "complemento_rfc_beneficiario" => 'required',
            "complemento_cta_beneficiario" => 'required',
            "complemento_rfc_ordenante" => 'required'
        ];
    }

    public function getReglasFacturaMultipleCuenta()
    {
        return [
            'pagos_factura_id' => 'required|numeric|exists:pagos_factura,id',
        ];
    }

    public function getReglasCancelarFactura()
    {
        return [
            CuentasPorCobrarModel::CUENTA_POR_COBRAR_ID => 'required|numeric|exists:cuentas_por_cobrar,id'
        ];
    }

    public function getAll(Request $request)
    {
        $request->merge(['page' => floor($request->start / ($request->length ?: 1)) + 1]);
        $consulta =  DB::table('cuentas_por_cobrar as cxc')
            ->join('folios', 'cxc.folio_id', '=', 'folios.id')
            ->join('clientes', 'cxc.cliente_id', '=', 'clientes.id')
            ->leftjoin('plazos_credito', 'cxc.plazo_credito_id', '=', 'plazos_credito.id')
            ->leftjoin('tipo_forma_pago', 'cxc.tipo_forma_pago_id', '=', 'tipo_forma_pago.id')
            ->leftjoin('estatus_cuenta', 'cxc.estatus_cuenta_id', '=', 'estatus_cuenta.id')
            //->leftJoin('abonos_por_cobrar', 'cxc.id', '=', 'abonos_por_cobrar.cuenta_por_cobrar_id')
            ->leftJoin('cat_tipo_pago', 'cxc.tipo_forma_pago_id', '=', 'cat_tipo_pago.id')
            //->leftJoin('cat_tipo_abono', 'abonos_por_cobrar.tipo_abono_id', '=', 'cat_tipo_abono.id')
            ->leftJoin('catalogo_procesos', 'folios.tipo_proceso_id', '=', 'catalogo_procesos.id')
            ->select(
                'cxc.id',
                'cxc.concepto',
                'cxc.numero_orden',
                'cxc.importe',
                'cxc.total',
                'cxc.fecha',
                'cxc.tipo_forma_pago_id',
                'folios.folio',
                'folios.tipo_proceso_id',
                'folios.id as folio_id',
                'clientes.numero_cliente as numero_cliente',
                'clientes.nombre as nombre_cliente',
                'clientes.apellido_paterno as apellido_paterno',
                'clientes.apellido_materno as apellido_materno',
                'clientes.nombre_empresa',
                'plazos_credito.nombre as plazo_credito',
                'plazos_credito.cantidad_mes',
                'tipo_forma_pago.descripcion as tipo_forma_pago',
                'estatus_cuenta.nombre as estatus_cuenta',
                'estatus_cuenta.id as estatus_cuenta_id',
                'catalogo_procesos.descripcion as proceso',
                'catalogo_procesos.clave_poliza',
                'cat_tipo_pago.nombre as tipo_pago'
            );
        $consulta->when($request->get(ClientesModel::NOMBRE), function ($q) use ($request) {
            $q->where(
                ClientesModel::getTableName() . '.' . ClientesModel::NOMBRE,
                'like',
                '%' . strtoupper($request->get(ClientesModel::NOMBRE)) . '%'
            );
        });
        $consulta->when($request->get(ClientesModel::APELLIDOS), function ($q) use ($request) {
            $q->Where(
                ClientesModel::getTableName() . '.' . ClientesModel::APELLIDO_PATERNO,
                'like',
                '%' . strtoupper($request->get(ClientesModel::APELLIDOS)) . '%'
            )
                ->orWhere(
                    ClientesModel::getTableName() . '.' . ClientesModel::APELLIDO_MATERNO,
                    'like',
                    '%' . strtoupper($request->get(ClientesModel::APELLIDOS)) . '%'
                );
        });
        $consulta->when($request->get(ClientesModel::NUMERO_CLIENTE), function ($q) use ($request) {
            $q->where(
                ClientesModel::getTableName() . '.' . ClientesModel::NUMERO_CLIENTE,
                'like',
                '%' . strtoupper($request->get(ClientesModel::NUMERO_CLIENTE)) . '%'
            );
        });
        $consulta->when($request->get(ClientesModel::RFC), function ($q) use ($request) {
            $q->where(
                ClientesModel::getTableName() . '.' . ClientesModel::RFC,
                $request->get(ClientesModel::RFC)
            );
        });
        if ($request->get('fecha_inicio') && $request->get('fecha_fin')) {
            $consulta->whereBetween(CuentasPorCobrarModel::ALIAS . '.' . CuentasPorCobrarModel::FECHA, [$request->get('fecha_inicio') . ' 00:00:00', $request->get('fecha_fin') . ' 23:59:59']);
        }
        if ($request->get('fecha_inicio') && !$request->get('fecha_fin')) {
            $consulta->whereDate(CuentasPorCobrarModel::ALIAS . '.' . CuentasPorCobrarModel::FECHA, '>=', $request->get('fecha_inicio'));
        }
        if (!$request->get('fecha_inicio') && $request->get('fecha_fin')) {
            $consulta->whereDate(CuentasPorCobrarModel::ALIAS . '.' . CuentasPorCobrarModel::FECHA, '<=', $request->get('fecha_fin'));
        }
        if ($request->get('cliente_id')) {
            $consulta->where('cxc.cliente_id', $request->get('cliente_id'));
        }
        if ($request->get('folio')) {
            $consulta->where('folios.folio', $request->get('folio'));
        }
        if ($request->get('tipo_proceso_id')) {
            $consulta->where('folios.tipo_proceso_id', $request->get('tipo_proceso_id'));
        }
        if ($request->get('numero_orden')) {
            $consulta->where('cxc.numero_orden', $request->get('numero_orden'));
        }
        if ($request->get('fecha')) {
            $consulta->where('cxc.fecha', $request->get('fecha'));
        }
        if ($request->get('tipo_forma_pago_id')) {
            $consulta->where('cxc.tipo_forma_pago_id', $request->get('tipo_forma_pago_id'));
        }
        if ($request->get('estatus_cuenta_id')) {
            $consulta->where('cxc.estatus_cuenta_id', $request->get('estatus_cuenta_id'));
        } else {
            $consulta->whereNotIn('cxc.estatus_cuenta_id', [4]);
        }
        $consulta->orderBy('cxc.id', 'desc');
        if (!isset($request->no_pagination)) {
            return $consulta->paginate(10);
        } else {
            return $consulta->get();
        }
    }

    public function getByIdCuenta($id)
    {
        return DB::table('cuentas_por_cobrar as cxc')
            ->join('folios', 'cxc.folio_id', '=', 'folios.id')
            ->join('clientes', 'cxc.cliente_id', '=', 'clientes.id')
            ->join('plazos_credito', 'cxc.plazo_credito_id', '=', 'plazos_credito.id')
            ->join('tipo_forma_pago', 'cxc.tipo_forma_pago_id', '=', 'tipo_forma_pago.id')
            ->join('estatus_cuenta', 'cxc.estatus_cuenta_id', '=', 'estatus_cuenta.id')
            ->leftJoin('catalogo_procesos', 'folios.tipo_proceso_id', '=', 'catalogo_procesos.id')
            ->select(
                'cxc.id',
                'cxc.concepto',
                'cxc.numero_orden',
                'cxc.importe',
                'cxc.intereses',
                'cxc.total',
                'cxc.comentarios',
                'cxc.enganche',
                'cxc.tipo_forma_pago_id',
                'cxc.tipo_pago_id',
                'cxc.tasa_interes',
                'cxc.estatus_cuenta_id',
                'cxc.fecha',
                'cxc.created_at',
                'folios.folio',
                'folios.id as folio_id',
                'folios.tipo_proceso_id',
                'clientes.nombre as nombre_cliente',
                'clientes.id AS cliente_id',
                'plazos_credito.nombre as plazo_credito',
                'plazos_credito.cantidad_mes',
                'tipo_forma_pago.descripcion as tipo_forma_pago',
                'estatus_cuenta.nombre as estatus_cuenta',
                'catalogo_procesos.descripcion',
                'catalogo_procesos.clave_poliza',
                'cxc.observaciones'
            )->where('cxc.id', $id)->first();
    }

    public function getByFolioId($folio_id)
    {
        return  DB::table('cuentas_por_cobrar as cxc')
            ->join('folios', 'cxc.folio_id', '=', 'folios.id')
            ->join('clientes', 'cxc.cliente_id', '=', 'clientes.id')
            ->select(
                'cxc.*',
                'folios.folio',
                'folios.id as folio_id',
                'folios.tipo_proceso_id',
                'clientes.nombre as nombre_cliente',
                'clientes.apellido_paterno',
                'clientes.apellido_materno',
                'clientes.id AS cliente_id'
            )->where('cxc.folio_id', $folio_id)
            ->first();
    }
    public function getByFolioNumeroOrden($folio_id)
    {
        return $this->modelo->join('folios', 'folios.id', '=', 'cuentas_por_cobrar.folio_id')
            ->select('cuentas_por_cobrar.id', 'cuentas_por_cobrar.numero_orden', 'cuentas_por_cobrar.folio_id', 'folios.tipo_proceso_id', 'cuentas_por_cobrar.estatus_cuenta_id')
            ->where('cuentas_por_cobrar.folio_id', $folio_id)->get();
    }

    public function getKardexPagos($params)
    {
        $consulta =  DB::table('cuentas_por_cobrar as cxc')
            ->join('folios', 'cxc.folio_id', '=', 'folios.id')
            ->join('clientes', 'cxc.cliente_id', '=', 'clientes.id')
            ->join('plazos_credito', 'cxc.plazo_credito_id', '=', 'plazos_credito.id')
            ->join('tipo_forma_pago', 'cxc.tipo_forma_pago_id', '=', 'tipo_forma_pago.id')
            ->join('estatus_cuenta', 'cxc.estatus_cuenta_id', '=', 'estatus_cuenta.id')
            ->join('abonos_por_cobrar', 'cxc.id', '=', 'abonos_por_cobrar.cuenta_por_cobrar_id')
            ->join('cat_estatus_abono', 'abonos_por_cobrar.estatus_abono_id', '=', 'cat_estatus_abono.id')
            ->join('cat_tipo_abono', 'abonos_por_cobrar.tipo_abono_id', '=', 'cat_tipo_abono.id')
            ->leftJoin('usuarios', 'cxc.usuario_gestor_id', '=', 'usuarios.id')
            ->select(
                'cxc.id',
                'cxc.concepto',
                'cxc.total',
                'cxc.tipo_forma_pago_id',
                'folios.folio',
                'clientes.numero_cliente as numero_cliente',
                'clientes.nombre as nombre_cliente',
                'plazos_credito.nombre as plazo_credito',
                'plazos_credito.cantidad_mes',
                'tipo_forma_pago.descripcion as tipo_forma_pago',
                'abonos_por_cobrar.total_abono',
                'abonos_por_cobrar.fecha_vencimiento',
                'abonos_por_cobrar.fecha_pago',
                'abonos_por_cobrar.total_pago',
                'abonos_por_cobrar.dias_moratorios',
                'cat_tipo_abono.nombre as tipo_pago',
                'cat_estatus_abono.nombre as estatus_abono',
                'cat_estatus_abono.id as estatus_abono_id',
                'usuarios.nombre as gestor_nombre',
                'usuarios.apellido_paterno as gestor_apellido_paterno'
            );
        if (isset($params['cliente_id']) && $params['cliente_id']) {
            $consulta->where('cxc.cliente_id', $params['cliente_id']);
        }
        if (isset($params['folio']) && $params['folio']) {
            $folio = $params['folio'];

            $consulta->where('folios.folio', 'iLIKE', '%' . $folio . '%');
        }
        if (isset($params['tipo_forma_pago_id']) && $params['tipo_forma_pago_id']) {
            $consulta->where('cxc.tipo_forma_pago_id', $params['tipo_forma_pago_id']);
        }
        if (isset($params['estatus_abono_id']) && $params['estatus_abono_id']) {
            $consulta->where('abonos_por_cobrar.estatus_abono_id', $params['estatus_abono_id']);
        }
        if (isset($params['fecha_inicio']) && !isset($params['fecha_fin'])) {
            $consulta->where('abonos_por_cobrar.fecha_vencimiento', '>=', $params['fecha_inicio']);
        }
        if (!isset($params['fecha_inicio']) && isset($params['fecha_fin'])) {
            $consulta->where('abonos_por_cobrar.fecha_vencimiento', '<=', $params['fecha_fin']);
        }
        if (isset($params['fecha_inicio']) && isset($params['fecha_fin'])) {
            $consulta->where('abonos_por_cobrar.fecha_vencimiento', '>=', $params['fecha_inicio']);
            $consulta->where('abonos_por_cobrar.fecha_vencimiento', '<=', $params['fecha_fin']);
        }
        if (isset($params['usuario_gestor_id']) && $params['usuario_gestor_id']) {
            $consulta->where('cxc.usuario_gestor_id', $params['usuario_gestor_id']);
        }
        $consulta->whereIn('cxc.estatus_cuenta_id', [1, 3]);
        $consulta->where('abonos_por_cobrar.tipo_abono_id', 2);

        $consulta->orderBy('abonos_por_cobrar.id', 'asc');

        return $consulta->get();
        //dd( DB::getQueryLog());
    }

    public function getCierreCaja($params)
    {
        $consulta =  DB::table('cuentas_por_cobrar as cxc')
            ->leftJoin('abonos_por_cobrar', 'cxc.id', '=', 'abonos_por_cobrar.cuenta_por_cobrar_id')
            ->leftJoin('cat_tipo_pago', 'abonos_por_cobrar.tipo_pago_id', '=', 'cat_tipo_pago.id')
            ->leftJoin('catalogo_caja', 'abonos_por_cobrar.caja_id', '=', 'catalogo_caja.id')
            ->select(
                DB::raw('sum(abonos_por_cobrar.total_pago) as total'),
                'cat_tipo_pago.id',
                'cat_tipo_pago.nombre as tipo_pago'
            );

        if (isset($params['estatus_abono_id']) && $params['estatus_abono_id']) {
            $consulta->where('abonos_por_cobrar.estatus_abono_id', $params['estatus_abono_id']);
        }
        if (isset($params['fecha_inicio']) && !isset($params['fecha_fin'])) {
            $consulta->where('abonos_por_cobrar.fecha_pago', '=', $params['fecha_inicio']);
        }
        if (!isset($params['fecha_inicio']) && isset($params['fecha_fin'])) {
            $consulta->where('abonos_por_cobrar.fecha_pago', '=', $params['fecha_fin']);
        }
        if (isset($params['fecha_inicio']) && isset($params['fecha_fin'])) {
            $consulta->where('abonos_por_cobrar.fecha_pago', '>=', $params['fecha_inicio']);
            $consulta->where('abonos_por_cobrar.fecha_pago', '<=', $params['fecha_fin']);
        }
        if (isset($params['caja_id']) && $params['caja_id']) {
            $consulta->where('abonos_por_cobrar.caja_id', $params['caja_id']);
        }

        $consulta->whereIn('cxc.estatus_cuenta_id', [1, 2, 3, 6]);
        $consulta->groupBy(
            'cat_tipo_pago.id',
            'tipo_pago'
        );
        return $consulta->get();
    }

    public function getDetalleCaja($params)
    {
        $consulta =  DB::table('cuentas_por_cobrar as cxc')
            ->join('abonos_por_cobrar', 'cxc.id', '=', 'abonos_por_cobrar.cuenta_por_cobrar_id')
            ->join('cat_tipo_pago', 'abonos_por_cobrar.tipo_pago_id', '=', 'cat_tipo_pago.id')
            ->select(
                DB::raw('sum(abonos_por_cobrar.total_pago) as total'),
                'cat_tipo_pago.nombre as tipo_pago'
            );

        if (isset($params['estatus_abono_id']) && $params['estatus_abono_id']) {
            $consulta->where('abonos_por_cobrar.estatus_abono_id', $params['estatus_abono_id']);
        }
        if (isset($params['fecha_inicio']) && !isset($params['fecha_fin'])) {
            $consulta->where('abonos_por_cobrar.fecha_pago', '=', $params['fecha_inicio']);
        }
        if (!isset($params['fecha_inicio']) && isset($params['fecha_fin'])) {
            $consulta->where('abonos_por_cobrar.fecha_pago', '=', $params['fecha_fin']);
        }
        if (isset($params['fecha_inicio']) && isset($params['fecha_fin'])) {
            $consulta->where('abonos_por_cobrar.fecha_pago', '>=', $params['fecha_inicio']);
            $consulta->where('abonos_por_cobrar.fecha_pago', '<=', $params['fecha_fin']);
        }

        $consulta->whereIn('cxc.estatus_cuenta_id', [1, 2, 3]);
        $consulta->groupBy(
            'tipo_pago'
        );
        return $consulta->get();
    }


    public function changeEstatusByFolio($estatus_cuenta_id, $folio_id)
    {
        $modelo = $this->modelo->where(CuentasPorCobrarModel::FOLIO_ID, $folio_id)->first();
        $modelo->estatus_cuenta_id = $estatus_cuenta_id;
        return $modelo->save();
    }

    public function getCuentasEnProcesoPago()
    {
        return $this->modelo->whereIn(CuentasPorCobrarModel::ESTATUS_CUENTA_ID, [EstatusCuentaModel::ESTATUS_ATRASADO, EstatusCuentaModel::ESTATUS_PROCESO])->get();
    }

    public function procesarCuentasPorCobrar($parametros)
    {
        //dd($parametros);
        $importe = $parametros[VentasRealizadasModel::VENTA_TOTAL];

        if ($parametros[CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID] == 2) {
            $tasa_interes = isset($parametros[CuentasPorCobrarModel::TASA_INTERES]) ? $parametros[CuentasPorCobrarModel::TASA_INTERES] : 0;
            $plazo_credito = $parametros[CuentasPorCobrarModel::PLAZO_CREDITO_ID];
            $enganche = isset($parametros[CuentasPorCobrarModel::ENGANCHE]) ? $parametros[CuentasPorCobrarModel::ENGANCHE] : 0;
            $totalsinenganche = $importe - $enganche;
            $cantidad_abonos = $this->modelo_plazo_credito->find($plazo_credito)->cantidad_mes;

            $abono_mensual = ($totalsinenganche / $cantidad_abonos);
            $interes_mensual = ($abono_mensual * $tasa_interes) / 100;
            $intereses = $interes_mensual * $cantidad_abonos;
            $total = $importe + $intereses;
        } else {
            $total = $parametros[VentasRealizadasModel::VENTA_TOTAL];
            $intereses = 0;
        }
        // validar si existe el folio antes de crearlo, checar esta funcionalidad para meterlo en el controlador donde se hacer la orden de servicio
        $folio = $parametros['folio_id'];
        $params = [
            CuentasPorCobrarModel::FOLIO_ID => $folio,
            CuentasPorCobrarModel::CLIENTE_ID => $parametros[CuentasPorCobrarModel::CLIENTE_ID],
            CuentasPorCobrarModel::CONCEPTO => $parametros[CuentasPorCobrarModel::CONCEPTO],
            CuentasPorCobrarModel::IMPORTE => $importe,
            CuentasPorCobrarModel::TOTAL => $total,
            CuentasPorCobrarModel::FECHA => isset($parametros[CuentasPorCobrarModel::FECHA]) ?  $parametros[CuentasPorCobrarModel::FECHA] : date('Y-m-d'),
            CuentasPorCobrarModel::USUARIO_GESTOR_ID => isset($parametros[CuentasPorCobrarModel::USUARIO_GESTOR_ID]) ?  $parametros[CuentasPorCobrarModel::USUARIO_GESTOR_ID] : null,
            CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID => $parametros[CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID],
            CuentasPorCobrarModel::TIPO_PAGO_ID => isset($parametros[CuentasPorCobrarModel::TIPO_PAGO_ID]) ? $parametros[CuentasPorCobrarModel::TIPO_PAGO_ID] : 10,
            CuentasPorCobrarModel::PLAZO_CREDITO_ID => isset($parametros[CuentasPorCobrarModel::PLAZO_CREDITO_ID]) ? $parametros[CuentasPorCobrarModel::PLAZO_CREDITO_ID] : 14,
            CuentasPorCobrarModel::ENGANCHE => isset($parametros[CuentasPorCobrarModel::ENGANCHE]) ?  $parametros[CuentasPorCobrarModel::ENGANCHE] : 0,
            CuentasPorCobrarModel::TASA_INTERES => isset($parametros[CuentasPorCobrarModel::TASA_INTERES]) ? $parametros[CuentasPorCobrarModel::TASA_INTERES] : 0,
            CuentasPorCobrarModel::INTERESES => $intereses,
            CuentasPorCobrarModel::ESTATUS_CUENTA_ID =>  isset($parametros[CuentasPorCobrarModel::ESTATUS_CUENTA_ID]) ? $parametros[CuentasPorCobrarModel::ESTATUS_CUENTA_ID] : EstatusCuentaModel::ESTATUS_POR_EMPEZAR,
            CuentasPorCobrarModel::VENTA_TOTAL_DESCUENTO => isset($parametros[CuentasPorCobrarModel::VENTA_TOTAL_DESCUENTO]) ? $parametros[CuentasPorCobrarModel::VENTA_TOTAL_DESCUENTO] : null

        ];
        //Verificamos si existe la cuenta por cobrar.
        $cuenta_por_cobrar = $this->getWhereInFirst(CuentasPorCobrarModel::FOLIO_ID, [$folio]);
        // Si no existe se crea
        if (!$cuenta_por_cobrar) {
            $params[CuentasPorCobrarModel::NUMERO_ORDEN] = isset($parametros[CuentasPorCobrarModel::NUMERO_ORDEN]) ? $parametros[CuentasPorCobrarModel::NUMERO_ORDEN] : '';
            $params[CuentasPorCobrarModel::COMENTARIOS] = isset($parametros[CuentasPorCobrarModel::COMENTARIOS]) ? $parametros[CuentasPorCobrarModel::COMENTARIOS] : '';
            $params[CuentasPorCobrarModel::OBSERVACIONES] = isset($parametros[CuentasPorCobrarModel::OBSERVACIONES]) ?  $parametros[CuentasPorCobrarModel::OBSERVACIONES] : '';

            $data = $this->crear($params);
            $cuenta_por_cobrar_id = $data->id;
            $abonos = $this->servicio_abonos->crearAbonosByCuentaId($cuenta_por_cobrar_id);
        } else {
            // Proceso de actualizacion de la cuenta y abonos.....
            $data = $this->massUpdateWhereId(CuentasPorCobrarModel::FOLIO_ID, $folio, $params);
            // $this->getWhereInFirst(CuentasPorCobrarModel::FOLIO_ID, [$folio]);
            $cuenta_por_cobrar_id = $cuenta_por_cobrar->id;
            $data_abonos = $this->servicio_abonos->getWhere(AbonosModel::CUENTA_POR_COBRAR_ID, $cuenta_por_cobrar_id);
            // SI ES COMPRA DE CONTADO ELIMINAMOS LOS ABONOS -- ESTO EN EL CASO DE QUE SE ELIGE PRIMERO A CREDITO Y DESPUÉS A CONTADO.
            if ($parametros[CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID] == 1) {
                foreach ($data_abonos as $val) {
                    $this->servicio_abonos->eliminar($val->id);
                }
                $abonos = $this->servicio_abonos->crearAbonosByCuentaId($cuenta_por_cobrar_id);
            } else {
                //FLUJO COMPRA A CREDITO---
                $abonos_pendientes = $this->servicio_abonos->getAbonosPendientesCredito($cuenta_por_cobrar_id);
                // Se identifican los abonos pendientes y se comparan con el plazo de credito si no son iguales se eliminan y se recalculan los abonos 
                //en caso contrario se actualiza el monto.
                if (count($abonos_pendientes) != $cantidad_abonos) {
                    foreach ($data_abonos as $val) {
                        $this->servicio_abonos->eliminar($val->id);
                    }
                    $abonos = $this->servicio_abonos->crearAbonosByCuentaId($cuenta_por_cobrar_id);
                } else {
                    foreach ($data_abonos as $val) {
                        if ($val->tipo_abono_id == 2) {
                            $total_sin_enganche = $total - $enganche;
                            $monto_abono_pendiente = $total_sin_enganche / count($abonos_pendientes);
                            $abonos =  $this->servicio_abonos->massUpdateWhereId('id', $val->id, [
                                AbonosModel::TOTAL_ABONO => round($monto_abono_pendiente, 2),
                            ]);
                        } else if ($val->tipo_abono_id == 1) {
                            $this->servicio_abonos->massUpdateWhereId('id', $val->id, [
                                AbonosModel::TOTAL_ABONO => round($enganche, 2),
                            ]);
                        }
                    }
                }
            }
        }
        // Se crean los abonos

        if (!$abonos) {
            return false;
        }
        return $data;
    }

    public function getByNumeroOrden($numero_orden)
    {
        return  $this->modelo->with(
            [
                CuentasPorCobrarModel::REL_CLIENTE,
                CuentasPorCobrarModel::REL_TIPO_PAGO,
                CuentasPorCobrarModel::REL_FORMA_PAGO,
                CuentasPorCobrarModel::REL_PLAZO_CREDITO,
                CuentasPorCobrarModel::REL_ESTATUS
            ]
        )
            ->where('numero_orden', $numero_orden)
            ->first();
    }

    public function getDataNumeroOrden($numero_orden)
    {
        return  $this->modelo->with(
            [
                CuentasPorCobrarModel::REL_ESTATUS
            ]
        )
            ->where('numero_orden', $numero_orden)
            ->first();
    }

    public function getCuentasPendientesPorClienteId($cliente_id)
    {
        $cuentas = $this->modelo->select(CuentasPorCobrarModel::ID, CuentasPorCobrarModel::TOTAL)
            ->where(CuentasPorCobrarModel::CLIENTE_ID, $cliente_id)
            ->whereIn(CuentasPorCobrarModel::ESTATUS_CUENTA_ID, [EstatusCuentaModel::ESTATUS_ATRASADO, EstatusCuentaModel::ESTATUS_PROCESO, EstatusCuentaModel::ESTATUS_POR_EMPEZAR])->get();
        $total = 0;
        foreach ($cuentas as $cuenta) {
            $monto_pagado = $this->servicio_abonos->getAbonosPendientesTodos($cuenta->id);
            $total = $cuenta->total - $monto_pagado + $total;
        }

        return $total;
    }

    public function getById(int $id)
    {
        return $this->modelo->with([
            CuentasPorCobrarModel::REL_FOLIO,
            CuentasPorCobrarModel::REL_CLIENTE
        ])
            ->find($id);
    }
    public function getDataByNumeroOrden(Request $request)
    {
        return CuentasResource::collection(
            $this->queryNumeroOrden($request)
        );
    }

    public function queryNumeroOrden(Request $request): Collection
    {
        return $this->modelo->with(
            [
                CuentasPorCobrarModel::REL_FOLIO,
                CuentasPorCobrarModel::REL_CLIENTE
            ]
        )
            ->where('numero_orden', $request->numero_orden)
            ->get();
    }
}
