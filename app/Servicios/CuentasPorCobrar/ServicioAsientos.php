<?php

namespace App\Servicios\CuentasPorCobrar;

use App\Servicios\Core\ServicioDB;
use App\Models\CuentasPorCobrar\AsientoModel;
use App\Servicios\Refacciones\ServicioCurl;
use App\Servicios\Polizas\ServicioMovimientos;
use App\Models\Caja\DescuentosModel;
use App\Models\Caja\AnticiposModel;
use App\Models\Contabilidad\CatalogoCuentasModel;
use App\Models\CuentasPorCobrar\CuentasPorCobrarModel;
use App\Models\CuentasPorCobrar\TipoPolizasModel;
use App\Models\CuentasPorCobrar\EstatusCuentaModel;
use App\Models\CuentasPorPagar\CuentasPorPagarModel;
use App\Models\Contabilidad\CatalogoProcesosModel;
use App\Models\Refacciones\VentasRealizadasModel;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\VentaProductoModel;
use App\Servicios\Contabilidad\ServicioCuentas;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Exceptions\ParametroHttpInvalidoException;
use DB;

class ServicioAsientos extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'Asientos';
        $this->modelo = new AsientoModel();
        $this->servicioCurl = new ServicioCurl();
        $this->servicioCuentas = new ServicioCuentas();
        $this->servicioMovimientos = new ServicioMovimientos();
        $this->modelo_descuentos = new DescuentosModel();
        $this->modelo_ventas = new VentasRealizadasModel();
        $this->modelo_venta_producto = new VentaProductoModel();
        $this->modelo_producto = new ProductosModel();
        $this->modelo_anticipos = new AnticiposModel();
    }

    public function getReglasGuardar()
    {
        return [
            AsientoModel::CONCEPTO => 'required',
            AsientoModel::TOTAL_PAGO => 'required|numeric',
            AsientoModel::CUENTA_ID => 'required|numeric|exists:catalogo_cuentas,id',
            AsientoModel::TIPO_ASIENTO_ID => 'required|numeric|exists:catalogo_cuentas,id',
            AsientoModel::FECHA => 'nullable|date',
            AsientoModel::FOLIO_ID => 'nullable|exists:folios,id',
            AsientoModel::CLAVE_POLIZA => 'nullable|exists:tipo_polizas,clave',
            AsientoModel::CLIENTE_ID => 'nullable|exists:clientes,id',
            AsientoModel::ESTATUS => 'nullable',
            AsientoModel::DEPARTAMENTO => 'nullable|numeric'
        ];
    }

    public function getReglasUpdate()
    {
        return [
            AsientoModel::NOMBRE => 'required',
            AsientoModel::REFERENCIA => 'required',
            AsientoModel::CONCEPTO => 'required',
            AsientoModel::TOTAL => 'required|numeric',
            AsientoModel::CUENTA_ID => 'required|numeric|exists:catalogo_cuentas,id',
            AsientoModel::TIPO_ASIENTO_ID => 'required|numeric|exists:catalogo_cuentas,id',
            AsientoModel::FECHA => 'nullable|date',
            AsientoModel::FOLIO_ID => 'nullable|exists:folios,id',
            AsientoModel::CLAVE_POLIZA => 'nullable|exists:tipo_polizas,clave'
        ];
    }

    public function getReglasPolizaHojalateria()
    {
        return [
            CuentasPorCobrarModel::CUENTA_POR_COBRAR_ID => 'required|exists:cuentas_por_cobrar,id',
            AsientoModel::TOTAL_REFACCIONES => 'nullable|numeric',
            AsientoModel::TOTAL_MANO_OBRA => 'nullable|numeric',
            AsientoModel::TOTAL_CLIENTE_SERVICIOS => 'nullable|numeric',
            AsientoModel::TOTAL_INVENTARIO_PROCESO => 'nullable|numeric',
            AsientoModel::TOTAL_IVA_FACTURADO => 'nullable|numeric',
            AsientoModel::TOTAL_COSTO_REFACCION => 'nullable|numeric',
            AsientoModel::TOTAL_DESCUENTO_REFACCION => 'nullable|numeric'
        ];
    }

    public function getReglasPolizaVentaMostrador()
    {
        return [
            CuentasPorCobrarModel::CUENTA_POR_COBRAR_ID => 'required|exists:cuentas_por_cobrar,id',
            AsientoModel::CUENTA_BANCARIA_ID => 'required|numeric'
        ];
    }

    public function getReglasPolizaVentaMostradorCredito()
    {
        return [
            CuentasPorCobrarModel::CUENTA_POR_COBRAR_ID => 'required|exists:cuentas_por_cobrar,id',
        ];
    }

    public function getReglasPolizaDevolucionServicio()
    {
        return [
            CuentasPorCobrarModel::CUENTA_POR_COBRAR_ID => 'required|exists:cuentas_por_cobrar,id',
        ];
    }

    public function getReglasPolizaDevolucionMostrador()
    {
        return [
            CuentasPorCobrarModel::CUENTA_POR_COBRAR_ID => 'required|exists:cuentas_por_cobrar,id',
        ];
    }

    public function getReglasPolizaGarantias()
    {
        return [
            CuentasPorCobrarModel::CUENTA_POR_COBRAR_ID => 'required|exists:cuentas_por_cobrar,id',
            AsientoModel::TOTAL_REFACCIONES => 'nullable|numeric',
            AsientoModel::TOTAL_MANO_OBRA => 'nullable|numeric',
            AsientoModel::TOTAL_GARANTIAS_POR_COBRAR => 'nullable|numeric',
            AsientoModel::TOTAL_INVENTARIO_PROCESO => 'nullable|numeric',
            AsientoModel::TOTAL_IVA_FACTURADO => 'nullable|numeric',
            AsientoModel::TOTAL_COSTO_REFACCION => 'nullable|numeric',
            AsientoModel::TOTAL_DESCUENTO_REFACCION => 'nullable|numeric'
        ];
    }

    public function getReglasPolizaServicio()
    {
        return [
            CuentasPorCobrarModel::CUENTA_POR_COBRAR_ID => 'required|exists:cuentas_por_cobrar,id',
            AsientoModel::TOTAL_REFACCIONES => 'nullable|numeric',
            AsientoModel::TOTAL_MANO_OBRA => 'nullable|numeric',
            AsientoModel::TOTAL_CLIENTE_SERVICIOS => 'nullable|numeric',
            AsientoModel::TOTAL_INVENTARIO_PROCESO => 'nullable|numeric',
            AsientoModel::TOTAL_IVA_FACTURADO => 'nullable|numeric',
            AsientoModel::TOTAL_COSTO_REFACCION => 'nullable|numeric',
            AsientoModel::TOTAL_DESCUENTO_REFACCION => 'nullable|numeric'
        ];
    }

    public function getReglasActualizaPolizaServicio()
    {
        return [
            CuentasPorCobrarModel::CUENTA_POR_COBRAR_ID => 'required|exists:cuentas_por_cobrar,id',
            DescuentosModel::TIPO_DESCUENTO => 'required|numeric'
        ];
    }

    public function getReglasPolizaServicioInterno()
    {
        return [
            CuentasPorCobrarModel::CUENTA_POR_COBRAR_ID => 'required|exists:cuentas_por_cobrar,id',
            AsientoModel::PROCESOS => 'nullable|numeric',
            AsientoModel::ACONDICIONAMIENTO => 'nullable|numeric',
            AsientoModel::CORTESIA_NUEVOS => 'nullable|numeric'
        ];
    }

    public function getReglasPolizaRefaccionTaller()
    {
        return [
            CuentasPorCobrarModel::CUENTA_POR_COBRAR_ID => 'required|exists:cuentas_por_cobrar,id',
            AsientoModel::TOTAL_COSTO_PIEZA => 'required|numeric'
        ];
    }

    public function getReglasPolizaCompraFordPlanta()
    {
        return [
            CuentasPorPagarModel::CUENTA_POR_PAGAR_ID => 'required|exists:cuentas_por_pagar,id',
            AsientoModel::TOTAL_FACTURA_SIN_IVA => 'required|numeric',
            AsientoModel::TOTAL_IVA_FACTURA => 'required|numeric',
            AsientoModel::TOTAL_FACTURA => 'required|numeric'
        ];
    }

    public function getReglasPolizaCompraProveedorExterno()
    {
        return [
            CuentasPorPagarModel::CUENTA_POR_PAGAR_ID => 'required|exists:cuentas_por_pagar,id',
            AsientoModel::TOTAL_FACTURA_SIN_IVA => 'required|numeric',
            AsientoModel::TOTAL_IVA_FACTURA => 'required|numeric',
            AsientoModel::TOTAL_FACTURA => 'required|numeric'
        ];
    }

    public function getReglasAplicarCuentas()
    {
        return [
            CuentasPorCobrarModel::CUENTA_POR_COBRAR_ID => 'required|exists:cuentas_por_cobrar,id',
        ];
    }

    public function servicioPolizaVentaMostrador($request)
    {
        $cuenta_por_cobrar = $this->servicioMovimientos->getTipoCuentaxCobrar($request['cuenta_por_cobrar_id']);
        $venta = $this->modelo_ventas->where(AsientoModel::FOLIO_ID, [$cuenta_por_cobrar['folio_id']])->first();
        $respuesta = [];

        $venta_productos = $this->getVentaProductos($venta['id']);
        $anticipos = $this->modelo_anticipos->where(AnticiposModel::FOLIO_APLICADO, $cuenta_por_cobrar['folio_id'])->first();

        if ($anticipos) {
            $data[AsientoModel::CONCEPTO] = isset($cuenta_por_cobrar['concepto']) ? $cuenta_por_cobrar['concepto'] : '';
            $data[AsientoModel::CLAVE_POLIZA] = env('SUCURSAL_DMS') == 'QUERETARO' ? TipoPolizasModel::CM : TipoPolizasModel::VR;
            $data[AsientoModel::ESTATUS] = 'POR_APLICAR';
            $data[AsientoModel::CLIENTE_ID] = isset($cuenta_por_cobrar['cliente_id']) ? $cuenta_por_cobrar['cliente_id'] : '';
            $data[AsientoModel::CLIENTE_NOMBRE] = isset($cuenta_por_cobrar['cliente_nombre']) ? $cuenta_por_cobrar['cliente_nombre'] : '';
            $data[AsientoModel::CLIENTE_APELLIDO1] = isset($cuenta_por_cobrar['cliente_apellido1']) ? $cuenta_por_cobrar['cliente_apellido1'] : '';
            $data[AsientoModel::CLIENTE_APELLIDO2] = isset($cuenta_por_cobrar['cliente_apellido2']) ? $cuenta_por_cobrar['cliente_apellido2'] : '';
            $data[AsientoModel::FECHA] = isset($cuenta_por_cobrar['fecha']) ? $cuenta_por_cobrar['fecha'] : '';
            $data[AsientoModel::FOLIO_ID] = isset($cuenta_por_cobrar['folio_id']) ? $cuenta_por_cobrar['folio_id'] : '';
            $data[AsientoModel::DEPARTAMENTO] = AsientoModel::ANTICIPOS;
            $data[AsientoModel::PROCEDENCIA] = AsientoModel::PROCEDENCIA_VENTA_MOSTRADOR;

            $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
            $data['cuenta_id'] = env('SUCURSAL_DMS') == 'QUERETARO' ? CatalogoCuentasModel::CUENTA_110003 : CatalogoCuentasModel::CUENTA_110003;
            $data['total_pago'] = isset($anticipos) ? $anticipos->total : 0;
            $respuesta[] = $this->curl_asiento_api($data);

            $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
            $data['cuenta_id'] = $request['cuenta_bancaria_id'];
            $data['total_pago'] = isset($anticipos) ? $anticipos->total : 0;
            $respuesta[] = $this->curl_asiento_api($data);
        }

        if ($venta_productos) {
            foreach ($venta_productos as $key => $val) {
                $subtotal = $val['venta_total'];
                $iva = $subtotal * 0.16;
                $total = $subtotal + $iva;
                $data[AsientoModel::CONCEPTO] = $val->no_identificacion . ' - ' . $val->nombre_producto;
                $data[AsientoModel::CLAVE_POLIZA] = env('SUCURSAL_DMS') == 'QUERETARO' ? TipoPolizasModel::CM : TipoPolizasModel::VR;
                $data[AsientoModel::ESTATUS] = 'POR_APLICAR';
                $data[AsientoModel::CLIENTE_ID] = isset($cuenta_por_cobrar['cliente_id']) ? $cuenta_por_cobrar['cliente_id'] : '';
                $data[AsientoModel::CLIENTE_NOMBRE] = isset($cuenta_por_cobrar['cliente_nombre']) ? $cuenta_por_cobrar['cliente_nombre'] : '';
                $data[AsientoModel::CLIENTE_APELLIDO1] = isset($cuenta_por_cobrar['cliente_apellido1']) ? $cuenta_por_cobrar['cliente_apellido1'] : '';
                $data[AsientoModel::CLIENTE_APELLIDO2] = isset($cuenta_por_cobrar['cliente_apellido2']) ? $cuenta_por_cobrar['cliente_apellido2'] : '';
                $data[AsientoModel::FECHA] = isset($cuenta_por_cobrar['fecha']) ? $cuenta_por_cobrar['fecha'] : '';
                $data[AsientoModel::FOLIO_ID] = isset($cuenta_por_cobrar['folio_id']) ? $cuenta_por_cobrar['folio_id'] : '';
                $data[AsientoModel::DEPARTAMENTO] = AsientoModel::VENTA_MOSTRADOR;
                $data[AsientoModel::PROCEDENCIA] = AsientoModel::PROCEDENCIA_VENTA_MOSTRADOR;
                $data[AsientoModel::COSTO_PROMEDIO] = $val->cantidad * $val->costo_promedio;
                $data[AsientoModel::CANTIDAD] = $val->cantidad;

                $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
                $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_330001;
                $data['total_pago'] = $subtotal;
                $respuesta[] = $this->curl_asiento_api($data);

                $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
                $data['cuenta_id'] = $request['cuenta_bancaria_id'];
                $data['total_pago'] = $total;
                $respuesta[] = $this->curl_asiento_api($data);

                $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
                $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_240012;
                $data['total_pago'] = $iva;
                $respuesta[] = $this->curl_asiento_api($data);

                $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
                $data['cuenta_id'] = env('SUCURSAL_DMS') == 'QUERETARO' ? CatalogoCuentasModel::CUENTA_440102 : CatalogoCuentasModel::CUENTA_440001;
                $data['total_pago'] = $val->cantidad * $val->costo_promedio;
                $respuesta[] = $this->curl_asiento_api($data);

                $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
                $data['cuenta_id'] = env('SUCURSAL_DMS') == 'QUERETARO' ? CatalogoCuentasModel::CUENTA_131000 : CatalogoCuentasModel::CUENTA_132000;
                $data['total_pago'] = $val->cantidad * $val->costo_promedio;
                $respuesta[] = $this->curl_asiento_api($data);
            }
        }
        return $respuesta;
    }

    private function getVentaProductos($venta_id)
    {
        return $this->modelo_venta_producto
            ->select('venta_producto.cantidad', 'venta_producto.costo_promedio', 'venta_producto.venta_total', 'producto.costo_promedio as producto_costo_promedio', 'producto.no_identificacion', 'producto.descripcion as nombre_producto')
            ->leftJoin('producto', 'producto.id', '=', 'venta_producto.producto_id')
            ->where(VentaProductoModel::VENTA_ID, $venta_id)->get();
    }

    /* SOLO SUCURSAL QUERETARO */
    public function servicioPolizaVentaMostradorCredito($request)
    {
        $cuenta_por_cobrar = $this->servicioMovimientos->getTipoCuentaxCobrar($request['cuenta_por_cobrar_id']);
        $venta = $this->modelo_ventas->where(AsientoModel::FOLIO_ID, [$cuenta_por_cobrar['folio_id']])->first();

        $venta_productos = $this->getVentaProductos($venta['id']);

        $respuesta = [];
        if ($venta_productos) {
            foreach ($venta_productos as $key => $val) {
                $subtotal = $val['venta_total'];
                $iva = $subtotal * 0.16;
                $total = $subtotal + $iva;
                $data[AsientoModel::CONCEPTO] = $val->no_identificacion . ' - ' . $val->nombre_producto;
                $data[AsientoModel::CLAVE_POLIZA] = TipoPolizasModel::CM;
                $data[AsientoModel::ESTATUS] = 'APLICADO';
                $data[AsientoModel::CLIENTE_ID] = isset($cuenta_por_cobrar['cliente_id']) ? $cuenta_por_cobrar['cliente_id'] : '';
                $data[AsientoModel::CLIENTE_NOMBRE] = isset($cuenta_por_cobrar['cliente_nombre']) ? $cuenta_por_cobrar['cliente_nombre'] : '';
                $data[AsientoModel::CLIENTE_APELLIDO1] = isset($cuenta_por_cobrar['cliente_apellido1']) ? $cuenta_por_cobrar['cliente_apellido1'] : '';
                $data[AsientoModel::CLIENTE_APELLIDO2] = isset($cuenta_por_cobrar['cliente_apellido2']) ? $cuenta_por_cobrar['cliente_apellido2'] : '';
                $data[AsientoModel::FECHA] = isset($cuenta_por_cobrar['fecha']) ? $cuenta_por_cobrar['fecha'] : '';
                $data[AsientoModel::FOLIO_ID] = isset($cuenta_por_cobrar['folio_id']) ? $cuenta_por_cobrar['folio_id'] : '';
                $data[AsientoModel::DEPARTAMENTO] = AsientoModel::VENTA_MOSTRADOR;
                $data[AsientoModel::PROCEDENCIA] = AsientoModel::PROCEDENCIA_VENTA_MOSTRADOR;
                $data[AsientoModel::COSTO_PROMEDIO] = $val->cantidad * $val->costo_promedio;
                $data[AsientoModel::CANTIDAD] = $val->cantidad;

                $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
                $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_340101;
                $data['total_pago'] = $subtotal ? $subtotal : 0;
                $respuesta[] = $this->curl_asiento_api($data);

                $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
                $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_110003;
                $data['total_pago'] = $total;
                $respuesta[] = $this->curl_asiento_api($data);

                $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
                $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_240013;
                $data['total_pago'] = $iva;
                $respuesta[] = $this->curl_asiento_api($data);

                $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
                $data['cuenta_id'] = env('SUCURSAL_DMS') == 'QUERETARO' ? CatalogoCuentasModel::CUENTA_440102 : CatalogoCuentasModel::CUENTA_440001;
                $data['total_pago'] = $val->cantidad * $val->costo_promedio;
                $respuesta[] = $this->curl_asiento_api($data);

                $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
                $data['cuenta_id'] = env('SUCURSAL_DMS') == 'QUERETARO' ? CatalogoCuentasModel::CUENTA_131000 : CatalogoCuentasModel::CUENTA_132000;
                $data['total_pago'] = $val->cantidad * $val->costo_promedio;
                $respuesta[] = $this->curl_asiento_api($data);
            }
        }
        return $respuesta;
    }

    public function servicioPolizaAnticipo($folio_id)
    {
        $anticipos = $this->modelo_anticipos->where(AnticiposModel::FOLIO_ID, $folio_id)->first();

        $respuesta = [];
        $data[AsientoModel::CONCEPTO] = 'Anticipo aplicado a folio ' . $anticipos['folio_apicado'];
        $data[AsientoModel::CLAVE_POLIZA] = TipoPolizasModel::CG;
        $data[AsientoModel::ESTATUS] = 'APLICADO';
        $data[AsientoModel::CLIENTE_ID] = isset($anticipos['cliente_id']) ? $anticipos['cliente_id'] : '';
        $data[AsientoModel::FECHA] = isset($anticipos['fecha']) ? $anticipos['fecha'] : '';
        $data[AsientoModel::FOLIO_ID] = isset($anticipos['folio_id']) ? $anticipos['folio_id'] : '';
        $data[AsientoModel::DEPARTAMENTO] = AsientoModel::VENTA_MOSTRADOR;
        $data[AsientoModel::PROCEDENCIA] = AsientoModel::PROCEDENCIA_VENTA_MOSTRADOR;

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_110003;
        $data['total_pago'] = isset($anticipos['total']) ? $anticipos['total'] : '';
        $respuesta[] = $this->curl_asiento_api($data);

        return $respuesta;
    }

    private function calculaCostoPromedio($venta_productos)
    {
        $costo_refaccion = 0;
        foreach ($venta_productos as $val) {
            if ($val->costo_promedio && $val->costo_promedio > 0) {
                $costo_refaccion += $val->cantidad * $val->costo_promedio;
            } else {
                $costo_refaccion += $val->cantidad * $val->producto_costo_promedio;
            }
        }
        return $costo_refaccion;
    }

    /* SOLO SUCURSAL QUERETARO */
    public function servicioPolizaHojalateria($request)
    {
        $cuenta_por_cobrar = $this->servicioMovimientos->getTipoCuentaxCobrar($request['cuenta_por_cobrar_id']);
        $api = $this->servicioCurl->curlGet(env('URL_CONTABILIDAD') . 'asientos/api/detalle?folio=' . $cuenta_por_cobrar->folio_id, false);
        $data_asientos = json_decode($api);

        if ($data_asientos->data != false) {
            throw new ParametroHttpInvalidoException([
                'mensaje' => 'Ya existen asientos creados para esta cuenta'
            ]);
        }
        if ($cuenta_por_cobrar->tipo_proceso_id != CatalogoProcesosModel::PROCESO_HOJALATERIA) {
            throw new ParametroHttpInvalidoException([
                'mensaje' => 'La cuenta no es del tipo HOJALATERIA'
            ]);
        } else if ($cuenta_por_cobrar->estatus_cuenta_id != EstatusCuentaModel::ESTATUS_POR_EMPEZAR) {
            throw new ParametroHttpInvalidoException([
                'mensaje' => 'La cuenta ya se encuentra en proceso de pago, no sé pueden asignar asientos'
            ]);
        }
        $respuesta = [];
        $data[AsientoModel::CONCEPTO] = isset($cuenta_por_cobrar['concepto']) ? $cuenta_por_cobrar['concepto'] : '';
        $data[AsientoModel::CLAVE_POLIZA] = TipoPolizasModel::QH;
        $data[AsientoModel::ESTATUS] = 'POR_APLICAR';
        $data[AsientoModel::CLIENTE_ID] = isset($cuenta_por_cobrar['cliente_id']) ? $cuenta_por_cobrar['cliente_id'] : '';
        $data[AsientoModel::CLIENTE_NOMBRE] = isset($cuenta_por_cobrar['cliente_nombre']) ? $cuenta_por_cobrar['cliente_nombre'] : '';
        $data[AsientoModel::CLIENTE_APELLIDO1] = isset($cuenta_por_cobrar['cliente_apellido1']) ? $cuenta_por_cobrar['cliente_apellido1'] : '';
        $data[AsientoModel::CLIENTE_APELLIDO2] = isset($cuenta_por_cobrar['cliente_apellido2']) ? $cuenta_por_cobrar['cliente_apellido2'] : '';
        $data[AsientoModel::FECHA] = isset($cuenta_por_cobrar['fecha']) ? $cuenta_por_cobrar['fecha'] : '';
        $data[AsientoModel::FOLIO_ID] = isset($cuenta_por_cobrar['folio_id']) ? $cuenta_por_cobrar['folio_id'] : '';
        $data[AsientoModel::DEPARTAMENTO] = AsientoModel::HOJALATERIA;
        $data[AsientoModel::PROCEDENCIA] = AsientoModel::PROCEDENCIA_SERVICIOS;

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_370002;
        $data['total_pago'] = $request[AsientoModel::TOTAL_REFACCIONES];
        $respuesta[CatalogoCuentasModel::CUENTA_370002] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_370003;
        $data['total_pago'] = $request[AsientoModel::TOTAL_MANO_OBRA];
        $respuesta[CatalogoCuentasModel::CUENTA_370003] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_110004;
        $data['total_pago'] = $request[AsientoModel::TOTAL_CLIENTE_SERVICIOS];
        $respuesta[CatalogoCuentasModel::CUENTA_110004] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_151000;
        $data['total_pago'] = $request[AsientoModel::TOTAL_INVENTARIO_PROCESO];
        $respuesta[CatalogoCuentasModel::CUENTA_151000] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_240013;
        $data['total_pago'] = $request[AsientoModel::TOTAL_IVA_FACTURADO];
        $respuesta[CatalogoCuentasModel::CUENTA_240013] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_470001;
        $data['total_pago'] = $request[AsientoModel::TOTAL_COSTO_REFACCION];
        $respuesta[CatalogoCuentasModel::CUENTA_470001] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_370004;
        $data['total_pago'] = $request[AsientoModel::TOTAL_DESCUENTO_REFACCION];
        $respuesta[CatalogoCuentasModel::CUENTA_370004] = $this->curl_asiento_api($data);

        return $respuesta;
    }

    public function servicioPolizaServicios($request)
    {
        $cuenta_por_cobrar = $this->servicioMovimientos->getTipoCuentaxCobrar($request['cuenta_por_cobrar_id']);
        $api = $this->servicioCurl->curlGet(env('URL_CONTABILIDAD') . 'asientos/api/detalle?folio=' . $cuenta_por_cobrar->folio_id, false);
        $data_asientos = json_decode($api);
        if ($data_asientos->data != false) {
            throw new ParametroHttpInvalidoException([
                'mensaje' => 'Ya existen asientos creados para esta cuenta'
            ]);
        }
        if ($cuenta_por_cobrar->tipo_proceso_id != CatalogoProcesosModel::PROCESO_SERVICIOS) {
            throw new ParametroHttpInvalidoException([
                'mensaje' => 'La cuenta no es del tipo SERVICIO'
            ]);
        }

        $respuesta = [];
        $data[AsientoModel::CONCEPTO] = isset($cuenta_por_cobrar['concepto']) ? $cuenta_por_cobrar['concepto'] : '';
        $data[AsientoModel::CLAVE_POLIZA] = env('SUCURSAL_DMS') == 'QUERETARO' ? TipoPolizasModel::QT : TipoPolizasModel::OT;
        $data[AsientoModel::ESTATUS] = 'POR_APLICAR';
        $data[AsientoModel::CLIENTE_ID] = isset($cuenta_por_cobrar['cliente_id']) ? $cuenta_por_cobrar['cliente_id'] : '';
        $data[AsientoModel::CLIENTE_NOMBRE] = isset($cuenta_por_cobrar['cliente_nombre']) ? $cuenta_por_cobrar['cliente_nombre'] : '';
        $data[AsientoModel::CLIENTE_APELLIDO1] = isset($cuenta_por_cobrar['cliente_apellido1']) ? $cuenta_por_cobrar['cliente_apellido1'] : '';
        $data[AsientoModel::CLIENTE_APELLIDO2] = isset($cuenta_por_cobrar['cliente_apellido2']) ? $cuenta_por_cobrar['cliente_apellido2'] : '';
        $data[AsientoModel::FECHA] = isset($cuenta_por_cobrar['fecha']) ? $cuenta_por_cobrar['fecha'] : '';
        $data[AsientoModel::FOLIO_ID] = isset($cuenta_por_cobrar['folio_id']) ? $cuenta_por_cobrar['folio_id'] : '';
        $data[AsientoModel::DEPARTAMENTO] = AsientoModel::SERVICIOS;
        $data[AsientoModel::PROCEDENCIA] = AsientoModel::PROCEDENCIA_SERVICIOS;

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = env('SUCURSAL_DMS') == 'QUERETARO' ? CatalogoCuentasModel::CUENTA_350001 : CatalogoCuentasModel::CUENTA_360001;
        $data['total_pago'] = $request[AsientoModel::TOTAL_REFACCIONES];
        $respuesta[] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = env('SUCURSAL_DMS') == 'QUERETARO' ? CatalogoCuentasModel::CUENTA_350003 : CatalogoCuentasModel::CUENTA_360003;
        $data['total_pago'] = $request[AsientoModel::TOTAL_MANO_OBRA];
        $respuesta[CatalogoCuentasModel::CUENTA_350003] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data['cuenta_id'] = env('SUCURSAL_DMS') == 'QUERETARO' ? CatalogoCuentasModel::CUENTA_110004 : CatalogoCuentasModel::CUENTA_110004;
        $data['total_pago'] = $request[AsientoModel::TOTAL_CLIENTE_SERVICIOS];
        $respuesta[] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = env('SUCURSAL_DMS') == 'QUERETARO' ? CatalogoCuentasModel::CUENTA_151000 : CatalogoCuentasModel::CUENTA_152000;
        $data['total_pago'] = $request[AsientoModel::TOTAL_INVENTARIO_PROCESO];
        $respuesta[] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = env('SUCURSAL_DMS') == 'QUERETARO' ? CatalogoCuentasModel::CUENTA_240013 : CatalogoCuentasModel::CUENTA_240013;
        $data['total_pago'] = $request[AsientoModel::TOTAL_IVA_FACTURADO];
        $respuesta[] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data['cuenta_id'] = env('SUCURSAL_DMS') == 'QUERETARO' ? CatalogoCuentasModel::CUENTA_450001 : CatalogoCuentasModel::CUENTA_460001;
        $data['total_pago'] = $request[AsientoModel::TOTAL_COSTO_REFACCION];
        $respuesta[] = $this->curl_asiento_api($data);

        if (!empty($request[AsientoModel::TOTAL_DESCUENTO_REFACCION])) {
            $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
            $data['cuenta_id'] = env('SUCURSAL_DMS') == 'QUERETARO' ? CatalogoCuentasModel::CUENTA_350008 : CatalogoCuentasModel::CUENTA_350008;
            $data['total_pago'] = $request[AsientoModel::TOTAL_DESCUENTO_REFACCION];
            $respuesta[] = $this->curl_asiento_api($data);
        }

        return $respuesta;
    }

    public function getByTotalDescuentosByFolioId($folio_id)
    {
        return $this->modelo_descuentos->select(
            DB::raw('sum(descuentos.cantidad_descontada) as suma_descuento')
        )
            ->where([
                DescuentosModel::FOLIO_ID => $folio_id,
            ])->groupBy(
                DescuentosModel::getTableName() . '.' . DescuentosModel::FOLIO_ID
            )
            ->first();
    }

    public function getDescuentoByFolioAndTipo($folio_id, $tipo)
    {
        return $this->modelo_descuentos
            ->where([
                DescuentosModel::FOLIO_ID => $folio_id,
                DescuentosModel::TIPO_DESCUENTO => $tipo
            ])->first();
    }

    public function servicioActualizaPoliza($cuenta_por_cobrar_id, $tipo_descuento)
    {
        $cuenta_por_cobrar = $this->servicioMovimientos->getTipoCuentaxCobrar($cuenta_por_cobrar_id);
        $data['descuentos'] = $this->getByTotalDescuentosByFolioId($cuenta_por_cobrar['folio_id']);

        $api_descuento_refaccion = $this->getDescuentoByFolioAndTipo($cuenta_por_cobrar['folio_id'], 1);
        $api_descuento_mano_obra = $this->getDescuentoByFolioAndTipo($cuenta_por_cobrar['folio_id'], 2);
        $api_descuento_general = $this->getDescuentoByFolioAndTipo($cuenta_por_cobrar['folio_id'], 3);
        //validar si es descuento 3 en el case
        // $data = $cuenta_por_cobrar_id;
        $data['descuento_refaccion'] = $api_descuento_refaccion ? $api_descuento_refaccion->cantidad_descontada : 0;
        $data['descuento_mano_obra'] = $api_descuento_mano_obra ? $api_descuento_mano_obra->cantidad_descontada : 0;
        $data['descuento_general'] = $api_descuento_general ? $api_descuento_general->cantidad_descontada : 0;
        $data['tipo_descuento'] = $tipo_descuento;
        $respuesta = [];
        $data_asiento[AsientoModel::CONCEPTO] = isset($cuenta_por_cobrar['concepto']) ? $cuenta_por_cobrar['concepto'] : '';
        $data_asiento[AsientoModel::CLAVE_POLIZA] = TipoPolizasModel::QT;
        $data_asiento[AsientoModel::ESTATUS] = 'POR_APLICAR';
        $data_asiento[AsientoModel::CLIENTE_ID] = isset($cuenta_por_cobrar['cliente_id']) ? $cuenta_por_cobrar['cliente_id'] : '';
        $data_asiento[AsientoModel::CLIENTE_NOMBRE] = isset($cuenta_por_cobrar['cliente_nombre']) ? $cuenta_por_cobrar['cliente_nombre'] : '';
        $data_asiento[AsientoModel::CLIENTE_APELLIDO1] = isset($cuenta_por_cobrar['cliente_apellido1']) ? $cuenta_por_cobrar['cliente_apellido1'] : '';
        $data_asiento[AsientoModel::CLIENTE_APELLIDO2] = isset($cuenta_por_cobrar['cliente_apellido2']) ? $cuenta_por_cobrar['cliente_apellido2'] : '';
        $data_asiento[AsientoModel::FECHA] = isset($cuenta_por_cobrar['fecha']) ? $cuenta_por_cobrar['fecha'] : '';
        $data_asiento[AsientoModel::FOLIO_ID] = isset($cuenta_por_cobrar['folio_id']) ? $cuenta_por_cobrar['folio_id'] : '';
        if ($cuenta_por_cobrar->tipo_proceso_id == CatalogoProcesosModel::PROCESO_SERVICIOS) {
            $data_asiento[AsientoModel::DEPARTAMENTO] = AsientoModel::SERVICIOS;
        } else if ($cuenta_por_cobrar->tipo_proceso_id == CatalogoProcesosModel::PROCESO_HOJALATERIA) {
            $data_asiento[AsientoModel::DEPARTAMENTO] = CatalogoProcesosModel::PROCESO_HOJALATERIA;
        }

        switch ($cuenta_por_cobrar['tipo_proceso_id']) {
            case CatalogoProcesosModel::PROCESO_SERVICIOS:
                $respuesta = $this->actualizaProcesoServicios($data, $data_asiento);
                break;
            case CatalogoProcesosModel::PROCESO_HOJALATERIA:
                $respuesta = $this->actualizaProcesoHojalateria($data, $data_asiento);
                break;
            default:
                break;
        }

        return $respuesta;
    }
    public function actualizaProcesoServicios($data, $data_asiento)
    {
        $respuesta = [];
        $contabilidad_api = $this->servicioCurl->curlPost(env('URL_CONTABILIDAD') . 'asientos/api/aplicar_folio', [
            'folio' => $data_asiento[AsientoModel::FOLIO_ID],
            'estatus' => 'ANULADO',
            'sucursal_id' => env('SUCURSAL_DMS') == 'QUERETARO' ? AsientoModel::SUCURSAL_QUERETARO : AsientoModel::SUCURSAL_SANJUAN
        ], false);
        $apijson = json_decode($contabilidad_api);
        if (!$apijson) {
            throw new ParametroHttpInvalidoException([
                'msg' => "Error al ANULAR los asientos"
            ]);
        } else {
            $descuento_general_refaccion = 0;
            $refacciones = $this->curl_asiento_api_detalle($data_asiento[AsientoModel::FOLIO_ID], env('SUCURSAL_DMS') == 'QUERETARO' ? 350001 : 360001);
            $mano_obra = $this->curl_asiento_api_detalle($data_asiento[AsientoModel::FOLIO_ID], env('SUCURSAL_DMS') == 'QUERETARO' ? 350003 : 360003);
            $costo_refaccion = $this->curl_asiento_api_detalle($data_asiento[AsientoModel::FOLIO_ID], env('SUCURSAL_DMS') == 'QUERETARO' ? 450001 : 450001);
            $inventario = $this->curl_asiento_api_detalle($data_asiento[AsientoModel::FOLIO_ID], env('SUCURSAL_DMS') == 'QUERETARO' ? 151000 : 151000);
            $total_iva_facturado = ((($refacciones->abono + $mano_obra->abono) - $data['descuentos']->suma_descuento) * 0.16);
            $total_iva_facturado = ((($refacciones->abono + $mano_obra->abono) - $data['descuentos']->suma_descuento) * 0.16);
            $data_refacciones['asiento_id'] = $refacciones->asiento_id;
            if ($data['tipo_descuento'] == 3) {
                $descuento_general_refaccion = $data['descuento_general'];
            }
            $data_asiento[AsientoModel::PROCEDENCIA] = AsientoModel::PROCEDENCIA_SERVICIOS;
            $data_asiento[AsientoModel::CANTIDAD] = 1;

            $data_asiento['total_pago'] = $refacciones->abono; 
            $data_asiento['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
            $data_asiento['cuenta_id'] = CatalogoCuentasModel::CUENTA_350001;
            $respuesta['350001'] = $this->curl_asiento_api($data_asiento);

            $data_asiento['total_pago'] = $mano_obra->abono; 
            $data_asiento['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
            $data_asiento['cuenta_id'] = CatalogoCuentasModel::CUENTA_350003;
            $respuesta['350003'] = $this->curl_asiento_api($data_asiento);
            //CALCULO NUEVO
            $data_asiento['total_pago'] = (($refacciones->abono + $mano_obra->abono + $total_iva_facturado) - $data['descuentos']->suma_descuento);
            $data_asiento['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
            $data_asiento['cuenta_id'] = CatalogoCuentasModel::CUENTA_110004;
            $respuesta['110004'] = $this->curl_asiento_api($data_asiento);
            //CALCULO NUEVO
            $data_asiento['total_pago'] = $total_iva_facturado;
            $data_asiento['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
            $data_asiento['cuenta_id'] = CatalogoCuentasModel::CUENTA_240013;
            $respuesta['240013'] = $this->curl_asiento_api($data_asiento);

            $data_asiento['total_pago'] = $inventario->abono;
            $data_asiento['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
            $data_asiento['cuenta_id'] = CatalogoCuentasModel::CUENTA_151000;
            $respuesta['151000'] = $this->curl_asiento_api($data_asiento);

            $data_asiento['total_pago'] = $costo_refaccion->cargo;
            $data_asiento['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
            $data_asiento['cuenta_id'] = CatalogoCuentasModel::CUENTA_450001;
            $respuesta['450001'] = $this->curl_asiento_api($data_asiento);

            if ($data['tipo_descuento'] == 1) {
                $data_asiento['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
                $data_asiento['cuenta_id'] = env('SUCURSAL_DMS') == 'QUERETARO' ? CatalogoCuentasModel::CUENTA_350008 : CatalogoCuentasModel::CUENTA_360008;;
                $data_asiento['total_pago'] = $data['descuento_refaccion'];
                $respuesta['350007_'] = $this->curl_asiento_api($data_asiento);
            }

            if ($data['tipo_descuento'] == 2) {
                $data_asiento['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
                $data_asiento['cuenta_id'] = env('SUCURSAL_DMS') == 'QUERETARO' ? CatalogoCuentasModel::CUENTA_350007 : CatalogoCuentasModel::CUENTA_360007;;
                $data_asiento['total_pago'] = $data['descuento_mano_obra'];
                $respuesta['350007'] = $this->curl_asiento_api($data_asiento);
            }

            if ($data['tipo_descuento'] == 3) {
                $data_asiento['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
                $data_asiento['cuenta_id'] = env('SUCURSAL_DMS') == 'QUERETARO' ? CatalogoCuentasModel::CUENTA_350008 : CatalogoCuentasModel::CUENTA_360008;;
                $data_asiento['total_pago'] = $descuento_general_refaccion;
                $respuesta['350008'] = $this->curl_asiento_api($data_asiento);
            }
            return $respuesta;
        }
    }

    public function actualizaProcesoHojalateria($data, $data_asiento)
    {
        $refacciones = $this->curl_asiento_api_detalle($data_asiento[AsientoModel::FOLIO_ID], 370002);
        $data_refacciones['asiento_id'] = $refacciones->asiento_id;
        $data_refacciones['abono'] = ($refacciones->abono - $data['descuento_refaccion']);
        $respuesta[CatalogoCuentasModel::CUENTA_370002] = $this->curl_asiento_api_modificar($data_refacciones);

        $mano_obra = $this->curl_asiento_api_detalle($data_asiento[AsientoModel::FOLIO_ID], 370003);
        $data_mano_obra['asiento_id'] = $mano_obra->asiento_id;
        $data_mano_obra['abono'] = ($mano_obra->abono - $data['descuento_mano_obra']);
        $respuesta[CatalogoCuentasModel::CUENTA_370003] = $this->curl_asiento_api_modificar($data_mano_obra);

        $costo_refaccion = $this->curl_asiento_api_detalle($data_asiento[AsientoModel::FOLIO_ID], 470001);
        $data_costo_refaccion['asiento_id'] = $costo_refaccion->asiento_id;
        $data_costo_refaccion['cargo'] = ($costo_refaccion->cargo - $data['descuento_refaccion']);
        $respuesta[CatalogoCuentasModel::CUENTA_470001] = $this->curl_asiento_api_modificar($data_costo_refaccion);

        $servicio_iva_facturado = $this->curl_asiento_api_detalle($data_asiento['folio_id'], 240013);
        $data_iva_facturado_data['asiento_id'] = $servicio_iva_facturado->asiento_id;
        $data_iva_facturado_data['abono'] = ((($data_refacciones['abono'] + $data_mano_obra['abono']) - $data['descuentos']->suma_descuento) * 0.16);
        $respuesta[CatalogoCuentasModel::CUENTA_240013] = $this->curl_asiento_api_modificar($data_iva_facturado_data);

        $servicio_clientes = $this->curl_asiento_api_detalle($data_asiento['folio_id'], 110004);
        $data_clientes_data['asiento_id'] = $servicio_clientes->asiento_id;
        $data_clientes_data['cargo'] = (($data_refacciones['abono'] + $data_mano_obra['abono'] + $data_iva_facturado_data['abono']) - $data['descuentos']->suma_descuento);
        $respuesta[CatalogoCuentasModel::CUENTA_110004] = $this->curl_asiento_api_modificar($data_clientes_data);

        $inventario_proceso = $this->curl_asiento_api_detalle($data_asiento[AsientoModel::FOLIO_ID], 151000);
        $data_inventario_proceso['asiento_id'] = $inventario_proceso->asiento_id;
        $data_inventario_proceso['abono'] = $data_refacciones['abono'];
        $respuesta[CatalogoCuentasModel::CUENTA_151000] = $this->curl_asiento_api_modificar($data_inventario_proceso);

        if ($data['tipo_descuento'] == 1) {
            $data_asiento['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
            $data_asiento['cuenta_id'] = CatalogoCuentasModel::CUENTA_370004;
            $data_asiento['total_pago'] = $data['descuento_refaccion'];
            $data_asiento[AsientoModel::PROCEDENCIA] = AsientoModel::PROCEDENCIA_SERVICIOS;
            $respuesta[CatalogoCuentasModel::CUENTA_370004] = $this->curl_asiento_api($data_asiento);
        }
        if ($data['tipo_descuento'] == 2) {
            $data_asiento['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
            $data_asiento['cuenta_id'] = CatalogoCuentasModel::CUENTA_370004;
            $data_asiento['total_pago'] = $data['descuento_mano_obra'];
            $data_asiento[AsientoModel::PROCEDENCIA] = AsientoModel::PROCEDENCIA_SERVICIOS;
            $respuesta[CatalogoCuentasModel::CUENTA_370004] = $this->curl_asiento_api($data_asiento);
        }
        return $respuesta;
    }

    public function servicioPolizaGarantias($request)
    {
        $cuenta_por_cobrar = $this->servicioMovimientos->getTipoCuentaxCobrar($request['cuenta_por_cobrar_id']);
        $respuesta = [];
        $data[AsientoModel::CONCEPTO] = isset($cuenta_por_cobrar['concepto']) ? $cuenta_por_cobrar['concepto'] : '';
        $data[AsientoModel::CLAVE_POLIZA] = env('SUCURSAL_DMS') == 'QUERETARO' ? TipoPolizasModel::QG : TipoPolizasModel::OG;
        $data[AsientoModel::ESTATUS] = isset($cuenta_por_cobrar['estatus']) ? $cuenta_por_cobrar['estatus'] : 'POR_APLICAR';
        $data[AsientoModel::CLIENTE_ID] = isset($cuenta_por_cobrar['cliente_id']) ? $cuenta_por_cobrar['cliente_id'] : '';
        $data[AsientoModel::CLIENTE_NOMBRE] = isset($cuenta_por_cobrar['cliente_nombre']) ? $cuenta_por_cobrar['cliente_nombre'] : '';
        $data[AsientoModel::CLIENTE_APELLIDO1] = isset($cuenta_por_cobrar['cliente_apellido1']) ? $cuenta_por_cobrar['cliente_apellido1'] : '';
        $data[AsientoModel::CLIENTE_APELLIDO2] = isset($cuenta_por_cobrar['cliente_apellido2']) ? $cuenta_por_cobrar['cliente_apellido2'] : '';
        $data[AsientoModel::FECHA] = isset($cuenta_por_cobrar['fecha']) ? $cuenta_por_cobrar['fecha'] : '';
        $data[AsientoModel::FOLIO_ID] = isset($cuenta_por_cobrar['folio_id']) ? $cuenta_por_cobrar['folio_id'] : '';
        $data[AsientoModel::DEPARTAMENTO] = AsientoModel::SERVICIOS;
        $data[AsientoModel::PROCEDENCIA] = AsientoModel::PROCEDENCIA_SERVICIOS;

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_330004;
        $data['total_pago'] = $request[AsientoModel::TOTAL_REFACCIONES];
        $respuesta[] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = env('SUCURSAL_DMS') == 'QUERETARO' ? CatalogoCuentasModel::CUENTA_350004 : CatalogoCuentasModel::CUENTA_360004;
        $data['total_pago'] = $request[AsientoModel::TOTAL_MANO_OBRA];
        $respuesta[] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_110008;
        $data['total_pago'] = $request[AsientoModel::TOTAL_GARANTIAS_POR_COBRAR];
        $respuesta[] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = env('SUCURSAL_DMS') == 'QUERETARO' ? CatalogoCuentasModel::CUENTA_151000 : CatalogoCuentasModel::CUENTA_154000;
        $data['total_pago'] = $request[AsientoModel::TOTAL_INVENTARIO_PROCESO];
        $respuesta[] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = env('SUCURSAL_DMS') == 'QUERETARO' ? CatalogoCuentasModel::CUENTA_240013 : CatalogoCuentasModel::CUENTA_240013;
        $data['total_pago'] = $request[AsientoModel::TOTAL_IVA_FACTURADO];
        $respuesta[] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data['cuenta_id'] = env('SUCURSAL_DMS') == 'QUERETARO' ? CatalogoCuentasModel::CUENTA_450002 : CatalogoCuentasModel::CUENTA_460002;
        $data['total_pago'] = $request[AsientoModel::TOTAL_COSTO_REFACCION];
        $respuesta[CatalogoCuentasModel::CUENTA_450002] = $this->curl_asiento_api($data);

        return $respuesta;
    }

    public function servicioPolizaServicioInterno($request)
    {
        $cuenta_por_cobrar = $this->servicioMovimientos->getTipoCuentaxCobrar($request['cuenta_por_cobrar_id']);
        $respuesta = [];
        $data[AsientoModel::CONCEPTO] = isset($cuenta_por_cobrar['concepto']) ? $cuenta_por_cobrar['concepto'] : '';
        $data[AsientoModel::CLAVE_POLIZA] = TipoPolizasModel::DO;
        $data[AsientoModel::ESTATUS] = isset($cuenta_por_cobrar['estatus']) ? $cuenta_por_cobrar['estatus'] : 'POR_APLICAR';
        $data[AsientoModel::CLIENTE_ID] = isset($cuenta_por_cobrar['cliente_id']) ? $cuenta_por_cobrar['cliente_id'] : '';
        $data[AsientoModel::CLIENTE_NOMBRE] = isset($cuenta_por_cobrar['cliente_nombre']) ? $cuenta_por_cobrar['cliente_nombre'] : '';
        $data[AsientoModel::CLIENTE_APELLIDO1] = isset($cuenta_por_cobrar['cliente_apellido1']) ? $cuenta_por_cobrar['cliente_apellido1'] : '';
        $data[AsientoModel::CLIENTE_APELLIDO2] = isset($cuenta_por_cobrar['cliente_apellido2']) ? $cuenta_por_cobrar['cliente_apellido2'] : '';
        $data[AsientoModel::FECHA] = isset($cuenta_por_cobrar['fecha']) ? $cuenta_por_cobrar['fecha'] : '';
        $data[AsientoModel::FOLIO_ID] = isset($cuenta_por_cobrar['folio_id']) ? $cuenta_por_cobrar['folio_id'] : '';
        $data[AsientoModel::DEPARTAMENTO] = AsientoModel::SERVICIOS;
        $data[AsientoModel::PROCEDENCIA] = AsientoModel::PROCEDENCIA_SERVICIOS;

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_151000;
        $data['total_pago'] = $request[AsientoModel::PROCESOS];
        $respuesta[CatalogoCuentasModel::CUENTA_151000] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_510034;
        $data['total_pago'] = $request[AsientoModel::ACONDICIONAMIENTO];
        $respuesta[CatalogoCuentasModel::CUENTA_510034] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_510034;
        $data['total_pago'] = $request[AsientoModel::CORTESIA_NUEVOS];
        $respuesta[CatalogoCuentasModel::CUENTA_510034] = $this->curl_asiento_api($data);

        return $respuesta;
    }

    public function servicioPolizaRefaccionTaller($request)
    {
        $cuenta_por_cobrar = $this->servicioMovimientos->getTipoCuentaxCobrar($request['cuenta_por_cobrar_id']);
        $respuesta = [];
        $data[AsientoModel::CONCEPTO] = isset($cuenta_por_cobrar['concepto']) ? $cuenta_por_cobrar['concepto'] : '';
        $data[AsientoModel::CLAVE_POLIZA] = TipoPolizasModel::V0;
        $data[AsientoModel::ESTATUS] = 'APLICADO';
        $data[AsientoModel::CLIENTE_ID] = isset($cuenta_por_cobrar['cliente_id']) ? $cuenta_por_cobrar['cliente_id'] : '';
        $data[AsientoModel::CLIENTE_NOMBRE] = isset($cuenta_por_cobrar['cliente_nombre']) ? $cuenta_por_cobrar['cliente_nombre'] : '';
        $data[AsientoModel::CLIENTE_APELLIDO1] = isset($cuenta_por_cobrar['cliente_apellido1']) ? $cuenta_por_cobrar['cliente_apellido1'] : '';
        $data[AsientoModel::CLIENTE_APELLIDO2] = isset($cuenta_por_cobrar['cliente_apellido2']) ? $cuenta_por_cobrar['cliente_apellido2'] : '';
        $data[AsientoModel::FECHA] = isset($cuenta_por_cobrar['fecha']) ? $cuenta_por_cobrar['fecha'] : '';
        $data[AsientoModel::FOLIO_ID] = isset($cuenta_por_cobrar['folio_id']) ? $cuenta_por_cobrar['folio_id'] : '';
        $data[AsientoModel::DEPARTAMENTO] = AsientoModel::TALLER;
        $data[AsientoModel::PROCEDENCIA] = AsientoModel::PROCEDENCIA_VENTA_MOSTRADOR;

        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_151000;
        $data['total_pago'] = $request[AsientoModel::TOTAL_COSTO_PIEZA];
        $respuesta[CatalogoCuentasModel::CUENTA_151000] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_131000;
        $data['total_pago'] = $request[AsientoModel::TOTAL_COSTO_PIEZA];
        $respuesta[CatalogoCuentasModel::CUENTA_131000] = $this->curl_asiento_api($data);

        return $respuesta;
    }

    public function servicioPolizaCompraFordPlanta($request)
    {
        ParametrosHttpValidador::validar_array($request, $this->getReglasPolizaCompraFordPlanta());
        $cuenta_por_pagar = $this->servicioMovimientos->getTipoCuentaxPagar($request['cuenta_por_pagar_id']);
        $respuesta = [];
        $data[AsientoModel::CONCEPTO] = isset($cuenta_por_pagar['concepto']) ? $cuenta_por_pagar['concepto'] : '';
        $data[AsientoModel::CLAVE_POLIZA] = TipoPolizasModel::C0;
        $data[AsientoModel::ESTATUS] = 'APLICADO';
        $data[AsientoModel::PROVEEDOR_ID] = isset($cuenta_por_pagar['proveedor_id']) ? $cuenta_por_pagar['proveedor_id'] : '';
        $data[AsientoModel::FECHA] = isset($cuenta_por_pagar['fecha']) ? $cuenta_por_pagar['fecha'] : '';
        $data[AsientoModel::FOLIO_ID] = isset($cuenta_por_pagar['folio_id']) ? $cuenta_por_pagar['folio_id'] : '';
        $data[AsientoModel::DEPARTAMENTO] = AsientoModel::COMPRAS;

        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_131000;
        $data['total_pago'] = $request[AsientoModel::TOTAL_FACTURA_SIN_IVA];
        $respuesta[CatalogoCuentasModel::CUENTA_131000] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_115005;
        $data['total_pago'] = $request[AsientoModel::TOTAL_IVA_FACTURA];
        $respuesta[CatalogoCuentasModel::CUENTA_115005] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_202000;
        $data['total_pago'] = $request[AsientoModel::TOTAL_FACTURA];
        $respuesta[CatalogoCuentasModel::CUENTA_202000] = $this->curl_asiento_api($data);

        return $respuesta;
    }

    public function servicioPolizaCompraProveedorExterno($request)
    {
        ParametrosHttpValidador::validar_array($request, $this->getReglasPolizaCompraProveedorExterno());
        $cuenta_por_pagar = $this->servicioMovimientos->getTipoCuentaxPagar($request['cuenta_por_pagar_id']);
        $respuesta = [];
        $data[AsientoModel::CONCEPTO] = isset($cuenta_por_pagar['concepto']) ? $cuenta_por_pagar['concepto'] : '';
        $data[AsientoModel::CLAVE_POLIZA] = TipoPolizasModel::C0;
        $data[AsientoModel::ESTATUS] = 'APLICADO';
        $data[AsientoModel::PROVEEDOR_ID] = isset($cuenta_por_pagar['proveedor_id']) ? $cuenta_por_pagar['proveedor_id'] : '';
        $data[AsientoModel::FECHA] = isset($cuenta_por_pagar['fecha']) ? $cuenta_por_pagar['fecha'] : '';
        $data[AsientoModel::FOLIO_ID] = isset($cuenta_por_pagar['folio_id']) ? $cuenta_por_pagar['folio_id'] : '';
        $data[AsientoModel::DEPARTAMENTO] = AsientoModel::COMPRAS;

        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_131000;
        $data['total_pago'] = $request[AsientoModel::TOTAL_FACTURA_SIN_IVA];
        $respuesta[CatalogoCuentasModel::CUENTA_131000] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_115005;
        $data['total_pago'] = $request[AsientoModel::TOTAL_IVA_FACTURA];
        $respuesta[CatalogoCuentasModel::CUENTA_115005] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_210002;
        $data['total_pago'] = $request[AsientoModel::TOTAL_FACTURA];
        $respuesta[CatalogoCuentasModel::CUENTA_210002] = $this->curl_asiento_api($data);

        return $respuesta;
    }

    public function servicioPolizaDevolucionFordPlanta($request)
    {
        $request = $request->toArray();
        ParametrosHttpValidador::validar_array($request, $this->getReglasPolizaCompraFordPlanta());
        $cuenta_por_pagar = $this->servicioMovimientos->getTipoCuentaxPagar($request['cuenta_por_pagar_id']);
        $respuesta = [];
        $data[AsientoModel::CONCEPTO] = isset($cuenta_por_pagar['concepto']) ? $cuenta_por_pagar['concepto'] : '';
        $data[AsientoModel::CLAVE_POLIZA] = TipoPolizasModel::ND;
        $data[AsientoModel::ESTATUS] = 'APLICADO';
        $data[AsientoModel::PROVEEDOR_ID] = isset($cuenta_por_pagar['proveedor_id']) ? $cuenta_por_pagar['proveedor_id'] : '';
        $data[AsientoModel::FECHA] = isset($cuenta_por_pagar['fecha']) ? $cuenta_por_pagar['fecha'] : '';
        $data[AsientoModel::FOLIO_ID] = isset($cuenta_por_pagar['folio_id']) ? $cuenta_por_pagar['folio_id'] : '';
        $data[AsientoModel::DEPARTAMENTO] = AsientoModel::COMPRAS;

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_131000;
        $data['total_pago'] = $request[AsientoModel::TOTAL_FACTURA_SIN_IVA];
        $respuesta[CatalogoCuentasModel::CUENTA_131000] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_115005;
        $data['total_pago'] = $request[AsientoModel::TOTAL_IVA_FACTURA];
        $respuesta[CatalogoCuentasModel::CUENTA_115005] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_202000;
        $data['total_pago'] = $request[AsientoModel::TOTAL_FACTURA];
        $respuesta[CatalogoCuentasModel::CUENTA_202000] = $this->curl_asiento_api($data);

        return $respuesta;
    }

    public function servicioPolizaDevolucionProveedorExterno($request)
    {
        $request = $request->toArray();
        ParametrosHttpValidador::validar_array($request, $this->getReglasPolizaCompraProveedorExterno());
        $cuenta_por_pagar = $this->servicioMovimientos->getTipoCuentaxPagar($request['cuenta_por_pagar_id']);
        $respuesta = [];
        $data[AsientoModel::CONCEPTO] = isset($cuenta_por_pagar['concepto']) ? $cuenta_por_pagar['concepto'] : '';
        $data[AsientoModel::CLAVE_POLIZA] = TipoPolizasModel::ND;
        $data[AsientoModel::ESTATUS] = 'APLICADO';
        $data[AsientoModel::PROVEEDOR_ID] = isset($cuenta_por_pagar['proveedor_id']) ? $cuenta_por_pagar['proveedor_id'] : '';
        $data[AsientoModel::FECHA] = isset($cuenta_por_pagar['fecha']) ? $cuenta_por_pagar['fecha'] : '';
        $data[AsientoModel::FOLIO_ID] = isset($cuenta_por_pagar['folio_id']) ? $cuenta_por_pagar['folio_id'] : '';
        $data[AsientoModel::DEPARTAMENTO] = AsientoModel::COMPRAS;

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_131000;
        $data['total_pago'] = $request[AsientoModel::TOTAL_FACTURA_SIN_IVA];
        $respuesta[CatalogoCuentasModel::CUENTA_131000] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_115005;
        $data['total_pago'] = $request[AsientoModel::TOTAL_IVA_FACTURA];
        $respuesta[CatalogoCuentasModel::CUENTA_115005] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_210002;
        $data['total_pago'] = $request[AsientoModel::TOTAL_FACTURA];
        $respuesta[CatalogoCuentasModel::CUENTA_210002] = $this->curl_asiento_api($data);

        return $respuesta;
    }

    public function aplicaAsientosCuentas($request)
    {
        $cuenta_por_cobrar = $this->servicioMovimientos->getTipoCuentaxCobrar($request['cuenta_por_cobrar_id']);
        $asientos = $this->curl_asiento_api_todos($cuenta_por_cobrar->folio_id, null);
        $folio_aplicar = null;
        if ($asientos) {
            foreach ($asientos as $item) {
                if ($item->PolizaNomenclatura_id != 'CG' && $item->estatus_id != 'APLICADO') {
                    $folio_aplicar = $item->folio;
                }
            }
        }
        if ($folio_aplicar) {
            $aplicar_folio = $this->servicioCurl->curlPost(env('URL_CONTABILIDAD') . 'asientos/api/aplicar_folio', [
                'folio' => $folio_aplicar,
                'estatus' => 'APLICADO',
                'sucursal_id' => env('SUCURSAL_DMS') == 'QUERETARO' ? AsientoModel::SUCURSAL_QUERETARO : AsientoModel::SUCURSAL_SANJUAN
            ], false);
            $respuesta = json_decode($aplicar_folio);
            if (!$respuesta && $respuesta->status != 'success') {
                throw new ParametroHttpInvalidoException([
                    'msg' => "Error al aplicar los asientos poliza"
                ]);
                return false;
            }
            return json_decode($aplicar_folio);
        } else {
            throw new ParametroHttpInvalidoException([
                'msg' => "No se encontrarón asientos por aplicar"
            ]);
        }
    }


    public function curl_asiento_api($data)
    {
        $data_asiento = [
            AsientoModel::CONCEPTO => isset($data['concepto']) ? $data['concepto'] : '',
            AsientoModel::TOTAL => isset($data['total_pago']) ? $data['total_pago'] : '',
            AsientoModel::CUENTA => isset($data['cuenta_id']) ? $data['cuenta_id'] : '',
            AsientoModel::TIPO => isset($data['tipo_asiento_id']) ? $data['tipo_asiento_id'] : '',
            AsientoModel::FECHA =>  date('Y-m-d'),
            AsientoModel::FOLIO => isset($data['folio_id']) ? $data['folio_id'] : '',
            AsientoModel::CLAVE_POLIZA => isset($data['clave_poliza']) ? $data['clave_poliza'] : '',
            AsientoModel::CLIENTE_ID => isset($data['cliente_id']) ? $data['cliente_id'] : '',
            AsientoModel::CLIENTE_NOMBRE => isset($data['cliente_nombre']) ? $data['cliente_nombre'] : '',
            AsientoModel::CLIENTE_APELLIDO1 => isset($data['cliente_apellido1']) ? $data['cliente_apellido1'] : '',
            AsientoModel::CLIENTE_APELLIDO2 => isset($data['cliente_apellido2']) ? $data['cliente_apellido2'] : '',
            AsientoModel::ESTATUS => isset($data['estatus']) ? $data['estatus'] : '',
            AsientoModel::DEPARTAMENTO => isset($data['departamento']) ? $data['departamento'] : '',
            AsientoModel::PROVEEDOR_ID => isset($data['proveedor_id']) ? $data['proveedor_id'] : '',
            AsientoModel::PROCEDENCIA => isset($data['procedencia_id']) ? $data['procedencia_id'] : '',
            AsientoModel::COSTO_PROMEDIO => isset($data['costo_promedio']) ? $data['costo_promedio'] : '',
            AsientoModel::CANTIDAD => isset($data['cantidad']) ? $data['cantidad'] : '',
            AsientoModel::SUCURSAL_ID => env('SUCURSAL_DMS') == 'QUERETARO' ? AsientoModel::SUCURSAL_QUERETARO : AsientoModel::SUCURSAL_SANJUAN
        ];

        return $this->servicioCurl->curlPost('https://mylsa.iplaneacion.com/dms/contabilidad_queretaro/asientos/api/agregar', $data_asiento, false);
    }

    public function curl_asiento_api_modificar($data)
    {
        return $this->servicioCurl->curlPost(env('URL_CONTABILIDAD') . 'asientos/api/modificar', [
            AsientoModel::ASIENTO_ID => isset($data['asiento_id']) ? $data['asiento_id'] : '',
            AsientoModel::CARGO => isset($data['cargo']) ? $data['cargo'] : '',
            AsientoModel::ABONO => isset($data['abono']) ? $data['abono'] : '',
        ], false);
    }
    public function curl_asiento_api_modificar_cuenta($data)
    {
        return $this->servicioCurl->curlPost(env('URL_CONTABILIDAD') . 'asientos/api/modificar', [
            AsientoModel::ASIENTO_ID => isset($data['asiento_id']) ? $data['asiento_id'] : '',
            AsientoModel::CUENTA => isset($data['cuenta']) ? $data['cuenta'] : '',
        ], false);
    }

    public function curl_asiento_api_detalle($folio_id, $cuenta, $nomenclatura = false)
    {
        if ($nomenclatura) {
            $api = $this->servicioCurl->curlGet(env('URL_CONTABILIDAD') . 'asientos/api/detalle?folio=' . $folio_id . '&cuenta=' . $cuenta . '&nomenclatura=' . $nomenclatura, false);
        } else {
            $api = $this->servicioCurl->curlGet(env('URL_CONTABILIDAD') . 'asientos/api/detalle?folio=' . $folio_id . '&cuenta=' . $cuenta . '&estatus=TODOS', false);
        }
        $respuesta = json_decode($api);
        return $respuesta->data ? end($respuesta->data) : [];
    }

    public function show_api_datos($folio_id, $cuenta, $nomenclatura = false, $departamento = false)
    {
        if ($departamento) {
            $api = $this->servicioCurl->curlGet(env('URL_CONTABILIDAD') . 'asientos/api/detalle?folio=' . $folio_id . '&cuenta=' . $cuenta . '&nomenclatura=' . $nomenclatura . '&departamento=' . $departamento, false);
        } else {
            $api = $this->servicioCurl->curlGet(env('URL_CONTABILIDAD') . 'asientos/api/detalle?folio=' . $folio_id . '&cuenta=' . $cuenta . '&nomenclatura=' . $nomenclatura, false);
        }

        $respuesta = json_decode($api);

        return $respuesta->data ? $respuesta->data : [];
    }

    public function curl_asiento_api_todos($folio_id, $cuenta)
    {
        $api = $this->servicioCurl->curlGet(env('URL_CONTABILIDAD') . 'asientos/api/detalle?folio=' . $folio_id . '&cuenta=' . $cuenta, false);
        $respuesta = json_decode($api);
        return $respuesta->data ? ($respuesta->data) : [];
    }

    public function servicioPolizaDevolucionServicios($request)
    {
        $cuenta_por_cobrar = $this->servicioMovimientos->getTipoCuentaxCobrar($request['cuenta_por_cobrar_id']);

        $respuesta = [];
        $data[AsientoModel::CONCEPTO] = isset($cuenta_por_cobrar['concepto']) ? $cuenta_por_cobrar['concepto'] : '';
        $data[AsientoModel::ESTATUS] = 'APLICADO';
        $data[AsientoModel::CLIENTE_ID] = isset($cuenta_por_cobrar['cliente_id']) ? $cuenta_por_cobrar['cliente_id'] : '';
        $data[AsientoModel::CLIENTE_NOMBRE] = isset($cuenta_por_cobrar['cliente_nombre']) ? $cuenta_por_cobrar['cliente_nombre'] : '';
        $data[AsientoModel::CLIENTE_APELLIDO1] = isset($cuenta_por_cobrar['cliente_apellido1']) ? $cuenta_por_cobrar['cliente_apellido1'] : '';
        $data[AsientoModel::CLIENTE_APELLIDO2] = isset($cuenta_por_cobrar['cliente_apellido2']) ? $cuenta_por_cobrar['cliente_apellido2'] : '';
        $data[AsientoModel::FECHA] = isset($cuenta_por_cobrar['fecha']) ? $cuenta_por_cobrar['fecha'] : '';
        $data[AsientoModel::FOLIO_ID] = isset($cuenta_por_cobrar['folio_id']) ? $cuenta_por_cobrar['folio_id'] : '';
        $data[AsientoModel::DEPARTAMENTO] = AsientoModel::SERVICIOS;
        $data[AsientoModel::PROCEDENCIA] = AsientoModel::PROCEDENCIA_SERVICIOS;

        $refacciones = $this->curl_asiento_api_detalle($data[AsientoModel::FOLIO_ID], env('SUCURSAL_DMS') == 'QUERETARO' ? 350001 : 360001, TipoPolizasModel::QT);
        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data[AsientoModel::CLAVE_POLIZA] = env('SUCURSAL_DMS') == 'QUERETARO' ? TipoPolizasModel::CT : TipoPolizasModel::OT;
        $data['cuenta_id'] = env('SUCURSAL_DMS') == 'QUERETARO' ? CatalogoCuentasModel::CUENTA_350001 : CatalogoCuentasModel::CUENTA_360001;
        $data['total_pago'] = isset($refacciones->abono) ? $refacciones->abono : 0;
        $respuesta[CatalogoCuentasModel::CUENTA_350001] = $this->curl_asiento_api($data);

        $mano_obra = $this->curl_asiento_api_detalle($data[AsientoModel::FOLIO_ID], env('SUCURSAL_DMS') == 'QUERETARO' ? 350003 : 360003, TipoPolizasModel::QT);
        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data[AsientoModel::CLAVE_POLIZA] = env('SUCURSAL_DMS') == 'QUERETARO' ? TipoPolizasModel::CT : TipoPolizasModel::OT;
        $data['cuenta_id'] = env('SUCURSAL_DMS') == 'QUERETARO' ? CatalogoCuentasModel::CUENTA_350003 : CatalogoCuentasModel::CUENTA_360003;
        $data['total_pago'] = isset($mano_obra->abono) ? $mano_obra->abono : 0;
        $respuesta[CatalogoCuentasModel::CUENTA_350003] = $this->curl_asiento_api($data);

        $servicio_clientes = $this->curl_asiento_api_detalle($data[AsientoModel::FOLIO_ID], 110004, TipoPolizasModel::QT);
        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data[AsientoModel::CLAVE_POLIZA] = env('SUCURSAL_DMS') == 'QUERETARO' ? TipoPolizasModel::CT : TipoPolizasModel::OT;
        $data['cuenta_id'] = env('SUCURSAL_DMS') == 'QUERETARO' ? CatalogoCuentasModel::CUENTA_110004 : CatalogoCuentasModel::CUENTA_110004;
        $data['total_pago'] = isset($servicio_clientes->cargo) ? $servicio_clientes->cargo : 0;
        $respuesta[CatalogoCuentasModel::CUENTA_110004] = $this->curl_asiento_api($data);

        $inventario_proceso = $this->curl_asiento_api_detalle($data[AsientoModel::FOLIO_ID], env('SUCURSAL_DMS') == 'QUERETARO' ? 151000 : 152000, TipoPolizasModel::QT);
        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data[AsientoModel::CLAVE_POLIZA] = env('SUCURSAL_DMS') == 'QUERETARO' ? TipoPolizasModel::CT : TipoPolizasModel::OT;
        $data['cuenta_id'] = env('SUCURSAL_DMS') == 'QUERETARO' ? CatalogoCuentasModel::CUENTA_151000 : CatalogoCuentasModel::CUENTA_152000;
        $data['total_pago'] = isset($inventario_proceso->abono) ? $inventario_proceso->abono : 0;
        $respuesta[CatalogoCuentasModel::CUENTA_151000] = $this->curl_asiento_api($data);

        $servicio_iva_facturado_credito = $this->curl_asiento_api_detalle($data[AsientoModel::FOLIO_ID], env('SUCURSAL_DMS') == 'QUERETARO' ? 240013 : 115015, TipoPolizasModel::QT);
        if (isset($servicio_iva_facturado_credito->abono)) {
            $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
            $data[AsientoModel::CLAVE_POLIZA] = env('SUCURSAL_DMS') == 'QUERETARO' ? TipoPolizasModel::CT : TipoPolizasModel::OT;
            $data['cuenta_id'] = env('SUCURSAL_DMS') == 'QUERETARO' ? CatalogoCuentasModel::CUENTA_240013 : CatalogoCuentasModel::CUENTA_240013;
            $data['total_pago'] = isset($servicio_iva_facturado_credito->abono) ? $servicio_iva_facturado_credito->abono : 0;
            $respuesta[CatalogoCuentasModel::CUENTA_240013] = $this->curl_asiento_api($data);
        }
        $servicio_iva_facturado_contado = $this->curl_asiento_api_detalle($data[AsientoModel::FOLIO_ID], env('SUCURSAL_DMS') == 'QUERETARO' ? 240012 : 115015, TipoPolizasModel::QT);
        if (isset($servicio_iva_facturado_contado->abono)) {
            $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
            $data[AsientoModel::CLAVE_POLIZA] = env('SUCURSAL_DMS') == 'QUERETARO' ? TipoPolizasModel::CT : TipoPolizasModel::OT;
            $data['cuenta_id'] = env('SUCURSAL_DMS') == 'QUERETARO' ? CatalogoCuentasModel::CUENTA_240012 : CatalogoCuentasModel::CUENTA_240012;
            $data['total_pago'] = isset($servicio_iva_facturado_contado->abono) ? $servicio_iva_facturado_contado->abono : 0;
            $respuesta[CatalogoCuentasModel::CUENTA_240012] = $this->curl_asiento_api($data);
        }

        $costo_refaccion = $this->curl_asiento_api_detalle($data[AsientoModel::FOLIO_ID], env('SUCURSAL_DMS') == 'QUERETARO' ? 450001 : 460001, TipoPolizasModel::QT);
        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data[AsientoModel::CLAVE_POLIZA] = env('SUCURSAL_DMS') == 'QUERETARO' ? TipoPolizasModel::CT : TipoPolizasModel::OT;
        $data['cuenta_id'] = env('SUCURSAL_DMS') == 'QUERETARO' ? CatalogoCuentasModel::CUENTA_450001 : CatalogoCuentasModel::CUENTA_460001;
        $data['total_pago'] = isset($costo_refaccion->cargo) ? $costo_refaccion->cargo : 0;
        $respuesta[CatalogoCuentasModel::CUENTA_450001] = $this->curl_asiento_api($data);

        $descuento_refaccion = $this->curl_asiento_api_detalle($data[AsientoModel::FOLIO_ID], env('SUCURSAL_DMS') == 'QUERETARO' ? 350008 : 350008, TipoPolizasModel::QT);
        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data[AsientoModel::CLAVE_POLIZA] = env('SUCURSAL_DMS') == 'QUERETARO' ? TipoPolizasModel::CT : TipoPolizasModel::OT;
        $data['cuenta_id'] = env('SUCURSAL_DMS') == 'QUERETARO' ? CatalogoCuentasModel::CUENTA_350008 : CatalogoCuentasModel::CUENTA_350008;
        $data['total_pago'] = isset($descuento_refaccion->cargo) ? $descuento_refaccion->cargo : 0;
        $respuesta[CatalogoCuentasModel::CUENTA_350008] = $this->curl_asiento_api($data);

        $descuento_mano_obra = $this->curl_asiento_api_detalle($data[AsientoModel::FOLIO_ID], env('SUCURSAL_DMS') == 'QUERETARO' ? 350007 : 350007, TipoPolizasModel::QT);
        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data[AsientoModel::CLAVE_POLIZA] = env('SUCURSAL_DMS') == 'QUERETARO' ? TipoPolizasModel::CT : TipoPolizasModel::OT;
        $data['cuenta_id'] = env('SUCURSAL_DMS') == 'QUERETARO' ? CatalogoCuentasModel::CUENTA_350007 : CatalogoCuentasModel::CUENTA_350007;
        $data['total_pago'] = isset($descuento_mano_obra->cargo) ? $descuento_mano_obra->cargo : 0;
        $respuesta[CatalogoCuentasModel::CUENTA_350007] = $this->curl_asiento_api($data);
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $cliente_servicio_caja = $this->curl_asiento_api_detalle($data[AsientoModel::FOLIO_ID], env('SUCURSAL_DMS') == 'QUERETARO' ? 110004 : 110004, TipoPolizasModel::CG);
        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data[AsientoModel::CLAVE_POLIZA] = env('SUCURSAL_DMS') == 'QUERETARO' ? TipoPolizasModel::CG : TipoPolizasModel::CG;
        $data['cuenta_id'] = env('SUCURSAL_DMS') == 'QUERETARO' ? CatalogoCuentasModel::CUENTA_110004 : CatalogoCuentasModel::CUENTA_110004;
        $data['total_pago'] = isset($cliente_servicio_caja->abono) ? $cliente_servicio_caja->abono : 0;
        $respuesta[CatalogoCuentasModel::CUENTA_110004] = $this->curl_asiento_api($data);

        $iva_facturado_caja = $this->curl_asiento_api_detalle($data[AsientoModel::FOLIO_ID], env('SUCURSAL_DMS') == 'QUERETARO' ? 240013 : 240013, TipoPolizasModel::CG);
        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data[AsientoModel::CLAVE_POLIZA] = env('SUCURSAL_DMS') == 'QUERETARO' ? TipoPolizasModel::CG : TipoPolizasModel::CG;
        $data['cuenta_id'] = env('SUCURSAL_DMS') == 'QUERETARO' ? CatalogoCuentasModel::CUENTA_240013 : CatalogoCuentasModel::CUENTA_240013;
        $data['total_pago'] = isset($iva_facturado_caja->cargo) ? $iva_facturado_caja->cargo : 0;
        $respuesta[CatalogoCuentasModel::CUENTA_240013] = $this->curl_asiento_api($data);

        $iva_cobrado_caja = $this->curl_asiento_api_detalle($data[AsientoModel::FOLIO_ID], env('SUCURSAL_DMS') == 'QUERETARO' ? 240012 : 240012, TipoPolizasModel::CG);
        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data[AsientoModel::CLAVE_POLIZA] = env('SUCURSAL_DMS') == 'QUERETARO' ? TipoPolizasModel::CG : TipoPolizasModel::CG;
        $data['cuenta_id'] = env('SUCURSAL_DMS') == 'QUERETARO' ? CatalogoCuentasModel::CUENTA_240012 : CatalogoCuentasModel::CUENTA_240012;
        $data['total_pago'] = isset($iva_cobrado_caja->abono) ? $iva_cobrado_caja->abono : 0;
        $respuesta[CatalogoCuentasModel::CUENTA_240012] = $this->curl_asiento_api($data);

        $cuentas_bancarias = $this->servicioCuentas->getCuentaBancarias();
        if ($cuentas_bancarias) {
            foreach ($cuentas_bancarias as $val) {
                $banco_caja = $this->curl_asiento_api_detalle($data[AsientoModel::FOLIO_ID],  $val['no_cuenta'], TipoPolizasModel::CG);
                if ($banco_caja) {
                    $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
                    $data[AsientoModel::CLAVE_POLIZA] = env('SUCURSAL_DMS') == 'QUERETARO' ? TipoPolizasModel::CG : TipoPolizasModel::CG;
                    $data['cuenta_id'] = $val['no_cuenta'];
                    $data['total_pago'] = isset($banco_caja->cargo) ? $banco_caja->cargo : 0;
                    $respuesta[$val['no_cuenta']] = $this->curl_asiento_api($data);
                }
            }
        }

        return $respuesta;
    }

    public function servicioPolizaDevolucionMostrador($cuenta_por_cobrar)
    {
        $respuesta = [];
        $data[AsientoModel::CONCEPTO] = isset($cuenta_por_cobrar['concepto']) ? $cuenta_por_cobrar['concepto'] : '';
        $data[AsientoModel::ESTATUS] = 'APLICADO';
        $data[AsientoModel::CLIENTE_ID] = isset($cuenta_por_cobrar['cliente_id']) ? $cuenta_por_cobrar['cliente_id'] : '';
        $data[AsientoModel::CLIENTE_NOMBRE] = isset($cuenta_por_cobrar['cliente_nombre']) ? $cuenta_por_cobrar['cliente_nombre'] : '';
        $data[AsientoModel::CLIENTE_APELLIDO1] = isset($cuenta_por_cobrar['cliente_apellido1']) ? $cuenta_por_cobrar['cliente_apellido1'] : '';
        $data[AsientoModel::CLIENTE_APELLIDO2] = isset($cuenta_por_cobrar['cliente_apellido2']) ? $cuenta_por_cobrar['cliente_apellido2'] : '';
        $data[AsientoModel::FECHA] = isset($cuenta_por_cobrar['fecha']) ? $cuenta_por_cobrar['fecha'] : '';
        $data[AsientoModel::FOLIO_ID] = isset($cuenta_por_cobrar['folio_id']) ? $cuenta_por_cobrar['folio_id'] : '';
        $data[AsientoModel::DEPARTAMENTO] = AsientoModel::VENTA_MOSTRADOR;
        $data[AsientoModel::PROCEDENCIA] = AsientoModel::PROCEDENCIA_VENTA_MOSTRADOR;

        if ($cuenta_por_cobrar['tipo_forma_pago_id'] == 1) {
            $cuenta_bancaria = $this->handleCuentasBancarias($data[AsientoModel::FOLIO_ID], TipoPolizasModel::CM);
            $anticipos = $this->curl_asiento_api_detalle($data[AsientoModel::FOLIO_ID], env('SUCURSAL_DMS') == 'QUERETARO' ? 110003 : 110003, TipoPolizasModel::CM);
            if ($anticipos) {
                $respuesta[$cuenta_bancaria] = $this->handleAsientos($data, $cuenta_bancaria, TipoPolizasModel::CM, CatalogoCuentasModel::ABONO, AsientoModel::ANTICIPOS);
                $respuesta['110003'] = $this->handleAsientos($data, 110003, TipoPolizasModel::CM, CatalogoCuentasModel::CARGO, AsientoModel::ANTICIPOS);
            }
            $respuesta['330001'] = $this->handleAsientos($data, 330001, TipoPolizasModel::CM, CatalogoCuentasModel::CARGO, AsientoModel::VENTA_MOSTRADOR);
            $respuesta[$cuenta_bancaria] = $this->handleAsientos($data, $cuenta_bancaria, TipoPolizasModel::CM, CatalogoCuentasModel::ABONO, AsientoModel::VENTA_MOSTRADOR);
            $respuesta['240012'] = $this->handleAsientos($data, 240012, TipoPolizasModel::CM, CatalogoCuentasModel::CARGO, AsientoModel::VENTA_MOSTRADOR);
            $respuesta['440102'] = $this->handleAsientos($data, 440102, TipoPolizasModel::CM, CatalogoCuentasModel::ABONO, AsientoModel::VENTA_MOSTRADOR);
            $respuesta['131000'] = $this->handleAsientos($data, 131000, TipoPolizasModel::CM, CatalogoCuentasModel::CARGO, AsientoModel::VENTA_MOSTRADOR);
        } else {

            $respuesta['340101'] = $this->handleAsientos($data, 340101, TipoPolizasModel::CM, CatalogoCuentasModel::CARGO, AsientoModel::VENTA_MOSTRADOR);
            $respuesta['110003'] = $this->handleAsientos($data, 110003, TipoPolizasModel::CM, CatalogoCuentasModel::ABONO, AsientoModel::VENTA_MOSTRADOR);
            $respuesta['240013'] = $this->handleAsientos($data, 240013, TipoPolizasModel::CM, CatalogoCuentasModel::CARGO, AsientoModel::VENTA_MOSTRADOR);
            $respuesta['440102'] = $this->handleAsientos($data, 440102, TipoPolizasModel::CM, CatalogoCuentasModel::ABONO, AsientoModel::VENTA_MOSTRADOR);
            $respuesta['131000'] = $this->handleAsientos($data, 131000, TipoPolizasModel::CM, CatalogoCuentasModel::CARGO, AsientoModel::VENTA_MOSTRADOR);

            $respuesta['110003'] = $this->handleAsientos($data, 110003, TipoPolizasModel::CG, CatalogoCuentasModel::CARGO, AsientoModel::VENTA_MOSTRADOR);
            $respuesta['240013'] = $this->handleAsientos($data, 240013, TipoPolizasModel::CG, CatalogoCuentasModel::ABONO, AsientoModel::VENTA_MOSTRADOR);
            $respuesta['240012'] = $this->handleAsientos($data, 240012, TipoPolizasModel::CG, CatalogoCuentasModel::CARGO, AsientoModel::VENTA_MOSTRADOR);
            $cuenta_bancaria = $this->handleCuentasBancarias($data[AsientoModel::FOLIO_ID], TipoPolizasModel::CG);
            $respuesta[$cuenta_bancaria] = $this->handleAsientos($data, $cuenta_bancaria, TipoPolizasModel::CG, CatalogoCuentasModel::ABONO, AsientoModel::VENTA_MOSTRADOR);
        }

        return $respuesta;
    }
    private function handleCuentasBancarias($folio, $poliza)
    {
        $response = [];
        $cuentas_bancarias = $this->servicioCuentas->getCuentaBancarias();
        if ($cuentas_bancarias) {
            foreach ($cuentas_bancarias as $val) {
                $response = $this->curl_asiento_api_detalle($folio,  $val['no_cuenta'], $poliza);
                if ($response) {
                    return $response->cuenta;
                }
            }
        }
    }
    private function  handleAsientos($data, $cuenta, $tipo_poliza, $tipo_asiento, $departamento = false)
    {
        $respuesta = [];
        $asientos_rows = $this->show_api_datos($data[AsientoModel::FOLIO_ID], $cuenta, $tipo_poliza, $departamento);
        foreach ($asientos_rows as $key => $value) {
            $total_pago = $tipo_asiento == 1 ? $value->cargo : $value->abono;
            $data['tipo_asiento_id'] = $tipo_asiento;
            $data['concepto'] = $value->concepto;
            $data['clave_poliza'] = env('SUCURSAL_DMS') == 'QUERETARO' ? TipoPolizasModel::CQ : TipoPolizasModel::CQ;
            $data['cuenta_id'] = $cuenta;
            $data['total_pago'] = $total_pago;
            $data['costo_promedio'] = $value->costo;
            $respuesta[$key] = $this->curl_asiento_api($data);
        }
        return $respuesta;
    }
}
