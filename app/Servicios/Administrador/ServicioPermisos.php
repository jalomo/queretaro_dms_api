<?php

namespace App\Servicios\Administrador;

use App\Servicios\Core\ServicioDB;
use App\Models\Usuarios\RolesModulosModel;
use App\Models\Usuarios\RolesSeccionesModel;
use App\Models\Usuarios\RolesSubmenuModel;
use App\Models\Usuarios\RolesVistasModel;
use App\Servicios\Usuarios\Menu\ServicioModulos;
use DB;

class ServicioPermisos extends ServicioDB
{

    public function __construct()
    {
        $this->recurso = 'menu';
        $this->rol_id = null;
        $this->servicioModulos = new ServicioModulos();
        $this->menuModulosModel = new RolesModulosModel();
        $this->menuSeccionesModel = new RolesSeccionesModel();
        $this->menuSubMenuModel = new RolesSubmenuModel();
        $this->menusVistasModel = new RolesVistasModel();
    }

    public function getReglasGuardar()
    {
        return [
            RolesModulosModel::ROL_ID => 'required|exists:roles,id',
            RolesModulosModel::MODULO_ID => 'required|exists:modulo,id',
            RolesSeccionesModel::SECCION_ID => 'required|exists:menu_secciones,id',
            RolesSubmenuModel::SUBMENU_ID => 'required|exists:menu_submenu,id',
            RolesVistasModel::VISTA_ID => 'required|exists:menu_vistas,id',
        ];
    }

    public function getPermisosMenu($parametros)
    {
        $modulos = $this->servicioModulos->getModulosPermisos($parametros)->toArray();
        $rol_permisos = [];
        if (isset($parametros[RolesVistasModel::ROL_ID])) {
            $rol_permisos = $this->menusVistasModel->where(RolesVistasModel::ROL_ID, '=',  $parametros[RolesVistasModel::ROL_ID])->get()->toArray();
        }
        $array_permisos = [];
        $lista = [];
        if (is_array($rol_permisos)) {
            foreach ($rol_permisos as $key => $val) {
                $array_permisos[$key] =  $val['vista_id'];
            }
        }
        if (is_array($modulos)) {
            foreach ($modulos as $modulo) {
                $modulo['checked'] = false;
                if (in_array($modulo['vista_id'], $array_permisos)) {
                    $modulo['checked'] = true;
                }
                array_push($lista, $modulo);
            }
        }
        return $lista;
    }

    public function createPermisosRol($request)
    {
        $this->menuModulosModel = new RolesModulosModel();
        $this->menuSeccionesModel = new RolesSeccionesModel();
        $this->menuSubMenuModel = new RolesSubmenuModel();
        $this->menusVistasModel = new RolesVistasModel();
        $error = false;
        DB::beginTransaction();
        $det_modulos = $this->menuModulosModel->where([
            RolesModulosModel::ROL_ID => $request->rol_id,
            RolesModulosModel::MODULO_ID => $request->modulo_id,
        ])->first();
        $det_secciones = $this->menuSeccionesModel->where([
            RolesSeccionesModel::ROL_ID => $request->rol_id,
            RolesSeccionesModel::SECCION_ID => $request->seccion_id,
        ])->first();
        $det_submenu = $this->menuSubMenuModel->where([
            RolesSubmenuModel::ROL_ID => $request->rol_id,
            RolesSubmenuModel::SUBMENU_ID => $request->submenu_id,
        ])->first();

        $det_vista = $this->menusVistasModel->where([
            RolesVistasModel::ROL_ID => $request->rol_id,
            RolesVistasModel::VISTA_ID => $request->vista_id,
        ])->first();

        if (!isset($det_modulos->id)) {
            $createModulo = $this->menuModulosModel->create(['rol_id' => $request->rol_id, RolesModulosModel::MODULO_ID => $request->modulo_id]);
            if (!$createModulo) {
                $error = true;
            }
        }
        if (!isset($det_secciones->id)) {
            $createSeccion = $this->menuSeccionesModel->create(['rol_id' => $request->rol_id, RolesSeccionesModel::SECCION_ID => $request->seccion_id]);
            if (!$createSeccion) {
                $error = true;
            }
        }
        if (!isset($det_submenu->id)) {
            $createSubMenu = $this->menuSubMenuModel->create(['rol_id' => $request->rol_id, RolesSubmenuModel::SUBMENU_ID => $request->submenu_id]);
            if (!$createSubMenu) {
                $error = true;
            }
        }
        if (!isset($det_vista->id)) {
            $createVista = $this->menusVistasModel->create(['rol_id' => $request->rol_id, RolesVistasModel::VISTA_ID => $request->vista_id]);
            if (!$createVista) {
                $error = true;
            }
        }
        if ($error == true) {
            DB::rollback();
            return false;
        } else {
            DB::commit();
            return true;
        }
    }

    public function deletePermisoRol($request)
    {
        $this->menuModulosModel = new RolesModulosModel();
        $this->menuSeccionesModel = new RolesSeccionesModel();
        $this->menuSubMenuModel = new RolesSubmenuModel();
        $this->menusVistasModel = new RolesVistasModel();
        $error = false;
        $det_modulos = $this->menuModulosModel->where([
            RolesModulosModel::ROL_ID => $request->rol_id,
            RolesModulosModel::MODULO_ID => $request->modulo_id,
        ])->first();

        $det_secciones = $this->menuSeccionesModel->where([
            RolesSeccionesModel::ROL_ID => $request->rol_id,
            RolesSeccionesModel::SECCION_ID => $request->seccion_id,
        ])->first();

        $det_submenu = $this->menuSubMenuModel->where([
            RolesSubmenuModel::ROL_ID => $request->rol_id,
            RolesSubmenuModel::SUBMENU_ID => $request->submenu_id,
        ])->first();

        $det_vista = $this->menusVistasModel->where([
            RolesVistasModel::ROL_ID => $request->rol_id,
            RolesVistasModel::VISTA_ID => $request->vista_id,
        ])->first();

        $det_views_rol = $this->menusVistasModel->where([
            RolesVistasModel::ROL_ID => $request->rol_id
        ])->get();

        DB::beginTransaction();
        if (count($det_views_rol) == 1) {
            if (isset($det_modulos->id)) {
                $delete_modulo = $this->menuModulosModel->destroy($det_modulos->id);
                if ($delete_modulo == 0) {
                    $error = true;
                }
            } else {
                $error = true;
            }
            if (isset($det_secciones->id)) {
                $delete_seccion = $this->menuSeccionesModel->destroy($det_secciones->id);
                if ($delete_seccion == 0) {
                    $error = true;
                }
            } else {
                $error = true;
            }
            if (isset($det_submenu->id)) {
                $delete_submenu = $this->menuSubMenuModel->destroy($det_submenu->id);
                if ($delete_submenu == 0) {
                    $error = true;
                }
            } else {
                $error = true;
            }
        }
        if (isset($det_vista->id)) {
            $delete_vista = $this->menusVistasModel->destroy($det_vista->id);
            if ($delete_vista == 0) {
                $error = true;
            }
        } else {
            $error = true;
        }

        if ($error == true) {
            DB::rollback();
            return false;
        } else {
            DB::commit();
            return true;
        }
    }
}
