<?php

namespace App\Servicios\Polizas;

use App\Models\Autos\UnidadesModel;
use App\Models\Autos\UnidadesNuevas\DetalleCostosRemisionModel;
use App\Models\Autos\UnidadesNuevas\RemisionModel;
use App\Models\Autos\VentasAutosModel;
use App\Models\Contabilidad\CatalogoProcesosModel;
use App\Models\CuentasPorCobrar\CuentasPorCobrarModel;
use App\Models\CuentasPorPagar\CuentasPorPagarModel;
use App\Models\Refacciones\FoliosModel;
use App\Models\Refacciones\VentasRealizadasModel;
use App\Models\Refacciones\VentaServicioModel;
use App\Models\Refacciones\ClientesModel;
use App\Servicios\Refacciones\ServicioCurl;
use GuzzleHttp\Psr7\Request;

class ServicioMovimientos
{
    public function __construct()
    {
        $this->servicioCurl = new ServicioCurl;
        $this->cuentasPorCobrarModel = new CuentasPorCobrarModel();
        $this->cuentasPorPagarModel = new CuentasPorPagarModel();
        $this->ventasModel = new VentasRealizadasModel();
        $this->ventasAutosModel = new VentasAutosModel();
    }

    public function getTipoCuentaxCobrar($cuenta_por_cobrar_id)
    {
        return $this->queryTipocuentasxCobrar($cuenta_por_cobrar_id);
    }

    public function getVentaAutoxFolio($folio_id)
    {

        $tabla_ventas_autos = VentasAutosModel::getTableName();
        $tabla_unidades = RemisionModel::getTableName();
        $tabla_unidades_costos = DetalleCostosRemisionModel::getTableName();

        $query = $this->ventasAutosModel->select(
            $tabla_ventas_autos . '.*',
            $tabla_unidades_costos . '.' . DetalleCostosRemisionModel::C_VALOR_UNIDAD . ' as precio_costo',
            $tabla_unidades . '.' . RemisionModel::C_TOTAL . ' as precio_venta',
            $tabla_unidades . '.' . RemisionModel::ESTATUS_ID
        )
            ->where($tabla_ventas_autos . '.' . VentasAutosModel::ID_FOLIO,  '=',  $folio_id)
            ->leftJoin($tabla_unidades, $tabla_unidades . '.' . RemisionModel::ID, '=', $tabla_ventas_autos . '.' . VentasAutosModel::ID_UNIDAD)
            ->leftJoin($tabla_unidades_costos, $tabla_unidades_costos . '.' . DetalleCostosRemisionModel::REMISIONID, '=', $tabla_unidades . '.' . RemisionModel::ID)
            ->get();
        return $query->first();
    }

    public function getVentaServicioxFolio($folio_id)
    {
        $tabla_ventas = VentasRealizadasModel::getTableName();
        $tabla_venta_servicio = VentaServicioModel::getTableName();
        $query = $this->ventasModel
            ->select(
                $tabla_ventas . '.' . VentasRealizadasModel::ID,
                $tabla_venta_servicio . '.' . VentaServicioModel::PRECIO,
                $tabla_venta_servicio . '.' . VentaServicioModel::IVA,
                $tabla_venta_servicio . '.' . VentaServicioModel::COSTO_MO
            )
            ->join($tabla_venta_servicio, $tabla_venta_servicio . '.' . VentaServicioModel::VENTA_ID, '=', $tabla_ventas . '.' . VentasRealizadasModel::ID)
            ->where($tabla_ventas . '.' . VentasRealizadasModel::FOLIO_ID, '=', $folio_id);
        return $query->first();
    }

    public function getTipoCuentaxPagar($cxp_id)
    {
        return $this->queryTipocuentasxPagar($cxp_id);
    }

    public function queryTipocuentasxCobrar($cuenta_id)
    {
        $tabla_cxc = CuentasPorCobrarModel::getTableName();
        $tabla_folios = FoliosModel::getTableName();
        $tabla_procesos = CatalogoProcesosModel::getTableName();
        $tabla_clientes = ClientesModel::getTableName();
        $query = $this->cuentasPorCobrarModel
            ->select(
                $tabla_cxc . '.' . CuentasPorCobrarModel::ID . ' as id_cxc',
                $tabla_cxc . '.' . CuentasPorCobrarModel::CONCEPTO,
                $tabla_cxc . '.' . CuentasPorCobrarModel::TOTAL,
                $tabla_cxc . '.' . CuentasPorCobrarModel::FECHA,
                $tabla_cxc . '.' . CuentasPorCobrarModel::CLIENTE_ID,
                $tabla_cxc . '.' . CuentasPorCobrarModel::ESTATUS_CUENTA_ID,
                $tabla_cxc . '.' . CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID,
                $tabla_cxc . '.' . CuentasPorCobrarModel::VENTA_TOTAL_DESCUENTO,
                $tabla_folios . '.' . FoliosModel::ID .' as folio_id',
                $tabla_folios . '.' . FoliosModel::FOLIO,
                $tabla_folios . '.' . FoliosModel::TIPO_PROCESO_ID,
                $tabla_procesos . '.' . CatalogoProcesosModel::DESCRIPCION . ' as proceso',
                $tabla_clientes. '.'. ClientesModel::NOMBRE . ' as cliente_nombre',
                $tabla_clientes. '.'. ClientesModel::APELLIDO_PATERNO . ' as cliente_apellido1',
                $tabla_clientes. '.'. ClientesModel::APELLIDO_MATERNO . ' as cliente_apellido2'
            )
            ->join($tabla_folios, $tabla_folios . '.' . FoliosModel::ID, '=', $tabla_cxc . '.' . CuentasPorCobrarModel::FOLIO_ID)
            ->join($tabla_procesos, $tabla_procesos . '.' . CatalogoProcesosModel::ID, '=', $tabla_folios . '.' . FoliosModel::TIPO_PROCESO_ID)
            ->join($tabla_clientes, $tabla_clientes . '.' . ClientesModel::ID, '=', $tabla_cxc . '.' . CuentasPorCobrarModel::CLIENTE_ID)
            ->where($tabla_cxc . '.' . CuentasPorCobrarModel::ID, '=', $cuenta_id);

        return $query->first();
    }

    public function queryTipocuentasxPagar($cuenta_id)
    {
        $tabla_cxp = CuentasPorPagarModel::getTableName();
        $tabla_folios = FoliosModel::getTableName();
        $tabla_procesos = CatalogoProcesosModel::getTableName();
        $query = $this->cuentasPorPagarModel
            ->select(
                $tabla_cxp . '.' . CuentasPorPagarModel::ID . ' as id_cxp',
                $tabla_cxp . '.' . CuentasPorPagarModel::CONCEPTO,
                $tabla_cxp . '.' . CuentasPorPagarModel::TOTAL,
                $tabla_cxp . '.' . CuentasPorPagarModel::FECHA,
                $tabla_cxp . '.' . CuentasPorPagarModel::PROVEEDOR_ID,
                $tabla_folios . '.' . FoliosModel::ID .' as folio_id',
                $tabla_folios . '.' . FoliosModel::FOLIO,
                $tabla_folios . '.' . FoliosModel::TIPO_PROCESO_ID,
                $tabla_procesos . '.' . CatalogoProcesosModel::DESCRIPCION . ' as proceso'
            )
            ->join($tabla_folios, $tabla_folios . '.' . FoliosModel::ID, '=', $tabla_cxp . '.' . CuentasPorPagarModel::FOLIO_ID)
            ->join($tabla_procesos, $tabla_procesos . '.' . CatalogoProcesosModel::ID, '=', $tabla_folios . '.' . FoliosModel::TIPO_PROCESO_ID)
            ->where($tabla_cxp . '.' . CuentasPorPagarModel::ID, '=', $cuenta_id);

        return $query->first();
    }
}
