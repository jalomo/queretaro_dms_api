<?php

namespace App\Servicios\Financiamientos;

use App\Servicios\Core\ServicioDB;
use App\Models\Financiamientos\CatUsoSeguroModel;

class ServicioCatUsoSeguro extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catálogo uso seguros';
        $this->modelo = new CatUsoSeguroModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatUsoSeguroModel::USO_SEGURO => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatUsoSeguroModel::USO_SEGURO => 'required'
        ];
    }
}


