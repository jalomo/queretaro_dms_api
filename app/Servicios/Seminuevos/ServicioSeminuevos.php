<?php

namespace App\Servicios\Seminuevos;

use App\Servicios\Core\ServicioDB;
use App\Models\Autos\UnidadesModel;

class ServicioSeminuevos extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'seminuevos';
        $this->modelo = new UnidadesModel();
    }

    public function getReglasGuardar()
    {
        return [
            UnidadesModel::USUARIO_RECIBE => 'required',
            UnidadesModel::FECHA_RECEPCION => 'required|date',
            UnidadesModel::ID_ESTADO => 'required|numeric',
            UnidadesModel::MARCA_ID => 'required|numeric|exists:catalogo_marcas,id',
            UnidadesModel::MODELO_ID => 'required|numeric|exists:catalogo_modelos,id',
            UnidadesModel::ANIO_ID => 'required|numeric|exists:catalogo_anio,id',
            UnidadesModel::COLOR_ID => 'required|numeric',
            UnidadesModel::MOTOR => 'required',
            UnidadesModel::TRANSMISION => 'required',
            UnidadesModel::KILOMETRAJE => 'required',
            UnidadesModel::NUMERO_CILINDROS => 'required',
            UnidadesModel::CAPACIDAD => 'required',
            UnidadesModel::NUMERO_PUERTAS => 'required',
            UnidadesModel::COMBUSTIBLE => 'required',
            UnidadesModel::PRECIO_COSTO => 'required',
            UnidadesModel::VIN => 'required',
            UnidadesModel::SERIE_CORTA => 'required',
            UnidadesModel::PRECIO_VENTA => 'required',
            UnidadesModel::ID_UBICACION => 'required',
            UnidadesModel::ID_UBICACION_LLAVES => 'required',
            UnidadesModel::NUMERO_ECONOMICO => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            UnidadesModel::USUARIO_RECIBE => 'required',
            UnidadesModel::FECHA_RECEPCION => 'required|date',
            UnidadesModel::ID_ESTADO => 'required|numeric',
            UnidadesModel::MARCA_ID => 'required|numeric|exists:catalogo_marcas,id',
            UnidadesModel::MODELO_ID => 'required|numeric|exists:catalogo_modelos,id',
            UnidadesModel::ANIO_ID => 'required|numeric|exists:catalogo_anio,id',
            UnidadesModel::COLOR_ID => 'required|numeric',
            UnidadesModel::MOTOR => 'required',
            UnidadesModel::TRANSMISION => 'required',
            UnidadesModel::KILOMETRAJE => 'required',
            UnidadesModel::NUMERO_CILINDROS => 'required',
            UnidadesModel::CAPACIDAD => 'required',
            UnidadesModel::NUMERO_PUERTAS => 'required',
            UnidadesModel::COMBUSTIBLE => 'required',
            UnidadesModel::PRECIO_COSTO => 'required',
            UnidadesModel::VIN => 'required',
            UnidadesModel::SERIE_CORTA => 'required',
            UnidadesModel::PRECIO_VENTA => 'required',
            UnidadesModel::ID_UBICACION => 'required',
            UnidadesModel::ID_UBICACION_LLAVES => 'required',
            UnidadesModel::NUMERO_ECONOMICO => 'required'
        ];
    }
}
