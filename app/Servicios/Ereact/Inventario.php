<?php

namespace App\Servicios\Ereact;

use App\Http\Resources\Ereact\InventarioResource;
use App\Models\Refacciones\VentaProductoModel;
use App\Servicios\Ereact\LayoutInterface;
use App\Servicios\Ereact\StringPadTrait;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Inventario implements LayoutInterface
{
    use StringPadTrait;

    protected $inventario;
    protected $fecha;
    protected $fecha_larga;
    protected $renglones;

    public function header(): string
    {
        return "HDRDEALERIN{$this->renglones}VIPIN{$this->fecha}" . PHP_EOL . "IREMEX137       {$this->fecha_larga}". PHP_EOL;
    }

    public function footer(): string
    {
        return "TRLDEALERIN{$this->renglones}";
    }

    public function body(Model $inventario): string
    {
        $linea = '';

        $linea .= 'DREF';
        $linea .= $this->str_pad_unicode($inventario->producto->prefijo, 25, ' ', STR_PAD_RIGHT);
        $linea .= $this->str_pad_unicode($inventario->producto->basico, 25, ' ', STR_PAD_RIGHT);
        $linea .= $this->str_pad_unicode($inventario->producto->sufijo, 25, ' ', STR_PAD_RIGHT);
        $linea .= $this->str_pad_unicode($inventario->producto->no_identificacion, 75, ' ', STR_PAD_RIGHT);
        $linea .= $this->str_pad_unicode(mb_substr($inventario->producto->descripcion, 0, 75), 75, ' ', STR_PAD_RIGHT);
        $linea .= $inventario->producto->created_at->format('Ymd');
        $linea .= $this->str_pad_unicode($inventario->producto->ubicacion == null? 'GEN.' : $inventario->producto->ubicacion->nombre, 20, ' ', STR_PAD_RIGHT);

        $linea .= $this->str_pad_unicode($inventario->mes_1, 10, '0', STR_PAD_LEFT);
        $linea .= $this->str_pad_unicode($inventario->mes_2, 10, '0', STR_PAD_LEFT);
        $linea .= $this->str_pad_unicode($inventario->mes_3, 10, '0', STR_PAD_LEFT);
        $linea .= $this->str_pad_unicode($inventario->mes_4, 10, '0', STR_PAD_LEFT);
        $linea .= $this->str_pad_unicode($inventario->mes_5, 10, '0', STR_PAD_LEFT);
        $linea .= $this->str_pad_unicode($inventario->mes_6, 10, '0', STR_PAD_LEFT);
        $linea .= $this->str_pad_unicode($inventario->mes_7, 10, '0', STR_PAD_LEFT);
        $linea .= $this->str_pad_unicode($inventario->mes_8, 10, '0', STR_PAD_LEFT);
        $linea .= $this->str_pad_unicode($inventario->mes_9, 10, '0', STR_PAD_LEFT);
        $linea .= $this->str_pad_unicode($inventario->mes_10, 10, '0', STR_PAD_LEFT);
        $linea .= $this->str_pad_unicode($inventario->mes_11, 10, '0', STR_PAD_LEFT);
        $linea .= $this->str_pad_unicode($inventario->mes_12, 10, '0', STR_PAD_LEFT);

        $linea .= $this->str_pad_unicode(number_format($inventario->producto->costo_promedio, 2, '.', ''), 10, '0', STR_PAD_LEFT);
        $linea .= $this->str_pad_unicode($inventario->producto->desglose_producto->cantidad_actual, 10, '0', STR_PAD_LEFT);
        $linea .= PHP_EOL;

        return $linea;
    }

    public function getFilename(): string
    {
        return "vip_inv_mex_137_{$this->fecha}.txt";
    }

    public function json(Request $request)
    {
        return response()->json(
            InventarioResource::collection(
                $this->query($request)
            )
        );
    }

    public function download(Request $request)
    {
        $this->inventario = $this->query($request);

        $this->fecha = Carbon::now()->format('ymdHis');
        $this->fecha_larga = Carbon::now()->format('YmdHis');

        $this->renglones = str_pad($this->inventario->count(), 6, "0", STR_PAD_LEFT);
        $filename = storage_path("app/{$this->getFilename()}.txt");

        $file = fopen($filename, 'a');
        fwrite($file, $this->header());

        foreach($this->inventario as $inv)
        {
            fwrite($file, $this->body($inv));
        }

        fwrite($file, $this->footer());
        fclose($file);

        return response()->download($filename, $this->getFilename(), ['Content-Type' => 'text/plain'])->deleteFileAfterSend();
    }

    public function query(Request $request): Collection
    {
        if($request->fecha_inicio == null)
        {
            $request->merge(['fecha_inicio' => Carbon::now()->format('Y-m-d')]);
        }

        $fecha_inicio =  Carbon::createFromFormat('Y-m-d', $request->fecha_inicio)->startOfMonth()->subMonthNoOverflow(12)->toDateString();
        $fecha_termino = Carbon::createFromFormat('Y-m-d', $request->fecha_inicio)->subMonthNoOverflow(1)->endOfMonth()->toDateString();

        // Hacemos los filtros de fechas para no tener que hacerlos directamente
        // en la base de datos y haya problemas de compatibilidad
        for($i = 12; $i >= 1; $i--)
        {
            $mes_inicio[$i] = Carbon::createFromFormat('Y-m-d', $request->fecha_inicio)->startOfMonth()->subMonthsNoOverflow($i)->toDateString();
            $mes_fin[$i] = Carbon::createFromFormat('Y-m-d', $request->fecha_inicio)->subMonthsNoOverflow($i)->endOfMonth()->toDateString();
        }

        $ventas = VentaProductoModel::with('producto', 'producto.ubicacion', 'producto.desglose_producto')
            ->join('ventas', 'venta_producto.venta_id', 'ventas.id')
            ->whereHas('venta', function($q) use($fecha_inicio, $fecha_termino){
                $q->when($fecha_inicio, function($q) use ($fecha_inicio){
                    $q->whereDate('created_at', '>=', $fecha_inicio);
                });
                $q->when($fecha_termino, function($q) use ($fecha_termino){
                    $q->whereDate('created_at', '<=', $fecha_termino);
                });
                $q->where('estatus_producto_orden', '<>', 'B');
            })
            ->groupBy('producto_id')
            ->select([
                DB::raw("SUM(CASE WHEN ventas.created_at >= '{$mes_inicio[12]}' AND ventas.created_at <= '{$mes_fin[12]}' THEN cantidad ELSE 0 END) as mes_1"),
                DB::raw("SUM(CASE WHEN ventas.created_at >= '{$mes_inicio[11]}' AND ventas.created_at <= '{$mes_fin[11]}' THEN cantidad ELSE 0 END) as mes_2"),
                DB::raw("SUM(CASE WHEN ventas.created_at >= '{$mes_inicio[10]}' AND ventas.created_at <= '{$mes_fin[10]}' THEN cantidad ELSE 0 END) as mes_3"),
                DB::raw("SUM(CASE WHEN ventas.created_at >= '{$mes_inicio[9]}' AND ventas.created_at <= '{$mes_fin[9]}' THEN cantidad ELSE 0 END) as mes_4"),
                DB::raw("SUM(CASE WHEN ventas.created_at >= '{$mes_inicio[8]}' AND ventas.created_at <= '{$mes_fin[8]}' THEN cantidad ELSE 0 END) as mes_5"),
                DB::raw("SUM(CASE WHEN ventas.created_at >= '{$mes_inicio[7]}' AND ventas.created_at <= '{$mes_fin[7]}' THEN cantidad ELSE 0 END) as mes_6"),
                DB::raw("SUM(CASE WHEN ventas.created_at >= '{$mes_inicio[6]}' AND ventas.created_at <= '{$mes_fin[6]}' THEN cantidad ELSE 0 END) as mes_7"),
                DB::raw("SUM(CASE WHEN ventas.created_at >= '{$mes_inicio[5]}' AND ventas.created_at <= '{$mes_fin[5]}' THEN cantidad ELSE 0 END) as mes_8"),
                DB::raw("SUM(CASE WHEN ventas.created_at >= '{$mes_inicio[4]}' AND ventas.created_at <= '{$mes_fin[4]}' THEN cantidad ELSE 0 END) as mes_9"),
                DB::raw("SUM(CASE WHEN ventas.created_at >= '{$mes_inicio[3]}' AND ventas.created_at <= '{$mes_fin[3]}' THEN cantidad ELSE 0 END) as mes_10"),
                DB::raw("SUM(CASE WHEN ventas.created_at >= '{$mes_inicio[2]}' AND ventas.created_at <= '{$mes_fin[2]}' THEN cantidad ELSE 0 END) as mes_11"),
                DB::raw("SUM(CASE WHEN ventas.created_at >= '{$mes_inicio[1]}' AND ventas.created_at <= '{$mes_fin[1]}' THEN cantidad ELSE 0 END) as mes_12"),
                DB::raw("SUM(cantidad) as cantidad"),
                'producto_id',
            ])
            ->get();

        return $ventas;
    }
}