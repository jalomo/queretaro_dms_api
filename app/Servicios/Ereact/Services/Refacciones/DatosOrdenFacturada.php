<?php

namespace App\Servicios\Ereact\Services\Refacciones;

use App\Servicios\Ereact\Services\Interfaces\Get;
use App\Servicios\Ereact\Services\Interfaces\WebService;
use App\Servicios\Ereact\Services\Traits\GetTrait;

class DatosOrdenFacturada implements WebService, Get
{
    use GetTrait;
    
    public function getPathWebservice(): string
    {
        return 'https://mylsa.iplaneacion.com/dms/queretaro_dms_api/public/api/cuentas-por-cobrar/ereact-por-numero_orden';
    }

    public function call(array $filters): array
    {
        $headers = [
            'Accept: application/json',
            'Content-Type: multipart/form-data',
        ];

        $headers_retrieve = [];

        // $parameters = http_build_query($filters);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "{$this->getPathWebservice()}");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);

        // Get Headers
        curl_setopt($ch, CURLOPT_HEADERFUNCTION, function($curl, $header) use (&$headers_retrieve){
            $len = strlen($header);
            $header = explode(':', $header, 2);

            if (count($header) < 2) // ignore invalid headers
                return $len;

            $headers_retrieve[strtolower(trim($header[0]))][] = trim($header[1]);
            
            return $len;
        });

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $filters);

        $response = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if($http_code != 200)
        {
            logger()->warning("{$filters['numero_orden']}: ERROR WEBSERVICE -> " . $this->getPathWebservice(), $headers_retrieve);
            throw new \Exception("{$filters['numero_orden']}: ERROR WEBSERVICE -> " . $this->getPathWebservice());
        }

        return json_decode($response, true);
    }
}