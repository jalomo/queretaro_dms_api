<?php

namespace App\Servicios\Ereact\Services\Refacciones;

use App\Servicios\Ereact\Services\Interfaces\Get;
use App\Servicios\Ereact\Services\Interfaces\WebService;
use App\Servicios\Ereact\Services\Resources\RefaccionesResource;
use App\Servicios\Ereact\Services\Traits\GetTrait;

class RefaccionesByOrdenes implements WebService, Get
{
    use GetTrait;
    
    public function getPathWebservice(): string
    {
        return 'https://mylsa.iplaneacion.com/servicios/queretaro/api/orders_details';
    }

    public function call(array $filters): array
    {
        $headers = [
            'Accept: application/json',
            'Content-Type: application/json',
        ];

        $parameters = http_build_query($filters);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "{$this->getPathWebservice()}");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([ 'orders' => [$filters['numero_orden']]]));

        $response = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if($http_code != 200)
            throw new \Exception('Error en la llamada al servicio web: ' . $this->getPathWebservice());

        $object = json_decode($response, false);

        // Devolvemos el objeto orden dentro de el numero de orden
        return RefaccionesResource::collection($object->{$filters['numero_orden']}->parts)->resolve();
    }
}