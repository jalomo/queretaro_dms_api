<?php

namespace App\Servicios\Ereact\Services\Refacciones;

use App\Models\Refacciones\VentaProductoModel;
use App\Servicios\Ereact\Services\Interfaces\Get;
use App\Servicios\Ereact\Services\Interfaces\WebService;
use App\Servicios\Ereact\Services\Resources\VentaProductoResource;
use App\Servicios\Ereact\Services\Traits\GetTrait;

class RefaccionesByOrdenFacturada implements WebService, Get
{
    use GetTrait;
    
    public function getPathWebservice(): string
    {
        return '';
    }

    // public function call(array $filters): array
    // {
    //     $headers = [
    //         'Accept: application/json',
    //         'Content-Type: application/json',
    //     ];

    //     $parameters = http_build_query($filters);

    //     $ch = curl_init();
    //     curl_setopt($ch, CURLOPT_URL, "{$this->getPathWebservice()}?{$parameters}");
    //     curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //     curl_setopt($ch, CURLOPT_HEADER, false);

    //     $response = curl_exec($ch);
    //     $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    //     curl_close($ch);

    //     if($http_code != 200)
    //         throw new \Exception('Error en la llamada al servicio web: ' . $this->getPathWebservice());

    //     return json_decode($response);
    // }

    public function call(array $filters): array
    {
        $venta_productos = VentaProductoModel::whereHas('venta', function($query) use ($filters){
            $query->where('numero_orden', $filters['numero_orden']);
        })
        ->with('producto')
        ->get();

        return VentaProductoResource::collection($venta_productos)->resolve();
    }
}