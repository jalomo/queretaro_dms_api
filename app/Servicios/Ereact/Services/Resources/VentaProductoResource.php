<?php

namespace App\Servicios\Ereact\Services\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VentaProductoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'prefijo' => $this->producto->prefijo?? '',
            'basico' => $this->producto->basico?? '',
            'sufijo' => $this->producto->sufijo?? '',
            'cantidad_parte' => $this->cantidad,
            'precio_parte' => $this->valor_unitario * $this->cantidad,
            'costo_parte' => ($this->valor_unitario * $this->cantidad) - $this->core,
            'descripcion_parte' => $this->producto->descripcion,
        ]; 
    }
}
