<?php

namespace App\Servicios\Ereact\Services\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrdenesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'fecha_inicio' => $this->inicio_trabajo?? '',
            'fecha_fin' => $this->hora_fin_trabajo_operacion?? '',
            'tipo_reparacion' => $this->tipo?? '', //! Falta
            'cde_dpo' => $this->cantidad, //! Falta
            'cde_reparacion' => $this->tipo, //! Falta
            'cargo_operacion' => $this->total,
            'descripcion_trabajo' => $this->descripcion,
            'num_tec' => $this->idStartecnico, //! Falta Pita
            'idStars' => $this->idStartecnico,
            'horas_facturadas' => $this->total_horas,
        ]; 
    }
}
