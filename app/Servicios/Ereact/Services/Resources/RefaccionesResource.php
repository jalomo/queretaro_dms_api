<?php

namespace App\Servicios\Ereact\Services\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RefaccionesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'fecha_agregada' => $this->fecha_agregada ??'', //! Falta
            'prefijo' => $this->producto->prefijo?? '',
            'basico' => $this->parte?? '',
            'sufijo' => $this->producto->sufijo?? '',
            'cantidad_parte' => $this->cantidad,
            'precio_parte' => $this->precio,
            'costo_parte' => $this->precio, //! Falta
            'descripcion_parte' => $this->descripcion_parte,
            'ident_ase_tec' => '', //! Falta
            'idstars_ase_tec' => '', //! Falta
        ]; 
    }
}
