<?php

namespace App\Servicios\Ereact\Services;

use App\Servicios\Ereact\Services\Refacciones\DatosOrdenFacturada;
use App\Servicios\Ereact\Services\Refacciones\OperacionesByOrdenes;
use App\Servicios\Ereact\Services\Refacciones\OrdenesFacturadas;
use App\Servicios\Ereact\Services\Refacciones\RefaccionesByOrdenes;
use App\Servicios\Ereact\Services\Refacciones\RefaccionesByOrdenFacturada;
use App\Models\Servicios\Citas;

use DateTime;
use App\Models\Refacciones\HistorialEreact;

class Ereact 
{
    const BIN_SUCURSAL = 'M2137';
    protected $fecha_transmision;
    protected $renglones = 0;
    protected $consecutivo = null;
    protected $ci;

    protected $tipo_reparacion = [
        'I' => 'I',
        'G' => 'W',
        'T' => 'C',
        'E' => 'E',
        'H' => 'A',
    ];

    protected $codigos_ind_hm = [
        "99P" => "INSPECCION HOJA MULTIPUNTOS",
        "BATEV" => "INSPECCION BATERIA",
        "BALATAV" => "INSPECCION BALATA",
        "DISCOV" => "INSPECCION DISCOS",
        "TAMBORV" => "INSPECCION TAMBORES",
        "LLANTAV" => "INSPECCION LLANTAS",
    ];

    protected $filters = [];

    public function __construct(array $filters = [])
    {
        $this->fecha_transmision = new DateTime;

        $this->filters = [
            'no_pagination' => true,
            'fecha_inicio'=> (new DateTime)->format('Y-m-d'),
            'fecha_fin'=> (new DateTime)->format('Y-m-d'),
            'tipo_proceso_id' => 8,
        ] + $filters;
    }

    public function getFilename()
    {
        // eREACT_MEX + Codigo de Distribución + YYYYMMDDHHiiss + .txt
        return "eREACT_MEXXXX". date("YmdHis"). ".txt";
    }

    public function make(array $filters = [])
    {
        $filters = array_merge($this->filters, $filters);
        
        $ordenes = OrdenesFacturadas::get($filters);

        // Codigo de distribucion
        $cod_dist = 'XXX'; 
        //Nombre del Archivo
        $filename = storage_path("ereact/eREACT_MEX".$cod_dist.date("YmdHis").".txt");

        $historial = HistorialEreact::create([
            'fecha_transmision' => $this->fecha_transmision->format('Y-m-d H:i:s'),
            'nombre_archivo' => $filename,
            'filters' => json_encode($filters),
            // Sacamos el consecutivo la numeración va del 0-99
            'consecutivo' => (HistorialEreact::latest()->first()->id + 1) % 100, 
        ]);

        $this->consecutivo = $historial->consecutivo;

        // $folio = $this->fecha_transmision->format('ymd') . "_" . $this->generateRandomString() . $this->fecha_transmision->format('His');
        
        $stream = fopen($filename, 'wb');

        // Cabecera de todo el Documento
        fwrite($stream, $this->getCabeceraDocumento());

        foreach($ordenes as $orden)
        {
            // Cabecera de Ordenes
            // Registro de Identificacion C1
            fwrite($stream, $this->getCabeceraOrden($orden->numero_orden));

            // Datos de Operaciones por Orden
            // Registro de Identificacion 01
            fwrite($stream, $this->getCuerpoOrdenOperaciones($orden->numero_orden));

            // Datos de Refacciones por Orden
            // Registro de Identificación 02
            fwrite($stream, $this->getCuerpoOrdenRefacciones($orden->numero_orden));

            // Datos de Facturación
            // Registro de Identificación P1
            fwrite($stream, $this->getDatosFacturacion($orden->numero_orden));

            // Datos de Facturación Por Refaccion
            // Registro de Identificación 03
            fwrite($stream, $this->getDatosFacturacionRefacciones($orden->numero_orden));
        }

        // Pie de todo el Documento
        fwrite($stream, $this->getPieDocumento());

        fclose($stream);

        // Marcamos el registro como completo
        $historial->completo = now();
        $historial->save();

        return response()->download($filename);
    }

    /**
     * Cabecera General de todo el documento
     *
     * @return string
     */
    public function getCabeceraDocumento(): string
    {
        // Encabezado del archivo
        //Identificación del registro del encabezado  (# 96)
        $cabecera = '$CUD-';
        //No de BID del distribuidor (# 90) 
        $cabecera .= Separador::left(self::BIN_SUCURSAL, 5, '0', 0);
        //Sub-número de Distribuidor 
        $cabecera .= '-136-';
        //Etiqueta 1 (6 caracteres)
        $cabecera .= '000000';
        //Etiqueta 2 (13 caracteres)
        $cabecera .= '-COMBAT  -KA-';
        // Etiqueta 3 (9 caracteres)
        $cabecera .= '00000000-';
        // Etiqueta 4 (5 caracteres)
        $cabecera .= '4.01G';
        // Etiqueta 5 (2 caracteres)
        $cabecera .= 'CD';
        //Fecha de transmisión (# 29) "MMDDYY"  (PENDIENTE confirmar)
        $cabecera .= $this->fecha_transmision->format('mdy');
        //Numero de Archivo Generados(# 30)
        $cabecera .= Separador::numeric($this->consecutivo, 2, 0);
        // Etiqueta 6 (1 caracter)
        $cabecera .= '-';
        // Codigo area geografica (9 caracteres)
        $cabecera .= Separador::left('', 9);
        // Etiqueta 7 (1 caracter)
        $cabecera .= '-';
        // Etiqueta 8 (17 caracteres)
        $cabecera .= Separador::left('', 17);
        // Etiqueta 9 (5 caracteres)
        $cabecera .= Separador::left('4.01G', 5);
        //Fecha de transmisión (# 29) "MMDDYY"
        $cabecera .= $this->fecha_transmision->format('mdy');
        //Numero de Archivo Generados(# 30)
        $cabecera .= Separador::numeric($this->consecutivo, 2, 0);
        //Cuenta de Registros (# 53)  (No aplica para méxico)
        $cabecera .= Separador::left('00000000', 8);
        //Sub-número de Distribuidor (# 120) (No aplica para méxico) 5 espacios
        $cabecera .= Separador::right('', 5);
        // Etiqueta 10 (8 caracteres)
        $cabecera .= '00000000';
        // Etiqueta 11 (3 caracteres)
        $cabecera .= 'CUD';
        // Etiqueta 12 (5 caracteres)
        $cabecera .= '4.01G';
        //Código de área geográfica (# 121) (3 caracteres)
        $cabecera .= 'MEX';
        //Código de Franquicia (# 373) (2 caracteres)
        //1 = Ford  3 = Lincoln o Mercury  U = No se sabe
        $cabecera .= Separador::right('1', 2);
        //Versión del software del DMS (9 caracteres)
        $cabecera .= Separador::right('SIMYLSA', 9);
        // Etiqueta 13 (11 caracteres)
        $cabecera .= '00136COMBAT';
        // Etiqueta 14 (2 caracteres)
        $cabecera .= Separador::right('', 2);
        // Etiqueta 15 (2 caracteres)
        $cabecera .= 'KA';
        // Etiqueta 16 (6 caracteres)
        $cabecera .= '000000';
        // Etiqueta 17 (3 caracteres)
        $cabecera .= '00D';
        //Clave de Proveedor (# 70) (6 caracteres)
        //Compañía = Dealer Computer Services, Hardware = Digital Rainbow 100, Clave de Proveedor = DCS100 
        $cabecera .= 'PRO027';
        //Tipo de Archivo (#397) (1 caracter)
        //H: Para archivos históricos D: Para indicar que es un archivo normal con frecuencia diaria 
        $cabecera .= 'D';
        //Indicador Multibyte (1 caracter)
        //S: Para indicar si es single byte M: Indica si es mutli byte 
        $cabecera .= 'M';
        // Fin de encabezado (1 caracter)
        $cabecera .= '>';
        // Salto de linea
        $cabecera .= PHP_EOL;

        $this->renglones++;

        return $cabecera;
    }

    /**
     * Este es el o los encabezados de las ordenes
     * ! Aun no se si es de orden o todas las ordenes juntas
     *
     * @return string
     */
    public function getCabeceraOrden($numero_orden): string
    {
        $numero_orden = $this->getNumeroOrden($numero_orden);

        // Quizas aqui se haga una consulta por cita voy a verificar eso
        $datos_servicio = Citas::DatosOrden($numero_orden)->first();

        // Creo que estos datos se obtienen de la api de Martin
        $factura_f = "0000-00-00 00:00:00";
        $costo_factura = 0;
        $iva_factura = 0;

        $fecha_apertura = new DateTime($datos_servicio["apertura_orden"]);
        $fecha_alta_cita = new DateTime($datos_servicio["fecha_alta_cita"]);
        $fecha_cita = new DateTime($datos_servicio["fecha_cita"]);
        $fecha_promesa = new DateTime($datos_servicio["fecha_entrega"]." ".$datos_servicio["hora_entrega"]);

        //Revisamos que exista del dato
        $fecha_cierre = (! $datos_servicio["cierre_orden"] == NULL) ? new DateTime($datos_servicio["cierre_orden"]) : NULL;

        //Revisamos que exista del dato
        $fecha_entrega = (! $datos_servicio["fecha_entrega_unidad"] == NULL) ? new DateTime($datos_servicio["fecha_entrega_unidad"]) : NULL;

        $fecha_adicionales = "00000000";
        $hora_adicionales = "0000";
        $monto_adicionales = "00000000000000";

        //Revisamos si existe un presupuesto
        //Confirmamos que el presupuesto haya sido afectado por el cliente
        if ( $datos_servicio["id_presupuesto"] != NULL && ( $datos_servicio["acepta_cliente"] == "Val" || $datos_servicio["acepta_cliente"] == "Si" )) 
        {
            $fecha_presupuesto = new DateTime($datos_servicio["termina_cliente"]);
            $fecha_adicionales = $fecha_presupuesto->format('Ymd');
            $hora_adicionales = $fecha_presupuesto->format('Hi');
            $monto_adicionales = $datos_servicio["total"];
        }

        $fecha_factura = new DateTime($factura_f);

        $serie = $datos_servicio["vehiculo_identificacion"]?? $datos_servicio["vehiculo_numero_serie"];
        $cliente = $datos_servicio["num_Cliente_cita"]?? $datos_servicio["num_Cliente_orden"];

        //Inicio de Cabecera (2 caracteres)
        $cabecera_orden = 'C1';
        // Serie (17 caracteres)
        $cabecera_orden .= Separador::right($serie, 17);
        // numero orden (16 caracteres)
        $cabecera_orden .= Separador::left($numero_orden, 16);
        // numero orden anterior (16 caracteres)
        $cabecera_orden .= Separador::left('', 16);
        // NSS Asesor (6 caracteres)
        $cabecera_orden .= Separador::right('', 6);
        // Fecha de Apertura orden (8 caracteres)
        $cabecera_orden .= Separador::left($fecha_apertura->format('Ymd'), 8);
        // Fecha de Cierre Orden (8 caracteres)
        $cabecera_orden .= Separador::left($fecha_cierre->format('Ymd'), 8);
        // Kilometraje unidad (6 caracteres)
        $cabecera_orden .= Separador::numeric($datos_servicio["km"], 6, 0);
        // Indicador Odometro M = Millas K = Kilometros (1 caracteres)
        $cabecera_orden .= Separador::left('K', 1);
        // Nombre del Cliente (30 caracteres)
        $cabecera_orden .= Separador::right($datos_servicio["datos_nombres"], 30);
        // Segundo nombre espacio(1 caracteres)
        $cabecera_orden .= Separador::left('', 1);
        // Apellido (80 caracteres)
        $cabecera_orden .= Separador::right($datos_servicio["datos_apellido_paterno"]." ".$datos_servicio["datos_apellido_materno"], 80);
        // Tipo cliente 1 = Persona Fisica 2 = Persona Moral
        $cabecera_orden .= Separador::right('1', 5);
        // Direccion Principal (2 caracteres)
        $cabecera_orden .= Separador::right($datos_servicio["calle"]." #".$datos_servicio["noexterior"]." Col. ".$datos_servicio["colonia"], 80);
        // Segunda Direccion (80 caracteres)
        $cabecera_orden .= Separador::right('', 80);
        // Ciudad (30 caracteres)
        $cabecera_orden .= Separador::right($datos_servicio["municipio"], 30);
        // Estado (30 caracteres)
        $cabecera_orden .= Separador::right($datos_servicio["sigla_estado"], 30);
        // Codigo Postal (20 caracteres)
        $cabecera_orden .= Separador::right($datos_servicio["cp"], 20);
        // Telefono Principal (20 caracteres)
        $cabecera_orden .= Separador::right($datos_servicio["datos_telefono"], 20);
        // Segundo Telefono (20 caracteres)
        $cabecera_orden .= Separador::right($datos_servicio["telefono_movil"], 20);
        // Monto Neto pagado por el cliente (14 caracteres)
        $cabecera_orden .= Separador::numeric($costo_factura?? '0.00', 14);
        // IVA o Impuestos (14 caracteres)
        $cabecera_orden .= Separador::numeric($iva_factura?? '0.00', 14);
        // Placas Vehiculo (20 caracteres)
        $cabecera_orden .= Separador::left($datos_servicio["vehiculo_placas"], 20);
        // Hora de apertura de la orden (4 caracteres)
        $cabecera_orden .= Separador::left($fecha_apertura->format('Hi'), 4);
        // Hora  de cierre de la orden (4 caracteres)
        $cabecera_orden .= Separador::left($fecha_cierre->format('Hi'), 4);
        // Tipo Cita (1 caracteres) A = Iniciado por Distribuidor
        // I = Internet, P = Telefono, W = LLevado por cliente, U No se sabe
        $cabecera_orden .= Separador::left($datos_servicio["codigo"]?? 'U', 1);
        // Fecha de Alta de la Cita
        $cabecera_orden .= Separador::left($fecha_alta_cita->format('Ymd'), 8);
        // Tipo Cliente (1 caracter) D = Distribuidor, F = Flotilla
        // V = Gobierno, I = Interno, R = Menudeo, U = Desconocido
        $cabecera_orden .= Separador::left('U', 1);
        // Identificación asesor (10 caracteres)
        $cabecera_orden .= Separador::right($datos_servicio["clave_asesor"], 10);
        // idstars_asesor_tecnico (9 caracteres)
        $cabecera_orden .= Separador::right($datos_servicio["aidStars"], 9);
        // Correo (100 caracteres)
        $cabecera_orden .= Separador::left($datos_servicio["datos_email"]?? 'notienecorreo@notienecorreo.com.mx', 100);
        // Telefono Adicional (20 caracteres)
        $cabecera_orden .= Separador::left($datos_servicio["otro_telefono"], 20);
        // Codigo País (3 caracteres)
        $cabecera_orden .= Separador::right('MEX', 3);
        // Direccion 3 (80 caracteres)
        $cabecera_orden .= Separador::right('', 80);
        // Descripcion Compañia (30 caracteres)
        $cabecera_orden .= Separador::right($datos_servicio["nombre_compania"], 30);
        // Correo empresa (100 caracteres)
        $cabecera_orden .= Separador::right($datos_servicio["correo_compania"], 100);
        // Correo 2 (100 caracteres)
        $cabecera_orden .= Separador::right('', 100);
        // Correo 3 (100 caracteres)
        $cabecera_orden .= Separador::right('', 100);
        // Fecha Cita (8 caracteres)
        $cabecera_orden .= Separador::left($fecha_cita->format('Ymd'), 8);
        // Hora Cita (4 caracteres)
        $cabecera_orden .= Separador::left($fecha_cita->format('Hi'), 4);
        // Costo Estimado (14 caracteres)
        $cabecera_orden .= Separador::numeric('', 14);
        // Indicador de Espera (1 caracteres) Y = si, N = no
        $cabecera_orden .= Separador::left(($datos_servicio["id_situacion_intelisis"] == '6' || $datos_servicio["id_situacion_intelisis"] == '5') ? "N" : "Y", 1);
        // Fecha Factura (8 caracteres)
        $cabecera_orden .= Separador::left($fecha_factura->format('Ymd'), 8);
        // Hora Factura (4 caracteres)
        $cabecera_orden .= Separador::left($fecha_factura->format('Hi'), 4);
        // Fecha Adicionales Presupuesto (8 caracteres)
        $cabecera_orden .= Separador::left($fecha_adicionales, 8);
        // Hora Adicionales Presupuesto (4 caracteres)
        $cabecera_orden .= Separador::left($hora_adicionales, 4);
        // Monto Adicional Presupuesto (14 caracteres)
        $cabecera_orden .= Separador::numeric($monto_adicionales, 14);
        // Fecha de Promesa de Entrega (8 caracteres)
        $cabecera_orden .= Separador::left($fecha_promesa->format('Ymd')?? '', 8);
        // Hora de Promesa de Entrega (4 caracteres)
        $cabecera_orden .= Separador::left($fecha_promesa->format('Hi')?? '', 4);
        // Fecha de Entrega (8 caracteres)
        $cabecera_orden .= Separador::left($fecha_entrega? $fecha_entrega->format('Ymd'): '', 8);
        // Hora de Entrega (4 caracteres)
        $cabecera_orden .= Separador::left($fecha_entrega? $fecha_entrega->format('Hi') : '', 4);
        // Programa de Lealtad (20 caracteres)
        $cabecera_orden .= Separador::right('', 20);
        // Numero de Cliente (10 caracteres)
        $cabecera_orden .= Separador::right($cliente, 10);
        //Permiso Dealer para uso de correo electrónico del cliente 
        $cabecera_orden .= 'Y';
        //Permiso Ford de México para uso de correo electrónico del cliente 
        $cabecera_orden .= 'Y';
        //Permiso Dealer para uso de correo electrónico del cliente 1 
        $cabecera_orden .= 'N';
        //Permiso Ford de México para uso de correo electrónico del cliente 1 
        $cabecera_orden .= 'N';
        //Permiso Dealer para uso de correo electrónico del cliente 2
        $cabecera_orden .= 'N';
        //Permiso Ford de México para uso de correo electrónico del cliente 2
        $cabecera_orden .= 'N';
        //Permiso Dealer para uso de correo electrónico del cliente 3
        $cabecera_orden .= 'N';
        //Permiso Ford de México para uso de correo electrónico del cliente 3
        $cabecera_orden .= 'N';
        //Permiso Dealer para uso de correo electrónico de la empresa
        $cabecera_orden .= 'N';
        //Permiso Ford de México para uso de correo electrónico de la empresa
        $cabecera_orden .= 'N';
        //Permiso Dealer para uso del telefono principal del cliente
        $cabecera_orden .= 'N';
        //Permiso Ford de México para uso del telefono principal del cliente
        $cabecera_orden .= 'N';
        //Permiso Dealer para uso del telefono celular del cliente
        $cabecera_orden .= 'N';
        //Permiso Ford de México para uso del telefono celular del cliente
        $cabecera_orden .= 'N';
        //Permiso Dealer para uso del telefono secundario del cliente
        $cabecera_orden .= 'N';
        //Permiso Ford de México para uso del telefono secundario del cliente
        $cabecera_orden .= 'N';
        //Permiso Dealer para uso de mensajes SMS del cliente
        $cabecera_orden .= 'N';
        //Permiso Ford de México para uso de mensajes SMS del cliente
        $cabecera_orden .= 'N';
        //Permiso Dealer para uso de mensajes multimedia del cliente
        $cabecera_orden .= 'N';
        //Permiso Ford de México para uso de mensajes multimedia del cliente
        $cabecera_orden .= 'N';
        // Final de Linea
        $cabecera_orden .= '>';
        // Salto de linea   
        $cabecera_orden .= PHP_EOL;

        $this->renglones++;

        return $cabecera_orden;
    }

    /**
     * Este es el resumen de las operaciones de una orden
     * Falta definir si es encabezado orden, operaciones y refacciones o
     * encabezado de todas las ordenes
     *
     * @param string $numero_orden
     * @return string
     */
    public function getCuerpoOrdenOperaciones($numero_orden) : string
    {
        $numero_orden = $this->getNumeroOrden($numero_orden);
        $operaciones = OperacionesByOrdenes::get([ 'numero_orden' => $numero_orden ]);
        $cuerpo = '';
        
        foreach($operaciones as $key => $operacion)
        {
            // Convertimos las fecha obtenidas string a DateTiem
            $fecha_tbinicio = new DateTime($operacion["fecha_inicio"]);
            $fecha_tbfin = new DateTime($operacion["fecha_fin"]);

            //Tipo de Registro (# 96) (2 caracteres)
            $cuerpo .= '01';
            //Numero de secuencia (# 375) (2 caracteres)
            // 1 = 00, 2 = 01, 3 = 02, etc.
            $cuerpo .= Separador::numeric($key, 2, 0);
            //Tipo de Reparación (#157)  (1 caracter)
            // C = Pagado por cliente, E = Extension servicio, I = Interno
            // W Garantia, A = Politica Servicio Ford, U = No se sabe
            $cuerpo .= $operacion["tipo_reparacion"];
            //# Seguro Social del Técnico de servicio  (# 131) (6 caracteres)
            // No aplica Mexico
            $cuerpo .= Separador::left('', 6);
            //Código del departamento (# 161) (1 caracter)
            // B = Body Shop, D = Preparacion Distribuidor, O = Subcontratado,
            // Q = Express, U = No se sabe
            $cuerpo .= Separador::left($operacion["cde_dpo"], 1);
            //Código de Operación (# 162) (20 caracteres)
            $cuerpo .= Separador::right($operacion["cde_reparacion"], 20);
            //Cargo por operación (# 335) (14 caracteres)
            $cuerpo .= Separador::numeric($operacion["cargo_operacion"], 14);
            // Horas facturadas (# 255) (4 caracteres)
            // Ejemplo 2.5 horas = 0025
            $cuerpo .= Separador::numeric($operacion["horas_facturadas"], 4, 1);
            //Descripción del servicio misceláneo  o subcontratado (# 289) (150 caracteres)
            $cuerpo .= Separador::left('', 150);
            //Monto misceláneo o subcontratado (# 337) (140 caracteres)
            $cuerpo .= Separador::numeric('', 140);
            //Descripción del trabajo (# 163) (256 caracteres)
            $cuerpo .= Separador::right($operacion["descripcion_trabajo"], 256);
            //Identificador del asesor técnico en la distribuidora  (# 381) (10 caracteres)
            $cuerpo .= Separador::right($operacion["num_tec"], 10);
            //Identificador del asesor técnico en  STARS (# 388) (9 caracteres)
            $cuerpo .= Separador::right($operacion["idStars"], 9);
            //Fecha de inicio del trabajo del técnico (#536) (8 caracteres)
            $cuerpo .= Separador::left($fecha_tbinicio->format('Ymd')?? '', 8);
            //Hora de inicio del trabajo del técnico (#537) (4 caracteres)
            $cuerpo .= Separador::left($fecha_tbinicio->format('Hi')?? '', 4);
            //Fecha de término del trabajo del técnico (#538) (8 caracteres)
            $cuerpo .= Separador::left($fecha_tbfin->format('Ymd')?? '', 8);
            //Hora de termino del trabajo del técnico (#539) (4 caracteres)
            $cuerpo .= Separador::left($fecha_tbfin->format('Hi')?? '', 4);
            //Servicios rechazados del código de operación (#540) (20 caracteres)
            $cuerpo .= Separador::right('', 20);
            //Descripción de servicios rechazados del código de operación (#539) 
            $cuerpo .= Separador::right('', 256);
            // Fin de cuerpo orden operaciones
            $cuerpo .= '>';
            // Salto de linea
            $cuerpo .= PHP_EOL;

            $this->renglones++;
        }

        return $cuerpo;
    }


    /**
     * Aqui va el resumen de todas las refacciones de la orden
     *
     * @param string $numero_orden
     * @return string
     */
    public function getCuerpoOrdenRefacciones($numero_orden): string
    {
        $numero_orden = $this->getNumeroOrden($numero_orden);

        $refacciones_orden = RefaccionesByOrdenes::get([ 'numero_orden' => $numero_orden ]);
        
        $cuerpo = '';

        foreach($refacciones_orden as $refacciones)
        {
            $fecha_agregada = new DateTime($refacciones['fecha_agregada']);
            //Tipo de Registro (# 96) (2 caracteres)
            $cuerpo .= '02';
            //Prefijo del número de parte (# 186) (6 caracteres)
            $cuerpo .= Separador::left($refacciones["prefijo"], 6);
            //Básico del número de Parte (# 187) (8 caracteres)
            $cuerpo .= Separador::left($refacciones["basico"], 8);
            //Sufijo del número de parte (# 188) (8 caracteres)
            $cuerpo .= Separador::right($refacciones["sufijo"], 8);
            //Cantidad de Partes (#338) (7 caracteres)
            $cuerpo .= Separador::numeric($refacciones["cantidad_parte"] * -1, 7, 0);
            //Precio de venta del total de la refaccion (# 352) (14 caracteres)
            $cuerpo .= Separador::numeric($refacciones["precio_parte"], 14);
            //Costo de la parte (# 353) (14 caracteres)
            //Nota: Costo Neto del Distribuidor. Costo Unitario de la parte 
            //Costo incluyendo Core, si aplica. Cargos/ Créditos  por Core  Incluidos 
            // (182 cargo por core) 
            $cuerpo .= Separador::numeric($refacciones["costo_parte"], 14);
            //Descripción de la parte (# 374) (50 caracteres)
            $cuerpo .= Separador::right($refacciones["descripcion_parte"], 50);
            //! Causa de falla de la parte (#354) (2 caracteres) Rota supongo es el codigo
            $cuerpo .= Separador::right('', 2);
            //# Seguro Social del asesor Técnico  (# 131) (6 caracteres)
            // No aplica Mexico
            $cuerpo .= Separador::left('', 6);
            //Identificador del asesor técnico en la distribuidora  (# 381) (10 caracteres)
            $cuerpo .= Separador::right($refacciones["ident_ase_tec"], 10);
            //Identificador del asesor técnico en  STARS (# 388) (9 caracteres)
            $cuerpo .= Separador::right($refacciones["idstars_ase_tec"], 9);
            //Partes agregadas (Fecha) (#564) (8 caracteres)
            $cuerpo .= $fecha_agregada->format('Ymd');
            //Partes  agregadas (Hora) (#542) (4 caracteres)
            $cuerpo .= $fecha_agregada->format('Hi');
            //Fin
            $cuerpo .= '>';
            // Salto de linea
            $cuerpo .= PHP_EOL;

            $this->renglones++;
        }

        return $cuerpo;
    }

    /**
     * Datos de Facturacion de la Orden
     *
     * @return string
     */
    public function getDatosFacturacion($numero_orden) : string
    {
        // $numero_orden = $this->getNumeroOrden($numero_orden);

        $datos_factura = DatosOrdenFacturada::get([ 'numero_orden' => $numero_orden ]);

        $fecha_factura = new DateTime($datos_factura["fecha_venta"]);

        //Tipo de Registro (# 96) (2 caracteres)
        $refaccion = 'P1';
        //Numero de Factura (# 355) (16 caracteres)
        $refaccion .= Separador::right($datos_factura["numero_factura"], 16);
        //Fecha de Venta  en la factura (# 356) (8 caracteres)
        $refaccion .= Separador::left($fecha_factura->format('Ymd'), 8);
        //Apellido (#142) (80 caracteres)
        $refaccion .= Separador::right($datos_factura["apellido"], 80);
        //Dirección 1 (#149)  (80 caracteres)
        $refaccion .= Separador::right($datos_factura["direccion"]?? '', 80);
        //Dirección 2 (#150)  (80 caracteres)
        $refaccion .= Separador::right($datos_factura["direccion2"]?? '', 80);
        //Ciudad (#151) (30 caracteres)
        $refaccion .= Separador::right($datos_factura["ciudad"]?? '', 30);
        //Estado (# 152) (30 caracteres)
        $refaccion .= Separador::right($datos_factura["estado"]?? '', 30);
        //Código Postal(#153) (20 caracteres)
        $refaccion .= Separador::right($datos_factura["codigo_postal"]?? '', 20);
        //Tipo de cliente de la parte (# 367) (1 caracter)
        // D = Distribuidor, F = Flotillas, V = Gobierno, I = Interno, R = Menudeo U = Desconocido
        $refaccion .= Separador::left($datos_factura["tipo_cliente"]?? 'U', 1);
        //Clave de identificación del cliente (# 369) (15 caracteres)
        $refaccion .= Separador::right($datos_factura["clave_cliente"]?? 'XAXX-010101-000', 15);
        //Impuestos (#361) (14 caracteres)
        $refaccion .= Separador::numeric($datos_factura["impuestos"], 14);
        // Seguro Social del vendedor (#131) 
        // No aplica para México
        $refaccion .= Separador::left('', 6);
        //Identificador del vendedor en la distribuidora  (# 381) (10 caracteres)
        $refaccion .= Separador::right($datos_factura["identificador_vendedor_distribuidora"], 10);
        //Identificador del vendedor en STARS  (# 388) (9 caracteres)
        $refaccion .= Separador::right($datos_factura["identificador_vendedor_stars"], 9);
        //Nombre (#137) (30 caracteres)
        $refaccion .= Separador::right($datos_factura["nombre"], 30);
        //Espacio Segundo Nombre del cliente (# 42) (1 caracter)
        $refaccion .= Separador::right($datos_factura["inicial_segundo_nombre"], 1);
        //Código de País del cliente (#401) (3 caracteres)
        $refaccion .= Separador::right('MEX', 3);
        //Teléfono Principal (#154) (20 caracteres)
        $refaccion .= Separador::numeric($datos_factura["telefono_principal"]?? '', 20, 0);
        //Teléfono Secundario (#155) (20 caracteres)
        $refaccion .= Separador::numeric($datos_factura["telefono_secundario"]?? '', 20, 0);
        //Teléfono adicional (#400) (20 caracteres)
        $refaccion .= Separador::numeric($datos_factura["telefono_adicional"]?? '', 20, 0);
        //Correo electrónico (#391) (100 caracteres)
        $refaccion .= Separador::right(empty($datos_factura["correo_electronico"])? 'notienecorreo@notienecorreo.com.mx' : $datos_factura["correo_electronico"], 100);
        //Monto de descuento (#398) (14 caracteres)
        $refaccion .= Separador::numeric($datos_factura["monto_descuento"], 14);
        //Monto de traslado (#399) (14 caracteres)
        $refaccion .= Separador::numeric($datos_factura["monto_traslado"], 14);
        //Tipo de Venta(#387) (2 caracteres)
        // F = Flotilla, R= Retail, C = Seminuevo
        $refaccion .= Separador::right($datos_factura["tipo_venta"], 2);
        //Descripción de tipo de Venta (#395) (15 caracteres)
        $refaccion .= Separador::right($datos_factura["descripcion_tipo_venta"], 15);
        //Dirección 3 (#500) (80 caracteres)
        $refaccion .= Separador::right($datos_factura["direccion3"] ?? '', 80);
        //Descripción compañia (30 caracteres)
        $refaccion .= Separador::right($datos_factura["descripcion_compania"], 15);
        //Correo electrónico de Empresa (100 caracteres)
        $refaccion .= Separador::right(empty($datos_factura["correo_electronico_empresa"])? 'notienecorreo@notienecorreo.com.mx' : $datos_factura["correo_electronico_empresa"], 100);
        // Correo Electrónico 2 (100 caracteres)
        $refaccion .= Separador::right(empty($datos_factura["correo_electronico2"])? 'notienecorreo@notienecorreo.com.mx' : $datos_factura["correo_electronico2"], 100);
        // Correo Electrónico 3 (100 caracteres)
        $refaccion .= Separador::right(empty($datos_factura["correo_electronico3"])? 'notienecorreo@notienecorreo.com.mx' : $datos_factura["correo_electronico3"], 100);
        //Número de miembro para programa de lealtad (20 caracteres)
        // No aplica Mexico 
        $refaccion .= Separador::right('', 20);
        //Número de cliente (10 caracteres)
        // No aplica Mexico
        $refaccion .= Separador::right('', 10);
        //Permiso Dealer para uso de correo electrónico del cliente 
        $refaccion .= 'Y';
        //Permiso Ford de México para uso de correo electrónico del cliente 
        $refaccion .= 'Y';
        //Permiso Dealer para uso de correo electrónico del cliente 1 
        $refaccion .= 'N';
        //Permiso Ford de México para uso de correo electrónico del cliente 1 
        $refaccion .= 'N';
        //Permiso Dealer para uso de correo electrónico del cliente 2
        $refaccion .= 'N';
        //Permiso Ford de México para uso de correo electrónico del cliente 2
        $refaccion .= 'N';
        //Permiso Dealer para uso de correo electrónico del cliente 3
        $refaccion .= 'N';
        //Permiso Ford de México para uso de correo electrónico del cliente 3
        $refaccion .= 'N';
        //Permiso Dealer para uso de correo electrónico de la empresa
        $refaccion .= 'N';
        //Permiso Ford de México para uso de correo electrónico de la empresa
        $refaccion .= 'N';
        //Permiso Dealer para uso del telefono principal del cliente
        $refaccion .= 'N';
        //Permiso Ford de México para uso del telefono principal del cliente
        $refaccion .= 'N';
        //Permiso Dealer para uso del telefono celular del cliente
        $refaccion .= 'N';
        //Permiso Ford de México para uso del telefono celular del cliente
        $refaccion .= 'N';
        //Permiso Dealer para uso del telefono secundario del cliente
        $refaccion .= 'N';
        //Permiso Ford de México para uso del telefono secundario del cliente
        $refaccion .= 'N';
        //Permiso Dealer para uso de mensajes SMS del cliente
        $refaccion .= 'N';
        //Permiso Ford de México para uso de mensajes SMS del cliente
        $refaccion .= 'N';
        //Permiso Dealer para uso de mensajes multimedia del cliente
        $refaccion .= 'N';
        //Permiso Ford de México para uso de mensajes multimedia del cliente
        $refaccion .= 'N';
        $refaccion .= '>';
        $refaccion .= PHP_EOL;

        $this->renglones++;

        return $refaccion;
    }

    /**
     * Obtener los Datos de la Facturacion de las Refacciones
     *
     * @return string
     */
    public function getDatosFacturacionRefacciones($numero_orden) : string
    {
        $numero_orden = $this->getNumeroOrden($numero_orden);

        $refacciones_facturadas = RefaccionesByOrdenFacturada::get([ 'numero_orden' => $numero_orden ]);
        $refaccion = '';

        foreach($refacciones_facturadas as $refaccion_facturada)
        {
            //Tipo de Registro (# 96) (2 caracteres)
            $refaccion .= '03';
            //Prefijo del número de parte (# 186) (6 caracteres)
            $refaccion .= Separador::left($refaccion_facturada["prefijo"], 6);
            //Básico del número de Parte (# 187) (8 caracteres)
            $refaccion .= Separador::left($refaccion_facturada["basico"], 8);
            //Sufijo del número de parte (# 188) (8 caracteres)
            $refaccion .= Separador::left($refaccion_facturada["sufijo"], 8);
            //Cantidad de Partes (#338) (14 caracteres)
            $refaccion .= Separador::numeric($refaccion_facturada["cantidad_parte"] * -1, 7, 0);
            //Precio de venta total de la parte (# 352) (14 caracteres)
            // Cantidad * Precio unitario
            $refaccion .= Separador::numeric($refaccion_facturada["precio_parte"], 14);
            //Costo de la parte (# 353)  (14 caracteres)
            // Nota: Costo Neto del Distribuidor. 
            // Costo Unitario de la parte Costo incluyendo Core, si aplica. 
            // Cargos/ Créditos  por Core  Incluidos (182 cargo por core) 
            $refaccion .= Separador::numeric($refaccion_facturada["costo_parte"], 14);
            //Descripción de la parte (# 374) (50 caracteres)
            $refaccion .= Separador::right($refaccion_facturada["descripcion_parte"], 50);
            //Fin del archivo
            $refaccion .= '>';
            $refaccion .= PHP_EOL;

            $this->renglones++;
        }

        return $refaccion;
    }

    /**
     * Retorna el Pie de todo el documento
     *
     * @return string
     */
    public function getPieDocumento(): string
    {
        $this->renglones++;
        
        // Fin de Archivo (# 96) (5 caracteres)
        $footer = '$EOF-';
        // Etiqueta 1 (4 caracteres)
        $footer .= '401G';
        //No de BID del distribuidor (# 90) (5 caracteres)
        $footer .= self::BIN_SUCURSAL;
        //Sub-número de Distribuidor (#120) 
        // No aplica para México (enviar default 5 "espacios")
        $footer .= Separador::left('', 5);
        //Código de área geográfica (# 121) (3 caracteres)
        $footer .= 'MEX';
        //Fecha de transmisión (# 29) "MMDDYY" (6 caracteres)
        $footer .= $this->fecha_transmision->format('mdy');
        //Número de Archivo (# 30) (2 caracteres)
        $footer .= Separador::numeric($this->consecutivo, 2, 0);
        //Cuenta de Registros (# 53) (8 caracteres)
        $footer .= Separador::numeric($this->renglones, 8, 0);
        //Tipo de Archivo (#397)
		// H: Para archivos históricos 
        // D: Para indicar que es un archivo normal con frecuencia diaria
        $footer .= 'D';
        // Cierre de linea
        $footer .= '>';

        return $footer;
    }

    protected function generateRandomString($length = 8)
    {
        $str = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $code = '';

        for($i=0; $i <= $length; $i++ )
        {
            $code .= substr($str, rand( 0, strlen($str) - 1 ) ,1 );
        }

        return $code;
    }

    protected function getNumeroOrden($numero_orden)
    {
        if(! ctype_digit($numero_orden))
        {
            return substr($numero_orden, 0, -1);
        }

        return $numero_orden;
    }
}