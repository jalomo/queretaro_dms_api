<?php

namespace App\Servicios\Ereact\Services\Interfaces;

interface Post
{
    public static function post(array $filters) : array;
}