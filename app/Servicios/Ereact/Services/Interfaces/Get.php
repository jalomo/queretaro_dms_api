<?php

namespace App\Servicios\Ereact\Services\Interfaces;

interface Get
{
    public static function get(array $filters): array;
}