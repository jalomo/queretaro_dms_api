<?php

namespace App\Servicios\Ereact\Services\Interfaces;

interface WebService
{
    public function getPathWebservice(): string;

    public function call(array $filters): array;
}