<?php

namespace App\Servicios\Ereact\Services\Traits;

trait PostTrait
{
    public static function get(array $filters): array
    {
        return (new static)->call($filters);
    }
}