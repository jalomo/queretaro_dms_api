<?php

namespace App\Servicios\Ereact\Services\Traits;

trait GetTrait
{
    public static function get(array $filters): array
    {
        return (new static)->call($filters);
    }
}