<?php

namespace App\Servicios\Ereact\Services;

class Separador
{
    const LEFT = 0;
    const RIGHT = 1;
    const BOTH = 2;

    public function padding(?string $string, int $length = 0, string $pad_string = ' ', $type = Separador::LEFT)
    {
        if(mb_strlen($string) > $length)
        {
            return $string = mb_substr($string, 0, $length);
        }

        return $this->mb_str_pad($string, $length, $pad_string, $type);
    }
    public static function right(?string $string, int $length, string $pad_string = ' ')
    {
        return (new static)->padding($string, $length, $pad_string, Separador::RIGHT);
    }

    public static function left(?string $string, int $length, string $pad_string = ' ')
    {
        return (new static)->padding($string, $length, $pad_string, Separador::LEFT);
    }

    public static function numeric(?string $number, int $length, int $decimal = 2)
    {
        $number = (float) $number;
        
        return self::left(number_format($number, $decimal, '', ''), $length, '0');
    }

    public function str_pad_unicode($str, $pad_len, $pad_str = ' ', $dir = STR_PAD_RIGHT) 
    {
        $str_len = mb_strlen($str);
        $pad_str_len = mb_strlen($pad_str);
        if (!$str_len && $dir == STR_PAD_LEFT) {
            $str_len = 1; // @debug
        }

        if (!$str_len && $dir == STR_PAD_RIGHT) {
            $str_len = 0; // @debug
        }
        
        if (!$pad_len || !$pad_str_len || $pad_len <= $str_len) {
            return $str;
        }
    
        $result = null;
        if ($dir == STR_PAD_BOTH) {
            $length = ($pad_len - $str_len) / 2;
            $repeat = ceil($length / $pad_str_len);
            $result = mb_substr(str_repeat($pad_str, $repeat), 0, floor($length))
                    . $str
                    . mb_substr(str_repeat($pad_str, $repeat), 0, ceil($length));
        } else {
            $repeat = ceil($str_len - $pad_str_len + $pad_len);
            if ($dir == STR_PAD_RIGHT) {
                $result = $str . str_repeat($pad_str, $repeat);
                $result = mb_substr($result, 0, $pad_len);
            } else if ($dir == STR_PAD_LEFT) {
                $result = str_repeat($pad_str, $repeat);
                $result = mb_substr($result, 0, 
                            $pad_len - (($str_len - $pad_str_len) + $pad_str_len))
                        . $str;
            }
        }
        
        return $result;
    }

    /**
     * mb_str_pad
     *
     * @param string $input
     * @param int $pad_length
     * @param string $pad_string
     * @param int $pad_type
     * @return string
     * @author Kari "Haprog" Sderholm
     */
    public function mb_str_pad( $input, $pad_length, $pad_string = ' ', $pad_type = STR_PAD_RIGHT)
    {
        $diff = strlen( $input ) - mb_strlen( $input );
        return str_pad( $input, $pad_length + $diff, $pad_string, $pad_type );
    }
}