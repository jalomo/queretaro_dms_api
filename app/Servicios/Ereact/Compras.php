<?php

namespace App\Servicios\Ereact;

use App\Http\Resources\Ereact\CompraResource;
use App\Models\Refacciones\PedidosProductos;
use App\Servicios\Ereact\LayoutInterface;
use App\Servicios\Ereact\StringPadTrait;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Carbon\Carbon;

class Compras implements LayoutInterface
{
    use StringPadTrait;

    protected $compras;
    protected $fecha;
    protected $fecha_larga;
    protected $renglones;

    public function header(): string
    {
        return "HDRDEALERPU{$this->renglones}VIPPU{$this->fecha}" . PHP_EOL . "IREMEX137       $this->fecha_larga" . PHP_EOL;
    }

    public function footer(): string
    {
        return "TRLDEALERPU{$this->renglones}";
    }

    public function body(Model $compra): string
    {
        $linea = '';

        $linea .= 'DRE';
        $linea .= $this->str_pad_unicode($compra->producto->no_identificacion, 75, ' ', STR_PAD_RIGHT);
        $linea .= $this->str_pad_unicode($compra->producto->prefijo, 25, ' ', STR_PAD_RIGHT);
        $linea .= $this->str_pad_unicode($compra->producto->basico, 25, ' ', STR_PAD_RIGHT);
        $linea .= $this->str_pad_unicode($compra->producto->sufijo, 25, ' ', STR_PAD_RIGHT);
        $linea .= $this->str_pad_unicode(mb_substr($compra->producto->descripcion, 0, 40), 40, ' ', STR_PAD_RIGHT);

        $linea .= $this->str_pad_unicode($compra->proveedor_clave, 10, ' ', STR_PAD_RIGHT);
        $linea .= $this->str_pad_unicode(mb_substr($compra->proveedor, 0, 100), 100, ' ', STR_PAD_RIGHT);
        $linea .= $this->str_pad_unicode($compra->rfc?? 'AAAA999999XXX', 15, ' ', STR_PAD_RIGHT);
        $linea .= $this->str_pad_unicode($compra->cantidad, 9, '0', STR_PAD_LEFT);
        $linea .= $this->str_pad_unicode(number_format($compra->total, 2, '.', ''), 12, '0', STR_PAD_LEFT);
        $linea .= $this->str_pad_unicode($compra->created_at->format('Ymd'), 8, ' ', STR_PAD_LEFT);
        $linea .= $this->str_pad_unicode($compra->factura, 10, ' ', STR_PAD_LEFT);
        $linea .= $this->str_pad_unicode(number_format($compra->total - ( $compra->total / 1.16), 2, '.', ''), 12, '0', STR_PAD_LEFT);
        $linea .= $this->str_pad_unicode($compra->tipo_venta?? 'FO', 2, ' ', STR_PAD_LEFT);

        $linea .= PHP_EOL;

        return $linea; 
    }

    public function getFilename(): string
    {
        return "vip_pur_mex_137_$this->fecha.txt";
    }

    public function json(Request $request)
    {
        return response()->json(
            CompraResource::collection(
                $this->query($request)
            )
        );
    }

    public function download(Request $request)
    {
        $this->compras = $this->query($request);

        $this->fecha = Carbon::now()->format('ymdHis');
        $this->fecha_larga = Carbon::now()->format('YmdHis');

        $this->renglones = str_pad($this->compras->count(), 6, "0", STR_PAD_LEFT);
        $filename = storage_path("app/{$this->getFilename()}.txt");

        $file = fopen($filename, 'a');
        fwrite($file, $this->header());

        foreach($this->compras as &$compra)
        {
            fwrite($file, $this->body($compra));
        }

        fwrite($file, $this->footer());
        fclose($file);

        return response()->download($filename, $this->getFilename(), ['Content-Type' => 'text/plain'])->deleteFileAfterSend();
    }

    public function query(Request $request): Collection
    {
        $compras = PedidosProductos::with('producto')
            ->join('pedidos', 'pedidos_productos.pedido_id', 'pedidos.id')
            ->whereHas('pedido', function($q) use($request){
                $q->when($request->fecha_inicio, function($q) use ($request){
                    $q->whereDate('created_at', '>=', $request->fecha_inicio);
                });
                $q->when($request->fecha_termino, function($q) use ($request){
                    $q->whereDate('created_at', '<=', $request->fecha_termino);
                });
                $q->where('entrada', true);
            })
            ->groupBy('producto_id', 'pedido_id', 'pedidos_productos.factura')
            ->select([
                DB::raw("SUM(total) as total"),
                DB::raw("SUM(cantidad_entregada) as cantidad"),
                'producto_id',
                'pedido_id',
                'pedidos_productos.factura',
                DB::raw('MAX(proveedor) as proveedor'),
                DB::raw('MAX(pedidos.created_at) as created_at')
            ])
            ->get();

        return $compras;
    }
}