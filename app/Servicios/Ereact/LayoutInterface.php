<?php

namespace App\Servicios\Ereact;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

interface LayoutInterface
{
    /**
     * Devuelve el string para el header
     *
     * @return string
     */
    public function header() : string;

    /**
     * Devuelve el string para el body
     *
     * @return string
     */
    public function body(Model $query) : string;

    /**
     * Devuelve el string para el footer
     *
     * @return string
     */
    public function footer() : string;

    /**
     * Devuelve el nombre del archivo de descarga
     *
     * @return string
     */
    public function getFilename() : string;

    /**
     * Crea el archivo
     *
     * @return void
     */
    public function json(Request $request);

    /**
     * Realiza la consulta y devuelve una query
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function query(Request $request) : Collection;

    /**
     * Devuelve el archivo para la descarga
     *
     * @return void
     */
    public function download(Request $request);
}