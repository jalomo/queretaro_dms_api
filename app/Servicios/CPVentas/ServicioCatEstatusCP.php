<?php
namespace App\Servicios\CPVentas;

use App\Models\CPVentas\CatStatusCPModel;
use App\Servicios\Core\ServicioDB;

class ServicioCatEstatusCP extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catálogo estatus cp ventas';
        $this->modelo = new CatStatusCPModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatStatusCPModel:: ESTATUS => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatStatusCPModel:: ESTATUS => 'required'
        ];
    }
}
