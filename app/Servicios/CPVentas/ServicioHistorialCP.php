<?php

namespace App\Servicios\CPVentas;

use App\Models\CPVentas\CatStatusCPModel;
use App\Models\CPVentas\CPHistorialModel;
use App\Models\CPVentas\CPVentasModel;
use App\Models\Usuarios\User;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\ServicioDB;
use Illuminate\Http\Request;
class ServicioHistorialCP extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catálogo historial cp';
        $this->servicioCP = new ServicioCPVentas();
        $this->modelo = new CPHistorialModel();
        $this->modeloProactivo = new CPVentasModel();
    }

    public function getReglasGuardar()
    {
        return [
            CPHistorialModel::ESTATUS_ID => 'required',
            CPHistorialModel::USER_ID => 'required',
            CPHistorialModel::CP_ID => 'required',
            CPHistorialModel::CP_ID => 'required',
            CPHistorialModel::FECHA => 'required',
            CPHistorialModel::HORA => 'required',
            CPHistorialModel::OBSERVACIONES => 'nullable'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CPHistorialModel::ESTATUS_ID => 'required',
            CPHistorialModel::USER_ID => 'required',
            CPHistorialModel::CP_ID => 'required',
            CPHistorialModel::FECHA => 'required',
            CPHistorialModel::HORA => 'required',
            CPHistorialModel::OBSERVACIONES => 'nullable'
        ];
    }
    public function store(Request $request)
    {
        ParametrosHttpValidador::validar($request, $this->getReglasGuardar());
        //Buscar intentos 
        $proactivo = $this->modeloProactivo->where(CPVentasModel::ID,$request->get(CPHistorialModel::CP_ID))->select(CPVentasModel::INTENTOS)->get();
        $intentos = (int)$proactivo[0]->intentos + 1;
        $historial = $this->crear([
            CPHistorialModel::OBSERVACIONES => $request->get(CPHistorialModel::OBSERVACIONES),
            CPHistorialModel::ESTATUS_ID => $request->get(CPHistorialModel::ESTATUS_ID),
            CPHistorialModel::USER_ID => $request->get(CPHistorialModel::USER_ID),
            CPHistorialModel::FECHA => $request->get(CPHistorialModel::FECHA),
            CPHistorialModel::HORA => $request->get(CPHistorialModel::HORA),
            CPHistorialModel::CP_ID => $request->get(CPHistorialModel::CP_ID)
        ]);
        //Acutalizar estatus
        $this->servicioCP->massUpdateWhereId(CPVentasModel::ID,  $request->get(CPHistorialModel::CP_ID), [
            CPVentasModel::ESTATUS_ID => $request->get(CPHistorialModel::ESTATUS_ID),
            CPVentasModel::INTENTOS => $intentos
        ]);
        return [
            'historial' => $historial,
            'intentos' => $intentos
        ];
    }
    public function getAllHistorial($parameters)
    {
        $query = $this->modelo
            ->select(
                CPHistorialModel::getTableName() . '.' . CPHistorialModel::ID,
                CPHistorialModel::getTableName() . '.' . CPHistorialModel::OBSERVACIONES,
                CPHistorialModel::getTableName() . '.' . CPHistorialModel::CREATED_AT,
                CPHistorialModel::getTableName() . '.' . CPHistorialModel::FECHA,
                CPHistorialModel::getTableName() . '.' . CPHistorialModel::HORA,
                CatStatusCPModel::getTableName() . '.' . CatStatusCPModel::ESTATUS,
                CPVentasModel::getTableName() . '.' . CPVentasModel::ID . ' AS cp_id',
                CPVentasModel::getTableName() . '.' . CPVentasModel::VIN,
                CPVentasModel::getTableName() . '.' . CPVentasModel::NO_CUENTA,
                CPVentasModel::getTableName() . '.' . CPVentasModel::NOMBRE,
                CPVentasModel::getTableName() . '.' . CPVentasModel::APELLIDO,
                CPVentasModel::getTableName() . '.' . CPVentasModel::APELLIDO,
                CPVentasModel::getTableName() . '.' . CPVentasModel::TELEFONO_OFICINA,
                CPVentasModel::getTableName() . '.' . CPVentasModel::TELEFONO_CASA,
                CPVentasModel::getTableName() . '.' . CPVentasModel::EMAIL,
                CPVentasModel::getTableName() . '.' . CPVentasModel::INTENTOS,
                CPVentasModel::getTableName() . '.' . CPVentasModel::MARCA,
                CPVentasModel::getTableName() . '.' . CPVentasModel::MODELO,
                CPVentasModel::getTableName() . '.' . CPVentasModel::LINEA,
                CPVentasModel::getTableName() . '.' . CPVentasModel::TIPO_VEHICULO,
                CPVentasModel::getTableName() . '.' . CPVentasModel::ANIO,
                User::getTableName() . '.' . User::NOMBRE . ' AS nombre_usuario',
                User::getTableName() . '.' . User::APELLIDO_PATERNO . ' AS ap_usuario',
                User::getTableName() . '.' . User::APELLIDO_MATERNO . ' AS am_usuario'
            )
            ->join(
                User::getTableName(),
                User::getTableName() . '.' . User::ID,
                '=',
                CPHistorialModel::getTableName() . '.' . CPHistorialModel::USER_ID
            )
            ->join(
                CPVentasModel::getTableName(),
                CPVentasModel::getTableName() . '.' . CPVentasModel::ID,
                '=',
                CPHistorialModel::getTableName() . '.' . CPHistorialModel::CP_ID
            )
            ->leftJoin(
                CatStatusCPModel::getTableName(),
                CatStatusCPModel::getTableName() . '.' . CatStatusCPModel::ID,
                '=',
                CPHistorialModel::getTableName() . '.' . CPHistorialModel::ESTATUS_ID
            )
            ->orderBy(CPHistorialModel::getTableName() . '.' . CPHistorialModel::CP_ID)
            ->orderBy(CPHistorialModel::getTableName() . '.' . CPHistorialModel::CREATED_AT,'desc')
            ;
        if (isset($parameters[CPVentasModel::VIN])) {
            $query->where(
                CPVentasModel::VIN,
                $parameters[CPVentasModel::VIN]
            );
        }
        if (isset($parameters[CPHistorialModel::CP_ID])) {
            $query->where(
                CPHistorialModel::CP_ID,
                $parameters[CPHistorialModel::CP_ID]
            );
        }
        if (isset($parameters[CPHistorialModel::USER_ID])) {
            $query->where(
                CPHistorialModel::USER_ID,
                $parameters[CPHistorialModel::USER_ID]
            );
        }
        if (isset($parameters[CPHistorialModel::ESTATUS_ID])) {
            $query->where(
                CPHistorialModel::getTableName().'.'.CPHistorialModel::ESTATUS_ID,
                $parameters[CPHistorialModel::ESTATUS_ID]
            );
        }
        if (isset($parameters['fecha_inicio'])&&isset($parameters['fecha_fin'])) {
            $query->where(
                CPHistorialModel::getTableName().'.'.CPHistorialModel::CREATED_AT,'>=',
                $parameters['fecha_inicio']
            );
            $query->where(
                CPHistorialModel::getTableName().'.'.CPHistorialModel::CREATED_AT,'<=',
                $parameters['fecha_fin']
            );
        }
        return $query->get();
    }
}
