<?php

namespace App\Servicios\Logs\Pilot;

use App\Models\Logs\Pilot\LogsPilotModel;
use App\Servicios\Core\ServicioDB;

class ServicioLogsPilot extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'logs_pilot';
        $this->modelo = new LogsPilotModel();
    }
}
