<?php
namespace App\Servicios\Core;

trait MensajesConstantes
{
    public static $E0001_FILL_USERNAME = "E0001_FILL_USERNAME";
    public static $E0002_FILL_PASSWORD = "E0002_FILL_PASSWORD";
    public static $E0003_WRONG_CREDENTIALS = "E0003 Credenciales no validas";
    public static $E0005_VALUE_TOO_LONG = "E0005_VALUE_TOO_LONG";
    public static $E0006_VALUE_TOO_SHORT = "E0006_VALUE_TOO_SHORT";
    public static $E0007_VALUE_ALPHA_NUMERIC = "E0007_VALUE_ALPHA_NUMERIC";
    public static $E0008_VALUE_ALPHA_NUMERIC_SPECIAL_CHAR = "E0008_VALUE_ALPHA_NUMERIC_SPECIAL_CHAR";
    public static $E0009_USER_NOT_FOUND = "E0009_USER_NOT_FOUND";
    public static $E0010_USER_BLOCKED = "E0010_USER_BLOCKED";
    public static $E0011_USER_ALREADY_LOGGED_IN = "E0011_USER_ALREADY_LOGGED_IN";
    public static $E0012_INVALID_CHARACTERS = "E0012_INVALID_CHARACTERS";
    public static $E0013_ERROR_REGISTERING_RESOURCE = "E0013_ERROR_REGISTERING_RESOURCE";
    public static $E0014_ERROR_UPDATING_RESOURCE = "E0014_ERROR_UPDATING_RESOURCE";
    public static $E0015_NOT_MATCHING_PARAMETERS = "E0015_NOT_MATCHING_PARAMETERS";
    public static $E0016_NOT_STATUS_CUENTA = "No sé puede cancelar la cuenta, ya se realizó el corte de caja ó ya esta cancelada";
    
    public static $E0017_ERROR_REGISTERING_DATA = "E0017_ERROR_REGISTERING_DATA";
    public static $E0019_UNKNOWN_ERROR = "E0019_UNKNOWN_ERROR";
    public static $E0020_ERROR_GETTING_RESOURCE = "E0020 Error obteniendo recurso";
    public static $E0021_ERROR_DELETING_RESOURCE = "E0021_ERROR_DELETING_RESOURCE";
    public static $E0025_ERROR_SAVING_FILE = "E0025_ERROR_SAVING_FILE";
    public static $E0029_ERROR_CREATING_RECORD = "E0029_ERROR_CREATING_RECORD";
    public static $E0031_ERROR_UNEXPECTED_PARAMETER = "E0031 El parametro :parametro no se esperaba.";

    public static $A0001_USER_EXISTS = "A0001_USER_EXISTS";
    public static $A0002_BLOCKED_USER = "A0002_BLOCKED_USER";
    public static $A0003_INACTIVE_USER = "A0003_INACTIVE_USER";

    public static $I0001_LOGOUT_SUCCESSFUL = "I0001_LOGOUT_SUCCESSFUL";
    public static $I0002_LOGIN_SUCCESSFUL = "I0002_LOGIN_SUCCESSFUL";
    public static $I0003_RESOURCE_REGISTERED = "I0003 :parametro registrado correctamente.";
    public static $I0004_RESOURCE_UPDATED = "I0004 :parametro actualizado correctamente.";
    public static $I0005_NO_RESULTS = "I0005 Sin resultados";
    public static $I0006_REQUIRED_PARAMETERS = "I0006 Parametros requeridos.";
    public static $I0007_INSUFICIENTES_PRODUCTOS_ALMACEN = "I0007 No hay suficientes productos en almacen para ese producto.";
    public static $I0008_NO_EXISTE_PRODUCTO = "I0008 No existe producto en inventario.";
    public static $I0009_ORDEN_COMPRA_TIENE_VENTAS = "I0009 Orden de compra tiene ventas.";
    public static $I0010_FACTURA_REGISTRADA = "I0010 La factura ya se registro previamente";
    public static $I0011_FACTURA_SUBIDA = "I0011 La factura se subió correctamente";
    public static $I0012_EVIDENCIA_TICKET_SUBIDA = "I0012 La evidencia del ticket se subió correctamente";
    public static $I0013_TICKET_ACTUALIZADO = "I0013 ticket actualizado correctamente";
    public static $I0014_FACTURA_APLICADA = "I0014 factura aplicada correctamente";
    public static $I0015_REMISION_ACTUALIZADA = "I0015 remisión actualizada correctamente";

    public static $E0016_MISSING_PARAMS = "E0016 Falta el parametro :parametro";
    public static $PDF_ERROR = "Ocurrio un error al generar el PDF";
    public static $CUENTA_CANCELADA = "Cuenta cancelada correctamente.";
}
