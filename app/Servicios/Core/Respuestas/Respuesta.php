<?php

namespace App\Servicios\Core\Respuestas;

use Illuminate\Http\Response;
use App\Exceptions\ParametroHttpInvalidoException;
use App\Servicios\Core\MensajesConstantes;


class Respuesta
{
    use MensajesConstantes;

    public static function json($datos = [], $estatus = 200, $mensaje = '')
    {
        if ($estatus === 0) {
            $estatus = 500;
        }

        if ($estatus === 200) {
            if (empty($datos)) {
                return Response()->json('', 204, [
                    "X-Message" => str_replace("\n", " ", json_encode(__(self::$I0005_NO_RESULTS)))
                ]);
            } else {
                return response()->json($datos, $estatus, [
                    "X-Message" => str_replace("\n", " ", $mensaje)
                ]);
            }
        } else {
            return response()->json($datos, $estatus, [
                "X-Message" => str_replace("\n", " ", $mensaje)
            ]);
        }
    }

    public static function noContent($estatus = 204, $mensaje = [])
    {
        return self::json('', $estatus, json_encode($mensaje));
    }

    public static function error_back($exception)
    {

        $mensaje = $exception instanceof ParametroHttpInvalidoException ? $exception->getErrors() : $exception->getMessage();

        if (empty($mensaje) || $exception->getCode() == 0)
            $mensaje = __(self::$E0019_UNKNOWN_ERROR);

        return self::noContent($exception->getCode(), $mensaje);
    }

    public static function error($exception)
    {
        $message = ($exception instanceof ParametroHttpInvalidoException)?
            $exception->getErrors() : $exception->getMessage();

        $code = $exception->getCode() > 0 && $exception->getCode() < 600? $exception->getCode() : 400;

        return self::noContent($code, $message);
    }
}
