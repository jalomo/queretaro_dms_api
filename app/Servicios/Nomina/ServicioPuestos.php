<?php

namespace App\Servicios\Nomina;

use App\Servicios\Core\ServicioDB;
use App\Models\Nomina\CaPuestosModel;

class ServicioPuestos extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'Puestos';
        $this->modelo = new CaPuestosModel();
    }

    public function getReglasGuardar()
    {
        return [
            CaPuestosModel::Descripcion => 'required',
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CaPuestosModel::Descripcion => 'required',
        ];
    }
}
