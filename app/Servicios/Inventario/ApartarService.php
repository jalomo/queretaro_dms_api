<?php

namespace App\Servicios\Inventario;

use App\Models\Refacciones\TipoMovimiento;
use App\Servicios\Inventario\Interfaces\DecrementInventarioInterface;
use App\Servicios\Inventario\Traits\DecrementTrait;

class ApartarPiezaService implements DecrementInventarioInterface
{
    use DecrementTrait;

    public function movimiento() : int
    {
        return TipoMovimiento::APARTADO;
    }

    public function movimiento_proceso(): int
    {
        return TipoMovimiento::EN_PROCESO_APARTAR;
    }
}