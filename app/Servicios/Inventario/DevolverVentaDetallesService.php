<?php

namespace App\Servicios\Inventario;

use App\Models\Refacciones\TipoMovimiento;
use App\Servicios\Inventario\Interfaces\IncrementInventarioInterface;
use App\Servicios\Inventario\Traits\IncrementTrait;

class DevolverVentaDetallesService implements IncrementInventarioInterface
{
    use IncrementTrait;

    public function movimiento() : int
    {
        return TipoMovimiento::DEVOLUCION_VENTA_DETALLES;
    }
}