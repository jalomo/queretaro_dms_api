<?php

namespace App\Servicios\Inventario;

use App\Models\Refacciones\TipoMovimiento;
use App\Servicios\Inventario\Interfaces\NoIdentificacionInterface;
use App\Servicios\Inventario\Traits\NoIdentificacionDecrementTrait;

class TraspasarService implements NoIdentificacionInterface
{
    use NoIdentificacionDecrementTrait;

    public function movimiento() : int
    {
        return TipoMovimiento::TRASPASO;
    }

    public function movimiento_proceso(): int
    {
        return TipoMovimiento::EN_PROCESO_TRASPASO;
    }
}