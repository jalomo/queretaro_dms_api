<?php

namespace App\Servicios\Inventario;

use App\Models\Refacciones\TipoMovimiento;
use App\Servicios\Inventario\Interfaces\NoIdentificacionInterface;
use App\Servicios\Inventario\Traits\NoIdentificacionDecrementTrait;

class CancelarTraspasadoService implements  NoIdentificacionInterface
{
    use NoIdentificacionDecrementTrait;

    public function movimiento() : int
    {
        return TipoMovimiento::CANCELACION_TRASPASO;
    }
}