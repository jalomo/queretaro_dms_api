<?php

namespace App\Servicios\Inventario;

use App\Models\Refacciones\TipoMovimiento;
use App\Servicios\Inventario\Interfaces\DecrementInventarioInterface;
use App\Servicios\Inventario\Traits\DecrementTrait;

class VenderService implements DecrementInventarioInterface
{
    use DecrementTrait;

    public function movimiento() : int
    {
        return TipoMovimiento::VENTA_MOSTRADOR;
    }

    public function movimiento_proceso(): int
    {
        return TipoMovimiento::EN_PROCESO_VENTA_MOSTRADOR;
    }
}