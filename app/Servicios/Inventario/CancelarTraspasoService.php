<?php

namespace App\Servicios\Inventario;

use App\Models\Refacciones\TipoMovimiento;
use App\Servicios\Inventario\Interfaces\NoIdentificacionInterface;
use App\Servicios\Inventario\Traits\NoIdentificacionIncrementTrait;

class CancelarTraspasoService implements NoIdentificacionInterface
{
    use NoIdentificacionIncrementTrait;

    public function movimiento() : int
    {
        return TipoMovimiento::CANCELACION_TRASPASO;
    }
}