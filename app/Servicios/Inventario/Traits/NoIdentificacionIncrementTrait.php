<?php

namespace App\Servicios\Inventario\Traits;

use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\InventarioProductos;
use App\Models\Refacciones\StockProductosModel;
use InvalidArgumentException;

Trait NoIdentificacionIncrementTrait
{
    /**
     * Incrementar Inventario
     *
     * @param string $no_identificacion
     * @param integer $cantidad
     * @return InventarioProductos
     */
    public function handle(string $no_identificacion, int $cantidad, float $precio = 0) : InventarioProductos
    {
        $producto = ProductosModel::where('no_identificacion', $no_identificacion)->first();
        
        if($producto == null)
            throw new InvalidArgumentException("El producto $no_identificacion no existe en el catalogo de productos.");
        
        $stock = StockProductosModel::where('producto_id', $producto->id)->first();
        $existe = $stock->cantidad_actual + $cantidad;
        $existia = $stock->cantidad_actual;

        $inventario = $producto->inventario()->create([
            'tipo_movimiento_id' => $this->movimiento(),
            'model_id' => $this->id?? null, // Si pasamos id de venta, traspaso, etc...
            'cantidad' => $cantidad,
            'existe' => $existe,
            'existia' => $existia,
            'precio' => $precio,
            'costo_promedio' => $producto->costo_promedio,
        ]);

        $stock->cantidad_actual = $existe;
        $stock->cantidad_almacen_primario = $existe;
        $producto->cantidad = $existe;

        $stock->save();
        $producto->save();

        return $inventario;
    }

    /**
     * Asigna el id para rastreo en el inventario
     * para identificar la venta, traspaso, devolucion, etc...
     *
     * @param integer $id
     * @return void
     */
    public function id(int $id)
    {
        $this->id = $id;
    }
}