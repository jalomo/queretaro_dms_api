<?php

namespace App\Servicios\Inventario\Traits;

use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\InventarioProductos;
use App\Models\Refacciones\StockProductosModel;
use InvalidArgumentException;

Trait NoIdentificacionDecrementTrait
{
    /**
     * Decrementar Inventario
     *
     * @param integer $no_identificacion
     * @param integer $cantidad
     * @return InventarioProductos
     */
    public function handle(string $no_identificacion, int $cantidad, float $precio = 0) : InventarioProductos
    {
        $producto = ProductosModel::where('no_identificacion', $no_identificacion)->first();
        $stock = StockProductosModel::where('producto_id', $producto->id)->first();
        
        if($producto == null)
            throw new InvalidArgumentException("El producto $no_identificacion no existe en el catalogo de productos.");
        
        if($stock->cantidad_actual < 1)
        {
            info(__("INVENTARIO INSUFICIENTE PARA :num_pieza EXISTENCIA: :cantidad_actual PROCESO: :cantidad_solicitada", [
                'num_pieza' => $producto->no_identificacion,
                'cantidad_actual' => $stock->cantidad_actual,
                'cantidad_solicitada' => $cantidad,
            ]));

            $inventario = $producto->inventario()->create([
                'tipo_movimiento_id' => $this->movimiento_proceso(),
                'model_id' => $this->id?? null,
                'cantidad' => $cantidad,
                'existe' => $stock->cantidad_actual,
                'existia' => $stock->cantidad_actual,
                'precio' => $precio,
                'costo_promedio' => $producto->costo_promedio,
            ]); 
        }
        elseif($stock->cantidad_actual < $cantidad)
        {
            info(__("INVENTARIO INSUFICIENTE PARA :num_pieza EXISTENCIA: :cantidad_actual PROCESO: :cantidad_solicitada", [
                'num_pieza' => $producto->no_identificacion,
                'cantidad_actual' => $stock->cantidad_actual,
                'cantidad_solicitada' => $cantidad - $stock->cantidad_actual,
            ]));

            // Creamos movimiento en proceso para posterior descuento
            $inventario = $producto->inventario()->create([
                'tipo_movimiento_id' => $this->movimiento_proceso(),
                'model_id' => $this->id?? null,
                'cantidad' => $cantidad - $stock->cantidad_actual,
                'existe' => $stock->cantidad_actual,
                'existia' => $stock->cantidad_actual,
                'precio' => $precio,
                'costo_promedio' => $producto->costo_promedio,
            ]); 

            // Creamos movimiento decremento de lo que existe en inventario
            $inventario = $producto->inventario()->create([
                'tipo_movimiento_id' => $this->movimiento(),
                'model_id' => $this->id?? null,
                'cantidad' => $stock->cantidad_actual,
                'existe' => 0,
                'existia' => $stock->cantidad_actual,
                'precio' => $precio,
                'costo_promedio' => $producto->costo_promedio,
            ]); 

            $stock->cantidad_actual = 0;
            $stock->cantidad_almacen_primario = 0;
            $producto->cantidad = 0;
    
            $stock->save();
            $producto->save();
        }
        else
        {
            $existe = $stock->cantidad_actual - $cantidad;
            $existia = $stock->cantidad_actual;
    
            $inventario = $producto->inventario()->create([
                'tipo_movimiento_id' => $this->movimiento(),
                'model_id' => $this->id?? null, // Si pasamos id de venta, traspaso, etc...
                'cantidad' => $cantidad,
                'existe' => $existe,
                'existia' => $existia,
                'precio' => $precio,
                'costo_promedio' => $producto->costo_promedio,
            ]);
    
            $stock->cantidad_actual = $existe;
            $stock->cantidad_almacen_primario = $existe;
            $producto->cantidad = $existe;
    
            $stock->save();
            $producto->save();
        }

        return $inventario;
    }

    /**
     * Asigna el id para rastreo en el inventario
     * para identificar la venta, traspaso, devolucion, etc...
     *
     * @param integer $id
     * @return void
     */
    public function id(int $id)
    {
        $this->id = $id;
    }
}