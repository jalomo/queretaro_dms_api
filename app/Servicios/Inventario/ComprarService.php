<?php

namespace App\Servicios\Inventario;

use App\Models\Refacciones\TipoMovimiento;
use App\Servicios\Inventario\Interfaces\IncrementInventarioInterface;
use App\Servicios\Inventario\Traits\IncrementTrait;

class ComprarService implements IncrementInventarioInterface
{
    use IncrementTrait;

    public function movimiento() : int
    {
        return TipoMovimiento::COMPRA;
    }
}