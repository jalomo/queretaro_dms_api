<?php 

namespace App\Servicios\Inventario\Interfaces;
use App\Models\Refacciones\InventarioProductos;

interface DecrementInventarioInterface
{
    public function id(int $model_id);

    public function handle(int $producto_id, int $cantidad, float $precio = 0) : InventarioProductos;

    public function movimiento() : int;

    public function movimiento_proceso() : int;
}