<?php 

namespace App\Servicios\Inventario\Interfaces;

use App\Models\Refacciones\InventarioProductos;

interface NoIdentificacionInterface
{
    public function id(int $model_id);

    public function handle(string $no_identificacion, int $cantidad, float $precio = 0) : InventarioProductos;

    public function movimiento() : int;
}