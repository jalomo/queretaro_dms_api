<?php 

namespace App\Servicios\Asientos;

interface ClienteInterface
{
    /**
     * Se envía el numero de cliente
     *
     * @return string
     */

    public function getCliente(): ?string;

    /**
     * Se envía el nombre de cliente
     *
     * @return string
     */
    public function getNombreCliente(): ?string;

    /**
     * Se envía el nombre de cliente
     *
     * @return string
     */
    
     public function getApellidoPaternoCliente(): ?string;
    /**
     * Se envía el nombre de cliente
     *
     * @return string
     */
    public function getApellidoMaternoCliente(): ?string;
}