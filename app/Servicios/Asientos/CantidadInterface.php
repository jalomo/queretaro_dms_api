<?php 

namespace App\Servicios\Asientos;

interface CantidadInterface
{
    /**
     * Se envía la cantidad
     *
     * @return string
     */

    public function getCantidad(): ?float;
}