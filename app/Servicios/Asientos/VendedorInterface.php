<?php 

namespace App\Servicios\Asientos;

interface VendedorInterface
{
    /**
     * Se envía el id o nombre del vendedor
     *
     * @return string
     */

    public function getVendedor(): ?string;
}