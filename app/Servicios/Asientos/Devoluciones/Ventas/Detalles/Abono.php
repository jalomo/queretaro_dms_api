<?php 

namespace App\Servicios\Asientos\Devoluciones\Ventas\Detalles;

use App\Servicios\Asientos\AgregarAsientoTrait;
use App\Servicios\Asientos\CantidadInterface;
use App\Servicios\Asientos\ContabilidadInterface;
use App\Servicios\Asientos\CostoPromedioInterface;
use App\Servicios\Asientos\VendedorInterface;
use Illuminate\Support\Facades\Log;

class Abono implements ContabilidadInterface, CantidadInterface, CostoPromedioInterface, VendedorInterface
{
    use AgregarAsientoTrait;

    protected $venta;
    protected $extra;

    public function __construct($venta, array $extra = [])
    {
        $this->venta = $venta;
        $this->extra = $extra;
    }

    public function getPoliza(): string
    {
        return 'DQ';
    }

    public function getDepartamento(): int
    {
        return 13;
    }

    public function getFolio(): string
    {
        return env('ALMACEN_PRIMARIO') == 'QUERETARO'? "QRT-VENTA-{$this->venta->numero_orden}" : "SNJ-VENTA-{$this->venta->numero_orden}";
    }

    public function getTipo(): int
    {
        return 1;
    }

    public function getCuenta(): int
    {
        return env('ALMACEN_PRIMARIO') == 'QUERETARO'? 151000 : 151000;
    }

    public function getConcepto(): string
    {
        if(isset($this->extra['num_pieza']))
        {
            return $this->extra['num_pieza'];
        }

        return env('ALMACEN_PRIMARIO') == 'QUERETARO'? 'Inventario en Proceso '. env('ALMACEN_PRIMARIO')  : 'Inventario en Proceso '. env('ALMACEN_SECUNDARIO');
    }

    public function getEstatus(): string
    {
        return 'APLICADO';
    }

    public function getCliente(): string
    {
        return ''; // No lo tenemos es cargamento
    }

    public function getSucursal(): int
    {
        return env('ALMACEN_PRIMARIO') == 'QUERETARO'? 1 : 2; // 1 Queretaro, 2 San Juan
    }

    public function getCantidad(): ?float
    {
        return $this->extra['cantidad']?? null;
    }

    public function getCostoPromedio(): ?float
    {
        return $this->extra['costo_promedio']?? 0;
    }

    public function getVendedor(): ?string
    {
        return $this->extra['vendedor']?? '';
    }

    public function failedMessage($message = 'La petición para agregar abono de la refaccion a los asientos a contabilidad no fue satisfactoria.'): string
    {
        return tap($message, function($message){
            Log::warning($message, $this->venta->toArray());
        });
    }
}