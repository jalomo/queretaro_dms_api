<?php 

namespace App\Servicios\Asientos;

interface FechaInterface
{
    /**
     * Se envía la fecha
     *
     * @return string
     */

    public function getFecha(): ?string;
}