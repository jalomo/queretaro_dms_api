<?php 

namespace App\Servicios\Asientos\Ajustes;

use App\Servicios\Asientos\AgregarAsientoTrait;
use App\Servicios\Asientos\ContabilidadInterface;
use Illuminate\Support\Facades\Log;

class Cargo implements ContabilidadInterface
{
    use AgregarAsientoTrait;

    protected $refaccion;

    public function __construct($refaccion)
    {
        $this->refaccion = $refaccion;
    }

    public function getPoliza(): string
    {
        return 'FE';
    }

    public function getDepartamento(): int
    {
        return 13;
    }

    public function getFolio(): string
    {
        return env('ALMACEN_PRIMARIO') == 'QUERETARO'? "QRT-AJUSTES-{$this->refaccion->auditoria->id}" : "SNJ-AJUSTES-{$this->refaccion->auditoria->id}";
    }

    public function getTipo(): int
    {
        return 2;
    }

    public function getCuenta(): int
    {
        return env('ALMACEN_PRIMARIO') == 'QUERETARO'? 150001 : 150001;
    }

    public function getConcepto(): string
    {
        return env('ALMACEN_PRIMARIO') == 'QUERETARO'? 'AJUSTES INVENTARIO '. env('ALMACEN_PRIMARIO')  : 'AJUSTES INVENTARIO '. env('ALMACEN_SECUNDARIO');
    }

    public function getEstatus(): string
    {
        return 'APLICADO';
    }

    public function getCliente(): string
    {
        return ''; // No lo tenemos es cargamento
    }

    public function getSucursal(): int
    {
        return env('ALMACEN_PRIMARIO') == 'QUERETARO'? 1 : 2; // 1 Queretaro, 2 San Juan
    }

    public function failedMessage($message = 'La petición para agregar cargo de la refaccion a los asientos a contabilidad no fue satisfactoria.'): string
    {
        return tap($message, function($message){
            Log::warning($message, $this->refaccion->toArray());
        });
    }
}