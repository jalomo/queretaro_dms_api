<?php 

namespace App\Servicios\Asientos\Ventas\Detalles;

use App\Servicios\Asientos\AgregarAsientoTrait;
use App\Servicios\Asientos\CantidadInterface;
use App\Servicios\Asientos\ClienteInterface;
use App\Servicios\Asientos\ContabilidadInterface;
use App\Servicios\Asientos\CostoPromedioInterface;
use App\Servicios\Asientos\FechaInterface;
use App\Servicios\Asientos\VendedorInterface;
use Illuminate\Support\Facades\Log;

class Cargo implements ContabilidadInterface, ClienteInterface, FechaInterface, CostoPromedioInterface, VendedorInterface, CantidadInterface
{
    use AgregarAsientoTrait;

    protected $venta;
    protected $extra;

    public function __construct($venta, array $extra = [])
    {
        $this->venta = $venta;
        $this->extra = $extra;
    }

    public function getPoliza(): string
    {
        return 'V0';
    }

    public function getDepartamento(): int
    {
        return 13;
    }

    public function getFolio(): string
    {
        return env('ALMACEN_PRIMARIO') == 'QUERETARO'? "QRT-VENTA-{$this->venta->numero_orden}" : "SNJ-VENTA-{$this->venta->numero_orden}";
    }

    public function getTipo(): int
    {
        return 2;
    }

    public function getCuenta(): int
    {
        return env('ALMACEN_PRIMARIO') == 'QUERETARO'? 151000 : 151000;
    }

    public function getConcepto(): string
    {
        if(isset($this->extra['num_pieza']))
        {
            return $this->extra['num_pieza'];
        }
        
        return env('ALMACEN_PRIMARIO') == 'QUERETARO'? 'Inventario en Proceso '. env('ALMACEN_PRIMARIO')  : 'Inventario en Proceso '. env('ALMACEN_SECUNDARIO');
    }

    public function getEstatus(): string
    {
        return 'APLICADO';
    }

    public function getCliente(): string
    {
        return $this->extra['id']?? ''; 
    }

    public function getNombreCliente(): string
    {
        return $this->extra['cliente_nombre'] ?? ''; 
    }

    public function getApellidoPaternoCliente(): string
    {
        return $this->extra['cliente_apellido_paterno'] ?? ''; 
    }

    public function getApellidoMaternoCliente(): string
    {
        return $this->extra['cliente_apellido_materno'] ?? ''; 
    }

    public function getFecha(): string
    {
        return $this->extra['fecha'] ?? '';
    }

    public function getCostoPromedio(): ?float
    {
        return $this->extra['costo_promedio']?? null;
    }

    public function getSucursal(): int
    {
        return env('ALMACEN_PRIMARIO') == 'QUERETARO'? 1 : 2; // 1 Queretaro, 2 San Juan
    }

    public function getVendedor(): ?string
    {
        return $this->extra['vendedor']?? null;
    }

    public function getCantidad(): ?float
    {
        return $this->extra['cantidad']?? 1;
    }

    public function failedMessage($message = 'La petición para agregar cargo de la refaccion a los asientos a contabilidad no fue satisfactoria.'): string
    {
        return tap($message, function($message){
            Log::warning($message, $this->venta->toArray());
        });
    }
}