<?php 

namespace App\Servicios\Asientos;

interface CostoPromedioInterface
{
    /**
     * Se envía la fecha
     *
     * @return string
     */

    public function getCostoPromedio(): ?float;
}