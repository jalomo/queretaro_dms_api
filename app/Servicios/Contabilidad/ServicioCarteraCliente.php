<?php

namespace App\Servicios\Contabilidad;

use App\Servicios\Core\ServicioDB;
use App\Models\Contabilidad\CarteraClienteModel;

class ServicioCarteraCliente extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'cartera cliente';
        $this->modelo = new CarteraClienteModel();
    }

    public function getReglasGuardar()
    {
        return [
            CarteraClienteModel::CLIENTE_ID => 'required|numeric|exists:clientes,id',
            CarteraClienteModel::PLAZO_CREDITO_ID => 'nullable|numeric|exists:plazos_credito,id',
            CarteraClienteModel::MONTO_ACTUAL => 'required|numeric',
            CarteraClienteModel::FECHA_OPERACION => 'required|date',
            CarteraClienteModel::COMENTARIOS => 'nullable'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CarteraClienteModel::CLIENTE_ID => 'required|numeric|exists:clientes,id',
            CarteraClienteModel::PLAZO_CREDITO_ID => 'nullable|numeric|exists:plazos_credito,id',
            CarteraClienteModel::MONTO_ACTUAL => 'required|numeric',
            CarteraClienteModel::FECHA_OPERACION => 'required|date',
            CarteraClienteModel::COMENTARIOS => 'nullable'
        ];
    }

    public function getCarteraCliente(int $cliente_id)
    {
        return $this->modelo->with([
            CarteraClienteModel::REL_CLIENTE,
            CarteraClienteModel::REL_PLAZO_CREDITO,
        ])->where(CarteraClienteModel::CLIENTE_ID, $cliente_id)->get();
    }
}
