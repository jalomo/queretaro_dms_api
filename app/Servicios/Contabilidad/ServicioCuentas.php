<?php

namespace App\Servicios\Contabilidad;

use App\Servicios\Core\ServicioDB;
use App\Models\Contabilidad\CatalogoCuentasModel;
use DB;

class ServicioCuentas extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catalogo cuentas';
        $this->modelo = new CatalogoCuentasModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatalogoCuentasModel::NO_CUENTA => 'required',
            CatalogoCuentasModel::NOMBRE_CUENTA => 'required',
            CatalogoCuentasModel::ID_MOVIMIENTO => 'nullable'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatalogoCuentasModel::NO_CUENTA => 'nullable',
            CatalogoCuentasModel::NOMBRE_CUENTA => 'nullable',
            CatalogoCuentasModel::ID_MOVIMIENTO => 'nullable'
        ];
    }

    public function getCuentaBancarias()
    {
        return $this->modelo->select(
            CatalogoCuentasModel::ID,
            CatalogoCuentasModel::NOMBRE_CUENTA,
            CatalogoCuentasModel::NO_CUENTA,
            CatalogoCuentasModel::ID_MOVIMIENTO,
            DB::raw('CONCAT(nombre_cuenta) as nombre')

        )
            ->whereIn(CatalogoCuentasModel::NO_CUENTA, ['105001', '105006', '105012', '105007', '105009', '114001'])->get();
    }
}
