<?php

namespace App\Servicios\Contabilidad;

use App\Servicios\Core\ServicioDB;
use App\Models\Contabilidad\CatalogoProcesosModel;
use CatalogoProcesosSeed;

class ServicioProcesos extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catalogo cuentas';
        $this->modelo = new CatalogoProcesosModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatalogoProcesosModel::ID => 'required',
            CatalogoProcesosModel::DESCRIPCION => 'required',
            CatalogoProcesosModel::CLAVE_POLIZA => 'nullable'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatalogoProcesosModel::ID => 'required',
            CatalogoProcesosModel::DESCRIPCION => 'required',
            CatalogoProcesosModel::CLAVE_POLIZA => 'nullable'
        ];
    }

    public function buscarTodos()
    {
        return $this->modelo->whereIn(CatalogoProcesosModel::ID, [
            CatalogoProcesosModel::PROCESO_VENTA_MOSTRADOR,
            CatalogoProcesosModel::PROCESO_SERVICIOS,
            CatalogoProcesosModel::PROCESO_VENTA_AUTOS_NUEVOS
        ])->get();
    }
}
