<?php

namespace App\Servicios\Pilot;

use App\Models\Logs\Pilot\LogsPilotModel;
use App\Servicios\Core\ServicioDB;
use App\Servicios\Logs\Pilot\ServicioLogsPilot;
use App\Servicios\Refacciones\ServicioCurl;
use App\Servicios\Soporte\ServicioTelefonosSoporte;
use Illuminate\Support\Facades\DB;

class ServicioPilot extends ServicioDB
{
    protected static $token = '';
    protected $servicioCURL;
    protected $servicioPilot;
    protected $servicioTelefonosSoporte;
    public function __construct()
    {
        $this->servicioCURL = new ServicioCurl();
        $this->servicioPilot = new ServicioLogsPilot();
        $this->servicioTelefonosSoporte = new ServicioTelefonosSoporte();
    }
    public function CreateUnidad($data, $type, $refreshToken = true)
    {
        $datosUnidad = [
            'data' => $data,
            'header' => $this->getHeaders('stock_create')
        ];
        $createdUnidadPilot = $this->servicioCURL->curlPost(
            env('URL_PILOT') . 'v1/stock/create.php',
            $datosUnidad,
            true
        );
        //Guadar logs
        $responseDecoded = json_decode($createdUnidadPilot);
        $this->servicioPilot->crear([
            LogsPilotModel::TYPE => $type,
            LogsPilotModel::POSTDATA => json_encode($datosUnidad),
            LogsPilotModel::RESPONSEDATA => $createdUnidadPilot,
            LogsPilotModel::STATUS => $responseDecoded->result->status,
        ]);
        if (
            $responseDecoded->result->status == 'error' &&
            $responseDecoded->result->code == 'unauthorized' &&
            $refreshToken
        ) {
            $this->refreshToken();
            $this->CreateUnidad($data, $type, false);
        }

        return $createdUnidadPilot;
    }
    public function UpdateUnidad($data, $type, $refreshToken = true)
    {
        $datosUnidad = [
            'data' => $data,
            'header' => $this->getHeaders('stock_update')
        ];
        $updatedUnidadPilot = $this->servicioCURL->curlPost(
            env('URL_PILOT') . 'v1/stock/update.php',
            $datosUnidad,
            true
        );

        //Guadar logs
        $responseDecoded = json_decode($updatedUnidadPilot);
        $this->servicioPilot->crear([
            LogsPilotModel::TYPE => $type,
            LogsPilotModel::POSTDATA => json_encode($datosUnidad),
            LogsPilotModel::RESPONSEDATA => $updatedUnidadPilot,
            LogsPilotModel::STATUS => $responseDecoded->result->status,
        ]);

        if (
            $responseDecoded->result->status == 'error' &&
            $responseDecoded->result->code == 'unauthorized' &&
            $refreshToken
        ) {
            $this->refreshToken();
            $this->UpdateUnidad($data, $type, false);
        }
        return $updatedUnidadPilot;
    }
    public function getHeaders($flowName)
    {
        if (!self::$token) {
            $this->refreshToken();
        }
        return [
            "FlowName" => $flowName,
            "SequenceId" => 1,
            "TimeStamp" => date('His'),
            "TrackingId" => "55A6BCD4-0857-4A86-85FB-09A228B641B4",
            "access_token" => self::$token,
        ];
    }
    public function refreshToken()
    {
        $datosLogin = [
            'username' => env('USER_PILOT'),
            'password' => env('PASS_PILOT'),
        ];
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => env('URL_PILOT') . 'v1/users/auth.php',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $datosLogin,
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $responseDecoded = json_decode($response);
        //Guadar logs
        $this->servicioPilot->crear([
            LogsPilotModel::TYPE => LogsPilotModel::TYPE_LOGIN,
            LogsPilotModel::POSTDATA => json_encode($datosLogin),
            LogsPilotModel::RESPONSEDATA => $response,
            LogsPilotModel::STATUS => $responseDecoded->result->status,
        ]);
        if ($responseDecoded->result->status == 'success') {
            self::$token = $responseDecoded->result->entitydata;
        }
    }
    public function enviarNotificacion(string $mensaje): void
    {
        $telefonos = $this->servicioTelefonosSoporte->buscarTodos();
        if (!empty($telefonos)) {
            foreach ($telefonos as $telefono) {
                $this->servicioCURL->curlPost(env('URL_NOTIFICACIONES'), [
                    'celular' => $telefono->telefono,
                    'mensaje' => $this->eliminarTildes($mensaje),
                    'sucursal' => 'M2137'
                ], false);
            }
        }
    }
    private function eliminarTildes($cadena)
    {

        $cadena = ($cadena);

        //Ahora reemplazamos las letras
        $cadena = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
            array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $cadena
        );

        $cadena = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $cadena
        );

        $cadena = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
            array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $cadena
        );

        $cadena = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $cadena
        );

        $cadena = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $cadena
        );

        $cadena = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),
            array('n', 'N', 'c', 'C'),
            $cadena
        );

        return $cadena;
    }
}
