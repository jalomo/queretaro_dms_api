<?php

namespace App\Servicios\Ventas\Refacciones;

use App\Servicios\Refacciones\ServicioFolios;
use App\Models\Refacciones\VentasRealizadasModel;
use App\Models\Refacciones\VentaProductoModel;
use App\Models\Refacciones\EstatusVentaModel;
use App\Models\Refacciones\ReVentasEstatusModel;
use App\Models\Contabilidad\CatalogoProcesosModel;
use App\Models\CuentasPorCobrar\AbonosModel;
use App\Models\CuentasPorCobrar\CuentasPorCobrarModel;
use App\Models\CuentasPorCobrar\EstatusCuentaModel;
use App\Models\CuentasPorCobrar\PlazoCreditoModel;
use App\Models\CuentasPorCobrar\TipoFormaPagoModel;
use App\Models\CuentasPorPagar\CatEstatusAbonoModel;
use App\Models\CuentasPorPagar\CatTipoAbonoModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Resources\DataTablesResource;
use App\Models\Refacciones\ProductosModel;
use Illuminate\Support\Facades\Http;
use Throwable;
use Exception;
use Carbon\Carbon;

class ServicioVentaRefacciones
{
    protected $modelo;
    protected $servicioFolio;
    protected $ventaProducto;

    public function __construct(
        VentasRealizadasModel $modelo,
        ServicioFolios $servicioFolio
    )
    {
        $this->modelo = $modelo;
        $this->servicioFolio = $servicioFolio;
    }

    /**
     * Paginación de ventas de refacciones
     * 
     * @return Illuminate\Http\Resources\Json\AnonymousResourceCollection 
     */
    public function all()
    {
        return $this->modelo
            ->paginate();
    }

    /**
     * Se busca venta individual
     * 
     * @param int $id
     * @return App\Models\Refacciones\VentasRealizadasModel
     */
    public function getById(int $id)
    {
        $this->exists($id);
        request()->merge(['page' => floor(request()->start / (request()->length?:1)) + 1]);

        $ventas = $this->modelo
            ->join('venta_producto', 'venta_producto.venta_id', '=', 'ventas.id')
            ->join('re_ventas_estatus', 're_ventas_estatus.ventas_id', '=', 'ventas.id')
            ->join('precio', 'precio.id', '=', 'venta_producto.precio_id')
            ->join('producto', 'producto.id', '=', 'venta_producto.producto_id')
            ->leftJoin('catalogo_ubicaciones_producto', 'catalogo_ubicaciones_producto.id', '=', 'producto.ubicacion_producto_id')
            ->join('almacen', 'venta_producto.almacen_id', '=', 'almacen.id')
            ->where('re_ventas_estatus.activo', true)
            ->where('ventas.id', $id)
            ->select(
                'venta_producto.*',
                'producto.no_identificacion',
                'producto.descripcion as descripcion_producto',
                'producto.unidad as unidad',
                'precio.precio_publico',
                'catalogo_ubicaciones_producto.nombre as ubicacion_producto',
                're_ventas_estatus.estatus_ventas_id',
                'almacen.ubicacion as ubicacion_almacen',
            )
            ->orderBy('venta_producto.id')
            ->paginate(request()->length);

        return new DataTablesResource($ventas);
    }

    /**
     * Se crea venta de refacciones
     * 
     * @param array $request
     * @return App\Models\Refacciones\VentasRealizadasModel
     */
    public function store(array $request)
    {
        try
        {
            DB::beginTransaction();

            // Generamos Folio DMS
            $folio = $this->servicioFolio->generarFolio(CatalogoProcesosModel::PROCESO_VENTA_MOSTRADOR);
    
            $request['folio_id'] = $folio->id;
            $request['subtotal'] = $request['valor_unitario'] * $request['cantidad'];
            $request['iva'] = $request['subtotal'] * VentasRealizadasModel::IVA_ACTUAL;
            $request['venta_total'] = $request['subtotal'] + $request['iva'];
            
            // Guardamos venta
            $venta = $this->modelo->create($request);
            $request['venta_id'] = $venta->id;

            if(null == $venta)
            {
                throw new Exception('La venta no se registro.' , 400);
            }
    
            // Guardamos nuevo estatus
            $venta->re_ventas_estatus()->save(new ReVentasEstatusModel([
                'venta_id' => $venta->id,
                'estatus_ventas_id' => EstatusVentaModel::ESTATUS_PROCESO,
                'user_id' => auth()->user()->id,
                'activo' => true,
            ]));
        
            // Agregamos detalle a asientos
            $request['id_requisicion_index'] = $this->storePiezas($venta, [
                'precio' => $request['venta_total'],
                'cantidad' => $request['cantidad'],
                'producto_id' => $request['producto_id'],
            ]);

            // Guardamos Productos
            $venta->detalle_venta()->save(new VentaProductoModel($request));

            DB::commit();
        }
        catch(Throwable $e)
        {
            DB::rollback();
            Log::warning($e->getMessage());

            return ['message' => 'Ocurrio un error al registrar la venta.'];
        }

        return ['id' => $venta->id, 'message' => 'Se registro venta con exito.'];
    }

    /**
     * Se actualiza venta de refacciones
     * 
     * @param array $request
     * @param int $id
     * @return App\Models\Refacciones\VentasRealizadasModel
     */
    public function update(array $request, int $id)
    {
        $venta = $this->exists($id);
        
        try {
            DB::beginTransaction();

            // Buscamos cuenta por cobrar con el folio
            $cuenta_por_cobrar = $venta->folio->cuenta_por_cobrar;

            // Si no existe se arroja error
            if (null != $cuenta_por_cobrar && $cuenta_por_cobrar->estatus_cuenta_id != 1) {
                throw new Exception(
                    "Error al actualizar !! La venta se encuentra en proceso de pago."
                );
            }

            $request['subtotal'] = VentaProductoModel::where('folio_id', $venta->folio->id)->sum('venta_total');
            $request['iva'] = $request['subtotal'] * VentasRealizadasModel::IVA_ACTUAL;
            $request['venta_total'] = $request['subtotal'] + $request['iva'];
            
            // Actualizamos Cuentas por Cobrar
            $cuenta_por_cobrar = CuentasPorCobrarModel::
                where(CuentasPorCobrarModel::FOLIO_ID, $venta->folio->id)
                ->update([
                    CuentasPorCobrarModel::TOTAL => $request['venta_total'],
                    CuentasPorCobrarModel::IMPORTE => $request['venta_total'],
                    CuentasPorCobrarModel::CONCEPTO => $request['concepto']
                ]);

            // Obtenemos Abonos Pendientes
            $abonos_pendientes = AbonosModel::
                where('cuenta_por_cobrar_id', $cuenta_por_cobrar->id)
                ->where('estatus_abono_id', CatEstatusAbonoModel::PENDIENTE)
                ->whereIn('tipo_abono_id', [CatTipoAbonoModel::ABONO, CatTipoAbonoModel::UN_SOLO_PAGO])
                ->get();

            // Se actualiza la cantidad por abonos pendientes
            $abono = round($cuenta_por_cobrar->total / count($abonos_pendientes));
            $abonos_pendientes->each->update([
                AbonosModel::TOTAL_ABONO => $abono
            ]);
            
            $venta->fill($request)->save();

            DB::commit();
        } 
        catch (Throwable $e) 
        {
            Log::warning($e->getMessage());
            DB::rollback();
        }

        return $venta;
    }

    /**
     * Se finaliza venta
     * 
     * @param int $id
     * @return App\Models\Refacciones\VentasRealizadasModel
     */
    public function finalizar(array $request, int $id)
    {
        $venta = $this->exists($id);

        try
        {
            DB::beginTransaction();

            $venta->fill($request)->save();

            if ($request['tipo_forma_pago_id'] == CatTipoAbonoModel::ABONO) 
            {
                $tasa_interes = $request['tasa_interes'];
                $plazo_credito = $request['plazo_credito_id'];
                $cantidad_abonos = PlazoCreditoModel::find($request['plazo_credito_id'])->cantidad_mes;

                $abono_mensual = (($venta->venta_total - $request['enganche']) / $cantidad_abonos);
                $interes_mensual = ($abono_mensual * $request['tasa_interes']) / 100;
                $intereses = $interes_mensual * $cantidad_abonos;
                $total = $venta->total_venta + $intereses;
            } 
            else 
            {
                $total = $venta->venta_total;
                $intereses = 0;
            }

            // Actualizamos cuenta por cobrar o la insertamos si no existe folio id
            $cuenta_por_cobrar = CuentasPorCobrarModel::updateOrCreate([
                'folio_id' => $venta->folio->id
            ], [
                CuentasPorCobrarModel::FOLIO_ID => $venta->folio->id,
                CuentasPorCobrarModel::CLIENTE_ID => $request['cliente_id'],
                CuentasPorCobrarModel::CONCEPTO => $request['concepto'],
                CuentasPorCobrarModel::NUMERO_ORDEN => ! empty($request['numero_orden']) ? $request['numero_orden'] : '',
                CuentasPorCobrarModel::IMPORTE => $venta->venta_total,
                CuentasPorCobrarModel::TOTAL => $total,
                CuentasPorCobrarModel::FECHA => ! empty($request['fecha']) ?  $request['fecha'] : date('Y-m-d'),
                CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID => $request['tipo_forma_pago_id'],
                CuentasPorCobrarModel::TIPO_PAGO_ID => $request['tipo_pago_id'],
                CuentasPorCobrarModel::PLAZO_CREDITO_ID => $request['plazo_credito_id'],
                CuentasPorCobrarModel::ENGANCHE => ! empty($request['enganche']) ? $request['enganche'] : 0,
                CuentasPorCobrarModel::TASA_INTERES => ! empty($request['tasa_interes']) ? $request['tasa_interes'] : 0,
                CuentasPorCobrarModel::INTERESES => $intereses,
                CuentasPorCobrarModel::ESTATUS_CUENTA_ID =>  ! empty($request['estatus_cuenta_id']) ? $request['estatus_cuenta_id'] : EstatusCuentaModel::ESTATUS_POR_EMPEZAR,
            ]);

            // Si es credito
            if ($cuenta_por_cobrar->tipo_forma_pago_id == TipoFormaPagoModel::FORMA_CREDITO) 
            {
                $abonos = AbonosModel::where(AbonosModel::CUENTA_POR_COBRAR_ID, $cuenta_por_cobrar->id)->get();

                // Si existe un abono un solo pago eliminamos el abono
                if ($abonos != null) 
                {
                    $abonos->each->delete();
                }

                // Si el enganche es mayor a 0 se agrega abono
                if ($cuenta_por_cobrar->enganche > 0) 
                {
                    AbonosModel::create([
                        AbonosModel::CUENTA_POR_COBRAR_ID => $cuenta_por_cobrar->id,
                        AbonosModel::TIPO_ABONO_ID => CatTipoAbonoModel::ENGANCHE,
                        AbonosModel::ESTATUS_ABONO_ID => CatEstatusAbonoModel::PENDIENTE,
                        AbonosModel::FECHA_VENCIMIENTO => $cuenta_por_cobrar->fecha,
                        AbonosModel::TOTAL_ABONO => $cuenta_por_cobrar->enganche
                    ]);
                }

                // Creamos los abonos para el credito
                for ($num_abono = 1; $num_abono <= $cantidad_abonos; $num_abono++) 
                {
                    AbonosModel::create([
                        AbonosModel::CUENTA_POR_COBRAR_ID => $cuenta_por_cobrar->id,
                        AbonosModel::TIPO_ABONO_ID => CatTipoAbonoModel::ABONO,
                        AbonosModel::ESTATUS_ABONO_ID => CatEstatusAbonoModel::PENDIENTE,
                        AbonosModel::FECHA_VENCIMIENTO => Carbon::createFromDate($cuenta_por_cobrar->fecha)->addMonths($num_abono)->format('Y-m-d'),
                        AbonosModel::TOTAL_ABONO => $abono_mensual + $interes_mensual
                    ]);
                }
            } 
            else 
            {
                // Si es exibicion en un solo pago se agrega el abono
                AbonosModel::create([
                    AbonosModel::CUENTA_POR_COBRAR_ID => $cuenta_por_cobrar->id,
                    AbonosModel::TIPO_ABONO_ID => CatTipoAbonoModel::UN_SOLO_PAGO,
                    AbonosModel::ESTATUS_ABONO_ID => CatEstatusAbonoModel::PENDIENTE,
                    AbonosModel::FECHA_VENCIMIENTO => $cuenta_por_cobrar->fecha,
                    AbonosModel::TOTAL_ABONO => $cuenta_por_cobrar->total
                ]);
            }

            DB::commit();
        } 
        catch(Throwable $e)
        {
            Log::warning($e->getMessage());
            DB::rollback();
        }

        return $venta;
    }

    /**
     * Se elimina la venta de las refacciones
     * 
     * @param int $id
     * @return bool
     */
    public function destroy(int $id)
    {
        $venta = $this->exists($id);

        return $venta->destroy();
    }

    

    /**
     * Comprueba que exista modelo
     * 
     * @return App\Models\Refacciones\VentasRealizadasModel
     */
    protected function exists($id)
    {
        $venta = $this->modelo->find($id);

        if(null == $venta)
        {
            throw new Exception('Venta no fue encontrada.', 400);
        }

        return $venta;
    }

    /**
     * Agregamos producto a asientos
     * 
     */
    protected function storePiezas($venta, $data)
    {
        $producto = ProductosModel::find($data['producto_id']);

        $response = Http::withBody(json_encode([[
            'codigo' => $producto->no_identificacion,
            'descripcion' => $producto->descripcion,
            'precio' => $data['precio'],
            'cantidad' => $data['cantidad'],
            'tiempo' => 0,
        ]]), 'json')
        ->post(env('URL_SERVICIOS') . 'api/addPiezasFromDMS/' . $venta->numero_orden);

        $pieza = $response->object();

        if(! $response->successful() || $pieza->exito == false)
        {
            throw new Exception('Respuesta no exitosa de: '. env('URL_SERVICIOS') . 'api/addPiezasFromDMS/' . $venta->numero_orden);
        }

        return $pieza->id;
    }

    /**
     * Eliminamos producto de asientos
     * 
     */
}