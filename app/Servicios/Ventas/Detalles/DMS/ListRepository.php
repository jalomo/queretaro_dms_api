<?php 

namespace App\Servicios\Ventas\Detalles\DMS;

use App\Http\Requests\Ventas\Detalles\DMS\ListRequest;
use App\Models\Refacciones\VentasRealizadasModel;
use App\Servicios\Ventas\Detalles\RepositoryAbstract;
use App\Servicios\Ventas\Detalles\RepositoryInterface;
use Illuminate\Http\Request;

class ListRepository extends RepositoryAbstract implements RepositoryInterface
{
    public function repository(Request $request)
    {        
        $refacciones = VentasRealizadasModel::with('productos')->find($request->id);

        if($refacciones == null)
        {
            logger()->warning(json_encode($request->validated()));

            return response()->json([
                'id' => null,
                'message' => __('Ocurrio un problema al eliminar la refacción.'),
            ], 400);
        }

        return response()->json($refacciones);
    }

    public function getFormRequest() : string
    {
        return ListRequest::class;
    }
}