<?php 

namespace App\Servicios\Ventas\Detalles\DMS;

use App\Http\Requests\Ventas\Detalles\DMS\UpdateRequest;
use App\Servicios\Ventas\Detalles\RepositoryInterface;
use App\Models\Refacciones\VentaProductoModel;
use App\Servicios\Ventas\Detalles\RepositoryAbstract;
use Illuminate\Http\Request;

class UpdateRepository extends RepositoryAbstract implements RepositoryInterface
{
    public function repository(Request $request)
    {   
        $refaccion = tap(VentaProductoModel::where('id', $request->id)->first())
            ->update(
                $request->validated(),
            );

        if($refaccion == null)
        {
            logger()->warning(json_encode($request->validated()));

            return response()->json([
                'id' => null,
                'message' => __('Ocurrio un problema al actualizar la refacción en dms.'),
            ], 400);
        }

        return response()->json([
            'id' => $refaccion->id,
            'message' => __('Se actualizó la pieza a detalles'),
        ]);
    }

    public function getFormRequest(): string
    {
        return UpdateRequest::class;
    }
}