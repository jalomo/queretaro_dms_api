<?php 

namespace App\Servicios\Ventas\Detalles\DMS;

use App\Http\Requests\Ventas\Detalles\DMS\StoreRequest;
use App\Servicios\Ventas\Detalles\RepositoryInterface;
use App\Models\Refacciones\VentaProductoModel;
use App\Servicios\Ventas\Detalles\RepositoryAbstract;
use Illuminate\Http\Request;

class StoreRepository extends RepositoryAbstract implements RepositoryInterface
{
    public function repository(Request $request)
    {
        $refaccion = VentaProductoModel::create($request->validated());

        if($refaccion == null)
        {
            logger()->warning(json_encode($request->validated()));

            return response()->json([
                'id' => null,
                'message' => __('Ocurrio un problema al insertar la refacción en detalles.'),
            ], 400);
        }

        return response()->json([
            'id' => $refaccion->id,
            'message' => __('Se agrego la pieza a detalles'),
        ]);
    }

    public function getFormRequest(): string
    {
        return StoreRequest::class;
    }
}