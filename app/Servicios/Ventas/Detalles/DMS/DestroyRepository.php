<?php 

namespace App\Servicios\Ventas\Detalles\DMS;

use App\Http\Requests\Ventas\Detalles\DMS\DestroyRequest;
use App\Servicios\Ventas\Detalles\RepositoryInterface;
use App\Models\Refacciones\VentaProductoModel;
use App\Servicios\Ventas\Detalles\RepositoryAbstract;
use Illuminate\Http\Request;

class DestroyRepository extends RepositoryAbstract implements RepositoryInterface
{
    public function repository(Request $request)
    {
        $refaccion = VentaProductoModel::where('id', $request->id)->first();

        if($refaccion == null || ! $refaccion->delete())
        {
            logger()->warning(json_encode($request->validated()));

            return response()->json([
                'id' => null,
                'message' => __('Ocurrio un problema al eliminar la refacción.'),
            ], 400);
        }

        return response()->json([
            'id' => $refaccion->id,
            'message' => __('Se eliminó la refacción a detalles.'),
        ]);
    }

    public function getFormRequest(): string
    {
        return DestroyRequest::class;
    }
}