<?php

namespace App\Servicios\Ventas\Detalles;

use Illuminate\Http\Request;

abstract class RepositoryAbstract
{
    // Declaramos clase si queremos asignar reglas de validacion
    // al request que llega a esta clase
    // public function getRules();

    public function handle(Request $request)
    {
        $this->validate($request);

        return $this->repository($request);
    }

    public function validate(Request $request)
    {
        if(method_exists($this, 'getRules')){
            return $request->validate($this->getRules());
        }

        // Ejecutamos el Request ya que no podemos inyectar 
        // directamente atráves del IoC de laravel debido a la interfaz
        // que es tipo request
        return app($this->getFormRequest());
    }

    public abstract function getFormRequest() : string;

    public abstract function repository(Request $request);
}