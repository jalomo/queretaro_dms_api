<?php 

namespace App\Servicios\Ventas\Preventivo\DMS;

use App\Http\Requests\Ventas\Preventivo\DMS\UpdateRequest;
use App\Models\Refacciones\ProductosModel;
use App\Servicios\Ventas\Detalles\RepositoryInterface;
use App\Servicios\Ventas\Detalles\RepositoryAbstract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class StoreRepository extends RepositoryAbstract implements RepositoryInterface
{
    public function repository(Request $request)
    {
        $producto = $this->getProducto($request->producto_id);

        $response = Http::withBody(json_encode([[
                'id' => $request->id,
                'codigo' => $producto->no_identificacion,
                'descripcion' => $producto->descripcion,
                'precio' => $request->precio,
                'cantidad' => $request->cantidad,
                'tiempo' => 0,
                'afecta_paquete' => $request->afecta_paquete,
                'afecta_mano_obra' => $request->afecta_mano_obra,
            ]]), 'json')
            ->post(env('URL_SERVICIOS') . 'api/updatePaqueteFromDMS/' . $request->numero_orden);

        $refaccion = $response->object();

        if(! $response->successful() || $refaccion == false || $refaccion->exito == false)
        {
            logger()->warning(json_encode($request->all()));

            return response()->json([
                'id' => null,
                'message' => __('Ocurrio un problema al actualizar la refacción en preventivo.'),
            ], 400);
        }

        return response()->json([
            'id' => collect($refaccion->dataUpdated)->firstWhere('codigo', $producto->no_identificacion)->id,
            'message' => __('Se actualiza la pieza a preventivo'),
        ]);
    }

    public function getFormRequest(): string
    {
        return UpdateRequest::class;
    }

    public function getProducto(int $id)
    {
        return ProductosModel::find($id);
    }
}