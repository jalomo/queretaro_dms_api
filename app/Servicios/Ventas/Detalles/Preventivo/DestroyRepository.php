<?php 

namespace App\Servicios\Ventas\Preventivo\DMS;

use App\Http\Requests\Ventas\Preventivo\DMS\DestroyRequest;
use App\Servicios\Ventas\Detalles\RepositoryInterface;
use App\Servicios\Ventas\Detalles\RepositoryAbstract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class StoreRepository extends RepositoryAbstract implements RepositoryInterface
{
    public function repository(Request $request)
    {
        $response = Http::withBody(json_encode([
                'id' => $request->id,
            ]), 'json')
            ->post(env('URL_SERVICIOS') . 'api/deletePiezasFromDMS/' . $request->numero_orden);

        $refaccion = $response->object();

        if(! $response->successful() || $refaccion == false || $refaccion->exito == false)
        {
            logger()->warning(json_encode($request->all()));

            return response()->json([
                'id' => null,
                'message' => __('Ocurrio un problema al actualizar la refacción en preventivo.'),
            ], 400);
        }

        return response()->json([
            'id' => $request->id,
            'message' => __('Se actualiza la pieza a preventivo'),
        ]);
    }

    public function getFormRequest(): string
    {
        return DestroyRequest::class;
    }
}