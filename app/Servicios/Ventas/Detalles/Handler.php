<?php

namespace App\Servicios\Ventas\Detalles;

use App\Exceptions\RepositoryEmptyException;
use App\Exceptions\RepositoryHandlerNotFoundException;
use Illuminate\Http\Request;

class Handler implements HandlerInterface
{
    protected $repositories = [
        'presupuesto' => [
            'store' => \App\Servicios\Ventas\Detalles\Presupuestos\StoreRepository::class,
            'update' => \App\Servicios\Ventas\Detalles\Presupuestos\UpdateRepository::class,
            'destroy' => \App\Servicios\Ventas\Detalles\Presupuestos\DestroyRepository::class,
            'list' => \App\Servicios\Ventas\Detalles\Presupuestos\ListRepository::class,
        ],
        'preventivo' => [
            'store' => \App\Servicios\Ventas\Detalles\Preventivo\StoreRepository::class,
            'update' => \App\Servicios\Ventas\Detalles\Preventivo\UpdateRepository::class,
            'destroy' => \App\Servicios\Ventas\Detalles\Preventivo\DestroyRepository::class,
            'list' => \App\Servicios\Ventas\Detalles\Preventivo\ListRepository::class,
        ],
        'dms' => [
            'store' => \App\Servicios\Ventas\Detalles\DMS\StoreRepository::class,
            'update' => \App\Servicios\Ventas\Detalles\DMS\UpdateRepository::class,
            'destroy' => \App\Servicios\Ventas\Detalles\DMS\DestroyRepository::class,
            'list' => \App\Servicios\Ventas\Detalles\DMS\ListRepository::class,
        ]
    ];

    public function handle(Request $request, $operation)
    {
        return resolve($this->getRepository($operation, $request->type?? ''))->handle($request);
    }

    public function getRepository(string $operation, string $type)
    {
        if(empty($type))
        {
            throw new RepositoryEmptyException("No se paso ningun repositorio.");
        }

        if(! isset($this->repositories[$type][$operation]))
        {
            throw new RepositoryHandlerNotFoundException("La siguiente operacion $operation no se encuentra implementada para $type");
        }

        return $this->repositories[$type][$operation];
    }
}