<?php

namespace App\Servicios\Ventas\Detalles;

use Illuminate\Http\Request;

interface HandlerInterface
{
    public function handle(Request $request, $type);

    public function getRepository(string $operation, string $type);
}