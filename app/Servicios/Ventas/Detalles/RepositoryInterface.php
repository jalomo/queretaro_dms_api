<?php

namespace App\Servicios\Ventas\Detalles;

use Illuminate\Http\Request;

interface RepositoryInterface
{
    public function handle(Request $request);
}