<?php

namespace App\Servicios\Ventas\Ventanilla;

use Illuminate\Http\Request;

interface VentaInterface
{
    public function ws(int $numero_orden);

    public function storeRefaccion($item, $request);

    public function storeServicio($item, $request);

    public function getRefaccion($item);

    public function setTotalVenta();

    public static function make(Request $request);
}