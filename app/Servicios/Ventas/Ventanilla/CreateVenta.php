<?php

namespace App\Servicios\Ventas\Ventanilla;

use App\Models\Contabilidad\CatalogoProcesosModel;
use App\Models\Refacciones\VentaProductoModel;
use App\Models\Refacciones\VentaServicioModel;
use App\Models\Refacciones\VentasRealizadasModel;
use App\Servicios\Producto\CrearProductoService;
use App\Servicios\Refacciones\ServicioFolios;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use Exception;
use InvalidArgumentException;

use RuntimeException;

class CreateVenta implements VentaInterface
{
    protected $venta;

    protected $folio;

    public function __construct(array $request)
    {
        $this->setVenta($request);
        $this->setFolio();
        
        $this->handle($request);
    }

    public function ws($numero_orden)
    {
        $dataWS = Http::post(env('URL_SERVICIOS') . env('URL_OPERACIONES'), [
            'id_cita' => $numero_orden,
        ]);

        if($dataWS->successful())
        {
            throw new RuntimeException('La peticion al webservice no fue cargada correctamente');
        }

        if (! $data = $dataWS->object()) 
        {
            throw new Exception('No se encontro infromacion en en la respuesta.');
        }

        if (! isset($data->refacciones)) 
        {
            throw new InvalidArgumentException('No se encontro el campo refacciones.');
        }

        return $data;
    }

    public function setVenta($request)
    {
        VentasRealizadasModel::firstOrCreate($request);
    }

    public function getVenta()
    {
        return $this->venta;
    }

    public function setFolio()
    {
        $servicioFolio = new ServicioFolios;
        $this->folio = $servicioFolio->generarFolio(CatalogoProcesosModel::PROCESO_SERVICIOS);
    }

    public function getFolio()
    {
        return $this->folio;
    }

    public function storeRefaccion($item, $request)
    {
        $producto = $this->getRefaccion($item);

        info("ACTUALIZAR VENTA DETALLES ID {$this->venta->id}, INDEX REQUISICION: $item->id :: $item->num_pieza -> $item->precio_unitario");

        // Creamos el registro en ventas_productos si no existen los campos
        // folio_id, venta_id, producto_id o Actualizamos
        VentaProductoModel::updateOrCreate(
            [
                VentaProductoModel::FOLIO_ID => $this->folio->id,
                VentaProductoModel::VENTA_ID => $this->venta->id,
                VentaProductoModel::PRODUCTO_ID => $producto->id,
                VentaProductoModel::ID_INDEX_REQUISICION => $item->id,
            ],
            [
                VentaProductoModel::PRODUCTO_ID => $producto->id,
                VentaProductoModel::PRECIO_ID => 1,
                VentaProductoModel::VALOR_UNITARIO => $item->precio_unitario, // Fix: porque el precio unitario es el total de todas las pieza
                VentaProductoModel::CANTIDAD => round($item->cantidad),
                VentaProductoModel::TOTAL_VENTA => $item->costo_refaccion + $item->costo_mano_obra * VentaProductoModel::IVA, //# costo_refaccion trae el total
                VentaProductoModel::DESCONTADO_ALMACEN => 1,
                VentaProductoModel::ORDEN_ORIGINAL => 1,
                VentaProductoModel::ESTATUS_PRODUCTO_ORDEN => "O",
                VentaProductoModel::ID_INDEX_REQUISICION => $item->id,
                VentaProductoModel::PREPIKING => $item->tipo == "PREPIKING" ? TRUE : FALSE,
                VentaProductoModel::AFECTA_PAQUETE => filter_var($item->tipo_preventivo, FILTER_VALIDATE_BOOLEAN) ? TRUE : FALSE,
                VentaProductoModel::PREVENTIVO => filter_var($item->tipo_preventivo, FILTER_VALIDATE_BOOLEAN) ? TRUE : FALSE,
                VentaProductoModel::PRESUPUESTO => filter_var($item->presupuesto, FILTER_VALIDATE_BOOLEAN) ? TRUE : FALSE,
                VentaProductoModel::COSTO_MANO_OBRA => $item->costo_mano_obra * VentaProductoModel::IVA,
            ]
        );
    }

    public function storeServicio($item, $request)
    {
        return $this->servicioVentasServicios->createUpdateServicioVenta([
            VentaServicioModel::NOMBRE_SERVICIO => $item->descripcion,
            VentaProductoModel::VENTA_ID =>  $this->venta->id,
            VentaServicioModel::PRECIO => (float) $item->costo_mo,
            VentaServicioModel::NO_IDENTIFICACION => trim($item->num_pieza),
            VentaServicioModel::CANTIDAD => (int) $item->cantidad,
            VentaServicioModel::IVA => $item->iva,
            VentaServicioModel::COSTO_MO =>  $item->costo_mo
        ]);
    }

    public function getRefaccion($item)
    {
        // Buscamos Producto     
        // y si no existe el producto se agrega el numero de pieza
        // al stock y se pone en ceros.
        $productoService = new CrearProductoService;

        return $productoService->make([
            'no_identificacion' => $item->num_pieza,
            'descripcion' => $item->descripcion,
            'valor_unitario' => $item->precio_unitario,
        ]);
    }

    public static function make(Request $request)
    {
        return new self($request->validated());
    }

    public function handle(array $request)
    {
        $items = $this->ws($request['numero_orden']);

        foreach($items as $item)
        {
            if ($item->tipo == "OPERACION")
                $this->storeServicio($item, $request);

            if($item->tipo == "REFACCION" && $item->num_pieza == '')
                $this->storeRefaccion($item, $request);

            if($item->tipo == 'PREPIKING')
                $this->storeRefaccion($item, $request);
        }
    }
}