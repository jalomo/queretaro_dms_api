<?php

namespace App\Servicios\Ventas;

use App\Models\Refacciones\StockProductosModel;
use App\Models\Refacciones\VentaProductoModel;
use App\Models\Refacciones\VentasRealizadasModel;
use App\Servicios\Inventario\VenderService;
use Illuminate\Support\Facades\DB;
use App\Models\Refacciones\Pedido;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Throwable;

class ServicioPedidoDiaMostrador
{
    public function make(VentasRealizadasModel $venta)
    {
        try
        {
            DB::beginTransaction();

            $max_time = (new Carbon('13:00:00', 'America/Mexico_City'))->subDay(1);
    
            // Buscamos pedido con antiguedad menor a un día
            $pedido = Pedido::where('actual', true)->whereDate('created_at', '>', $max_time)->first();
    
            $ventas = VentaProductoModel::where('venta_id', $venta->id)->where('estatus_producto_orden', '<>', 'B')->get();
    
            foreach($ventas as $venta)
            {
                $stock = StockProductosModel::where('producto_id', $venta->producto_id)->first();
                $vender = new VenderService;
    
                // Comprobamos si no existe pedido y hay necesidad de crearlo porque no se tiene stock
                if($pedido == null && ($stock->cantidad_actual - $venta->cantidad < 0))
                {
                    // Creamos un nuevo pedido
                    $pedido = Pedido::create([
                        'actual' => true,
                    ]);
                }

                if($stock->cantidad_actual - $venta->cantidad >= 0)
                {
                    // Descontamos la cantidad total del producto
                    $vender->id($venta->id);
                    $vender->handle($venta->producto_id, $venta->cantidad, $venta->valor_unitario);

                    // Guardamos cuantas piezas se entregan
                    $venta->cantidad_entregada = $venta->cantidad;
                }
                elseif($stock->cantidad_actual > 0)
                {
                    // Se descuenta solo el stock con el que se cuenta
                    $vender->id($venta->id);
                    $vender->handle($venta->producto_id, $stock->cantidad_actual, $venta->valor_unitario);
                    // $vender->handle($venta->producto_id, $venta->cantidad);
    
                    // El resto se manda al pedido del dia
                    $pedido->productos()->attach($pedido->id, [
                        'model_id' => $venta->id,
                        'producto_id' => $venta->producto_id,
                        'cantidad' => $venta->cantidad - $stock->cantidad_actual,
                        'created_at' => now(),
                    ]);

                    // Guardamos cuantas piezas se entregan
                    $venta->cantidad_entregada = $stock->cantidad_actual;
                }
                elseif($stock->cantidad_actual <= 0)
                {
                    // $vender->handle($venta->producto_id, $venta->cantidad, $venta->valor_unitario);
                    
                    // Se manda todo al pedido del dia
                    $pedido->productos()->attach($pedido->id, [
                        'model_id' => $venta->id,
                        'producto_id' => $venta->producto_id,
                        'cantidad' => $venta->cantidad,
                        'created_at' => now(),
                    ]);
                }

                $venta->save();
            }
    
            DB::commit();

            return true;

        } catch(Throwable $exception){

            Log::warning($exception->getMessage());

            return false;
        }
    }
}