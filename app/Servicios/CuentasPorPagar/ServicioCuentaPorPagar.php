<?php

namespace App\Servicios\CuentasPorPagar;

use App\Servicios\Core\ServicioDB;
use App\Models\CuentasPorPagar\CuentasPorPagarModel;
use App\Models\CuentasPorCobrar\EstatusCuentaModel;
use Illuminate\Support\Facades\DB;

class ServicioCuentaPorPagar extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'cuentas por pagar';
        $this->modelo = new CuentasPorPagarModel();
    }

    public function getReglasGuardarTipoContado()
    {
        return [
            CuentasPorPagarModel::FOLIO_ID => 'required|exists:folios,id',
            CuentasPorPagarModel::PROVEEDOR_ID => 'required|exists:proveedor_refacciones,id',
            CuentasPorPagarModel::ESTATUS_CUENTA_ID => 'required|exists:estatus_cuenta,id',
            CuentasPorPagarModel::CONCEPTO => 'required|string',
            CuentasPorPagarModel::TIPO_FORMA_PAGO_ID => 'required|numeric|exists:tipo_forma_pago,id',
            CuentasPorPagarModel::TIPO_PAGO_ID => 'required|numeric|exists:cat_tipo_pago,id',   
            CuentasPorPagarModel::IMPORTE => 'required|numeric',
            CuentasPorPagarModel::TOTAL => 'nullable|numeric',
            CuentasPorPagarModel::COMENTARIOS => 'nullable|string',
            CuentasPorPagarModel::FECHA => 'required|date',
            CuentasPorPagarModel::PLAZO_CREDITO_ID => 'nullable|numeric|exists:plazos_credito,id',
            CuentasPorPagarModel::ENGANCHE => 'nullable|numeric',
            CuentasPorPagarModel::TASA_INTERES => 'nullable|numeric',
            CuentasPorPagarModel::INTERESES => 'nullable|numeric',
        ];
    }
    public function getReglasGuardarTipoCredito()
    {
        return [
            CuentasPorPagarModel::FOLIO_ID => 'required|exists:folios,id',
            CuentasPorPagarModel::PROVEEDOR_ID => 'required|exists:proveedor_refacciones,id',
            CuentasPorPagarModel::ESTATUS_CUENTA_ID => 'required|exists:estatus_cuenta,id',
            CuentasPorPagarModel::CONCEPTO => 'required|string',
            CuentasPorPagarModel::TIPO_FORMA_PAGO_ID => 'required|numeric|exists:tipo_forma_pago,id',
            CuentasPorPagarModel::TIPO_PAGO_ID => 'required|numeric|exists:cat_tipo_pago,id',
            CuentasPorPagarModel::PLAZO_CREDITO_ID => 'required|numeric|exists:plazos_credito,id',
            CuentasPorPagarModel::IMPORTE => 'required|numeric',
            CuentasPorPagarModel::TOTAL => 'required|numeric',
            CuentasPorPagarModel::ENGANCHE => 'nullable|numeric',
            CuentasPorPagarModel::TASA_INTERES => 'nullable|numeric',
            CuentasPorPagarModel::INTERESES => 'nullable|numeric',
            CuentasPorPagarModel::COMENTARIOS => 'nullable|string',
            CuentasPorPagarModel::FECHA => 'required|date'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CuentasPorPagarModel::FOLIO_ID => 'nullable|exists:folios,id',
            CuentasPorPagarModel::PROVEEDOR_ID => 'nullable|exists:proveedor_refacciones,id',
            CuentasPorPagarModel::ESTATUS_CUENTA_ID => 'required|exists:estatus_cuenta,id',
            CuentasPorPagarModel::CONCEPTO => 'nullable|string',
            CuentasPorPagarModel::TIPO_FORMA_PAGO_ID => 'nullable|numeric|exists:tipo_forma_pago,id',
            CuentasPorPagarModel::TIPO_PAGO_ID => 'nullable|numeric|exists:cat_tipo_pago,id',
            CuentasPorPagarModel::PLAZO_CREDITO_ID => 'nullable|numeric|exists:plazos_credito,id',
            CuentasPorPagarModel::IMPORTE => 'nullable|numeric',
            CuentasPorPagarModel::TOTAL => 'nullable|numeric',
            CuentasPorPagarModel::ENGANCHE => 'nullable|numeric',
            CuentasPorPagarModel::TASA_INTERES => 'nullable|numeric',
            CuentasPorPagarModel::INTERESES => 'nullable|numeric',
            CuentasPorPagarModel::COMENTARIOS => 'nullable|string',
            CuentasPorPagarModel::FECHA => 'nullable|date'
        ];
    }

    public function getReglasByCuentaID()
    {
        return [
            CuentasPorPagarModel::CUENTA_POR_PAGAR_ID => 'required|numeric|exists:cuentas_por_pagar,id',
        ];
    }

    public function getAll($params)
    {
        $consulta =  DB::table('cuentas_por_pagar as cxp')
            ->join('folios', 'cxp.folio_id', '=', 'folios.id')
            ->join('proveedor_refacciones', 'cxp.proveedor_id', '=', 'proveedor_refacciones.id')
            ->join('plazos_credito', 'cxp.plazo_credito_id', '=', 'plazos_credito.id')
            ->join('tipo_forma_pago', 'cxp.tipo_forma_pago_id', '=', 'tipo_forma_pago.id')
            ->join('estatus_cuenta', 'cxp.estatus_cuenta_id', '=', 'estatus_cuenta.id')
            ->leftJoin('abonos_por_pagar', 'cxp.id', '=', 'abonos_por_pagar.cuenta_por_pagar_id')
            ->select(
                'cxp.id',
                'cxp.concepto',
                'cxp.importe',
                'cxp.total',
                'cxp.fecha',
                'cxp.tipo_forma_pago_id',
                'folios.folio',
                'proveedor_refacciones.proveedor_numero as numero_proveedor',
                'proveedor_refacciones.proveedor_nombre as proveedor',
                'plazos_credito.nombre as plazo_credito',
                'plazos_credito.cantidad_mes',
                'tipo_forma_pago.descripcion as tipo_forma_pago',
                'estatus_cuenta.nombre as estatus_cuenta',
                'estatus_cuenta.id as estatus_cuenta_id',
                DB::raw('sum(abonos_por_pagar.total_pago) as monto_pagado')
            );
            if(isset($params['proveedor_id']) && $params['proveedor_id']) {
                $consulta->where('cxp.proveedor_id', $params['proveedor_id']);
            }
            if(isset($params['folio']) && $params['folio']) {
                $folio = $params['folio'];
                $consulta->whereRaw('LOWER(folios.folio) LIKE (?) ', ["%{$folio}%"]);
            }
            if(isset($params['tipo_forma_pago_id']) && $params['tipo_forma_pago_id']) {
                $consulta->where('cxp.tipo_forma_pago_id', $params['tipo_forma_pago_id']);
            }
            if(isset($params['estatus_cuenta_id']) && $params['estatus_cuenta_id']) {
                $consulta->where('cxp.estatus_cuenta_id', $params['estatus_cuenta_id']);
            }
            $consulta->groupBy(
                'cxp.id',
                'cxp.concepto',
                'cxp.importe',
                'cxp.total',
                'cxp.fecha',
                'cxp.tipo_forma_pago_id',
                'folios.folio',
                'numero_proveedor',
                'proveedor',
                'plazo_credito',
                'plazos_credito.cantidad_mes',
                'tipo_forma_pago',
                'estatus_cuenta.nombre',
                'estatus_cuenta.id'
            );
        return $consulta->get();
    }

    public function getByWhere($params) {
        $consulta =  DB::table('cuentas_por_pagar as cxp')
        ->join('folios', 'cxp.folio_id', '=', 'folios.id')
        ->join('proveedor_refacciones', 'cxp.proveedor_id', '=', 'proveedor_refacciones.id')
        ->join('plazos_credito', 'cxp.plazo_credito_id', '=', 'plazos_credito.id')
        ->join('tipo_forma_pago', 'cxp.tipo_forma_pago_id', '=', 'tipo_forma_pago.id')
        ->join('cat_tipo_pago', 'cxp.tipo_pago_id', '=', 'cat_tipo_pago.id')
        ->join('estatus_cuenta', 'cxp.estatus_cuenta_id', '=', 'estatus_cuenta.id')
        ->select(
            'cxp.id',
            'cxp.concepto',
            'cxp.total',
            'cxp.comentarios',
            'cxp.enganche',
            'cxp.tipo_forma_pago_id',
            'cxp.tipo_pago_id',
            'cxp.tasa_interes',
            'cxp.importe',
            'cxp.intereses',
            'cxp.estatus_cuenta_id',
            'cxp.plazo_credito_id',
            'cxp.fecha',
            'cxp.created_at',
            'folios.folio',
            'proveedor_refacciones.proveedor_numero as numero_proveedor',
            'proveedor_refacciones.proveedor_nombre as proveedor',
            'proveedor_refacciones.id AS proveedor_id',
            'plazos_credito.nombre as plazo_credito',
            'plazos_credito.cantidad_mes',
            'tipo_forma_pago.descripcion as tipo_forma_pago',
            'cat_tipo_pago.nombre as tipo_pago',
            'estatus_cuenta.nombre as estatus_cuenta'
        );
        if(isset($params['id']) && $params['id']) {
            $consulta->where('cxp.id', $params['id']);
        }
        if(isset($params['folio_id']) && $params['folio_id']) {
            $consulta->where('cxp.folio_id', $params['folio_id']);
        }
        return $consulta->first();
    }

    public function getKardexPagos($params)
    {
        $consulta =  DB::table('cuentas_por_pagar as cxp')
            ->join('folios', 'cxp.folio_id', '=', 'folios.id')
            ->join('proveedor_refacciones', 'cxp.proveedor_id', '=', 'proveedor_refacciones.id')
            ->join('plazos_credito', 'cxp.plazo_credito_id', '=', 'plazos_credito.id')
            ->join('tipo_forma_pago', 'cxp.tipo_forma_pago_id', '=', 'tipo_forma_pago.id')
            ->join('estatus_cuenta', 'cxp.estatus_cuenta_id', '=', 'estatus_cuenta.id')
            ->join('abonos_por_pagar', 'cxp.id', '=', 'abonos_por_pagar.cuenta_por_pagar_id')
            ->join('cat_estatus_abono', 'abonos_por_pagar.estatus_abono_id', '=', 'cat_estatus_abono.id')
            ->join('cat_tipo_abono', 'abonos_por_pagar.tipo_abono_id', '=', 'cat_tipo_abono.id')
            ->select(
                'cxp.id',
                'cxp.concepto',
                'cxp.total',
                'cxp.tipo_forma_pago_id',
                'folios.folio',
                'proveedor_refacciones.proveedor_numero as numero_proveedor',
                'proveedor_refacciones.proveedor_nombre as proveedor',
                'plazos_credito.nombre as plazo_credito',
                'plazos_credito.cantidad_mes',
                'tipo_forma_pago.descripcion as tipo_forma_pago',
                'abonos_por_pagar.total_abono',
                'abonos_por_pagar.fecha_vencimiento',
                'abonos_por_pagar.total_pago',
                'abonos_por_pagar.fecha_pago',
                'abonos_por_pagar.estatus_abono_id',
                'cat_tipo_abono.nombre as tipo_pago',
                'cat_estatus_abono.nombre as estatus_abono',
                'cat_estatus_abono.id as estatus_abono_id'
            );
            if(isset($params['proveedor_id']) && $params['proveedor_id']) {
                $consulta->where('cxp.proveedor_id', $params['proveedor_id']);
            }
            if(isset($params['folio_id']) && $params['folio_id']) {
                $folio = $params['folio_id'];
                $consulta->whereRaw('LOWER(folios.folio) LIKE (?) ', ["%{$folio}%"]);
            }
            if(isset($params['estatus_abono_id']) && $params['estatus_abono_id']) {
                $consulta->where('abonos_por_pagar.estatus_abono_id', $params['estatus_abono_id']);
            }
            if(isset($params['tipo_forma_pago_id']) && $params['tipo_forma_pago_id']) {
                $consulta->where('cxp.tipo_forma_pago_id', $params['tipo_forma_pago_id']);
            }
            if(isset($params['fecha_inicio']) && !isset($params['fecha_fin'])) {
                $consulta->where('abonos_por_pagar.fecha_vencimiento','>=', $params['fecha_inicio']);
            }
            if(!isset($params['fecha_inicio']) && isset($params['fecha_fin'])) {
                $consulta->where('abonos_por_pagar.fecha_vencimiento','<=', $params['fecha_fin']);
            }
            if(isset($params['fecha_inicio']) && isset($params['fecha_fin'])) {
                $consulta->where('abonos_por_pagar.fecha_vencimiento','>=', $params['fecha_inicio']);
                $consulta->where('abonos_por_pagar.fecha_vencimiento','<=', $params['fecha_fin']);
            }
        $consulta->whereIn('cxp.estatus_cuenta_id', [EstatusCuentaModel::ESTATUS_ATRASADO,EstatusCuentaModel::ESTATUS_PROCESO]);
        $consulta->orderBy('abonos_por_pagar.fecha_vencimiento','asc');
        return $consulta->get();
    }

    public function changeEstatusByFolio($estatus_cuenta_id, $folio_id)
	{ 
        $modelo = $this->modelo->where(CuentasPorPagarModel::FOLIO_ID,$folio_id)->first();
		$modelo->estatus_cuenta_id = $estatus_cuenta_id;
		return $modelo->save();
    }
    
    public function getCuentasEnProcesoPago()
    {
        return $this->modelo->whereIn(CuentasPorPagarModel::ESTATUS_CUENTA_ID, [EstatusCuentaModel::ESTATUS_ATRASADO, EstatusCuentaModel::ESTATUS_PROCESO])->get();
    }

    public function getByFolioId($folio_id)
    {
        return $this->modelo->where(CuentasPorPagarModel::FOLIO_ID, $folio_id)->first();

    }


}
