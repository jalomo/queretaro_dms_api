<?php

namespace App\Servicios\Facturas;

use App\Servicios\Core\ServicioDB;
use App\Models\Facturas\TimbreFiscal;

class ServicioTimbreFiscal extends ServicioDB
{
    public function __construct()
    {
        $this->modelo = new TimbreFiscal();
    }

    public function guardarTimbre($id, array $data)
    {
        return $this->createOrUpdate(
            [TimbreFiscal::UUID => $id],
            $data
        );
    }

    public function existeTimbrefiscal($uuid)
    {
        $data = $this->modelo->where(TimbreFiscal::UUID, $uuid)->get()->toArray();
        return count($data) > 0 ? true : false;
    }

    public function getWhere($columna, $param)
    {
        return $this->modelo->where($columna, $param)->get()->toArray();
        
    }
}
