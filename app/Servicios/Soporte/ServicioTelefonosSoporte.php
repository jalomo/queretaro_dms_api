<?php

namespace App\Servicios\Soporte;

use App\Models\Soporte\TelefonosSoporteModel;
use App\Servicios\Core\ServicioDB;

class ServicioTelefonosSoporte extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'telefonos soporte';
        $this->modelo = new TelefonosSoporteModel();
    }

    public function getReglasGuardar()
    {
        return [
            TelefonosSoporteModel::USUARIO => 'required',
            TelefonosSoporteModel::TELEFONO => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            TelefonosSoporteModel::USUARIO => 'required',
            TelefonosSoporteModel::TELEFONO => 'required'
        ];
    }
}
