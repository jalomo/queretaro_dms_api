<?php

namespace App\Servicios\Soporte;
use App\Models\Soporte\CatStatusTickets;
use App\Servicios\Core\ServicioDB;

class ServicioCatEstatusTickets extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catálogo estatus tickets';
        $this->modelo = new CatStatusTickets();
    }

    public function getReglasGuardar()
    {
        return [
            CatStatusTickets::ESTATUS => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatStatusTickets::ESTATUS => 'required'
        ];
    }
}
