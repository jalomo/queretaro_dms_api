<?php

namespace App\Servicios\Presupuestos;

use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\VentaProductoModel;
use App\Models\Refacciones\VentasRealizadasModel;
use App\Servicios\Inventario\VenderDetallesService;
use Illuminate\Support\Facades\Log;
use RuntimeException;
use Throwable;

class RecepcionRefaccionesEnPresupuesto
{
    protected $message = '';

    public function handle(string $numero_orden, string $no_identificacion, int $cantidad)
    {
        try{
            $venta = VentasRealizadasModel::where('numero_orden', $numero_orden)->first();

            if($venta == null)
            {
                throw new RuntimeException("El numero de orden $numero_orden no se encuentra en el DMS.");
            }

            $producto = ProductosModel::where('no_identificacion', "$no_identificacion")->first();

            $producto_venta = VentaProductoModel::where('venta_id', $venta->id)
                ->where('producto_id', $producto->id)
                ->first();
    
            $inventario = new VenderDetallesService;
            $inventario->id($producto_venta->id);
            $inventario->handle($producto->id, $cantidad, $producto->valor_unitario);

            $this->setMessage("El producto $no_identificacion se desconto satisfactoriamente para la orden: $numero_orden.");
            Log::warning($this->getMessage());

            return true;

        } catch(Throwable $exception){
            $this->setMessage($exception->getMessage());
            Log::warning($this->getMessage());

            return false;
        }
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    public function getMessage()
    {
        return $this->message;
    }
}