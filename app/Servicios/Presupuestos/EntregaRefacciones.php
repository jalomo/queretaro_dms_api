<?php

namespace App\Servicios\Presupuestos;

use App\Models\Refacciones\VentasRealizadasModel;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\VentaProductoModel;
use App\Servicios\Inventario\VenderDetallesService;

class EntregaRefacciones
{
    /**
     * Servicio donde entregamos piezas 
     *
     * @param integer $numero_orden
     * @param string $no_identificacion
     * @param integer $cantidad
     * @return App\Models\Refacciones\VentaProductoModel
     */
    public function handle(int $numero_orden, string $no_identificacion, int $cantidad)
    {
        $producto = ProductosModel::where('no_identificacion', $no_identificacion)->first();
        $venta = VentasRealizadasModel::where('numero_orden', $numero_orden)->first();

        $producto_venta = VentaProductoModel::where('venta_id', $venta->id)
            ->where('producto_id', $producto->id)
            ->first();

        $producto_venta->cantidad_entregada += $cantidad;

        // Descontamos producto del inventario como venta
        $inventario = new VenderDetallesService;
        $inventario->id($producto_venta->id);
        $inventario->handle($producto->id, $cantidad, $producto->valor_unitario);

        return $producto_venta->save();
    }
}