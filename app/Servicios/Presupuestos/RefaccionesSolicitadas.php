<?php

namespace App\Servicios\Presupuestos;

use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\VentasRealizadasModel;
use App\Models\Refacciones\VentaProductoModel;
use Illuminate\Support\Facades\Http;

class RefaccionesSolicitadas
{
    public function send(VentasRealizadasModel $venta)
    {
        $productos = VentaProductoModel::where('venta_id', $venta->id)->where('estatus_producto_orden', '<>', 'B')->get();

        $data = [];

        foreach($productos as $producto)
        {
            $prod = ProductosModel::find($producto->id);

            $data[] = [
                'numero_orden' => $venta->numero_orden,
                'no_identificacion' => $prod->no_identificacion,
                'cantidad' => $producto->cantidad,
            ];
        }

        $response = Http::withBody(json_encode($data), 'json')
            ->post(env('URL_SERVICIOS ') . '/citas/citas_queretaro/conexion/Conexion_DMS/refacciones_solicitadas');

        if($response->successful())
        {
            return true;
        }

        return false;
    }
}