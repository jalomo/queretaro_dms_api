<?php

namespace App\Servicios\Detalles;

use App\Models\Refacciones\VentasRealizadasModel;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;

class RecuperarRefaccionesPresupuesto
{
    /**
     * Recuperar las refacciones cargadas en el presupuesto
     *
     * @param VentasRealizadasModel $venta
     * @return \Illuminate\Support\Collection
     */
    public function get(VentasRealizadasModel $venta): Collection
    {
        $response = Http::get(env('URL_SERVICIOS') . 'citas/citas_queretaro/enlaces/dms/obtener_presupuesto', [
            'id_cita' => $venta->numero_orden,
        ]);

        if($response->object()->status == 'ok' && $response->object()->data->refacciones != false)
        {
            return collect($response->object()->data->refacciones);
        }

        return collect([]);
    }
}