<?php

namespace App\Servicios\Detalles;

use App\Chain\AbstractHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Http;

class OrdenCerrada extends AbstractHandler
{
    public function execute($venta) : ?JsonResponse
    {
        $response = Http::get(env('URL_SERVICIOS') . "api/validarOrdenCerrada/{$venta->numero_orden}");

        if($response->failed() || $response->object()->orden_cerrada == true)
        {
            return response()->json(
                [ 'message' => __("El numero de orden :numero_orden se encuentra cerrada.", ['numero_orden' => $venta->numero_orden])], 
                400, 
                ['X-Message' => __("El numero de orden :numero_orden se encuentra cerrada.", ['numero_orden' => $venta->numero_orden])]
            );
        }

        return null;
    }
}