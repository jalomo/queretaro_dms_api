<?php 

namespace App\Servicios\Detalles;

use App\Chain\AbstractHandler;
use Illuminate\Http\JsonResponse as Response;

class RefaccionesEntregadas extends AbstractHandler
{
    public function execute($venta): ?Response
    {
        $productos = $venta->productos()->where('estatus_producto_orden', '<>', 'B')->get();

        // Si hay alguna refaccion que no este dada de baja que tenga al menos una refaccion
        // sin entregar devolvemos false
        // return $productos->some(fn($producto) => $producto->cantidad != $producto->cantidad_entregada);

        $total_refacciones = $productos->sum(fn($producto) => $producto->cantidad);
        $refacciones_entregadas = $productos->sum(fn($producto) => $producto->cantidad_entregada);

        $sinEntregar = $total_refacciones - $refacciones_entregadas;

        if($sinEntregar > 0)
        {
            return response()->json([
                'message' => __('Aun faltan :numero_refacciones refacciones que deben ser firmadas por el técnico.', ['numero_refacciones' => $sinEntregar]),
                'refacciones_entregadas' => $refacciones_entregadas,
                'refacciones_sin_entregar' => $sinEntregar,
            ], 400);
        }

        return null;
    }
}