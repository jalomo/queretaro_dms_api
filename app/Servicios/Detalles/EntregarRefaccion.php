<?php

namespace App\Servicios\Detalles;

use App\Chain\AbstractHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class EntregarRefaccion extends AbstractHandler
{
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;    
    }

    public function execute($refaccion): ?JsonResponse
    {
        $refaccion->cantidad_entregada += $this->request->cantidad_entregada;
        $refaccion->costo_promedio = $refaccion->producto->costo_promedio;

        if($refaccion->save())
        {
            info("ENTREGA DE REFACCIONES {$refaccion->venta->numero_orden}", ['no_identificacion' => $refaccion->producto->no_identificacion, 'cantidad' => $refaccion->cantidad, 'cantidad_entregada' => $this->request->cantidad_entregada]);

            return response()->json(['message' => __('Se entregaron las refacciones')]);
        }

        return response()->json(['message' => __('Ocurrio un error al guardar las refacciones.')], 400);
    }
}