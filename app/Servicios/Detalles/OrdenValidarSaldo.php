<?php 

namespace App\Servicios\Detalles;

use App\Chain\AbstractHandler;
use App\Models\Refacciones\VentaProductoModel;
use App\Models\Refacciones\VentaServicioModel;
use Illuminate\Http\JsonResponse as Response;
use Illuminate\Support\Facades\DB;

class OrdenValidarSaldo extends AbstractHandler
{
    public function execute($venta): ?Response
    {
        // total refacciones que afectan mano de obra
        $total_refacciones_afectan_mo = $venta->productos()->where('estatus_producto_orden', '<>', 'B')->where('afecta_mano_obra', true)->sum(DB::raw('venta_total'));
        // total refacciones que son prepiking
        $total_refacciones_prepiking = $venta->productos()->where('estatus_producto_orden', '<>', 'B')->where('afecta_paquete', true)->sum(DB::raw('venta_total'));
        // total servicios
        $total_servicios = VentaServicioModel::where('venta_id', $venta->id)->sum('costo_mo');
        // total refacciones con iva
        $total_refacciones = ($total_refacciones_prepiking + $total_refacciones_afectan_mo) * VentaProductoModel::IVA;

        $saldo = $total_servicios - $total_refacciones;

        if($saldo < 0)
        {
            return response()->json(['message' => "El resultado de afectar la mano de obra y paquete a la orden da un saldo negativo: $saldo"], 400);
        }

        return null;
    }
}