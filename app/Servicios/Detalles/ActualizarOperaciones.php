<?php

namespace App\Servicios\Detalles;

use App\Models\Refacciones\VentaServicioModel;
use App\Models\Refacciones\VentasRealizadasModel;
use App\Servicios\Detalles\RecuperarOperacionesServicio;
use App\Servicios\Refacciones\ServicioVentasRealizadas;

class ActualizarOperaciones
{
    /**
     * Ejecutamos la actualizacion de operaciones en el DMS
     *
     * @param VentasRealizadasModel $venta
     * @return VentasRealizadasModel
     */
    public function execute(VentasRealizadasModel $venta)
    {
        // Llamamos al servicio de recuperar operaciones
        $operaciones = (new RecuperarOperacionesServicio)->get($venta->numero_orden);

        $servicios = collect();

        foreach ($operaciones as $item) 
        {
            // Checamos si se encuentra la refaccion u operaccion eliminada
            // si es asi continuamos al siguiente item
            if($item->activo == 0)
            {
                continue;
            }

            $servicios->push(tap(VentaServicioModel::updateOrCreate(
                [
                    'venta_id' =>  $venta->id,
                    'no_identificacion' => trim($item->num_pieza),
                    'nombre_servicio' => $item->descripcion,
                    'id_index_requisicion' => $item->id,
                ],
                [
                    'precio' => (float) $item->costo_mano_obra * 1.16 + (float) $item->costo_refaccion * 1.16,
                    'cantidad' => (int) $item->cantidad,
                    'iva' => ((float) $item->costo_mano_obra + (float) $item->costo_refaccion) * 0.16,
                    'costo_mo' =>  (float) $item->costo_mano_obra * 1.16,
                    'refacciones' => (float) $item->costo_refaccion,
                    'preventivo' => filter_var($item->tipo_preventivo, FILTER_VALIDATE_BOOLEAN)? TRUE : FALSE,
                    'presupuesto' => filter_var($item->presupuesto?? false, FILTER_VALIDATE_BOOLEAN)? TRUE : FALSE,
                    'bodyshop' => filter_var($item->bodyshop?? false, FILTER_VALIDATE_BOOLEAN)? TRUE : FALSE,
                ]
            ), function($servicio) use($venta){
                if($servicio == false)
                    return;

                info("ACTUALIZAR OPERACION {$venta->numero_orden}", $servicio->toArray());
            }));
        }

        // Borramos servicios u operaciones que ya no existen o se eliminaron en servicios.
        $servicios->whenNotEmpty(fn($servicios) => 
            VentaServicioModel::where('venta_id', $venta->id)->whereNotIn('id', $servicios->pluck('id'))->delete()
        );

        // Calculamos el total de la Venta
        (new ServicioVentasRealizadas)->calcularVentaTotal($venta->id);

        return $venta;
    }
}