<?php

namespace App\Servicios\Detalles;

use App\Chain\AbstractHandler;
use App\Models\Refacciones\ProductosModel;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ValidarExistenciaUpdateRefaccion extends AbstractHandler
{
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function execute($refaccion): ?JsonResponse
    {
        $producto = $refaccion->producto()->first();
        $stock = $producto->desglose_producto()->first();
        $cantidad = $this->request->cantidad - $refaccion->cantidad;

        if(is_null($stock))
        {
            return response()->json(['message' => "No hay existencias para esta refacción."], 400);
        }

        if($cantidad > $stock->cantidad_actual)
        {
            return response()->json(['message' => "No hay suficiente existencia para agregar esta refaccion. cantidad en existencia: $stock->cantidad_actual, cantidad solicitada: $cantidad"], 400, [
                'X-Message' => "No hay suficiente existencia para agregar esta refaccion. cantidad en existencia: $stock->cantidad_actual, cantidad solicitada: $cantidad",
            ]);
        }

        return null;
    }
}