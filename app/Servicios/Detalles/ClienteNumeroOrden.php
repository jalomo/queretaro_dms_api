<?php

namespace App\Servicios\Detalles;

use Illuminate\Support\Facades\Http;

class ClienteNumeroOrden
{
    public function get(int $numero_orden) : Object
    {
        // https://mylsa.iplaneacion.com/servicios/apis/citas_queretaro/ordenes/citas/detalle?id_cita=193319
        $response = Http::get('https://mylsa.iplaneacion.com/servicios/apis/citas_queretaro/ordenes/citas/detalle', [
            'id_cita' => $numero_orden,
        ]);

        $data = $response->object();

        if(! empty($data))
        {
            return $data;
        }

        return (object) [
            'cliente_id' => 'S/A',
            'cliente_nombre' => 'SIN RECUPERAR NOMBRE',
            'cliente_apellido_paterno' => 'SIN RECUPERAR APELLIDO',
            'cliente_apellido_materno' => 'SIN RECUPERAR APELLIDO',
        ];
    }
}