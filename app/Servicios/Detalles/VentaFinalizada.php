<?php

namespace App\Servicios\Detalles;

use App\Chain\AbstractHandler;
use App\Models\Refacciones\EstatusVentaModel;
use App\Models\Refacciones\ReVentasEstatusModel;
use Illuminate\Http\JsonResponse as Response;

class VentaFinalizada extends AbstractHandler
{
    public function execute($venta) : ?Response
    {
        ReVentasEstatusModel::create([
            'ventas_id' => $venta->id,
            'estatus_ventas_id' => EstatusVentaModel::ESTATUS_FINALIZADA,
            'user_id' => auth()->user()->id?? null,
        ]);

        $venta->completa = true;

        if($venta->save())
        {
            return response()->json(['message' => 'Se finalizo la venta correctamente.']);
        }

        return response()->json([ 'message' => 'Ocurrio un error al cambiar el estatus.'], 400);
    }
}