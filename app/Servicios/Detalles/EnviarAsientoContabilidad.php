<?php

namespace App\Servicios\Detalles;

use App\Chain\AbstractHandler;
use Illuminate\Http\JsonResponse;
use App\Servicios\Asientos\Ventas\Detalles\Abono as AbonoVenta;
use App\Servicios\Asientos\Ventas\Detalles\Cargo as CargoVenta;

class EnviarAsientoContabilidad extends AbstractHandler
{
    private $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function execute($refaccion): ?JsonResponse
    {
        $total = $refaccion->valor_unitario * $this->request->cantidad_entregada;
        $costo_promedio_total = $refaccion->costo_promedio * $this->request->cantidad_entregada;

        // Abono Poliza V0
        $abono = new AbonoVenta($refaccion->venta, [
            'cliente_id' => $this->request->cliente_id,
            'cliente_nombre' => $this->request->cliente_nombre,
            'cliente_apellido_paterno' => $this->request->cliente_apellido_paterno,
            'cliente_apellido_materno' => $this->request->cliente_apellido_materno,
            'fecha' => optional($this->request->fecha)->format('Y-m-d H:i:s'),
            'num_pieza' => "{$refaccion->producto->no_identificacion} - {$refaccion->producto->descripcion}",
            'costo_promedio' => $costo_promedio_total,
            'vendedor' => $refaccion->vendedor_id,
            'cantidad' => $this->request->cantidad_entregada,
        ]);
        $abono->make($total);

        // Cargo Poliza V0
        $cargo = new CargoVenta($refaccion->venta, [
            'cliente_id' => $this->request->cliente_id,
            'cliente_nombre' => $this->request->cliente_nombre,
            'cliente_apellido_paterno' => $this->request->cliente_apellido_paterno,
            'cliente_apellido_materno' => $this->request->cliente_apellido_materno,
            'fecha' => optional($this->request->fecha)->format('Y-m-d H:i:s'),
            'num_pieza' => "{$refaccion->producto->no_identificacion} - {$refaccion->producto->descripcion}",
            'costo_promedio' => $costo_promedio_total,
            'vendedor' => $refaccion->vendedor_id,
            'cantidad' => $this->request->cantidad_entregada,
        ]);
        $cargo->make($total);

        return null;
    }
}