<?php

namespace App\Servicios\Detalles;

use App\Chain\AbstractHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ValidarCantidadEntregada extends AbstractHandler
{
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;        
    }

    public function execute($refaccion): ?JsonResponse
    {
        if(($refaccion->cantidad - $refaccion->cantidad_entregada - $this->request->cantidad_entregada) < 0)
        {
            return response()->json(['message' => __('La cantidad entregada es mayor a la cantidad asignada en el dms.')], 400);
        }

        return null;
    }
}