<?php

namespace App\Servicios\Detalles;

use Illuminate\Support\Facades\Http;

class RecuperarOperacionesServicio
{
    public function get($numero_orden)
    {
        $data = [];
        $response = Http::get($this->getURL(), [ 'id_cita' => $numero_orden ]);

        if($response->successful() && $response->object()->status == 'ok')
        {    
            info("{$this->getURL()}?id_cita=$numero_orden", (array) $response->object());

            $data = optional($response->object())->data->operaciones;
        }
        
        if(! $response->successful())
        {
            info("{$this->getURL()}?id_cita=$numero_orden", ['body' => $response->body() ]);
        }

        return collect($data);
    }

    public function getURL()
    {
        return env('URL_SERVICIOS') . 'citas/citas_queretaro/enlaces/dms/obtener_operaciones';
    }
}