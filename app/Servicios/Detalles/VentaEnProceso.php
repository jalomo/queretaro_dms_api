<?php

namespace App\Servicios\Detalles;

use App\Chain\AbstractHandler;
use App\Models\Refacciones\EstatusVentaModel;
use App\Models\Refacciones\ReVentasEstatusModel;
use Illuminate\Http\JsonResponse;

class VentaEnProceso extends AbstractHandler
{
    public function execute($venta): ?JsonResponse
    {
        // Reaperturamos la orden de venta
        $venta->completa = false;

        ReVentasEstatusModel::create([
            'ventas_id' => $venta->id,
            'estatus_ventas_id' => EstatusVentaModel::ESTATUS_PROCESO,
            'user_id' => auth()->user()->id,
        ]);

        if($venta->save())
        {
            return response()->json([ 'message' => 'Reapertura de la orden fue satisfactoria.' ]);
        }

        return response()->json(['message' => 'La reaperura de la orden no se pudo realizar'], 400);
    }
}