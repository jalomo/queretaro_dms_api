<?php

namespace App\Servicios\Detalles;

use App\Chain\AbstractHandler;
use App\Models\Refacciones\ProductosModel;
use Illuminate\Http\JsonResponse;

class ValidarExistenciaRefaccion extends AbstractHandler
{
    public function execute($request): ?JsonResponse
    {
        $refaccion = ProductosModel::find($request->producto_id);
        $stock = $refaccion->desglose_producto()->first();

        if(is_null($stock))
        {
            return response()->json(['message' => "No hay existencias para esta refacción."], 400);
        }

        if($request->cantidad > $stock->cantidad_actual)
        {
            return response()->json(['message' => "No hay suficiente existencia para agregar esta refaccion. cantidad en existencia: $stock->cantidad_actual, cantidad solicitada: $request->cantidad"], 400, [
                'X-Message' => "No hay suficiente existencia para agregar esta refaccion. cantidad en existencia: $stock->cantidad_actual, cantidad solicitada: $request->cantidad",
            ]);
        }

        return null;
    }
}