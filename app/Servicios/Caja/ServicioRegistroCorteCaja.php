<?php

namespace App\Servicios\Caja;

use App\Servicios\Core\ServicioDB;
use App\Models\Caja\RegistroCorteCajaModel;
use App\Models\Refacciones\FoliosModel;

class ServicioRegistroCorteCaja extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'cajas';
        $this->modelo = new RegistroCorteCajaModel();
    }

    public function getReglasGuardar()
    {

        return [
            RegistroCorteCajaModel::FECHA_REGISTRO => 'required|date',
            RegistroCorteCajaModel::FOLIO_ID => 'required|numeric|exists:folios,id',
            RegistroCorteCajaModel::DESCRIPCION => 'nullable|string',
            RegistroCorteCajaModel::TIPO_PAGO_ID => 'required|numeric|exists:cat_tipo_pago,id',
            RegistroCorteCajaModel::TOTAL_CANTIDAD_CAJA => 'nullable|numeric',
            RegistroCorteCajaModel::TOTAL_CANTIDAD_REPORTADA => 'required|numeric',
            RegistroCorteCajaModel::FALTANTES => 'nullable|nullable',
            RegistroCorteCajaModel::SOBRANTES => 'nullable|nullable'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            RegistroCorteCajaModel::FECHA_REGISTRO => 'required|date',
            RegistroCorteCajaModel::FOLIO_ID => 'required|numeric|exists:folios,id',
            RegistroCorteCajaModel::DESCRIPCION => 'nullable|string',
            RegistroCorteCajaModel::TIPO_PAGO_ID => 'required|numeric|exists:cat_tipo_pago,id',
            RegistroCorteCajaModel::TOTAL_CANTIDAD_CAJA => 'nullable|numeric',
            RegistroCorteCajaModel::TOTAL_CANTIDAD_REPORTADA => 'required|numeric',
            RegistroCorteCajaModel::FALTANTES => 'nullable|numeric',
            RegistroCorteCajaModel::SOBRANTES => 'nullable|numeric'
        ];
    }


    public function getFilter($request)
    {
        $tbl_principal = RegistroCorteCajaModel::getTableName();
        $tbl_folios = FoliosModel::getTableName();

        $consulta = $this->modelo
            ->select(
                $tbl_principal . '.*',
                $tbl_folios . '.' . FoliosModel::FOLIO,
            )
            ->join($tbl_folios, $tbl_folios . '.' . FoliosModel::ID, $tbl_principal . '.' . RegistroCorteCajaModel::FOLIO_ID);

        if ($request->get(RegistroCorteCajaModel::FECHA_REGISTRO)) {
            $consulta->where($tbl_principal . '.' . RegistroCorteCajaModel::FECHA_REGISTRO, $request->get(RegistroCorteCajaModel::FECHA_REGISTRO));
        }
        return $consulta->get();
    }
}
