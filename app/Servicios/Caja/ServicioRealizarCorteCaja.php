<?php

namespace App\Servicios\Caja;

use App\Servicios\Core\ServicioDB;
use App\Models\Caja\RealizarCorteCajaModel;
use App\Models\Refacciones\CatTipoPagoModel;

class ServicioRealizarCorteCaja extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'cajas';
        $this->modelo = new RealizarCorteCajaModel();
    }

    public function getReglasGuardar()
    {

        return [
            RealizarCorteCajaModel::FECHA_REGISTRO => 'required|date',
            RealizarCorteCajaModel::NUMERO_DOCUMENTO => 'nullable|string',
            RealizarCorteCajaModel::DESCRIPCION => 'nullable|string',
            RealizarCorteCajaModel::TIPO_PAGO_ID => 'required|numeric|exists:cat_tipo_pago,id',
            RealizarCorteCajaModel::CANTIDAD => 'required|numeric',
            RealizarCorteCajaModel::CANTIDAD_SISTEMA => 'nullable|numeric',
            RealizarCorteCajaModel::FALTANTES => 'nullable|nullable',
            RealizarCorteCajaModel::SOBRANTES => 'nullable|nullable',
            RealizarCorteCajaModel::AUTORIZADO => 'nullable|nullable'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            RealizarCorteCajaModel::FECHA_REGISTRO => 'required|date',
            RealizarCorteCajaModel::NUMERO_DOCUMENTO => 'nullable|string',
            RealizarCorteCajaModel::DESCRIPCION => 'nullable|string',
            RealizarCorteCajaModel::TIPO_PAGO_ID => 'required|numeric|exists:cat_tipo_pago,id',
            RealizarCorteCajaModel::CANTIDAD => 'required|numeric',
            RealizarCorteCajaModel::CANTIDAD_SISTEMA => 'nullable|numeric',
            RealizarCorteCajaModel::FALTANTES => 'nullable|nullable',
            RealizarCorteCajaModel::SOBRANTES => 'nullable|nullable',
            RealizarCorteCajaModel::AUTORIZADO => 'nullable|nullable'
        ];
    }


    public function getFilter($request)
    {
        $tbl_principal = RealizarCorteCajaModel::getTableName();
        $tbl_tipo_pago = CatTipoPagoModel::getTableName();

        $consulta = $this->modelo
            ->select(
                $tbl_principal . '.*',
                $tbl_tipo_pago . '.' . CatTipoPagoModel::NOMBRE . ' as tipo_pago',
            )
            ->join($tbl_tipo_pago, $tbl_tipo_pago . '.' . CatTipoPagoModel::ID, $tbl_principal . '.' . RealizarCorteCajaModel::TIPO_PAGO_ID);

        if ($request->get(RealizarCorteCajaModel::FECHA_REGISTRO)) {
            $consulta->where($tbl_principal . '.' . RealizarCorteCajaModel::FECHA_REGISTRO, $request->get(RealizarCorteCajaModel::FECHA_REGISTRO));
        }
        return $consulta->get();
    }
}
