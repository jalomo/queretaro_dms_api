<?php

namespace App\Servicios\Caja;

use App\Servicios\Core\ServicioDB;
use App\Models\Caja\CatalogoBancosModel;

class ServicioCatalogoBancos extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'bancos';
        $this->modelo = new CatalogoBancosModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatalogoBancosModel::NOMBRE => 'required|string|regex:/^[\pL\s]+$/u|max:50|unique:' . $this->modelo->getTable() . ',nombre'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatalogoBancosModel::NOMBRE => 'required|string|regex:/^[\pL\s]+$/u|max:50|unique:' . $this->modelo->getTable() . ',nombre'
        ];
    }
}
