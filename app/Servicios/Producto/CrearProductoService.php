<?php

namespace App\Servicios\Producto;

use App\Models\Refacciones\CatalogoUbicacionProductoModel;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\StockProductosModel;
use Exception;

class CrearProductoService 
{
    protected $grupo = '';
    protected $prefijo = '';
    protected $basico = '';
    protected $sufijo = '';
    protected $no_identificacion = '';
    protected $descripcion = '';
    protected $cantidad = 0;
    protected $ubicacion = 'GEN.';
    protected $unidad = 'PIEZA';
    protected $existencia = false;
    protected $inventariable = false;
    protected $valor_unitario = 0;
    protected $costo_promedio = null;
    protected $precio_factura = null;
    protected $clave_prod_serv = '';
    protected $clave_unidad = '';
    protected $taller_id = 1;
    protected $almacen_id = 1;
    protected $precio_id = 1;

    protected function boot(array &$props)
    {
        foreach($props as $key => $prop)
        {
            if(isset($this->{$key}))
            {
                $this->{$key} = $prop;
            }
        }
    }

    public static function create(array $props = [])
    {
        return (new static)->make($props);
    }

    public function make(array $props = [])
    {
        if(empty($props))
            throw new Exception('No se pasaron atributos para crear el producto.');

        $this->boot($props);

        if(empty($this->no_identificacion))
            $this->no_identificacion = $this->grupo . $this->prefijo .$this->basico . $this->sufijo;

        if(! empty($this->ubicacion))
            $this->ubicacion = CatalogoUbicacionProductoModel::firstOrCreate([
                'nombre' => $this->ubicacion
            ]);

        $producto = ProductosModel::firstOrCreate([
            'no_identificacion' => $this->clean_grupo(
                mb_strtoupper($this->escape($this->no_identificacion))
            )
        ],[
            'gpo' => $this->escape($this->grupo),
            'prefijo' => $this->escape($this->prefijo),
            'basico' => $this->escape($this->basico),
            'sufijo' => $this->escape($this->sufijo),
            'descripcion' => $this->descripcion,
            'ubicacion_producto_id' => $this->ubicacion->id,
            'unidad' => $this->unidad,
            'existencia' => $this->existencia,
            'cantidad' => $this->cantidad,
            'inventariable' => $this->inventariable,
            'valor_unitario' => $this->valor_unitario,
            'costo_promedio' => $this->costo_promedio?? $this->valor_unitario,
            'precio_factura' => $this->precio_factura?? $this->valor_unitario,
            'almacen_id' => $this->almacen_id,
            'taller_id' => $this->taller_id,
            'precio_id' => $this->precio_id,
        ]);

        if(! empty($this->clave_prod_serv))
            $producto->clave_prod_serv = $this->clave_prod_serv;

        if(! empty($this->clave_unidad))
            $producto->clave_unidad = $this->clave_unidad;
        
        $stock = StockProductosModel::firstOrCreate([
            'producto_id' => $producto->id,
        ],[
            StockProductosModel::CANTIDAD_ACTUAL => $this->cantidad, //$existencia,
            StockProductosModel::CANTIDAD_ALMACEN_PRIMARIO => $this->cantidad, // $existencia,
        ]);

        if($this->cantidad > 0)
        {
            $producto->cantidad = $this->cantidad;
            // Actualizamos la cantidad
            $stock->cantidad_actual = $this->cantidad;
            $stock->cantidad_almacen_primario = $this->cantidad;
            $stock->save();
        }

        if(! is_null($this->costo_promedio))
            $producto->costo_promedio = $this->costo_promedio;

        return tap($producto)->save();
    }

    public function escape($str)
    {
        $str = mb_detect_encoding($str, 'UTF-8', true) == false? mb_convert_encoding($str, 'UTF-8') : $str;
        // Escapamos los caracteres espacios, puntos y guiones
        return  preg_replace('/[^A-Za-z0-9]+/', '', $str);
    }

    public function clean_grupo($no_identificacion)
    {
        // Encontramos el grupo FM, MC, NF
        if(! empty($this->grupo))
        {
            // Limpiamos refacciones que puedan traer el grupo FM,MC,NF
            // Ejemplo FMMPM14, FMMPM14 a MPM14
            $no_identificacion =  preg_replace('/^FM|^MC|^NF|^VA|^LL/', '', mb_strtoupper($no_identificacion));

            return "{$this->grupo}$no_identificacion";
        }

        return $no_identificacion;
    }
}