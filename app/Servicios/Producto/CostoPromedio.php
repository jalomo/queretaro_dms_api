<?php

namespace App\Servicios\Producto;

use App\Models\Refacciones\ProductosModel as Producto;
use App\Models\Refacciones\CostoPromedio as Historial;

class CostoPromedio
{
    protected $producto, $cantidad, $valor_unitario, $costo_promedio;

    public function __construct(Producto $producto, int $cantidad, float $valor_unitario)
    {
        $this->producto = $producto;
        $this->cantidad = $cantidad;
        $this->valor_unitario = $valor_unitario;
    }

    public function setProducto(Producto $producto): CostoPromedio
    {
        $this->producto = $producto;

        return $this;
    }

    public function setCantidad(int $cantidad): CostoPromedio
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    public function setValorUnitario(float $valor_unitario): CostoPromedio
    {
        $this->valor_unitario = $valor_unitario;

        return $this;
    }

    /**
     * Set the value of costo_promedio
     *
     * @return  self
     */ 
    public function setCostoPromedio($costo_promedio): CostoPromedio
    {
        $this->costo_promedio = $costo_promedio;

        return $this;
    }

    public function entrada()
    {
        // Obtenemos el stock
        $stock = $this->producto->desglose_producto()->first();

        // Formula obtener costo promedio refacciones
        // (Existencia * Costo Promedio en Inventario) + (Cantidad Refacciones Entrada * Valor Unitario) / (Existencia + Cantidad Refacciones Entrada)
        $costo_inventario_actual = $this->producto->costo_promedio * $stock->cantidad_actual;

        $costo_inventario_movimiento = $this->cantidad * $this->valor_unitario;

        $costo_promedio = ($costo_inventario_actual + $costo_inventario_movimiento) / ($this->cantidad + $stock->cantidad_actual);
        
        $valor_unitario = $costo_promedio / 0.60;

        // Guardamos el historial del cambio
        $historial = Historial::create([
            'producto_id' => $this->producto->id,
            'movimiento_id' => 1, // Entrada
            'existencia' => $stock->cantidad_actual,
            'entrada' => $this->cantidad,
            'costo_existencia' => $costo_inventario_actual,
            'costo_movimiento' => $costo_inventario_movimiento,
            'costo_promedio_existencia' => $this->producto->costo_promedio,
            'costo_promedio_movimiento' => $this->valor_unitario,
            'costo_promedio' => $costo_promedio,
            'valor_unitario_existencia' => $this->producto->valor_unitario,
            'valor_unitario_movimiento' => $valor_unitario
        ]);

        $this->producto->costo_promedio = $costo_promedio;

        return tap($this->producto)->save();
    }

    public function salida()
    {
        // Obtenemos el stock
        $stock = $this->producto->desglose_producto()->first();

        // Formula obtener costo promedio al devolver refacciones
        // (Existencia * Costo Promedio en Inventario) - (Cantidad Refacciones Salida * Valor Unitario) / (Existencia - Cantidad Refacciones Salida)
        $costo_inventario_actual = $this->producto->costo_promedio * $stock->cantidad_actual;

        $costo_inventario_salida = $this->cantidad * $this->valor_unitario;
        
        $costo_promedio = ($stock->cantidad_actual - $this->cantidad) > 0?  
            ($costo_inventario_actual - $costo_inventario_salida) / ($stock->cantidad_actual - $this->cantidad) : 0;

        $valor_unitario = $costo_promedio / 0.60;

        // Guardamos el historial del cambio
        $historial = Historial::create([
            'producto_id' => $this->producto->id,
            'movimiento_id' => 2, // Salida
            'existencia' => $stock->cantidad_actual,
            'entrada' => $this->cantidad,
            'costo_existencia' => $costo_inventario_actual,
            'costo_movimiento' => $costo_inventario_salida,
            'costo_promedio_existencia' => $this->producto->costo_promedio,
            'costo_promedio_movimiento' => $this->valor_unitario,
            'costo_promedio' => $costo_promedio,
            'valor_unitario_existencia' => $this->producto->valor_unitario,
            'valor_unitario_movimiento' => $valor_unitario,
        ]);

        $this->producto->costo_promedio = $costo_promedio;

        return tap($this->producto)->save();
    }

    public function catalogo()
    {
        // Obtenemos el stock
        $stock = $this->producto->desglose_producto()->first();

        // Formula obtener costo promedio refacciones
        // (Existencia * Costo Promedio en Inventario) + (Cantidad Refacciones Entrada * Valor Unitario) / (Existencia + Cantidad Refacciones Entrada)
        $costo_inventario_actual = $this->producto->costo_promedio * $stock->cantidad_actual;

        $costo_inventario_movimiento = $this->cantidad * $this->valor_unitario;

        $costo_promedio = $this->costo_promedio;

        // Guardamos el historial del cambio
        $historial = Historial::create([
            'producto_id' => $this->producto->id,
            'movimiento_id' => Historial::CATALOGO, // Movimiento Directo en el Catalogo
            'existencia' => $stock->cantidad_actual,
            'entrada' => $this->cantidad,
            'costo_existencia' => $costo_inventario_actual,
            'costo_movimiento' => $costo_inventario_movimiento,
            'costo_promedio_existencia' => $this->producto->costo_promedio,
            'costo_promedio_movimiento' => $this->valor_unitario,
            'costo_promedio' => $costo_promedio,
            'valor_unitario_existencia' => $this->producto->valor_unitario,
            'valor_unitario_movimiento' => $this->valor_unitario
        ]);

        $this->producto->costo_promedio = $costo_promedio;

        return tap($this->producto)->save();
    }
}