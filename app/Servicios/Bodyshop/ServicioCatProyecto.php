<?php
namespace App\Servicios\Bodyshop;
use App\Models\Bodyshop\CatProyectoModel;
use App\Servicios\Core\ServicioDB;

class ServicioCatProyecto extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catálogo proyectos bodyshop';
        $this->modelo = new CatProyectoModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatProyectoModel::PROYECTO => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatProyectoModel::PROYECTO => 'required'
        ];
    }
}


