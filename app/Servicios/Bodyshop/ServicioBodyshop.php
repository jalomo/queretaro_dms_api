<?php

namespace App\Servicios\Bodyshop;

use App\Models\Bodyshop\BodyshopModel;
use App\Servicios\Core\ServicioDB;

class ServicioBodyshop extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catálogo bodyshop';
        $this->modelo = new BodyshopModel();
    }

    public function getReglasGuardar()
    {
        return [
            BodyshopModel::ID_PROYECTO => 'required',
            BodyshopModel::DESCRIPCION => 'required',
            BodyshopModel::NUMERO_SINIESTRO => 'required',
            BodyshopModel::NUMERO_POLIZA => 'required',
            BodyshopModel::ID_ANIO => 'required',
            BodyshopModel::PLACAS => 'required',
            BodyshopModel::ID_COLOR => 'required',
            BodyshopModel::ID_MODELO => 'required',
            BodyshopModel::SERIE => 'required',
            BodyshopModel::CITA => 'nullable',
            BodyshopModel::ID_ASESOR => 'required',
            BodyshopModel::FECHA_CITA => 'required',
            BodyshopModel::HORA_CITA => 'required',
            BodyshopModel::ID_STATUS => 'required',
            BodyshopModel::FECHA_INICIO => 'required',
            BodyshopModel::FECHA_FIN => 'required',
            BodyshopModel::TIPO_GOLPE => 'required',
            BodyshopModel::COMENTARIOS => 'required',
        ];
    }
    public function getReglasUpdate()
    {
        return [
            BodyshopModel::ID_PROYECTO => 'required',
            BodyshopModel::DESCRIPCION => 'required',
            BodyshopModel::NUMERO_SINIESTRO => 'required',
            BodyshopModel::NUMERO_POLIZA => 'required',
            BodyshopModel::ID_ANIO => 'required',
            BodyshopModel::PLACAS => 'required',
            BodyshopModel::ID_COLOR => 'required',
            BodyshopModel::ID_MODELO => 'required',
            BodyshopModel::SERIE => 'required',
            BodyshopModel::CITA => 'required',
            BodyshopModel::ID_ASESOR => 'required',
            BodyshopModel::FECHA_CITA => 'required',
            BodyshopModel::HORA_CITA => 'required',
            BodyshopModel::ID_STATUS => 'required',
            BodyshopModel::FECHA_INICIO => 'required',
            BodyshopModel::FECHA_FIN => 'required',
            BodyshopModel::TIPO_GOLPE => 'required',
            BodyshopModel::COMENTARIOS => 'required',
        ];
    }
    //Obtener los horarios que tiene el asesor ocupados
    public function getHorariosOcupadosCita($parametros)
    {
        $horarios = $this->modelo
            ->where(BodyshopModel::ID_ASESOR, $parametros[BodyshopModel::ID_ASESOR])
            ->where(BodyshopModel::FECHA_CITA, $parametros[BodyshopModel::FECHA_CITA])
            ->select(BodyshopModel::HORA_CITA)
            ->get();

        $array_horarios = array();
        foreach ($horarios as $key => $value) {
            $array_horarios[] = $value[BodyshopModel::HORA_CITA];
        }
        return $array_horarios;
    }
    //Función para saber si ya existe la cita
    public function validarReservacion($parametros){
        return $this->modelo->where(BodyshopModel:: FECHA_CITA,$parametros[BodyshopModel:: FECHA_CITA])
                    ->where(BodyshopModel:: HORA_CITA,$parametros[BodyshopModel:: HORA_CITA])
                    ->where(BodyshopModel::ID,'!=',$parametros[BodyshopModel::ID])
                    ->get();
    }
}
