<?php

namespace App\Servicios\Refacciones;

use App\Models\Refacciones\CatCfdiModel;
use App\Servicios\Core\ServicioDB;
use DB;

class ServicioCatalogoCfdi extends ServicioDB
{

    public function __construct()
    {
        $this->recurso = 'cfdi';
        $this->modelo = new CatCfdiModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatCfdiModel::CLAVE => 'required',
            CatCfdiModel::DESCRIPCION => 'required',
            CatCfdiModel::PERSONA_FISICA => 'required|boolean',
            CatCfdiModel::PERSONA_MORAL => 'required|boolean'


        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatCfdiModel::CLAVE => 'required',
            CatCfdiModel::DESCRIPCION => 'required',
            CatCfdiModel::PERSONA_FISICA => 'required|boolean',
            CatCfdiModel::PERSONA_MORAL => 'required|boolean'
        ];
    }

    public function buscarTodos() {
        return $this->modelo->select(
            CatCfdiModel::getTableName() . '.*',
            DB::raw('CONCAT(descripcion) as nombre'))->get();
    }
}
