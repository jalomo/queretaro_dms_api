<?php

namespace App\Servicios\Refacciones;

use App\Models\Refacciones\SucursalesModel;
use App\Servicios\Core\ServicioDB;

class ServicioSucursales extends ServicioDB
{

    public function __construct()
    {
        $this->recurso = 'sucursales';
        $this->modelo = new SucursalesModel();
    }

    public function getReglasGuardar()
    {
        return [
            SucursalesModel::NOMBRE => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            SucursalesModel::NOMBRE => 'required'
        ];
    }
}
