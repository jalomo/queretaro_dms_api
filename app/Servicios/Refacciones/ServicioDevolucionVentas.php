<?php

namespace App\Servicios\Refacciones;

use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Core\ServicioDB;
use App\Models\Contabilidad\CatalogoProcesosModel;
use App\Models\Refacciones\ClientesModel;
use App\Models\Refacciones\DevolucionVentaProductoModel;
use App\Models\Refacciones\DevolucionVentasModel;
use App\Models\Refacciones\EstatusCompra;
use App\Models\Refacciones\VentasRealizadasModel;
use App\Models\Refacciones\EstatusVentaModel;
use App\Models\Refacciones\ReVentasEstatusModel;
use App\Models\Refacciones\Traspasos;
use App\Models\Refacciones\FoliosModel;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\VentaProductoModel;
use App\Models\Refacciones\VendedorModel;
use App\Models\CuentasPorCobrar\EstatusCuentaModel;
use App\Servicios\CuentasPorCobrar\ServicioCuentaPorCobrar;
use Illuminate\Support\Facades\DB;


class ServicioDevolucionVentas  extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'devolucion ventas';
        $this->modelo = new DevolucionVentasModel();
        $this->modeloDevolucionProducto = new DevolucionVentaProductoModel();
        $this->servicioVentaProducto = new ServicioVentaProducto();
        $this->servicioVenta = new ServicioVentasRealizadas();
        $this->servicioFolios = new ServicioFolios();
        $this->servicioReVentasEstatus = new ServicioReVentasEstatus();
        $this->servicioCuentaPorCobrar = new ServicioCuentaPorCobrar();
        $this->modeloVentaRealizada = new VentasRealizadasModel();
        $this->modeloCliente = new ClientesModel();
    }

    public function getReglasGuardar()
    {
        return [

            DevolucionVentasModel::CLIENTE_ID => 'required|exists:clientes,id',
            DevolucionVentasModel::FOLIO_ID => 'required',
            DevolucionVentasModel::OBSERVACIONES => 'required',
            DevolucionVentasModel::VENTA_ID => 'required|numeric',
            DevolucionVentasModel::VENDEDOR_ID => 'required|numeric',
            DevolucionVentasModel::TOTAL_DEVOLUCION => 'required|numeric',
        ];
    }
    public function getReglasUpdate()
    {
        return [

            DevolucionVentasModel::CLIENTE_ID => 'exists:clientes,id',
            DevolucionVentasModel::FOLIO_ID => 'required|exists:folios,id',
            DevolucionVentasModel::VENTA_ID => 'required|numeric',
            DevolucionVentasModel::OBSERVACIONES => 'required',
            DevolucionVentasModel::VENDEDOR_ID => 'required|numeric',
            DevolucionVentasModel::TOTAL_DEVOLUCION => 'required|numeric',
        ];
    }

    public function getReglasBusquedaTraspaso()
    {
        return [
            Traspasos::FECHA_INICIO => 'nullable|date',
            Traspasos::FECHA_FIN => 'nullable|date',
            VentasRealizadasModel::FOLIO_ID => 'nullable|exists:folios,id',
        ];
    }

    public function devolucionServicio($parametros)
    {
        $venta_by_orden = $this->servicioVenta
            ->getWhere(VentasRealizadasModel::NUMERO_ORDEN, $parametros[VentasRealizadasModel::NUMERO_ORDEN])
            ->first();
        
        $venta_id = $venta_by_orden->id;
        $folio_id = $venta_by_orden->folio_id;
        $cliente_id = $venta_by_orden->cliente_id;
        $venta_total = $venta_by_orden->venta_total;
        $folio = $this->servicioFolios->generarFolio(CatalogoProcesosModel::PROCESO_DEVOLUCION_VENTA);

        $venta_productos = $this->servicioVentaProducto->getWhere(VentaProductoModel::VENTA_ID, $venta_id);
        foreach ($venta_productos as $key => $item) {
            $this->modeloDevolucionProducto->create([
                DevolucionVentaProductoModel::FOLIO_ID => $folio->id,
                DevolucionVentaProductoModel::VENTA_ID => $venta_id,
                DevolucionVentaProductoModel::PRODUCTO_ID => $item->producto_id,
                DevolucionVentaProductoModel::PRECIO_ID => $item->precio_id,
                DevolucionVentaProductoModel::CANTIDAD => $item->cantidad
            ]);
        }
       
        $this->servicioCuentaPorCobrar->changeEstatusByFolio(EstatusCuentaModel::ESTATUS_CANCELADO, $folio_id);
        $this->servicioReVentasEstatus->storeReVentasEstatus([
            ReVentasEstatusModel::VENTA_ID => $venta_id,
            ReVentasEstatusModel::ESTATUS_VENTA_ID => EstatusVentaModel::ESTATUS_DEVOLUCION
        ]);

        //marcamos como cancelado el la cuenta por cobrar.
        return $this->crear([
            DevolucionVentasModel::CLIENTE_ID => $cliente_id,
            DevolucionVentasModel::FOLIO_ID => $folio->id,
            DevolucionVentasModel::VENTA_ID => $venta_id,
            DevolucionVentasModel::TOTAL_DEVOLUCION => $venta_total,
            DevolucionVentasModel::VENDEDOR_ID => $parametros[DevolucionVentasModel::VENDEDOR_ID],
            DevolucionVentasModel::OBSERVACIONES => $parametros[DevolucionVentasModel::OBSERVACIONES],
        ]);
    }

    public function generarDevolucion($data)
    {
        try {
            $folio = $this->servicioFolios->generarFolio(CatalogoProcesosModel::PROCESO_DEVOLUCION_VENTA);
            $venta_productos = $this->servicioVentaProducto->getDetalleVentaByFolio($data[DevolucionVentasModel::FOLIO_ID]);

            foreach ($venta_productos as $key => $items) {
                $productos = [
                    DevolucionVentaProductoModel::FOLIO_ID => $folio->id,
                    DevolucionVentaProductoModel::VENTA_ID => $data[DevolucionVentaProductoModel::VENTA_ID],
                    DevolucionVentaProductoModel::PRODUCTO_ID => $items->producto_id,
                    DevolucionVentaProductoModel::PRECIO_ID => $items->precio_id,
                    DevolucionVentaProductoModel::CANTIDAD => $items->cantidad
                ];
                $this->modeloDevolucionProducto->create($productos);

            }
            $re_estatus_venta = [
                ReVentasEstatusModel::VENTA_ID => $data[DevolucionVentaProductoModel::VENTA_ID],
                ReVentasEstatusModel::ESTATUS_VENTA_ID => EstatusVentaModel::ESTATUS_DEVOLUCION
            ];

            $data['folio_id'] = $folio->id;
            $this->servicioReVentasEstatus->storeReVentasEstatus($re_estatus_venta);
            //marcamos como cancelado el la cuenta por cobrar.

            return $this->crear($data);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getByIdDevolucion($id)
    {
        $data = $this->modelo->where(DevolucionVentaProductoModel::ID, $id)->get();
        foreach ($data as $key => $item) {
            $data[$key]['cliente'] = $this->modeloCliente->find($item[DevolucionVentasModel::CLIENTE_ID]);
            $data[$key]['venta'] = $this->modeloVentaRealizada->find($item[DevolucionVentasModel::VENTA_ID]);
        }

        return $data;
    }

    public function getByIdVenta($id)
    {
        $data = $this->modelo->where(DevolucionVentaProductoModel::VENTA_ID, $id)->get();
        foreach ($data as $key => $item) {
            $data[$key]['cliente'] = $this->modeloCliente->find($item[DevolucionVentasModel::CLIENTE_ID]);
            $data[$key]['venta'] = $this->modeloVentaRealizada->find($item[DevolucionVentasModel::VENTA_ID]);
        }

        return $data;
    }

    public function getDevolucionMostradorByFechas($request)
    {
        $tableDevolucionVentas = DevolucionVentasModel::getTableName();
        $tableVentas = VentasRealizadasModel::getTableName();
        $tableFolios = FoliosModel::getTableName();
        $tableVentaProducto = VentaProductoModel::getTableName();
        $tableProducto = ProductosModel::getTableName();
        $tableReVentaEstatus = ReVentasEstatusModel::getTableName();
        $tableEstatusVenta = EstatusVentaModel::getTableName();
        $tableVendedor = VendedorModel::getTableName();
        $query = DB::table($tableDevolucionVentas)
            ->join($tableVentas, $tableVentas . '.' . VentasRealizadasModel::ID, '=', $tableDevolucionVentas . '.' . DevolucionVentasModel::VENTA_ID)
            ->join($tableVentaProducto, $tableVentaProducto . '.' . VentaProductoModel::VENTA_ID, '=', $tableVentas . '.' . VentasRealizadasModel::ID)
            ->join($tableFolios, $tableVentas . '.' . VentasRealizadasModel::FOLIO_ID, '=', $tableFolios . '.' . FoliosModel::ID)
            ->join($tableProducto, $tableVentaProducto . '.' . VentaProductoModel::PRODUCTO_ID, '=', $tableProducto . '.' . ProductosModel::ID)
            ->join($tableReVentaEstatus, $tableReVentaEstatus . '.' . ReVentasEstatusModel::VENTA_ID, '=', $tableVentas . '.' . VentasRealizadasModel::ID)
            ->join($tableEstatusVenta, $tableEstatusVenta . '.' . EstatusVentaModel::ID, '=', $tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID)
            ->join($tableVendedor, $tableVendedor . '.' . VendedorModel::ID, '=', $tableDevolucionVentas . '.' . DevolucionVentasModel::VENDEDOR_ID)
            ->select(
                'ventas.id',
                'ventas.venta_total',
                'ventas.almacen_id',
                'folios.folio',
                'venta_producto.cantidad',
                'venta_producto.venta_total as venta_total_producto',
                'venta_producto.valor_unitario',
                'producto.no_identificacion',
                'producto.descripcion',
                'producto.unidad',
                'ventas.created_at',
                'devolucion_venta.observaciones',
                'devolucion_venta.total_devolucion',
                'vendedor.nombre as vendedor',
                $tableEstatusVenta . '.' . EstatusVentaModel::NOMBRE . ' as estatusVenta',
                $tableEstatusVenta . '.' . EstatusVentaModel::ID . ' as estatusId'
            );
            if ($request->get(Traspasos::FECHA_INICIO) && $request->get(Traspasos::FECHA_FIN)) {
                $query->whereBetween('ventas.created_at', [$request->get(Traspasos::FECHA_INICIO) . ' 00:00:00', $request->get(Traspasos::FECHA_FIN) . ' 23:59:59']);
            }
            if ($request->get(VentasRealizadasModel::FOLIO_ID)) {
                $query->where($tableVentas . '.' . VentasRealizadasModel::FOLIO_ID, $request->get(VentasRealizadasModel::FOLIO_ID));
            }
            $query->where($tableReVentaEstatus . '.' . ReVentasEstatusModel::ACTIVO, true)->get();
        return [
            'data' => $query->get()
        ];
    }

    public function getTotalesDevolucionMostradorByFechas($request)
    {
        $tableDevolucionVentas = DevolucionVentasModel::getTableName();
        $tableVentas = VentasRealizadasModel::getTableName();
        $tableVentaProducto = VentaProductoModel::getTableName();
        $tableProducto = ProductosModel::getTableName();
        $tableReVentaEstatus = ReVentasEstatusModel::getTableName();
        $query = DB::table($tableDevolucionVentas)
            ->join($tableVentas, $tableVentas . '.' . VentasRealizadasModel::ID, '=', $tableDevolucionVentas . '.' . DevolucionVentasModel::VENTA_ID)
            ->join($tableVentaProducto, $tableVentaProducto . '.' . VentaProductoModel::VENTA_ID, '=', $tableVentas . '.' . VentasRealizadasModel::ID)
            ->join($tableProducto, $tableVentaProducto . '.' . VentaProductoModel::PRODUCTO_ID, '=', $tableProducto . '.' . ProductosModel::ID)
            ->join($tableReVentaEstatus, $tableReVentaEstatus . '.' . ReVentasEstatusModel::VENTA_ID, '=', $tableVentas . '.' . VentasRealizadasModel::ID)
            ->select(
                DB::raw('count(ventas.id) as total_ventas'),
                DB::raw('sum(venta_producto.cantidad) as total_cantidad'),
                DB::raw('sum(venta_producto.venta_total) as sum_venta_total'),
                DB::raw('sum(venta_producto.valor_unitario) as sum_valor_unitario')
            );
            if ($request->get(Traspasos::FECHA_INICIO) && $request->get(Traspasos::FECHA_FIN)) {
                $query->whereBetween('ventas.created_at', [$request->get(Traspasos::FECHA_INICIO) . ' 00:00:00', $request->get(Traspasos::FECHA_FIN) . ' 23:59:59']);
            }
            if ($request->get(VentasRealizadasModel::FOLIO_ID)) {
                $query->where($tableVentas . '.' . VentasRealizadasModel::FOLIO_ID, $request->get(VentasRealizadasModel::FOLIO_ID));
            }
            $query->where($tableReVentaEstatus . '.' . ReVentasEstatusModel::ACTIVO, true);
            $query->whereIn($tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID, array(EstatusVentaModel::ESTATUS_DEVOLUCION));
            return $query->first();

    }
}
