<?php

namespace App\Servicios\Refacciones;

use App\Models\Refacciones\CatalogoMarcasModel;
use App\Servicios\Core\ServicioDB;

class ServicioMarcas extends ServicioDB
{

    public function __construct()
    {
        $this->recurso = 'marcas';
        $this->modelo = new CatalogoMarcasModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatalogoMarcasModel::NOMBRE => 'required'
        ];
    }

    public function getReglasUpdate()
    {
        return [
            CatalogoMarcasModel::NOMBRE => 'required'
        ];
    }

    public function getReglasBusquedaMarca()
    {
        return [
            CatalogoMarcasModel::NOMBRE => 'required'
        ];
    }

    public function searchIdMarca($request)
    {
        $data = $this->modelo->where(CatalogoMarcasModel::NOMBRE, $request->get(CatalogoMarcasModel::NOMBRE))
            ->select(CatalogoMarcasModel::ID)->limit(1)->get();

        return $data;
    }

    public function getCatalogo()
    {
        return  $this->modelo->whereIn(CatalogoMarcasModel::ID, [1, 33])
            ->select(CatalogoMarcasModel::ID, CatalogoMarcasModel::NOMBRE)->get();
    }
}
