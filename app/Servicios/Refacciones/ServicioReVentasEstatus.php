<?php

namespace App\Servicios\Refacciones;

use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\ReVentasEstatusModel;
use App\Models\Refacciones\EstatusVentaModel;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Refacciones\VentaProductoModel;
use App\Events\Refacciones\DecrementStockEvent;
use App\Events\Refacciones\IncrementStockEvent;

class ServicioReVentasEstatus extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 're ventas estatus';
        $this->modelo = new ReVentasEstatusModel();
    }

    public function getReglasGuardar()
    {
        return [
            ReVentasEstatusModel::VENTA_ID => 'required|numeric|exists:ventas,id',
            ReVentasEstatusModel::ESTATUS_VENTA_ID => 'required|numeric|exists:estatus_venta,id',
            ReVentasEstatusModel::ACTIVO => 'nullable|boolean',
            ReVentasEstatusModel::USER_ID => 'nullable',
        ];
    }
    public function getReglasUpdate()
    {
        return [
            ReVentasEstatusModel::VENTA_ID => 'required|numeric|exists:ventas,id',
            ReVentasEstatusModel::ESTATUS_VENTA_ID => 'required|numeric|exists:estatus_venta,id',
            ReVentasEstatusModel::ACTIVO => 'required|boolean',
            ReVentasEstatusModel::USER_ID => 'nullable',
        ];
    }

    public function storeReVentasEstatus($request)
    {
        $params = [   
            ReVentasEstatusModel::VENTA_ID => $request[ReVentasEstatusModel::VENTA_ID],
            ReVentasEstatusModel::ESTATUS_VENTA_ID => $request[ReVentasEstatusModel::ESTATUS_VENTA_ID], 
            ReVentasEstatusModel::USER_ID => Auth::user() ? Auth::user()->id : 1  //Ver de donde obtendremos este valor
        ];

        $this->validaArrayEstatusVentas($params);

        $this->updateActivosByVentas($request[ReVentasEstatusModel::VENTA_ID]);

        return $this->modelo->create($params);
    }

    public function updateActivosByVentas($venta_id)
    {
        return $this->modelo
                    ->where(ReVentasEstatusModel::VENTA_ID, $venta_id)
                    ->update([
                        ReVentasEstatusModel::ACTIVO => false
                    ]);
    }

    private function getByVentaId($venta_id){

         return $this->modelo
        ->where(ReVentasEstatusModel::VENTA_ID, $venta_id)
        ->where(ReVentasEstatusModel::ACTIVO,true)
        ->first();
    }

    private function validaArrayEstatusVentas($request) {
        ParametrosHttpValidador::validar_array([
            ReVentasEstatusModel::VENTA_ID => $request[ReVentasEstatusModel::VENTA_ID],
            ReVentasEstatusModel::ESTATUS_VENTA_ID => $request[ReVentasEstatusModel::ESTATUS_VENTA_ID], 
            ReVentasEstatusModel::USER_ID => $request[ReVentasEstatusModel::USER_ID] 
        ], $this->getReglasGuardar());
    }
}
