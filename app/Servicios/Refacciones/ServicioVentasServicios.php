<?php

namespace App\Servicios\Refacciones;

use App\Models\Refacciones\ReVentasEstatusModel;
use App\Models\Refacciones\VentaServicioModel;
use App\Models\Refacciones\VentasRealizadasModel;
use App\Servicios\Core\ServicioDB;
use Illuminate\Support\Facades\DB;

class ServicioVentasServicios extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'ventas servicio';
        $this->modelo = new VentaServicioModel();
    }

    public function getReglasGuardar()
    {
        return [
            VentaServicioModel::VENTA_ID => 'required|exists:ventas,id',
            VentaServicioModel::NOMBRE_SERVICIO => 'required',
            VentaServicioModel::PRECIO => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            VentaServicioModel::VENTA_ID => 'nullable|exists:ventas,id',
            VentaServicioModel::NOMBRE_SERVICIO => 'nullable',
            VentaServicioModel::PRECIO => 'nullable'
        ];
    }

    public function createUpdateServicioVenta($parametros)
    {
        return $this->modelo->updateOrInsert($parametros);
    }

    public function totalservicio($venta_id)
    {
        $ventas_servicio = $this->modelo->getTable();
        $query = $this->modelo->select(
            DB::raw('SUM(' . $ventas_servicio . '.' . VentaServicioModel::COSTO_MO . ') as total_costo')
        )->from($ventas_servicio)
            ->whereNull('deleted_at')
            ->where($ventas_servicio . '.' . VentaServicioModel::VENTA_ID, $venta_id);

        return $query->count() > 0 ? $query->first()->total_costo : 0;
    }

    ##test
    public function ventaservicio($venta_id)
    {
        return DB::table('venta_servicio')
            ->join('ventas', 'venta_servicio.venta_id', '=',  'ventas.id')
            ->join('re_ventas_estatus', 're_ventas_estatus.ventas_id', '=',  'ventas.id')
            ->where('re_ventas_estatus.activo', true)
            ->where('ventas.id', $venta_id)
            ->whereNull('venta_servicio.deleted_at')
            ->select(
                'venta_servicio.id',
                'venta_servicio.venta_id',
                'venta_servicio.no_identificacion',
                'venta_servicio.nombre_servicio as descripcion_producto',
                'venta_servicio.precio as valor_unitario',
                'venta_servicio.cantidad',
                'venta_servicio.costo_mo',
                'venta_servicio.iva',
                'venta_servicio.refacciones',
                'venta_servicio.precio',
                're_ventas_estatus.estatus_ventas_id',
                DB::raw('venta_servicio.cantidad * venta_servicio.precio as venta_total'),
                DB::raw('TRUE as servicio'),
                'venta_servicio.preventivo',
                'venta_servicio.presupuesto',
                'venta_servicio.bodyshop',
                'venta_servicio.id_index_requisicion',
            )
            ->get();
    }
}
