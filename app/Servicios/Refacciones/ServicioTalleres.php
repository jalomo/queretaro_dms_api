<?php

namespace App\Servicios\Refacciones;

use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\Talleres;

class ServicioTalleres extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'talleres';
        $this->modelo = new Talleres();
    }

    public function getReglasGuardar()
    {
        return [
            Talleres::NOMBRE => 'required',
            Talleres::UBICACION => 'required',
        ];
    }
    public function getReglasUpdate()
    {
        return [
            Talleres::NOMBRE => 'required',
            Talleres::UBICACION => 'required',
        ];
    }
}
