<?php

namespace App\Servicios\Refacciones;

use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\CatalogoUbicacionProductoModel;

class ServicioUbicacionProducto extends ServicioDB
{

    public function __construct()
    {
        $this->recurso = 'Ubicación producto';
        $this->modelo = new CatalogoUbicacionProductoModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatalogoUbicacionProductoModel::NOMBRE => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatalogoUbicacionProductoModel::NOMBRE => 'required'
        ];
    }
}
