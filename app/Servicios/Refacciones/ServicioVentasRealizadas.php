<?php

namespace App\Servicios\Refacciones;

use App\Exceptions\ParametroHttpInvalidoException;
use App\Servicios\Autos\ServicioHistoricoProductosServicio;
use App\Servicios\Core\ServicioDB;
use App\Servicios\CuentasPorCobrar\ServicioCuentaPorCobrar;
use App\Servicios\CuentasPorCobrar\ServicioAbono;
use App\Servicios\CuentasPorCobrar\ServicioAsientos;
use App\Servicios\Refacciones\ServicioFolios;
use App\Models\Contabilidad\CatalogoProcesosModel;
use App\Models\CuentasPorCobrar\CuentasPorCobrarModel;
use App\Models\CuentasPorCobrar\EstatusCuentaModel;
use App\Models\Refacciones\FoliosModel;
use App\Models\Refacciones\Almacenes;
use App\Models\Refacciones\ClientesModel;
use App\Models\Refacciones\EstatusCompra;
use App\Models\Refacciones\VentaProductoModel;
use App\Models\Refacciones\VentasRealizadasModel;
use App\Models\Refacciones\Traspasos;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\StockProductosModel;
use App\Models\Refacciones\ReVentasEstatusModel;
use App\Models\Refacciones\EstatusVentaModel;
use App\Models\Refacciones\permisoVentaModel;
use App\Models\Refacciones\DevolucionVentasModel;
use App\Models\Refacciones\ReVentasVentanillaTallerModel;
use App\Servicios\Refacciones\ServicioCurl;
use App\Models\CuentasPorCobrar\PlazoCreditoModel;
use App\Models\CuentasPorCobrar\TipoFormaPagoModel;
use App\Models\Refacciones\CatalogoUbicacionProductoModel;
use App\Models\Refacciones\OperacionesPiezasModel;
use App\Models\Refacciones\ProductosPedidosModel;
use App\Models\Refacciones\VentaServicioModel;
use App\Models\Usuarios\User;
use App\Servicios\Core\ServicioManejoArchivos;
use App\Servicios\Producto\CrearProductoService;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use Exception;
use InvalidArgumentException;

class ServicioVentasRealizadas extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'ventas';


        $this->modelo = new VentasRealizadasModel();
        $this->modeloEstatusCompra = new EstatusCompra();
        $this->modeloCliente = new ClientesModel();
        $this->modelo_plazo_credito = new PlazoCreditoModel();
        $this->modeloFolio = new FoliosModel();
        $this->modeloVentaServicio = new VentaServicioModel();
        $this->modeloOperacionesPiezas = new OperacionesPiezasModel();

        $this->servicioFolio = new ServicioFolios();
        $this->servicioVentaProducto = new ServicioVentaProducto();
        $this->servicioReVentasEstatus = new ServicioReVentasEstatus();
        $this->servicioReVentaVentanillaTaller = new ServicioReVentaVentanillaTaller();
        $this->servicioCurl = new ServicioCurl();
        $this->servicioProductos = new ServicioProductos();
        $this->servicioCuentaxCobrar = new ServicioCuentaPorCobrar();
        $this->servicio_abonos = new ServicioAbono();
        $this->servicioHistoricoProductosServicio = new ServicioHistoricoProductosServicio();
        $this->servicioDesgloseProductos = new ServicioDesgloseProductos();
        $this->servicioVentasServicios = new ServicioVentasServicios();
        $this->servicioOrdenCompra = new ServicioOrdenCompra();
        $this->servicioProductoPedidos = new ServicioProductoPedidos();
        $this->servicioListaProductosOrdenCompra = new ServicioListaProductosOrdenCompra();
        $this->servicioManejoArchivos = new ServicioManejoArchivos();
        $this->servicioAsientos = new ServicioAsientos();
    }

    public function getReglasGuardar()
    {
        return [
            VentasRealizadasModel::FOLIO_ID => 'required|exists:folios,id',
            VentasRealizadasModel::TIPO_VENTA => 'required|numeric',
            VentasRealizadasModel::CLIENTE_ID => 'exists:clientes,id',
            VentasRealizadasModel::VENTA_TOTAL => 'required|numeric',
            VentasRealizadasModel::VENDEDOR_ID => 'required|numeric',
            VentasRealizadasModel::ALMACEN_ID => 'required|numeric'
        ];
    }

    public function getReglasCrearVenta()
    {
        return [
            VentasRealizadasModel::CLIENTE_ID => 'required|exists:clientes,id',
            VentasRealizadasModel::TIPO_VENTA => 'required|numeric',
            VentasRealizadasModel::TIPO_CLIENTE_ID => 'required|numeric',
            VentasRealizadasModel::PRECIO_ID => 'nullable|numeric',
            VentasRealizadasModel::TIPO_PRECIO_ID => 'nullable|numeric',
            VentasRealizadasModel::VENTA_TOTAL => 'nullable|numeric',
            VentasRealizadasModel::VENDEDOR_ID => 'nullable|numeric',
            VentasRealizadasModel::FOLIO_ID => 'nullable|exists:folios,id',
            VentasRealizadasModel::FECHA_VENTA => 'required|date'
        ];
    }

    public function getReglasGuardarSinFolio()
    {
        return [
            VentasRealizadasModel::CLIENTE_ID => 'required|exists:clientes,id',
            VentasRealizadasModel::VENTA_TOTAL => 'required|numeric',
            VentasRealizadasModel::SUBTOTAL => 'nullable|numeric',
            VentasRealizadasModel::IVA => 'nullable|numeric',
            VentasRealizadasModel::TIPO_VENTA => 'required|numeric',
            VentasRealizadasModel::REL_PRODUCTO_ID => 'required|exists:producto,id',
            VentasRealizadasModel::PRECIO_ID => 'required|exists:precio,id',
            VentaProductoModel::CANTIDAD => 'required|numeric',
            VentasRealizadasModel::VENDEDOR_ID => 'required|numeric',
            VentaProductoModel::VALOR_UNITARIO => 'required|numeric',
            VentasRealizadasModel::ALMACEN_ID => 'required|numeric',
            VentasRealizadasModel::TIPO_PRECIO_ID => 'required|numeric'
        ];
    }

    public function getReglasUpdate()
    {
        return [
            VentasRealizadasModel::FOLIO_ID => 'required|exists:folios,id',
            VentasRealizadasModel::CLIENTE_ID => 'nullable|exists:clientes,id',
            VentasRealizadasModel::VENTA_TOTAL => 'required|numeric',
            VentasRealizadasModel::SUBTOTAL => 'nullable|numeric',
            VentasRealizadasModel::IVA => 'nullable|numeric',
            CuentasPorCobrarModel::TIPO_PAGO_ID => 'nullable|numeric',
            CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID => 'required|numeric',
            CuentasPorCobrarModel::PLAZO_CREDITO_ID => 'required|numeric',
            CuentasPorCobrarModel::ENGANCHE => 'required|numeric',
            CuentasPorCobrarModel::TASA_INTERES => 'required|numeric',
            VentasRealizadasModel::FECHA_VENTA => 'nullable|date'
        ];
    }

    public function getReglasFinalizaVentaContado()
    {
        return [
            VentasRealizadasModel::FOLIO_ID => 'required|exists:folios,id',
            VentasRealizadasModel::CLIENTE_ID => 'nullable|exists:clientes,id',
            VentasRealizadasModel::VENTA_TOTAL => 'required|numeric',
            VentasRealizadasModel::SUBTOTAL => 'nullable|numeric',
            VentasRealizadasModel::IVA => 'nullable|numeric',
            CuentasPorCobrarModel::CONCEPTO => 'required|string',
            CuentasPorCobrarModel::TIPO_PAGO_ID => 'required|numeric',
            CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID => 'required|numeric',
            CuentasPorCobrarModel::PLAZO_CREDITO_ID => 'nullable|numeric',
            CuentasPorCobrarModel::ENGANCHE => 'nullable|numeric',
            CuentasPorCobrarModel::TASA_INTERES => 'nullable|numeric',
            CuentasPorCobrarModel::CONCEPTO => 'nullable',
            CuentasPorCobrarModel::USUARIO_GESTOR_ID => 'nullable|numeric|exists:usuarios,id',
            CuentasPorCobrarModel::ESTATUS_CUENTA_ID => 'nullable|numeric',
            CuentasPorCobrarModel::CUENTA_POR_COBRAR_ID => 'nullable|exists:cuentas_por_cobrar,id',
            VentasRealizadasModel::FECHA_VENTA => 'nullable|date'
        ];
    }

    public function getReglasFinalizaVentaCredito()
    {
        return [
            VentasRealizadasModel::FOLIO_ID => 'required|exists:folios,id',
            VentasRealizadasModel::CLIENTE_ID => 'nullable|exists:clientes,id',
            VentasRealizadasModel::VENTA_TOTAL => 'required|numeric',
            VentasRealizadasModel::SUBTOTAL => 'nullable|numeric',
            VentasRealizadasModel::IVA => 'nullable|numeric',
            CuentasPorCobrarModel::CONCEPTO => 'required|string',
            CuentasPorCobrarModel::USUARIO_GESTOR_ID => 'nullable|numeric|exists:usuarios,id',
            CuentasPorCobrarModel::TIPO_PAGO_ID => 'nullable|numeric',
            CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID => 'required|numeric',
            CuentasPorCobrarModel::PLAZO_CREDITO_ID => 'required|numeric',
            CuentasPorCobrarModel::ENGANCHE => 'required|numeric',
            CuentasPorCobrarModel::TASA_INTERES => 'required|numeric',
            CuentasPorCobrarModel::CONCEPTO => 'required',
            CuentasPorCobrarModel::ESTATUS_CUENTA_ID => 'nullable|numeric',
            CuentasPorCobrarModel::CUENTA_POR_COBRAR_ID => 'nullable|exists:cuentas_por_cobrar,id',
            VentasRealizadasModel::FECHA_VENTA => 'nullable|date'
        ];
    }
    public function getReglasVentasAgrupadasMes()
    {
        return [
            VentaProductoModel::PRODUCTO_ID => 'required|exists:producto,id',
            VentasRealizadasModel::CANTIDAD_MESES => 'nullable|numeric',
            VentasRealizadasModel::YEAR => 'required|numeric'

        ];
    }

    public function getReglasUpdateOnlyVenta()
    {
        return [
            VentasRealizadasModel::CLIENTE_ID => 'required|exists:clientes,id',
            VentasRealizadasModel::TIPO_VENTA => 'required|numeric',
            VentasRealizadasModel::TIPO_CLIENTE_ID => 'required|numeric',
            VentasRealizadasModel::PRECIO_ID => 'nullable|numeric',
            VentasRealizadasModel::TIPO_PRECIO_ID => 'nullable|numeric',
            VentasRealizadasModel::VENTA_TOTAL => 'nullable|numeric',
            VentasRealizadasModel::VENDEDOR_ID => 'nullable|numeric',
            VentasRealizadasModel::FOLIO_ID => 'nullable|exists:folios,id',
            VentasRealizadasModel::FECHA_VENTA => 'nullable|date'
        ];
    }

    public function getReglasTipoVenta()
    {
        return [
            VentasRealizadasModel::TIPO_VENTA => 'required|numeric',
        ];
    }

    public function getReglasCliente()
    {
        return [
            VentasRealizadasModel::CLIENTE_ID => 'required|exists:clientes,id',
        ];
    }

    public function registrarVenta($parametros)
    {
        $folio = $this->servicioFolio->generarFolio(CatalogoProcesosModel::PROCESO_VENTA_MOSTRADOR);
        $data_ventas = $parametros;
        $data_ventas[VentasRealizadasModel::FOLIO_ID] = $folio->id;

        if ($venta = $this->modelo->create($data_ventas)) {
            $this->servicioReVentasEstatus->storeReVentasEstatus([
                ReVentasEstatusModel::VENTA_ID => $venta->id,
                ReVentasEstatusModel::ESTATUS_VENTA_ID => EstatusVentaModel::ESTATUS_PROCESO
            ]);

            $cantidad_stock = $this->servicioDesgloseProductos->getStockByProducto([
                'producto_id' => $parametros['producto_id']
            ]);

            if ($parametros[VentaProductoModel::CANTIDAD] > $cantidad_stock->cantidad_actual) {
                $this->servicioProductoPedidos->store([
                    ProductosPedidosModel::PRODUCTO_ID => $parametros[ProductosPedidosModel::PRODUCTO_ID],
                    ProductosPedidosModel::CANTIDAD_SOLICITADA => $parametros[VentaProductoModel::CANTIDAD] - $cantidad_stock->cantidad_actual
                ]);
            }

            $parametros[VentasRealizadasModel::VENTA_ID] = $venta->id;

            $data_producto = [
                VentaProductoModel::PRODUCTO_ID => $parametros[VentaProductoModel::PRODUCTO_ID],
                VentaProductoModel::ALMACEN_ID => $parametros[VentaProductoModel::ALMACEN_ID],
                VentaProductoModel::PRECIO_ID => $parametros[VentaProductoModel::PRECIO_ID],
                VentaProductoModel::CLIENTE_ID => $parametros[VentaProductoModel::CLIENTE_ID],
                VentaProductoModel::FOLIO_ID => $folio->id,
                VentaProductoModel::CANTIDAD => $parametros[VentaProductoModel::CANTIDAD],
                VentaProductoModel::TOTAL_VENTA => $parametros[VentaProductoModel::VALOR_UNITARIO] * $parametros[VentaProductoModel::CANTIDAD],
                VentaProductoModel::VALOR_UNITARIO => $parametros[VentaProductoModel::VALOR_UNITARIO],
                VentaProductoModel::VENTA_ID => $parametros[VentaProductoModel::VENTA_ID],
            ];

            return $this->servicioVentaProducto->crear($data_producto);
        }

        return $venta;
    }

    public function getReglasCrearVentaMpm()
    {
        return [
            VentasRealizadasModel::PRECIO_ID => 'required|exists:precio,id',
            VentasRealizadasModel::CLIENTE_ID => 'required|exists:clientes,id',
            VentasRealizadasModel::VENTA_TOTAL => 'required|numeric',
            VentasRealizadasModel::TIPO_VENTA => 'required|numeric',
            VentasRealizadasModel::ALMACEN_ID => 'required|numeric',
            VentasRealizadasModel::TIPO_PRECIO_ID => 'required|numeric',
            ReVentasVentanillaTallerModel::NUMERO_ORDEN => 'required|numeric'
        ];
    }

    public function listaProductosTaller($numero_orden)
    {
        return $this->curlGetDataApiMpm($numero_orden);
    }

    public function crearVentaMpm($parametros)
    {
        $venta = $this->modelo->where(VentasRealizadasModel::NUMERO_ORDEN, $parametros[VentasRealizadasModel::NUMERO_ORDEN])->first();

        if ($venta == null) {

            $cliente = ClientesModel::where('id', $parametros['cliente_id'])->first();

            if ($cliente == null) {
                throw new Exception('EL cliente no se encuentra registrado en el catalogo de clientes');
            }

            $folio = $this->servicioFolio->generarFolio(CatalogoProcesosModel::PROCESO_SERVICIOS);

            $parametros[VentasRealizadasModel::CLIENTE_ID] = $cliente->id;
            $parametros[VentasRealizadasModel::FOLIO_ID] = $folio->id;
            $parametros[VentasRealizadasModel::VENTA_TOTAL] = 0;
            $parametros[VentasRealizadasModel::NUMERO_ORDEN];

            $venta = $this->modelo->create($parametros);

            $this->servicioReVentasEstatus->storeReVentasEstatus([
                ReVentasEstatusModel::VENTA_ID => $venta->id,
                ReVentasEstatusModel::ESTATUS_VENTA_ID => EstatusVentaModel::ESTATUS_PROCESO,
            ]);

            $productos_taller = $this->getRefaccionesDeTaller($parametros[ReVentasVentanillaTallerModel::NUMERO_ORDEN]);

            $this->guardarDetalleProductoVentaMpm($productos_taller, [
                VentaProductoModel::PRECIO_ID =>  $parametros[VentaProductoModel::PRECIO_ID],
                VentaProductoModel::CLIENTE_ID => $parametros[VentaProductoModel::CLIENTE_ID],
                VentaProductoModel::FOLIO_ID => $venta->folio_id,
                VentaProductoModel::VENTA_ID => $venta->id,
                ReVentasVentanillaTallerModel::NUMERO_ORDEN => $parametros[ReVentasVentanillaTallerModel::NUMERO_ORDEN],
            ]);
        }

        $this->calcularVentaTotal($venta->id);

        return $venta;
    }

    public function guardarDetalleProductoVentaMpm($data, $parametros_venta)
    {
        foreach ($data as $key => $item) 
        {
            if ($item->tipo == "OPERACION") 
            {
                $precio_total = (float) $item->costo_mo;
                $this->servicioVentasServicios->createUpdateServicioVenta([
                    VentaServicioModel::NOMBRE_SERVICIO => $item->descripcion,
                    VentaProductoModel::VENTA_ID =>  $parametros_venta[VentaProductoModel::VENTA_ID],
                    VentaServicioModel::PRECIO => $precio_total,
                    VentaServicioModel::NO_IDENTIFICACION => trim($item->num_pieza),
                    VentaServicioModel::CANTIDAD => (int) $item->cantidad,
                    VentaServicioModel::IVA => $item->iva,
                    VentaServicioModel::COSTO_MO =>  $item->costo_mo,
                    VentaServicioModel::PREVENTIVO => filter_var($item->tipo_preventivo, FILTER_VALIDATE_BOOLEAN)? TRUE : FALSE,
                    VentaServicioModel::PRESUPUESTO => filter_var($item->presupuesto?? false, FILTER_VALIDATE_BOOLEAN)? TRUE : FALSE,
                    VentaServicioModel::BODYSHOP => filter_var($item->bodyshop?? false, FILTER_VALIDATE_BOOLEAN)? TRUE : FALSE,
                    VentaServicioModel::ID_INDEX_REQUISICION => $item->id,
                ]);
            } 

            if($item->tipo == "REFACCION" && $item->num_pieza == '')
            {
                VentaServicioModel::updateOrCreate(
                    [
                        'venta_id' =>  $parametros_venta[VentaProductoModel::VENTA_ID],
                        'no_identificacion' => trim($item->num_pieza),
                        'nombre_servicio' => $item->descripcion,
                        'id_index_requisicion' => $item->id,
                    ],
                    [
                        'precio' => (float) $item->costo_total_sin_iva + (float) $item->costo_iva,
                        'cantidad' => $item->cantidad,
                        'iva' => $item->costo_iva,
                        'costo_mo' =>  (float) $item->costo_mano_obra * VentaProductoModel::IVA,
                        'refacciones' => (float) $item->costo_refaccion,
                        'preventivo' => filter_var($item->tipo_preventivo, FILTER_VALIDATE_BOOLEAN)? TRUE : FALSE,
                        'presupuesto' => filter_var($item->presupuesto?? false, FILTER_VALIDATE_BOOLEAN)? TRUE : FALSE,
                        'bodyshop' => filter_var($item->bodyshop?? false, FILTER_VALIDATE_BOOLEAN)? TRUE : FALSE,
                    ]
                );
            }
            
            if(($item->tipo == 'PREPIKING' || $item->tipo == 'REFACCION') && $item->num_pieza != '')
            {
                $num_pieza = preg_replace('/^FM/', '', $item->num_pieza); // Fix: algunas piezas traen ya el el prefijo

                // Hay piezas con extra que traen FM unicamente esas no se agregaran
                if (empty($num_pieza) || $num_pieza == 'FM') {
                    continue;
                }

                // Buscamos Producto     
                // y si no existe el producto se agrega el numero de pieza
                // al stock y se pone en ceros.
                $productoService = new CrearProductoService;
                $productos = $productoService->make([
                    'no_identificacion' => $item->num_pieza,
                    'descripcion' => $item->descripcion,
                    'valor_unitario' => $item->precio_unitario,
                ]);

                Log::info("ACTUALIZAR VENTA DETALLES ID {$parametros_venta[VentaProductoModel::VENTA_ID]}, INDEX REQUISICION: $item->id :: $item->num_pieza -> $item->precio_unitario");

                // Creamos el registro en ventas_productos si no existen los campos
                // folio_id, venta_id, producto_id o Actualizamos
                $venta_producto = VentaProductoModel::updateOrCreate(
                    [
                        VentaProductoModel::FOLIO_ID => $parametros_venta[VentaProductoModel::FOLIO_ID],
                        VentaProductoModel::VENTA_ID => $parametros_venta[VentaProductoModel::VENTA_ID],
                        VentaProductoModel::PRODUCTO_ID => $productos->id,
                        VentaProductoModel::ID_INDEX_REQUISICION => $item->id,
                    ],
                    [
                        VentaProductoModel::PRODUCTO_ID => $productos->id,
                        VentaProductoModel::PRECIO_ID => 1,
                        VentaProductoModel::VALOR_UNITARIO => $item->precio_unitario, // Fix: porque el precio unitario es el total de todas las pieza
                        VentaProductoModel::CANTIDAD => round($item->cantidad),
                        VentaProductoModel::TOTAL_VENTA => $item->costo_refaccion + $item->costo_mano_obra * VentaProductoModel::IVA, //# costo_refaccion trae el total
                        VentaProductoModel::DESCONTADO_ALMACEN => 1,
                        VentaProductoModel::ORDEN_ORIGINAL => 1,
                        VentaProductoModel::ESTATUS_PRODUCTO_ORDEN => "O",
                        VentaProductoModel::ID_INDEX_REQUISICION => $item->id,
                        VentaProductoModel::PREPIKING => $item->tipo == "PREPIKING" ? TRUE : FALSE,
                        VentaProductoModel::AFECTA_PAQUETE => filter_var($item->tipo_preventivo, FILTER_VALIDATE_BOOLEAN)? TRUE : FALSE,
                        VentaProductoModel::PREVENTIVO => filter_var($item->tipo_preventivo, FILTER_VALIDATE_BOOLEAN)? TRUE : FALSE,
                        VentaProductoModel::PRESUPUESTO => filter_var($item->presupuesto, FILTER_VALIDATE_BOOLEAN) ? TRUE : FALSE,
                        VentaProductoModel::BODYSHOP => filter_var($item->bodyshop?? false, FILTER_VALIDATE_BOOLEAN) ? TRUE : FALSE,
                        VentaProductoModel::COSTO_MANO_OBRA => $item->costo_mano_obra * VentaProductoModel::IVA,
                        VentaProductoModel::COSTO_PROMEDIO => $productos->costo_promedio,
                    ]
                );
            }
        }
    }

    public function curlGetDataApiMpm($numero_orden)
    {
        //DMS - https://sohex.net/queretaro_citas_dms/citas/dms_panles_queretaro/conexion/Conexion_DMS/recuperar_operaciones
        //MPM - https://so-hex.com/cs/mpm/matriz_mpm/orden/mpm_matriz/api/Conexion_DMS/recuperar_operaciones
        // dd("sasd");
        $request = $this->servicioCurl->curlPost(env('URL_SERVICIOS')  . env('URL_OPERACIONES'), [
            'id_cita' => $numero_orden
        ], true);


        if (empty($request)) {
            throw new ParametroHttpInvalidoException([
                'validacion_msg' => __(self::$I0008_NO_EXISTE_PRODUCTO, ["parametro" => ""])
            ]);
        }

        return $this->validarPiezasToMpm($request);
    }

    public function getRefaccionesDeTaller($numero_orden)
    {
        $ws_url = env('URL_SERVICIOS') . env('URL_OPERACIONES2');

        $dataWS = Http::get($ws_url, [ 'id_cita' => $numero_orden ]);

        if (!$data = $dataWS->object()) {
            throw new \Exception('No se encontraron refacciones en el webservice de servicios.');
        }

        if (!isset($data->refacciones)) {
            throw new InvalidArgumentException('No se encontraron refacciones o formato incorrecto');
        }

        foreach ($data->refacciones as $key => &$item) {
            if (!isset($item->num_pieza))
                continue;

            $cantidad_stock = ProductosModel::join('stock_productos', 'producto.id', '=', 'stock_productos.producto_id')
                ->where('producto.no_identificacion', trim($item->num_pieza))
                ->first(['producto.id', 'cantidad_actual', 'valor_unitario']);

            $item->producto_id = isset($cantidad_stock->id) ? $cantidad_stock->id : null;
            $item->se_registro = $cantidad_stock == null ? false : true;
            $item->cantidad_stock = isset($cantidad_stock->cantidad_actual) ? $cantidad_stock->cantidad_actual : 0;
            $item->costo_mo = $item->costo_total_con_iva;
            $item->iva = $item->costo_iva;
            // $item->activo = $item->activo == 1? true : false;
        }

        return tap(collect($data->refacciones), function($collection) use($ws_url, $numero_orden) {
            info("{$ws_url} {$numero_orden}", $collection->toArray());
        });
    }

    public function validarPiezasToMpm($request)
    {
        $new_array = [];
        $data_api =  json_decode($request);
        $validar = !empty($data_api) ? $data_api : [];
        // dd($validar);
        foreach ($validar->refacciones as $key => $value) {
            if (isset($value->num_pieza) && $value->num_pieza !== "") {
                $existe_producto = false;
                if ($value->tipo !== "OPERACION") {
                    $valida_producto = $this->servicioProductos->validarExistenciaProducto([
                        'No_identificacion' => trim($value->num_pieza)
                    ])->first();

                    $cantidad_stock = ProductosModel::join('stock_productos', 'producto.id', '=', 'stock_productos.producto_id')
                        ->where('producto.no_identificacion', trim($value->num_pieza))
                        ->first(['producto.id', 'cantidad_actual', 'valor_unitario']);

                    $existe_producto = !empty($valida_producto) || !empty($cantidad_stock->id) ? true : false;
                } else {
                    $cantidad_stock = null;
                }

                $item = new \stdClass;
                $item->id = $value->id;
                $item->producto_id = isset($cantidad_stock->id) ? $cantidad_stock->id : null;
                $item->se_registro = $existe_producto;
                $item->cantidad = $value->cantidad;
                $item->cantidad_stock = isset($cantidad_stock->cantidad_actual) ? $cantidad_stock->cantidad_actual : 0;
                $item->descripcion = $value->descripcion;
                $item->num_pieza = trim($value->num_pieza);
                $item->existe = $value->existe;
                $item->precio_unitario =  isset($cantidad_stock->id) && $value->precio_unitario == '0' ? $cantidad_stock->valor_unitario : $value->precio_unitario;
                $item->total_horas = $value->total_horas;
                $item->costo_mo = $value->costo_total_con_iva;
                $item->iva = $value->costo_iva;
                $item->tipo = $value->tipo;
                // if (trim($value->num_pieza) == 'MXO5W30B') {
                // echo '<pre>';
                //     print_r($cantidad_stock);
                //     print_r($value);
                // print_r($item);
                // echo '</pre>';
                // die();
                // }
                array_push($new_array, $item);
            }
        }

        $responseData = [
            'vehiculo' => $validar->vehiculo,
            'refacciones' => $new_array
        ];
        return $responseData;
    }

    public function ventasByFolioId($folio_id)
    {
        return VentasRealizadasModel::with([
            VentasRealizadasModel::REL_FOLIO,
            VentasRealizadasModel::REL_CLIENTE,
            VentasRealizadasModel::REL_VENTAS_ESTATUS . '.' . ReVentasEstatusModel::REL_ESTATUS,
            VentasRealizadasModel::REL_PRECIO,
            VentasRealizadasModel::REL_ALMACEN => function ($query) {
                $query->select(
                    Almacenes::ID,
                    Almacenes::NOMBRE,
                    Almacenes::CODIGO_ALMACEN
                );
            }
        ])
        ->where(VentasRealizadasModel::FOLIO_ID, $folio_id)
        ->first();
    }

    public function finalizarVenta($id, $request)
    {
        DB::beginTransaction();
        $modelo = $this->modelo->findOrFail($id);
        try {
            $datos[VentasRealizadasModel::SUBTOTAL] = $this->servicioVentaProducto->totalVentaByFolio($request->folio_id);
            $datos[VentasRealizadasModel::IVA] = $datos[VentasRealizadasModel::SUBTOTAL] * 0.16;
            $datos[VentasRealizadasModel::VENTA_TOTAL] = $datos[VentasRealizadasModel::SUBTOTAL] + $datos[VentasRealizadasModel::IVA];
            $datos[VentasRealizadasModel::CLIENTE_ID] = $request->get(VentasRealizadasModel::CLIENTE_ID);
            $actualizar = parent::massUpdateWhereId(VentasRealizadasModel::ID, $id, $datos);
            if (!$actualizar) {
                throw new ParametroHttpInvalidoException([
                    'msg' => "Error al actualizar !!"
                ]);
            }
            if (empty($request->mpm)) {
                $data_cuentas = $request->all();
                $data_cuentas[VentasRealizadasModel::VENTA_TOTAL] = $datos[VentasRealizadasModel::VENTA_TOTAL];
                $cuentas_cobrar = $this->servicioCuentaxCobrar->procesarCuentasPorCobrar($data_cuentas);
                $this->servicioReVentasEstatus->storeReVentasEstatus([
                    ReVentasEstatusModel::VENTA_ID => $id,
                    ReVentasEstatusModel::ESTATUS_VENTA_ID => EstatusVentaModel::ESTATUS_FINALIZADA
                ]);

                DB::commit();
                return $cuentas_cobrar;
            }
            return $modelo;
        } catch (QueryException $e) {
            Log::warning($e->getMessage());
            DB::rollback();
        }
    }

    public function editarVenta($id, $request)
    {
        $modelo = $this->modelo->findOrFail($id);

        try {
            DB::beginTransaction();

            $data = $request->all();
            $dataCuenta = $this->servicioCuentaxCobrar->getByFolioId($data['folio_id']);

            if ($dataCuenta && $dataCuenta->estatus_cuenta_id != 1) {
                throw new ParametroHttpInvalidoException([
                    'msg' => "Error al actualizar !! La venta se encuentra en proceso de pago."
                ]);
            }
            $datos[VentasRealizadasModel::SUBTOTAL] = $this->servicioVentaProducto->totalVentaByFolio($request->folio_id);
            $datos[VentasRealizadasModel::IVA] = $datos[VentasRealizadasModel::SUBTOTAL] * 0.16;
            $datos[VentasRealizadasModel::VENTA_TOTAL] = $datos[VentasRealizadasModel::SUBTOTAL] + $datos[VentasRealizadasModel::IVA];

            $modelo = parent::massUpdateWhereId(VentasRealizadasModel::ID, $id, $datos);

            if ($modelo && $dataCuenta) {
                $data_cuentas_cobrar = $request->all();
                $data_cuentas_cobrar[VentasRealizadasModel::VENTA_TOTAL] = $datos[VentasRealizadasModel::VENTA_TOTAL];
                $cuentas_cobrar = $this->servicioCuentaxCobrar->procesarCuentasPorCobrar($data_cuentas_cobrar);
                DB::commit();
            }

            return $cuentas_cobrar;
        } catch (\Throwable $e) {
            Log::warning($e->getMessage());
            DB::rollback();
        }

        return response()->json($modelo);
    }

    //parche del parche
    public function descontarVentaMpm($id, $request)
    {
        DB::beginTransaction();

        $modelo = $this->modelo->findOrFail($id);
        try {
            $data = $request->all();
            $modelo->fill($data);
            $modelo = $this->guardarModelo($modelo);
            DB::commit();
            return $modelo;
        } catch (QueryException $e) {
            Log::warning($e->getMessage());
            DB::rollback();
        }
    }

    public function getAll()
    {
        $data =  $this->modelo->with([
            VentasRealizadasModel::REL_FOLIO => function ($query) {
                $query->with([
                    'cuenta_por_cobrar'
                ]);
            },
            VentasRealizadasModel::REL_CLIENTE,
            VentasRealizadasModel::REL_VENTAS_FINALIZADAS => function ($query) {

                $query->with([
                    'estatus_venta'
                ]);
            }
        ])->get();

        $new_array = [];
        foreach ($data as $key => $item) {
            if (isset($item->ventas_finalizadas)) {
                array_push($new_array, $item);
            }
        }

        return $new_array;
    }

    public function reglasListaVentaByStatus()
    {
        return [
            VentasRealizadasModel::ESTATUS_COMPRA => 'required|array',
            VentasRealizadasModel::CLIENTE_ID => 'nullable'
        ];
    }

    //TODO: listado ventas realizadas, separar devoluciones
    public function getVentasByStatus($parametros)
    {

        $tableVentas = VentasRealizadasModel::getTableName();
        $tableFolios = FoliosModel::getTableName();
        $tableReVentaEstatus = ReVentasEstatusModel::getTableName();
        $tableEstatusVenta = EstatusVentaModel::getTableName();
        $tabla_clientes = ClientesModel::getTableName();

        $query = DB::table($tableVentas)
            ->join($tableFolios, $tableVentas . '.' . VentasRealizadasModel::FOLIO_ID, '=', $tableFolios . '.' . FoliosModel::ID)
            ->join($tabla_clientes, $tableVentas . '.' . VentasRealizadasModel::CLIENTE_ID, '=', $tabla_clientes . '.' . ClientesModel::ID)
            ->join($tableReVentaEstatus, $tableReVentaEstatus . '.' . ReVentasEstatusModel::VENTA_ID, '=', $tableVentas . '.' . VentasRealizadasModel::ID)
            ->join($tableEstatusVenta, $tableEstatusVenta . '.' . EstatusVentaModel::ID, '=', $tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID)
            ->select(
                'ventas.id',
                'ventas.venta_total',
                'folios.folio',
                'estatus_venta.nombre as estatus_venta',
                'clientes.nombre as nombre_cliente',
                'clientes.id as cliente_id',
                'ventas.created_at',
                $tableEstatusVenta . '.' . EstatusVentaModel::NOMBRE . ' as estatusVenta'
            );

        if (isset($parametros[VentasRealizadasModel::CLIENTE_ID])) {
            $query->where($tabla_clientes . '.' . ClientesModel::ID, $parametros[VentasRealizadasModel::CLIENTE_ID]);
        }

        $query->where(ReVentasEstatusModel::ACTIVO, true);
        $query->whereIn($tableEstatusVenta . '.' . EstatusVentaModel::ID, $parametros[VentasRealizadasModel::ESTATUS_COMPRA]);
        return $query->get();
    }

    public function getByIdWith($id)
    {
        $data =  $this->modelo->with([
            VentasRealizadasModel::REL_FOLIO => function ($query) {
                $query->with([
                    'cuenta_por_cobrar'
                ]);
            },
            VentasRealizadasModel::REL_CLIENTE,
            VentasRealizadasModel::REL_VENTAS_FINALIZADAS => function ($query) {
                $query->with([
                    'estatus_venta'
                ]);
            }
        ])
            ->where(VentasRealizadasModel::ID, $id)
            ->get();

        $new_array = [];
        foreach ($data as $key => $item) {
            if (isset($item->ventas_finalizadas)) {
                array_push($new_array, $item);
            }
        }

        return $new_array;
    }

    public function getById($id)
    {
        return $this->modelo->with([
            VentasRealizadasModel::REL_FOLIO
        ])
            ->where(VentasRealizadasModel::ID, $id)
            ->first();
    }

    public function updateStatusVenta($venta_id, $status_id)
    {
        $modelo = $this->modelo->find($venta_id);
        $modelo->estatus_id = $status_id;
        return $modelo->save();
    }

    public function getReglasBusqueda()
    {
        return [
            VentasRealizadasModel::FECHA_INICIO => 'required|date',
            VentasRealizadasModel::FECHA_FIN => 'required|date',
        ];
    }

    public function getReglasBusquedaTraspaso()
    {
        return [
            'fecha_inicio' => 'nullable|date',
            'fecha_fin' => 'nullable|date',
            VentasRealizadasModel::FOLIO_ID => 'nullable|exists:folios,id',
        ];
    }

    public function calcularTotal($modelo)
    {
        $ventas =  $modelo;
        $total = 0;
        foreach ($ventas as $key => $item) {
            $total = $total + $item->venta_total;
        }

        return [
            'total_venta' => $total,
            'data' => $modelo
        ];
    }

    public function filtrarPorFechas($parametros)
    {
        return $this->modelo->with([
            'folio' => function ($query) {
                $query->select('id', 'folio');
            },
            'estatus' => function ($query) {
                $query->select('id', 'nombre');
            },
            'cliente' => function ($query) {
                $query->select('id', 'nombre_empresa', 'numero_cliente', 'rfc');
            },
            'detalle_venta' => function ($query) {
                $query->with([
                    'producto' => function ($query_prod) {
                        $query_prod->select('id', 'no_identificacion', 'descripcion', 'valor_unitario');
                    }
                ]);
            }
        ])
            ->whereDate(VentasRealizadasModel::CREATED_AT, '>=', $parametros[VentasRealizadasModel::FECHA_INICIO])
            ->whereDate(VentasRealizadasModel::CREATED_AT, '<=', $parametros[VentasRealizadasModel::FECHA_FIN])
            ->get();
    }

    public function getVentasByFechas($request)
    {
        $tableVentas = VentasRealizadasModel::getTableName();
        $tableFolios = FoliosModel::getTableName();
        $tableVentaProducto = VentaProductoModel::getTableName();
        $tableProducto = ProductosModel::getTableName();
        $tableReVentaEstatus = ReVentasEstatusModel::getTableName();
        $tableEstatusVenta = EstatusVentaModel::getTableName();

        $query = DB::table($tableVentas)
            ->join($tableFolios, $tableVentas . '.' . VentasRealizadasModel::FOLIO_ID, '=', $tableFolios . '.' . FoliosModel::ID)
            ->join($tableVentaProducto, $tableFolios . '.' . FoliosModel::ID, '=', $tableVentaProducto . '.' . VentaProductoModel::FOLIO_ID)
            ->join($tableProducto, $tableVentaProducto . '.' . VentaProductoModel::PRODUCTO_ID, '=', $tableProducto . '.' . ProductosModel::ID)
            ->join($tableReVentaEstatus, $tableReVentaEstatus . '.' . ReVentasEstatusModel::VENTA_ID, '=', $tableVentas . '.' . VentasRealizadasModel::ID)
            ->join($tableEstatusVenta, $tableEstatusVenta . '.' . EstatusVentaModel::ID, '=', $tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID)
            ->select(
                'ventas.id',
                'ventas.venta_total',
                'ventas.almacen_id',
                'folios.folio',
                'venta_producto.cantidad',
                'venta_producto.venta_total as venta_total_producto',
                'venta_producto.valor_unitario',
                'producto.no_identificacion',
                'producto.descripcion',
                'producto.unidad',
                'ventas.created_at',
                $tableEstatusVenta . '.' . EstatusVentaModel::NOMBRE . ' as estatusVenta',
                $tableEstatusVenta . '.' . EstatusVentaModel::ID . ' as estatusId'
            );
        if ($request->get('fecha_inicio') && $request->get('fecha_fin')) {
            $query->whereBetween('ventas.created_at', [$request->get('fecha_inicio') . ' 00:00:00', $request->get('fecha_fin') . ' 23:59:59']);
        }
        if ($request->get(VentasRealizadasModel::FOLIO_ID)) {
            $query->where($tableVentas . '.' . VentasRealizadasModel::FOLIO_ID, $request->get(VentasRealizadasModel::FOLIO_ID));
        }
        $query->where($tableReVentaEstatus . '.' . ReVentasEstatusModel::ACTIVO, true);
        $query->whereIn($tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID, array(EstatusVentaModel::ESTATUS_FINALIZADA))
            ->get();
        return [
            'data' => $query->get()
        ];
    }

    public function getTotalesVentasByFechas($request)
    {
        $tableVentas = VentasRealizadasModel::getTableName();
        $tableFolios = FoliosModel::getTableName();
        $tableVentaProducto = VentaProductoModel::getTableName();
        $tableReVentaEstatus = ReVentasEstatusModel::getTableName();
        $tableEstatusVenta = EstatusVentaModel::getTableName();
        $array_estatus = [EstatusVentaModel::ESTATUS_FINALIZADA];

        $query = DB::table($tableVentas)
            ->join($tableFolios, $tableVentas . '.' . VentasRealizadasModel::FOLIO_ID, '=', $tableFolios . '.' . FoliosModel::ID)
            ->join($tableVentaProducto, $tableFolios . '.' . FoliosModel::ID, '=', $tableVentaProducto . '.' . VentaProductoModel::FOLIO_ID)
            ->join($tableReVentaEstatus, $tableReVentaEstatus . '.' . ReVentasEstatusModel::VENTA_ID, '=', $tableVentas . '.' . VentasRealizadasModel::ID)
            ->join($tableEstatusVenta, $tableEstatusVenta . '.' . EstatusVentaModel::ID, '=', $tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID)
            ->select(
                DB::raw('count(ventas.id) as total_ventas'),
                DB::raw('sum(venta_producto.cantidad) as total_cantidad'),
                DB::raw('sum(venta_producto.venta_total) as sum_venta_total'),
                DB::raw('sum(venta_producto.valor_unitario) as sum_valor_unitario')
            );
        if ($request->get('fecha_inicio') && $request->get('fecha_fin')) {
            $query->whereBetween('ventas.created_at', [$request->get('fecha_inicio') . ' 00:00:00', $request->get('fecha_fin') . ' 23:59:59']);
        }
        if ($request->get(VentasRealizadasModel::FOLIO_ID)) {
            $array_estatus = [EstatusVentaModel::ESTATUS_FINALIZADA, EstatusVentaModel::ESTATUS_PROCESO];
            $query->where($tableVentas . '.' . VentasRealizadasModel::FOLIO_ID, $request->get(VentasRealizadasModel::FOLIO_ID));
        }
        $query->where($tableReVentaEstatus . '.' . ReVentasEstatusModel::ACTIVO, true);
        $query->whereIn($tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID, $array_estatus);
        return $query->first();
    }

    public function getBusquedaVentas($request)
    {
        $request->merge(['page' => floor($request->get('start') / ($request->get('length') ?: 1)) + 1]);

        $tableVentas = VentasRealizadasModel::getTableName();
        $tableFolios = FoliosModel::getTableName();
        $tableReVentaEstatus = ReVentasEstatusModel::getTableName();
        $tableEstatusVenta = EstatusVentaModel::getTableName();
        $tableClientes = ClientesModel::getTableName();
        $tableOrdenCompra = CuentasPorCobrarModel::getTableName();
        $tableEstatusCuenta = EstatusCuentaModel::getTableName();
        $tableUsuarios = User::getTableName();

        $query = $this->modelo
            ->join($tableFolios, $tableVentas . '.' . VentasRealizadasModel::FOLIO_ID, '=', $tableFolios . '.' . FoliosModel::ID)
            ->join($tableReVentaEstatus, $tableReVentaEstatus . '.' . ReVentasEstatusModel::VENTA_ID, '=', $tableVentas . '.' . VentasRealizadasModel::ID)
            ->join($tableEstatusVenta, $tableEstatusVenta . '.' . EstatusVentaModel::ID, '=', $tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID)
            ->join($tableClientes, $tableVentas . '.' . VentasRealizadasModel::CLIENTE_ID, '=', $tableClientes . '.' . ClientesModel::ID)
            ->leftJoin($tableOrdenCompra, $tableVentas . '.' . VentasRealizadasModel::FOLIO_ID, '=', $tableOrdenCompra . '.' . CuentasPorCobrarModel::FOLIO_ID)
            ->leftJoin($tableEstatusCuenta, $tableOrdenCompra . '.' . CuentasPorCobrarModel::ESTATUS_CUENTA_ID, '=', $tableEstatusCuenta . '.' . EstatusCuentaModel::ID)
            ->leftJoin($tableUsuarios, $tableVentas . '.' . VentasRealizadasModel::VENDEDOR_ID, '=', $tableUsuarios . '.' . USER::ID)
            ->select(
                $tableVentas . '.' . VentasRealizadasModel::ID,
                $tableVentas . '.' . VentasRealizadasModel::FOLIO_ID,
                $tableFolios . '.' . FoliosModel::FOLIO,
                $tableClientes . '.' . ClientesModel::NUMERO_CLIENTE,
                DB::raw("CONCAT(clientes.nombre,' ',clientes.apellido_paterno,' ',clientes.apellido_materno) as cliente"),
                $tableVentas . '.' . VentasRealizadasModel::VENTA_TOTAL,
                $tableVentas . '.' . VentasRealizadasModel::TIPO_VENTA,
                $tableEstatusVenta . '.' . EstatusVentaModel::NOMBRE . ' as estatus',
                $tableOrdenCompra . '.' . CuentasPorCobrarModel::ESTATUS_CUENTA_ID,
                $tableOrdenCompra . '.' . CuentasPorCobrarModel::CONCEPTO,
                $tableOrdenCompra . '.' . CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID,
                $tableOrdenCompra . '.' . CuentasPorCobrarModel::ID . ' as cuenta_por_cobrar_id',
                $tableEstatusCuenta . '.' . EstatusCuentaModel::NOMBRE . ' as estatus_cuenta',
                DB::raw("CONCAT(usuarios.nombre,' ',usuarios.apellido_paterno,' ',usuarios.apellido_materno) as vendedor"),
                $tableVentas . '.' . VentasRealizadasModel::VENDEDOR_ID,
                $tableVentas . '.' . VentasRealizadasModel::SUBTOTAL,
                $tableVentas . '.' . VentasRealizadasModel::IVA,
                $tableVentas . '.' . VentasRealizadasModel::CREATED_AT,
                $tableVentas . '.' . VentasRealizadasModel::CREATED_AT . ' as fecha_venta_created',
                $tableVentas . '.' . VentasRealizadasModel::FECHA_VENTA . ' as fecha_venta',
            );
            
        if ($request->get(ReVentasEstatusModel::ESTATUS_VENTA_ID)) {
            $query->where($tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID, $request->get(ReVentasEstatusModel::ESTATUS_VENTA_ID));
        } else {
            $query->whereIn($tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID, [
                EstatusVentaModel::ESTATUS_PROCESO,
                EstatusVentaModel::ESTATUS_FINALIZADA,
                EstatusVentaModel::ESTATUS_DEVOLUCION,
                EstatusVentaModel::ESTATUS_APARTADO
            ]);
        }

        if ($request->get(VentasRealizadasModel::VENDEDOR_ID) && $request->get(VentasRealizadasModel::VENDEDOR_ID) >= 1) {
            $query->where($tableVentas . '.' . VentasRealizadasModel::VENDEDOR_ID, $request->get(VentasRealizadasModel::VENDEDOR_ID));
        }
        if ($request->get(VentasRealizadasModel::CLIENTE_ID)) {
            $query->where($tableVentas . '.' . VentasRealizadasModel::CLIENTE_ID, $request->get(VentasRealizadasModel::CLIENTE_ID));
        }
        if ($request->get(VentasRealizadasModel::TIPO_VENTA)) {
            $query->where($tableVentas . '.' . VentasRealizadasModel::TIPO_VENTA, $request->get(VentasRealizadasModel::TIPO_VENTA));
        } else {
            $query->whereIn($tableVentas . '.' . VentasRealizadasModel::TIPO_VENTA, [VentasRealizadasModel::TIPO_VENTA_VENTANILLA, VentasRealizadasModel::TIPO_VENTA_VENTANILLA_TALLER]);
        }
        if ($request->get(FoliosModel::FOLIO)) {
            $query->where($tableFolios . '.' . FoliosModel::FOLIO, $request->get(FoliosModel::FOLIO));
        }
        if ($request->get(VentasRealizadasModel::FECHA_INICIO) && $request->get(VentasRealizadasModel::FECHA_FIN)) {
            // $query->whereBetween(VentasRealizadasModel::getTableName(). '.' . VentasRealizadasModel::CREATED_AT, [$request->get('fecha_inicio') . ' 00:00:00',$request->get('fecha_fin') . ' 23:59:59']);
            // $consulta->whereBetween(CorteCajaModel::getTableName(). '.' . CorteCajaModel::FECHA_TRANSACCION, [$params['fecha_inicio'] . ' 00:00:00',$params['fecha_fin'] . ' 23:59:59']);

            $query->whereDate(VentasRealizadasModel::getTableName() . '.' . VentasRealizadasModel::FECHA_VENTA, '>=', $request->get(VentasRealizadasModel::FECHA_INICIO) . ' 00:00:00');
            $query->whereDate(VentasRealizadasModel::getTableName() . '.' . VentasRealizadasModel::FECHA_VENTA, '<=', $request->get(VentasRealizadasModel::FECHA_FIN) . ' 23:59:59');
        }
        if ($request->get(VentasRealizadasModel::FECHA_INICIO) && !$request->get(VentasRealizadasModel::FECHA_FIN)) {
            $query->whereDate(VentasRealizadasModel::getTableName() . '.' . VentasRealizadasModel::FECHA_VENTA, '>=', $request->get(VentasRealizadasModel::FECHA_INICIO) . ' 00:00:00');
        }
        if (!$request->get(VentasRealizadasModel::FECHA_INICIO) && $request->get(VentasRealizadasModel::FECHA_FIN)) {
            $query->whereDate(VentasRealizadasModel::getTableName() . '.' . VentasRealizadasModel::FECHA_VENTA, '<=', $request->get(VentasRealizadasModel::FECHA_FIN) . ' 23:59:59');
        }
        $query->when($request->get(ClientesModel::NOMBRE), function ($q) use ($request) {
            $q->where(
                ClientesModel::getTableName() . '.' . ClientesModel::NOMBRE,
                'like',
                '%' . strtoupper($request->get(ClientesModel::NOMBRE)) . '%'
            );
        });
        $query->when($request->get(ClientesModel::APELLIDOS), function ($q) use ($request) {
            $q->where(
                ClientesModel::getTableName() . '.' . ClientesModel::APELLIDO_PATERNO,
                'like',
                '%' . strtoupper($request->get(ClientesModel::APELLIDOS)) . '%'
            )
                ->orWhere(
                    ClientesModel::getTableName() . '.' . ClientesModel::APELLIDO_MATERNO,
                    'like',
                    '%' . strtoupper($request->get(ClientesModel::APELLIDOS)) . '%'
                );
        });
        $query->where($tableReVentaEstatus . '.' . ReVentasEstatusModel::ACTIVO, true);
        $query->orderBy($tableVentas . '.' . VentasRealizadasModel::ID, 'DESC');
        if ($request->get('detalle_producto')) {
            $query->with(VentasRealizadasModel::REL_DETALLE_VENTA . '.' .  VentaProductoModel::REL_PRODUCTO);
            return $query->get();
        }
        //$query->get();
        //$query->paginate($request->length);
        //dd(DB::getQueryLog());

        return $query->paginate($request->length);
    }

    public function getServicioByFolio($request)
    {

        $tableServicios = VentaServicioModel::getTableName();
        $tableVentas = VentasRealizadasModel::getTableName();
        $tableFolios = FoliosModel::getTableName();
        $tableReVentaEstatus = ReVentasEstatusModel::getTableName();
        $tableEstatusVenta = EstatusVentaModel::getTableName();
        $tableClientes = ClientesModel::getTableName();

        $query = $this->modeloVentaServicio
            ->join($tableVentas, $tableVentas . '.' . VentasRealizadasModel::ID, '=', $tableServicios . '.' . VentaServicioModel::VENTA_ID)
            ->join($tableFolios, $tableVentas . '.' . VentasRealizadasModel::FOLIO_ID, '=', $tableFolios . '.' . FoliosModel::ID)
            ->join($tableReVentaEstatus, $tableReVentaEstatus . '.' . ReVentasEstatusModel::VENTA_ID, '=', $tableVentas . '.' . VentasRealizadasModel::ID)
            ->join($tableEstatusVenta, $tableEstatusVenta . '.' . EstatusVentaModel::ID, '=', $tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID)
            ->join($tableClientes, $tableVentas . '.' . VentasRealizadasModel::CLIENTE_ID, '=', $tableClientes . '.' . ClientesModel::ID)
            ->select(
                $tableServicios . '.' . '*',
                $tableVentas . '.' . VentasRealizadasModel::FOLIO_ID,
                $tableFolios . '.' . FoliosModel::FOLIO,
                $tableClientes . '.' . ClientesModel::NUMERO_CLIENTE,
                $tableClientes . '.' . ClientesModel::NOMBRE . ' as cliente',
                $tableVentas . '.' . VentasRealizadasModel::TIPO_VENTA,
                $tableEstatusVenta . '.' . EstatusVentaModel::NOMBRE . ' as estatus',
                $tableVentas . '.' . VentasRealizadasModel::CREATED_AT
            );

        if ($request->get(VentasRealizadasModel::FOLIO_ID)) {
            $query->where($tableVentas . '.' . VentasRealizadasModel::FOLIO_ID, $request->get(VentasRealizadasModel::FOLIO_ID));
        }
        $query->where($tableReVentaEstatus . '.' . ReVentasEstatusModel::ACTIVO, true);


        return $query->get();
    }

    public function getAllVentas($request)
    {
        $tableVentas = VentasRealizadasModel::getTableName();
        $tableFolios = FoliosModel::getTableName();
        $tableReVentaEstatus = ReVentasEstatusModel::getTableName();
        $tableEstatusVenta = EstatusVentaModel::getTableName();
        $tableClientes = ClientesModel::getTableName();
        $tableCxc = CuentasPorCobrarModel::getTableName();

        $query = $this->modelo
            ->join($tableFolios, $tableVentas . '.' . VentasRealizadasModel::FOLIO_ID, '=', $tableFolios . '.' . FoliosModel::ID)
            ->join($tableReVentaEstatus, $tableReVentaEstatus . '.' . ReVentasEstatusModel::VENTA_ID, '=', $tableVentas . '.' . VentasRealizadasModel::ID)
            ->join($tableEstatusVenta, $tableEstatusVenta . '.' . EstatusVentaModel::ID, '=', $tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID)
            ->join($tableClientes, $tableVentas . '.' . VentasRealizadasModel::CLIENTE_ID, '=', $tableClientes . '.' . ClientesModel::ID)
            ->leftJoin($tableCxc, $tableVentas . '.' . VentasRealizadasModel::FOLIO_ID, '=', $tableCxc . '.' . CuentasPorCobrarModel::FOLIO_ID)
            ->select(
                $tableVentas . '.' . VentasRealizadasModel::ID,
                $tableVentas . '.' . VentasRealizadasModel::FOLIO_ID,
                $tableFolios . '.' . FoliosModel::FOLIO,
                $tableClientes . '.' . ClientesModel::NUMERO_CLIENTE,
                $tableClientes . '.' . ClientesModel::NOMBRE . ' as cliente',
                $tableVentas . '.' . VentasRealizadasModel::VENTA_TOTAL,
                $tableVentas . '.' . VentasRealizadasModel::TIPO_VENTA,
                $tableEstatusVenta . '.' . EstatusVentaModel::NOMBRE . ' as estatus',
                $tableVentas . '.' . VentasRealizadasModel::CREATED_AT
            );

        $query->whereIn($tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID, array(EstatusVentaModel::ESTATUS_FINALIZADA, EstatusVentaModel::FACTURA_PAGADA));

        if ($request->get(VentasRealizadasModel::CLIENTE_ID)) {
            $query->where($tableVentas . '.' . VentasRealizadasModel::CLIENTE_ID, $request->get(VentasRealizadasModel::CLIENTE_ID));
        }
        if ($request->get(FoliosModel::FOLIO)) {
            $query->whereRaw('LOWER(' . $tableFolios . '.' . FoliosModel::FOLIO . ') LIKE (?) ', ["%{$request->get(FoliosModel::FOLIO)}%"]);
        }
        $query->where($tableReVentaEstatus . '.' . ReVentasEstatusModel::ACTIVO, true);

        return $query->get();
    }

    public function getBusquedaVentasDevoluciones($request)
    {
        $tableVentas = VentasRealizadasModel::getTableName();
        $tableFolios = FoliosModel::getTableName();
        $tableReVentaEstatus = ReVentasEstatusModel::getTableName();
        $tableEstatusVenta = EstatusVentaModel::getTableName();
        $tableClientes = ClientesModel::getTableName();
        $tableDevolucionVenta = DevolucionVentasModel::getTableName();
        $query = $this->modelo
            ->join($tableDevolucionVenta, $tableVentas . '.' . VentasRealizadasModel::ID, '=', $tableDevolucionVenta . '.' . DevolucionVentasModel::VENTA_ID)
            ->join($tableFolios, $tableDevolucionVenta . '.' . DevolucionVentasModel::FOLIO_ID, '=', $tableFolios . '.' . FoliosModel::ID)
            ->join($tableReVentaEstatus, $tableReVentaEstatus . '.' . ReVentasEstatusModel::VENTA_ID, '=', $tableVentas . '.' . VentasRealizadasModel::ID)
            ->join($tableEstatusVenta, $tableEstatusVenta . '.' . EstatusVentaModel::ID, '=', $tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID)
            ->join($tableClientes, $tableVentas . '.' . VentasRealizadasModel::CLIENTE_ID, '=', $tableClientes . '.' . ClientesModel::ID)
            ->select(
                $tableVentas . '.' . VentasRealizadasModel::ID,
                $tableDevolucionVenta . '.' . DevolucionVentasModel::FOLIO_ID,
                $tableFolios . '.' . FoliosModel::FOLIO,
                $tableClientes . '.' . ClientesModel::NUMERO_CLIENTE,
                $tableClientes . '.' . ClientesModel::NOMBRE . ' as cliente',
                $tableVentas . '.' . VentasRealizadasModel::VENTA_TOTAL,
                $tableVentas . '.' . VentasRealizadasModel::TIPO_VENTA,
                $tableEstatusVenta . '.' . EstatusVentaModel::NOMBRE . ' as estatus',
                $tableVentas . '.' . VentasRealizadasModel::CREATED_AT
            );
        if ($request->get(ReVentasEstatusModel::ESTATUS_VENTA_ID)) {
            $query->where($tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID, $request->get(ReVentasEstatusModel::ESTATUS_VENTA_ID));
        } else {
            $query->whereIn($tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID, array(EstatusVentaModel::ESTATUS_FINALIZADA, EstatusVentaModel::ESTATUS_DEVOLUCION));
        }
        if ($request->get(VentasRealizadasModel::CLIENTE_ID)) {
            $query->where($tableVentas . '.' . VentasRealizadasModel::CLIENTE_ID, $request->get(VentasRealizadasModel::CLIENTE_ID));
        }
        if ($request->get(FoliosModel::FOLIO)) {
            $query->whereRaw('LOWER(' . $tableFolios . '.' . FoliosModel::FOLIO . ') LIKE (?) ', ["%{$request->get(FoliosModel::FOLIO)}%"]);
        }
        $query->where($tableReVentaEstatus . '.' . ReVentasEstatusModel::ACTIVO, true);

        return $query->get();
    }

    //TODO: Ventas
    public function reglasAutorizarPrecio()
    {
        return [
            permisoVentaModel::USUARIO_ID => 'required',
            // permisoVentaModel::ven =>'required',
        ];
    }

    public function finalizarVentaServicio($parametros)
    {
        $venta = $this->buscarPorNumeroOrden($parametros[VentasRealizadasModel::NUMERO_ORDEN]);
        if (empty($venta)) {
            throw new ParametroHttpInvalidoException([
                'msg' => "No se encontro la orden"
            ]);
        } else if (isset($venta) && $venta->orden_cerrada == 1) {
            throw new ParametroHttpInvalidoException([
                'msg' => "Orden cerrada"
            ]);
        }

        $total_venta = $this->servicioVentaProducto->totalVentaByFolio($venta->folio_id);
        $modelo = $this->modelo->findOrFail($venta->id);
        $modelo->fill([VentasRealizadasModel::ORDEN_CERRADA => true]);
        $modelo = $this->guardarModelo($modelo);
        $this->servicioCuentaxCobrar->procesarCuentasPorCobrar([
            VentasRealizadasModel::VENTA_TOTAL => $total_venta,
            CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID => TipoFormaPagoModel::FORMA_CONTADO,
            CuentasPorCobrarModel::FOLIO_ID => $venta->folio_id,
            CuentasPorCobrarModel::CLIENTE_ID => $venta->cliente_id,
            CuentasPorCobrarModel::CONCEPTO => "CIERRE DE ORDEN",
            CuentasPorCobrarModel::TIPO_PAGO_ID => TipoFormaPagoModel::FORMA_CONTADO,
            CuentasPorCobrarModel::PLAZO_CREDITO_ID => PlazoCreditoModel::UNA_EXHIBICION,
            CuentasPorCobrarModel::ENGANCHE => null,
            CuentasPorCobrarModel::TASA_INTERES => null,
            CuentasPorCobrarModel::ESTATUS_CUENTA_ID => EstatusCuentaModel::ESTATUS_LIQUIDADO
        ]);


        $this->servicioReVentasEstatus->storeReVentasEstatus([
            ReVentasEstatusModel::VENTA_ID => $venta->id,
            ReVentasEstatusModel::ESTATUS_VENTA_ID => EstatusVentaModel::ESTATUS_FINALIZADA
        ]);

        DB::commit();
        return $venta;
    }

    public function abrirOrdenServicio($parametros)
    {
        $venta = $this->buscarPorNumeroOrden($parametros[VentasRealizadasModel::NUMERO_ORDEN]);

        $modelo = $this->modelo->findOrFail($venta->id);
        $modelo->fill([VentasRealizadasModel::ORDEN_CERRADA => false]);
        $modelo = $this->guardarModelo($modelo);


        $this->servicioReVentasEstatus->storeReVentasEstatus([
            ReVentasEstatusModel::VENTA_ID => $venta->id,
            ReVentasEstatusModel::ESTATUS_VENTA_ID => EstatusVentaModel::ESTATUS_PROCESO
        ]);
    }

    public function buscarPorNumeroOrden($numero_orden)
    {
        return $this->modelo->where(VentasRealizadasModel::NUMERO_ORDEN, $numero_orden)->first();
    }

    public function getYearVentasByProducto($params)
    {
        $tableVentas = VentasRealizadasModel::getTableName();
        $tableVentaProducto = VentaProductoModel::getTableName();
        $tableReVentaEstatus = ReVentasEstatusModel::getTableName();
        $fecha_actual = date('Y-m-d');
        //DB::connection()->enableQueryLog();
        $query = DB::table($tableVentas)
            ->join($tableVentaProducto, $tableVentas . '.' . VentasRealizadasModel::ID, '=', $tableVentaProducto . '.' . VentaProductoModel::VENTA_ID)
            ->join($tableReVentaEstatus, $tableReVentaEstatus . '.' . ReVentasEstatusModel::VENTA_ID, '=', $tableVentas . '.' . VentasRealizadasModel::ID)
            ->select(
                DB::raw('EXTRACT(YEAR FROM ' . $tableVentas . '.' . VentasRealizadasModel::CREATED_AT . ') as anio')
            );
        if (isset($params[VentaProductoModel::PRODUCTO_ID]) && $params[VentaProductoModel::PRODUCTO_ID]) {
            $query->where($tableVentaProducto . '.' . VentaProductoModel::PRODUCTO_ID, $params[VentaProductoModel::PRODUCTO_ID]);
        }
        if (isset($params[VentasRealizadasModel::YEAR]) && $params[VentasRealizadasModel::YEAR]) {
            $query->whereYear($tableVentas . '.' . VentasRealizadasModel::CREATED_AT, $params[VentasRealizadasModel::YEAR]);
        }
        // if (isset($params[VentasRealizadasModel::CANTIDAD_MESES]) && $params[VentasRealizadasModel::CANTIDAD_MESES]) {
        //     $fecha_inicial = date("Y-m-d", strtotime($fecha_actual . " - " . $params[VentasRealizadasModel::CANTIDAD_MESES] . " month"));
        //     $query->whereBetween(VentaProductoModel::getTableName() . '.' . VentaProductoModel::CREATED_AT, [
        //         $fecha_inicial . ' 00:00:00',
        //         $fecha_actual . ' 23:59:59'
        //     ]);
        // }

        $query->where($tableReVentaEstatus . '.' . ReVentasEstatusModel::ACTIVO, true);
        $query->whereIn($tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID, array(EstatusVentaModel::ESTATUS_FINALIZADA));
        $query->groupBy(
            'producto_id',
            'anio'
        );
        return $query->get();
        //dd(DB::getQueryLog());
    }

    public function getVentasAgrupadasMes($params)
    {
        $where = '';
        $where_mes = '';

        $fecha_actual = date('Y-m-d');
        $mes_actual = Carbon::parse($fecha_actual)->format('m');
        $mes_actual = intval($mes_actual);

        $fecha_actual = date('Y-m-d', strtotime($fecha_actual . ' +1 day'));

        if (isset($params[VentasRealizadasModel::CANTIDAD_MESES])) {
            // dd($params[VentasRealizadasModel::CANTIDAD_MESES] , $mes_actual);
            // if ($params[VentasRealizadasModel::CANTIDAD_MESES] > $mes_actual) {
            //     throw new ParametroHttpInvalidoException([
            //         'msg' => "No puedes sobrepasar la cantidad máxima al mes inicial"
            //     ]);
            // }

            $fecha_inicio = date("Y-m-d", strtotime($fecha_actual . " - " . $params[VentasRealizadasModel::CANTIDAD_MESES] . " month"));
            $mes_inicio = date("Y-m-d", strtotime($fecha_actual . " - " . $params[VentasRealizadasModel::CANTIDAD_MESES] . " month"));
            $mes_inicio = Carbon::parse($mes_inicio)->format('m');
            $mes_inicio = intval($mes_inicio);
            // dd($mes_inicio);
            // $mes_inicio = intval($mes_actual) - ($params[VentasRealizadasModel::CANTIDAD_MESES] + 1);
            // dd(intval($mes_actual), $mes_inicio, $fecha_inicio, $fecha_actual);

            $mes_inicio = abs($mes_inicio);

            $where_mes .= ($fecha_inicio) ? " where cat_meses.id between '{$mes_inicio}' and '{$mes_actual}'" : "";
            $where .= ($fecha_inicio) ? " and ventas.created_at between '{$fecha_inicio}' and '{$fecha_actual}'" : "";
        }

        $estatus = EstatusVentaModel::ESTATUS_FINALIZADA;
        $sql = "SELECT cat_meses.nombre as mes, T1.total_ventas, T1.venta_total
                FROM cat_meses
                LEFT JOIN(
                SELECT
                    SUM (venta_producto.cantidad) AS total_ventas,
                    SUM (venta_producto.venta_total) AS venta_total,
                    EXTRACT (MONTH FROM ventas.created_at) AS mes
                FROM ventas
                INNER JOIN venta_producto ON ventas.id = venta_producto.venta_id
                INNER JOIN re_ventas_estatus ON re_ventas_estatus.ventas_id = ventas.id
                WHERE venta_producto.producto_id = {$params[VentaProductoModel::PRODUCTO_ID]}
                AND re_ventas_estatus.activo = true
                AND re_ventas_estatus.estatus_ventas_id IN ($estatus) 
                $where 
                GROUP BY producto_id, mes ) T1 ON T1.mes::integer  = cat_meses.id::integer $where_mes";
        // dd($sql);
        return DB::select(DB::raw($sql));
    }

    public function getVentasAgrupadasMes2($params)
    {
        $where = '';
        $where_mes = '';

        $fecha_actual = date('Y-m-d');
        $mes_actual = Carbon::parse($fecha_actual)->format('m');
        $mes_actual = intval($mes_actual);

        $fecha_actual = date('Y-m-d', strtotime($fecha_actual . ' +1 day'));

        if (isset($params[VentasRealizadasModel::CANTIDAD_MESES])) {
            // dd($params[VentasRealizadasModel::CANTIDAD_MESES] , $mes_actual);
            // if ($params[VentasRealizadasModel::CANTIDAD_MESES] > $mes_actual) {
            //     throw new ParametroHttpInvalidoException([
            //         'msg' => "No puedes sobrepasar la cantidad máxima al mes inicial"
            //     ]);
            // }

            $fecha_inicio = date("Y-m-d", strtotime($fecha_actual . " - " . $params[VentasRealizadasModel::CANTIDAD_MESES] . " month"));
            $mes_inicio = date("Y-m-d", strtotime($fecha_actual . " - " . $params[VentasRealizadasModel::CANTIDAD_MESES] . " month"));
            $mes_inicio = Carbon::parse($mes_inicio)->format('m');
            $mes_inicio = intval($mes_inicio);
            $mes_inicio = abs($mes_inicio);

            // dd($mes_inicio);
            // $mes_inicio = intval($mes_actual) - ($params[VentasRealizadasModel::CANTIDAD_MESES] + 1);
            // dd(intval($mes_actual), $mes_inicio, $fecha_inicio, $fecha_actual);
            // dd(($mes_actual - $mes_inicio),$mes_inicio,  $mes_actual);
            // dd($mes_actual);
            $array_meses = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
            rsort($array_meses);

            $rangoMeses = "(";

            foreach ($array_meses as $key => $mes) {
                if ($mes >= $mes_inicio) {
                    $rangoMeses .= "$mes,";
                }

                if ($mes_actual > $mes) {
                    $rangoMeses .= "$mes,";
                }
            }
            $rangoMeses .= ")";
            $rangoMeses = str_replace(",)", ")", $rangoMeses);

            $where_mes .= ($fecha_inicio) ? " where cat_meses.id in $rangoMeses" : "";
            $where .= ($fecha_inicio) ? " and ventas.created_at between '{$fecha_inicio}' and '{$fecha_actual}'" : "";
        }

        $estatus = EstatusVentaModel::ESTATUS_FINALIZADA;
        $sql = "SELECT cat_meses.nombre as mes, T1.total_ventas, T1.venta_total
                FROM cat_meses
                LEFT JOIN(
                SELECT
                    SUM (venta_producto.cantidad) AS total_ventas,
                    SUM (venta_producto.venta_total) AS venta_total,
                    EXTRACT (MONTH FROM ventas.created_at) AS mes
                FROM ventas
                INNER JOIN venta_producto ON ventas.id = venta_producto.venta_id
                INNER JOIN re_ventas_estatus ON re_ventas_estatus.ventas_id = ventas.id
                WHERE venta_producto.producto_id = {$params[VentaProductoModel::PRODUCTO_ID]}
                AND re_ventas_estatus.activo = true
                AND re_ventas_estatus.estatus_ventas_id IN ($estatus) 
                $where GROUP BY producto_id, mes ) T1 ON T1.mes::integer  = cat_meses.id::integer $where_mes";

        // dd($sql);
        return DB::select(DB::raw($sql));
    }

    public function getVentasByUser($user_id)
    {

        $tabla_venta = VentasRealizadasModel::getTableName();
        $tabla_venta_producto = VentaProductoModel::getTableName();
        $tabla_producto = ProductosModel::getTableName();

        $query = $this->modelo->select(
            $tabla_venta_producto . '.' . VentaProductoModel::CANTIDAD,
            $tabla_venta_producto . '.' . VentaProductoModel::VALOR_UNITARIO,
            $tabla_producto . '.' . ProductosModel::DESCRIPCION,
            $tabla_producto . '.' . ProductosModel::NO_IDENTIFICACION_FACTURA,
            $tabla_producto . '.' . ProductosModel::PREFIJO,
            $tabla_producto . '.' . ProductosModel::SUFIJO,
            $tabla_producto . '.' . ProductosModel::BASICO,
            DB::raw('sum(' . $tabla_venta . '.' . VentasRealizadasModel::VENTA_TOTAL . ') as venta_total'),
        )->from($tabla_venta);
        $query->join($tabla_venta_producto, $tabla_venta_producto . '.' . VentaProductoModel::VENTA_ID, '=', $tabla_venta . '.' . VentasRealizadasModel::ID);
        $query->join($tabla_producto, $tabla_producto . '.' . ProductosModel::ID, '=', $tabla_venta_producto . '.' . VentaProductoModel::PRODUCTO_ID);

        //$query->where($tabla_venta . '.' . VentasRealizadasModel::VENDEDOR_ID, $user_id);

        $query->groupBy(
            $tabla_venta_producto . '.' . VentaProductoModel::CANTIDAD,
            $tabla_venta_producto . '.' . VentaProductoModel::VALOR_UNITARIO,
            $tabla_producto . '.' . ProductosModel::DESCRIPCION,
            $tabla_producto . '.' . ProductosModel::NO_IDENTIFICACION_FACTURA,
            $tabla_producto . '.' . ProductosModel::PREFIJO,
            $tabla_producto . '.' . ProductosModel::SUFIJO,
            $tabla_producto . '.' . ProductosModel::BASICO
        );
        return $query->get();
    }

    //archivo que se envia a ford mensualmente
    public function generarlayoutford($accion = "inventario")
    {
        $folder = 'layout_ford';
        switch ($accion) {
            case 'inventario':
                $file_name = 'vip_inv_mex_' . date('Ymd') . ".txt";
                $this->servicioManejoArchivos->setDirectory("$folder");
                $storage = Storage::disk('local');
                $storage->prepend($file_name, "HDRDEALERIN003504VIPIN211116193324"); //Encabezado

                $productos =  $this->servicioProductos->getAllProductos([]);
                foreach ($productos as $key => $producto) {
                    $descripcion = str_replace(' ', '-', trim($producto->descripcion));
                    $descripcion =  $this->validarEspacios($descripcion, 40, " ", 1); //$tototal_desc <= 40 ? $descripcion.str_repeat(" ", (40 - $tototal_desc)) : substr($descripcion, 0, 40);

                    $gpo =  $this->validarEspacios($producto->gpo, 10, " ", 1);
                    $prefijo = $this->validarEspacios($producto->prefijo, 25, " ", 1);
                    $basico = $this->validarEspacios($producto->basico, 25, " ", 1);
                    $sufijo = $this->validarEspacios($producto->sufijo, 25, " ", 1);
                    $producto_id = $this->validarEspacios($producto->id, 10, " ", 1);
                    $ubicacion_producto = $this->validarEspacios($producto->ubicacion->nombre, 10, " ", 1);
                    $num_pieza_completo = $this->validarEspacios("$producto->prefijo$producto->basico$producto->sufijo", 75, " ", 1);
                    $cantidad_stock = $this->servicioDesgloseProductos->getStockByProducto(['producto_id' => $producto_id]);
                    $precio_costo =  $this->validarEspacios(floatval($producto->precio_factura), 10, 0, 2);
                    $precio_publico = $this->validarEspacios(round(floatval($producto->valor_unitario), 2), 10, 0, 2);
                    $ventas_meses = $this->getVentasAgrupadasMes2([VentasRealizadasModel::CANTIDAD_MESES => 6, VentaProductoModel::PRODUCTO_ID => $producto_id]);
                    //6 meses atras de fecha actual
                    $mes1 = !empty($ventas_meses[5]->total_ventas) ? $this->validarEspacios($ventas_meses[5]->total_ventas, 10, "0") : $this->validarEspacios(0, 10, "0");
                    $mes2 = !empty($ventas_meses[4]->total_ventas) ? $this->validarEspacios($ventas_meses[4]->total_ventas, 10, "0") : $this->validarEspacios(0, 10, "0");
                    $mes3 = !empty($ventas_meses[3]->total_ventas) ? $this->validarEspacios($ventas_meses[3]->total_ventas, 10, "0") : $this->validarEspacios(0, 10, "0");
                    $mes4 = !empty($ventas_meses[2]->total_ventas) ? $this->validarEspacios($ventas_meses[2]->total_ventas, 10, "0") : $this->validarEspacios(0, 10, "0");
                    $mes5 = !empty($ventas_meses[1]->total_ventas) ? $this->validarEspacios($ventas_meses[1]->total_ventas, 10, "0") : $this->validarEspacios(0, 10, "0");
                    $mes6 = !empty($ventas_meses[0]->total_ventas) ? $this->validarEspacios($ventas_meses[0]->total_ventas, 10, "0") : $this->validarEspacios(0, 10, "0");

                    $compra = $this->servicioOrdenCompra->ultimacompraByProducto($producto->id);
                    $ultima_compra_pieza = isset($compra->created_at) ?  $this->validarEspacios(date('ymd', $compra->created_at), 8, "0") : '00000000';
                    $existencia_producto = $this->validarEspacios($cantidad_stock->cantidad_actual, 10, 0);
                    $strLine = "$gpo$prefijo$basico$sufijo$num_pieza_completo$descripcion$ultima_compra_pieza$ubicacion_producto$mes1$mes2$mes3$mes4$mes5$mes6$precio_costo$existencia_producto$precio_publico";
                    $storage->prepend($file_name, $strLine); //Fin de archivo
                    $storage->prepend($file_name, "TRLDEALERIN0035040000008735"); //Fin de archivo
                    return $file_name;
                }
                break;
            case 'salidas':
                $file_name = 'vip_sal_mex_137' . date('Ymd') . ".txt";
                $this->servicioManejoArchivos->setDirectory("$folder");
                $storage = Storage::disk('local');
                $storage->prepend($file_name, "HDRDEALERSA003489VIPSA211116193407"); //Encabezado
                $storage->prepend($file_name, "IREMEX137       20211116193407"); //Encabezado

                $ventas = $this->getVentasReporte6MesesAtras();
                // dd($ventas);
                foreach ($ventas as $key => $producto) {
                    // $gpo =  $this->validarEspacios($producto->gpo, 10, " ", 1);
                    $prefijo = $this->validarEspacios($producto->prefijo, 26, " ", 1);
                    $basico = $this->validarEspacios($producto->basico, 26, " ", 1);
                    $sufijo = $this->validarEspacios($producto->sufijo, 51, " ", 1);
                    $num_pieza_completo = $this->validarEspacios("$producto->gpo$producto->prefijo$producto->basico$producto->sufijo", 79, " ", 1);

                    $descripcion = str_replace(' ', '-', trim($producto->descripcion));
                    $descripcion =  $this->validarEspacios($descripcion, 41, " ", 1);

                    $clave_stars =  $this->validarEspacios("001044345", 11, " ", 1);

                    $numero_cliente = $this->validarEspacios($producto->numero_cliente, 41, " ", 1);
                    $nombre_completo_cliente = "$producto->nombre_cliente $producto->apellido_paterno $producto->apellido_Materno";
                    $cliente = $this->validarEspacios($nombre_completo_cliente, 101, " ", 1);
                    $vendedor = $this->validarEspacios($producto->nombre_vendedor, 41, " ", 1);
                    $rfc = $this->validarEspacios($producto->rfc, 15, " ", 1);
                    $cantidad = $this->validarEspacios($producto->cantidad, 9, "0", 2);
                    $precio_costo = $this->validarEspacios(round(floatval($producto->precio_costo), 2), 12, "0", 2);
                    $precio_factura = $this->validarEspacios(round(floatval($producto->precio_factura), 2), 12, "0", 2);
                    $date = \Carbon\Carbon::parse($producto->created_at);
                    $fecha_venta = $this->validarEspacios($date->format('Ymd'), 11, "0", 2);
                    $folio_id = $this->validarEspacios($producto->folio_id, 15, '');
                    $nci = $this->validarEspacios("", 15, ' ');
                    $tipo_venta = $this->validarEspacios($producto->tipo_venta_id, 1, '');
                    $iva = $this->validarEspacios(round(0.0, 2), 12, '0', 1);
                    $cp = $this->validarEspacios($producto->codigo_postal, 8, '0', 2);
                    $strLine = "$num_pieza_completo$prefijo$basico$sufijo$descripcion$clave_stars$vendedor$numero_cliente$cliente$rfc$cantidad$precio_factura$precio_costo$fecha_venta$folio_id$nci$tipo_venta$iva$cp";
                    $storage->prepend($file_name, $strLine); //Escribe linea

                }
                $storage->prepend($file_name, "TRLDEALERSA0034890000000071"); //Fin de archivo
                return $file_name;
                break;
            case 'compras':
                $file_name = 'vip_sal_mex_137' . date('Ymd') . ".txt";
                $this->servicioManejoArchivos->setDirectory("$folder");
                $storage = Storage::disk('local');
                $storage->prepend($file_name, "HDRDEALERPU003460VIPPU211116193412"); //Encabezado
                $storage->prepend($file_name, "IREMEX137       20211116193412"); //Encabezado
                $ordern_compra = $this->servicioOrdenCompra->listado([]);

                if (!empty($ordern_compra)) {
                    foreach ($ordern_compra as $key => $compra) {
                        $productos_orden_compra = $this->servicioListaProductosOrdenCompra->productosByOrdenCompra($compra->id);
                        foreach ($productos_orden_compra as $key => $item_producto) {
                            $prefijo = $this->validarEspacios($item_producto->prefijo, 26, " ", 1);
                            $basico = $this->validarEspacios($item_producto->basico, 26, " ", 1);
                            $sufijo = $this->validarEspacios($item_producto->sufijo, 51, " ", 1);
                            $descripcion = $this->validarEspacios($item_producto->descripcion, 51, " ", 1);
                            $numero_proveedor =  $this->validarEspacios($item_producto->proveedor_id, 10, " ", 1);
                            $proveedor = $this->validarEspacios($item_producto->proveedor_nombre, 101, " ", 1);
                            $rfc =  $this->validarEspacios($item_producto->proveedor_rfc, 15, " ", 1);
                            $cantidad = $this->validarEspacios($item_producto->proveedor_rfc, 9, "0", 1);
                            $compra_total = $this->validarEspacios(round(floatval($item_producto->total), 2), 9, "0", 1);

                            $fecha_compra = $this->validarEspacios(date('Ymd', strtotime($item_producto->created_at)), 6, "", 2);
                            $folio_factura = $this->validarEspacios($compra->folioFactura, 11, " ", 1);

                            $iva = "0.0";
                            $iva = $this->validarEspacios(round(floatval($iva), 2), 9, "0", 2);
                            $tipo_compra = "2";
                            $clave = "FO";
                            $num_pieza_completo = $this->validarEspacios("$item_producto->gpo$item_producto->prefijo$item_producto->basico$item_producto->sufijo", 79, " ", 1);
                            $strLine = "$num_pieza_completo$prefijo$basico$sufijo$descripcion$numero_proveedor$proveedor$rfc$cantidad$compra_total$fecha_compra$folio_factura$iva$tipo_compra$clave";
                            $storage->prepend($file_name, $strLine); //Escribe linea
                        }
                    }
                }

                $storage->prepend($file_name, "TRLDEALERPU0034600000000005"); //Fin de archivo
                return $file_name;
                break;
            case 'otro4':
                $file_name = 'vip_sal_mex_137' . date('Ymd') . ".txt";
                $this->servicioManejoArchivos->setDirectory("$folder");
                $storage = Storage::disk('local');
                $storage->prepend($file_name, "HDRDEALERLM002381VIPLM211116193417"); //Encabezado
                $storage->prepend($file_name, "IREMEX137       20211116193417"); //Encabezado
                $storage->prepend($file_name, "TRLDEALERLM0023810000000003"); //pie
                break;
            default:
                break;
        }
    }

    //1-despues 1-antes
    public function validarEspacios($Val, $cantidad_espacios, $elemento_remplazo = ' ', $antes_despues = 2)
    {
        return strlen($Val) <= $cantidad_espacios ? ($antes_despues == 1 ? $Val . str_repeat($elemento_remplazo,  $cantidad_espacios - strlen($Val)) : str_repeat($elemento_remplazo,  $cantidad_espacios - strlen($Val)) . $Val)  :  substr($Val, 0, $cantidad_espacios);
    }


    //FILTRO POR PRODUCTO
    public function getVentasByProducto($producto_id, $meses = '1')
    {

        $tableVentas = VentasRealizadasModel::getTableName();
        $tabla_venta_producto = VentaProductoModel::getTableName();
        $tableReVentaEstatus = ReVentasEstatusModel::getTableName();

        $query = $this->modelo
            ->join($tableReVentaEstatus, $tableReVentaEstatus . '.' . ReVentasEstatusModel::VENTA_ID, '=', $tableVentas . '.' . VentasRealizadasModel::ID)
            ->join($tabla_venta_producto, $tableVentas . '.' . VentasRealizadasModel::ID, '=', $tabla_venta_producto . '.' . VentaProductoModel::VENTA_ID)
            ->select(
                DB::raw('SUM( venta_producto.venta_total ) AS venta_total'),
                DB::raw('SUM ( venta_producto.cantidad ) AS cantidad')
            );

        $query->whereIn($tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID, array(EstatusVentaModel::ESTATUS_FINALIZADA, EstatusVentaModel::FACTURA_PAGADA));
        $query->where($tableReVentaEstatus . '.' . ReVentasEstatusModel::ACTIVO, true);
        $query->where($tabla_venta_producto . '.' . VentaProductoModel::PRODUCTO_ID, '=', $producto_id);
        $query->whereMonth($tableVentas . '.' . VentasRealizadasModel::CREATED_AT, '=',  date("m", strtotime(date('Y-m-d') . "- $meses month")));
        return $query->first();
    }

    //TODAS LAS VENTAS
    public function getVentasReporte6MesesAtras()
    {
        $fecha_desde = date("Y-m-d", strtotime(date('Y-m-d') . "- 6 month"));
        $fecha_hasta = date("Y-m-d", strtotime(date('Y-m-d') . "+ 1 day")); //date('Y-m-d');

        $tableVentas = VentasRealizadasModel::getTableName();
        $tabla_venta_producto = VentaProductoModel::getTableName();
        $tableReVentaEstatus = ReVentasEstatusModel::getTableName();
        $tabla_producto = ProductosModel::getTableName();
        $ubicacion_producto = CatalogoUbicacionProductoModel::getTableName();
        $tableUsuario = User::getTableName();
        $tableCliente = ClientesModel::getTableName();

        $query = $this->modelo
            ->join($tabla_venta_producto, $tableVentas . '.' . VentasRealizadasModel::ID, '=', $tabla_venta_producto . '.' . VentaProductoModel::VENTA_ID)
            ->join($tabla_producto,  $tabla_venta_producto . '.' . VentaProductoModel::PRODUCTO_ID, '=', $tabla_producto . '.' . ProductosModel::ID)
            ->join($ubicacion_producto,  $tabla_producto . '.' . ProductosModel::UBICACION_PRODUCTO_ID, '=', $ubicacion_producto . '.' . CatalogoUbicacionProductoModel::ID)
            ->join($tableReVentaEstatus, $tableReVentaEstatus . '.' . ReVentasEstatusModel::VENTA_ID, '=', $tableVentas . '.' . VentasRealizadasModel::ID)
            ->join($tableUsuario, $tableUsuario . '.' . User::ID, '=', $tableVentas . '.' . VentasRealizadasModel::VENDEDOR_ID)
            ->join($tableCliente . ' as cliente', 'cliente.' . ClientesModel::ID, '=', $tableVentas . '.' . VentasRealizadasModel::CLIENTE_ID)
            ->select(
                $tabla_producto . '.' . ProductosModel::ID,
                $tabla_producto . '.' . ProductosModel::DESCRIPCION,
                $tabla_producto . '.' . ProductosModel::NO_IDENTIFICACION_FACTURA,
                $tabla_producto . '.' . ProductosModel::PREFIJO,
                $tabla_producto . '.' . ProductosModel::SUFIJO,
                $tabla_producto . '.' . ProductosModel::GPO,
                $tabla_producto . '.' . ProductosModel::BASICO,
                $tabla_producto . '.' . ProductosModel::PRECIO_FACTURA,
                $tabla_producto . '.' . ProductosModel::VALOR_UNITARIO,
                $tabla_venta_producto . '.' . VentaProductoModel::CANTIDAD,
                $tableUsuario . '.' . User::NOMBRE . ' as nombre_vendedor',
                $tableUsuario . '.' . User::APELLIDO_MATERNO . ' as apellido_materno_vendedor',
                $tableUsuario . '.' . User::APELLIDO_PATERNO . ' as apellido_paterno_vendedor',
                'cliente.' . ClientesModel::NOMBRE . ' as nombre_cliente',
                'cliente.' . ClientesModel::APELLIDO_PATERNO . ' as apellido_paterno_cliente',
                'cliente.' . ClientesModel::APELLIDO_MATERNO . ' as apellido_materno_cliente',
                'cliente.' . ClientesModel::NUMERO_CLIENTE,
                'cliente.' . ClientesModel::RFC,
                'cliente.' . ClientesModel::CODIGO_POSTAL,
                $ubicacion_producto . '.' . CatalogoUbicacionProductoModel::NOMBRE . ' as ubicacion',
                $tableVentas . '.' . VentasRealizadasModel::CREATED_AT,
                $tableVentas . '.' . VentasRealizadasModel::FOLIO_ID,
                $tableVentas . '.' . VentasRealizadasModel::TIPO_VENTA
            );

        $query->whereIn($tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID, array(EstatusVentaModel::ESTATUS_FINALIZADA, EstatusVentaModel::FACTURA_PAGADA));
        $query->where($tableReVentaEstatus . '.' . ReVentasEstatusModel::ACTIVO, true);
        $query->whereBetween($tableVentas . '.' . VentasRealizadasModel::CREATED_AT, [$fecha_desde, $fecha_hasta]);
        $query->groupBy(
            $tabla_producto . '.' . ProductosModel::ID,
            $tabla_venta_producto . '.' . VentaProductoModel::PRODUCTO_ID,
            $ubicacion_producto . '.' . CatalogoUbicacionProductoModel::NOMBRE,
            $tableUsuario . '.' . User::NOMBRE,
            $tableUsuario . '.' . User::APELLIDO_MATERNO,
            $tableUsuario . '.' . User::APELLIDO_PATERNO,
            'cliente.' . ClientesModel::NOMBRE,
            'cliente.' . ClientesModel::APELLIDO_PATERNO,
            'cliente.' . ClientesModel::APELLIDO_MATERNO,
            'cliente.' . ClientesModel::RFC,
            'cliente.' . ClientesModel::CODIGO_POSTAL,
            $tabla_venta_producto . '.' . VentaProductoModel::CANTIDAD,
            $tableVentas . '.' . VentasRealizadasModel::CREATED_AT,
            $tableVentas . '.' . VentasRealizadasModel::FOLIO_ID,
            $tableVentas . '.' . VentasRealizadasModel::TIPO_VENTA,
            'cliente.' . ClientesModel::NUMERO_CLIENTE
        );
        return $query->get();
    }

    public function updateOnlyVenta(int $id, $request)
    {

        $dataCuenta = $this->servicioCuentaxCobrar->getByFolioId($request->get(VentasRealizadasModel::FOLIO_ID));
        if ($dataCuenta && $dataCuenta->estatus_cuenta_id != 1) {
            throw new ParametroHttpInvalidoException([
                'msg' => "Error al actualizar !! La venta se encuentra en proceso de pago."
            ]);
        }
        $datos = $this->getTotalesByVenta($id);
        $datos[VentasRealizadasModel::VENDEDOR_ID]  = $request->get(VentasRealizadasModel::VENDEDOR_ID);
        $datos[VentasRealizadasModel::CLIENTE_ID]  = $request->get(VentasRealizadasModel::CLIENTE_ID);
        $datos[VentasRealizadasModel::TIPO_CLIENTE_ID]  = $request->get(VentasRealizadasModel::TIPO_CLIENTE_ID);
        $datos[VentasRealizadasModel::FECHA_VENTA]  = $request->get(VentasRealizadasModel::FECHA_VENTA);
        $modelo = parent::massUpdateWhereId(VentasRealizadasModel::ID, $id, $datos);

        return $modelo;
    }

    public function getTotalesByVenta($venta_id)
    {
        $getTotales = $this->servicioVentaProducto->getTotalCantidadByVentaId($venta_id);
        return [
            VentasRealizadasModel::SUBTOTAL => $getTotales[VentasRealizadasModel::SUBTOTAL],
            VentasRealizadasModel::IVA => $getTotales[VentasRealizadasModel::IVA],
            VentasRealizadasModel::VENTA_TOTAL => $getTotales[VentasRealizadasModel::VENTA_TOTAL],
        ];
    }

    public function getByVentaCotizadorPorClienteId($cliente_id)
    {
        return $this->modelo->with([
            VentasRealizadasModel::REL_COTIZACION,
        ])->where(VentasRealizadasModel::CLIENTE_ID, $cliente_id)->get()->last();
    }

    public function calcularPreciosProductos(VentasRealizadasModel $venta)
    {
        $productos = VentaProductoModel::where('venta_id', $venta->id)->get();

        foreach ($productos as $producto) {
            $precio_producto = ProductosModel::where('id', $producto->producto_id)->first();
            $producto->valor_unitario = $precio_producto->valor_unitario * 1.6; // Precio al público
            $producto->venta_total = $producto->cantidad * $producto->valor_unitario;
            $producto->save();
        }

        return $productos;
    }

    public function calcularVentaTotal($venta_id)
    {
        // total refacciones que no son prepiking
        $total_refacciones = VentaProductoModel::where('venta_id', $venta_id)->where('estatus_producto_orden', '<>', 'B')->where('afecta_paquete', false)->where('afecta_mano_obra', false)->sum(DB::raw('cantidad * valor_unitario'));
        // total refacciones que afectan mano de obra
        $total_refacciones_afectan_mo = VentaProductoModel::where('venta_id', $venta_id)->where('estatus_producto_orden', '<>', 'B')->where('afecta_mano_obra', true)->sum(DB::raw('cantidad * valor_unitario'));
        // total refacciones que son prepiking
        $total_refacciones_prepiking = VentaProductoModel::where('venta_id', $venta_id)->where('estatus_producto_orden', '<>', 'B')->where('prepiking', true)->sum(DB::raw('cantidad * valor_unitario'));
        // total refacciones con mano de obra
        $total_mano_obra_refacciones = VentaProductoModel::where('venta_id', $venta_id)->where('estatus_producto_orden', '<>', 'B')->where('presupuesto', true)->sum('costo_mano_obra');
        // total servicios
        $total_servicios = $this->servicioVentasServicios->totalServicio($venta_id);
        // total refacciones servicios
        $total_servicios_refacciones = VentaServicioModel::where('venta_id', $venta_id)->whereNull('deleted_at')->sum('refacciones');

        $total = ($total_refacciones + $total_servicios_refacciones) * 1.16 + ($total_mano_obra_refacciones + $total_servicios);

        // Actualizamos tabla ventas con el nuevo total
        $venta = VentasRealizadasModel::where(VentasRealizadasModel::ID, $venta_id)->first();

        $venta->update([
            VentasRealizadasModel::VENTA_TOTAL => $total
        ]);

        info("TOTAL ORDEN {$venta->numero_orden}", [
            'total_refacciones' => $total_refacciones, 
            'total_mano_obra_refacciones' => $total_mano_obra_refacciones, 
            'total_servicios' => $total_servicios, 
            'total_servicios_refacciones' => $total_servicios_refacciones,
            'total_refacciones_afectan_mo' => $total_refacciones_afectan_mo,
            'total_refacciones_prepiking' => $total_refacciones_prepiking,
            'total_iva_refacciones' => ($total_refacciones + $total_servicios_refacciones) * 1.16,
            'total' => $total,
        ]);

        return $total;
    }

    public function vendedorByFolioId($folio_id)
    {
        return VentasRealizadasModel::select('ventas.folio_id as folio',
            'usuarios.clave_vendedor',
            DB::raw("CONCAT(usuarios.nombre,' ',usuarios.apellido_paterno,' ',usuarios.apellido_materno) as vendedor")
        )
        ->join('usuarios', 'usuarios.id', '=', 'ventas.vendedor_id')
        ->where(VentasRealizadasModel::FOLIO_ID, $folio_id)
        ->first();
    }
    
}
