<?php

namespace App\Servicios\Refacciones;

use Illuminate\Http\Request;
use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\StockFileJobs;
use App\Jobs\StockUploadJob;
use App\Jobs\StockTxtUploadJob;
use App\Jobs\UploadClientesCSV;
use Illuminate\Support\Facades\Storage;

class ServicioStockUpload
{
    protected $stock;
    protected $isUploaded = false;

    public function __construct(StockFileJobs $stock)
    {
        $this->stock = $stock;
    }

    public function getAll()
    {
        return $this->stock
            ->leftJoin('usuarios', 'stock_file_jobs.usuario_id', 'usuarios.id')
            ->select([
                'usuarios.nombre as nombre_usuario',
                'usuarios.apellido_paterno',
                'usuarios.apellido_materno',
                'stock_file_jobs.*'
            ])
            ->latest()
            ->limit(100)
            ->get();
    }

    public function upload($request)
    {
        $filename = $this->chunks($request);
        
        if ($this->isUploaded) 
        {  
            $this->stock->nombre = $request->file('archivo')->getClientOriginalName();
            $this->stock->archivo = "app/stock/$filename";
            $this->stock->usuario_id = auth()->user()->id?? null;
            $this->stock->save();
            StockUploadJob::dispatch($this->stock);

            return ['message' => __('Archivo se subio correctamente.'), 'filename' => $filename];
        }
        
        return ['message' => __('Archivo se subió parcialmente.'), 'uuid' => $filename ];
    }

    public function txt($request)
    {
        $filename = $this->chunks($request);

        if($this->isUploaded)
        {
            $this->stock->nombre = $request->file('archivo')->getClientOriginalName();
            $this->stock->archivo = "app/stock/$filename";
            $this->stock->usuario_id = auth()->user()->id?? null;
            $this->stock->save();

            StockTxtUploadJob::dispatch($this->stock);

            return ['message' => __('Archivo se subio correctamente.'), 'filename' => $filename];
        }

        return ['message' => __('Archivo se subió parcialmente.'), 'uuid' => $filename ];
    }

    public function clientes($request)
    {
        $filename = $this->chunks($request);

        if($this->isUploaded)
        {
            $this->stock->nombre = $request->file('archivo')->getClientOriginalName();
            $this->stock->archivo = "app/stock/$filename";
            $this->stock->usuario_id = auth()->user()->id?? null;
            $this->stock->save();

            UploadClientesCSV::dispatch($this->stock);

            return ['message' => __('Archivo clientes se subio correctamente.'), 'filename' => $filename];
        }

        return ['message' => __('Archivo clientes se subió parcialmente.'), 'uuid' => $filename ];
    }

    public function chunks(Request $request)
    {
        Storage::makeDirectory('chunks');

        $file = $request->file('archivo');
        
        $filename = $request->filename ? $request->filename : $file->hashName();
        $path = storage_path("app/chunks/$filename");
      
        $handle = fopen($path, 'ab');
        fwrite($handle, $file->get());
        fclose($handle);
        
        if($request->boolean('last'))
        {
            Storage::move('chunks/' . $filename, "stock/$filename");
            $this->isUploaded = true;
        }

        return $filename;
    }
}