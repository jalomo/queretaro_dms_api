<?php

namespace App\Servicios\Refacciones;

use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\ListaProductosOrdenCompraModel;
use App\Models\Refacciones\OrdenCompraModel;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\ProveedorRefacciones;
use App\Models\Refacciones\ReComprasEstatusModel;
use Illuminate\Support\Facades\DB;

class ServicioListaProductosOrdenCompra extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'productos orden compra';
        $this->servicioProducto = new ServicioProductos();
        $this->modelo = new ListaProductosOrdenCompraModel();
        $this->modeloProductos = new ProductosModel();
    }

    public function getReglasGuardar()
    {
        return [
            ListaProductosOrdenCompraModel::PRODUCTO_ID => 'required',
            ListaProductosOrdenCompraModel::TOTAL => 'nullable',
            ListaProductosOrdenCompraModel::ORDEN_COMPRA_ID => 'required',
            ListaProductosOrdenCompraModel::CANTIDAD => 'required|numeric',
            ListaProductosOrdenCompraModel::PRECIO => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            ListaProductosOrdenCompraModel::PRODUCTO_ID => 'required',
            ListaProductosOrdenCompraModel::ORDEN_COMPRA_ID => 'required',
            ListaProductosOrdenCompraModel::CANTIDAD => 'numeric',
            ListaProductosOrdenCompraModel::PRECIO => '',
            ListaProductosOrdenCompraModel::TOTAL => 'nullable'
        ];
    }

    public function store($request)
    {
        $producto = $this->existeProducto($request->orden_compra_id, $request->producto_id);

        if (count($producto) > 0) {
            $total_productos = $producto[0][ListaProductosOrdenCompraModel::CANTIDAD] + $request->cantidad;
            $total = $total_productos * $request->precio;
            $data[ListaProductosOrdenCompraModel::PRODUCTO_ID] = $request->producto_id;
            $data[ListaProductosOrdenCompraModel::ORDEN_COMPRA_ID] = $request->orden_compra_id;
            $data[ListaProductosOrdenCompraModel::PRECIO] = $request->precio;
            $data[ListaProductosOrdenCompraModel::CANTIDAD] = $total_productos;
            $data[ListaProductosOrdenCompraModel::TOTAL] = $total;
            return DB::table($this->modelo->getTableName())
                ->where(ListaProductosOrdenCompraModel::ID, $producto[0][ListaProductosOrdenCompraModel::ID])
                ->update($data);
        } else {
            $data[ListaProductosOrdenCompraModel::PRODUCTO_ID] = $request->producto_id;
            $data[ListaProductosOrdenCompraModel::ORDEN_COMPRA_ID] = $request->orden_compra_id;
            $data[ListaProductosOrdenCompraModel::PRECIO] = $request->precio;
            $data[ListaProductosOrdenCompraModel::CANTIDAD] = $request->cantidad;
            $data[ListaProductosOrdenCompraModel::TOTAL] = $request->cantidad * $request->precio;
            return $this->modelo->create($data);
        }
    }

    public function existeProducto($orden_compra_id, $pructo_id)
    {
        return $this->modelo
            ->where(ListaProductosOrdenCompraModel::ORDEN_COMPRA_ID, $orden_compra_id)
            ->where(ListaProductosOrdenCompraModel::PRODUCTO_ID, $pructo_id)
            ->get()->toArray();
    }

    public function relacionByOrdenCompra($orden_compra_id)
    {
        $data = $this->modelo
            ->where(ListaProductosOrdenCompraModel::ORDEN_COMPRA_ID, $orden_compra_id)
            ->get();
        $productos = [];
        foreach ($data as $key => $items) {
            $productos[$key]['productos_orden_compra'] = $items;
            $productos[$key]['productos'] = $this->servicioProducto->getproductodataByid($items->producto_id)->toArray();
        }

        return $productos;
    }

    public function productosByOrdenCompra($orden_compra_id)
    {
        $tabla_productos = ProductosModel::getTableName();
        $tabla_proveedor = ProveedorRefacciones::getTableName();
        $tabla_orden_compra = OrdenCompraModel::getTableName();
        $tabla_productos_orden_compra = ListaProductosOrdenCompraModel::getTableName();
        $re_compra_estatus = ReComprasEstatusModel::getTableName();
        $data = $this->modelo
            ->select(
                $tabla_productos . '.' . ProductosModel::NO_IDENTIFICACION_FACTURA,
                $tabla_productos . '.' . ProductosModel::GPO,
                $tabla_productos . '.' . ProductosModel::PREFIJO,
                $tabla_productos . '.' . ProductosModel::BASICO,
                $tabla_productos . '.' . ProductosModel::SUFIJO,
                $tabla_productos . '.' . ProductosModel::DESCRIPCION,
                $tabla_productos . '.' . ProductosModel::PRECIO_FACTURA,
                $tabla_productos . '.' . ProductosModel::VALOR_UNITARIO,
                $tabla_proveedor . '.' . ProveedorRefacciones::PROVEEDOR_NOMBRE,
                $tabla_proveedor . '.' . ProveedorRefacciones::PROVEEDOR_RFC,
                $tabla_proveedor . '.' . ProveedorRefacciones::ID . ' as proveedor_id',
                $tabla_orden_compra . '.' . OrdenCompraModel::FACTURA,
                $tabla_productos_orden_compra . '.' . ListaProductosOrdenCompraModel::CANTIDAD,
                $tabla_productos_orden_compra . '.' . ListaProductosOrdenCompraModel::PRECIO,
                $tabla_productos_orden_compra . '.' . ListaProductosOrdenCompraModel::TOTAL,
                $tabla_productos_orden_compra . '.' . ListaProductosOrdenCompraModel::CREATED_AT,
            )
            ->join(
                $tabla_productos,
                $tabla_productos . '.' . ProductosModel::ID,
                $tabla_productos_orden_compra . '.' . ListaProductosOrdenCompraModel::PRODUCTO_ID
            )
            ->join(
                $tabla_orden_compra,
                $tabla_orden_compra . '.' . OrdenCompraModel::ID,
                $tabla_productos_orden_compra . '.' . ListaProductosOrdenCompraModel::ORDEN_COMPRA_ID
            )
            ->join(
                $tabla_proveedor,
                $tabla_proveedor . '.' . ProveedorRefacciones::ID,
                $tabla_orden_compra . '.' . OrdenCompraModel::PROVEEDOR_ID
            )
            ->where(
                $tabla_productos_orden_compra . '.' . ListaProductosOrdenCompraModel::ORDEN_COMPRA_ID,
                $orden_compra_id
            )->get();//->toArray();
        return $data;
    }

    public function totalByOrdenCompra($id)
    {
        $data = $this->modelo
            ->where(ListaProductosOrdenCompraModel::ORDEN_COMPRA_ID, $id)
            ->get();

        $total = 0;
        foreach ($data as $key => $item) {
            $total = $total + $item->total;
        }

        return ["total" => $total];
    }
}
