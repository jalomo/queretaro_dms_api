<?php

namespace App\Servicios\Refacciones;

use App\Models\Refacciones\HistorialPreciosRefaccionesModel;
use App\Models\Refacciones\ProductosModel;
use App\Servicios\Core\ServicioDB;

class ServicioHistorialPreciosRefacciones extends ServicioDB
{

    public function __construct()
    {
        $this->recurso = 'precio refacciones';
        $this->modelo = new HistorialPreciosRefaccionesModel();
        $this->modeloProductos = new ProductosModel();
    }

    public function getReglasGuardar()
    {
        return [
            HistorialPreciosRefaccionesModel::PRODUCTO_ID => 'required|exists:' . ProductosModel::getTableName() . ',id',
            HistorialPreciosRefaccionesModel::PRECIO => 'required|numeric',
            HistorialPreciosRefaccionesModel::PRECIO_PUBLICO => 'required|numeric'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            HistorialPreciosRefaccionesModel::PRODUCTO_ID => 'required|exists:' . ProductosModel::getTableName() . ',id',
            HistorialPreciosRefaccionesModel::PRECIO => 'required|numeric',
            HistorialPreciosRefaccionesModel::PRECIO_PUBLICO => 'required|numeric'
        ];
    }
}
