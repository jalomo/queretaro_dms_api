<?php

namespace App\Servicios\Refacciones;

use App\Models\Refacciones\DetalleCotizadorModel;
use App\Servicios\Core\ServicioDB;

class ServicioDetalleCotizador extends ServicioDB
{

    public function __construct()
    {
        $this->recurso = 'detalle cotizador refacciones';
        $this->modelo = new DetalleCotizadorModel();
    }

    public function getReglasGuardar()
    {

        return [
            DetalleCotizadorModel::COTIZADOR_ID => 'required|numeric',
            DetalleCotizadorModel::CANTIDAD => 'numeric|required',
            DetalleCotizadorModel::DESCRIPCION => 'required',
            DetalleCotizadorModel::NUMERO_PARTE => 'required',
            DetalleCotizadorModel::PRECIO_NETO => 'required|numeric',
        ];
    }
    public function getReglasUpdate()
    {
        return [
            DetalleCotizadorModel::COTIZADOR_ID => 'required|numeric',
            DetalleCotizadorModel::CANTIDAD => 'numeric|required',
            DetalleCotizadorModel::DESCRIPCION => 'required',
            DetalleCotizadorModel::NUMERO_PARTE => 'required',
            DetalleCotizadorModel::PRECIO_NETO => 'required|numeric',
        ];
    }

    public function getDetalleCotizadorByCotizadoId(int $cotizador_id)
    {
        $total = 0;
        $items = $this->modelo->where(DetalleCotizadorModel::COTIZADOR_ID, $cotizador_id)->get();
        if ($items) {
            foreach ($items as $item) {
                $total = ($item->precio_neto * $item->cantidad) + $total;
            }
        }
        return $total;
    }
}
