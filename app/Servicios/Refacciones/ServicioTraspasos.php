<?php

namespace App\Servicios\Refacciones;

use App\Servicios\Core\ServicioDB;
use App\Models\Contabilidad\CatalogoProcesosModel;
use App\Models\Refacciones\FoliosModel;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\Traspasos;
use Illuminate\Support\Facades\DB;

class ServicioTraspasos extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'traspasos';
        $this->modelo = new Traspasos();
        $this->servicioFolio = new ServicioFolios();
    }

    public function getReglasGuardar()
    {
        return [
            Traspasos::ALMACEN_ORIGEN_ID => 'required|numeric',
            Traspasos::ALMACEN_DESTINO_ID => 'required|numeric'
        ];
    }

    public function getReglasUpdate()
    {
        return [
            Traspasos::ALMACEN_ORIGEN_ID => 'numeric',
            Traspasos::ALMACEN_DESTINO_ID => 'numeric'
        ];
    }

    public function getReglasBusqueda()
    {
        return [
            Traspasos::FECHA_INICIO => 'required|date',
            Traspasos::FECHA_FIN => 'required|date'
        ];
    }

    public function store($parametros)
    {
        $folio = $this->servicioFolio->generarFolio(CatalogoProcesosModel::PROCESO_TRASPASO);
        $parametros[Traspasos::FOLIO_ID] = $folio->id;
        return $this->modelo->create($parametros);
    }

    public function getAll()
    {
        return $this->modelo->with([
            Traspasos::REL_FOLIO,
            Traspasos::REL_ALMACEN_ORIGEN,
            Traspasos::REL_ALMACEN_DESTINO
        ])->get();
    }

    public function showById($id)
    {
        $data = $this->modelo->with([
            Traspasos::REL_FOLIO,
            Traspasos::REL_ALMACEN_ORIGEN,
            Traspasos::REL_ALMACEN_DESTINO
        ])->where(Traspasos::ID, $id)->get();

        foreach ($data as $key => $item) {

            $detalles = $this->searchDetails(
                [
                    Traspasos::ID => $item->id,
                    ProductosModel::NO_IDENTIFICACION_FACTURA =>  null
                ]
            );
            $item->total_traspaso = $this->calcularTotal($detalles);
            $item->detalle_productos_almacen = $detalles;
        }

        return $data;
    }

    public function searchDetails($parametros)
    {
        $modelo = DB::table('traspasos as ts')
            ->join('detalle_traspaso_producto_almacen as dt', 'ts.id', '=', 'dt.traspaso_id')
            ->join('almacen as alm_orig', 'alm_orig.id', '=', 'dt.almacen_origen_id')
            ->join('almacen as alm_dest', 'alm_dest.id', '=', 'dt.almacen_destino_id')
            ->join('producto as prod', 'prod.id', '=', 'dt.producto_id')
            ->select(
                'ts.id as id_trasp',
                'ts.estatus',
                'dt.id as id_detalle_t',
                'dt.total',
                'dt.cantidad',
                'dt.valor_unitario',
                'dt.descontado_a_producto',
                'alm_orig.nombre as almacen_origen',
                'alm_dest.nombre as almacen_dest',
                'alm_orig.codigo_almacen as codigo_almacen_orig',
                'alm_dest.codigo_almacen as codigo_almacen_dest',
                'prod.no_identificacion',
                'prod.descripcion'
            );

        $modelo->where('ts.' . Traspasos::ID, $parametros[Traspasos::ID]);

        if (isset($parametros[ProductosModel::NO_IDENTIFICACION_FACTURA])) {
            $modelo->where('prod.' . ProductosModel::NO_IDENTIFICACION_FACTURA, $parametros[ProductosModel::NO_IDENTIFICACION_FACTURA]);
        }

        return $modelo->get();
    }

    public function detallePolizaTraspasoAlmacenes($parametros)
    {
        $modelo = DB::table('traspasos as ts')
            ->join('detalle_traspaso_producto_almacen as dts', 'ts.id', '=', 'dts.traspaso_id')
            ->join('almacen as al_or', 'al_or.id', '=', 'dts.almacen_origen_id')
            ->join('almacen as al_dest', 'al_dest.id', '=', 'dts.almacen_destino_id');
        $modelo->whereDate('ts.' . Traspasos::CREATED_AT, '>=', $parametros[Traspasos::FECHA_INICIO]);
        $modelo->whereDate('ts.' . Traspasos::CREATED_AT, '<=', $parametros[Traspasos::FECHA_FIN]);
        $modelo->select(
            'ts.id AS id_traspaso',
            'al_or.nombre AS almacen_origen',
            'al_dest.nombre AS almacen_dest',
            'dts.total',
            'dts.cantidad',
            'ts.created_at'
        );

        return $modelo->get();
    }

    public function getTraspasosByFechas($parametros)
    {
        return $this->modelo->with([
            Traspasos::REL_FOLIO,
        ])->whereDate(Traspasos::CREATED_AT, '>=', $parametros[Traspasos::FECHA_INICIO])
            ->whereDate(Traspasos::CREATED_AT, '<=', $parametros[Traspasos::FECHA_FIN])
            ->get();
    }

    public function addProductosDetailsToArray($dataModel, $parametros)
    {
        $data  = $dataModel;
        $total_generado = 0;
        foreach ($data as $key => $item) {

            $detalles = $this->searchDetails(
                [
                    Traspasos::ID => $item->id,
                    ProductosModel::NO_IDENTIFICACION_FACTURA => isset($parametros[ProductosModel::NO_IDENTIFICACION_FACTURA]) ? $parametros[ProductosModel::NO_IDENTIFICACION_FACTURA] : null
                ]
            );
            $item->total_traspaso = $this->calcularTotal($detalles);
            $total_generado = $total_generado + $item->total_traspaso;
            $item->detalle_productos_almacen = $detalles;
        }
        $poliza = $this->detallePolizaTraspasoAlmacenes($parametros);
        return [
            'total_generado' => $total_generado,
            'poliza' => $poliza,
            'data' => $data
        ];
    }

    public function calcularTotal($arrayModel)
    {

        $total_traspaso = 0;
        foreach ($arrayModel as $key => $item) {
            $total_traspaso = $total_traspaso + $item->total;
        }

        return $total_traspaso;
    }
}
