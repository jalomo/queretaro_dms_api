<?php

namespace App\Servicios\Refacciones;

use App\Models\Refacciones\CotizadorRefaccionesModel;
use App\Models\Refacciones\VentasRealizadasModel;
use App\Models\Refacciones\ClientesModel;
use App\Servicios\Core\ServicioDB;
use Illuminate\Http\Request;
use DB;

class ServicioCotizadorRefacciones extends ServicioDB
{

    public function __construct()
    {
        $this->recurso = 'cotizador refacciones';
        $this->modelo = new CotizadorRefaccionesModel();
    }

    public function getReglasGuardar()
    {
        return [
            CotizadorRefaccionesModel::VENTA_ID => 'nullable|numeric',
            CotizadorRefaccionesModel::MARCA_ID => 'required|numeric|exists:catalogo_marcas,id',
            CotizadorRefaccionesModel::MODELO_ID => 'required|numeric|exists:catalogo_modelos,id',
            CotizadorRefaccionesModel::ANIO => 'required|numeric',
            CotizadorRefaccionesModel::NO_SERIE => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CotizadorRefaccionesModel::VENTA_ID => 'nullable|numeric',
            CotizadorRefaccionesModel::MARCA_ID => 'required|numeric|exists:catalogo_marcas,id',
            CotizadorRefaccionesModel::MODELO_ID => 'required|numeric|exists:catalogo_modelos,id',
            CotizadorRefaccionesModel::ANIO => 'nullable',
            CotizadorRefaccionesModel::NO_SERIE => 'nullable'
        ];
    }
    public function getReglasPorVenta()
    {
        return [
            CotizadorRefaccionesModel::VENTA_ID => 'required|numeric',
        ];
    }

    public function createRequestCotizador(Request $request)
    {
        return new Request([
            CotizadorRefaccionesModel::VENTA_ID => $request->get(CotizadorRefaccionesModel::VENTA_ID),
            CotizadorRefaccionesModel::MARCA_ID => $request->get(CotizadorRefaccionesModel::MARCA_ID),
            CotizadorRefaccionesModel::MODELO_ID => $request->get(CotizadorRefaccionesModel::MODELO_ID),
            CotizadorRefaccionesModel::ANIO => $request->get(CotizadorRefaccionesModel::ANIO),
            CotizadorRefaccionesModel::NO_SERIE => $request->get(CotizadorRefaccionesModel::NO_SERIE),
        ]);
    }

    public function getByIdVenta(int $venta_id)
    {
        return $this->modelo->where(CotizadorRefaccionesModel::VENTA_ID, $venta_id)->with(
            [
                CotizadorRefaccionesModel::REL_MARCA,
                CotizadorRefaccionesModel::REL_MODELO,
                CotizadorRefaccionesModel::REL_VENTA . '.' . VentasRealizadasModel::REL_FOLIO,
                CotizadorRefaccionesModel::REL_VENTA . '.' . VentasRealizadasModel::REL_CLIENTE => function ($query2) {
                    $query2->select(ClientesModel::ID, ClientesModel::NUMERO_CLIENTE, DB::raw("CONCAT(nombre,' ',apellido_paterno,' ',apellido_materno) as nombre_cliente"));
                }
            ]
        )
            ->first();
    }

    public function getCotizaciones()
    {
        return $this->modelo->with([
            CotizadorRefaccionesModel::REL_MARCA,
            CotizadorRefaccionesModel::REL_MODELO,
            CotizadorRefaccionesModel::REL_VENTA . '.' . VentasRealizadasModel::REL_FOLIO,
            CotizadorRefaccionesModel::REL_VENTA . '.' . VentasRealizadasModel::REL_CLIENTE => function ($query2) {
                $query2->select(ClientesModel::ID, ClientesModel::NUMERO_CLIENTE, DB::raw("CONCAT(nombre,' ',apellido_paterno,' ',apellido_materno) as nombre_cliente"));
            }
        ])->get();
    }
}
