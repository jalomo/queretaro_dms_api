<?php

namespace App\Servicios\Refacciones;

use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\CaRegimenFiscalModel;

class ServicioRegimenFiscal extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'regimen fiscal';
        $this->modelo = new CaRegimenFiscalModel();
    }

    public function getReglasGuardar()
    {
        return [
            CaRegimenFiscalModel::NOMBRE => 'required|string|regex:/^[\pL\s]+$/u|max:50|unique:' . $this->modelo->getTable() . ',nombre',
            CaRegimenFiscalModel::CLAVE => 'required|string',
            CaRegimenFiscalModel::PERSONA_FISICA => 'required|boolean',
            CaRegimenFiscalModel::PERSONA_MORAL => 'required|boolean'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CaRegimenFiscalModel::NOMBRE => 'required|string|regex:/^[\pL\s]+$/u|max:50|unique:' . $this->modelo->getTable() . ',nombre',
            CaRegimenFiscalModel::CLAVE => 'required|string',
            CaRegimenFiscalModel::PERSONA_FISICA => 'required|boolean',
            CaRegimenFiscalModel::PERSONA_MORAL => 'required|boolean'
        ];
    }

    public function getBusqueda($request)
    {
        return $this->modelo
            ->when($request->get(CaRegimenFiscalModel::PERSONA_FISICA), function ($q) use ($request) {
                $q->where(
                    CaRegimenFiscalModel::getTableName() . '.' . CaRegimenFiscalModel::PERSONA_FISICA,
                    $request->get(CaRegimenFiscalModel::PERSONA_FISICA)
                );
            })
            ->when($request->get(CaRegimenFiscalModel::PERSONA_MORAL), function ($q) use ($request) {
                $q->where(
                    CaRegimenFiscalModel::getTableName() . '.' . CaRegimenFiscalModel::PERSONA_MORAL,
                    $request->get(CaRegimenFiscalModel::PERSONA_MORAL)
                );
            })
            ->get();
    }
}
