<?php

namespace App\Servicios\Refacciones;

use App\Servicios\Core\ServicioDB;
use App\Servicios\Refacciones\ServicioClientes;
use App\Servicios\Refacciones\ServicioFolios;
use App\Servicios\Refacciones\ServicioPrecios;
use App\Servicios\Refacciones\ServicioProductoAlmacen;
use App\Models\Refacciones\ClientesModel;
use App\Models\Refacciones\Precios;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\VentaProductoModel;
use App\Models\Refacciones\EstatusVentaModel;
use App\Models\Refacciones\VentasRealizadasModel;
use App\Models\Refacciones\ReVentasEstatusModel;
use App\Models\Refacciones\DevolucionVentasModel;
use App\Models\CuentasPorCobrar\CuentasPorCobrarModel;
use App\Models\Refacciones\FoliosModel;
use App\Models\Usuarios\User as UsuarioModel;
use App\Servicios\Refacciones\ServicioVentasServicios;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\Ereact\VentasResource;
use App\Servicios\Producto\CrearProductoService;
use App\Models\Refacciones\HistorialPresupuestos;

class ServicioVentaProducto extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'ventas';
        $this->modelo = new VentaProductoModel();
        $this->servicioFolio = new ServicioFolios();
        $this->servicioPrecios = new ServicioPrecios();
        $this->servicioClientes = new ServicioClientes();
        $this->servicioProductos = new ServicioProductos();
        $this->servicioProductoAlmacen = new ServicioProductoAlmacen();
        $this->servicioVentasServicios = new ServicioVentasServicios;
    }

    public function getReglasGuardar()
    {
        return [
            VentaProductoModel::PRODUCTO_ID => 'required|exists:producto,id',
            VentaProductoModel::PRECIO_ID => 'required|exists:precio,id',
            VentaProductoModel::CLIENTE_ID => 'nullable|exists:clientes,id',
            VentaProductoModel::FOLIO_ID => 'required',
            VentaProductoModel::VALOR_UNITARIO => 'required',
            VentaProductoModel::CANTIDAD => 'required|numeric',
            VentaProductoModel::ORDEN_ORIGINAL => 'nullable|boolean',
            VentaProductoModel::ESTATUS_PRODUCTO_ORDEN => 'nullable',
            VentaProductoModel::PRECIO_MANUAL => 'nullable|integer|min:0|max:100',
            VentaProductoModel::VENTA_ID => 'required|numeric',
            VentaProductoModel::ALMACEN_ID => 'integer',
            VentaProductoModel::TIPO_DESCUENTO_ID => 'nullable|numeric',
            VentaProductoModel::COSTO_ADICIONAL => 'nullable|numeric',
            'core' => 'nullable|numeric',
            'afecta_paquete' => 'nullable|boolean',
            'afecta_mano_obra' => 'nullable|boolean',
            'core' => 'required|numeric',
        ];
    }
    public function getReglasUpdate()
    {
        return [
            VentaProductoModel::PRODUCTO_ID => 'exists:producto,id',
            VentaProductoModel::PRECIO_ID => 'exists:precio,id',
            VentaProductoModel::CLIENTE_ID => 'exists:clientes,id',
            VentaProductoModel::FOLIO_ID => 'required|exists:folios,id',
            VentaProductoModel::VALOR_UNITARIO => 'nullable',
            VentaProductoModel::CANTIDAD => 'numeric',
            VentaProductoModel::ORDEN_ORIGINAL => 'nullable|boolean',
            VentaProductoModel::ESTATUS_PRODUCTO_ORDEN => 'nullable',
            VentaProductoModel::PRECIO_MANUAL => 'nullable',
            VentaProductoModel::VENTA_ID => 'required|numeric',
            VentaProductoModel::TIPO_DESCUENTO_ID => 'nullable|numeric',
            VentaProductoModel::COSTO_ADICIONAL => 'nullable|numeric',
            'core' => 'nullable|numeric',
            'afecta_paquete' => 'nullable|boolean',
            'afecta_mano_obra' => 'nullable|boolean',
            'core' => 'required|numeric',
        ];
    }

    public function getDetalleVenta()
    {
        return $this->modelo->with([
            ProductosModel::getTableName(),
            Precios::getTableName(),
            ClientesModel::getTableName()
        ])->get();
    }

    public function geDetalleVentaById($id)
    {
        return DB::table('venta_producto')
            ->join('producto', 'producto.id', '=', 'venta_producto.producto_id')
            ->leftJoin('catalogo_ubicaciones_producto', 'catalogo_ubicaciones_producto.id', '=', 'producto.ubicacion_producto_id')
            ->join('precio', 'precio.id', '=', 'venta_producto.precio_id')
            ->join('ventas', 'ventas.id', '=', 'venta_producto.venta_id')
            ->join('clientes', 'clientes.id', '=', 'ventas.cliente_id')
            ->join('re_ventas_estatus', 're_ventas_estatus.ventas_id', '=', 'ventas.id')
            ->join('estatus_venta', 'estatus_venta.id', '=', 're_ventas_estatus.estatus_ventas_id')
            ->join('stock_productos', 'stock_productos.producto_id', '=', 'producto.id')
            ->leftJoin('reemplazos', 'producto.reemplazo_id', 'reemplazos.id')
            ->leftJoin('producto as producto_reemplazado', 'reemplazos.reemplazo_id', 'producto_reemplazado.id')
            ->where('re_ventas_estatus.activo', true)
            ->where('venta_producto.venta_id', $id)
            ->where('venta_producto.estatus_producto_orden', '!=', 'B')
            ->select(
                'venta_producto.*',
                'producto.no_identificacion',
                'producto.costo_promedio',
                'producto.descripcion as descripcion_producto',
                'producto.unidad as unidad',
                'precio.precio_publico',
                'estatus_venta.nombre as estatus_venta',
                'catalogo_ubicaciones_producto.nombre as ubicacion_producto',
                're_ventas_estatus.estatus_ventas_id',
                'stock_productos.cantidad_actual as stock',
                'producto_reemplazado.no_identificacion as reemplazo_no_identificacion',
                'producto_reemplazado.id as reemplazo_producto_id',
            )
            ->orderBy('venta_producto.presupuesto')
            ->orderBy('venta_producto.afecta_mano_obra')
            ->orderBy('venta_producto.afecta_paquete')
            ->orderBy('producto.descripcion')
            ->get();
    }

    public function getDetalleVentaByFolio($folio_id)
    {

        return DB::table('venta_producto')
            ->join('producto', 'producto.id', '=', 'venta_producto.producto_id')
            ->join('precio', 'precio.id', '=', 'venta_producto.precio_id')
            ->join('clientes', 'clientes.id', '=', 'venta_producto.cliente_id')
            ->join('ventas', 'ventas.id', '=', 'venta_producto.venta_id')
            ->join('re_ventas_estatus', 're_ventas_estatus.ventas_id', '=', 'ventas.id')
            ->join('estatus_venta', 'estatus_venta.id', '=', 're_ventas_estatus.estatus_ventas_id')
            ->where('re_ventas_estatus.activo', true)
            ->where('venta_producto.folio_id', $folio_id)
            ->select(
                'venta_producto.*',
                'producto.no_identificacion',
                'producto.descripcion as descripcion_producto',
                'producto.unidad as unidad',
                'precio.precio_publico',
                'estatus_venta.nombre as estatus_venta',
                're_ventas_estatus.estatus_ventas_id'
            )
            ->get();
    }

    public function totalVentaByFolio($folio_id)
    {
        return $this->totalventa([
            'folio_id' => $folio_id
        ]);
    }

    public function totalVentaById($venta_id)
    {
        return $this->totalventa([
            'venta_id' => $venta_id
        ]);
    }

    public function totalventa($parametros)
    {
        $query = DB::table('venta_producto')
            ->join('producto', 'producto.id', '=', 'venta_producto.producto_id')
            ->join('precio', 'precio.id', '=', 'venta_producto.precio_id')
            ->join('clientes', 'clientes.id', '=', 'venta_producto.cliente_id')
            ->join('ventas', 'ventas.id', '=', 'venta_producto.venta_id')
            ->join('re_ventas_estatus', 're_ventas_estatus.ventas_id', '=', 'ventas.id')
            ->join('estatus_venta', 'estatus_venta.id', '=', 're_ventas_estatus.estatus_ventas_id')
            ->where('re_ventas_estatus.activo', true);

        if (isset($parametros['folio_id'])) {
            $query->where('ventas.folio_id', $parametros['folio_id']);
        }
        if (isset($parametros['venta_id'])) {
            $query->where('venta_producto.venta_id', $parametros['venta_id']);
        }

        $query->select(
            DB::raw('SUM(venta_producto.venta_total ) as venta_total')
        );

        $resultado = $query->first();
        // $subtotal = $resultado->venta_total;
        // $iva = $subtotal * 0.16;
        // $total = $subtotal + $iva;
        return isset($resultado->venta_total) ? $resultado->venta_total : 0;
    }

    public function porcentajeProducto($descuento, $precio_producto, $cantidad)
    {
        $des = (int) $descuento / 100;
        $precio_total = (100 * $des) + $precio_producto;
        return $precio_total * $cantidad;
    }

    public function store($request)
    {
        $producto = $this->existeProducto([
            VentaProductoModel::FOLIO_ID => $request->folio_id,
            VentaProductoModel::PRODUCTO_ID => $request->producto_id,
            VentaProductoModel::PRECIO_ID => $request->precio_id,
            VentaProductoModel::ALMACEN_ID => $request->almacen_id ?? 1,
        ]);
        if (count($producto) > 0) {
            $parametros[VentaProductoModel::PRODUCTO_ID] = $request->producto_id;
            $parametros[VentaProductoModel::PRECIO_ID] = $request->precio_id;
            $parametros[VentaProductoModel::FOLIO_ID] = $request->folio_id;
            $parametros[VentaProductoModel::VALOR_UNITARIO] = $request->valor_unitario - $request->core;
            $parametros[VentaProductoModel::TOTAL_VENTA] = ($request->valor_unitario * $request->cantidad) - $request->core;
            $parametros[VentaProductoModel::ESTATUS_PRODUCTO_ORDEN] = VentaProductoModel::ESTATUS_PRODUCTO_ORDEN_O;

            $this->modelo
                ->where(VentaProductoModel::ID, $producto[0][VentaProductoModel::ID])
                ->update($parametros);

            $venta_producto = $this->modelo->where(VentaProductoModel::ID, $producto[0][VentaProductoModel::ID])->first();
        } else {
            $parametros = $request->all();
            $itemProducto = $this->servicioProductos->getById($parametros[VentaProductoModel::PRODUCTO_ID]);
            $parametros[VentaProductoModel::ESTATUS_PRODUCTO_ORDEN] = VentaProductoModel::ESTATUS_PRODUCTO_ORDEN_O;
            $parametros[VentaProductoModel::VALOR_UNITARIO] = $request->valor_unitario - $request->core;
            $parametros[VentaProductoModel::TOTAL_VENTA] = ($request->valor_unitario * $request->cantidad) - $request->core;
            $parametros[VentaProductoModel::COSTO_PROMEDIO] = $itemProducto['costo_promedio'];
            $venta_producto = $this->modelo->create($parametros);
        }

        $venta_producto->save();

        return $venta_producto;
    }

    public function storeVentaDetalle($request)
    {
        $afecta_mano_obra = filter_var($request['afecta_mano_obra']?? false, FILTER_VALIDATE_BOOL);
        $afecta_paquete = filter_var($request['afecta_paquete']?? false, FILTER_VALIDATE_BOOL);

        $parametros = [
            VentaProductoModel::ESTATUS_PRODUCTO_ORDEN => VentaProductoModel::ESTATUS_PRODUCTO_ORDEN_O,
            VentaProductoModel::TOTAL_VENTA => $request->valor_unitario,
            VentaProductoModel::VALOR_UNITARIO => $request->valor_unitario / $request->cantidad,
            VentaProductoModel::VALOR_UNITARIO_ORIGINAL => $request->valor_unitario / $request->cantidad,
            VentaProductoModel::AFECTA_PAQUETE => $afecta_paquete,
            VentaProductoModel::AFECTA_MANO_OBRA => $afecta_mano_obra,
            VentaProductoModel::DETALLES => true,
            VentaProductoModel::CORE => $request->core,
        ] + $request->all();
       
        $venta_producto = $this->modelo->create($parametros);
        $venta_producto->costo_promedio = $venta_producto->producto->costo_promedio;

        $venta = VentasRealizadasModel::find($venta_producto->venta_id);

        // Agregamos detalle a asientos
        $venta_producto->id_index_requisicion = $this->storePiezas($venta, [
            'precio' => $parametros['venta_total'],
            'cantidad' => $parametros['cantidad'],
            'producto_id' => $parametros['producto_id'],
            'afecta_paquete' => $parametros['afecta_paquete'],
            'afecta_mano_obra' => $parametros['afecta_mano_obra'],
            'presupuesto' => $venta_producto->presupuesto,
        ]);

        //! No Borrar esto mantiene la consistencia del total entre productos y servicios
        $this->calcularVentaTotal($request->venta_id);

        return tap($venta_producto)->save();
    }

    /**
     * Actualiza los productos de la venta y
     * llamada a env('URL_SERVICIOS') . 'api/updatePaqueteFromDMS/' . $venta->numero_orden
     * 
     * @param array $data
     * @param int $id
     * @return App\Models\Refacciones\VentaProductoModel $venta
     */
    public function updateVenta($data, $id, $user = null)
    {
        $venta_producto = VentaProductoModel::findOrFail($id);
        $venta = VentaProductoModel::findOrFail($id)->venta;
        $data['es_total'] = filter_var($data['es_total'], FILTER_VALIDATE_BOOLEAN);
        $precio = $data['es_total'] ? $data['valor_unitario'] : $data['cantidad'] * $data['valor_unitario'];
        $afecta_mano_obra = isset($data['afecta_mano_obra']) ? filter_var($data['afecta_mano_obra'], FILTER_VALIDATE_BOOLEAN) : 0;
        $afecta_paquete = isset($data['afecta_paquete']) ? filter_var($data['afecta_paquete'], FILTER_VALIDATE_BOOLEAN) : 0;
        
        // Fix: si no existe se agrega que es de detalles
        // Se hace antes de que se asigne un id_index_requisicion si no existiera
        $venta_producto->detalles = is_null($venta_producto->id_index_requisicion)? true : $venta_producto->detalles;

        if ( ! is_null($venta_producto->id_index_requisicion) && $venta_producto->presupuesto) {
            $es_nuevo = filter_var($data['nuevo'], FILTER_VALIDATE_BOOLEAN);

            if ($es_nuevo == true) {
                // Buscamos Producto si no existe lo creamos     
                $productoService = new CrearProductoService;
                $producto = $productoService->make([
                    'no_identificacion' => $data['no_identificacion'],
                    'descripcion' => 'Sin descripción',
                ]);

                $data['producto_id'] = $producto->id;
            } else {
                $producto = ProductosModel::find($data['producto_id']);
            }

            $historial = new HistorialPresupuestos([
                'venta_id' => $venta_producto->venta_id,
                'venta_producto_id' => $venta_producto->id,
                'user_id' => $user->id ?? null,
                'producto_id' => $producto->id,
                'producto_id_latest' => $venta_producto->producto_id,
                'cantidad' => $data['cantidad'],
                'cantidad_anterior' => $venta_producto->cantidad,
                'precio_refaccion' => $precio,
                'precio_refaccion_anterior' => $venta_producto->valor_unitario,
                'costo_mano_obra' => $venta_producto->costo_mano_obra,
                'costo_mano_obra_anterior' => $venta_producto->costo_mano_obra,
            ]);

            $historial->save();

            $this->updatePiezasPresupuesto($venta, [
                'id' => $venta_producto->id_index_requisicion,
                'codigo' => $producto->no_identificacion,
                'descripcion' => $producto->descripcion,
                'producto_id' => $producto->id,
                'precio' => $precio,
                'original' => $venta_producto->valor_unitario,
                'cantidad' => $data['cantidad'],
                'afecta_paquete' => $afecta_paquete,
                'afecta_mano_obra' => $afecta_mano_obra,
                'presupuesto' => $venta_producto->presupuesto,
                'costo_mo' => $venta_producto->costo_mano_obra / VentaProductoModel::IVA,
            ]);
        } else {
            $this->updatePiezas($venta, [
                'id' => $venta_producto->id_index_requisicion,
                'codigo' => $data['no_identificacion'],
                'precio' => $precio,
                'cantidad' => $data['cantidad'],
                'afecta_paquete' => $afecta_paquete,
                'afecta_mano_obra' => $afecta_mano_obra,
                'presupuesto' => $venta_producto->presupuesto,
            ]);
        }

        $venta_producto->cantidad = $data['cantidad'];
        $venta_producto->producto_id = $data['producto_id'];
        $venta_producto->almacen_id = $data['almacen_id'];
        $venta_producto->precio_id = $data['precio_id'];
        $venta_producto->valor_unitario = $data['es_total'] ? $data['valor_unitario'] / $data['cantidad'] : $data['valor_unitario'];
        $venta_producto->venta_total = $precio + $venta_producto->costo_mano_obra;
        $venta_producto->afecta_paquete = $afecta_paquete;
        $venta_producto->afecta_mano_obra = $afecta_mano_obra;
        $venta_producto->core = $data['core']?? 0;

        //! No Borrar esto mantiene la consistencia del total entre productos y servicios
        $this->calcularVentaTotal($venta->id);

        return tap($venta_producto)->save();
    }

    public function existeProducto($parametros)
    {
        $query = $this->modelo
            ->where(VentaProductoModel::FOLIO_ID, $parametros[VentaProductoModel::FOLIO_ID])
            ->where(VentaProductoModel::ESTATUS_PRODUCTO_ORDEN, '<>', VentaProductoModel::ESTATUS_PRODUCTO_ORDEN_B);

        if (isset($parametros[VentaProductoModel::PRODUCTO_ID])) {
            $query->where(VentaProductoModel::PRODUCTO_ID, $parametros[VentaProductoModel::PRODUCTO_ID]);
        }

        if (isset($parametros[VentaProductoModel::PRECIO_ID])) {
            $query->where(VentaProductoModel::PRECIO_ID, $parametros[VentaProductoModel::PRECIO_ID]);
        }

        if (isset($parametros[VentaProductoModel::VENTA_ID])) {
            $query->where(VentaProductoModel::VENTA_ID, $parametros[VentaProductoModel::VENTA_ID]);
        }

        if (isset($parametros[VentaProductoModel::ALMACEN_ID])) {
            $query->where(VentaProductoModel::ALMACEN_ID, $parametros[VentaProductoModel::ALMACEN_ID]);
        }

        return $query->get()->toArray();
    }

    public function descontarVenta($modelo)
    {
        foreach ($modelo as $key => $item) {
            $total_vendida = $item->cantidad_vendida;
            $this->servicioProductos->descontarProductos([
                ProductosModel::ID => $item->producto_id,
                ProductosModel::CANTIDAD => $total_vendida
            ]);

            $this->servicioProductoAlmacen->descontarAlmacen([
                ProductosModel::ID => $item->producto_id,
                ProductosModel::CANTIDAD => $total_vendida
            ]);
        }
    }

    public function getDataByFolio($folio_id)
    {
        $modelo = DB::table('venta_producto as vp')
            ->join('producto', 'vp.producto_id', '=', 'producto.id')
            ->select(
                'vp.id',
                'vp.producto_id',
                'vp.precio_id',
                'vp.cliente_id',
                'vp.folio_id',
                'vp.descontado_almacen',
                'vp.cantidad as cantidad_vendida',
                'producto.no_identificacion',
                'producto.descripcion',
                'producto.cantidad as producto_cantidad',
                'producto.valor_unitario'
            )->where(VentaProductoModel::FOLIO_ID, $folio_id)->get();

        return $modelo;
    }

    public function getIdVentaProducto($venta_id, $request)
    {
        return $this->modelo
            ->where(
                [
                    VentaProductoModel::VENTA_ID => $venta_id,
                    VentaProductoModel::PRODUCTO_ID => $request->producto_id
                ]
            )->first();
    }


    public function getTotalCantidadByVentaId(int $venta_id)
    {
        $respuesta = $this->modelo
            ->select(
                DB::raw('SUM(' . VentaProductoModel::TOTAL_VENTA . ') as total')
            )->where(VentaProductoModel::VENTA_ID, $venta_id)->first();


        $datos['subtotal'] = $respuesta->total;
        $datos['iva'] = $datos['subtotal'] * 0.16;
        $datos['venta_total'] = $datos['subtotal'] + $datos['iva'];

        return $datos;
    }

    /**
     * Agregamos producto a asientos
     * 
     */
    public function storePiezas($venta, $data)
    {
        if (! is_null($venta->numero_orden)) 
        {
            $producto = ProductosModel::find($data['producto_id']);

            $codigo = $producto->no_identificacion;

            $response = Http::withBody(json_encode([[
                'codigo' => $codigo,
                'descripcion' => $producto->descripcion,
                'precio' => $data['precio'],
                'cantidad' => $data['cantidad'],
                'tiempo' => 0,
                'afecta_paquete' => $data['afecta_paquete'],
                'afecta_mano_obra' => $data['afecta_mano_obra'],
                'presupuesto' => $data['presupuesto'],
            ]]), 'json')
                ->post(env('URL_SERVICIOS') . "api/addPiezasFromDMS/{$venta->numero_orden}");

            $pieza = $response->object();

            if (! $response->successful() || $pieza->exito == false) {

                info('ERROR AL AGREGAR REFACCION PREVENTIVO', [
                    'numero_orden' => $venta->numero_orden,
                    'codigo' => $codigo,
                    'descripcion' => $producto->descripcion,
                    'precio' => $data['precio'],
                    'cantidad' => $data['cantidad'],
                    'afecta_paquete' => $data['afecta_paquete'],
                    'afecta_mano_obra' => $data['afecta_mano_obra'],
                    'presupuesto' => $data['presupuesto'],
                    'mensaje' => optional($pieza)->message,
                    'producto_id' => $producto->id,
                ]);

                return null;
            }

            info('AGREGAR REFACCION', [
                'codigo' => $codigo,
                'descripcion' => $producto->descripcion,
                'precio' => $data['precio'],
                'cantidad' => $data['cantidad'],
                'tiempo' => 0,
                'afecta_paquete' => $data['afecta_paquete'],
                'afecta_mano_obra' => $data['afecta_mano_obra'],
                'presupuesto' => $data['presupuesto'],
                'id_index_requisicion' => optional(collect($pieza->dataUpdated)->firstWhere('codigo', $codigo))->id,
                'producto_id' => $producto->id,
            ]);

            return collect($pieza->dataUpdated)->firstWhere('codigo', $codigo)->id;
        }

        return null;
    }

    /**
     * Actualizamos producto a asientos
     *
     * @param App\Models\Refacciones\VentasRealizadasModel $venta
     * @param array $data
     * @return int $id || null
     */
    public function updatePiezas($venta, $data)
    {
        if ($venta->numero_orden != null) {
            $codigo = $data['codigo'];

            $response = Http::withBody(json_encode([[
                'id' => $data['id'],
                'codigo' => $codigo,
                'precio' => $data['precio'],
                'cantidad' => $data['cantidad'],
                'afecta_paquete' => $data['afecta_paquete'],
                'afecta_mano_obra' => $data['afecta_mano_obra'],
                'presupuesto' => $data['presupuesto'],
            ]]), 'json')
                ->post(env('URL_SERVICIOS') . "api/updatePaqueteFromDMS/{$venta->numero_orden}");

            $pieza = $response->object();

            if (!$response->successful() || $pieza->exito == false) {

                info('ERROR AL ACTUALIZAR PREVENTIVO', [
                    'numero_orden' => $venta->numero_orden,
                    'codigo' => $codigo,
                    'precio' => $data['precio'],
                    'cantidad' => $data['cantidad'],
                    'afecta_paquete' => $data['afecta_paquete'],
                    'afecta_mano_obra' => $data['afecta_mano_obra'],
                    'presupuesto' => $data['presupuesto'],
                    'id_index_requisicion' => $data['id'],
                    'mensaje' => optional($pieza)->message,
                ]);

                return null;
            }

            info('ACTUALIZAR REFACCION', [
                'id' => $data['id'],
                'codigo' => $codigo,
                'precio' => $data['precio'],
                'cantidad' => $data['cantidad'],
                'afecta_paquete' => $data['afecta_paquete'],
                'afecta_mano_obra' => $data['afecta_mano_obra'],
                'presupuesto' => $data['presupuesto'],
            ]);

            return collect($pieza->dataUpdated)->firstWhere('codigo', $codigo)->id;
        }

        return null;
    }

    public function updatePiezasPresupuesto($venta, $data)
    {
        $response = Http::asForm()->post(env('URL_SERVICIOS_PRESUPUESTO') . "/enlaces/dms/presupuesto_multipunto_update/{$data['id']}", [
            'id_cita' => $venta->numero_orden,
            // 'cantidad' => $data['cantidad'],
            // 'descripcion' => $data['descripcion'] ?? 'Sin descripcion',
            'num_pieza' => $data['codigo'],
            // "costo_pieza" => $data['precio'] / $data['cantidad'],
            'diferencia_venta' => $data['precio'] - $data['original'],
            // "horas_mo" => 1,
            // "costo_mo" => $data['costo_mo'],
            // "horas_mo_garantias" => 0,
            // "costo_mo_garantias" => 0,
        ]);

        $refaccion = $response->object();

        info('ACTUALIZAR REFACCION EN PRESUPUESTO', [
            'id_cita' => $venta->numero_orden,
            'cantidad' => $data['cantidad'],
            'descripcion' => $data['descripcion'] ?? 'Sin descripcion',
            'num_pieza' => $data['codigo'],
            "costo_pieza" => $data['precio'],
            "horas_mo" => 1,
            "costo_mo" => $data['costo_mo'],
            "horas_mo_garantias" => 0,
            "costo_mo_garantias" => 0,
        ]);

        if ($response->successful() && optional($refaccion)->estatus == "ok") {
            return $data['id'];
        }

        info('ERROR ACTUALIZACION PRESUPUESTOS', ['body' => $response->body() ]);

        return null;
    }

    /**
     * Eliminamos piezas de los asientos
     *
     * @param App\Models\Refacciones\VentasRealizadasModel $venta
     * @param int $id id_requisicion_index
     * @return $id || null
     */
    public function deletePiezas($venta, $id, $venta_producto)
    {
        $response = Http::withBody(
            json_encode([
                'id' => $id,
            ]),
            'json'
        )
            ->post(env('URL_SERVICIOS') . "api/deletePiezasFromDMS/{$venta->numero_orden}");

        $pieza = $response->object();

        if (!$response->successful() || $pieza->exito == false) {
            info('ERROR AL ELIMINAR PREVENTIVO', [
                'numero_orden' => $venta->numero_orden,
                'id_index_requisicion' => $id,
                'mensaje' => optional($pieza)->message,
            ]);
        }

        info('ELIMINAR REFACCION', [
            'numero_orden' => $venta->numero_orden,
            'id_index_requisicion' => $id,
        ]);

        return $id;


        $this->calcularVentaTotal($venta->id);
    }

    public function calcularVentaTotal($venta_id)
    {
        // total refacciones que no son prepiking o afectan mano de obra
        $total_refacciones = VentaProductoModel::where('venta_id', $venta_id)->where('estatus_producto_orden', '<>', 'B')->where('prepiking', false)->where('afecta_mano_obra', false)->sum('venta_total');

        // total de mano de obra de refacciones
        $total_mano_obra_refacciones = VentaProductoModel::where('venta_id', $venta_id)->where('estatus_producto_orden', '<>', 'B')->where('presupuesto', true)->sum('costo_mano_obra');
        // total servicios
        $total_servicios = $this->servicioVentasServicios->totalServicio($venta_id);

        // Actualizamos tabla ventas con el nuevo total
        VentasRealizadasModel::where(VentasRealizadasModel::ID, $venta_id)
            ->update([
                VentasRealizadasModel::VENTA_TOTAL => $total_refacciones + $total_servicios + $total_mano_obra_refacciones
            ]);
    }

    public function getEreactVentas($request)
    {
        $request->merge(['page' => floor($request->get('start') / ($request->get('length') ?: 1)) + 1]);

        $tableVentaProductos = VentaProductoModel::getTableName();
        $tableVentas = VentasRealizadasModel::getTableName();
        $tableUsuarios = UsuarioModel::getTableName();
        $tableClientes = ClientesModel::getTableName();
        $tableReVentaEstatus = ReVentasEstatusModel::getTableName();
        $tableFolios = FoliosModel::getTableName();
        $tableCuentasPorCobrar = CuentasPorCobrarModel::getTableName();
        $tableProductos = ProductosModel::getTableName();
        //$tableDevolucionVenta = DevolucionVentasModel::getTableName();

        $query = $this->modelo
            ->join($tableVentas, $tableVentas . '.' . VentasRealizadasModel::ID, '=', $tableVentaProductos . '.' . VentaProductoModel::VENTA_ID)
            ->join($tableReVentaEstatus, $tableReVentaEstatus . '.' . ReVentasEstatusModel::VENTA_ID, '=', $tableVentas . '.' . VentasRealizadasModel::ID)
            ->join($tableUsuarios, $tableUsuarios . '.' . UsuarioModel::ID, '=', $tableVentas . '.' . VentasRealizadasModel::VENDEDOR_ID)
            ->join($tableClientes, $tableClientes . '.' . ClientesModel::ID, '=', $tableVentas . '.' . VentasRealizadasModel::CLIENTE_ID)
            ->join($tableFolios, $tableFolios . '.' . FoliosModel::ID, '=', $tableVentas . '.' . VentasRealizadasModel::FOLIO_ID)
            ->join($tableCuentasPorCobrar, $tableCuentasPorCobrar . '.' . CuentasPorCobrarModel::FOLIO_ID, '=', $tableFolios . '.' . FoliosModel::ID)
            ->join($tableProductos, $tableProductos . '.' . ProductosModel::ID, '=', $tableVentaProductos . '.' . VentaProductoModel::PRODUCTO_ID)
            ->select(
                $tableVentaProductos . '.' . VentaProductoModel::ID,
                $tableVentaProductos . '.' . VentaProductoModel::VENTA_ID,
                $tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID,
                $tableVentaProductos . '.' . VentaProductoModel::PRODUCTO_ID,
                $tableVentaProductos . '.' . VentaProductoModel::CANTIDAD,
                $tableVentaProductos . '.' . VentaProductoModel::VALOR_UNITARIO,
                $tableVentaProductos . '.' . VentaProductoModel::TOTAL_VENTA . ' AS venta_total_producto',
                $tableVentas . '.' . VentasRealizadasModel::SUBTOTAL,
                $tableVentas . '.' . VentasRealizadasModel::IVA,
                $tableVentas . '.' . VentasRealizadasModel::VENTA_TOTAL,
                $tableUsuarios . '.' . UsuarioModel::CLAVE_STARS,
                DB::raw("CONCAT(usuarios.nombre,' ',usuarios.apellido_paterno,' ',usuarios.apellido_materno) as nombre_vendedor"),
                $tableClientes . '.' . ClientesModel::NUMERO_CLIENTE,
                $tableClientes . '.' . ClientesModel::RFC,
                $tableClientes . '.' . ClientesModel::CODIGO_POSTAL,
                DB::raw("CONCAT(clientes.nombre,' ',clientes.apellido_paterno,' ',clientes.apellido_materno) as nombre_cliente"),
                $tableFolios . '.' . FoliosModel::FOLIO,
                $tableCuentasPorCobrar . '.' . CuentasPorCobrarModel::CONCEPTO,
                $tableProductos . '.' . ProductosModel::NO_IDENTIFICACION_FACTURA,
                $tableProductos . '.' . ProductosModel::PREFIJO,
                $tableProductos . '.' . ProductosModel::SUFIJO,
                $tableProductos . '.' . ProductosModel::BASICO,
                $tableProductos . '.' . ProductosModel::DESCRIPCION,
                $tableProductos . '.' . ProductosModel::COSTO_PROMEDIO
            );
        if ($request->get(VentasRealizadasModel::FECHA_INICIO) && $request->get(VentasRealizadasModel::FECHA_FIN)) {
            $query->whereBetween($tableVentas . '.' . VentasRealizadasModel::CREATED_AT, [$request->get('fecha_inicio') . ' 00:00:00', $request->get('fecha_fin') . ' 23:59:59']);
        }
        if ($request->get(VentasRealizadasModel::FECHA_INICIO) && !$request->get(VentasRealizadasModel::FECHA_FIN)) {
            $query->whereDate($tableVentas . '.' . VentasRealizadasModel::CREATED_AT, '>=', $request->get('fecha_inicio') . ' 00:00:00');
        }
        if (!$request->get(VentasRealizadasModel::FECHA_INICIO) && $request->get(VentasRealizadasModel::FECHA_FIN)) {
            $query->whereDate($tableVentas . '.' . VentasRealizadasModel::CREATED_AT, '<=', $request->get('fecha_fin') . ' 00:00:00');
        }
        $query->whereIn($tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID, array(EstatusVentaModel::ESTATUS_FINALIZADA, EstatusVentaModel::FACTURA_PAGADA, EstatusVentaModel::ESTATUS_DEVOLUCION));
        $query->where($tableReVentaEstatus . '.' . ReVentasEstatusModel::ACTIVO, TRUE);
        if ($request->paginate) {
            return $query->paginate($request->length);
        } else {
            return $this->handleEreactVenta($query->get());
        }
    }

    public function getEreactVentas_($request)
    {
        $query = VentaProductoModel::select('id', 'venta_id', 'producto_id', 'cantidad', 'valor_unitario', 'venta_total as venta_total_producto')->with([
            'venta' => function ($query) {
                $query->select('id', 'cliente_id', 'vendedor_id', 'subtotal', 'iva', 'venta_total', 'folio_id');
                $query->with([
                    'vendedor' => function ($query_prod) {
                        $query_prod->select('id', 'clave_stars', DB::raw("CONCAT(nombre,' ',apellido_paterno,' ',apellido_materno) as nombre_vendedor"));
                    }
                ]);
                $query->with([
                    'cliente' => function ($query_prod) {
                        $query_prod->select('id', 'numero_cliente', 'rfc', 'codigo_postal', DB::raw("CONCAT(nombre,' ',apellido_paterno,' ',apellido_materno) as nombre_cliente"));
                    }
                ]);
                $query->with([
                    'folio' => function ($query_prod) {
                        $query_prod->select('id', 'folio');
                        $query_prod->with([
                            'cuenta_por_cobrar' => function ($q) {
                                $q->select('folio_id', 'concepto');
                            }
                        ]);
                    }
                ]);
            },
            'producto' => function ($query) {
                $query->select('id', 'no_identificacion', 'gpo', 'prefijo', 'sufijo', 'basico', 'descripcion', 'costo_promedio');
            }
        ]);
        if ($request->get(VentasRealizadasModel::FECHA_INICIO) && $request->get(VentasRealizadasModel::FECHA_FIN)) {
            $query->whereBetween(VentaProductoModel::getTableName() . '.' . VentaProductoModel::CREATED_AT, [$request->get('fecha_inicio') . ' 00:00:00', $request->get('fecha_fin') . ' 23:59:59']);
        }
        if ($request->get(VentasRealizadasModel::FECHA_INICIO) && !$request->get(VentasRealizadasModel::FECHA_FIN)) {
            $query->whereDate(VentaProductoModel::getTableName() . '.' . VentaProductoModel::CREATED_AT, '>=', $request->get('fecha_inicio') . ' 00:00:00');
        }
        if (!$request->get(VentasRealizadasModel::FECHA_INICIO) && $request->get(VentasRealizadasModel::FECHA_FIN)) {
            $query->whereDate(VentaProductoModel::getTableName() . '.' . VentaProductoModel::CREATED_AT, '<=', $request->get('fecha_fin') . ' 00:00:00');
        }
        $consulta = $query->paginate();
        return $this->handleEreactVenta($consulta);
    }

    private function handleEreactVenta($query)
    {
        $handle = [];
        foreach ($query as $val) {
            $precio_venta = str_replace(',', '', number_format($val->venta_total_producto, 2));
            $valor['numero_parte'] = $this->validarEspacios('DRE' . $val->no_identificacion, 75, ' ', 1);
            $valor['prefijo'] = $this->validarEspacios($val->prefijo, 25, ' ', 1);
            $valor['basico'] = $this->validarEspacios($val->basico, 25, ' ', 1);
            $valor['sufijo'] = $this->validarEspacios($val->sufijo, 25, ' ', 1);
            $valor['descripcion'] = $this->validarEspacios($val->descripcion, 10, ' ', 1);
            $valor['clave_star'] = $this->validarEspacios($val->clave_star, 10, ' ', 1);
            $valor['nombre_vendedor'] = $this->validarEspacios($val->nombre_vendedor, 100, ' ', 1);
            $valor['clave_cliente'] = $this->validarEspacios($val->numero_cliente, 10, ' ', 1);
            $valor['nombre_cliente'] = $this->validarEspacios($val->nombre_cliente, 100, ' ', 1);
            $valor['rfc_cliente'] = $this->validarEspacios($val->rfc, 15, ' ', 1);
            if ($val->estatus_ventas_id == 5) {
                $cantidad_ = $this->validarEspacios('-' . $val->cantidad, 9, 0, 1);
            } else {
                $cantidad_ = $this->validarEspacios($val->cantidad, 9, 0, 1);
            }

            $precio_venta_ = $this->validarEspacios($val->venta_total_producto, 12, 0, 2);
            $costo_ = $this->validarEspacios($val->costo_promedio, 2, 12, 0, 2);
            $valor['cpcf'] = $cantidad_ .  $precio_venta_ . $costo_ . date('Ymd');
            $valor['factura_folio'] = $this->validarEspacios($val->folio, 10, ' ', 1);
            $valor['tipo_venta'] = $this->validarEspacios($val->tipo_venta ?? 1, 1, ' ', 1);
            $valor['iva'] = $this->validarEspacios(number_format($precio_venta * 0.16, 2), 12, ' ', 2);
            $valor['codigo_postal'] = $this->validarEspacios($val->codigo_postal, 5, ' ', 1);
            array_push($handle, $valor);
        }

        return ($handle);
    }
    public function validarEspacios($value, $cantidad_espacios, $elemento_remplazo = '*', $antes_despues = 2)
    {
        return strlen($value) <= $cantidad_espacios ? ($antes_despues == 1 ? $value . str_repeat($elemento_remplazo,  $cantidad_espacios - strlen($value)) : str_repeat($elemento_remplazo,  $cantidad_espacios - strlen($value)) . $value)  :  substr($value, 0, $cantidad_espacios);
    }

    public  function handleTxtFile($data)
    {
        Storage::makeDirectory('ereact');
        $folder = 'ereact/';
        $file_name = 'vip_sal_mex_137' . date('YmdHmi') . ".txt";
        $path = $folder . $file_name;

        $storage = Storage::disk('local');
        $storage->append($path, "HDRDEALERSA003707VIPSA" . date('YmdHmi'));
        $storage->append($path, "IREMEX137       " . date('YmdHmi'));

        foreach ($data as $val) {
            $strText = $val['numero_parte'] . $val['prefijo'] . $val['basico'] . $val['sufijo'] . $val['descripcion'] .
                $val['clave_star'] . $val['nombre_vendedor'] . $val['clave_cliente'] . $val['nombre_cliente'] . $val['rfc_cliente'] .
                $val['cpcf'] . '     ' . $val['factura_folio'] . $val['tipo_venta'] . $val['iva'] . $val['codigo_postal'];
            $storage->append($path, $strText);
        }
        $storage->append($path, "TRLDEALERSA0037070000000068");
        return $file_name;
    }
}
