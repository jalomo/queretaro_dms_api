<?php

namespace App\Servicios\Refacciones;

use App\Models\Refacciones\ComentariosProactivosModel;
use App\Servicios\Core\ServicioDB;
use Illuminate\Http\Request;
use DB;

class ServicioComentariosProactivo extends ServicioDB
{

    public function __construct()
    {
        $this->recurso = 'comentarios proactivo';
        $this->modelo = new ComentariosProactivosModel();
    }

    public function getReglasGuardar()
    {
        return [
            ComentariosProactivosModel::DESCRIPCION => 'required|string',
            ComentariosProactivosModel::COTIZADOR_REFACCION_ID => 'required|numeric|exists:cotizador_refacciones,id',
            ComentariosProactivosModel::USUARIO_ID => 'required|numeric|exists:usuarios,id',
        ];
    }
    public function getReglasUpdate()
    {
        return [
            ComentariosProactivosModel::DESCRIPCION => 'required|string',
            ComentariosProactivosModel::COTIZADOR_REFACCION_ID => 'required|numeric|exists:cotizador_refacciones,id',
            ComentariosProactivosModel::USUARIO_ID => 'required|numeric',
        ];
    }
    public function getReglasPorCotizadorRefaccionID()
    {
        return [
            ComentariosProactivosModel::COTIZADOR_REFACCION_ID => 'required|numeric|exists:cotizador_refacciones,id',
        ];
    }
    public function getByCotizadorRefaccion($cotizador_id)
    {

        return $this->modelo
        ->with([ComentariosProactivosModel::REL_USUARIOS])->where(ComentariosProactivosModel::COTIZADOR_REFACCION_ID, $cotizador_id)->get();
    }
}
