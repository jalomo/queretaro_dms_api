<?php

namespace App\Servicios\Refacciones;

use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\Almacenes;

class ServicioAlmacenes extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'almacenes';
        $this->modelo = new Almacenes();
    }

    public function getReglasGuardar()
    {
        return [
            Almacenes::NOMBRE => 'required',
            Almacenes::UBICACION => 'required',
            Almacenes::CODIGO_ALMACEN => 'required',
        ];
    }
    public function getReglasUpdate()
    {
        return [
            Almacenes::NOMBRE => 'required',
            Almacenes::CODIGO_ALMACEN => 'required',
            Almacenes::UBICACION => 'required',
        ];
    }
}
