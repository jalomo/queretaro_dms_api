<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class TipoPedidoModel extends Modelo
{
    protected $table = 'cat_tipo_pedido';
    const ID = "id";
    const NOMBRE = "nombre";
    const CLAVE = "clave";

    protected $fillable = [
        self::NOMBRE,
        self::CLAVE
    ];
}
