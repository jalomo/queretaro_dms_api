<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;
use App\Models\Usuarios\User;

class ComentariosProactivosModel extends Modelo
{
    protected $table = 'comentarios_proactivo';
    const ID = "id";
    const DESCRIPCION = "descripcion";
    const USUARIO_ID = "usuario_id";
    const COTIZADOR_REFACCION_ID = 'cotizador_refaccion_id';

    const REL_USUARIOS = "usuarios";

    protected $fillable = [
        self::COTIZADOR_REFACCION_ID,
        self::DESCRIPCION,
        self::USUARIO_ID
    ];

    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:m',
     ];

    public function getFromDateAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y');
    }
    public function usuarios()
    {
        return $this->belongsTo(User::class, self::USUARIO_ID)->select(array('id', 'nombre', 'apellido_paterno', 'apellido_materno'));;
    }
}
