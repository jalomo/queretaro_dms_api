<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;
use App\Models\Refacciones\CatalogoMarcasModel;
use App\Models\Autos\CatModelosModel;
class CotizadorRefaccionesModel extends Modelo
{
    protected $table = 'cotizador_refacciones';
    const ID = "id";
    const NOMBRE_CONTACTO = "nombre_contacto";
    const TELEFONO = "telefono";
    const MARCA = "marca";
    const MODELO = "modelo";
    const ANIO = "anio";
    const NO_SERIE = "no_serie";
    const VENTA_ID = "venta_id";
    const MARCA_ID = "marca_id";
    const MODELO_ID = "modelo_id";
    
    const REL_MARCA = "marca";
    const REL_MODELO = "modelo";
    const REL_VENTA = "venta";

    protected $fillable = [
        self::NOMBRE_CONTACTO,
        self::TELEFONO,
        self::MARCA_ID,
        self::MODELO_ID,
        self::ANIO,
        self::NO_SERIE,
        self::VENTA_ID
    ];

    public function venta()
    {
        return $this->belongsTo(VentasRealizadasModel::class);
    }

    public function marca()
    {
        return $this->belongsTo(CatalogoMarcasModel::class);
    }

    public function modelo()
    {
        return $this->belongsTo(CatModelosModel::class);
    }
}
