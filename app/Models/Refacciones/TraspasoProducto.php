<?php

namespace App\Models\Refacciones;

use Illuminate\Database\Eloquent\Model;

class TraspasoProducto extends Model
{
    protected $table = 'traspaso_productos';

    protected $fillable = [
        'producto_id',
        'traspaso_id',
        'no_identificacion',
        'cantidad',
        'cantidad_entregada',
        'cantidad_devuelta',
        'valor_unitario',
    ];

    protected $casts = [
        'cantidad' => 'int',
        'cantidad_entregada' => 'int',
        'cantidad_entregada' => 'int',
    ];

    public function traspaso()
    {
        return $this->belongsTo(Traspasos::class, 'traspasos_id');
    }

    public function producto()
    {
        return $this->belongsTo(ProductosModel::class, 'no_identificacion', 'no_identificacion');
    }
}
