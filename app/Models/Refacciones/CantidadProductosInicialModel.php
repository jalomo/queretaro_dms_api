<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class CantidadProductosInicialModel extends Modelo
{
    protected $table = 'cat_productos_cantidad_inicial';
    const ID = "id";
    const PRODUCTO_ID = "producto_id";
    const CANTIDAD = "cantidad";

    protected $fillable = [
        self::PRODUCTO_ID,
        self::CANTIDAD,
    ];
}
