<?php

namespace App\Models\Refacciones;

use Illuminate\Database\Eloquent\Model;

class EstatusTraspaso extends Model
{
    protected $table = 'estatus_traspaso';

    const GENERADO = 1;
    const ACTUALIZADO = 2;
    const PEDIDO = 3;
    const PROCESANDO = 4;
    const ENVIADO = 5;
    const FINALIZADO = 6;
    const CANCELADO = 7;
    const PROCESANDO_DEVOLUCION = 8;
    const ENVIO_DEVOLUCION = 9;
    const DEVUELTO = 10;
    const FIRMA_SOLICITADO = 11;
    const FIRMA_AUTORIZADO = 12;
    const SUBIR_EVIDENCIA = 13;

    protected $fillable = [
        'nombre'
    ];
}
