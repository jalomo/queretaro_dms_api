<?php

namespace App\Models\Refacciones;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HistorialPresupuestos extends Model
{
    //
    use SoftDeletes;

    protected $fillable = [
        'venta_id',
        'venta_producto_id',
        'user_id',
        'producto_id',
        'producto_id_latest',
        'cantidad',
        'cantidad_anterior',
        'precio_refaccion',
        'precio_refaccion_anterior',
        'costo_mano_obra',
        'costo_mano_obra_anterior',
    ];

    public function producto()
    {
        return $this->belongsTo('producto_id');
    }

    public function ultimo_producto()
    {
        return $this->belongsTo('producto_id', 'id', 'producto_id_latest');
    }

    public function venta_producto()
    {
        return $this->belongsTo('venta_producto', 'id', 'venta_producto_id');
    }

    public function venta()
    {
        return $this->belongsTo('venta');
    }
}
