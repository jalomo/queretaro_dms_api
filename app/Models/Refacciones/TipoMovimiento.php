<?php

namespace App\Models\Refacciones;

use Illuminate\Database\Eloquent\Model;

class TipoMovimiento extends Model
{
    //
    protected $table = 'tipo_movimiento';

    public $timestamps = false;

    const COMPRA = 1;
    const VENTA_MOSTRADOR = 2;
    const DEVOLUCION_MOSTRADOR = 3;
    const TRASPASO = 4;
    const CANCELACION_TRASPASO = 5;
    const PEDIDO = 6;
    const DEVOLUCION_PEDIDO = 7;
    const APARTADO = 8;
    const DESAPARTADO = 9;
    const VENTA_DETALLES = 10;
    const DEVOLUCION_VENTA_DETALLES = 11;
    const AJUSTE_AUDITORIA_AUMENTO = 12;
    const AJUSTE_AUDITORIA_DISMINUCION = 13;

    const EN_PROCESO_VENTA_MOSTRADOR = 14;
    const EN_PROCESO_VENTA_DETALLES = 15;
    const EN_PROCESO_DEVOLUCION_PEDIDO = 16;
    const EN_PROCESO_APARTAR = 17;
    const EN_PROCESO_AJUSTE_AUDITORIA_DISMINUCION = 18;
    const EN_PROCESO_TRASPASO = 19;

}
