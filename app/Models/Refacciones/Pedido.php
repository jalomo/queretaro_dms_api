<?php

namespace App\Models\Refacciones;

use App\Models\Usuarios\User;
use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    protected $fillable = [
        'actual',
        'entrada',
        'ford',
        'tipo',
    ];
    
    //
    public function productos()
    {
        return $this->belongsToMany(ProductosModel::class, 'pedidos_productos', 'pedido_id', 'producto_id')
            ->as('pedido')
            ->withPivot('id', 'cantidad', 'factura', 'proveedor', 'cantidad_entregada', 'precio', 'total', 'model_id', 'created_at');
    }

    public function piezas()
    {
        return $this->hasMany(PedidosProductos::class, 'pedido_id');
    }

    public function usuario()
    {
        return $this->belongsTo(User::class, 'autorizado', 'id');
    }
}
