<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;
use App\Models\Refacciones\FoliosModel;
use App\Models\Refacciones\ClientesModel;
use App\Models\Refacciones\Precios;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\StockProductosModel;
use VentasRealizadas;

class VentaProductoModel extends Modelo
{
    protected $table = 'venta_producto';
    const ID = "id";
    const PRODUCTO_ID = "producto_id";
    const VENTA_ID = "venta_id";
    const PRECIO_ID = "precio_id";
    const CLIENTE_ID = "cliente_id";
    const FOLIO_ID = "folio_id";
    const CANTIDAD = "cantidad";
    const TOTAL_VENTA = "venta_total";
    const VALOR_UNITARIO = "valor_unitario";
    const VALOR_UNITARIO_ORIGINAL = "valor_unitario_original";
    const DESCONTADO_ALMACEN = "descontado_almacen";
    const ORDEN_ORIGINAL = "orden_original";
    const ESTATUS_PRODUCTO_ORDEN = "estatus_producto_orden";
    const ID_INDEX_REQUISICION = "id_index_requisicion";
    const PRECIO_MANUAL = "precio_manual";
    const ALMACEN_ID = 'almacen_id';
    const PREPIKING = 'prepiking';
    const AFECTA_MANO_OBRA = 'afecta_mano_obra';
    const TIPO_DESCUENTO_ID = 'tipo_descuento_id';
    const PREVENTIVO = 'preventivo';
    const COSTO_ADICIONAL = 'costo_adicional';
    const PRESUPUESTO = 'presupuesto';
    const BODYSHOP = 'bodyshop';
    const DETALLES = 'detalles';
    const COSTO_MANO_OBRA = 'costo_mano_obra';
    const AFECTA_PAQUETE = 'afecta_paquete';
    const COSTO_PROMEDIO = 'costo_promedio';
    const CORE = 'core';
    const VENTA_SERVICIO_ID = 'venta_servicio_id';
    const VENDEDOR_ID = 'vendedor_id';

    const REL_CLIENTE = 'cliente';
    const REL_PRODUCTO = 'producto';
    const REL_VENTA = 'venta';

    const ESTATUS_PRODUCTO_ORDEN_O = 'O'; //ORIGINAL
    const ESTATUS_PRODUCTO_ORDEN_B = 'B'; //BORRADO
    const ESTATUS_PRODUCTO_ORDEN_A = 'A'; //AGREGADO

    CONST IVA = 1.16;

    protected $fillable = [
        self::PRODUCTO_ID,
        self::CLIENTE_ID,
        self::TOTAL_VENTA,
        self::VALOR_UNITARIO,
        self::VALOR_UNITARIO_ORIGINAL,
        self::PRECIO_ID,
        self::FOLIO_ID,
        self::CANTIDAD,
        self::VENTA_ID,
        self::DESCONTADO_ALMACEN,
        self::ORDEN_ORIGINAL,
        self::ESTATUS_PRODUCTO_ORDEN,
        self::ID_INDEX_REQUISICION,
        self::PRECIO_MANUAL,
        self::ALMACEN_ID,
        self::PREPIKING,
        self::AFECTA_MANO_OBRA,
        self::TIPO_DESCUENTO_ID,
        self::PREVENTIVO,
        self::COSTO_ADICIONAL,
        self::PRESUPUESTO,
        self::DETALLES,
        self::BODYSHOP,
        self::COSTO_MANO_OBRA,
        self::AFECTA_PAQUETE,
        self::CORE,
        self::COSTO_PROMEDIO,
        self::VENTA_SERVICIO_ID,
        self::VENDEDOR_ID,
    ];

    public function producto()
    {
        return $this->belongsTo(ProductosModel::class, VentaProductoModel::PRODUCTO_ID, ProductosModel::ID);
    }

    public function precio()
    {
        return $this->belongsTo(Precios::class);
    }

    public function cliente()
    {
        return $this->belongsTo(ClientesModel::class);
    }

    public function rel_folio()
    {
        return $this->hasOne(FoliosModel::class, FoliosModel::ID);
    }

    public function venta()
    {
        return $this->belongsTo(VentasRealizadasModel::class);
    }

    public function folio()
    {
        return $this->belongsTo(FoliosModel::class, FoliosModel::ID);
    }
}
