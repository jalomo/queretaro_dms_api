<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class Traspasos extends Model
{
    protected $table = 'traspasos';

    const ESTATUS_FINALIZADO = 1;
    const ESTATUS_NO_FINALIZADO = 0;
    
    protected $fillable = [
        'folio',
        'origen_almacen_id',
        'destino_almacen_id',
        'venta_id',
        'firma_solicitud',
        'firma_autorizacion',
        'traspaso_directo',
    ];

    public function productos()
    {
        return $this->hasMany(TraspasoProducto::class);
    }

    public function estatus()
    {
        return $this->hasMany(ReEstatusTraspaso::class);
    }

    public function almacen_origen()
    {
        return $this->belongsTo(Almacenes::class, 'origen_almacen_id');
    }

    public function almacen_destino()
    {
        return $this->belongsTo(Almacenes::class, 'destino_almacen_id');
    }
}
