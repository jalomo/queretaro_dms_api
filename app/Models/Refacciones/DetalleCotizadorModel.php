<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class DetalleCotizadorModel extends Modelo
{
    protected $table = 'detalle_cotizador';
    const ID = "id";
    const COTIZADOR_ID = "cotizador_id";
    const CANTIDAD = "cantidad";
    const DESCRIPCION = "descripcion";
    const NUMERO_PARTE = "numero_parte";
    const PRECIO_NETO = "precio_neto";
    
    protected $fillable = [
        self::COTIZADOR_ID,
        self::CANTIDAD,
        self::DESCRIPCION,
        self::NUMERO_PARTE,
        self::PRECIO_NETO
    ];
}
