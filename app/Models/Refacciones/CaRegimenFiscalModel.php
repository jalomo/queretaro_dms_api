<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class CaRegimenFiscalModel extends Modelo
{
    protected $table = 'ca_regimen_fiscal';
    const ID = "id";
    const NOMBRE = "nombre";
    const CLAVE = "clave";
    const PERSONA_FISICA = "persona_fisica";
    const PERSONA_MORAL = "persona_moral";
    
    protected $fillable = [
        self::NOMBRE,
        self::CLAVE,
        self::PERSONA_FISICA,
        self::PERSONA_MORAL,
    ];
}
