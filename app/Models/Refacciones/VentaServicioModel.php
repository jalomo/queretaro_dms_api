<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\SoftDeletes;

class VentaServicioModel extends Modelo
{
    use SoftDeletes;

    protected $table = 'venta_servicio';
    const ID = "id";
    const VENTA_ID = 'venta_id';
    const NOMBRE_SERVICIO = 'nombre_servicio';
    const NO_IDENTIFICACION = 'no_identificacion';
    const CANTIDAD = 'cantidad';
    const PRECIO = 'precio';
    const COSTO_MO = 'costo_mo';
    const IVA = 'iva';
    const REFACCIONES = 'refacciones';
    const PREVENTIVO = 'preventivo';
    const PRESUPUESTO = 'presupuesto';
    const BODYSHOP = 'bodyshop';
    const ID_INDEX_REQUISICION = 'id_index_requisicion';
    
    // const NO_SERVICIO = 'no_servicio';

    protected $fillable = [
        self::VENTA_ID,
        self::NOMBRE_SERVICIO,
        self::PRECIO,
        self::NO_IDENTIFICACION,
        self::CANTIDAD,
        self::COSTO_MO,
        self::IVA,
        self::REFACCIONES,
        self::PREVENTIVO,
        self::PRESUPUESTO,
        self::BODYSHOP,
        self::ID_INDEX_REQUISICION,
        // self::NO_SERVICIO
    ];


    public function venta()
    {
        return $this->belongsTo(VentasRealizadasModel::class);
    }
}
