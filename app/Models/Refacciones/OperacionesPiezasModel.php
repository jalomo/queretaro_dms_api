<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;
use App\Servicios\Core\ServicioDB;

class OperacionesPiezasModel extends Modelo
{
    protected $table = 'operaciones_piezas';
    const ID = "id";
    const NO_ORDEN = "no_orden";
    const NO_OPERACION = "no_operacion";
    const ID_REFACCION = "id_refaccion";
    const TOTAL = "total";
    
    protected $fillable = [
        self::NO_ORDEN,
        self::NO_OPERACION,
        self::ID_REFACCION,
        self::TOTAL
    ];
}
