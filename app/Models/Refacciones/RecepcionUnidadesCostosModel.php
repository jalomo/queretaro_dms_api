<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class RecepcionUnidadesCostosModel extends Modelo
{
    protected $table = 'unidades_costos';
    const ID = "id";
    const VALOR_UNIDAD_COSTO = "valor_unidad";
    const VALOR_UNIDAD_VENTA = "valor_unidad_venta";
    const OTROS_GASTOS = "otros_gastos";
    const EQUIPO_BASE_COSTO = "equipo_base_costo";
    const EQUIPO_BASE_VENTA = "equipo_base_venta";
    const GASTOS_ACONDI = "gastos_acondi";
    const SEG_TRASLADO_COSTO = "seg_traslado_costo";
    const SEG_TRASLADO_VENTA = "seg_traslado_venta";
    const AP_FONDOS_PUB = "ap_fondos_pub";
    const IMPUESTO_IMPORT_COSTO = "impuesto_import_costo";
    const IMPUESTO_IMPORT_VENTA = "impuesto_import_venta";
    const AP_PROG_CIVIL = "ap_prog_civil";
    const FLETES_IMPORT_COSTO = "fletes_import_costo";
    const FLETES_IMPORT_VENTA = "fletes_import_venta";
    const CUOTA_AMDA = "cuota_amda";
    const GASTOS_TRASLADO_COSTO = "gastos_traslado_costo";
    const GASTOS_TRASLADO_VENTA = "gastos_traslado_venta";
    const CUOTA_COPARMEX = "cuota_coparmex";
    const DEDUCCION_FORD_COSTO = "deduccion_ford_costo";
    const DEDUCCION_FORD_VENTA = "deduccion_ford_venta";
    const CUOTA_ASOCIACION_FORD = "cuota_asociacion_ford";
    const BONIFICACION_COSTO = "bonificacion_costo";
    const BONIFICACION_VENTA = "bonificacion_venta";
    const SUBTOTAL_COSTO = "subtota_costo";
    const SUBTOTAL_VENTA = "subtota_venta";
    const IVA_COSTO = "iva_costo";
    const IVA_VENTA = "iva_venta";
    const TOTAL_COSTO = "total_costo";
    const TOTAL_VENTA = "total_venta";
    const UNIDAD_ID = 'unidad_id';

    protected $fillable = [
        self::VALOR_UNIDAD_COSTO,
        self::VALOR_UNIDAD_VENTA,
        self::OTROS_GASTOS,
        self::EQUIPO_BASE_COSTO,
        self::EQUIPO_BASE_VENTA,
        self::GASTOS_ACONDI,
        self::SEG_TRASLADO_COSTO,
        self::SEG_TRASLADO_VENTA,
        self::AP_FONDOS_PUB,
        self::IMPUESTO_IMPORT_COSTO,
        self::IMPUESTO_IMPORT_VENTA,
        self::AP_PROG_CIVIL,
        self::FLETES_IMPORT_COSTO,
        self::FLETES_IMPORT_VENTA,
        self::CUOTA_AMDA,
        self::GASTOS_TRASLADO_COSTO,
        self::GASTOS_TRASLADO_VENTA,
        self::CUOTA_COPARMEX,
        self::DEDUCCION_FORD_COSTO,
        self::DEDUCCION_FORD_VENTA,
        self::CUOTA_ASOCIACION_FORD,
        self::BONIFICACION_COSTO,
        self::BONIFICACION_VENTA,
        self::SUBTOTAL_COSTO,
        self::SUBTOTAL_VENTA,
        self::IVA_COSTO,
        self::IVA_VENTA,
        self::TOTAL_COSTO,
        self::TOTAL_VENTA,
        self::UNIDAD_ID
    ];
}
