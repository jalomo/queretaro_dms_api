<?php

namespace App\Models\Refacciones;

use App\Casts\MexicoTimezoneCast;
use Illuminate\Database\Eloquent\Model;

class InventarioProductos extends Model
{
    //
    protected $table = 'inventario_productos';

    protected $casts = [
        'created_at' => MexicoTimezoneCast::class
    ];

    protected $fillable = [
        'tipo_movimiento_id',
        'cantidad',
        'existe',
        'existia',
        'model_id',
        'precio',
        'costo_promedio',
    ];

    public function producto()
    {
        return $this->belongsTo(ProductosModel::class);
    }

    public function tipo_movimiento()
    {
        return $this->belongsTo(TipoMovimiento::class);
    }

    public function pedido()
    {
        return $this->belongsTo(Pedido::class, 'model_id');
    }

    public function venta()
    {
        return $this->belongsTo(VentaProductoModel::class, 'model_id');
    }

    public function venta_producto()
    {
        return $this->belongsTo(VentaProductoModel::class, 'model_id');
    }
}
