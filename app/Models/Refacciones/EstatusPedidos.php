<?php

namespace App\Models\Refacciones;

use Illuminate\Database\Eloquent\Model;

class EstatusPedidos extends Model
{
    protected $table = 'estatus_pedidos';

    const SIN_AUTORIZAR = 1;
    const ACTUALIZADO = 2;
    const PEDIDO = 3;
    const AUTORIZADO = 4;
    const PEDIDO_CARGADO = 5;

    protected $fillable = [
        'nombre'
    ];
}
