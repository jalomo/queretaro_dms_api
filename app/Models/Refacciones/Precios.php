<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class Precios extends Modelo
{
    protected $table = 'precio';
    const ID = "id";
    const PRECIO_PUBLICO = "precio_publico";
    const PRECIO_MAYOREO = "precio_mayoreo";
    const PRECIO_INTERNO = "precio_interno";
    const PRECIO_TALLER = "precio_taller";
    const PRECIO_OTRAS_DISTRIBUIDORAS = "precio_otras_distribuidoras";
    const IMPUESTO = 'impuesto';
    const DESCRIPCION = 'descripcion';

    //
    //id para filtrar en vista
    const ID_VISTA = 'id_vista';
    const ID_VMOSTRADOR = 1;
    const ID_SEXCELLENT = 2;
    const ID_TODAS = 3;

    //niveles precio
    const NIVEL_TRADICIONAL = 1;
    const NIVEL_GARANTIAS = 2;
    const NIVEL_INTERNO = 3;
    const NIVEL_HOJALATERIA = 4;

    //tipo precio para filtrar en vistas
    const ID_PP = 1; //p publico
    const ID_PM = 2; //p mayoreo
    const ID_PI = 3; //p interno
    const ID_PT = 4; //p taller
    const ID_POD = 5; //p otras dist

   
    const DEFAULT_PRECIO = 1;

    protected $fillable = [
        self::PRECIO_PUBLICO,
        self::PRECIO_MAYOREO,
        self::PRECIO_INTERNO,
        self::PRECIO_TALLER,
        self::PRECIO_OTRAS_DISTRIBUIDORAS,
        self::IMPUESTO,
        self::DESCRIPCION
    ];
}
