<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class EstatusVentaModel extends Modelo
{
    protected $table = 'estatus_venta';
    const ID = "id";
    const NOMBRE = "nombre";

    const ESTATUS_PROCESO = 1;
    const ESTATUS_FINALIZADA = 2;
    const FACTURA_PAGADA = 3;
    const ESTATUS_CANCELADA = 4;
    const ESTATUS_DEVOLUCION = 5;
    const ESTATUS_APARTADO = 6;
    const ESTATUS_DESAPARTADO= 7;

    // ANTERIORMENTE
    // const ESTATUS_CANCELADA = 1;
    // const ESTATUS_PROCESO = 2;
    // const ESTATUS_FINALIZADA = 3;
    // const FACTURA_PAGADA = 4;
    // const ESTATUS_DEVOLUCION = 5;
    
    protected $fillable = [
        self::NOMBRE
    ];
}
