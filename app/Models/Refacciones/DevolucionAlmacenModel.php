<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class DevolucionAlmacenModel extends Modelo
{
    protected $table = 'devolucion_almacen';
    const ID = "id";
    const ORDEN_COMPRA_ID = "orden_compra_id";
    const PRODUCTO_ID = "producto_id";
    const ALMACEN_ID = "almacen_id";
    const CANTIDAD = "cantidad";
    
    protected $fillable = [
        self::ORDEN_COMPRA_ID,
        self::PRODUCTO_ID,
        self::ALMACEN_ID,
        self::CANTIDAD
    ];
}
