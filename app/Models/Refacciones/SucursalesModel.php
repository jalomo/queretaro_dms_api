<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class SucursalesModel extends Modelo
{
    protected $table = 'sucursales';
    const ID = "id";
    const NOMBRE = "nombre";
    
    protected $fillable = [
        self::NOMBRE
    ];
}
