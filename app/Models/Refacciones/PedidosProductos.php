<?php

namespace App\Models\Refacciones;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PedidosProductos extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'cantidad',
        'pedido_id',
        'producto_id',
    ];

    public function venta()
    {
        return $this->belongsTo(VentasRealizadasModel::class, 'model_id');
    }

    public function pedido()
    {
        return $this->belongsTo(Pedido::class);
    }

    public function producto()
    {
        return $this->belongsTo(ProductosModel::class);
    }
}