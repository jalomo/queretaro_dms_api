<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class UbicacionProducto extends Modelo
{
    protected $table = 'ubicacion_producto';
    const ID = "id";
    const UBICACION = "ubicacion";
    const PRODUCTO_ID = "producto_id";

    protected $fillable = [
        self::UBICACION,
        self::PRODUCTO_ID,
    ];
}
