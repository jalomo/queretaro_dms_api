<?php

namespace App\Models\Refacciones;

use Illuminate\Database\Eloquent\Model;

class ReEstatusTraspaso extends Model
{
    protected $table = 're_estatus_traspaso';

    protected $fillable = [
        'traspaso_id',
        'estatus_id',
        'user_id',
    ];
}
