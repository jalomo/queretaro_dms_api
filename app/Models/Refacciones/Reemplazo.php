<?php

namespace App\Models\Refacciones;

use Illuminate\Database\Eloquent\Model;

class Reemplazo extends Model
{
    //

    protected $fillable = [
        'producto_id',
        'reemplazo_id',
        'precio_producto',
        'precio_reemplazo'
    ];

    public function producto()
    {
        return $this->belongsTo(ProductosModel::class, 'producto_id');
    }

    public function reemplazo()
    {
        return $this->belongsTo(ProductosModel::class, 'reemplazo_id');
    }
}
