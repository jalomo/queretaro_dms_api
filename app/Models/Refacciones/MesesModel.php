<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class MesesModel extends Modelo
{
    protected $table = 'cat_meses';
    const ID = "id";
    const NOMBRE = "nombre";

    protected $fillable = [
        self::NOMBRE
    ];
}
