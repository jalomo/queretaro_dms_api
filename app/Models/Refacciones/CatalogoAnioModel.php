<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class CatalogoAnioModel extends Modelo
{
    protected $table = 'catalogo_anio';
    const ID = "id";
    const NOMBRE = "nombre";

    protected $fillable = [
        self::NOMBRE
    ];
}
