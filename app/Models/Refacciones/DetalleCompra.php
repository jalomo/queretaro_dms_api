<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class DetalleCompra extends Modelo
{
    protected $table = 'detalle_compra';
    const ID = "id";
    const COMPRA_ID = "compra_id";
    const PRODUCTO_ID = "producto_id";
    const CANTIDAD = "cantidad";
    const COSTO = "costo";
    const TOTAL = "total";

    protected $fillable = [
        self::PRODUCTO_ID,
        self::COMPRA_ID,
        self::CANTIDAD,
        self::COSTO,
        self::TOTAL
    ];
}
