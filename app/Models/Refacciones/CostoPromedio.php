<?php

namespace App\Models\Refacciones;

use Illuminate\Database\Eloquent\Model;

class CostoPromedio extends Model
{
    const ENTRADA = 1;
    const SALIDA = 2;
    const CATALOGO = 3;
    
    protected $table = 'costo_promedio';
    
    protected $fillable = [
        'movimiento_id',
        'producto_id',
        'existencia',
        'entrada',
        'costo_existencia',
        'costo_movimiento',
        'costo_promedio_existencia',
        'costo_promedio_movimiento',
        'costo_promedio',
        'valor_unitario_existencia',
        'valor_unitario_movimiento',
    ];

    public function producto()
    {
        return $this->belongsTo(ProductosModel::class);
    }
}
