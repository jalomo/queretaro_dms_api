<?php

namespace App\Models\Refacciones;

use Illuminate\Database\Eloquent\Model;

class HistorialEreact extends Model
{
    //
    protected $table = 'historial_ereact';

    protected $fillable = [
      'fecha_transmision',
      'nombre_archivo',
      'filters',
      'consecutivo',
    ];
}
