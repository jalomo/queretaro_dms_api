<?php

namespace App\Models\Facturas;

use App\Models\Core\Modelo;

class ComprobanteFactura extends Modelo
{
    protected $table = 'comprobante_factura';
    const ID = "id";
    const VERSION = 'version';
    const FECHA = 'fecha';
    const MONEDA = 'moneda';
    const TIPO_CAMBIO = 'tipo_cambio';
    const SUBTOTAL = 'SubTotal';
    const TOTAL = 'Total';
    const FORMA_PAGO = 'FormaPago';
    const CONDICIONES_PAGO = 'condiciones_de_pago';
    const TIPO_COMPROBANTE = 'tipo_comprobante';
    const METODO_PAGO = 'metodo_pago';
    const LUGAR_EXPEDICION = 'lugar_expedicion';
    const NO_CERTIFICADO = 'no_certificado';
    const SERIE = 'serie';
    const FOLIO = 'folio';
    const CERTIFICADO = 'certificado';
    const SELLO = 'sello';
    const FACTURA_ID = "factura_id";

    protected $fillable = [
        self::VERSION,
        self::FECHA,
        self::MONEDA,
        self::TIPO_CAMBIO,
        self::SUBTOTAL,
        self::TOTAL,
        self::FORMA_PAGO,
        self::CONDICIONES_PAGO,
        self::TIPO_COMPROBANTE,
        self::METODO_PAGO,
        self::LUGAR_EXPEDICION,
        self::NO_CERTIFICADO,
        self::SERIE,
        self::FOLIO,
        self::CERTIFICADO,
        self::SELLO,
        self::FACTURA_ID
    ];
}
