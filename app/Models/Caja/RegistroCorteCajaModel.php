<?php

namespace App\Models\Caja;

use App\Models\Core\Modelo;

class RegistroCorteCajaModel extends Modelo
{
    protected $table = 'registro_corte_caja';

    const ID = "id";
    const FECHA_REGISTRO = 'fecha_registro';
    const DESCRIPCION = 'descripcion';
    const FOLIO_ID = 'folio_id';
    const TIPO_PAGO_ID = 'tipo_pago_id';
    const TOTAL_CANTIDAD_CAJA = 'total_cantidad_caja';
    const TOTAL_CANTIDAD_REPORTADA = 'total_cantidad_reportada';
    const FALTANTES = 'faltantes';
    const SOBRANTES = 'sobrantes';

    protected $fillable = [
        self::FECHA_REGISTRO,
        self::DESCRIPCION,
        self::FOLIO_ID,
        self::TIPO_PAGO_ID,
        self::TOTAL_CANTIDAD_CAJA,
        self::TOTAL_CANTIDAD_REPORTADA,
        self::FALTANTES,
        self::SOBRANTES
    ];
}
