<?php

namespace App\Models\Caja;

use App\Models\Core\Modelo;

class EstatusAnticiposModel extends Modelo
{
    protected $table = 'estatus_anticipos';
    
    const ID = "id";
    const NOMBRE = 'nombre';

    protected $fillable = [
        self::NOMBRE
    ];

    const ESTATUS_ADQUIRIDO = 1;
    const ESTATUS_APLICADO = 2;
    const ESTATUS_ANULADO = 3;
}
