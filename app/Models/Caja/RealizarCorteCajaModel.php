<?php

namespace App\Models\Caja;

use App\Models\Core\Modelo;

class RealizarCorteCajaModel extends Modelo
{
    protected $table = 'realizar_corte_caja';

    const ID = "id";
    const NUMERO_DOCUMENTO = 'numero_documento';
    const DESCRIPCION = 'descripcion';
    const TIPO_PAGO_ID = 'tipo_pago_id';
    const CANTIDAD = 'cantidad';
    const CANTIDAD_SISTEMA = 'cantidad_sistema';
    const FALTANTES = 'faltantes';
    const SOBRANTES = 'sobrantes';
    const FECHA_REGISTRO = 'fecha_registro';
    const AUTORIZADO = 'autorizado';

    protected $fillable = [
        self::NUMERO_DOCUMENTO,
        self::DESCRIPCION,
        self::TIPO_PAGO_ID,
        self::CANTIDAD,
        self::CANTIDAD_SISTEMA,
        self::FALTANTES,
        self::SOBRANTES,
        self::FECHA_REGISTRO,
        self::AUTORIZADO
    ];
}
