<?php

namespace App\Models\Caja;

use App\Models\Core\Modelo;

class DescuentosModel extends Modelo
{
    protected $table = 'descuentos';
    
    const ID = "id";
    const FOLIO_ID = 'folio_id';
    const TOTAL_APLICAR = 'total_aplicar';
    const TIPO_DESCUENTO = 'tipo_descuento';
    const PORCENTAJE = 'porcentaje';
    const CANTIDAD_DESCONTADA = 'cantidad_descontada';
    const TOTAL_DESCUENTO = 'total_descuento';
    const COMENTARIO = 'comentario';
    const FECHA = 'fecha';

    const DESCUENTO_REFACCIONES = 1;
    const DESCUENTO_MANO_OBRA = 2;

    protected $fillable = [
        self::FOLIO_ID,
        self::TOTAL_APLICAR,
        self::TIPO_DESCUENTO,
        self::PORCENTAJE,
        self::CANTIDAD_DESCONTADA,
        self::TOTAL_DESCUENTO,
        self::COMENTARIO,
        self::FECHA
    ];
}
