<?php

namespace App\Models\Caja;

use App\Models\Core\Modelo;

class CorteCajaModel extends Modelo
{
    protected $table = 'corte_caja';
    
    const ID = "id";
    const CAJA_ID = 'caja_id';
    const MOVIMIENTO_CAJA_ID = 'movimiento_caja_id';
    const FECHA_TRANSACCION = 'fecha_transaccion';
    const USUARIO_REGISTRO = 'usuario_registro';


    protected $fillable = [
        self::CAJA_ID,
        self::MOVIMIENTO_CAJA_ID,
        self::FECHA_TRANSACCION,
        self::USUARIO_REGISTRO
    ];
}
