<?php

namespace App\Models\Caja;

use App\Models\Core\Modelo;
use App\Models\Contabilidad\CatalogoProcesosModel;
use App\Models\Refacciones\FoliosModel;

class AnticiposModel extends Modelo
{
    protected $table = 'anticipos';
    
    const ID = "id";
    const CLIENTE_ID = 'cliente_id';
    const TIPO_PROCESO_ID = 'tipo_proceso_id';
    const ESTATUS_ID = 'estatus_id';
    const TOTAL = 'total';
    const FOLIO_ID = 'folio_id';
    const COMENTARIO = 'comentario';
    const FECHA = 'fecha';
    const FOLIO_APLICADO = 'folio_aplicado';

    const REL_PROCESOS = 'procesos';
    const REL_FOLIOS = 'folios';

    protected $fillable = [
        self::CLIENTE_ID,
        self::TIPO_PROCESO_ID,
        self::ESTATUS_ID,
        self::TOTAL,
        self::FOLIO_ID,
        self::COMENTARIO,
        self::FECHA,
        self::FOLIO_APLICADO
    ];

    public function procesos()
    {
        return $this->belongsTo(CatalogoProcesosModel::class,  self::TIPO_PROCESO_ID);

    }
    public function folios()
    {
        return $this->belongsTo(FoliosModel::class,  self::FOLIO_ID);

    }
}
