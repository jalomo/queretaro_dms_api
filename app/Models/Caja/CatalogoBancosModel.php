<?php

namespace App\Models\Caja;

use App\Models\Core\Modelo;

class CatalogoBancosModel extends Modelo
{
    protected $table = 'catalogo_bancos';
    
    const ID = "id";
    const NOMBRE = 'nombre';

    protected $fillable = [
        self::NOMBRE
    ];
}
