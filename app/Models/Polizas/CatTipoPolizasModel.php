<?php

namespace App\Models\Polizas;

use App\Models\Core\Modelo;

class CatTipoPolizasModel extends Modelo
{
    protected $table = 'tipo_poliza';
    const ID = "id";
    const NOMBRE = 'nombre';

    protected $fillable = [
        self::NOMBRE,
    ];
}
