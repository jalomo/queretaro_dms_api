<?php

namespace App\Models\Financiamientos;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class CatUsoSeguroModel extends Modelo
{
    protected $table = 'catalogo_uso_seguro';
    const ID = 'id';
    const USO_SEGURO = 'uso_seguro';

    protected $fillable = [
        self::ID,
        self::USO_SEGURO
    ];
}
