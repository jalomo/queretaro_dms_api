<?php

namespace App\Models\Financiamientos;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class CatUdiModel extends Modelo
{
    protected $table = 'cat_udi';
    const ID = 'id';
    const UDI = 'udi';
    const VALOR = 'valor';

    protected $fillable = [
        self::ID,
        self::UDI,
        self::VALOR
    ];
}
