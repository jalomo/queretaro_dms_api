<?php

namespace App\Models\Telemarketing;

use App\Models\Core\Modelo;

class CitasVentasModel extends Modelo
{
    protected $table = 'citas_venta';
    const ID = 'id';
    const ID_VENTA = 'id_venta';
    const ID_ASESOR = 'id_asesor';
    const ID_STATUS = 'id_status'; //1:sin asignar, 2:llegó, 3:no llegó
    const ID_USUARIO_CREO = 'id_usuario_creo';
    const FECHA_CITA = 'fecha_cita';
    const HORARIO = 'horario';

    protected $fillable = [
        self::ID,
        self::ID_VENTA,
        self::ID_ASESOR,
        self::ID_STATUS,
        self::ID_USUARIO_CREO,
        self::FECHA_CITA,
        self::HORARIO,
    ];
}




