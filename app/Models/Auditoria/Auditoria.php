<?php

namespace App\Models\Auditoria;

use App\Models\Refacciones\ProductosModel;
use App\Models\Usuarios\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Auditoria extends Model
{
    //
    use SoftDeletes;

    protected $fillable = [
        'user_id',
        'inicio',
    ];

    public function productos()
    {
        return $this->belongsToMany(ProductosModel::class, 'auditoria_producto', 'auditoria_id', 'producto_id')
            ->withPivot('id', 'ubicacion' , 'existencia_sistema', 'existencia_fisico')
            ->as('auditoria')
            ->wherePivotNull('deleted_at');
    }

    public function usuario()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
