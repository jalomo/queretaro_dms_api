<?php

namespace App\Models\Auditoria;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AuditoriaProducto extends Model
{
    //
    use SoftDeletes;

    protected $table = 'auditoria_producto';

    protected $fillable = [
        'producto_id',
        'existencia_id',
        'auditoria_id',
        'existencia_sistema',
        'existencia_fisico',
        'ubicacion',
    ];

    public function auditoria()
    {
        return $this->belongsTo(Auditoria::class);
    }
}
