<?php

namespace App\Models\Soporte;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class CatStatusPrioridadesModel extends Modelo
{
    protected $table = 'cat_estatus_prioridades';
    const ID = "id";
    const PRIORIDAD = "prioridad";
    protected $fillable = [
        self::PRIORIDAD
    ];
}
