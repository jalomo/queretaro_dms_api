<?php

namespace App\Models\Soporte;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class HistorialTicketsModel extends Modelo
{
    protected $table = 'historial_tickets';
    const ID = "id";
    const ESTATUS_ANTERIOR_ID = "estatus_anterior_id";
    const ESTATUS_NUEVO_ID = "estatus_nuevo_id";
    const USER_ID = "user_id";
    const USUARIO = "usuario";
    const TICKET_ID = "ticket_id";
    const COMENTARIO = "comentario";
    const EVIDENCIA = "evidencia";
    const INICIO_ESTATUS = "inicio_estatus";
    const FIN_ESTATUS = "fin_estatus";
    protected $fillable = [
        self::ESTATUS_ANTERIOR_ID,
        self::ESTATUS_NUEVO_ID,
        self::USER_ID,
        self::USUARIO,
        self::COMENTARIO,
        self::TICKET_ID,
        self::EVIDENCIA,
        self::INICIO_ESTATUS,
        self::FIN_ESTATUS
    ];
}
