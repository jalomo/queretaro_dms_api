<?php

namespace App\Models\Soporte;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class CatStatusTickets extends Modelo
{
    protected $table = 'cat_estatus_tickets';
    const ID = "id";
    const ESTATUS = "estatus";
    protected $fillable = [
        self::ESTATUS
    ];
}
