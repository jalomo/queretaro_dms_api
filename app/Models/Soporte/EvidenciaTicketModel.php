<?php

namespace App\Models\Soporte;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class EvidenciaTicketModel extends Modelo
{
    protected $table = 'evidencia_tickets';
    const ID = "id";
    const TICKET_ID = "ticket_id";
    const EVIDENCIA = 'evidencia';
    const DIRECTORIO_EVIDENCIA = 'tickets';
    const COMENTARIO = 'comentario';
    const USER_ID = 'user_id';
    protected $fillable = [
        self::TICKET_ID,
        self::EVIDENCIA,
        self::COMENTARIO,
    ];
}
