<?php

namespace App\Models\Soporte;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class TicketsModel extends Modelo
{
    protected $table = 'tickets';
    const ID = "id";
    const ASUNTO = "asunto";
    const COMENTARIO = "comentario";
    const USER_ID = "user_id";
    const PRIORIDAD_ID = "prioridad_id";
    const ESTATUS_ID = "estatus_id";
    const FOLIO = "folio";
    const ROL_ID = "rol_id";
    const EVIDENCIA = 'evidencia';
    const DIRECTORIO_TICKET = 'tickets';
    protected $fillable = [
        self::ASUNTO,
        self::COMENTARIO,
        self::EVIDENCIA,
        self::USER_ID,
        self::PRIORIDAD_ID,
        self::ESTATUS_ID,
        self::FOLIO,
        self::ROL_ID
    ];
}
