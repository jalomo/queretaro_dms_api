<?php

namespace App\Models\Oasis;

use App\Models\Autos\UnidadesNuevas\RemisionModel;
use App\Models\Core\Modelo;

class MantenimientoUnidadesModel extends Modelo
{
    protected $table = 'mantenimiento_unidades';

    const REMISION_ID = "remision_id";
    const TIPO_UNIDAD = "tipo_unidad";
    const TIPO_UNIDAD_ID = "tipo_unidad_id";
    const TECNICO_MANTENIMIENTO_ID = "tecnico_mantenimiento_id";
    const TECNICO_MANTENIMIENTO = "tecnico_mantenimiento";
    const NO_ORDEN_REPARACION = "no_orden_reparacion";
    const FECHA_RECIBO_UNIDAD = "fecha_recibo_unidad";
    const FECHA_MANTENIMIENTO = "fecha_mantenimiento";
    const ESTADO_BATERIA = "estado_bateria";
    const ESTATUS_VOLTAJE_BATERIA = "estatus_voltaje_bateria";
    const VOLTAJE_BATERIA_INICIAL = "voltaje_bateria_inicial";
    const VOLTAJE_BATERIA_FINAL = "voltaje_bateria_final";
    const ESTADO_BATERIA_ELECTRICO = "estado_bateria_electrico";
    const ESTATUS_VOLTAJE_BATERIA_ELECTRICO = "estatus_voltaje_bateria_electrico";
    const VOLTAJE_BATERIA_INICIAL_ELECTRICO = "voltaje_bateria_inicial_electrico";
    const VOLTAJE_BATERIA_FINAL_ELECTRICO = "voltaje_bateria_final_electrico";
    const TEMPERATURA_NORMAL = "temperatura_normal";
    const ESTEREO_APAGADO = "estereo_apagado";
    const AIRE_APAGADO = "aire_apagado";
    const CAMBIOS_TRANSMISION = "cambios_transmision";
    const ROTACION_LLANTAS = "rotacion_llantas";
    const ESTADO_ELEVADORES = "estado_elevadores";
    const FUGAS_FLUIDOS = "fugas_fluidos";
    const PROTECTORES_ASIENTOS = "protectores_asientos";
    const PRESION_LLANTA_DD = "presion_llanta_dd";
    const PRESION_LLANTA_DI = "presion_llanta_di";
    const PRESION_LLANTA_TI = "presion_llanta_ti";
    const PRESION_LLANTA_TD = "presion_llanta_td";
    const CADUCIDAD_WRAP = "caducidad_wrap";
    const LAVADO_FORD = "lavado_ford";
    const ESTADO_PINTURA = "estado_pintura";
    const CERA_FORD = "cera_ford";
    const DESCONTAMINACION_PINTURA = "descontaminacion_pintura";
    const GOMAS_LIMPIAPARABRISAS = "gomas_limpiaparabrisas";
    const NIVEL_GASOLINA = "nivel_gasolina";
    const PARTE_BAJA_VIECULO = "parte_baja_vieculo";
    const NIVEL_ANTICONGELANTE = "nivel_anticongelante";
    const NIVEL_ACEITE_MOTOR = "nivel_aceite_motor";
    const NIVEL_LIQUIDO_FRENOS = "nivel_liquido_frenos";
    const NIVEL_ACEITE_TRANSMICION = "nivel_aceite_transmicion";
    const NIVEL_CHISGUETEROS = "nivel_chisgueteros";
    const NIVEL_ACEITE_DIECCION = "nivel_aceite_dieccion";
    const LUCES_DELANTERAS = "luces_delanteras";
    const DIRECCIONALES = "direccionales";
    const LUCES_TRACERAS = "luces_traceras";
    const LUZ_TOLDO = "luz_toldo";
    const LUZ_EMERGENCIA = "luz_emergencia";
    const LUZ_VANIDAD = "luz_vanidad";
    const LUZ_FRENOS = "luz_frenos";
    const LUZ_REVERSA = "luz_reversa";
    const LUZ_PLACA = "luz_placa";
    const KILOMETRAJE_MADRINA = "kilometraje_madrina";
    const KILOMETRAJE_INTERCAMBIO = "kilometraje_intercambio";
    const KILOMETRAJE_TRASLADO = "kilometraje_traslado";
    const KILOMETRAJE_PRUEBA = "kilometraje_prueba";
    const PAPELERIA_COMPLETA = "papeleria_completa";
    const FECHA_VENTA = "fecha_venta";

    protected $fillable = [
        self::REMISION_ID,
        self::TIPO_UNIDAD,
        self::TIPO_UNIDAD_ID,
        self::TECNICO_MANTENIMIENTO_ID,
        self::TECNICO_MANTENIMIENTO,
        self::NO_ORDEN_REPARACION,
        self::FECHA_RECIBO_UNIDAD,
        self::FECHA_MANTENIMIENTO,
        self::ESTADO_BATERIA,
        self::ESTATUS_VOLTAJE_BATERIA,
        self::VOLTAJE_BATERIA_INICIAL,
        self::VOLTAJE_BATERIA_FINAL,
        self::ESTADO_BATERIA_ELECTRICO,
        self::ESTATUS_VOLTAJE_BATERIA_ELECTRICO,
        self::VOLTAJE_BATERIA_INICIAL_ELECTRICO,
        self::VOLTAJE_BATERIA_FINAL_ELECTRICO,
        self::TEMPERATURA_NORMAL,
        self::ESTEREO_APAGADO,
        self::AIRE_APAGADO,
        self::CAMBIOS_TRANSMISION,
        self::ROTACION_LLANTAS,
        self::ESTADO_ELEVADORES,
        self::FUGAS_FLUIDOS,
        self::PROTECTORES_ASIENTOS,
        self::PRESION_LLANTA_DD,
        self::PRESION_LLANTA_DI,
        self::PRESION_LLANTA_TI,
        self::PRESION_LLANTA_TD,
        self::CADUCIDAD_WRAP,
        self::LAVADO_FORD,
        self::ESTADO_PINTURA,
        self::CERA_FORD,
        self::DESCONTAMINACION_PINTURA,
        self::GOMAS_LIMPIAPARABRISAS,
        self::NIVEL_GASOLINA,
        self::PARTE_BAJA_VIECULO,
        self::NIVEL_ANTICONGELANTE,
        self::NIVEL_ACEITE_MOTOR,
        self::NIVEL_LIQUIDO_FRENOS,
        self::NIVEL_ACEITE_TRANSMICION,
        self::NIVEL_CHISGUETEROS,
        self::NIVEL_ACEITE_DIECCION,
        self::LUCES_DELANTERAS,
        self::DIRECCIONALES,
        self::LUCES_TRACERAS,
        self::LUZ_TOLDO,
        self::LUZ_EMERGENCIA,
        self::LUZ_VANIDAD,
        self::LUZ_FRENOS,
        self::LUZ_REVERSA,
        self::LUZ_PLACA,
        self::KILOMETRAJE_MADRINA,
        self::KILOMETRAJE_INTERCAMBIO,
        self::KILOMETRAJE_TRASLADO,
        self::KILOMETRAJE_PRUEBA,
        self::PAPELERIA_COMPLETA,
        self::FECHA_VENTA,
    ];

    protected $casts = [
        'fecha_mantenimiento'  => 'date:d-m-Y',
        'fecha_recibo_unidad'  => 'date:d-m-Y',
        'fecha_venta'  => 'date:d-m-Y',
    ];

    public function unidad()
    {
        return $this->belongsTo(RemisionModel::class, self::REMISION_ID);
    }
}
