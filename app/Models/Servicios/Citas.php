<?php

namespace App\Models\Servicios;

use Illuminate\Database\Eloquent\Model;

class Citas extends Model
{
    // Conexion de la base de datos Servicios
    protected $connection = 'servicios';
    protected $table = 'citas';

    public function scopeDatosOrden($query, $id_cita)
    {
        $query->where('citas.id_cita', $id_cita)
        ->join('ordenservicio AS o','o.id_cita', 'citas.id_cita')
        ->leftJoin('operadores AS op','citas.asesor', 'op.nombre')
        ->leftJoin('presupuesto_registro AS prep','prep.id_cita', 'citas.id_cita')
        ->leftJoin('cat_opcion_citas AS cat_op','citas.id_opcion_cita', 'cat_op.id')
        ->leftJoin('cat_situacion_intelisis AS cat_it','o.id_situacion_intelisis', 'cat_it.id') 
        ->select([
            'o.vehiculo_kilometraje AS km', 
            'citas.vehiculo_numero_serie', 
            'o.vehiculo_identificacion', 
            'o.hora_recepcion', 
            'o.fecha_recepcion', 
            'o.created_at AS apertura_orden', 
            'o.cierre_orden', 
            'citas.datos_nombres' ,
            'citas.datos_apellido_paterno', 
            'citas.datos_apellido_materno', 
            'o.tipo_cliente', 
            'o.calle',
            'o.nointerior',
            'o.noexterior', 
            'o.colonia', 
            'o.municipio', 
            'o.estado', 
            'o.sigla_estado', 
            'o.cp', 
            'citas.datos_telefono', 
            'o.telefono_movil', 
            'o.otro_telefono', 
            'citas.vehiculo_placas', 
            'cat_op.codigo', 
            'citas.fecha_creacion_all AS fecha_alta_cita', 
            'op.clave AS clave_asesor', 
            'op.idStars AS aidStars', 
            'citas.datos_email', 
            'o.nombre_compania', 
            'o.correo_compania', 
            'citas.fecha_hora AS fecha_cita',
            'o.fecha_entrega', 
            'o.hora_entrega', 
            'citas.fecha_entrega_unidad', 
            'o.numero_cliente as num_Cliente_orden', 
            'citas.numero_cliente as num_Cliente_cita', 
            'o.id_situacion_intelisis', 
            'prep.acepta_cliente', 
            'prep.id AS id_presupuesto', 
            'prep.anticipo', 
            'prep.subtotal', 
            'prep.cancelado', 
            'prep.iva', 
            'prep.total', 
            'prep.termina_cliente', 
            'cat_it.mostrar_texto', 
            'o.updated_at',
        ])
        ->orderBy("o.id", "DESC");
    }
}
