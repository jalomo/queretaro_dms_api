<?php

namespace App\Models\Logistica;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class CatEstatusDaniosBodyshopModel extends Modelo
{
    protected $table = 'cat_estatus_danios_bodyshop';
    const ID = 'id';
    const ESTATUS = 'estatus';

    protected $fillable = [
        self::ESTATUS
    ];
}
