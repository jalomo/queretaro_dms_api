<?php

namespace App\Models\Logistica;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class DaniosBodyshopModel extends Modelo
{
    protected $table = 'danios_bodyshop';
    const ID = 'id';
    const FECHA_ARRIBO = 'fecha_arribo';
    const ID_ESTATUS_ADMVO = 'id_estatus_admvo';
    const ID_ASESOR = 'id_asesor';
    const SERIE = 'serie';
    const ID_COLOR = 'id_color';
    const DANIOS = 'danios';
    const ID_UBICACION_LLAVES = 'id_ubicacion_llaves';
    const DESCRIPCION = 'descripcion';
    const FALTANTES = 'faltantes';
    const ID_ESTATUS = 'id_estatus';
    const FECHA_INGRESO = 'fecha_ingreso';
    const FECHA_ENTREGA = 'fecha_entrega';
    const OBSERVACIONES = 'observaciones';
    const ID_AGENCIA = 'id_agencia';
    const ID_ORIGEN = 'id_origen';
    const HORA_RECEPCION = 'hora_recepcion';
    const CARGO_A = 'cargo_a';
    const COBRO_PROVEEDORES = 'cobro_proveedores';
    const MEDIO_TRANSPORTACION = 'medio_transportacion';
    const ID_AREA_REPARACION = 'id_area_reparacion';

    protected $fillable = [
        self::FECHA_ARRIBO,
        self::ID_ESTATUS_ADMVO,
        self::ID_ASESOR,
        self::SERIE,
        self::ID_COLOR,
        self::DANIOS,
        self::ID_UBICACION_LLAVES,
        self::DESCRIPCION,
        self::FALTANTES,
        self::ID_ESTATUS,
        self::FECHA_INGRESO,
        self::FECHA_ENTREGA,
        self::OBSERVACIONES,
        self::ID_AGENCIA,
        self::ID_ORIGEN,
        self::HORA_RECEPCION,
        self::CARGO_A,
        self::COBRO_PROVEEDORES,
        self::MEDIO_TRANSPORTACION,
        self::ID_AREA_REPARACION,
    ];
}
