<?php

namespace App\Models\Nomina;

use App\Models\Core\ModeloNominas;
use Illuminate\Database\Eloquent\Model;

class CaDepartamentoModel extends ModeloNominas
{
    
     
    const ID = "id_Departamento";
    const Clave = "Clave";
    const Descripcion = "Descripcion";
    const CuentaCOI = "CuentaCOI";
    const DepartamentoCOI = "DepartamentoCOI";

    protected $table = 'ca_Departamentos';
    protected $primaryKey = self::ID;
   
    protected $fillable = [
        self::Clave,
        self::Descripcion,
        self::CuentaCOI,
        self::DepartamentoCOI
    ];

    public static function getClave()
    {
        return 'holaaaa';
        //return  $order = Order::find(\DB::table('orders')->max('id'));

        //table
    }
}
