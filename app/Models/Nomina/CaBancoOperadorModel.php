<?php

namespace App\Models\Nomina;

use App\Models\Core\ModeloNominas;
use Illuminate\Database\Eloquent\Model;

class CaBancoOperadorModel extends ModeloNominas
{
    protected $table = 'ca_BancoOperador';
     
    
    const ID = "id_BancoOperador";
    const Clave = "Clave";
    const Descripcion = "Descripcion";
   
    protected $fillable = [
        self::Clave,
        self::Descripcion
    ];
}