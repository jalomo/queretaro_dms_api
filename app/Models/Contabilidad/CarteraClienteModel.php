<?php

namespace App\Models\Contabilidad;

use App\Models\Core\Modelo;
use App\Models\Refacciones\ClientesModel;
use App\Models\CuentasPorCobrar\PlazoCreditoModel;
class CarteraClienteModel extends Modelo
{
    protected $table = 'cartera_cliente';
    const ID = "id";
    const CLIENTE_ID = "cliente_id";
    const PLAZO_CREDITO_ID = "plazo_credito_id";
    const MONTO_ACTUAL = "monto_actual";
    const FECHA_OPERACION = "fecha_operacion";
    const COMENTARIOS = "comentarios";

    const REL_CLIENTE = 'cliente';
    const REL_PLAZO_CREDITO = 'plazo_credito';


    protected $fillable = [
        self::CLIENTE_ID,
        self::PLAZO_CREDITO_ID,
        self::MONTO_ACTUAL,
        self::FECHA_OPERACION,
        self::COMENTARIOS,
    ];

    public function cliente()
    {
        return $this->belongsTo(ClientesModel::class);
    }

    public function plazo_credito()
    {
        return $this->belongsTo(PlazoCreditoModel::class);
    }
}
