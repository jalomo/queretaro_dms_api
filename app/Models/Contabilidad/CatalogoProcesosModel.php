<?php

namespace App\Models\Contabilidad;

use App\Models\Core\Modelo;

class CatalogoProcesosModel extends Modelo
{
    protected $table = 'catalogo_procesos';
    const ID = "id";
    const DESCRIPCION = "descripcion";
    const CLAVE_POLIZA = "clave_poliza";
    
    
    const PROCESO_VENTA_MOSTRADOR = 1;
    const PROCESO_TRASPASO = 2;
    const PROCESO_COMPRAS = 3;
    const PROCESO_DEVOLUCION_PROVEEDOR = 4;
    const PROCESO_DEVOLUCION_VENTA = 5;
    const PROCESO_VENTA_AUTOS_NUEVOS = 6;
    const PROCESO_VARIOS = 7;
    const PROCESO_SERVICIOS = 8;
    const PROCESO_HOJALATERIA = 9;
    const PROCESO_VENTA_GARANTIAS = 10;
    const PROCESO_ANTICIPO = 11;


    protected $fillable = [
        self::DESCRIPCION,
        self::CLAVE_POLIZA,
    ];
}
