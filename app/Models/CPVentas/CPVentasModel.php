<?php

namespace App\Models\CPVentas;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class CPVentasModel extends Modelo
{
    protected $table = 'cp_ventas';
    const ID = "id";
    const NO_CUENTA = "no_cuenta";
    const VIN = "vin";
    const APELLIDO = "apellido";
    const NOMBRE = "nombre";
    const TIPO_CLIENTE = "tipo_cliente";
    const TELEFONO_OFICINA = "telefono_oficina";
    const TELEFONO_CASA = "telefono_casa";
    const ENGANCHE = "enganche";
    const MESES_CONTRATO = "meses_contrato";
    const FECHA_INICIO_CONTRATO = "fecha_inicio_contrato";
    const FECHA_TERMINO = "fecha_termino";
    const TASA_INTERES = "tasa_interes";
    const MENSUALIDAD = "mensualidad";
    const MONTO_FINANCIADO = "monto_financiado";
    const MARCA = "marca";
    const MODELO = "modelo";
    const LINEA = "linea";
    const TIPO_VEHICULO = "tipo_vehiculo";
    const ANIO = "anio";
    const PLAN_FINANCIAMIENTO = "plan_financiamiento";
    const VENDEDOR_ASIGNADO = "vendedor_asignado";
    const ASIGNADO = "asignado";
    const EMAIL = "email";
    const INTENTOS = "intentos";
    const ESTATUS_ID = "estatus_id";
    protected $fillable = [
        self::ID,
        self::NO_CUENTA,
        self::VIN,
        self::APELLIDO,
        self::NOMBRE,
        self::TIPO_CLIENTE,
        self::TELEFONO_OFICINA,
        self::TELEFONO_CASA,
        self::ENGANCHE,
        self::MESES_CONTRATO,
        self::FECHA_INICIO_CONTRATO,
        self::FECHA_TERMINO,
        self::TASA_INTERES,
        self::MENSUALIDAD,
        self::MONTO_FINANCIADO,
        self::MARCA,
        self::MODELO,
        self::LINEA,
        self::TIPO_VEHICULO,
        self::ANIO,
        self::PLAN_FINANCIAMIENTO,
        self::VENDEDOR_ASIGNADO,
        self::ASIGNADO,
        self::EMAIL,
        self::INTENTOS,
        self::ESTATUS_ID
    ];
}
