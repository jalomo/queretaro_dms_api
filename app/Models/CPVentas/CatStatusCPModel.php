<?php

namespace App\Models\CPVentas;

use App\Models\Core\Modelo;
class CatStatusCPModel extends Modelo
{
    protected $table = 'cp_cat_estatus';
    const ID = "id";
    const ESTATUS = "estatus";
    protected $fillable = [
        self::ID,
        self::ESTATUS
    ];
}
