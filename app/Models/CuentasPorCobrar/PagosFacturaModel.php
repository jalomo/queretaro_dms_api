<?php

namespace App\Models\CuentasPorCobrar;

use App\Models\Core\Modelo;

class PagosFacturaModel extends Modelo
{
    protected $table = 'pagos_factura';
    const ID = "id";
    const CUENTA_POR_COBRAR_ID = "cuenta_por_cobrar_id";
    const TIPO_PAGO_ID = "tipo_pago_id";
    const CFDI_ID = "cfdi_id";
    const FACTURA_ID = 'factura_id';
    const IMPORTE = "importe";
    const FACTURADO = 'facturado';
    const REGIMEN_FISCAL = 'regimen_fiscal';
    const SAT_UUID = 'sat_uuid';
    const SAT_PDF = 'sat_pdf';


    protected $fillable = [
        self::CUENTA_POR_COBRAR_ID,
        self::TIPO_PAGO_ID,
        self::CFDI_ID,
        self::FACTURA_ID,
        self::IMPORTE,
        self::FACTURADO,
        self::REGIMEN_FISCAL,
        self::SAT_UUID,
        self::SAT_PDF
    ];
}
