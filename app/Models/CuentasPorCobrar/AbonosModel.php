<?php

namespace App\Models\CuentasPorCobrar;

use App\Models\Core\Modelo;

class AbonosModel extends Modelo
{
    protected $table = 'abonos_por_cobrar';
    const ID = "id";
    const CUENTA_POR_COBRAR_ID = "cuenta_por_cobrar_id";
    const TIPO_ABONO_ID = "tipo_abono_id";
    const TOTAL_ABONO = "total_abono";
    const FECHA_VENCIMIENTO = 'fecha_vencimiento';
    const TOTAL_PAGO = "total_pago";
    const FECHA_PAGO = 'fecha_pago';
    const TIPO_PAGO_ID = "tipo_pago_id";
    const TIPO_ASIENTO_ID = "tipo_asiento_id";
    const DIAS_MORATORIOS = 'dias_moratorios';
    const ESTATUS_ABONO_ID = 'estatus_abono_id';
    const FECHA_ACTUAL = 'fecha_actual';
    const MONTO_MORATORIO = 'monto_moratorio';
    const CFDI_ID = 'cfdi_id';
    const POLIZA_ID = 'poliza_id';
    const CAJA_ID = 'caja_id';
    const TIPO_PROCESO_ID = 'tipo_proceso_id';
    const BANCO_ID = 'banco_id';
    const BANCO_PAGAR_ID = 'banco_pagar_id';
    const REFERENCIA_TARJETA = 'referencia_tarjeta';
    const REFERENCIA_CHEQUE = 'referencia_cheque';
    const ABONO_ID = 'abono_id';
    const PARCIALIDAD = 'parcialidad';
    const REGIMEN_FISCAL = 'regimen_fiscal';

    const ESTATUS_TRANSFERENCIA_NO_AUTORIZADA = 3;
    const ESTATUS_TRANSFERENCIA_AUTORIZADA = 2;
    const ESTATUS_TRANSFERENCIA_ESPERA_AUTORIZACION = 1;
    const ESTATUS_TRANSFERENCIA_NO_APLICA = 0;
    const TRANSFERENCIA_AUTORIZADA = 'transferencia_autorizada'; 

    protected $fillable = [
        self::CUENTA_POR_COBRAR_ID,
        self::TIPO_ABONO_ID,
        self::TOTAL_ABONO,
        self::TOTAL_PAGO,
        self::TIPO_PAGO_ID,
        self::FECHA_PAGO,
        self::FECHA_VENCIMIENTO,
        self::ESTATUS_ABONO_ID,
        self::MONTO_MORATORIO,
        self::CFDI_ID,
        self::CAJA_ID,
        self::TRANSFERENCIA_AUTORIZADA,
        self::BANCO_PAGAR_ID,
        self::REFERENCIA_TARJETA,
        self::REFERENCIA_CHEQUE,
        self::REGIMEN_FISCAL
    ];
}
