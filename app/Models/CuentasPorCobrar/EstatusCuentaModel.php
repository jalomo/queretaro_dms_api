<?php

namespace App\Models\CuentasPorCobrar;

use App\Models\Core\Modelo;

class EstatusCuentaModel extends Modelo
{
    protected $table = 'estatus_cuenta';
    const ID = "id";
    const NOMBRE = "nombre";

    const ESTATUS_POR_EMPEZAR = 1;
    const ESTATUS_LIQUIDADO = 2;
    const ESTATUS_ATRASADO = 3;
    const ESTATUS_CANCELADO = 4;
    const ESTATUS_PROCESO = 5;
    const ESTATUS_FACTURADO = 6;
    const ESTATUS_TERMINADO = 7;
    const ESTATUS_FACTURA_MULTIPLE = 8;
    const ESTATUS_FACTURA_CANCELADO = 9;
    const ESTATUS_DEVOLUCION = 10;

    protected $fillable = [
        self::NOMBRE
    ];
}
