<?php

namespace App\Models\Notificaciones;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class NotificacionesModel extends Modelo
{
    protected $table = 'notificaciones';


    const ID = "id";
    const FECHA_NOTIFICACION = "fecha_notificacion";
    const ASUNTO = "asunto";
    const TEXTO = "texto";
    const LEIDA = "leida";
    const ELIMINADA = "eliminada";
    const USER_ID = "user_id";


    protected $fillable = [
        self::FECHA_NOTIFICACION,
        self::ASUNTO,
        self::TEXTO,
        self::LEIDA,
        self::ELIMINADA,
        self::USER_ID,
    ];
}
