<?php

namespace App\Models\CuentasPorPagar;

use App\Models\Core\Modelo;

class AbonosPorPagarModel extends Modelo
{
    protected $table = 'abonos_por_pagar';
    const ID = "id";
    const CUENTA_POR_PAGAR_ID = "cuenta_por_pagar_id";
    const TIPO_ABONO_ID = "tipo_abono_id";
    const TOTAL_ABONO = "total_abono";
    const FECHA_VENCIMIENTO = 'fecha_vencimiento';
    const TOTAL_PAGO = "total_pago";
    const FECHA_PAGO = 'fecha_pago';
    const TIPO_PAGO_ID = "tipo_pago_id";
    const DIAS_MORATORIOS = 'dias_moratorios';
    const ESTATUS_ABONO_ID = 'estatus_abono_id';
    const MONTO_MORATORIO = 'monto_moratorio';
    const CFDI_ID = 'cfdi_id';
    const FECHA_ACTUAL = 'fecha_actual';
    const POLIZA_ID = 'poliza_id';
    const CAJA_ID = 'caja_id';

    protected $fillable = [
        self::CUENTA_POR_PAGAR_ID,
        self::TIPO_ABONO_ID,
        self::TOTAL_ABONO,
        self::TOTAL_PAGO,
        self::TIPO_PAGO_ID,
        self::FECHA_PAGO,
        self::FECHA_VENCIMIENTO,
        self::MONTO_MORATORIO,
        self::CFDI_ID,
        self::ESTATUS_ABONO_ID,
        self::POLIZA_ID,
        self::CAJA_ID
    ];
}
