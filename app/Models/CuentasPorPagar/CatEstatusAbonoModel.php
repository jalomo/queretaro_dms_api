<?php

namespace App\Models\CuentasPorPagar;

use App\Models\Core\Modelo;

class CatEstatusAbonoModel extends Modelo
{
    protected $table = 'cat_estatus_abono';
    const ID = "id";
    const NOMBRE = 'nombre';

    const PENDIENTE = 1;
    const MORATORIO = 2;
    const PAGADO = 3;

    protected $fillable = [
        self::NOMBRE
    ];
}
