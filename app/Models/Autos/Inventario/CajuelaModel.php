<?php

namespace App\Models\Autos\Inventario;

use App\Models\Core\Modelo;

class CajuelaModel extends Modelo
{
    protected $table = 'inventario_cajuela';
    const ID = "id";
    const HERRAMIENTA = "herramienta";
    const GATO_LLAVE = "gato_llave";
    const REFLEJANTES = "reflejantes";
    const CABLES = "cables";
    const EXTINTOR = "extintor";
    const LLANTA_REFACCION = "llanta_refaccion";
    const ID_INVENTARIO = "id_inventario";
    
    protected $fillable = [
        self::HERRAMIENTA,
        self::GATO_LLAVE,
        self::REFLEJANTES,
        self::CABLES,
        self::EXTINTOR,
        self::LLANTA_REFACCION,
        self::ID_INVENTARIO
    ];
}
