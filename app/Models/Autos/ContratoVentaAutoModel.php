<?php

namespace App\Models\Autos;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class ContratoVentaAutoModel extends Modelo
{
    protected $table = 'contrato_venta_auto';
    const ID = "id";
    const ID_VENTA_AUTO = "id_venta_auto";
    const FIRMA_GERENTE_CREDITO = "firma_gerente_credito";
    const FIRMA_GERENTE_VENTAS = "firma_gerente_ventas";
    const FIRMA_CLIENTE = "firma_cliente";

    protected $fillable = [
        self::ID_VENTA_AUTO,
        self::FIRMA_GERENTE_CREDITO,
        self::FIRMA_GERENTE_VENTAS,
        self::FIRMA_CLIENTE
    ];
}
