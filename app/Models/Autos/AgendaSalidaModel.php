<?php

namespace App\Models\Autos;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class AgendaSalidaModel extends Modelo
{
    protected $table = 'agenda_salida';
    const ID = "id";
    const ID_VENTA_AUTO = "id_venta_auto";
    const TITULO = "titulo";
    const DATETIME_INICIO = "datetime_inicio";
    
    protected $fillable = [
        self::ID_VENTA_AUTO,
        self::TITULO,   
        self::DATETIME_INICIO
    ];
}
