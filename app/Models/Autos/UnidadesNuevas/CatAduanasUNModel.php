<?php

namespace App\Models\Autos\UnidadesNuevas;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class CatAduanasUNModel extends Modelo
{
    protected $table = 'catalogo_aduanas_un';
    const ID = 'id';
    const ADUANA = 'aduana';

    protected $fillable = [
        self::ID,
        self::ADUANA
    ];
}
