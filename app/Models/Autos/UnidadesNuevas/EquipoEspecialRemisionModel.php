<?php

namespace App\Models\Autos\UnidadesNuevas;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class EquipoEspecialRemisionModel extends Modelo
{
    protected $table = 'equipo_especial_remision';
    const ID = 'id';
    const DESCRIPCION = 'descripcion';
    const IMPORTE = 'importe';
    const REMISION_ID = 'remision_id';

    protected $fillable = [
        self::ID,
        self::DESCRIPCION,
        self::IMPORTE,
        self::REMISION_ID,
    ];
}
