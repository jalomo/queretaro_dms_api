<?php

namespace App\Models\Autos\UnidadesNuevas;
use App\Models\Core\Modelo;
class CatCombustible extends Modelo
{
    protected $table = 'cat_combustible';
    const ID = "id";
    const DESCRIPCION = 'descripcion';

    protected $fillable = [
        self::DESCRIPCION
    ];
}
