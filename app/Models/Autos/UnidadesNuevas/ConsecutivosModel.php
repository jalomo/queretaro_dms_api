<?php

namespace App\Models\Autos\UnidadesNuevas;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class ConsecutivosModel extends Modelo
{
    protected $table = 'consecutivos_unidades_nuevas';
    const ID = "id";
    const ANIO = 'anio';
    const CAT_CONSECUTIVO = 'cat_consecutivo';
    const TIPO = 'tipo';
    const CONSECUTIVO = 'consecutivo';

    protected $fillable = [
        self::ANIO,
        self::CAT_CONSECUTIVO,
        self::TIPO,
        self::CONSECUTIVO
    ];
}
