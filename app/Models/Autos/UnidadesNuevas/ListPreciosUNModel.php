<?php

namespace App\Models\Autos\UnidadesNuevas;

use App\Models\Core\Modelo;
class ListPreciosUNModel extends Modelo
{
    protected $table = 'lista_precios_un';
    const ID = "id";
    const CAT = 'cat';
    const CLAVE_VEHICULAR = 'clave_vehicular';
    const DESCRIPCION = 'descripcion';
    protected $fillable = [
        self::CAT,
        self::DESCRIPCION,
        self::CLAVE_VEHICULAR
    ];
}
