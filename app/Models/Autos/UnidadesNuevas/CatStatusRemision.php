<?php

namespace App\Models\Autos\UnidadesNuevas;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class CatStatusRemision extends Modelo
{
    protected $table = 'cat_estatus_remision';
    const ID = "id";
    const ESTATUS = 'estatus';

    protected $fillable = [
        self::ESTATUS
    ];
}
