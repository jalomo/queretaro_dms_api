<?php

namespace App\Models\Autos\UnidadesNuevas;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class MemoCompraModel extends Modelo
{
    protected $table = 'memo_compra';
    const ID = 'id';
    const REMISION_ID = 'remision_id';
    const CM = 'cm';
    const MES = 'mes';
    const NO_PRODUCTO = 'no_producto';
    const MEMO = 'memo';
    const COSTO_REMISION = 'costo_remision';
    const COMPRAR = 'comprar';
    const MES_COMPRA = 'mes_compra';
    const FP = 'fp';
    const DIAS = 'dias';

    protected $fillable = [
        self::REMISION_ID,
        self::CM,
        self::MES,
        self::NO_PRODUCTO,
        self::MEMO,
        self::COSTO_REMISION,
        self::COMPRAR,
        self::MES_COMPRA,
        self::FP,
        self::DIAS,
    ];
}
