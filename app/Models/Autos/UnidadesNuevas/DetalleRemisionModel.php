<?php

namespace App\Models\Autos\UnidadesNuevas;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class DetalleRemisionModel extends Modelo
{
    protected $table = 'detalle_remision';
    const ID = "id";
    const REMISIONID = "remision_id";
    const PUERTAS = "puertas";
    const CILINDROS = "cilindros";
    const TRANSMISION = "transmision";
    const COMBUSTIBLE_ID = "combustible_id";
    const CAPACIDAD = "capacidad";
    const CAPACIDAD_KG = "capacidad_kg";
    const COLORINTID = "color_int_id";
    const COLOREXTID = "color_ext_id";

    protected $fillable = [
        self::REMISIONID,
        self::PUERTAS,
        self::CILINDROS,
        self::TRANSMISION,
        self::COMBUSTIBLE_ID,
        self::CAPACIDAD,
        self::CAPACIDAD_KG,
        self::COLORINTID,
        self::COLOREXTID,
    ];
}
