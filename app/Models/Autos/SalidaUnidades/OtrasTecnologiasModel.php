<?php

namespace App\Models\Autos\SalidaUnidades;

use App\Models\Core\Modelo;

class OtrasTecnologiasModel extends Modelo
{
    protected $table = 'otras_tecnologias';
    const ID = "id";
    const EASY_FUEL = "easy_fuel";
    const TRACK_APPS = "track_apps";
    const AUTO_START_STOP = "auto_start_stop";
    const SISTEMA_CTR_TERRENO = "sistema_ctr_terreno";
    const ID_VENTA_AUTO = 'id_venta_auto';

    protected $fillable = [
        self::EASY_FUEL,
        self::TRACK_APPS,
        self::AUTO_START_STOP,
        self::SISTEMA_CTR_TERRENO,
        self::ID_VENTA_AUTO
    ];
}
