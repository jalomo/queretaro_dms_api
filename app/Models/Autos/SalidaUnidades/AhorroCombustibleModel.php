<?php

namespace App\Models\Autos\SalidaUnidades;

use App\Models\Core\Modelo;

class AhorroCombustibleModel extends Modelo
{
    protected $table = 'ahorro_combustible';
    const ID = "id";
    const IVCT = "ivct";
    const TIVCT = "tivct";
    const ID_VENTA_AUTO = 'id_venta_auto';

    protected $fillable = [
        self::IVCT,
        self::TIVCT,
        self::ID_VENTA_AUTO
    ];

}
