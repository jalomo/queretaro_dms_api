<?php

namespace App\Models\Autos\SalidaUnidades;

use App\Models\Core\Modelo;

class DesempenoModel extends Modelo
{
    protected $table = 'desempeno';
    const ID = "id";

    const MOTORES = "motores";
    const CLUTCH = "clutch";
    const CAJA_CAMBIOS = "caja_cambios";
    const ARBOL_TRANSMISIONES = "arbol_transmisiones";
    const DIFERENCIAL = "diferencial";
    const DIFERENCIAL_TRACCION = "diferencial_traccion";
    const CAJA_REDUCTORA = "caja_reductora";
    const CAJA_TRANSFERENCIA = "caja_transferencia";
    const SISTEMA_DINAMICO = "sistema_dinamico";
    const DIRECCION = "direccion";
    const SUSPENSION = "suspension";
    const TOMA_PODER = "toma_poder";

    const ID_VENTA_AUTO = 'id_venta_auto';

    protected $fillable = [
        self::MOTORES,
        self::CLUTCH,
        self::CAJA_CAMBIOS,
        self::ARBOL_TRANSMISIONES,
        self::DIFERENCIAL,
        self::DIFERENCIAL_TRACCION,
        self::CAJA_REDUCTORA,
        self::CAJA_TRANSFERENCIA,
        self::SISTEMA_DINAMICO,
        self::DIRECCION,
        self::SUSPENSION,
        self::TOMA_PODER,
        self::ID_VENTA_AUTO
    ];
}
