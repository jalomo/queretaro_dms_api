<?php

namespace App\Models\Autos\SalidaUnidades;

use App\Models\Core\Modelo;

class IluminacionModel extends Modelo
{
    protected $table = 'iluminacion';
    const ID = "id";
    const ILUMINACION_AMBIENTAL = "iluminacion_ambiental";
    const SISTEMA_ILUMINACION = "sistema_iluminacion";
    const FAROS_BIXENON = "faros_bixenon";
    const HID = "hid";
    const LED = "led";
    const ID_VENTA_AUTO = "id_venta_auto";

    protected $fillable = [
        self::ILUMINACION_AMBIENTAL,
        self::SISTEMA_ILUMINACION,
        self::FAROS_BIXENON,
        self::HID,
        self::LED,
        self::ID_VENTA_AUTO
    ];
}
