<?php

namespace App\Models\Autos;

use App\Models\Autos\SalidaUnidades\AhorroCombustibleModel;
use App\Models\Autos\SalidaUnidades\ComentariosModel;
use App\Models\Autos\SalidaUnidades\ConfortModel;
use App\Models\Autos\SalidaUnidades\DesempenoModel;
use App\Models\Autos\SalidaUnidades\DocumentosModel;
use App\Models\Autos\SalidaUnidades\EspecialesModel;
use App\Models\Autos\SalidaUnidades\ExtrasModel;
use App\Models\Autos\SalidaUnidades\FuncionamientoVehiculoModel;
use App\Models\Autos\SalidaUnidades\IluminacionModel;
use App\Models\Autos\SalidaUnidades\OtrasTecnologiasModel;
use App\Models\Autos\SalidaUnidades\SeguridadModel;
use App\Models\Autos\SalidaUnidades\SeguridadPasivaModel;
use App\Models\Autos\SalidaUnidades\VehiculoHibridoModel;
use App\Models\Autos\UnidadesNuevas\RemisionModel;
use App\Models\Core\Modelo;
use App\Models\Refacciones\FoliosModel;
use App\Models\Refacciones\ClientesModel;
use App\Models\Usuarios\User;

class VentasAutosModel extends Modelo
{
    protected $table = 'ventas_autos';
    const ID = "id";
    const ID_UNIDAD = "id_unidad";
    const ID_FOLIO = "id_folio";
    const ID_CLIENTE = "id_cliente";
    const ID_ESTATUS = 'id_estatus'; // estatus de venta
    const ID_TIPO_AUTO = 'id_tipo_auto'; //nuevo - semi nuevo
    const MONEDA = "moneda";
    const NO_ECONOMICO = 'no_economico';
    const ID_PLAN_CREDITO = "id_plan_credito";
    const SERVICIO_CITA_ID = "servicio_cita_id";

    const FECHA_REGRESO = "fecha_regreso";
    const NO_SERIE = "no_serie";
    const ID_ASESOR = "id_asesor";
    const FIRMA_CLIENTE = "firma_cliene";
    const FIRMA_ASESOR = "firma_asesor";
    const FIRMA_CREDITO = 'firma_credito';
    const FIRMA_CONTADORA = 'firma_contadora';
    
    const CREDITO_APROVADO = "credito_aprovado";
    // const VENTA_CONTADO = 1;
    // const VENTA_CREDITO = 2;
    const TIPO_VENTA = "tipo_venta";

    const FECHA_INICIO = "fecha_fin";
    const FECHA_FIN = "fecha_inicio";
    const IMPUESTO_ISAN = 'impuesto_isan';
    const IVA = 'iva';
    const DESCUENTO = 'descuento';

    const ID_VENTA_AUTO = 'id_venta_auto';
    const DESCRIPCION_MODELO = 'descripcion_modelo';
    const SOLICITA_FACTURACION = 'solicita_facturacion';


    protected $fillable = [
        self::ID_UNIDAD,
        self::ID_FOLIO,
        self::ID_CLIENTE,
        self::ID_ESTATUS,
        self::ID_TIPO_AUTO,
        self::MONEDA,
        self::SERVICIO_CITA_ID,
        self::NO_SERIE,
        self::ID_ASESOR,
        self::FIRMA_CLIENTE,
        self::FIRMA_ASESOR,
        self::FIRMA_CREDITO,
        self::FIRMA_CONTADORA,
        self::IMPUESTO_ISAN,
        self::IVA,
        self::NO_ECONOMICO,
        self::DESCUENTO,
        self::CREDITO_APROVADO,
        self::TIPO_VENTA,
        self::DESCRIPCION_MODELO, // una venta de unidad sin remision 
        self::SOLICITA_FACTURACION
    ];



    public function folio()
    {
        return $this->hasOne(FoliosModel::class, FoliosModel::ID, self::ID_FOLIO);
    }

    public function unidad()
    {
        return $this->hasOne(RemisionModel::class, RemisionModel::ID, self::ID_UNIDAD);
    }

    public function asesor()
    {
        return $this->hasOne(User::class, User::ID, self::ID_ASESOR);
    }

    public function cliente()
    {
        return $this->hasOne(ClientesModel::class, ClientesModel::ID, self::ID_CLIENTE);
    }

    public function seguridad()
    {
        return $this->hasOne(SeguridadModel::class, SeguridadModel::ID, self::ID_UNIDAD);
    }

    public function ahorro_combustible()
    {
        return $this->hasOne(AhorroCombustibleModel::class, AhorroCombustibleModel::ID_VENTA_AUTO, self::ID);
    }

    public function comentarios()
    {
        return $this->hasOne(ComentariosModel::class, ComentariosModel::ID_VENTA_AUTO, self::ID);
    }

    public function confort()
    {
        return $this->hasOne(ConfortModel::class, ConfortModel::ID_VENTA_AUTO, self::ID);
    }

    public function desempeno()
    {
        return $this->hasOne(DesempenoModel::class, DesempenoModel::ID_VENTA_AUTO, self::ID);
    }

    public function documentos()
    {
        return $this->hasOne(DocumentosModel::class, DocumentosModel::ID_VENTA_AUTO, self::ID);
    }

    public function especiales()
    {
        return $this->hasOne(EspecialesModel::class, EspecialesModel::ID_VENTA_AUTO, self::ID);
    }

    public function extras()
    {
        return $this->hasOne(ExtrasModel::class, ExtrasModel::ID_VENTA_AUTO, self::ID);
    }

    public function funcionamiento_vehiculo()
    {
        return $this->hasOne(FuncionamientoVehiculoModel::class, FuncionamientoVehiculoModel::ID_VENTA_AUTO, self::ID);
    }

    public function iluminacion()
    {
        return $this->hasOne(IluminacionModel::class, IluminacionModel::ID_VENTA_AUTO, self::ID);
    }

    public function otras_tecnologias()
    {
        return $this->hasOne(OtrasTecnologiasModel::class, OtrasTecnologiasModel::ID_VENTA_AUTO, self::ID);
    }

    public function seguridad_pasiva()
    {
        return $this->hasOne(SeguridadPasivaModel::class, SeguridadPasivaModel::ID_VENTA_AUTO, self::ID);
    }

    public function vehiculo_hibrido()
    {
        return $this->hasOne(VehiculoHibridoModel::class, VehiculoHibridoModel::ID_VENTA_AUTO, self::ID);
    }
}
