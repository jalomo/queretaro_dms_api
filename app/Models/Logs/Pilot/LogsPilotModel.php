<?php

namespace App\Models\Logs\Pilot;

use App\Models\Core\Modelo;

class LogsPilotModel extends Modelo
{
    protected $table = 'logs_pilot';
    const ID = "id";
    const TYPE = "type";
    const POSTDATA = "postdata";
    const RESPONSEDATA = "responsedata";
    const STATUS = "status";

    const TYPE_LOGIN = 1;
    const TYPE_CREATE_UNIDAD = 2;
    const TYPE_UPDATE_UNIDAD = 3;
    const TYPE_CREATE_UNIDAD_USADO = 4;
    const TYPE_UPDATE_UNIDAD_USADO = 5;
    const TYPE_CAMBIAR_DISPONIBILIDAD_NUEVO = 6;
    const TYPE_CAMBIAR_DISPONIBILIDAD_USADO = 7;
    protected $fillable = [
        self::TYPE,
        self::POSTDATA,
        self::RESPONSEDATA,
        self::STATUS,
    ];
}
