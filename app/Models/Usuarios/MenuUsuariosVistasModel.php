<?php

namespace App\Models\Usuarios;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class MenuUsuariosVistasModel extends Modelo
{
    protected $table = 'menu_usuario_vista';
    const ID = "id";
    const USUARIO_ID = "usuario_id";
    const SECCION_ID = "seccion_id";
    const SUBMENU_ID = "submenu_id";
    const VISTA_ID = "vista_id";
    
    protected $fillable = [
        self::ID,
        self::USUARIO_ID,
        self::SECCION_ID,
        self::SUBMENU_ID,
        self::VISTA_ID
    ];
}
