<?php

namespace App\Models\Usuarios;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class MenuSubmenuModel extends Modelo
{
    protected $table = 'menu_submenu';
    const ID = "id";
    const NOMBRE = "nombre_submenu";
    const SECCION_ID = "seccion_id";
    const VISIBLE = 'visible';
    
    protected $fillable = [
        self::ID,
        self::NOMBRE,
        self::SECCION_ID,
        self::VISIBLE
    ];

    // public function setNombreAttribute($value)
    // {
    //     $this->attributes[self::NOMBRE] = mb_strtoupper($value,'UTF-8');
    // }
}
