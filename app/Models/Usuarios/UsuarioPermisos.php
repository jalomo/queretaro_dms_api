<?php

namespace App\Models\Usuarios;

use App\Models\Core\Modelo;

class UsuarioPermisos extends Modelo
{
    protected $table = 'usuario_permisos';
    const ID = "id";
    const PERMISO_ID = 'permiso_id';
    const USUARIO_ID = 'usuario_id';

    protected $fillable = [
        self::PERMISO_ID,
        self::USUARIO_ID
    ];
}
