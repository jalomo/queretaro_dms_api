<?php

namespace App\Models\Usuarios;

use App\Models\Core\Modelo;

class UsuariosModulosModel extends Modelo
{
    protected $table = 'det_usuarios_modulos';
    const ID = "id";
    const USUARIO_ID = "usuario_id";
    const MODULO_ID = "modulo_id";
    
    protected $fillable = [
        self::USUARIO_ID,
        self::MODULO_ID
    ];
}
