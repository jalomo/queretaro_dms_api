<?php

namespace App\Models\Usuarios;

use App\Models\Core\Modelo;

class RolModel extends Modelo
{
    protected $table = 'roles';
    const ID = "id";
    const ROL = "rol";
    
    const ROL_ADMIN = 1;
    const ROL_REFACCIONES = 2;
    const ROL_CONTABILIDAD = 3;
    const ROL_CAJA = 4;
    protected $fillable = [
        self::ROL
    ];
}
