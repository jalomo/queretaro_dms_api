<?php

namespace App\Log;

use Monolog\Handler\AbstractProcessingHandler;
use App\Log\Models\Logger;

class DatabaseHandler extends AbstractProcessingHandler
{
    protected function write(array $record): void
    {
        Logger::create([
            'level' => $record['level'],
            'level_name' => $record['level_name'],
            'message' => $record['message'],
            'created_at' => $record['datetime'],
            'context' => json_encode($record['context']),
            'extra' => json_encode($record['extra']),
        ]);
    }
}