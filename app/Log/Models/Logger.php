<?php

namespace App\Log\Models;

use Illuminate\Database\Eloquent\Model;

class Logger extends Model
{
    protected $table = 'logger';
    
    protected $fillable = [
        'level',
        'level_name',
        'message',
        'created_at',
        'context',
        'extra',
    ];
}