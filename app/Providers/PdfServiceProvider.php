<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Pdf\PdfInterface;
use App\Pdf\Pdf;

class PdfServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind(PdfInterface::class, Pdf::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
