<?php

namespace App\Providers;

use App\Models\Auditoria\Auditoria;
use App\Models\Auditoria\AuditoriaProducto;
use App\Policies\AuditoriaPolicy;
use App\Policies\AuditoriaProductoPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;
use Carbon\Carbon;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
        AuditoriaProducto::class => AuditoriaProductoPolicy::class,
        Auditoria::class => AuditoriaPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        // date_default_timezone_set('America/Mexico_City');
        $this->registerPolicies();
        Passport::routes();
        Passport::personalAccessTokensExpireIn(Carbon::now()->addHours(24));
    }
}
