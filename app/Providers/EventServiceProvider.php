<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
use App\Events\DeleteRefaccionDetalleEvent;
use App\Events\Refacciones\DecrementStockEvent;
use App\Events\Refacciones\IncrementStockEvent;
use App\Events\StoreRefaccionDetalleEvent;
use App\Events\UpdateRefaccionDetalleEvent;
use App\Listeners\Refacciones\DecrementStockListener;
use App\Listeners\Refacciones\DescuentoIncrementoInventarioDetalles;
use App\Listeners\Refacciones\DescuentoInventarioDetalles;
use App\Listeners\Refacciones\DevolucionInventarioDetalles;
use App\Listeners\Refacciones\IncrementStockListener;
use App\Listeners\DevolucionUpdateContabilidad;
use App\Listeners\Refacciones\DevolverRefaccionesContabilidadSinEntregar;
use App\Listeners\Refacciones\EnviarFileRefacciones;
use App\Listeners\Refacciones\EnviarPolizaContabilidadRefaccion;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        DecrementStockEvent::class => [
            DecrementStockListener::class,
        ],
        IncrementStockEvent::class => [
            IncrementStockListener::class,
        ],
        DeleteRefaccionDetalleEvent::class => [
            // DevolucionRefaccionListener::class,
            // DevolucionRefaccionPreventivoListener::class,
            DevolverRefaccionesContabilidadSinEntregar::class,
            DevolucionInventarioDetalles::class,
            // DeletePreventivoListener::class,
        ],
        UpdateRefaccionDetalleEvent::class => [
            DescuentoIncrementoInventarioDetalles::class,
            DevolucionUpdateContabilidad::class,
            // UpdatePreventivoListener::class,
        ],
        StoreRefaccionDetalleEvent::class => [
            DescuentoInventarioDetalles::class,
            EnviarPolizaContabilidadRefaccion::class,
            // EnviarFileRefacciones::class,
            // StorePreventivoListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
        //
    }
}
