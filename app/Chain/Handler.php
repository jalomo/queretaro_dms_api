<?php 

namespace App\Chain;

use Illuminate\Http\JsonResponse as Response;

interface Handler
{
    public function nextHandler(Handler $handler): Handler;

    public function handle($request) : ?Response;

    public function execute($request) : ?Response;
}