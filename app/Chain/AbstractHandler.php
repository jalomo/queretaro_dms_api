<?php

namespace App\Chain;

use Illuminate\Http\JsonResponse as Response;

abstract class AbstractHandler implements Handler
{
    private $nextHandler;

    public function nextHandler(Handler $handler) : Handler
    {
        $this->nextHandler = $handler;   
        
        return $handler;
    }

    public function handle($request): ?Response
    {
        $handlerResult = $this->execute($request);

        if( $handlerResult instanceof Response)
        {
            return $handlerResult;
        }

        if($this->nextHandler)
        {
            return $this->nextHandler->handle($request);
        }

        return null;
    }

    public abstract function execute($request): ?Response;
}