<?php

namespace App\Http\Controllers\Pedidos;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Core\Controller;
use App\Http\Controllers\Pedidos\Archivos\Csv;
use App\Http\Controllers\Pedidos\Archivos\Xml40;
use App\Http\Controllers\Pedidos\Asientos\Devolucion\Devolucion;
// Asientos para la carga a contabilidad de proveedores ford y externos
use App\Http\Controllers\Pedidos\Asientos\Ford\IVA;
use App\Http\Controllers\Pedidos\Asientos\Ford\TotalSinIVA;
use App\Http\Controllers\Pedidos\Asientos\Ford\Total;
use App\Http\Controllers\Pedidos\Asientos\Externo\IVA as IVAExterno;
use App\Http\Controllers\Pedidos\Asientos\Externo\TotalSinIVA as TotalSinIVAExterno;
use App\Http\Controllers\Pedidos\Asientos\Externo\Total as TotalExterno;

use App\Http\Requests\Pedido\StoreCargamentoRequest;
use App\Http\Requests\Pedidos\UploadCSVRequest;
use App\Http\Requests\Pedidos\UploadFacturaRequest;
use App\Http\Requests\Pedidos\UploadXMLRequest;
use App\Models\Refacciones\Pedido;
use App\Http\Resources\DataTablesResource;
use App\Http\Resources\Pedidos\FacturaByFolio;
use App\Http\Resources\Pedidos\ProductosCollection;
use App\Http\Resources\Pedidos\ProductosContabilidadResource;
use App\Http\Resources\Pedidos\VentaProductosCollection;
use App\Models\Refacciones\PedidosProductos;
use App\Models\Usuarios\User;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\VentaProductoModel;
use App\Models\Refacciones\VentasRealizadasModel;
use App\Servicios\Inventario\PedirService;
use Symfony\Component\HttpFoundation\StreamedResponse;
use App\Servicios\Producto\CrearProductoService;
use App\Servicios\Inventario\DevolverPedidoService;
use App\Pdf\Pdf;
use App\Servicios\Producto\CostoPromedio;
use Exception;
use Illuminate\Support\Facades\Log;
use stdClass;
use Throwable;

class PedidosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $request->merge(['page' => floor($request->get('start') / ($request->get('length') ?: 1)) + 1]);

        $pedidos = Pedido::with('usuario')->latest()->paginate($request->length);

        return response()->json(new DataTablesResource($pedidos));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try{
            DB::beginTransaction();

            $pedido = Pedido::create($request->only('fecha_registro', 'venta_id'));
    
            foreach($request->productos as $producto)
            {
                $pedido->productos()->attach($pedido->id, [
                    'producto_id' => $producto['id'],
                    'cantidad' => $producto['cantidad'],
                ]);
            }

            DB::commit();
        } catch (Throwable $e) {
            DB::rollBack();

            logger()->warning('Error al guardar el pedido', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'code' => $e->getCode(),
            ]);

            return response()->json([
                'id' => null,
                'message' => 'Ocurrio un error al guardar el pedido.',
                'errors' => $e->getMessage(),
            ], 400);
        }

        return response()->json([
            'id' => $pedido->id,
            'message' => 'El pedido ha sido guardado correctamente',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function cargamento(StoreCargamentoRequest $request)
    {
        //
        try{
            DB::beginTransaction();

            $es_ford = $request->ford == 'FM'? true : false;

            $pedido = Pedido::create(['fecha_registro' => now(), 'venta_id' => null, 'entrada' => true, 'ford' => $es_ford, 'tipo' => 'manual' ]);
    
            $total = 0;

            foreach($request->productos as $producto)
            {
                $valor_unitario = $es_ford == true? $producto['valor_unitario'] / 1.16 : $producto['valor_unitario'];

                // Buscamos el producto si no existe se crea
                $prod = CrearProductoService::create([
                    'no_identificacion' => $producto['grupo']. $producto['prefijo'] . $producto['basico'] . $producto['sufijo'],
                    'descripcion' => $producto['descripcion'],
                    'valor_unitario' => $valor_unitario,
                    'grupo' => $producto['grupo'],
                    'prefijo' => $producto['prefijo'],
                    'basico' => $producto['basico'],
                    'sufijo' => $producto['sufijo'],
                ]);

                // Creamos registro de la entrada del producto en pedidos
                $pedido->productos()->attach($pedido->id, [
                    'producto_id' => $prod->id,
                    'cantidad_entregada' => $producto['cantidad'],
                    'precio' => $valor_unitario,
                    'total' => $valor_unitario * $producto['cantidad'],
                    'factura' => $producto['factura'],
                    'proveedor' => $producto['proveedor'],
                    'clave_sat' => $producto['clave_sat'],
                    'unidad_sat' => $producto['unidad_sat'],
                ]);

                $total = $producto['valor_unitario'] * $producto['cantidad'];

                // El calculo del Costo Promedio debe ir antes que el registro de la entrada
                // en el inventario si no se calculará mal
                $costoPromedio = new CostoPromedio($prod, $producto['cantidad'], $valor_unitario);
                $costoPromedio->entrada();

                // Registramos la entrada del producto en el inventario
				$inventario = new PedirService;
                $inventario->id($pedido->id);
                $inventario->handle($prod->id, $producto['cantidad'], $producto['valor_unitario']);

                if($es_ford == true)
                {
                    // Mandamos el total sin IVA a contabilidad
                    $total_sin_iva = new TotalSinIVA($pedido, [
                        'cliente_nombre' => 'FORD MOTORS',
                        'num_pieza' => $prod->no_identificacion,
                        'cantidad' => $producto['cantidad'],
                        'costo_promedio' => $producto['valor_unitario'],
                    ]);
                    $total_sin_iva->make($total / 1.16);
        
                    // Mandamos el Total con IVA a contabilidad
                    $total_con_iva = new Total($pedido, [
                        'cliente_nombre' => 'FORD MOTORS',
                        'num_pieza' => $prod->no_identificacion,
                        'cantidad' => $producto['cantidad'],
                        'costo_promedio' => $producto['valor_unitario'],
                    ]);
                    $total_con_iva->make($total);
        
                    // Mandamos el IVA a contabilidad
                    $iva = new IVA($pedido, [
                        'cliente_nombre' => 'FORD MOTORS',
                        'num_pieza' => $prod->no_identificacion,
                        'cantidad' => $producto['cantidad'],
                        'costo_promedio' => $producto['valor_unitario'],
                    ]);
                    $iva->make($total - ($total / 1.16));
    
                    Log::info('PROVEEDOR FORD');
                }
    
                if($es_ford == false)
                {
                    // Mandamos el total sin IVA a contabilidad
                    $total_sin_iva = new TotalSinIVAExterno($pedido, [
                        'cliente_nombre' => 'FORD MOTORS',
                        'num_pieza' => $prod->no_identificacion,
                        'cantidad' => $producto['cantidad'],
                        'costo_promedio' => $producto['valor_unitario'],
                    ]);
                    $total_sin_iva->make($total);
        
                    // Mandamos el Total con IVA a contabilidad
                    $total_con_iva = new TotalExterno($pedido, [
                        'cliente_nombre' => 'FORD MOTORS',
                        'num_pieza' => $prod->no_identificacion,
                        'cantidad' => $producto['cantidad'],
                        'costo_promedio' => $producto['valor_unitario'],
                    ]);
                    $total_con_iva->make($total * 1.16);
        
                    // Mandamos el IVA a contabilidad
                    $iva = new IVAExterno($pedido, [
                        'cliente_nombre' => 'FORD MOTORS',
                        'num_pieza' => $prod->no_identificacion,
                        'cantidad' => $producto['cantidad'],
                        'costo_promedio' => $producto['valor_unitario'],
                    ]);
                    $iva->make($total * 0.16);
    
                    Log::info('PROVEEDOR EXTERNO');
                }
            }

            DB::commit();

            return response()->json([
                'id' => $pedido->id,
                'message' => 'El pedido ha sido guardado correctamente',
            ]);

        } catch (Throwable $e) {
            DB::rollBack();

            Log::warning($e->getMessage());

            return response()->json([
                'id' => null,
                'message' => 'Ocurrio un error al guardar el pedido.',
                'errors' => $e->getMessage(),
            ], 400);
        }
    }

    /**
     * Retorna los productos del pedido
     *
     * @param App\Models\Refacciones\Pedido $pedido
     * @return \Illuminate\Http\Response
     */
    public function productos(Pedido $pedido)
    {
        $productos = Pedido::
            join('pedidos_productos', 'pedidos_productos.pedido_id', 'pedidos.id')
            ->join('producto', 'pedidos_productos.producto_id', 'producto.id')
            ->leftJoin('venta_producto', 'pedidos_productos.model_id', 'venta_producto.id')
            ->leftJoin('ventas', 'venta_producto.venta_id', 'ventas.id')
            ->leftJoin('catalogo_ubicaciones_producto', 'producto.ubicacion_producto_id', 'catalogo_ubicaciones_producto.id')
            ->leftJoin('folios', 'ventas.folio_id', 'folios.id')
            ->select([
                'pedidos_productos.id',
                'pedidos_productos.cantidad',
                'pedidos_productos.cantidad_entregada',
                'producto.no_identificacion',
                'producto.descripcion',
                'pedidos_productos.created_at',
                'catalogo_ubicaciones_producto.nombre as ubicacion',
                'ventas.id as venta_id',
                'ventas.numero_orden',
                'folios.folio',
            ])
            ->where('pedidos.id', $pedido->id)
            ->whereNull('pedidos_productos.deleted_at')
            ->paginate(1000);

        // $pedido->productos()->paginate(1000);

        return response()->json(new ProductosCollection($productos));
    }

    /**
     * Retorna los productos del cargamento
     *
     * @param App\Models\Refacciones\Pedido $pedido
     * @return \Illuminate\Http\Response
     */
    public function productos_cargamento(Pedido $pedido)
    {
        $productos = Pedido::
            join('pedidos_productos', 'pedidos_productos.pedido_id', 'pedidos.id')
            ->join('producto', 'pedidos_productos.producto_id', 'producto.id')
            ->leftJoin('catalogo_ubicaciones_producto', 'producto.ubicacion_producto_id', 'catalogo_ubicaciones_producto.id')
            ->select([
                'pedidos_productos.id',
                'pedidos_productos.cantidad',
                'pedidos_productos.cantidad_entregada',
                'pedidos_productos.producto_id',
                'producto.no_identificacion',
                'producto.descripcion',
                'pedidos_productos.created_at',
                'pedidos_productos.proveedor',
                'pedidos_productos.total',
                'catalogo_ubicaciones_producto.nombre as ubicacion',
                'pedidos.factura',
                'pedidos_productos.deleted_at',
            ])
            ->where('pedidos.id', $pedido->id)
            ->paginate(1000);

        return response()->json(new ProductosCollection($productos));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Models\Refacciones\Pedido $pedido
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pedido $pedido)
    {
        try{
            DB::beginTransaction();

            $pedido->fecha_registro = $request->fecha_registro;

            $piezas = $pedido->piezas()->get();
            $requestProductos = collect($request->productos);
            $piezasIds = [];

            foreach($piezas as $pieza)
            {
                $p = $requestProductos->firstWhere('id', $pieza->id);

                if($p == null){
                    continue;
                }
                
                $piezasIds[] = $pieza->id;
                $pieza->cantidad = $p['cantidad'];
                $pieza->save();
            }

            //Borramos las piezas que no se pasan
            PedidosProductos::where('pedido_id', $pedido->id)->whereNotIn('id', $piezasIds)->delete();

            // Agregamos productos que esten con id null
            foreach($requestProductos as $producto)
            {
                if($producto['id'] == null)
                {
                    PedidosProductos::create([
                        'cantidad' => $producto['cantidad'],
                        'producto_id' => $producto['producto_id'],
                        'pedido_id' => $pedido['id'],
                    ]);
                }
            }
            
            DB::commit();

            return response()->json([
                'id' => $pedido->id,
                'message' => 'El pedido ha sido guardado correctamente',
            ]);

        } catch( Throwable $e){
            DB::rollBack();

            return response()->json([
                'id' => null,
                'message' => 'Ocurrio un error al guardar el pedido.',
                'errors' => $e->getMessage(),
                'line' => $e->getLine(),
            ], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pedido $pedido)
    {
        //
        try{
            DB::beginTransaction();
            
            $pedido->productos()->delete();

            $pedido->delete();
            DB::commit();
        } catch(Throwable $e) {

            DB::rollBack();

            return response()->json([
                'id' => $pedido->id,
                'message' => 'El pedido se borro correctamente',
                'errors' => $e->getMessage(),
            ]);
        }

        return response()->json([
            'id' => $pedido->id,
            'message' => 'El pedido se elimino correctamente.'
        ]);
    }

    /**
     * Se sube CSV para actualizar stock del pedido
     *
     * @return \Illuminate\Http\Response
     */
    public function csv(UploadCSVRequest $request)
    {
        try{
            DB::beginTransaction();

            $pedido = Pedido::create(['entrada' => true, 'ford' => true, 'tipo' => 'csv']);
            $clave_grupo = $request->proveedor;

            // Guardamos el archivo que se subio
            $csv = $request->file('csv');
            $pedido->factura = $csv->store('facturas_pedidos');
            $pedido->save();

            // Procesamiento de datos
            $productos = Csv::make(storage_path("app/$pedido->factura"))->get();

            $productos->each(function ($producto) use ($pedido, $clave_grupo){

                // Extraemos array a variables 
                // no_identificacion, descripcion, precio, grupo, prefijo, basico y sufijo, total_refaccion, factura
                // proveedor, clave_sat, unidad_sat
                extract($producto);
    
                // Buscamos el producto si no existe se crea
                $producto = CrearProductoService::create([
                    'no_identificacion' => $no_identificacion,
                    'descripcion' => $descripcion,
                    'valor_unitario' => $precio,
                    'grupo' => $clave_grupo, // El archivo csv no trae esta clave
                    'prefijo' => $prefijo,
                    'basico' => $basico,
                    'sufijo' => $sufijo,
                ]);
                
                // El calculo del Costo Promedio debe ir antes que el registro de la entrada
                // en el inventario si no se calculará mal
                $costoPromedio = new CostoPromedio($producto, $cantidad, $precio);
                $costoPromedio->entrada();
    
                // Actualizamos stock
                $inventario = new PedirService;
                $inventario->id($pedido->id);
                $inventario->handle($producto->id, $cantidad, $precio);
                
                $pedido->productos()->attach($producto->id, [
                    "cantidad_entregada" => $cantidad,
                    "precio" => $precio,
                    "total" => $total,
                    "factura" => $factura,
                    'proveedor' => $proveedor,
                ]);
    
                // Mandamos el total sin IVA a contabilidad
                $total_sin_iva = new TotalSinIVA($pedido, [
                    'cliente_nombre' => $proveedor,
                    'num_pieza' => "$no_identificacion - $descripcion",
                    'cantidad' => $cantidad,
                    'costo_promedio' => $precio,
                ]);
                $total_sin_iva->make($total);
    
                // Mandamos el Total con IVA a contabilidad
                $total_con_iva = new Total($pedido, [
                    'cliente_nombre' => $proveedor,
                    'num_pieza' => "$no_identificacion - $descripcion",
                    'cantidad' => $cantidad,
                    'costo_promedio' => $precio,
                ]);
                $total_con_iva->make($total * 1.16);
    
                // Mandamos el IVA a contabilidad
                $iva = new IVA($pedido, [
                    'cliente_nombre' => $proveedor,
                    'num_pieza' => "$no_identificacion - $descripcion",
                    'cantidad' => $cantidad,
                    'costo_promedio' => $precio,
                ]);
                $iva->make($total * 0.16);
            });  

			DB::commit();

			return response()->json([
				'id' => $pedido->id,
				'message' => 'Se agregaron productos al inventario',
			]);
		} catch(\Throwable $e){
			DB::rollBack();

            logger()->warning($e->getMessage(), [
                'file' => $e->getLine(),
                'line' => $e->getFile()
            ]);
		}

        return response()->json([
            'message' => 'Ocurrio un error verifique el formato del archivo CSV sea correcto.',
            'errors' => $e->getMessage(),
        ], 400);
    }

    public function xml(UploadXMLRequest $request)
    {
        try{
            DB::beginTransaction();

            //Creamos Pedido Cargamento
            $pedido = Pedido::create(['fecha_registro' => now(), 'venta_id' => null, 'entrada' => true, 'tipo' => 'xml']);
            $clave_grupo = $request->proveedor;
            
            // Guardamos el archivo que se subio
            $file = $request->file('xml');
            $pedido->factura = $file->store('facturas_pedidos');
            $pedido->save();

            //Procesamiento de datos
            $productos = Xml40::make(storage_path("app/$pedido->factura"))->get();

            // Concepto de Productos
            $productos->each(function($producto) use ($pedido, $clave_grupo){
                // Extraemos array a variables 
                // no_identificacion, descripcion, precio, grupo, prefijo, basico y sufijo, total_refaccion, factura
                // proveedor, clave_sat, unidad_sat
                extract($producto);

                // Buscamos el producto si no existe se crea
                $prod = CrearProductoService::create([
                    'no_identificacion' => $no_identificacion,
                    'grupo' => $clave_grupo, // La factura no trae esta clave en el archivo
                    'descripcion' => $descripcion,
                    'valor_unitario' => $precio,
                    'clave_prod_serv' => $clave_sat,
                    'clave_unidad' => $unidad_sat,
                ]);

                // Creamos registro de la entrada del producto en pedidos
                $pedido->productos()->attach($pedido->id, [
                    'producto_id' => $prod->id,
                    'cantidad_entregada' => $cantidad,
                    'precio' => $precio,
                    'total' => $total,
                    'factura' => $factura,
                    'proveedor' => $proveedor,
                    'clave_sat' => $clave_sat,
                    'unidad_sat' => $unidad_sat,
                ]);

                // El calculo del Costo Promedio debe ir antes que el registro de la entrada
                // en el inventario si no se calculará mal
                $costoPromedio = new CostoPromedio($prod, $cantidad, $precio);
                $costoPromedio->entrada();

                // Registramos la entrada del producto en el inventario
                $inventario = new PedirService;
                $inventario->id($pedido->id);
                $inventario->handle($prod->id, $cantidad, $precio);

                // Mandamos el total sin IVA a contabilidad
                $total_sin_iva = new TotalSinIVA($pedido, [
                    'cliente_nombre' => $proveedor,
                    'num_pieza' => "$no_identificacion - $descripcion",
                    'cantidad' => $cantidad,
                    'costo_promedio' => $precio,
                ]);
                $total_sin_iva->make($total);
    
                // Mandamos el Total con IVA a contabilidad
                $total_con_iva = new Total($pedido, [
                    'cliente_nombre' => $proveedor,
                    'num_pieza' => "$no_identificacion - $descripcion",
                    'cantidad' => $cantidad,
                    'costo_promedio' => $precio,
                ]);
                $total_con_iva->make($total * 1.16);
    
                // Mandamos el IVA a contabilidad
                $iva = new IVA($pedido, [
                    'cliente_nombre' => $proveedor,
                    'num_pieza' => "$no_identificacion - $descripcion",
                    'cantidad' => $cantidad,
                    'costo_promedio' => $precio,
                ]);
                $iva->make($total * 0.16);
            });

            DB::commit();

            return response()->json([
                'id' => $pedido->id,
                'message' => 'El pedido se creo correctamente',
            ]);

        } catch(\Throwable $e){
            DB::rollBack();

            return response()->json([
                'message' => 'Ocurrio un error al generar el pedido revisar el formato del archivo xml.',
                'errors' => $e->getMessage(),
                'line' => $e->getLine(),
                'file' => $e->getFile(),
            ], 400);
        }
    }

    public function descargar_csv(Pedido $pedido)
    {
        $productos = $pedido->productos()->get();

        return new StreamedResponse(function() use ($productos){
            $stream = fopen('php://output', 'w');

            $productos->map(function($producto){
                $obj = new stdClass;
                $obj->no_identificacion = "{$producto->prefijo}{$producto->basico}{$producto->sufijo}";
                $obj->cantidad = $producto->pedido->cantidad;

                return $obj;
            })
            ->filter(function($producto){
                return $producto->cantidad > 0;
            })
            ->every(function($producto) use($stream){
                return fputcsv($stream, [
                    $producto->no_identificacion,
                    $producto->cantidad,
                ]);
            });

            fclose($stream);
        }, 200, [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="pedido.csv"',
        ]);
    }

    public function autorizar(Request $request, Pedido $pedido)
    {
        if($user = User::where($request->only('usuario', 'password'))->first())
        {
            $pedido->autorizado = $user->id;
            $pedido->actual = false;

            $pedido->save();

            return response()->json([
                'message' => 'Pedido Autorizado.',
            ]);
        }

        return response()->json([
            'message' => 'Credenciales incorrectas.'
        ], 400);
    }

    public function ventas(VentasRealizadasModel $venta)
    {
        $productos = VentaProductoModel::with('producto')->where('venta_id', $venta->id)->where('estatus_producto_orden', '<>', 'B')->paginate();

        return response()->json(new VentaProductosCollection($productos));
    }

    public function pedido_del_dia(Request $request)
    {
        try{
            DB::beginTransaction();

            $pedido = Pedido::where('actual', true)->first();

            if($pedido == null)
            {
                $pedido = Pedido::create([
                    'actual' => true,
                ]);
            }

            foreach($request->productos as $producto)
            {
                $pedido->productos()->attach($pedido->id, [
                    'producto_id' => $producto['id'],
                    'cantidad' => $producto['cantidad'],
                ]);
            }

            DB::commit();

            return response()->json([
                'message' => 'Se agregaron los productos al pedido diario.'
            ]);

        } catch(Throwable $e) {

            DB::rollBack();
            return response()->json([
                'message' => 'Ocurrio un error al enviar pedido del día.',
                'errors' => $e->getMessage(),
            ], 400);
        }
    }

    public function descargar_pdf_cargamento(Pedido $pedido)
    {
        $productos = $pedido->productos()->get();

        $html = view('polizas.pedido_cargamento', [
            'productos' => $productos,
            'pedido' => $pedido,
        ])
        ->render();

        $pdf = new Pdf;
        $pdf->html($html);

        return $pdf->output();
    }

    public function cambiar_producto(PedidosProductos $pedido, Request $request)
    {
        $request->validate([
            'producto_id' => 'required|exists:producto,id',
            'descripcion' => 'required|string',
        ]);

        try{
            // El calculo del Costo Promedio debe ir antes que el registro de la salida
            // en el inventario si no se calculará mal
            $costoPromedio = new CostoPromedio($pedido->producto, (int) $pedido->cantidad_entregada, $pedido->precio);
            $costoPromedio->salida();

            // Devolvemos Pedido
            $devolver = new DevolverPedidoService;
            $devolver->id($pedido->id);
            $devolver->handle($pedido->producto_id, $pedido->cantidad_entregada, $pedido->precio);

            // Realizamos la devolucion en Contabilidad
            (new Devolucion($pedido))->handle();
    
            $producto = ProductosModel::find($request->producto_id);
            $producto->descripcion = $request->descripcion;
            $pedido->producto_id = $request->producto_id;

            // El calculo del Costo Promedio debe ir antes que el registro de la entrada
            // en el inventario si no se calculará mal
            $costoPromedio = new CostoPromedio($pedido->producto, (int) $pedido->cantidad_entregada, $pedido->precio);
            $costoPromedio->entrada();
    
            // Registramos la entrada del producto en el inventario
            $inventario = new PedirService;
            $inventario->id($pedido->id);
            $inventario->handle($pedido->producto_id, (int) $pedido->cantidad_entregada, $pedido->precio);

            if($request->ford == true)
            {
                // Mandamos el total sin IVA a contabilidad
                $total_sin_iva = new TotalSinIVA($pedido, [
                    'cliente_nombre' => $pedido->proveedor,
                    'num_pieza' => "{$producto->no_identificacion} - {$producto->descripcion}",
                    'cantidad' => $pedido->cantidad_entregada,
                    'costo_promedio' => $pedido->precio,
                ]);
                $total_sin_iva->make($pedido->total / 1.16);
    
                // Mandamos el Total con IVA a contabilidad
                $total_con_iva = new Total($pedido, [
                    'cliente_nombre' => $pedido->proveedor,
                    'num_pieza' => "{$producto->no_identificacion} - {$producto->descripcion}",
                    'cantidad' => $pedido->cantidad_entregada,
                    'costo_promedio' => $pedido->precio,
                ]);
                $total_con_iva->make($pedido->total);
    
                // Mandamos el IVA a contabilidad
                $iva = new IVA($pedido, [
                    'cliente_nombre' => $pedido->proveedor,
                    'num_pieza' => "{$producto->no_identificacion} - {$producto->descripcion}",
                    'cantidad' => $pedido->cantidad_entregada,
                    'costo_promedio' => $pedido->precio,
                ]);
                $iva->make($pedido->total - ($pedido->total / 1.16));

                Log::info('PROVEEDOR FORD');
            }

            if($request->ford == false)
            {
                // Mandamos el total sin IVA a contabilidad
                $total_sin_iva = new TotalSinIVAExterno($pedido, [
                    'cliente_nombre' => $pedido->proveedor,
                    'num_pieza' => "{$producto->no_identificacion} - {$producto->descripcion}",
                    'cantidad' => $pedido->cantidad_entregada,
                    'costo_promedio' => $pedido->precio,
                ]);
                $total_sin_iva->make($pedido->total);
    
                // Mandamos el Total con IVA a contabilidad
                $total_con_iva = new TotalExterno($pedido, [
                    'cliente_nombre' => $pedido->proveedor,
                    'num_pieza' => "{$producto->no_identificacion} - {$producto->descripcion}",
                    'cantidad' => $pedido->cantidad_entregada,
                    'costo_promedio' => $pedido->precio,
                ]);
                $total_con_iva->make($pedido->total * 1.16);
    
                // Mandamos el IVA a contabilidad
                $iva = new IVAExterno($pedido, [
                    'cliente_nombre' => $pedido->proveedor,
                    'num_pieza' => "{$producto->no_identificacion} - {$producto->descripcion}",
                    'cantidad' => $pedido->cantidad_entregada,
                    'costo_promedio' => $pedido->precio,
                ]);
                $iva->make($pedido->total * 0.16);

                Log::info('PROVEEDOR EXTERNO');
            }

        } catch(Throwable $e){
            return response()->json([
                'message' => 'El producto se actualizo correctamente.',
                'errors' => $e->getMessage(),
            ], 400);
        }

        if($pedido->save() && $producto->save())
        {
            return response()->json([
                'message' => 'El producto se actualizo correctamente.'
            ]);
        }
        
        return response()->json([
            'message' => 'Ocurrio un error al actualizar el producto.'
        ], 400);
    }

    public function subir_factura(Pedido $pedido, UploadFacturaRequest $request)
    {
        $pedido->factura = $request->file('factura')->store('facturas_pedidos');

        if($pedido->save())
        {
            return response()->json([
                'message' => 'Se guardo correctamente la factura.',
            ]);
        }
        
        return response()->json([
            'message' => 'Ocurrio un error al guardar el archivo.'
        ], 400);
    }

    public function devolver_refaccion_pedido(PedidosProductos $producto)
    {
        // Realizamos la devolucion en Contabilidad
        (new Devolucion($producto))->handle();

        // El calculo del Costo Promedio debe ir antes que el registro de la salida
        // en el inventario si no se calculará mal
        $costoPromedio = new CostoPromedio($producto->producto, (int) $producto->cantidad_entregada, $producto->precio);
        $costoPromedio->salida();

        // Registramos la salida del producto en el inventario
        $inventario = new DevolverPedidoService;
        $inventario->id($producto->id);
        $inventario->handle($producto->producto_id, $producto->cantidad_entregada, $producto->precio);

        if($producto->delete())
        {
            return response()->json([
                'message' => 'Se realizo la devolución correctamente.'
            ]);
        }

        return response()->json(
            [ 'message' => 'Se ocurrio un error al realizar la devolución.' ], 400
        );
    }

    public function getFacturaByFolio(string $numero_folio)
    {
        preg_match('/\d+/', $numero_folio, $match);

        $pedido = Pedido::with('piezas')
            ->where('entrada', true)
            ->where('id', $match[0])
            ->first();

        if($pedido == null)
        {
            return response()->json(
                [ 'message' => __("Folio no existe $numero_folio") ], 400
            );
        }

        return response()->json(new FacturaByFolio($pedido));
    }

    public function productos_cargamento_contabilidad(Pedido $pedido)
    {
        return  (ProductosContabilidadResource::collection($pedido->productos()->get()))
            ->additional([ 
                'pedido' => [
                    'id' => $pedido->id,
                    'ford' => $pedido->ford,
                    'proveedor' => $pedido->ford? 'FORD' : $pedido->productos()->first()->pedido->proveedor,
                    'factura' => $pedido->productos()->first()->pedido->factura,
                    'created_at' => $pedido->created_at,
                ]
            ]);
    }
}
