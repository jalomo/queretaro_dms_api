<?php 

namespace App\Http\Controllers\Pedidos\Asientos;

use Exception;
use Illuminate\Support\Facades\Http;

Trait AgregarAsientoTrait
{
    public function make(float $total): Bool
    {
        $contabilidad = Http::asForm()
            ->post(env('URL_CONTABILIDAD'). 'asientos/api/agregar', $this->getRequestData() + [ 'total' => $total ]);

        if($contabilidad->failed())
            throw new Exception($this->failedMessage());

        if($contabilidad->object()->status == 'error')
            throw new Exception($this->failedMessage($contabilidad->object()->message));

        logger()->info("Asiento Agregado {$this->getFolio()}", [
            'status' => $contabilidad->object()->status, 'folio' => $this->getFolio(), 'tipo' => $this->getTipo(), 'total' => $total, 'request' => $this->getRequestData()
        ]);

        return true;
    }

    public function getRequestData()
    {
        return [
            'clave_poliza' => $this->getPoliza(),
            'tipo' => $this->getTipo(), // 1 Abono, 2 Cargo
            'cuenta' => $this->getCuenta(),
            'concepto' => $this->getConcepto(),
            'departamento' => $this->getDepartamento(),
            'estatus' => $this->getEstatus(),
            'folio' => $this->getFolio(),
            'cliente_id' => $this->getCliente(),
            'sucursal_id' => $this->getSucursal(),
            'cantidad' => $this->getCantidad(),
            'costo_promedio' => $this->getCostoPromedio(),
            'cliente_nombre' => $this->getClienteNombre(),
        ];
    }
}