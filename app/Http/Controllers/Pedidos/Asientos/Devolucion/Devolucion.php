<?php 

namespace App\Http\Controllers\Pedidos\Asientos\Devolucion;

use App\Http\Controllers\Pedidos\Asientos\Devolucion\Ford\IVA as IVADevolucion;
use App\Http\Controllers\Pedidos\Asientos\Devolucion\Ford\TotalSinIVA as TotalDevolucionSinIVA;
use App\Http\Controllers\Pedidos\Asientos\Devolucion\Ford\Total as TotalDevolucion;
use App\Http\Controllers\Pedidos\Asientos\Devolucion\Externo\IVA as IVADevolucionExterno;
use App\Http\Controllers\Pedidos\Asientos\Devolucion\Externo\TotalSinIVA as TotalDevolucionSinIVAExterno;
use App\Http\Controllers\Pedidos\Asientos\Devolucion\Externo\Total as TotalDevolucionExterno;
use App\Models\Refacciones\PedidosProductos;

class Devolucion
{
    protected $refaccion;

    public function __construct(PedidosProductos $refaccion)
    {
        $this->refaccion = $refaccion;        
    }

    public function handle(array $request = [])
    {
        if($this->refaccion->pedido->ford == true)
        {
            // Mandamos el total sin IVA a contabilidad
            $total_sin_iva = new TotalDevolucionSinIVA($this->refaccion, [
                'cliente_nombre' => $this->refaccion->proveedor,
                'num_pieza' => "{$this->refaccion->producto->no_identificacion} - {$this->refaccion->producto->descripcion}",
                'cantidad' => $this->refaccion->cantidad_entregada,
                'costo_promedio' => $this->refaccion->precio,
            ]);
            $total_sin_iva->make($this->refaccion->total);

            // Mandamos el Total con IVA a contabilidad
            $total_con_iva = new TotalDevolucion($this->refaccion, [
                'cliente_nombre' => $this->refaccion->proveedor,
                'num_pieza' => "{$this->refaccion->producto->no_identificacion} - {$this->refaccion->producto->descripcion}",
                'cantidad' => $this->refaccion->cantidad_entregada,
                'costo_promedio' => $this->refaccion->precio,
            ]);
            $total_con_iva->make($this->refaccion->total * 1.16);

            // Mandamos el IVA a contabilidad
            $iva = new IVADevolucion($this->refaccion, [
                'cliente_nombre' => $this->refaccion->proveedor,
                'num_pieza' => "{$this->refaccion->producto->no_identificacion} - {$this->refaccion->producto->descripcion}",
                'cantidad' => $this->refaccion->cantidad_entregada,
                'costo_promedio' => $this->refaccion->precio,
            ]);
            $iva->make($this->refaccion->total * 0.16);

            logger()->info('DEVOLUCIÓN PROVEEDOR EXTERNO');
        }

        if($this->refaccion->pedido->ford == false)
        {
            // Mandamos el total sin IVA a contabilidad
            $total_sin_iva = new TotalDevolucionSinIVAExterno($this->refaccion, [
                'cliente_nombre' => $this->refaccion->proveedor,
                'num_pieza' => "{$this->refaccion->producto->no_identificacion} - {$this->refaccion->producto->descripcion}",
                'cantidad' => $this->refaccion->cantidad_entregada,
                'costo_promedio' => $this->refaccion->precio,
            ]);
            $total_sin_iva->make($this->refaccion->total);

            // Mandamos el Total con IVA a contabilidad
            $total_con_iva = new TotalDevolucionExterno($this->refaccion, [
                'cliente_nombre' => $this->refaccion->proveedor,
                'num_pieza' => "{$this->refaccion->producto->no_identificacion} - {$this->refaccion->producto->descripcion}",
                'cantidad' => $this->refaccion->cantidad_entregada,
                'costo_promedio' => $this->refaccion->precio,
            ]);
            $total_con_iva->make($this->refaccion->total * 1.16);

            // Mandamos el IVA a contabilidad
            $iva = new IVADevolucionExterno($this->refaccion, [
                'cliente_nombre' => $this->refaccion->proveedor,
                'num_pieza' => "{$this->refaccion->producto->no_identificacion} - {$this->refaccion->producto->descripcion}",
                'cantidad' => $this->refaccion->cantidad_entregada,
                'costo_promedio' => $this->refaccion->precio,
            ]);
            $iva->make($this->refaccion->total * 0.16);

            logger()->info('DEVOLUCIÓN  PROVEEDOR EXTERNO');
        }
    }
}