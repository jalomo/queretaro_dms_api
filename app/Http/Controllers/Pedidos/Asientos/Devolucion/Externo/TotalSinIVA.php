<?php 

namespace App\Http\Controllers\Pedidos\Asientos\Devolucion\Externo;

use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Pedidos\Asientos\AgregarAsientoTrait;
use App\Http\Controllers\Pedidos\Asientos\ContabilidadInterface;

class TotalSinIVA implements ContabilidadInterface
{
    use AgregarAsientoTrait;

    protected $pedido;
    protected $extra;

    public function __construct($producto, array $extra = [])
    {
        $this->pedido = $producto->pedido;
        $this->extra = $extra;
    }

    public function getPoliza(): string
    {
        return 'C0';
    }

    public function getDepartamento(): int
    {
        return 13;
    }

    public function getFolio(): string
    {
        return env('ALMACEN_PRIMARIO') == 'QUERETARO'? "QRT-PEDIDO-{$this->pedido->id}" : "SNJ-PEDIDO-{$this->pedido->id}";
    }

    public function getTipo(): int
    {
        return 2;
    }

    public function getCuenta(): int
    {
        return env('ALMACEN_PRIMARIO') == 'QUERETARO'? 158 : 159;
    }

    public function getConcepto(): string
    {
        if(isset($this->extra['num_pieza']))
        {
            return $this->extra['num_pieza'];
        }

        return env('ALMACEN_PRIMARIO') == 'QUERETARO'? 'ALMACEN '. env('ALMACEN_PRIMARIO')  : 'ALMACEN '. env('ALMACEN_SECUNDARIO');
    }

    public function getEstatus(): string
    {
        return 'APLICADO';
    }

    public function getCliente(): string
    {
        return ''; // No lo tenemos es cargamento
    }

    public function getSucursal(): int
    {
        return env('ALMACEN_PRIMARIO') == 'QUERETARO'? 1 : 2; // 1 Queretaro, 2 San Juan
    }

    public function getCantidad(): int
    {
        return $this->extra['cantidad']?? 1;
    }

    public function getCostoPromedio(): float
    {
        return $this->extra['costo_promedio']?? 0;
    }

    public function getClienteNombre(): string
    {
        return $this->extra['cliente_nombre']?? '';
    }

    public function failedMessage($message = 'La petición para agregar el total sin IVA a los asientos a contabilidad no fue satisfactoria.'): string
    {
        return tap($message, function($message){
            Log::warning($message);
        });
    }
}