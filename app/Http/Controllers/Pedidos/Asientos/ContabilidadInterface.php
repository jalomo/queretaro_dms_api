<?php 

namespace App\Http\Controllers\Pedidos\Asientos;

interface ContabilidadInterface
{
    /**
     * Retorna clave poliza
     *
     * @return string
     */
    public function getPoliza(): string;

    /**
     * Retorna Tipo Abono = 1, Cargo = 2
     *
     * @return int
     */
    public function getTipo(): int;

    /**
     * Retorna el numero de cuenta
     *
     * @return int
     */
    public function getCuenta(): int;

    /**
     * Retorna el concepto de pago o descripción
     *
     * @return string
     */
    public function getConcepto(): string;

    /**
     * Retorna el estatus de aplicado, etc.
     *
     * @return string
     */
    public function getEstatus(): string;

    /**
     * Retorna el departamento
     *
     * @return int
     */
    public function getDepartamento(): int;

    /**
     * Se envía el numero de cliente
     *
     * @return string
     */
    public function getCliente(): string;

    /**
     * Retorna el id de la sucursal
     *
     * @return int
     */

    public function getSucursal(): int;
    /**
     * Retorna la cantidad
     *
     * @return int
     */
    public function getCantidad(): int;

    /**
     * Retorna el numero de folio generado
     *
     * @return string
     */
    public function getFolio(): string;

    /**
     * Ejecuta la api y envía a contabilidad
     * 
     * @param float $total
     * @return Boolean
     */
    public function make(float $total): Bool;

    /**
     * Envía mensajes al log y de respuesta
     * 
     * @param int $message
     * @return string
     */
    public function failedMessage($message): string;
}