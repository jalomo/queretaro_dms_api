<?php

namespace App\Http\Controllers\Pedidos\Archivos;

use Exception;

class Csv implements ArchivoInterface
{
    use Base;

    protected $collection = [];

    public function load(string $filepath): ArchivoInterface
    {
        if(! file_exists($filepath))
        {
            throw new Exception("Archivo no encontrado: $filepath");
        }

        $file = fopen($filepath, 'r');

        $start = 2;
        $end = 3;
        $blank_lines = 0;

        while(($data = fgetcsv($file)) !== false)
        {
            $isEmpty = $this->isEmpty($data);
            $blank_lines += $isEmpty? 1 : 0;
            
            // Si aun no llegamos a la segunda fila en blanco 
            // o llegamos y continuamos a la siguiente
            if($blank_lines < $start || $isEmpty)
                continue;

            // Encontramos la segunda linea
            if($blank_lines == $end)
                break;     

            $grupo = '';
            $prefijo = trim($data[5]);
            $basico = trim($data[6]);
            $sufijo = trim($data[7]);

            $this->collection[] = [
                'factura' => $data[0],
                'grupo' => $grupo, // Al parecer todo lo que se carga aqui es FORD
                'prefijo' => $prefijo,
                'basico' => $basico,
                'sufijo' => $sufijo,
                'no_identificacion' => "$grupo$prefijo$basico$sufijo",
                'cantidad' => $data[12],
                'precio' => $data[9],
                'descripcion' => $data[10],
                'total' => $data[13],
                'clave_sat' => '', // No es proporcionada
                'unidad_sat' => '', // No es proporcionada
                'proveedor' => 'FORD MOTORS', // No es proporcionada
            ];
        }

        return $this;
    }

    protected function isEmpty($array)
    {
        foreach($array as $element)
        {
            if(! empty($element))
                return false;
        }

        return true;
    }
}