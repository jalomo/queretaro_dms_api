<?php

namespace App\Http\Controllers\Pedidos\Archivos;

use Illuminate\Support\Collection;

trait Base
{
    public function get(): Collection
    {
        return collect($this->collection);
    }

    public static function make(string $filepath): ArchivoInterface
    {
        return (new static)->load($filepath);
    }
}