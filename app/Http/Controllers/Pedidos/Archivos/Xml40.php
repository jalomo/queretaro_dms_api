<?php 

namespace App\Http\Controllers\Pedidos\Archivos;

use Exception;

class Xml40 implements ArchivoInterface
{
    use Base;

    protected $collection = [];

    public function load(string $filepath): ArchivoInterface
    {
        if(! file_exists($filepath))
        {
            throw new Exception("Archivo no encontrado: $filepath");
        }

        $xml = simplexml_load_file($filepath);

        // Folio Factura
        $factura = $xml->attributes()->Folio;
        // Proveedor
        $proveedor = (string) $xml->xpath('cfdi:Emisor')[0]->attributes()->Nombre;

        foreach($xml->xpath('cfdi:Conceptos//cfdi:Concepto') as $producto)
        {
            // Num. Pieza
            $no_identificacion = preg_replace('/\s/', '', $producto->attributes()->NoIdentificacion);

            $this->collection[] = [
                'factura' => $factura,
                'grupo' => '', // No es proporcionada
                'prefijo' => '', // No es proporcionada
                'basico' => '', // No es proporcionada
                'sufijo' => '', // No es proporcionada
                'no_identificacion' => $no_identificacion,
                'cantidad' => (int) $producto->attributes()->Cantidad,
                'precio' => (float) $producto->attributes()->ValorUnitario,
                'descripcion' => (string) $producto->attributes()->Descripcion,
                'total' => (float) $producto->attributes()->Importe,
                'clave_sat' => (string) $producto->attributes()->ClaveProdServ,
                'unidad_sat' => (string) $producto->attributes()->ClaveUnidad,
                'proveedor' => $proveedor,
            ];
        }

        return $this;   
    }
}