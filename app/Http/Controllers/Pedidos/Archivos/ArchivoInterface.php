<?php

namespace App\Http\Controllers\Pedidos\Archivos;

use Illuminate\Support\Collection;

interface ArchivoInterface
{
    /**
     * Crea insancia del Archivo
     *
     * @param string $filepath
     * @return ArchivoInterface
     */
    public static function make(string $filepath) : ArchivoInterface;

    /**
     * Carga el archivo y lo procesa
     *
     * @param string $filepath
     * @return ArchivoInterface
     */
    public function load(string $filepath) : ArchivoInterface;

    /**
     * Retorna la coleccion del archivo procesado
     *
     * @return Collection
     */
    public function get() : Collection;
}