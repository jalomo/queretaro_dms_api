<?php

namespace App\Http\Controllers\Nomina\Catalogos;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Nomina\ServicioPuestos;
use Illuminate\Http\Request;

// Necesitaremos el modelo Fabricante para ciertas tareas.
///use App\Fabricante;

class PuestosController extends CrudController
{
  public function __construct()
  {
    $this->servicio = new ServicioPuestos();
  }
}
