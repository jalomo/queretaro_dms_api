<?php

namespace App\Http\Controllers\Reemplazo;

use App\Http\Controllers\Core\Controller;
use App\Http\Requests\Reemplazo\StoreReemplazo;
use App\Http\Resources\DataTablesResource;
use App\Models\Refacciones\ProductosModel;
use Illuminate\Http\Request;
use App\Models\Refacciones\Reemplazo;

class ReemplazoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $reemplazos = Reemplazo::with('producto', 'reemplazo')
        ->when($request->no_identificacion, function($q) use($request){
            $q->whereHas('producto', function($q) use ($request){
                $q->where('no_identificacion', 'like', "%$request->no_identificacion%");
            });
            $q->orWhereHas('reemplazo', function($q) use ($request){
                $q->where('no_identificacion', 'like', "%$request->no_identificacion%");
            });
        })
        ->paginate();

        return response()->json(new DataTablesResource($reemplazos));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreReemplazo $request)
    {
        //
        $producto = ProductosModel::where('no_identificacion', $request->producto)->first();
        $reemplazo = ProductosModel::where('no_identificacion', $request->reemplazo)->first();

        $reemplazo = Reemplazo::create([
            'producto_id' => $producto->id,
            'reemplazo_id' => $reemplazo->id,
            'precio_producto' => $producto->valor_unitario,
            'precio_reemplazo' => $reemplazo->valor_unitario,
        ]);

        $producto->reemplazo_id = $reemplazo->id;

        if($reemplazo->id && $producto->save())
        {
            return response()->json([
                'message' => 'Se registro reemplazo',
            ], 201);
        }

        return response()->json([
            'message' => 'Ocurrio un error al registrar el reeemplazo',
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reemplazo $reemplazo)
    {
        //
        if($reemplazo->delete())
        {
            return response()->json([
                'message' => 'El reemplazo se elimino.',
            ]);
        }

        return response()->json([
            'message' => 'El reemplazo no se pudo eliminar.'
        ], 400);
    }
}
