<?php

namespace App\Http\Controllers\Ventas\Detalles;

use App\Exceptions\RepositoryHandlerNotFoundException;
use App\Http\Controllers\Core\Controller;
use App\Servicios\Ventas\Detalles\Handler;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class DetallesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Handler $handlerRepository)
    {
        //
        try{
            return $handlerRepository->handle($request, 'list');
        } catch(RepositoryHandlerNotFoundException $e)
        {
            return response()->json([
                'message' => $e->getMessage(),
            ], 400);
        }
        catch(Exception $e)
        {
            return response()->json([
                'message' => 'Ocurrio un error al obtener las refacciones.',
            ], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Handler $handlerRepository)
    {
        //
        try{
            return $handlerRepository->handle($request, 'store');
        } catch(RepositoryHandlerNotFoundException $e)
        {
            return response()->json([
                'message' => $e->getMessage(),
            ], 400);
        }
        catch(ValidationException $e)
        {
            throw new ValidationException($e);
        }
        catch(Exception $e)
        {
            return response()->json([
                'message' => 'Ocurrio un error al agregar la refacción.',
            ], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Handler $handlerRepository)
    {
        //
        try{
            return $handlerRepository->handle($request, 'update');
        } catch(RepositoryHandlerNotFoundException $e)
        {
            return response()->json([
                'message' => $e->getMessage(),
            ], 400);
        }
        catch(Exception $e)
        {
            return response()->json([
                'message' => 'Ocurrio un error al actualizar la refacción.',
            ], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Handler $handlerRepository)
    {
        //
        try{
            return $handlerRepository->handle($request, 'destroy');
        } catch(RepositoryHandlerNotFoundException $e)
        {
            return response()->json([
                'message' => $e->getMessage(),
            ], 400);
        }
        catch(Exception $e)
        {
            return response()->json([
                'message' => 'Ocurrio un error al eliminar la refacción.',
            ], 400);
        }
    }
}
