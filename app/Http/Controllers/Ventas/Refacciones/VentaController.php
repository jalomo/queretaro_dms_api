<?php

namespace App\Http\Controllers\Ventas\Refacciones;

use App\Http\Controllers\Core\Controller;
use App\Servicios\Ventas\Refacciones\ServicioVentaRefacciones;
use App\Http\Requests\Ventas\StoreVenta;
use App\Http\Requests\Ventas\UpdateVenta;
use App\Http\Requests\Ventas\FinalizarVenta;
use Throwable;

class VentaController extends Controller
{
    protected $servicio;

    public function __construct(ServicioVentaRefacciones $servicio)
    {
        $this->servicio = $servicio;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $refacciones = $this->servicio->all();

        return response()->json($refacciones);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Illuminate\Http\Requests\Ventas\StoreVenta  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreVenta $request)
    {
        //
        try
        {
            $response = $this->servicio->store($request->validated());
            $code = 201;
        } 
        catch(Throwable $e)
        {
            $response = [ 'message' => $e->getMessage() ];
            $code = 400;
        }        

        return response()->json($response, $code);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $response = $this->servicio->getById($id);

        return response()->json($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Illuminate\Http\Requests\Ventas\UpdateVenta  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateVenta $request, $id)
    {
        //
        try
        {
            $response = $this->servicio->update($request->validated(), $id);
            $code = 200;
        } 
        catch(Throwable $e)
        {
            $response = [ 'message' => $e->getMessage() ];
            $code = 400;
        }   

        return response()->json($response, $code);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Illuminate\Http\Requests\Ventas\FinalizarVenta  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function finalizar(FinalizarVenta $request, $id)
    {
        //
        try
        {
            $response = $this->servicio->finalizar($request->validated(), $id);
            $code = 200;
        } 
        catch(Throwable $e)
        {
            $response = [ 'message' => $e->getMessage() ];
            $code = 400;
        }   

        return response()->json($response, $code);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try
        {
            $response = $this->servicio->destroy($id);
            $code = 200;
        }
        catch(Throwable $e)
        {
            $response = $e->getMessage();
            $code = 400;
        }

        return response()->json($response, $code);
    }
}
