<?php

namespace App\Http\Controllers\Orden;

use App\Http\Controllers\Core\Controller;
use Illuminate\Http\Request;
use App\Models\CuentasPorCobrar\CuentasPorCobrarModel;
use App\Http\Requests\Orden\RefaccionesFacturadasRequest;
use App\Http\Resources\Orden\RefaccionesFacturadasResource;

class RefaccionesFacturadasController extends Controller
{
    /**
     * Devuelve las refacciones facturadas
     * 
     * @param App\Http\Requests\Orden\RefaccionesFacturadasRequest $request
     * @return App\Http\Resources\Orden\RefaccionesFacturadasResource
     */
    public function index(RefaccionesFacturadasRequest $request)
    {
        $refacciones = CuentasPorCobrarModel::
            join('ventas', 'cuentas_por_cobrar.folio_id', '=', 'ventas.folio_id')
            ->join('venta_producto', 'ventas.id', '=', 'venta_producto.venta_id')
            ->join('producto', 'venta_producto.producto_id', 'producto.id')
            ->where([
                'cuentas_por_cobrar.numero_orden' => $request->orden,
                'cuentas_por_cobrar.estatus_cuenta_id' => 6, // Facturada
            ])
            ->select(
                'cuentas_por_cobrar.numero_orden', 
                'producto.basico', 
                'producto.sufijo', 
                'producto.gpo', 
                'producto.prefijo', 
                'venta_producto.cantidad', 
                'venta_producto.venta_total', 
                'venta_producto.valor_unitario', 
                'producto.descripcion',
            )
            ->get();

        return response()->json(RefaccionesFacturadasResource::collection($refacciones));
    }
}
