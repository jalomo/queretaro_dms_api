<?php

namespace App\Http\Controllers\Contabilidad;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Contabilidad\ServicioProcesos;

class ProcesosController extends CrudController
{
    public function __construct() {
        $this->servicio = new ServicioProcesos();
    }
}
