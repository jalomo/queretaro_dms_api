<?php

namespace App\Http\Controllers\Contabilidad;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Contabilidad\ServicioCarteraCliente;
use App\Servicios\Core\Respuestas\Respuesta;
use Illuminate\Http\Request;
use App\Models\Contabilidad\CarteraClienteModel;

class CarteraClienteController extends CrudController
{
    public function __construct() {
        $this->servicio = new ServicioCarteraCliente();
    }

    public function getByClienteId(Request $request){
        try {
            return Respuesta::json($this->servicio->getCarteraCliente($request->get(CarteraClienteModel::CLIENTE_ID)),200);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
    }
}
