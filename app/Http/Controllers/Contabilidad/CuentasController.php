<?php

namespace App\Http\Controllers\Contabilidad;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Contabilidad\ServicioCuentas;
use App\Servicios\Core\Respuestas\Respuesta;

class CuentasController extends CrudController
{
    public function __construct() {
        $this->servicio = new ServicioCuentas();
    }

    public function getCuentasBancarias(){
        try {
			return Respuesta::json($this->servicio->getCuentaBancarias(), 200);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
    }
}
