<?php

namespace App\Http\Controllers\Inventario;

use App\Http\Controllers\Core\Controller;
use App\Servicios\Presupuestos\RecepcionRefaccionesEnPresupuesto;
use App\Http\Requests\Inventarios\DescontarVentaRequest;

class InventarioController extends Controller
{
    /**
     * Se descuenta el inventario de una venta con el numero de orden y no. de parte
     *
     * @param Request $request
     * @param string $numero_orden
     * @return Illuminate\Http\Response
     */
    public function descontar_inventario_venta(DescontarVentaRequest $request, $numero_orden)
    {
        $inventario = new RecepcionRefaccionesEnPresupuesto;

        if($inventario->handle($numero_orden, $request->no_identificacion, $request->cantidad))
        {
            return response()->json([
                'message' => $inventario->getMessage(),
            ]);
        }

        return response()->json([
            'message' => $inventario->getMessage(),
        ], 400);
    }
}