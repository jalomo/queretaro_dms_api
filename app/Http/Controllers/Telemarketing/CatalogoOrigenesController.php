<?php

namespace App\Http\Controllers\Telemarketing;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Telemarketing\ServicioCatOrigenes;
use Illuminate\Http\Request;

class CatalogoOrigenesController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioCatOrigenes();
    }
}
