<?php

namespace App\Http\Controllers\Oasis;

use Illuminate\Http\Request;
use App\Http\Controllers\Core\CrudController;
use App\Http\Requests\Oasis\MantenimientoUnidadesRequest;
use App\Models\Oasis\MantenimientoUnidadesModel;

class MantenimientoUnidadesController extends CrudController
{
    public function guardarMantenimiento(MantenimientoUnidadesRequest $request){
        $mantenimiento = new MantenimientoUnidadesModel();
        $mantenimiento->remision_id = $request->unidad_id;
        $mantenimiento->tecnico_mantenimiento = $request->tecnico_mantenimiento;
        $mantenimiento->no_orden_reparacion = $request->no_orden_reparacion;
        $mantenimiento->fecha_recibo_unidad = $request->fecha_recibo_unidad;
        $mantenimiento->fecha_mantenimiento = $request->fecha_mantenimiento;
        $mantenimiento->estado_bateria = $request->estado_bateria;
        $mantenimiento->estatus_voltaje_bateria = $request->estatus_voltaje_bateria;
        $mantenimiento->voltaje_bateria_inicial = $request->voltaje_bateria_inicial;
        $mantenimiento->voltaje_bateria_final = $request->voltaje_bateria_final;
        $mantenimiento->estado_bateria_electrico = $request->estado_bateria_electrico;
        $mantenimiento->estatus_voltaje_bateria_electrico = $request->estatus_voltaje_bateria_electrico;
        $mantenimiento->voltaje_bateria_inicial_electrico = $request->voltaje_bateria_inicial_electrico;
        $mantenimiento->voltaje_bateria_final_electrico = $request->voltaje_bateria_final_electrico;

        $mantenimiento->temperatura_normal = $request->temperatura_normal;
        $mantenimiento->estereo_apagado = $request->estereo_apagado;
        $mantenimiento->aire_apagado = $request->aire_apagado;
        $mantenimiento->cambios_transmision = $request->cambios_transmision;
        $mantenimiento->rotacion_llantas = $request->rotacion_llantas;
        $mantenimiento->estado_elevadores = $request->estado_elevadores;
        $mantenimiento->fugas_fluidos = $request->fugas_fluidos;
        $mantenimiento->protectores_asientos = $request->protectores_asientos;
        $mantenimiento->presion_llanta_dd = $request->presion_llanta_dd;
        $mantenimiento->presion_llanta_di = $request->presion_llanta_di;
        $mantenimiento->presion_llanta_ti = $request->presion_llanta_ti;
        $mantenimiento->presion_llanta_td = $request->presion_llanta_td;
        $mantenimiento->caducidad_wrap = $request->caducidad_wrap;
        $mantenimiento->lavado_ford = $request->lavado_ford;
        $mantenimiento->estado_pintura = $request->estado_pintura;
        $mantenimiento->cera_ford = $request->cera_ford;
        $mantenimiento->descontaminacion_pintura = $request->descontaminacion_pintura;
        $mantenimiento->gomas_limpiaparabrisas = $request->gomas_limpiaparabrisas;
        $mantenimiento->nivel_gasolina = $request->nivel_gasolina;
        $mantenimiento->parte_baja_vieculo = $request->parte_baja_vieculo;
        $mantenimiento->nivel_anticongelante = $request->nivel_anticongelante;
        $mantenimiento->nivel_aceite_motor = $request->nivel_aceite_motor;
        $mantenimiento->nivel_liquido_frenos = $request->nivel_liquido_frenos;
        $mantenimiento->nivel_aceite_transmicion = $request->nivel_aceite_transmicion;
        $mantenimiento->nivel_chisgueteros = $request->nivel_chisgueteros;
        $mantenimiento->nivel_aceite_dieccion = $request->nivel_aceite_dieccion;
        $mantenimiento->luces_delanteras = $request->luces_delanteras;
        $mantenimiento->direccionales = $request->direccionales;
        $mantenimiento->luces_traceras = $request->luces_traceras;
        $mantenimiento->luz_toldo = $request->luz_toldo;
        $mantenimiento->luz_emergencia = $request->luz_emergencia;
        $mantenimiento->luz_vanidad = $request->luz_vanidad;
        $mantenimiento->luz_frenos = $request->luz_frenos;
        $mantenimiento->luz_reversa = $request->luz_reversa;
        $mantenimiento->luz_placa = $request->luz_placa;
        $mantenimiento->luz_guantera = $request->luz_guantera;

        $mantenimiento->kilometraje_madrina = $request->kilometraje_madrina;
        $mantenimiento->kilometraje_intercambio = $request->kilometraje_intercambio;
        $mantenimiento->kilometraje_traslado = $request->kilometraje_traslado;
        $mantenimiento->kilometraje_prueba = $request->kilometraje_prueba;
        $mantenimiento->papeleria_completa = $request->papeleria_completa;
        $mantenimiento->fecha_venta = $request->fecha_venta;
        $mantenimiento->save();
    }

    /* Historial de mantenimientos por remision_id */
    public function historial($id){
        $historialUnidad = MantenimientoUnidadesModel::with('unidad')
                                                     ->where('remision_id', $id)                                                    
                                                    ->get();
        return $historialUnidad;        
    }
    /* Historial de mantnimiento por id mantenimiento */
    public function historialId($id)
    {
        $historialUnidad = MantenimientoUnidadesModel::with('unidad')
                                                     ->whereId($id)
                                                    ->get();        
        return $historialUnidad;
    }

    public function historialMantenimientosUnidad(Request $request)
    {
        $arrayMantenimientosId = $request->all();
        $historialUnidad = MantenimientoUnidadesModel::with('unidad')
                                                     ->whereIn('id', $arrayMantenimientosId)
                                                    ->get();
        return $historialUnidad;
    }
}
