<?php

namespace App\Http\Controllers\Oasis;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Bodyshop\ServiciosOasis;
use App\Servicios\Core\Respuestas\Respuesta;
use Illuminate\Http\Request;

class OasisController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServiciosOasis();
    }
    public function getAll(Request $request){
        return Respuesta::json($this->servicio->getAll($request->all()), 200);
    }
}
