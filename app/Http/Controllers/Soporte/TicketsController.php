<?php

namespace App\Http\Controllers\Soporte;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Models\Notificaciones\NotificacionesModel;
use App\Models\Soporte\HistorialTicketsModel;
use App\Models\Soporte\TicketsModel;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Core\ServicioManejoArchivos;
use App\Servicios\Soporte\ServicioTickets;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;

class TicketsController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioTickets();
        $this->servicioArchivos = new ServicioManejoArchivos();
        date_default_timezone_set('America/Mexico_City');
    }
    public function updateTicket(Request $request, $id)
    {
        try {
            $reglas = [
                HistorialTicketsModel::ESTATUS_NUEVO_ID => 'required',
                HistorialTicketsModel::USER_ID => 'nullable',
                HistorialTicketsModel::USUARIO => 'required',
                HistorialTicketsModel::COMENTARIO => 'required',
                HistorialTicketsModel::EVIDENCIA => 'nullable',
                NotificacionesModel::FECHA_NOTIFICACION => 'required',
                NotificacionesModel::ASUNTO => 'required',
                NotificacionesModel::TEXTO => 'required',
                NotificacionesModel::USER_ID => 'required'

            ];
            ParametrosHttpValidador::validar($request, $reglas);
            $ticket = $this->servicio->updateTicket($request, $id);
            $mensaje = __(static::$I0013_TICKET_ACTUALIZADO, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($ticket, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
    public function uploadTicket(Request $request)
    {
        try {
            ParametrosHttpValidador::validar(
                $request,
                [
                    TicketsModel::ASUNTO => 'required',
                    TicketsModel::COMENTARIO => 'required',
                    TicketsModel::EVIDENCIA => 'required',
                    TicketsModel::USER_ID => 'required',
                    TicketsModel::PRIORIDAD_ID => 'required',
                    TicketsModel::ROL_ID => 'required',
                    HistorialTicketsModel::USUARIO => 'required',
                    TicketsModel::EVIDENCIA => 'nullable|mimes:pdf,jpg,jpeg,png,mp4,avi'
                ]
            );
            $path = '';
            if (TicketsModel::EVIDENCIA != '') {
                $file = $request->file(TicketsModel::EVIDENCIA);
                $newFileName = $this->servicioArchivos->setFileName($file);
                $directorio = $this->servicioArchivos->setDirectory(TicketsModel::DIRECTORIO_TICKET);
                $url_image = $this->servicioArchivos->upload($file, $directorio, $newFileName);
                $path =  DIRECTORY_SEPARATOR . $directorio  . DIRECTORY_SEPARATOR . $newFileName;
            }


            $xml_parameters = $this->servicio->handleSaveTicket($path, $request->all());
            $mensaje = __(static::$I0011_FACTURA_SUBIDA, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($xml_parameters, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
    public function getAll(Request $request)
    {
        try {
            return Respuesta::json($this->servicio->getAll($request->all()), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
