<?php

namespace App\Http\Controllers\Notificaciones;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Notificaciones\ServicioNotificaciones;
use Illuminate\Http\Request;

class NotificacionesController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioNotificaciones();
    }
    public function getAll(Request $request){
        try {
			return Respuesta::json($this->servicio->getAll($request->all()), 200);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
    }
}
