<?php

namespace App\Http\Controllers\Caja;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Caja\ServicioEstatusAnticipos;

class EstatusAnticiposController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioEstatusAnticipos();
    }

}
