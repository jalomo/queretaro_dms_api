<?php

namespace App\Http\Controllers\Caja;

use App\Http\Controllers\Core\Controller;
use Illuminate\Http\Request;
use App\Models\Refacciones\ClientesModel;
use App\Servicios\CuentasPorCobrar\ServicioCuentaPorCobrar;
use App\Servicios\Refacciones\ServicioFolios;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class ClienteCuentasController extends Controller
{
    /**
     * Se procesa el archivo
     * 
     */
    public function store(Request $request)
    {
        $filename = $request->file('archivo')->store('chunks');

        $file_path = storage_path('app/' .$filename);

        try{
            $file = fopen($file_path, 'r');
            $cont = 0;

            while (($data = fgetcsv($file)) !== false) 
            {
                $cont++;
                // Saltamos las primeras lineas que corresponden
                // al Header del archivo .csv que se sube
                if($cont <= 4) 
                    continue;

                $venta_servicio = (float) $data[2];
                $venta_refaccion = (float) $data[3];

                $cliente = ClientesModel::updateOrCreate([
                    'numero_cliente' => $data[0]
                ], [
                    'nombre_empresa' => utf8_encode($data[1]),
                    'nombre' => utf8_encode($data[1]),
                    'aplica_credito' => $venta_servicio > 0 || $venta_refaccion > 0? true : false,
                ]);


                // Servicio folios
                $servicioFolio = new ServicioFolios;
                $folioServicio = $servicioFolio->generarFolio(8);

                // Cuenta x Cobrar Servicios
                if($venta_servicio > 0)
                {
                    $servicio = [
                        "cliente_id" => $cliente->id,
                        "folio_id" => $folioServicio->id,
                        "concepto" => 'Servicio',
                        "estatus_cuenta_id" => 1,
                        "fecha" => ! empty($data[7]) ? Carbon::createFromFormat('d/m/Y', $data[7]) : Carbon::now(),
                        "plazo_credito_id" => 1,
                        "tipo_forma_pago_id" => 2,
                        "tipo_pago_id" => 22,
                        "tipo_proceso" => 8,
                        "venta_total" => $venta_servicio,
                    ];
                    
                    $cuenta_servicios = new ServicioCuentaPorCobrar;
                    $cuenta_servicios->procesarCuentasPorCobrar($servicio);
                }

                // Refaccion folios
                $refaccionFolio = new ServicioFolios;
                $folioRefaccion = $refaccionFolio->generarFolio(1);

                // Cuenta x Cobrar Refacciones
                if($venta_refaccion > 0)
                {
                    $refacciones = [
                        "cliente_id" => $cliente->id,
                        "folio_id" => $folioRefaccion->id,
                        "concepto" => 'refacciones',
                        "estatus_cuenta_id" => 5,
                        "fecha" => ! empty($data[7]) ? Carbon::createFromFormat('d/m/Y', $data[7]) : Carbon::now(),
                        "plazo_credito_id" => 1,
                        "tipo_forma_pago_id" => 2,
                        "tipo_pago_id" => 9,
                        "tipo_proceso" => 1,
                        "venta_total" => $venta_refaccion,
                    ];

                    $cuenta_refacciones = new ServicioCuentaPorCobrar;
                    $cuenta_refacciones->procesarCuentasPorCobrar($refacciones);
                }

            }
        }
        catch(\Exception $e){
            Log::emergency("Se Procesaron $cont");
            Log::warning($e->getMessage());
            return response()->json(['message' => 'Ocurrio un error en la carga de clientes.'], 400);
        }

        // Borramos el archivo procesado
        unlink($file_path);

        return response()->json(['message' => 'La carga de cuentas se realizo correctamente.']);
    }
}
