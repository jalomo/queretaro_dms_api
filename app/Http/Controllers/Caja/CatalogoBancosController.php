<?php

namespace App\Http\Controllers\Caja;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Caja\ServicioCatalogoBancos;

class CatalogoBancosController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioCatalogoBancos();
    }

}
