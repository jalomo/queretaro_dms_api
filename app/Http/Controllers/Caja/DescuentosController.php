<?php

namespace App\Http\Controllers\Caja;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Caja\ServicioDescuentos;
use App\Servicios\CuentasPorCobrar\ServicioAsientos;
use App\Servicios\CuentasPorCobrar\ServicioCuentaPorCobrar;
use App\Servicios\Core\Respuestas\Respuesta;
use Illuminate\Http\Request;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Exceptions\ParametroHttpInvalidoException;
use DB;

class DescuentosController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioDescuentos();
        $this->servicio_cuenta_por_cobrar = new ServicioCuentaPorCobrar();
        $this->servicio_asientos = new ServicioAsientos();
    }


    public function getByDescuentoByFolioId(int $id)
    {
        try {
            return Respuesta::json($this->servicio->getByFolioId($id), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getByDescuentoByFolioTipo(Request $request)
    {
        try {
            return Respuesta::json($this->servicio->getByFolioTipo($request), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getByTotalByFolioId(int $id)
    {
        try {
            $respuesta = $this->servicio->getByTotalByFolioId($id);
            $response['total'] = $respuesta ? $respuesta->sumatotal : 0;
            return Respuesta::json($response, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function aplicarDescuento(Request $request, $id)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasAplicarFolio());
            $mensaje = __(static::$I0004_RESOURCE_UPDATED, ['parametro' => $this->servicio->getRecurso()]);
            $modelo = $this->servicio->aplicarDescuento($request, $id);
            return Respuesta::json($modelo, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $respuesta = [];
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
            $respuesta = $this->servicio->store($request);
            $cuenta_por_cobrar = $this->servicio_cuenta_por_cobrar->getByFolioId($request->get('folio_id'));
            $actualizaPoliza = $this->servicio_asientos->servicioActualizaPoliza($cuenta_por_cobrar->id, $request->get('tipo_descuento'));
            if (!$actualizaPoliza) {
                DB::rollback();
                throw new ParametroHttpInvalidoException([
                    'msg' => "Error al actualizar la poliza"
                ]);
            } else {
                $aplicarDescuento = $this->servicio->aplicarDescuento($request);
                if (!$aplicarDescuento) {
                    throw new ParametroHttpInvalidoException([
                        'msg' => "Error al aplicar el descuento"
                    ]);
                    DB::rollback();
                }

                $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
                DB::commit();
                return Respuesta::json($respuesta, 201, $mensaje);
            }
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }
}
