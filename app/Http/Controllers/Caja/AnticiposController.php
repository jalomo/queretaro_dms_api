<?php

namespace App\Http\Controllers\Caja;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Caja\ServicioAnticipos;
use App\Servicios\Refacciones\ServicioFolios;
use App\Servicios\CuentasPorCobrar\ServicioAsientos;
use App\Models\Contabilidad\CatalogoProcesosModel;
use App\Servicios\Core\Respuestas\Respuesta;
use Illuminate\Http\Request;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Refacciones\ServicioCurl;
class AnticiposController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioAnticipos();
        $this->servicio_folios = new ServicioFolios();
        $this->servicioCurl = new ServicioCurl();
        $this->servicioAsientos = new ServicioAsientos();
    }

    public function store(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
            $folio = $this->servicio_folios->generarFolio(CatalogoProcesosModel::PROCESO_ANTICIPO);

            if ($folio) {
                $request->merge(['folio_id' => $folio->id]);
                $modelo = $this->servicio->store($request);
                if ($modelo) {
                    if ($modelo->tipo_proceso_id == 1) {
                        $this->servicio->polizaVentaMostradorCaja($request);
                    } else if ($modelo->tipo_proceso_id == 8) {
                        $this->servicio->polizaServiciosCaja($request);
                    }
                }
            }
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($modelo, 201, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getFilter(Request $request)
    {
        try {
            return Respuesta::json($this->servicio->getAll($request), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getByAnticipoId(int $id)
    {
        try {
            return Respuesta::json($this->servicio->getById($id), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getByAnticipoByFolioId(int $id)
    {
        try {
            return Respuesta::json($this->servicio->getByFolioId($id), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getByTotalByFolioId(int $id)
    {
        try {
            $respuesta = $this->servicio->getByTotalByFolioId($id);
            $response['total'] = $respuesta ? $respuesta->sumatotal : 0;
            return Respuesta::json($response, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function updateEstatus(Request $request, $id)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasEstatus());
            $mensaje = __(static::$I0004_RESOURCE_UPDATED, ['parametro' => $this->servicio->getRecurso()]);
            $modelo = $this->servicio->changeEstatus($request, $id);
            if ($modelo) {
                $anticipo = $this->servicio->getById($id);
                $contabilidad_api = $this->servicioCurl->curlPost(env('URL_CONTABILIDAD') . 'asientos/api/aplicar_folio', [
                    'folio' => $anticipo->folio_id,
                    'estatus' => 'ANULADO',
                ], false);

                $respuesta = json_decode($contabilidad_api);
                if ($respuesta) {
                    $mensaje = __(static::$CUENTA_CANCELADA, ['recurso' => $this->servicio->getRecurso()]);
                    return Respuesta::json($modelo, 200, $mensaje);
                }
            }
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function aplicarAnticipo(Request $request, $id)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasAplicarFolio());
            $mensaje = __(static::$I0004_RESOURCE_UPDATED, ['parametro' => $this->servicio->getRecurso()]);
            $modelo = $this->servicio->aplicarAnticipo($request, $id);
            $this->servicioAsientos->servicioPolizaAnticipo($modelo->folio_id);
            return Respuesta::json($modelo, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
