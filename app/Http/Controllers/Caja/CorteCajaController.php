<?php

namespace App\Http\Controllers\Caja;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Caja\ServicioCorteCaja;
use App\Servicios\Core\Respuestas\Respuesta;
use Illuminate\Http\Request;

class CorteCajaController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioCorteCaja();
    }

    public function getPolizas(Request $request)
    {
        $modelo = $this->servicio->getPolizas($request->toArray());
        return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
    }
    
    public function getByUsuario(Request $request)
    {
        $modelo = $this->servicio->getByUsuario($request->toArray());
        return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
    }

    public function mostrarPorUsuarioAndFecha(Request $request)
    {   
        $modelo = $this->servicio->getByUsuarioFecha($request->toArray());
        return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
    }

}
