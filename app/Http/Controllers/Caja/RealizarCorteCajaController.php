<?php

namespace App\Http\Controllers\Caja;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Caja\ServicioRealizarCorteCaja;
use App\Servicios\Core\Respuestas\Respuesta;
use Illuminate\Http\Request;
class RealizarCorteCajaController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioRealizarCorteCaja();
    }

    public function getFilter(Request $request)
    {
        try {
            return Respuesta::json($this->servicio->getFilter($request), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

}
