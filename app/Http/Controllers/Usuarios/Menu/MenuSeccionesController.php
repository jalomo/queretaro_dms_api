<?php

namespace App\Http\Controllers\Usuarios\Menu;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Usuarios\Menu\ServicioSeccionesMenu;
use Illuminate\Http\Request;

class MenuSeccionesController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioSeccionesMenu();
    }

    public function getCatalogo()
    {
        try {
            $menu = $this->servicio->getCatalogo();
            return Respuesta::json($menu, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
    public function getSeccionesByModulo(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasSecciones());
            $menu = $this->servicio->getSecciones($request->all());
            return Respuesta::json($menu, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getSeccionesByRol(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, ['rol_id' => 'required']);
            $menu = $this->servicio->getSeccionesByRol($request->all());
            return Respuesta::json($menu, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function storeSeccionesByRole(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, [
                'seccion_id' => 'required|array',
                'rol_id' => 'required|numeric'
            ]);
            return Respuesta::json($this->servicio->storeSeccionesByRole($request->all()), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
