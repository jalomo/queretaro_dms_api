<?php

namespace App\Http\Controllers\Usuarios\Menu;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Models\Usuarios\UsuariosModulosModel;
use App\Servicios\Usuarios\Menu\ServicioModulos;
use App\Servicios\Usuarios\Menu\ServicioUsuariosModulos;
use Illuminate\Http\Request;

class ModulosMenuController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioModulos();
        $this->servicioUsuariosModulos = new ServicioUsuariosModulos();
    }

    public function getModulosByUserID($id_usuario)
    {
        try {
            return Respuesta::json($this->servicioUsuariosModulos->usuariosAndModulos([
                UsuariosModulosModel::USUARIO_ID => $id_usuario
            ]), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
