<?php

namespace App\Http\Controllers\Usuarios\Menu;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Usuarios\Menu\ServicioMenu;
use Illuminate\Http\Request;
use App\Exceptions\ParametroHttpInvalidoException;

class MenuController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioMenu();
    }

    public function menu(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasMenu());
            $menu = $this->servicio->getmenu($request->all());
            return Respuesta::json($menu, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
    public function permisos(Request $request)
    {
        try {
            return Respuesta::json($this->servicio->getPermisosMenu($request), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function insertPermiso(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
            $modelo = $this->servicio->createPermisosRol($request);
            if ($modelo) {
                return Respuesta::json($modelo, 201, 'Permiso creado correctamente');
            } else {
                throw new ParametroHttpInvalidoException([
                    'msg' => "Ocurrio un error al crear el permiso"
                ]);
            }
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function deletePermiso(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
            $verifica = $this->servicio->getPermisosMenu($request);
            if ($verifica) {
                $modelo = $this->servicio->deletePermisoRol($request);
                if ($modelo) {
                    return Respuesta::json($modelo, 200, 'Permiso eliminado correctamente');
                } else {
                    throw new ParametroHttpInvalidoException([
                        'msg' => "Ocurrio un error al eliminar el permiso"
                    ]);
                }
            } else {
                throw new ParametroHttpInvalidoException([
                    'msg' => "No éxiste el permiso para el rol indicado."
                ]);
            }
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
