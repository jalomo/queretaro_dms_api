<?php

namespace App\Http\Controllers\Auditoria;

use App\Http\Controllers\Core\Controller;
use App\Http\Requests\Auditoria\AplicarAjustesRequest;
use App\Models\Auditoria\Auditoria;
use App\Http\Requests\Auditoria\StoreAuditoriaRequest;
use App\Http\Requests\Auditoria\UpdateAuditoriaRequest;
use App\Http\Resources\DataTablesResource;
use App\Models\Usuarios\User;
use App\Servicios\Asientos\Ajustes\Abono;
use App\Servicios\Asientos\Ajustes\Cargo;
use App\Servicios\Inventario\AjusteAumentoService;
use App\Servicios\Inventario\AjusteDisminucionService;
use Exception;
use Illuminate\Support\Facades\DB;

class AuditoriaController extends Controller
{
    /**
     * Devolvemos el listado de las auditorias
     *
     * @return void
     */
    public function index()
    {
        $auditorias = Auditoria::with('usuario')->latest()->paginate();

        return response()->json(new DataTablesResource($auditorias));
    }

    public function activa()
    {
        $existen_auditorias = Auditoria::whereNull('terminada')->count();

        return response()->json(['auditorias' => $existen_auditorias ]);
    }

    public function show(Auditoria $auditoria)
    {
        $auditoria = $auditoria->load('productos', 'usuario');

        return response()->json($auditoria);
    }

    public function store(StoreAuditoriaRequest $request)
    {
        $this->authorize('create', Auditoria::class);

        $user = User::where($request->only('usuario', 'password'))->first();

        if($user == null)
        {
            return response()->json(['message' => __('Usuario o Contraseña no son correctos.')], 422);
        }

        $auditoria = new Auditoria(['user_id' => $user->id, 'inicio' => now() ]);

        if($auditoria->save())
        {
            return response()->json(['id' => $auditoria->id, 'message' => __('Auditoria Creada')], 201);
        }
        
        return response()->json(['message' => __('Ocurrio un error al crear la auditoria.')], 400);
    }

    public function update(UpdateAuditoriaRequest $request, Auditoria $auditoria)
    {
        if($auditoria->update($request->validated()))
        {
            return response()->json(['id' => $auditoria->id, 'message' => __('Auditoria Actualizada')], 200);
        }
        
        return response()->json(['message' => __('Ocurrio un error al actualizar la auditoria.')], 400);
    }

    public function completar(Auditoria $auditoria)
    {
        $this->authorize('completar', $auditoria);

        $auditoria->terminada = now();

        if($auditoria->save())
        {
            return response()->json(['id' => $auditoria->id, 'message' => __('Auditoria finalizada correctamente')], 200);
        }

        return response()->json([ 'message' => __('Ocurrio un error al completar la auditoria.')]);
    }

    public function cancelar(Auditoria $auditoria)
    {
        $this->authorize('cancelar', $auditoria);

        $auditoria->cancelada = now();
        $auditoria->terminada = now();

        if($auditoria->save())
        {
            return response()->json(['id' => $auditoria->id, 'message' => __('Auditoria cancelada correctamente')], 200);
        }

        return response()->json(['id' => $auditoria->id, 'message' => __('Auditoria no se cancelo')], 400);
    }

    public function destroy(Auditoria $auditoria)
    {
        $this->authorize('delete', $auditoria);

        if($auditoria->delete())
        {
            return response()->json(['id' => $auditoria->id, 'message' => 'Auditoria Eliminada'], 201);
        }
        
        return response()->json(['message' => __('Ocurrio un error al eliminar la auditoria.')], 400);
    }

    public function aplicar_ajustes(AplicarAjustesRequest $request, Auditoria $auditoria)
    {
        $this->authorize('aplicar_ajustes', $auditoria);

        $user = User::where($request->only('usuario', 'password'))->first();

        if($user == null)
        {
            return response()->json(['message' => __('Usuario o Contraseña no son correctos.')], 422);
        }

        $refacciones = $auditoria->productos()->get();

        try
        {
            DB::beginTransaction();

            $refacciones->each(function($refaccion){
                
                if($refaccion->auditoria->existencia_fisico > $refaccion->auditoria->existencia_sistema)
                {
                    // Realizamos el aumento de la existencia si lo que hay en fisico
                    // es mayor a lo que hay registrado en el sistema
                    $ajuste = new AjusteAumentoService;
                    $ajuste->id($refaccion->auditoria->id);
                    $ajuste->handle(
                        $refaccion->auditoria->producto_id, 
                        $refaccion->auditoria->existencia_fisico - $refaccion->auditoria->existencia_sistema
                    );

                    //Aplicar ajustes de contabilidad
                    $total = ($refaccion->auditoria->existencia_fisico - $refaccion->auditoria->existencia_sistema) * $refaccion->costo_promedio;
                    $contabilidad = new Abono($refaccion);
                    $contabilidad->make($total);

                    $contabilidad = new Cargo($refaccion);
                    $contabilidad->make($total);
                }
    
                if($refaccion->auditoria->existencia_sistema > $refaccion->auditoria->existencia_fisico)
                {
                    // Realizamos la disminución del inventario si lo que hay en fisico
                    // es menor a lo que hay registrado en el sistema
                    $ajuste = new AjusteDisminucionService;
                    $ajuste->id($refaccion->auditoria->id);
                    $ajuste->handle(
                        $refaccion->auditoria->producto_id, 
                        $refaccion->auditoria->existencia_sistema - $refaccion->auditoria->existencia_fisico
                    );

                    //Aplicar ajustes de contabilidad
                    $total = ($refaccion->auditoria->existencia_sistema - $refaccion->auditoria->existencia_fisico) * $refaccion->costo_promedio;
                    $contabilidad = new Abono($refaccion);
                    $contabilidad->make($total);

                    $contabilidad = new Cargo($refaccion);
                    $contabilidad->make($total);
                }
    
            });

            $auditoria->aprobada_por = $user->id;
            $auditoria->save();

            DB::commit();

            return response()->json(['message' => __('Se aplicaron los ajustes necesarios a las cuentas y el inventario.')]);
        } 
        catch(Exception $exception)
        {
            DB::rollBack();

            logger()->warning($exception->getMessage(), [
                'file' => $exception->getFile(),
                'line' => $exception->getLine(),
                'code' => $exception->getCode(),
            ]);

            return response()->json(['message' => __('Ocurrio un error al aplicar los ajustes al inventario.')], 400);
        }

    }
}
