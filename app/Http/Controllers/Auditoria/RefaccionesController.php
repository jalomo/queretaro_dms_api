<?php

namespace App\Http\Controllers\Auditoria;

use App\Http\Controllers\Core\Controller;
use App\Http\Requests\Auditoria\StoreAuditoriaRefaccionRequest;
use App\Http\Requests\Auditoria\UpdateAuditoriaRefaccionRequest;
use App\Models\Auditoria\Auditoria;
use App\Models\Auditoria\AuditoriaProducto;
use App\Models\Refacciones\InventarioProductos;
use App\Models\Refacciones\ProductosModel;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RefaccionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $productos = ProductosModel::when($request->no_identificacion, function($q) use ($request){
            $q->where('no_identificacion', 'like', "%$request->no_identificacion%");
        })
        ->limit(20)
        ->get();

        return response()->json($productos);
    }

    public function cargar_refacciones(Auditoria $auditoria)
    {
        $this->authorize('cargar_refacciones', $auditoria);

        if($auditoria->terminada != null)
        {
            return response()->json(['message' => __('Auditoria se encuentra terminada.')], 400);
        }

        $refacciones = InventarioProductos::with('producto.ubicacion', 'producto.desglose_producto')->groupBy('producto_id')->select('producto_id')->get();

        try
        {
            DB::beginTransaction();

            $refacciones->each(function($refaccion) use ($auditoria){
                $auditoria->productos()->attach($refaccion->producto->id, [
                    'existencia_sistema' => $refaccion->producto->desglose_producto->cantidad_actual,
                    'existencia_fisico' => 0, // Aun no se realizo la auditoria a el inventario,
                    'ubicacion' => $refaccion->producto->ubicacion->nombre?? 'GEN.',
                ]);
            });

            DB::commit();
            
            return response()->json(['message' => __('Se cargaron las refacciones a la auditoria.') ]);
        } 
        catch (Exception $e)
        {
            DB::rollBack();

            return response()->json(['message' => __('Ocurrio un error al cargar las refacciones a la auditoria.')], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAuditoriaRefaccionRequest $request, Auditoria $auditoria)
    {
        $this->authorize('create', [AuditoriaProducto::class, $auditoria]);

        $producto = ProductosModel::find($request->producto_id);
        //
        $refaccion = new AuditoriaProducto($request->validated() + [ 
            'auditoria_id' => $auditoria->id, 
            'existencia_sistema' => optional($producto->desglose_producto)->cantidad_actual?? 0,
        ]);

        if($refaccion->save())
        {
            return response()->json(['id' => $refaccion->id, 'message' => __('Refaccion guardada en la auditoria.')], 201);
        }

        return response()->json(['message' => __('Refaccion no se guardó en la auditoria.')], 400);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAuditoriaRefaccionRequest $request, AuditoriaProducto $refaccion)
    {
        $this->authorize('update', $refaccion);
        
        if($refaccion->update($request->validated()))
        {
            return response()->json(['id' => $refaccion->id, 'message' => __('Refaccion actualizada en la auditoria.')]);
        }

        return response()->json(['message' => __('Refaccion no se actualizó en la auditoria.')], 400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(AuditoriaProducto $refaccion)
    {
        $this->authorize('delete', $refaccion);

        //
        if($refaccion->delete())
        {
            return response()->json(['id' => $refaccion->id, 'message' => __('Refaccion eliminada de la auditoria.')]);
        }

        return response()->json(['message' => __('Refaccion no se elimino.')], 400);
    }
}
