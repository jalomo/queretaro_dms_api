<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Refacciones\ServicioMaProductoPedido;
use Illuminate\Http\Request;

class MaProductoPedidoController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioMaProductoPedido();
    }

    public function index()
    {
        try {
            $modelo = $this->servicio->buscarTodos();
            return Respuesta::json(['data' => $modelo], empty($modelo) ? 404 : 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function store(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
            $modelo = $this->servicio->validarActivoOrStore($request->all());
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($modelo, 201, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function filtrarPedidos(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, ['estatus_id'=>'nullable']);
            $modelo = $this->servicio->createQuery($request->all());
            return Respuesta::json(['data' => $modelo], empty($modelo) ? 404 : 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
