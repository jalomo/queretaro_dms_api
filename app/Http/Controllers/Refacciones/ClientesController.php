<?php

namespace App\Http\Controllers\Refacciones;

use Illuminate\Http\Request;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Refacciones\ServicioClientes;
use App\Servicios\CuentasPorCobrar\ServicioCuentaPorCobrar;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Refacciones\ServicioCatalogoClaveCliente;
use App\Models\Refacciones\ClientesModel;
use App\Http\Resources\DataTablesResource;
use App\Exceptions\ParametroHttpInvalidoException;

class ClientesController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioClientes();
        $this->servicioCatalogoClaveCliente = new ServicioCatalogoClaveCliente();
        $this->servicioCuentasxCobrar = new ServicioCuentaPorCobrar();
    }

    public function getClientes(Request $request)
    {
        try {
            $modelo = $this->servicio->getBusqueda($request);
            return response()->json(
                new DataTablesResource($modelo)
            );
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function selectCatalogo(Request $request)
    {
        try {
            $modelo = $this->servicio->getCatalogo($request);
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function show($id)
    {
        try {
            $modelo = $this->servicio->getOneCliente($id);
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getLastRecord()
    {
        try {
            $modelo = $this->servicio->lastRecord();
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    #TODO: REVISAR CONSULTAS - MEJORAR
    public function searchNombreCliente(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasBusquedaNombre());
            $modelo = $this->servicio->searchNombreCliente($request->all());
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getClienteByclave(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasClaveCliente());
            $modelo = $this->servicio->clientePorClave($request->all());
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function searchCliente(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasBusquedaCliente());
            $modelo = $this->servicio->searchCliente($request);
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function tieneCredito(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasClienteId());
            $modelo = $this->servicio->tieneCredito($request);
            $cuenta_actual = $this->servicioCuentasxCobrar->getCuentasPendientesPorClienteId($request->get(ClientesModel::ID));
            $modelo->credito_actual = $modelo->limite_credito - $cuenta_actual;
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function searchNumeroCliente(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasCliente());
            $modelo = $this->servicio->searchNumeroCliente($request->all());
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function actualizarAplicaCredito(Request $request, $id)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasUpdateAplicaCredito());
            $data = [
                ClientesModel::APLICA_CREDITO => $request->aplica_credito,
                ClientesModel::PLAZO_CREDITO_ID => $request->plazo_credito_id,
                ClientesModel::LIMITE_CREDITO => $request->limite_credito
            ];
            $mensaje = __(static::$I0004_RESOURCE_UPDATED, ['parametro' => $this->servicio->getRecurso()]);

            $modelo = $this->servicio->updateAplicaCredito($data, $id);
            return Respuesta::json($modelo, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function actualizarPlazoCredito(Request $request, $id)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasUpdatePlazoCredito());
            $data = [
                ClientesModel::PLAZO_CREDITO_ID => $request->plazo_credito_id,
                ClientesModel::LIMITE_CREDITO => $request->limite_credito,
                ClientesModel::APLICA_CREDITO => true,
            ];
            $mensaje = __(static::$I0004_RESOURCE_UPDATED, ['parametro' => $this->servicio->getRecurso()]);

            $modelo = $this->servicio->updatePlazoCredito($data, $id);
            return Respuesta::json($modelo, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function crearRegresarCliente(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, [
                ClientesModel::NUMERO_CLIENTE => 'nullable',
                ClientesModel::TIPO_REGISTRO => 'nullable',
                ClientesModel::REGIMEN_FISCAL => 'nullable',
                ClientesModel::NOMBRE_EMPRESA => 'nullable',
                ClientesModel::NOMBRE => 'required',
                ClientesModel::APELLIDO_MATERNO => 'nullable',
                ClientesModel::APELLIDO_PATERNO => 'required',
                ClientesModel::RFC => 'required',
                ClientesModel::CORREO_ELECTRONICO => 'nullable|email',
                ClientesModel::DIRECCION => 'nullable',
                ClientesModel::NUMERO_EXT => 'nullable',
                ClientesModel::NUMERO_INT => 'nullable',
                ClientesModel::COLONIA => 'nullable',
                ClientesModel::MUNICIPIO => 'nullable',
                ClientesModel::ESTADO => 'nullable',
                ClientesModel::CODIGO_POSTAL => 'nullable',

            ]);

            $modelo = $this->servicio->createOrUpdate(
                [
                    ClientesModel::NUMERO_CLIENTE => $request->numero_cliente,
                    ClientesModel::TIPO_REGISTRO => $request->tipo_registro,
                    ClientesModel::REGIMEN_FISCAL => $request->regimen_fiscal,
                    ClientesModel::NOMBRE_EMPRESA => $request->nombre_empresa,
                    ClientesModel::NOMBRE => $request->nombre,
                    ClientesModel::APELLIDO_PATERNO => $request->apellido_paterno,
                    ClientesModel::APELLIDO_MATERNO => $request->apellido_materno,
                    ClientesModel::CORREO_ELECTRONICO => $request->correo_electronico,
                    ClientesModel::RFC => $request->rfc,
                    ClientesModel::DIRECCION => $request->direccion
                ],
                $request->all()
            );
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function updateDatosCliente(int $cliente_id, Request $request)
    {
        try {
            ParametrosHttpValidador::validar_array([
                ClientesModel::ID => $cliente_id,
                ClientesModel::TIPO_REGISTRO => $request->get(ClientesModel::TIPO_REGISTRO),
                ClientesModel::REGIMEN_FISCAL => $request->get(ClientesModel::REGIMEN_FISCAL),
                ClientesModel::NOMBRE_EMPRESA => $request->get(ClientesModel::NOMBRE_EMPRESA),
                ClientesModel::RFC => $request->get(ClientesModel::RFC),
                ClientesModel::DIRECCION => $request->get(ClientesModel::DIRECCION),
            ], $this->servicio->getReglasUpdateClienteDatos());
            $modelo = $this->servicio->massUpdateWhereId(ClientesModel::ID, $cliente_id, $request->except(['cliente_id']));
            $mensaje = __(static::$I0004_RESOURCE_UPDATED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($modelo, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getInformacionCliente(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasClienteId());
            $modelo = $this->servicio->tieneCredito($request);
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function createClienteModal(Request $request)
    {
        try {
            $max_id = $this->servicio->getNextId();
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasClienteModal());
            // if ($request->get(ClientesModel::RFC) != "XAXX010101000") {
            //     ParametrosHttpValidador::validar_array([
            //         ClientesModel::RFC => $request->get(ClientesModel::RFC),
            //     ], $this->servicio->getReglasClienteValidaRFC());
            // } else {
                if ($this->servicio->getValidaCliente($request)) {
                    throw new ParametroHttpInvalidoException([
						'mensaje' => 'El cliente ya se encuentra registrado'
					]);
                }
            //}
            $tipo_clave = $this->servicioCatalogoClaveCliente->getById($request->get(ClientesModel::TIPO_REGISTRO));
            //if ($tipo_clave && isset($tipo_clave->clave))
            $request->merge([ClientesModel::NUMERO_CLIENTE => $tipo_clave->clave . $max_id]);
            $modelo = $this->servicio->crear($request->all());
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
    public function updateClienteModal(int $id, Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasClienteModalUpdate());
            if ($request->get(ClientesModel::RFC) != "XAXX010101000") {
                ParametrosHttpValidador::validar_array([
                    ClientesModel::RFC => $request->get(ClientesModel::RFC),
                ], $this->servicio->getReglasClienteValidaRFCEdit($id));
            } else {
            }
            $modelo = $this->servicio->massUpdateWhereId(ClientesModel::ID, $id, $request->toArray());
            if ($modelo) {
                $response = $this->servicio->getById($id);
            }
            return Respuesta::json($response, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getByRFC(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasPorRFC());

            $modelo = $this->servicio->getWhereInFirst(ClientesModel::RFC, [$request->get(ClientesModel::RFC)]);
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function creditoByRFC(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasPorRFC());
            $modelo = $this->servicio->getCreditoByRFC($request);
            $cuenta_actual = $this->servicioCuentasxCobrar->getCuentasPendientesPorClienteId($request->get(ClientesModel::ID));
            $modelo->credito_actual = $modelo->limite_credito - $cuenta_actual;
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
