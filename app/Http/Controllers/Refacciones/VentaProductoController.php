<?php

namespace App\Http\Controllers\Refacciones;

use App\Events\DeleteRefaccionDetalleEvent;
use App\Events\StoreRefaccionDetalleEvent;
use App\Events\UpdateRefaccionDetalleEvent;
use App\Http\Controllers\Core\CrudController;
use App\Http\Requests\Refacciones\VentaDeletePresupuestoRequest;
use App\Http\Requests\Refacciones\VentaUpdatePresupuestoRequest;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\VentaProductoModel;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Refacciones\ServicioCurl;
use App\Servicios\Refacciones\ServicioVentaProducto;
use Illuminate\Http\Request;
use App\Http\Requests\Refacciones\VentaUpdateRequest;
use App\Servicios\Refacciones\ServicioVentasServicios;
use Illuminate\Support\Facades\Log;
use App\Http\Resources\DataTablesResource;
use App\Servicios\Usuarios\ServicioUsuarios;
use Exception;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;

class VentaProductoController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioVentaProducto();
        $this->servicioCurl = new ServicioCurl();
        $this->productosModel = new ProductosModel();
        $this->servicioVentasServicios = new ServicioVentasServicios;
        $this->servicioUsuario = new ServicioUsuarios;
    }

    public function detalleVenta()
    {
        try {
            $data = $this->servicio->getDetalleVenta();
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($data, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getDetalleVentaByFolio($folio_id)
    {
        try {
            $data = $this->servicio->getDetalleVentaByFolio($folio_id);
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getDetalleVentaId($id)
    {
        try {
            $data = $this->servicio->geDetalleVentaById($id);
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function totalVentaByFolio($folio_id)
    {
        try {
            $data = $this->servicio->totalVentaByFolio($folio_id);
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($data, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function store(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
            $data = $this->servicio->store($request);
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            dd($e);
            return Respuesta::error($e);
        }
    }

    public function storeVentaDetalle(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
            $venta_producto = $this->servicio->storeVentaDetalle($request);

            event(new StoreRefaccionDetalleEvent($venta_producto));

            return Respuesta::json($venta_producto, 200);
        } catch (\Throwable $e) {

            Log::warning($e->getMessage() . $e->getFile() . $e->getLine());
            return Respuesta::error($e);
        }
    }

    /**
     * Llama al servicio updateVenta
     *
     * @param VentaUpdateRequest $request
     * @param int $id
     * @return \Illuminate\Http\Response 
     */
    public function updateVenta(VentaUpdateRequest $request, $id)
    {
        try {
            $venta_producto = $this->servicio->updateVenta($request->validated(), $id);

            event(new UpdateRefaccionDetalleEvent($venta_producto));

            return response()->json($venta_producto);

        } catch (\Throwable $e) {
            return response()->json($e->getMessage());
        }
    }

    /**
     * Llama al servicio updateVentaPresupuesto
     *
     * @param VentaUpdateRequest $request
     * @param int $id
     * @return \Illuminate\Http\Response 
     */
    public function updateVentaPresupuesto(VentaUpdatePresupuestoRequest $request, $id)
    {
        try {
            if(! $user = $this->servicioUsuario->existeUsuario(['usuario' => $request->usuario, 'password' => $request->password]))
            {
                throw new Exception('Usuario o contraseña incorrectas.');
            }

            return response()->json(
                $this->servicio->updateVenta($request->validated(), $id, $user)
            );
        } 
        catch (\Throwable $e) 
        {
            throw new HttpResponseException(response()->json(
                ['message' => $e->getMessage()], Response::HTTP_BAD_REQUEST, 
                ['X-Message' => json_encode(['message' => $e->getMessage()])]
            ));
        }
    }

    public function validaCambioPreciosApartado(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, ['venta_id' => 'required']);
            $productos_venta = $this->servicio->geDetalleVentaById($request->venta_id)->toArray();
            $rel_precio_productos = [];
            foreach ($productos_venta as $key => $venta) {
                $producto = $this->productosModel->where(ProductosModel::ID, $venta->producto_id)->first();

                if ($producto->valor_unitario > $venta->valor_unitario) {
                    $rel_precio_productos[$key] = [
                        'no_identificacion' => $producto->no_identificacion,
                        'valor_actual' => $venta->valor_unitario,
                        'valor_actualizado' => $producto->valor_unitario
                    ];
                }
            }

            return Respuesta::json($rel_precio_productos, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    //Actualiza precios despues de apartarlos
    public function actualizarProductosVenta(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, ['venta_id' => 'required']);
            $productos_venta = $this->servicio->geDetalleVentaById($request->venta_id)->toArray();
            $rel_precio_productos = [];
            foreach ($productos_venta as $key => $venta) {
                // $producto = $this->productosModel->where(ProductosModel::ID, $venta->producto_id)->first();
                // $venta_total = $producto->venta_total * $venta->cantidad;
                // $this->servicio->massUpdateWhereId(VentaProductoModel::ID, $request->venta_id, [
                //     VentaProductoModel::TOTAL_VENTA => 1
                // ]);
                // $producto = $this->productosModel->where(ProductosModel::ID, $venta->producto_id)->first();
                // $this->servicioProductos->mass
                // if ($producto->valor_unitario > $venta->valor_unitario) {
                //     $rel_precio_productos[$key] = [
                //         'no_identificacion' => $producto->no_identificacion,
                //         'valor_actual' => $venta->valor_unitario,
                //         'valor_actualizado' => $producto->valor_unitario
                //     ];
                // }
            }

            return Respuesta::json($rel_precio_productos, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function deleteEliminarVentaProducto($id, Request $request)
    {
        try {
            $venta_producto = $this->servicio->getIdVentaProducto($id, $request);
            if ($venta_producto) {
                $this->servicio->eliminar($venta_producto->id);
            }
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function deleteRefaccionPresupuesto(VentaDeletePresupuestoRequest $request, VentaProductoModel $refaccion)
    {
        if(! $user = $this->servicioUsuario->existeUsuario(['usuario' => $request->usuario, 'password' => $request->password]))
        {
            return response()->json(['message' => __('Usuario o contraseña incorrectas.')], 400);
        }

        $refaccion->estatus_producto_orden = VentaProductoModel::ESTATUS_PRODUCTO_ORDEN_B;

        //! Falta cambiar por variable env
        $response = Http::asForm()->post('https://mylsa.iplaneacion.com/servicios/queretaro/citas/citas_queretaro/enlaces/dms/baja_refaccion', [
            'id_refaccion' => $refaccion->id_index_requisicion,
            'id_cita' => $refaccion->venta->numero_orden,
            'usuario_actualiza' => $user->full_name,
        ]);

        if($response->successful() && $response->object()->status == "ok" && $refaccion->save())
        {
            event(new DeleteRefaccionDetalleEvent($refaccion));

            logger()->info("ELIMINAR REFACCION PRESUPUESTO {$refaccion->venta->numero_orden} {$refaccion->producto->no_identificacion}", [ 
                'usuario' => auth()->user()->full_name,
                'credenciales' => $user->full_name,
            ]);

            return response()->json(['message' => __('Refaccion eliminada correctamente.')]);
        }

        // Se guarda en el log el error
        logger()->warning('ELIMINAR REFACCION PRESUPUESTO: '. $refaccion->venta->numero_orden, (array) $response->object());

        return response()->json(['message' => __('Error al eliminar la refaccion.')], 400);
    }

    public function deleteMpmitem($id, Request $request)
    {
        try {
            $venta_producto = VentaProductoModel::find($id);

            ParametrosHttpValidador::validar($request, ['id_cita' => 'required', 'id_indice' => 'required']);
            $this->servicio->massUpdateWhereId(VentaProductoModel::ID, $id, [
                VentaProductoModel::ESTATUS_PRODUCTO_ORDEN => VentaProductoModel::ESTATUS_PRODUCTO_ORDEN_B
            ]);

            $this->servicio->deletePiezas($venta_producto->venta, $request->id_indice, $venta_producto);
            //! No Borrar esto mantiene la consistencia del total entre productos y servicios

            event(new DeleteRefaccionDetalleEvent($venta_producto));

            return response()->json([
                'message' => 'Refacción se elimino correctamente.'
            ]);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getEreactVentas(Request $request)
    {
        try {
            $request->paginate = true;
            $modelo = $this->servicio->getEreactVentas($request);
            return Respuesta::json(new DataTablesResource($modelo), 200);

                
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function generateEreactVentasFile(Request $request)
    {
        $modelo = $this->servicio->getEreactVentas($request);
        return Respuesta::json($this->servicio->handleTxtFile($modelo), 200);
    }
}
