<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\CuentasPorPagar\ServicioProveedorPedido;
use App\Servicios\Refacciones\ServicioProveedor;
use App\Models\Refacciones\ProveedorRefacciones;
use Illuminate\Http\Request;

class ProveedorRefaccionesController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioProveedor();
        $this->servicioProveedorPedido = new ServicioProveedorPedido();
    }

    public function index()
    {
        try {
            $modelo = $this->servicio->getAll();
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function show($id)
    {
        try {
            $modelo = $this->servicio->getOneProveedor($id);
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getProveedorByRFC(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasRFC());
            $modelo = $this->servicio->getWhereInFirst(ProveedorRefacciones::PROVEEDOR_RFC, [$request->get(ProveedorRefacciones::PROVEEDOR_RFC)]);
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getPedidosByProveedor(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, ['proveedor_id' => 'nullable']);
            if ($request->get('proveedor_id') == '') {
                return Respuesta::json(['data' => []], 200);
            }
            $data = $this->servicioProveedorPedido->polizaspedidoproveedor($request->all());
            return Respuesta::json(['data' => $data], 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
