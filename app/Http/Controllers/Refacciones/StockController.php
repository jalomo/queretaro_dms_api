<?php

namespace App\Http\Controllers\Refacciones;

use Illuminate\Http\Request;
use App\Http\Controllers\Core\Controller;
use App\Http\Requests\Refacciones\StockRequest;
use App\Servicios\Refacciones\ServicioStockUpload;

class StockController extends Controller
{
    protected $servicio;

    public function __construct(ServicioStockUpload $servicio)
    {
        $this->servicio = $servicio;
    }

    public function index()
    {
        return response()->json($this->servicio->getAll());
    }

    //
    public function csv(StockRequest $request)
    {
        $data = $this->servicio->upload($request);

        return response()->json($data);
    }

    public function text(StockRequest $request)
    {
        $data = $this->servicio->txt($request);

        return response()->json($data);
    }

    public function clientes(StockRequest $request)
    {
        $data = $this->servicio->clientes($request);

        return response()->json($data);
    }
}
