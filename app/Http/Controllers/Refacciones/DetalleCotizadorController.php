<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Refacciones\ServicioDetalleCotizador;
use Illuminate\Http\Request;

class DetalleCotizadorController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioDetalleCotizador();
    }
    public function getByIdCotizacion(Request $request)
    {
        return $this->servicio->getWhere('cotizador_id', $request->get('cotizador_id'));
    }
}
