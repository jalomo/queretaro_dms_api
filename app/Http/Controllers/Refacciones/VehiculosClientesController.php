<?php

namespace App\Http\Controllers\Refacciones;

use Illuminate\Http\Request;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Refacciones\ServicioClienteVehiculos;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;


class VehiculosClientesController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioClienteVehiculos();
    } 

    public function getVehiculosByClienteId($cliente_id)
    {
        try {
            return Respuesta::json($this->servicio->vehiculoByClienteId($cliente_id), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function searchSerieCliente(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasBusquedaSerie());
            $modelo = $this->servicio->searchVehiculoCliente($request->all()); 
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
