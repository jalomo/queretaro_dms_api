<?php

namespace App\Http\Controllers\Refacciones;

use App\Models\Autos\HistoricoProductosServicioModel;
use App\Models\CuentasPorCobrar\TipoFormaPagoModel;
use App\Models\Refacciones\EstatusVentaModel;
use App\Models\Refacciones\OperacionesPiezasModel;
use App\Models\Refacciones\ReVentasEstatusModel;
use App\Models\Refacciones\VentasRealizadasModel;
use App\Models\Contabilidad\CatalogoProcesosModel;
use App\Models\Refacciones\VentaProductoModel;
use App\Models\Refacciones\VentaServicioModel;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Refacciones\ServicioPermisoVenta;
use App\Servicios\Refacciones\ServicioVentasRealizadas;
use App\Servicios\Refacciones\ServicioFolios;
use App\Servicios\Refacciones\ServicioDesgloseProductos;
use App\Servicios\Refacciones\ServicioProductos;
use App\Servicios\Refacciones\ServicioReVentasEstatus;
use App\Servicios\Refacciones\ServicioVentaProducto;
use App\Servicios\Refacciones\ServicioVentasServicios;
use App\Servicios\Refacciones\ServicioCotizadorRefacciones;
use App\Servicios\Refacciones\ServicioCurl;
use App\Servicios\CuentasPorCobrar\ServicioCuentaPorCobrar;
use App\Servicios\Producto\CrearProductoService;
use App\Servicios\Ventas\ServicioPedidoDiaDetalles;
use App\Http\Controllers\Core\CrudController;
use App\Http\Requests\Refacciones\VentaStoreRequest;
use App\Http\Resources\DataTablesResource;
use App\Http\Resources\Ventas\ResumenResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Pdf\Pdf;
use Throwable;
use App\Http\Resources\Ventas\RefaccionesOperacionesLista;
use App\Servicios\Asientos\Ventas\Detalles\Abono as AbonoVenta;
use App\Servicios\Asientos\Ventas\Detalles\Cargo as CargoVenta;
use App\Servicios\Inventario\DevolverVentaDetallesService;
use App\Servicios\Inventario\VenderDetallesService;
use Illuminate\Support\Facades\Http;
use RuntimeException;
use Carbon\Carbon;
use App\Casts\MexicoTimezoneCast;


class VentasRealizadasController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioVentasRealizadas();
        $this->servicioVentasPermiso = new ServicioPermisoVenta();
        $this->servicioVentaProducto = new ServicioVentaProducto();
        $this->servicioVentasServicios = new ServicioVentasServicios();
        $this->servicioDesgloseProductos = new ServicioDesgloseProductos();
        $this->servicioCuentaxCobrar = new ServicioCuentaPorCobrar();
        $this->servicioReVentasEstatus = new ServicioReVentasEstatus();
        $this->ventasRealizadasModel = new VentasRealizadasModel();
        $this->operacionesPiezasModel = new OperacionesPiezasModel();
        $this->historicoProductosServicioModel = new HistoricoProductosServicioModel();
        $this->servicioProductos = new ServicioProductos();
        $this->servicioFolio = new ServicioFolios();
        $this->servicioCotizadorRefacciones = new ServicioCotizadorRefacciones();
        $this->servicioCurl = new servicioCurl;
    }

    public function storeVenta(VentaStoreRequest $request)
    {
        try {
            DB::beginTransaction();
            $data = $this->servicio->registrarVenta($request->validated());
            DB::commit();
            return Respuesta::json($data, 200, __(static::$I0003_RESOURCE_REGISTERED, [
                'parametro' => $this->servicio->getRecurso()
            ]));
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function ventasByFolioId($folio_id)
    {
        try {
            $venta = $this->servicio->ventasByFolioId($folio_id);
            $validacion = [];

            $response = Http::get(env('URL_SERVICIOS') . "/api/validaciones_ordenes/{$venta->numero_orden}");

            if($response->successful())
            {
                $validacion = [
                    'incompleta' => $response->object()->exito? false : true,
                    'validaciones' => $response->object()->validaciones,
                ];
            }

            return Respuesta::json(array_merge($venta->toArray(), $validacion), 200);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function totalventa($venta_id)
    {
        try {
            $venta_productos = $this->servicioVentaProducto->totalVentaById(['venta_id' => $venta_id]);
            $venta_servicios = $this->servicioVentasServicios->totalservicio($venta_id);
            $total = (float) $venta_servicios + (float)$venta_productos;
            return Respuesta::json($total, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function finalizarVenta(Request $request, $id)
    {
        try {
            //TODO: revisar venta total- VENTA TOTAL 
            DB::beginTransaction();
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasFinalizaVentaContado());
            if ($request->get('tipo_forma_pago_id') == TipoFormaPagoModel::FORMA_CREDITO) {
                ParametrosHttpValidador::validar($request, $this->servicio->getReglasFinalizaVentaCredito());
            }
            $data = $this->servicio->finalizarVenta($id, $request);
            DB::commit();
            $mensaje = __(static::$I0004_RESOURCE_UPDATED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($data, 200, $mensaje);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function editarVenta(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasFinalizaVentaContado());
            if ($request->get('tipo_forma_pago_id') == TipoFormaPagoModel::FORMA_CREDITO) {
                ParametrosHttpValidador::validar($request, $this->servicio->getReglasFinalizaVentaCredito());
            }
            $data = $this->servicio->editarVenta($id, $request);
            DB::commit();
            $mensaje = __(static::$I0004_RESOURCE_UPDATED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($data, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function descontarVentaMpm(Request $request, $id)
    {
        try {
            //TODO: revisar venta total- VENTA TOTAL 
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasFinalizaVentaContado());

            if ($request->get('tipo_forma_pago_id') == TipoFormaPagoModel::FORMA_CREDITO) {
                ParametrosHttpValidador::validar($request, $this->servicio->getReglasFinalizaVentaCredito());
            }
            $request->merge(['mpm' => true]);
            $data = $this->servicio->finalizarVenta($id, $request);
            $mensaje = __(static::$I0004_RESOURCE_UPDATED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($data, 200, $mensaje);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function index()
    {
        try {
            $data = $this->servicio->getAll();
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function listaVentasByStatus(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->reglasListaVentaByStatus());
            $data = $this->servicio->getVentasByStatus($request->all());
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function show($id)
    {
        try {
            $data = $this->servicio->getByIdWith($id);
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function showByVentaID($id)
    {
        try {
            $data = $this->servicio->getById($id);
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function ventasFiltrarWithDetails(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasBusqueda());
            $data = $this->servicio->filtrarPorFechas($request->all());
            $array_final = $this->servicio->calcularTotal($data);
            return Respuesta::json($array_final, 200);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function searchByDates(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasBusquedaTraspaso());
            $result = $this->servicio->getVentasByFechas($request);
            return Respuesta::json($result, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getServicioByFolio(Request $request)
    {
        try {
            // ParametrosHttpValidador::validar($request, $this->servicio->getReglasBusquedaTraspaso());
            $result = $this->servicio->getServicioByFolio($request);
            return Respuesta::json($result, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function totalesSearchByDates(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasBusquedaTraspaso());
            $result = $this->servicio->getTotalesVentasByFechas($request);
            return Respuesta::json($result, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getBusquedaVentas(Request $request)
    {
        try {
            $data = $this->servicio->getBusquedaVentas($request);
            if ($request->get('no_pagination')) {
                return Respuesta::json($data, 200);
            } else {
                return response()->json(
                    new DataTablesResource($data)
                );
            }
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function downloadReporteVentas(Request $request)
    {
        $currentDateTime = Carbon::now()->tz('America/Chihuahua');
        $request->merge(['detalle_producto' => true]);
        $data = $this->servicio->getBusquedaVentas($request);
        $pdf = new Pdf;
        
        $pdf->html(
            view('reportes.venta_mostrador', [
                'ventas' => $data->toArray() ?? [],
                'fecha_inicio' => $request->fecha_inicio ?? 'No aplica',
                'fecha_fin' => ($request->fecha_fin) ?? 'No aplica',
                'vendedor' => ($request->vendedor_id) ?? 'Todos',
                'fecha_elaboracion' =>  $currentDateTime->format('d/m/Y H:i')
            ])
                ->render()
        );

        return $pdf->output();
    }
    //Obtener ventas para generar archivo
    public function getVentas6MesesAtras(Request $request)
    {
        try {
            $this->servicio->generarlayoutford($request->archivo);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getAllVentas(Request $request)
    {
        try {
            $data = $this->servicio->getAllVentas($request);
            return Respuesta::json(array('data' => $data), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getBusquedaVentasDevoluciones(Request $request)
    {
        try {
            $data = $this->servicio->getBusquedaVentasDevoluciones($request);
            return Respuesta::json(array('data' => $data), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function curlGetVehiculoInfo($numero_orden)
    {
        try {
            $data =  $this->servicio->curlGetDataApiMpm($numero_orden);
            $responseData = isset($data['vehiculo']) ? $data['vehiculo'] : [];
            return Respuesta::json($responseData, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getRefaccionesVentanillaTaller($numero_orden)
    {
        try {

            $existe_venta = $this->ventasRealizadasModel
                ->where(VentasRealizadasModel::NUMERO_ORDEN, '=', $numero_orden)
                ->first();
            $newArray = [];
            $dataWs = $this->servicio->listaProductosTaller($numero_orden);

            if ($existe_venta) {
                $productos_venta = $this->servicioVentaProducto->geDetalleVentaById($existe_venta->id)->toArray();
                $VentaServicio = $this->servicioVentasServicios->ventaservicio($existe_venta->id)->toArray();
                $new_array = array_merge($VentaServicio, $productos_venta);

                foreach ($new_array as $key => $item) {

                    $newArray[$key]["id"] = $item->id;
                    $newArray[$key]["id_index_requisicion"] = isset($item->id_index_requisicion) ?  $item->id_index_requisicion : null;
                    $producto_id =  !empty($item->producto_id) ? $item->producto_id : null;
                    $newArray[$key]["producto_id"] =  $producto_id;
                    $newArray[$key]["cantidad"] = $item->cantidad;
                    $newArray[$key]["descripcion"] = isset($item->descripcion_producto) ? $item->descripcion_producto : $item->descripcion;  //valodar si se pone uno u otro al traspaso
                    $newArray[$key]["num_pieza"] = $item->no_identificacion;
                    $newArray[$key]["precio_unitario"] = isset($item->costo_mo) ? $item->costo_mo : $item->valor_unitario;
                    $newArray[$key]["tipo"] = !empty($item->producto_id) ? "PREPIKING" : "OPERACION";
                    $newArray[$key]["se_registro"] = true;
                    $newArray[$key]["cantidad_stock"] = isset($item->stock) ? $item->stock : 0;
                    $newArray[$key]["orden_original"] = isset($item->orden_original) &&  $producto_id !== null ? $item->orden_original : 1;
                    $newArray[$key]["estatus_producto_orden"] =  isset($item->estatus_producto_orden) &&  $producto_id !== null ? $item->estatus_producto_orden : "O";
                }
                return Respuesta::json($newArray, 200);
            }

            foreach ($dataWs['refacciones'] as $key => $itemWs) {
                //checar esta validacion
                $historicoProductoServicio = $this->historicoProductosServicioModel
                    ->where(HistoricoProductosServicioModel::getTableName() . '.' . HistoricoProductosServicioModel::PRODUCTO_ID,  $itemWs->producto_id)
                    ->first();
                if ($historicoProductoServicio) {
                    $stock = $this->servicioProductos->stockByProductoId($historicoProductoServicio->producto_id);
                    $precio_unitario = $stock->valor_unitario;
                    $stock = $stock->desglose_producto->cantidad_actual;

                    $newArray[$key]["precio_unitario"] =  $precio_unitario;
                    $newArray[$key]["cantidad_stock"] = $stock;
                    $newArray[$key]["se_registro"] = $itemWs->se_registro;
                } else {

                    $newArray[$key]["cantidad_stock"] = $itemWs->cantidad_stock; //$cantidad_stock;
                    $newArray[$key]["precio_unitario"] = !empty($itemWs->producto_id) ? $itemWs->precio_unitario : $itemWs->costo_mo;
                    $newArray[$key]["se_registro"] =  $itemWs->se_registro;
                }


                $newArray[$key]["id"] = $itemWs->id; //verificar id
                $newArray[$key]["producto_id"] = !empty($itemWs->producto_id) ? $itemWs->producto_id : null;
                $newArray[$key]["cantidad"] = isset($historicoProductoServicio->cantidad) && $itemWs->cantidad !== $historicoProductoServicio->cantidad ? $historicoProductoServicio->cantidad : $itemWs->cantidad;
                $newArray[$key]["descripcion"] = isset($itemWs->descripcion_producto) ? $itemWs->descripcion_producto : $itemWs->descripcion;  //valodar si se pone uno u otro al traspaso
                $newArray[$key]["num_pieza"] = $itemWs->num_pieza; //validar remplazo
                $newArray[$key]["tipo"] = $itemWs->tipo;
                $newArray[$key]["orden_original"] = 1;
                $newArray[$key]["estatus_producto_orden"] = "O";
            }
            return Respuesta::json($newArray, 200);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function getRefaccionesDeTaller($numero_orden)
    {
        try {
            return response()->json(
                $this->servicio->getRefaccionesDeTaller($numero_orden)
            );
        } catch (\Throwable $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], 400);
        }
    }

    public function ventaMpm(Request $request)
    {
        try {
            DB::beginTransaction();
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasCrearVentaMpm());
            $data = $this->servicio->crearVentaMpm($request->all());
            DB::commit();
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($data, 200, $mensaje);
        } catch (\Throwable $e) {
            DB::rollback();

            info([$e->getLine(), $e->getCode(), $e->getFile()]);
            return Respuesta::error($e);
        }
    }

    public function detalleOperacion(Request $request)
    {
        try {

            ParametrosHttpValidador::validar($request, [
                'venta_id' => 'required',
                // 'no_identificacion' => 'required'
            ]);
            $no_orden = $this->ventasRealizadasModel
                ->select('numero_orden')
                ->where('id', '=', $request->venta_id)
                // ->where('no_identificacion', '=', $request->no_identificacion)
                ->first();

            // producto.no_identificacion,producto.valor_unitario
            $data = $this->operacionesPiezasModel
                ->select('producto.*')
                ->join('producto', 'producto.id', '=', 'operaciones_piezas.id_refaccion')
                ->where('no_orden', '=', $no_orden->numero_orden)
                ->get();
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function detalleVentampmById(Request $request, $venta_id)
    {
        try {
            $venta = VentasRealizadasModel::find($venta_id);

            $productos_venta = $this->servicioVentaProducto->geDetalleVentaById($venta_id);
            // dd($productos_venta);
            $venta_servicios = $this->servicioVentasServicios->ventaservicio($venta_id);

            $refacciones_operaciones = $venta_servicios->concat($productos_venta);

            return Respuesta::json([
                'aaData' => RefaccionesOperacionesLista::collection($refacciones_operaciones),
                'total' => $venta->venta_total?? 0,
                'draw' => request()->input('draw'),
                'iTotalDisplayRecords' => $refacciones_operaciones->count(),
                'iTotalRecords' => 25
            ]);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function detalleVentaByNumeroOrden(int $numero_orden)
    {
        try{

            $venta = VentasRealizadasModel::whereNotNull('numero_orden')->where('numero_orden', $numero_orden)->first();

            $productos_venta = $venta->productos()->where('estatus_producto_orden', '<>', 'B')->orderBy('id')->get();
            $venta_servicios = $venta->servicios()->orderBy('id')->get();

            $refacciones_operaciones = $venta_servicios->concat($productos_venta);

            return Respuesta::json([
                'aaData' => RefaccionesOperacionesLista::collection($refacciones_operaciones),
                'total' => $venta->venta_total?? 0,
                'draw' => request()->input('draw'),
                'iTotalDisplayRecords' => $refacciones_operaciones->count(),
                'iTotalRecords' => 25
            ]);
        }
        catch(Throwable $e){
            return Respuesta::error($e);
        }
    }

    public function FinalizarVentaServicio(Request $request)
    {
        try {
            DB::beginTransaction();
            ParametrosHttpValidador::validar($request, [VentasRealizadasModel::NUMERO_ORDEN => 'required']);
            $data = $this->servicio->finalizarVentaServicio($request->all());
            DB::commit();
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($data, 200, $mensaje);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }


    #TODO: revisar si al abrir de nuevo afecta a kardex y otro flujo
    public function abrirVentaServicio(Request $request)
    {
        try {
            DB::beginTransaction();
            ParametrosHttpValidador::validar($request, [VentasRealizadasModel::NUMERO_ORDEN => 'required']);
            $data = $this->servicio->abrirOrdenServicio($request->all());
            DB::commit();
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($data, 200, $mensaje);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function getVentasPorMesByProducto(Request $request)
    {
        try {

            ParametrosHttpValidador::validar($request, $this->servicio->getReglasVentasAgrupadasMes());

            $data = $this->servicio->getVentasAgrupadasMes($request->all());
            $listado = [];
            $totalVentas = 0;
            foreach ($data as $val) {
                $totalVentas = $totalVentas + $val->total_ventas;
                $val->año = $request->get('year');
                array_push($listado, $val);
            }
            $cantidad = $request->get('cantidad_meses') ? $request->get('cantidad_meses') : 12;
            $promedio = $totalVentas / $cantidad;

            $totales[] = [
                'total' => $totalVentas,
                'promedio' => $promedio,
                'year' => $request->get('year'),
            ];


            return Respuesta::json([
                'lista_ventas' => $listado,
                'totales' => $totales
            ], 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function apartarVenta(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, ['venta_id' => 'required']);
            $productos_venta = $this->servicioVentaProducto->geDetalleVentaById($request->venta_id)->toArray();
            $this->servicioReVentasEstatus->storeReVentasEstatus([
                ReVentasEstatusModel::VENTA_ID => $request->venta_id,
                ReVentasEstatusModel::ESTATUS_VENTA_ID  => EstatusVentaModel::ESTATUS_APARTADO
            ]);

            if (!empty($productos_venta)) {
                foreach ($productos_venta as $key => $venta) {
                    $this->servicioDesgloseProductos->actualizaStockByProducto(['producto_id' => $venta->producto_id]);
                }
            }

            $this->servicio->massUpdateWhereId(VentasRealizadasModel::ID, $request->venta_id, [
                VentasRealizadasModel::TIENE_PIEZAS_APARTADAS => 1
            ]);
            return Respuesta::json($productos_venta, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function ventasByUser($user_id)
    {
        try {
            $data = $this->servicio->getVentasByUser($user_id);
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function getOrdenesAbiertas(Request $request)
    {
        try {

            $response = Http::withBody(json_encode([
                'numero_orden' => $request->numero_orden?? '',
                'fecha_inicio' => $request->fecha_inicio?? '',
                'fecha_final' => $request->fecha_final?? '',
                'page' => floor($request->start / ($request->length ?: 1)) + 1,
            ]), 'application/json')
            ->post(env('SERVICIOS_ORDENES_ABIERTAS'));

            $data = $response->object();

            $tableVentas = VentasRealizadasModel::getTableName();

            $arrayData = [];
            foreach ($data as $key => $item) {
                $arrayData[$key]['id_cita'] = $item->id_cita;
                $arrayData[$key]['cliente'] = $item->cliente;
                $arrayData[$key]['numero_cliente'] = $item->numero_cliente;
                $arrayData[$key]['tipo_orden'] = $item->tipo_orden;
                $arrayData[$key]['asesor'] = $item->asesor;
                $arrayData[$key]['tecnico'] = $item->tecnico;
                $arrayData[$key]['modelo'] = $item->modelo;
                $arrayData[$key]['kilometraje'] = $item->kilometraje;
                $arrayData[$key]['placas'] = $item->placas;
                $arrayData[$key]['serie'] = $item->serie;
                $arrayData[$key]['anio'] = $item->anio;
                $arrayData[$key]['estatus_orden'] = $item->estatus_orden;
                $arrayData[$key]['presupuesto_adicional'] = $item->presupuesto_adicional;
                $arrayData[$key]['presupuesto_afectado'] = $item->presupuesto_afectado;
                $arrayData[$key]['requisicion'] = $item->requisicion ? env('URL_SERVICIOS') . "citas/citas_queretaro/$item->requisicion" : null;
                $data =  $this->ventasRealizadasModel->where($tableVentas . '.' . VentasRealizadasModel::NUMERO_ORDEN, $item->id_cita)->where('completa', true)->count();
                $arrayData[$key]['orden_venta'] = $data > 0 ? true : false;
                $arrayData[$key]['fecha_recepcion'] = $item->fecha_recepcion;
                $arrayData[$key]['hora_recepcion'] = $item->hora_recepcion;
                $arrayData[$key]['servicio_unidad'] = $item->servicio_unidad;
            }

            return Respuesta::json([
                'aaData' => $arrayData,
                'draw' => request()->input('draw'),
                'iTotalDisplayRecords' => 1000,
                'iTotalRecords' => count($arrayData)
            ], 200);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function createVenta(Request $request)
    {
        try {
            DB::beginTransaction();
            $requestVenta = $this->createRequestVenta($request);
            ParametrosHttpValidador::validar($requestVenta, $this->servicio->getReglasCrearVenta());
            if ($request->get(VentasRealizadasModel::TIPO_VENTA) == 3) { // SI ES TIPO COTIZACIÓN
                $requestCotizador = $this->servicioCotizadorRefacciones->createRequestCotizador($request);
                ParametrosHttpValidador::validar($requestCotizador, $this->servicioCotizadorRefacciones->getReglasGuardar());
            }

            $folio = $this->servicioFolio->generarFolio(CatalogoProcesosModel::PROCESO_VENTA_MOSTRADOR);
            $requestVenta->merge([VentasRealizadasModel::FOLIO_ID => $folio->id]);
            // $requestVenta->merge([VentasRealizadasModel::VENDEDOR_ID => Auth::user() ? Auth::user()->id : 1]);

            $venta = $this->servicio->crear($requestVenta->all());

            if ($venta) {
                $re_estatus = $this->createReVentasEstatus($venta->id);
                if ($re_estatus) {
                    if ($request->get(VentasRealizadasModel::TIPO_VENTA) == 3) { // SI ES TIPO COTIZACIÓN
                        $requestCotizador->merge([VentasRealizadasModel::VENTA_ID => $venta->id]);
                        $this->servicioCotizadorRefacciones->crear($requestCotizador->all());
                    }
                }
                DB::commit();
                $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
                return Respuesta::json($venta, 200, $mensaje);
            }
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }


    public function updateOnlyVenta(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $requestVenta = $this->createRequestVenta($request);
            ParametrosHttpValidador::validar($requestVenta, $this->servicio->getReglasUpdateOnlyVenta());
            if ($request->get(VentasRealizadasModel::TIPO_VENTA) == 3) { // SI ES TIPO COTIZACIÓN
                $requestCotizador = $this->servicioCotizadorRefacciones->createRequestCotizador($request);
                $requestCotizador->merge([VentasRealizadasModel::VENTA_ID => $id]);
                ParametrosHttpValidador::validar($requestCotizador, $this->servicioCotizadorRefacciones->getReglasGuardar());
            }
            $data = $this->servicio->updateOnlyVenta($id, $requestVenta);
            if ($data) {
                if ($request->get(VentasRealizadasModel::TIPO_VENTA) == 3) { // SI ES TIPO COTIZACIÓN
                    $this->servicioCotizadorRefacciones->massUpdateWhereId(VentasRealizadasModel::VENTA_ID, $id, $requestCotizador->toArray());
                }
            }
            DB::commit();
            $mensaje = __(static::$I0004_RESOURCE_UPDATED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($data, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }


    public function createRequestVenta(Request $request)
    {
        return new Request([
            VentasRealizadasModel::CLIENTE_ID => $request->get(VentasRealizadasModel::CLIENTE_ID),
            VentasRealizadasModel::TIPO_VENTA => $request->get(VentasRealizadasModel::TIPO_VENTA),
            VentasRealizadasModel::TIPO_CLIENTE_ID => $request->get(VentasRealizadasModel::TIPO_CLIENTE_ID),
            VentasRealizadasModel::PRECIO_ID => 1,
            VentasRealizadasModel::TIPO_PRECIO_ID => 1,
            VentasRealizadasModel::VENTA_TOTAL => 0,
            VentasRealizadasModel::VENDEDOR_ID => $request->get(VentasRealizadasModel::VENDEDOR_ID),
            VentasRealizadasModel::FECHA_VENTA => $request->get(VentasRealizadasModel::FECHA_VENTA),
        ]);
    }

    public function createReVentasEstatus($venta_id)
    {
        $re_estastus[ReVentasEstatusModel::VENTA_ID] = $venta_id;
        $re_estastus[ReVentasEstatusModel::ESTATUS_VENTA_ID] = EstatusVentaModel::ESTATUS_PROCESO;
        return $this->servicioReVentasEstatus->storeReVentasEstatus($re_estastus);
    }

    public function updateTipoVenta(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasTipoVenta());
            $data = $this->servicio->massUpdateWhereId(VentasRealizadasModel::ID, $id, $request->toArray());
            DB::commit();
            $mensaje = __(static::$I0004_RESOURCE_UPDATED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($data, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getTotales($venta_id)
    {
        try {
            $data = $this->servicio->getTotalesByVenta($venta_id);
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function getByVentaCotizadorPorClienteId($cliente_id)
    {
        try {
            ParametrosHttpValidador::validar_array(['cliente_id' => $cliente_id], $this->servicio->getReglasCliente());
            $data = $this->servicio->getByVentaCotizadorPorClienteId($cliente_id);
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function getFolio($numero_orden)
    {
        return response()->json(
            VentasRealizadasModel::where('numero_orden', $numero_orden)->first()
        );
    }

    public function getTotalesServiciosRefacciones($numero_orden)
    {
        $venta = VentasRealizadasModel::where('numero_orden', $numero_orden)->first();

        if (is_null($venta)) {
            return response()->json(['message' => 'El numero de orden no se encontro', 'existe_orden' => false], 404);
        }

        // total refacciones que no son prepiking
        $total_refacciones = VentaProductoModel::where('venta_id', $venta->id)->where('estatus_producto_orden', '<>', 'B')->where('afecta_paquete', false)->where('afecta_mano_obra', false)->sum(DB::raw('valor_unitario * cantidad'));

        // total refacciones que afectan mano de obra
        $total_refacciones_afectan_mo = VentaProductoModel::where('venta_id', $venta->id)->where('estatus_producto_orden', '<>', 'B')->where('afecta_mano_obra', true)->sum(DB::raw('valor_unitario * cantidad'));

        // total refacciones que son prepiking
        $total_refacciones_prepiking = VentaProductoModel::where('venta_id', $venta->id)->where('estatus_producto_orden', '<>', 'B')->where('afecta_paquete', true)->sum(DB::raw('valor_unitario * cantidad'));

        // total refacciones con mano de obra
        $total_mano_obra_refacciones = VentaProductoModel::where('venta_id', $venta->id)->where('estatus_producto_orden', '<>', 'B')->where('presupuesto', true)->sum('costo_mano_obra');

        // total servicios
        $total_servicios = $this->servicioVentasServicios->totalServicio($venta->id);

        // total refacciones servicios
        $total_servicios_refacciones = VentaServicioModel::where('venta_id', $venta->id)->sum('refacciones');

        // total refacciones mano de obra con iva
        $total_refaccion_prepiking_mo_iva = ($total_refacciones_prepiking + $total_refacciones_afectan_mo) * VentaProductoModel::IVA;

        // total costo promedio refacciones
        $total_costo_promedio_refacciones = VentaProductoModel::where('venta_id', $venta->id)->where('estatus_producto_orden', '<>', 'B')->sum(DB::raw('costo_promedio * cantidad'));

        // total descuento refacciones cuando es de presupuestos
        $total_descuento_refacciones = VentaProductoModel::whereNotNull('valor_unitario_original')->where('venta_id', $venta->id)->where('estatus_producto_orden', '<>', 'B')->where('presupuesto', true)->sum(DB::raw('(valor_unitario * cantidad) - (valor_unitario_original * cantidad)'));

        $response = [
            'total_refacciones' => $total_refacciones + $total_servicios_refacciones,
            'total_refacciones_iva' => ($total_refacciones + $total_servicios_refacciones) * VentaProductoModel::IVA,
            'total_refacciones_prepiking' => $total_refacciones_prepiking,
            'total_refacciones_prepiking_iva' => $total_refacciones_prepiking * VentaProductoModel::IVA,
            'total_refacciones_afectan_mano_obra' => $total_refacciones_afectan_mo,
            'total_refacciones_afectan_mano_obra_iva' => $total_refacciones_afectan_mo * VentaProductoModel::IVA,
            'total_refacciones_mano_obra' => $total_mano_obra_refacciones,
            'total_refacciones_mano_obra_sin_iva' => $total_mano_obra_refacciones / VentaProductoModel::IVA,
            'total_costo_promedio_refacciones' => $total_costo_promedio_refacciones,
            'total_descuento_refacciones' => $total_descuento_refacciones,
            'total_servicios' => $total_servicios,
            'costo_mano_obra' => $total_servicios + $total_mano_obra_refacciones - $total_refaccion_prepiking_mo_iva,
            'costo_mano_obra_sin_iva' => (($total_servicios - $total_refaccion_prepiking_mo_iva) / VentaProductoModel::IVA)  + ($total_mano_obra_refacciones / VentaProductoModel::IVA),
            'venta_total' => $total_refacciones + ($total_servicios / VentaProductoModel::IVA) + ($total_mano_obra_refacciones / VentaProductoModel::IVA) + $total_servicios_refacciones,
            'venta_total_iva' => ($total_refacciones + $total_servicios_refacciones) * VentaProductoModel::IVA + ($total_mano_obra_refacciones + $total_servicios),
            'venta_total_descuento' => ($total_refacciones + $total_servicios_refacciones) * VentaProductoModel::IVA + ($total_mano_obra_refacciones + $total_servicios) - $total_descuento_refacciones * VentaProductoModel::IVA, 
        ];

        if (($total_servicios - $total_refaccion_prepiking_mo_iva) < 0) {
            return response()->json(['message' => 'El resultado de afectar mano de obra con las refacciones da un saldo negativo.'], 400);
        }

        return response()->json($response);
    }

    /**
     * Sincronizar refacciones y operaciones desde servicios
     * 
     * @param  App\Models\Refacciones\VentasRealizadasModel $venta
     * @return Illuminate\Http\Response
     * @deprecated deja de utilizarse por cambio de 
     */
    public function actualizar_refacciones_operaciones(VentasRealizadasModel $venta)
    {
        return response()->json([
            'message' => 'Esta api queda obsoleta eliminar la llamada.',
        ]);
    }
    
    /**
     * Actualizar operaciones con numero de orden
     * llamada ejecutada desde servicios
     *
     * @param string $numero_orden
     * @return void
     * @deprecated cambio de flujo
     */
    public function actualizar_refacciones_operaciones_orden($numero_orden)
    {
        $venta = VentasRealizadasModel::where('numero_orden', $numero_orden)->first();

        if($venta == null)
        {
            return response()->json(['message' => 'El numero de orden no existe.'], 400);
        }

        // info("SOLICITUD DE ACTUALIZACION (PREVENTIVO O PRESUPUESTOS): $numero_orden");

        return $this->actualizar_refacciones_operaciones($venta);
    }

    /**
     * Reabrir orden de venta
     *
     * @param VentasRealizadasModel $venta
     * @throws RuntimeException
     * @return Illuminate\Http\Response
     * @deprecated por App\Http\Controllers\Detalles\VentaDetallesController
     */
    public function reabrir_orden_completa(VentasRealizadasModel $venta)
    {
        try {

            $response = Http::get(env('URL_SERVICIOS') . "api/validarOrdenCerrada/$venta->numero_orden");

            if($response->failed() || $response->object()->orden_cerrada == true)
                throw new RuntimeException("El numero de orden $venta->numero_orden se encuentra cerrada.");

            DB::beginTransaction();

            $venta->completa = false;

            $venta->save();

            DB::commit();

            return response()->json([
                'message' => 'La orden se abrio correctamente y se devolvieron las piezas al inventario.',
            ]);
        } catch(RuntimeException $exception){

            return response()->json([
                'message' => $exception->getMessage(),
            ], 400);
        }
        catch (Throwable $exception) {

            DB::rollBack();

            return response()->json([
                'message' => 'Ocurrio un error al abrir la orden.',
                'errors' => $exception->getMessage(),
            ], 400);
        }
    }

    public function resumen_detalles($numero_orden)
    {
        $venta = VentasRealizadasModel::with('productos.producto')->where('numero_orden', $numero_orden)->first();

        if ($venta == null) {
            return response()->json(['message' => "El numero de orden no existe $numero_orden"], 400);
        }

        $productos = $venta->productos()->where('estatus_producto_orden', '<>', 'B')->get();
        $servicios = $venta->servicios()->get();
        $productos->loadMissing('producto');

        return response()->json(
            new ResumenResource([
                'servicios' => $servicios,
                'productos' => $productos,
            ])
        );
    }

    /**
     * Habilitar la firma de la requisicion
     *
     * @param integer $numero_orden
     * @return Illuminate\Http\Response
     * @deprecated elimina no es necesaria
     */
    public function habilitar_firma_requisicion(int $numero_orden)
    {
        $response = Http::post(env('URL_SERVICIOS') . "citas/citas_queretaro/ordenServicio/Documentacion_Ordenes/habilitar_firma_requisicion/$numero_orden");

        if($response->successful() && $response->object()->success == true)
        {
            return response()->json(['message' => 'Se realizó la habilitación para firma de la requisición.']);
        }

        return response()->json([
            'message' => 'No se realizó la habilitación para firma de la requisición.',
            'errors' => $response->object() == null? 'Sin respuesta en json del servidor' : $response->object()->message,
        ], 400);
    }

    /**
     * Envio a contabilidad por numero de orden
     *
     * @param string $numero_orden
     * @return \Illuminate\Http\Response
     * @deprecated 
     */
    public function contabilidad_numero_orden($numero_orden)
    {
        $numero_orden = preg_replace('/[^0-9]/', '', $numero_orden);
        
        $venta = VentasRealizadasModel::where('numero_orden', $numero_orden)->first();

        if($venta == null)
        {
            return response()->json(['message' => __("Numero de orden :numero_orden no existe.", ['numero_orden' => $numero_orden]) ], 400);
        }

        $productos = $venta->productos()->where('estatus_producto_orden', '<>', 'B')->get();
        
        $costo_promedio = $productos->map(function($item){
            $item->costo_promedio = $item->producto->costo_promedio;
            $item->save();

            return $item->cantidad * $item->costo_promedio;
        })
        ->sum();

        $total = $productos->map(fn($item) => $item->cantidad * $item->valor_unitario)
        ->sum();

        // Enviamos a contabilidad el abono y cargo de las ventas
        // Sin el IVA
        $abono = new AbonoVenta($venta, [
            'costo_promedio' => $costo_promedio,
        ]);
        $abono->make($total);

        $cargo = new CargoVenta($venta, [
            'costo_promedio' => $costo_promedio,
        ]);
        $cargo->make($total);

        return response()->json([
            'message' => __('Se realizo el cargo a contabilidad de la venta para esta orden :numero_orden con un saldo de :total.', ['numero_orden' => $numero_orden, 'total' => $total]),
        ]);
    }

    /**
     * Envio a contabilidad por numero de orden refacciones de presupuestos
     *
     * @param string $numero_orden
     * @return \Illuminate\Http\Response
     * @deprecated 
     */
    public function contabilidad_numero_orden_presupuestos($numero_orden)
    {
        $numero_orden = preg_replace('/[^0-9]/', '', $numero_orden);
        
        $venta = VentasRealizadasModel::where('numero_orden', $numero_orden)->first();
        
        if($venta == null)
        {
            return response()->json(['message' => __("Numero de orden :numero_orden no existe.", ['numero_orden' => $numero_orden]) ], 400);
        }

        $productos = $venta->productos()->where('estatus_producto_orden', '<>', 'B')->get();

        // Total costo promedio
        $costo_promedio = $productos
        ->filter(function($producto){
            return $producto->presupuesto === true;
        })->map(function($item){
            $item->costo_promedio = $item->producto->costo_promedio;
            $item->save();

            return $item->cantidad * $item->costo_promedio;
        })
        ->sum();

        // Total venta
        $total = $productos
        ->filter(function($producto){
            return $producto->presupuesto === true;
        })->map(fn($item) => $item->cantidad * $item->valor_unitario)
        ->sum();
    
        // Enviamos a contabilidad el abono y cargo de las ventas
        // Sin el IVA
        $abono = new AbonoVenta($venta, [
            'costo_promedio' => $costo_promedio,
        ]);
        $abono->make($total);

        $cargo = new CargoVenta($venta, [
            'costo_promedio' => $costo_promedio,
        ]);
        $cargo->make($total);

        return response()->json([
            'message' => __('Se realizo el cargo a contabilidad de la venta para esta orden :numero_orden con un saldo de :total.', ['numero_orden' => $numero_orden, 'total' => $total]),
        ]);        
    }

    /**
     * Envio a contabilidad por numero de orden y refaccion
     *
     * @param Request $request
     * @param string $numero_orden
     * @param string $num_pieza
     * @return \Illuminate\Http\Response
     * @deprecated 
     */
    public function contabilidad_numero_orden_y_refaccion(Request $request, $numero_orden, $num_pieza)
    {
        $numero_orden = preg_replace('/[^0-9]/', '', $numero_orden);
        $num_pieza = preg_replace('/[\s]/', '', $num_pieza);
        
        $venta = VentasRealizadasModel::where('numero_orden', $numero_orden)->first();
        
        if($venta == null)
        {
            return response()->json(['message' => __("Numero de orden :numero_orden no existe.", ['numero_orden' => $numero_orden]) ], 400);
        }

        $productos = $venta->productos()->where('estatus_producto_orden', '<>', 'B')->get();

        $costo_promedio = $productos
        ->filter(fn($producto) => $producto->producto->no_identificacion == $num_pieza )
        ->map(function($item){
            $item->costo_promedio = $item->producto->costo_promedio;
            $item->save();

            return $item->cantidad * $item->costo_promedio;
        })
        ->sum();

        $total = $productos
        ->filter(fn($producto) => $producto->producto->no_identificacion == $num_pieza )
        ->map(fn($item) => $item->cantidad * $item->valor_unitario)
        ->sum();

        // Enviamos a contabilidad el abono y cargo de las ventas
        // Sin el IVA
        if($total > 0)
        {
            $abono = new AbonoVenta($venta, [
                'cliente_id' => $request->cliente_id,
                'cliente_nombre' => $request->cliente_nombre,
                'cliente_apellido_paterno' => $request->cliente_apellido_paterno,
                'cliente_apellido_materno' => $request->cliente_apellido_materno,
                'fecha' => optional($request->fecha)->format('Y-m-d H:i:s'),
                'num_pieza' => $num_pieza,
                'costo_promedio' => $costo_promedio,
            ]);

            $abono->make($total);

            $cargo = new CargoVenta($venta, [
                'cliente_id' => $request->cliente_id,
                'cliente_nombre' => $request->cliente_nombre,
                'cliente_apellido_paterno' => $request->cliente_apellido_paterno,
                'cliente_apellido_materno' => $request->cliente_apellido_materno,
                'fecha' => optional($request->fecha)->format('Y-m-d H:i:s'),
                'num_pieza' => $num_pieza,
                'costo_promedio' => $costo_promedio,
            ]);

            $cargo->make($total);

            return response()->json([
                'message' => __('Se realizo el cargo a contabilidad de la venta para esta orden :numero_orden con un saldo de :total.', ['numero_orden' => $numero_orden, 'total' => $total]),
            ]);
        }

        return response()->json([
            'message' => __('Verificar que el valor unitario de la refaccion :refaccion no sea 0 o exista', [ 'refaccion' => $num_pieza ]),
        ], 400);
    }

    public function getVendedorPorFolio(Request $request){
        try {
            ParametrosHttpValidador::validar($request, ['folio' => 'required|exists:ventas,folio_id']);
            $venta = $this->servicio->vendedorByFolioId($request->folio);

            return Respuesta::json($venta, 200);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }
}
