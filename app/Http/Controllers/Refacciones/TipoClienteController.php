<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Refacciones\ServicioTipoCliente;
use Illuminate\Http\Request;

class TipoClienteController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioTipoCliente();
    }
}
