<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Models\Refacciones\Precios;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Refacciones\ServicioPrecios;
use Illuminate\Http\Request;
use Throwable;

class PreciosController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioPrecios();
    }

    public function filtrarPrecios(Request $request)
    {
        try {
            if (!empty($request->get(Precios::ID_VISTA))) {
                switch ($request->get(Precios::ID_VISTA)) {
                    case Precios::ID_VMOSTRADOR:
                        return Respuesta::json($this->servicio->getWhereIn(Precios::ID, [Precios::NIVEL_TRADICIONAL]), 200);
                        break;
                    case Precios::ID_SEXCELLENT:
                        return Respuesta::json($this->servicio->buscarTodos(), 200);
                        break;

                    default:
                        return Respuesta::json($this->servicio->buscarTodos(), 200);
                        break;
                }
            }

            return Respuesta::json($this->servicio->buscarTodos(), 200);
        } catch (Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
