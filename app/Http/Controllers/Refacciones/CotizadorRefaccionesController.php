<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Refacciones\ServicioCotizadorRefacciones;
use App\Servicios\Refacciones\ServicioDetalleCotizador;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use Illuminate\Http\Request;

class CotizadorRefaccionesController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioCotizadorRefacciones();
        $this->servicioDetalleCotizador = new ServicioDetalleCotizador();
    }

    public function index()
    {
        $datos = $this->servicio->getCotizaciones();
        
        return Respuesta::json($datos, 200);
    }

    public function getByIdVenta(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasPorVenta());
            $modelo = $this->servicio->getByIdVenta($request->get('venta_id'));
            if ($modelo) {
                return Respuesta::json($modelo, 200);
            }
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
