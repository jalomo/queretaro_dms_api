<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Refacciones\ServicioOrdenCompra;
use App\Servicios\Refacciones\ServicioDevolucionProveedor;
use App\Servicios\Refacciones\ServicioFolios;
use App\Servicios\Refacciones\ServicioReOrdenCompraEstatus;
use App\Servicios\Refacciones\ServicioProveedor;
use App\Servicios\CuentasPorPagar\ServicioCuentaPorPagar;
use App\Servicios\CuentasPorCobrar\ServicioAsientos;
use App\Models\Refacciones\EstatusCompra;
use App\Models\Refacciones\DevolucionVentasModel;
use App\Models\Contabilidad\CatalogoProcesosModel;
use App\Models\CuentasPorCobrar\EstatusCuentaModel;
use App\Models\CuentasPorPagar\CuentasPorPagarModel;
use App\Models\CuentasPorCobrar\AsientoModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Exceptions\ParametroHttpInvalidoException;

class DevolucionProveedorController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioDevolucionProveedor();
        $this->servicioReOrdenCompraEstatus = new ServicioReOrdenCompraEstatus();
        $this->servicioFolios = new ServicioFolios();
        $this->servicioOrdenCompra = new ServicioOrdenCompra();
        $this->servicioCuentaPorPagar = new ServicioCuentaPorPagar();
        $this->servicio_proveedor = new ServicioProveedor();
        $this->servicio_asientos = new ServicioAsientos();
    }
    public function store(Request $request)
    {
        DB::beginTransaction();
        $msg = "Error al procesar la devolución";
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
            $params = $request->all();
            // Verificar si existen ventas de los productos de la orden compra
            $verifica_ventas = $this->servicio->verificaVentasOrdenCompra($params['orden_compra_id']);
            if (!$verifica_ventas) {
                $msg = "No existen ventas en la orden compra";
                throw new ParametroHttpInvalidoException([
                    'msg' => $msg
                ]);
            }
            $orden_compra = $this->servicioOrdenCompra->getById($params['orden_compra_id']);
            $folio_orden_compra = $orden_compra ? $orden_compra->folio_id : null;

            //GENERAMOS UN NUEVO FOLIO POR LA DEVOLUCION DE LA ORDEN_COMPRA
            $folio = $this->servicioFolios->generarFolio(CatalogoProcesosModel::PROCESO_DEVOLUCION_PROVEEDOR);
            $params[DevolucionVentasModel::FOLIO_ID] = $folio->id;

            $devolucion = $this->servicio->crear($params);
            $this->servicio->descontarAlmacenPorOrdenCompra($params['orden_compra_id']);
            $proceso_terminado = $this->servicioReOrdenCompraEstatus->storeReOrdenCompraEstatus(
                [
                    'orden_compra_id' => $params['orden_compra_id'],
                    'estatus_compra_id' => EstatusCompra::ESTATUS_DEVOLUCION
                ]
            );
            if ($proceso_terminado) {
                $data_poliza = $this->polizaDevolucion($folio_orden_compra);
                if ($data_poliza->status != 'success') {
                    $msg = "Error al procesar los asientos";
                    throw new ParametroHttpInvalidoException([
                        'msg' => $msg
                    ]);
                    DB::rollback();
                }
            } else {
                $msg = "Error al procesar el cambio de esatus";
                throw new ParametroHttpInvalidoException([
                    'msg' => $msg
                ]);
                DB::rollback();
            }
            DB::commit();

            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            //TO-DO VA DESAPARECER CON LA FUNCIONALIDAD DE CHAVEZ proceso para actualizar inventario de productos de la orden compra
            $this->servicio->actualizarInventarioPorOrdenCompra($params['orden_compra_id']);
            return Respuesta::json($devolucion, 200, $mensaje);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    function polizaDevolucion($folio_orden_compra)
    {
        $cuenta_por_pagar = $this->servicioCuentaPorPagar->getByFolioId($folio_orden_compra);
        $proveedor = $this->servicio_proveedor->getById($cuenta_por_pagar->proveedor_id);
        $importe = $cuenta_por_pagar->total;
        $iva = $importe * 0.16;
        $importe_sin_iva = $importe - $iva;

        $request = new Request;

        $request[CuentasPorPagarModel::CUENTA_POR_PAGAR_ID] = $cuenta_por_pagar->id;
        $request[AsientoModel::TOTAL_FACTURA_SIN_IVA] =  $importe_sin_iva;
        $request[AsientoModel::TOTAL_IVA_FACTURA] =  $iva;
        $request[AsientoModel::TOTAL_FACTURA] =  $importe;

        if ($proveedor->tipo_proveedor == 1) {
            $api_asientos = $this->servicio_asientos->servicioPolizaDevolucionFordPlanta($request);
        } else if ($proveedor->tipo_proveedor == 2) {
            $api_asientos = $this->servicio_asientos->servicioPolizaDevolucionProveedorExterno($request);
        }

        return $api_asientos ? json_decode(current($api_asientos)) : [];
    }
    function calcularIva($importe)
    {
        $moneda_abono_sin_iva = 0;
        $moneda_abono_iva = 0;
        # PARCHE PARA CALCULAR IVA
        $i = $importe - ($importe * 0.165);
        $i_fin = $importe - ($importe * 0.10);
        $error = 0;

        while ($i < $i_fin) :
            $porcentaje = (float)($i * 0.16) + $i;
            if ($porcentaje >= $importe) {
                $moneda_abono_iva = $i * 0.16;
                $moneda_abono_sin_iva = $importe - $moneda_abono_iva;
                $i = $i_fin;
            }

            $i = $i + 0.01;
            $error = $error + 1;

            if ($error >= 30000) {
                $i = $i_fin;
            }
        endwhile;

        return ['iva' => $moneda_abono_iva, 'subtotal' => $moneda_abono_sin_iva];
    }
    public function showByOrdenCompraId($orden_compra_id)
    {
        try {
            $result = $this->servicio->getByOrdenCompraId($orden_compra_id);
            return Respuesta::json($result, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function searchByDates(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasBusquedaTraspaso());
            $result = $this->servicio->getDevolucionProveedorByFechas($request);
            return Respuesta::json($result, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function totalesSearchByDates(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasBusquedaTraspaso());
            $result = $this->servicio->getTotalesDevolucionProveedorByFechas($request);
            return Respuesta::json($result, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
