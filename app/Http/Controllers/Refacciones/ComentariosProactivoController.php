<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Refacciones\ServicioComentariosProactivo;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use Illuminate\Http\Request;

class ComentariosProactivoController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioComentariosProactivo();
    }

    public function getByCotizadorRefaccionesID(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasPorCotizadorRefaccionID());
            $modelo = $this->servicio->getByCotizadorRefaccion($request->get('cotizador_refaccion_id'));
            if ($modelo) {
                return Respuesta::json($modelo, 200);
            }
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
