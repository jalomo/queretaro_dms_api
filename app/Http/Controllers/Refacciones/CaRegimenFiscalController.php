<?php

namespace App\Http\Controllers\Refacciones;

use Illuminate\Http\Request;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Refacciones\ServicioRegimenFiscal;
use App\Servicios\Core\Respuestas\Respuesta;

class CaRegimenFiscalController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioRegimenFiscal();
    }

    public function getBusqueda(Request $request)
    {
        try {
            $modelo = $this->servicio->getBusqueda($request);
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

}
