<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Refacciones\ServicioFolios;
use Illuminate\Http\Request;

class FoliosController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioFolios();
    }

    //ESPECIFICAR TIPO CUENTA
    public function generarFolio()
    {
        try {
            $data = $this->servicio->generarFolio();
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($data, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getByFolio($folio)
    {
        try {
            $data = $this->servicio->getByFolio($folio);
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
    public function getFolioByString(Request $request)
    {
        try {
            $data = $this->servicio->getFolioByString($request->get('folio'));
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
