<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\ProductosPedidosModel;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Refacciones\ServicioPedidoSugerido;
use App\Servicios\Refacciones\ServicioProductoPedidos;
use App\Servicios\Refacciones\ServicioProductos;
use Illuminate\Http\Request;

class ProductoPedidosController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioProductoPedidos();
        $this->servicioPedidoSugerido = new ServicioPedidoSugerido();
        $this->servicioProductos = new ServicioProductos();
    }

    public function getpedidoproductos(Request $request)
    {
        //guarda el pedido sugerido
        $this->servicioPedidoSugerido->dataproductopedidosugerido();
        // dd($this->servicio->filterQuerys($request->only(['proveedor_id','estatus_id','ma_pedido_id'])));
        return Respuesta::json(['data' => $this->servicio->filterQuerys($request->only(['proveedor_id', 'estatus_id', 'ma_pedido_id']))], 200);
    }

    public function store(Request $request)
    {
        try {
            
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
            if (empty($request->id)) {
                $modelo = $this->servicio->store($request->all());
                $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
                return Respuesta::json($modelo, 201, $mensaje);
            } else {
                $modelo = $this->servicio->massUpdateWhereId(ProductosPedidosModel::ID, $request->id, $request->except([ProductosPedidosModel::ID]));
                return Respuesta::json($modelo, 200);
            }
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
