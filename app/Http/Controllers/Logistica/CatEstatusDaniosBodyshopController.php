<?php

namespace App\Http\Controllers\Logistica;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Logistica\ServicioCatEstatusDaniosBS;
use Illuminate\Http\Request;

class CatEstatusDaniosBodyshopController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioCatEstatusDaniosBS();
    }
}
