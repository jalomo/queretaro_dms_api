<?php
namespace App\Http\Controllers\Logistica;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Logistica\ServicioLogistica;
use Illuminate\Http\Request;

class LogisticaController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioLogistica();
    }
    public function getByParametros(Request $request){
        return Respuesta::json($this->servicio->getLogistica($request->all()), 200);
    }
    public function getHorarios(Request $request){
        return Respuesta::json($this->servicio->getHorarios($request->all()), 200);
    }
    public function horariosOCupados(Request $request)
    {
        try {
            $modelo = $this->servicio->getHorariosOcupadosCarril($request->all());
            return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
    public function existeReservacion(Request $request){
        try {
            $modelo = $this->servicio->validarReservacion($request->all());
            return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
