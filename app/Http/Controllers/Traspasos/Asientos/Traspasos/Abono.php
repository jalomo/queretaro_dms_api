<?php 

namespace App\Http\Controllers\Traspasos\Asientos\Traspasos;

use App\Http\Controllers\Traspasos\Asientos\ContabilidadInterface;
use App\Http\Controllers\Traspasos\Asientos\AgregarAsientoTrait;
use App\Http\Controllers\Traspasos\Asientos\CantidadInterface;
use App\Http\Controllers\Traspasos\Asientos\ClienteInterface;
use App\Http\Controllers\Traspasos\Asientos\CostoPromedioInterface;
use App\Http\Controllers\Traspasos\Asientos\FechaInterface;
use Illuminate\Support\Facades\Log;

class Abono implements ContabilidadInterface, ClienteInterface, FechaInterface, CostoPromedioInterface, CantidadInterface
{
    use AgregarAsientoTrait;

    protected $traspaso;
    protected $extra;

    public function __construct($traspaso, array $extra = [])
    {
        $this->traspaso = $traspaso;
        $this->extra = $extra;
    }

    public function getPoliza(): string
    {
        return env('ALMACEN_PRIMARIO') == 'QUERETARO'? 'CS' : 'FH';
    }

    public function getDepartamento(): int
    {
        return 13;
    }

    public function getFolio(): string
    {
        return env('ALMACEN_PRIMARIO') == 'QUERETARO'? "QRT-SNJ-{$this->traspaso->id}" : "SNJ-QRT-{$this->traspaso->id}";
    }

    public function getTipo(): int
    {
        return 1;
    }

    public function getCuenta(): int
    {
        return env('ALMACEN_PRIMARIO') == 'QUERETARO'? 158 : 159;
    }

    public function getConcepto(): string
    {
        if(isset($this->extra['num_pieza']))
        {
            return $this->extra['num_pieza'];
        }

        return 'Traspaso '. env('ALMACEN_PRIMARIO') .' a '. env('ALMACEN_SECUNDARIO');
    }

    public function getEstatus(): string
    {
        return 'APLICADO';
    }

    public function getCliente(): string
    {
        return $this->extra['cliente_id']?? ''; 
    }

    public function getNombreCliente(): string
    {
        return $this->extra['cliente_nombre'] ?? ''; 
    }

    public function getApellidoPaternoCliente(): string
    {
        return $this->extra['cliente_apellido_paterno'] ?? ''; 
    }

    public function getApellidoMaternoCliente(): string
    {
        return $this->extra['cliente_apellido_materno'] ?? ''; 
    }

    public function getFecha(): string
    {
        return $this->extra['fecha']?? '';
    }

    public function getCostoPromedio(): ?float
    {
        return $this->extra['costo_promedio']?? null;
    }

    public function getSucursal(): int
    {
        return env('ALMACEN_PRIMARIO') == 'QUERETARO'? 1 : 2; // 1 Queretaro, 2 San Juan
    }

    public function getCantidad(): ?float
    {
        return $this->extra['cantidad']?? 1;
    }

    public function failedMessage($message = 'La petición para agregar abono de la refaccion a los asientos a contabilidad no fue satisfactoria.'): string
    {
        return tap($message, function($message){
            Log::warning($message, $this->traspaso->toArray());
        });
    }
}