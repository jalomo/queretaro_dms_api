<?php 

namespace App\Http\Controllers\Traspasos\Asientos;

interface CostoPromedioInterface
{
    /**
     * Se envía la fecha
     *
     * @return string
     */

    public function getCostoPromedio(): ?float;
}