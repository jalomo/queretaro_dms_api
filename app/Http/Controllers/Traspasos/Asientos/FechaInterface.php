<?php 

namespace App\Http\Controllers\Traspasos\Asientos;

interface FechaInterface
{
    /**
     * Se envía la fecha
     *
     * @return string
     */

    public function getFecha(): ?string;
}