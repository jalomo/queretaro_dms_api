<?php 

namespace App\Http\Controllers\Traspasos\Asientos;

interface CantidadInterface
{
    /**
     * Se envía la cantidad
     *
     * @return string
     */

    public function getCantidad(): ?float;
}