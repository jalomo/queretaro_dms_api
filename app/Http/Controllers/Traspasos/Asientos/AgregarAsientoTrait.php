<?php 

namespace App\Http\Controllers\Traspasos\Asientos;

use App\Exceptions\AsientosException;
use Exception;
use Illuminate\Support\Facades\Http;

Trait AgregarAsientoTrait
{
    public function make(float $total): Bool
    {
        try
        {
            $contabilidad = Http::asForm()->post(
                env('URL_CONTABILIDAD'). 'asientos/api/agregar', 
                $this->getRequestData() + ['total' => $total]
            );
    
            if($contabilidad->failed())
            {
                throw new AsientosException($this->failedMessage());
            }
    
            if($contabilidad->object()->status == 'error')
            {
                if(is_object($contabilidad->object()->message))
                {
                    $message = implode(', ', (array) $contabilidad->object()->message);
    
                    throw new AsientosException($this->failedMessage($message));
                }
    
                throw new AsientosException($this->failedMessage($contabilidad->object()->message));
            }
    
            logger()->info("Asiento Agregado {$this->getFolio()}", [
                'status' => $contabilidad->object()->status, 'folio' => $this->getFolio(), 'tipo' => $this->getTipo(), 'total' => $total, 'request' => $this->getRequestData()
            ]); 

        } catch(Exception $e)
        {
            logger()->info("ERROR ASIENTO", [
                'line' => $e->getLine(),
                'file' => $e->getFile(),
                'code' => $e->getCode()
            ]);

            return false;
        }

        return true;
    }

    public function getRequestData()
    {
        $data = [
            'clave_poliza' => $this->getPoliza(),
            'tipo' => $this->getTipo(), // 1 Abono, 2 Cargo
            'cuenta' => $this->getCuenta(),
            'concepto' => $this->getConcepto(),
            'departamento' => $this->getDepartamento(),
            'estatus' => $this->getEstatus(),
            'folio' => $this->getFolio(),
            'cliente_id' => $this->getCliente(),
            'cliente_nombre' => method_exists($this, 'getNombreCliente')? $this->getNombreCliente() : null,
            'cliente_apellido1' => method_exists($this, 'getApellidoPaternoCliente')? $this->getApellidoPaternoCliente() : null,
            'cliente_apellido2' => method_exists($this, 'getApellidoMaternoCliente')? $this->getApellidoMaternoCliente() : null,
            'fecha' => method_exists($this, 'getFecha')? $this->getFecha() : null,
            'cantidad' => method_exists($this, 'getCantidad')? $this->getCantidad() : null,
            'costo_promedio' => method_exists($this, 'getCostoPromedio')? $this->getCostoPromedio() : null,
            'sucursal_id' => $this->getSucursal(),
        ];

        return array_filter($data, fn($item) => ! empty($item));
    }
}