<?php

namespace App\Http\Controllers\Traspasos;

use App\Http\Controllers\Core\Controller;
use App\Http\Controllers\Traspasos\Asientos\Traspasos\Cargo;
use App\Http\Controllers\Traspasos\Asientos\Traspasos\Abono;
use App\Http\Controllers\Traspasos\Handler\AumentarTraspaso;
use App\Http\Controllers\Traspasos\Handler\AvisarAlmacenSecundario;
use App\Http\Controllers\Traspasos\Handler\DescontarTraspaso;
use App\Http\Controllers\Traspasos\Handler\EnviarTraspasoContabilidad;
use App\Http\Controllers\Traspasos\Handler\TraspasoEnviado;
use App\Http\Controllers\Traspasos\Handler\ValidarRefaccionesTraspaso;
use App\Http\Requests\Traspasos\SolicitudTraspasoRequest;
use Illuminate\Http\Request;
use App\Models\Refacciones\Traspasos;
use App\Models\Refacciones\VentaProductoModel;
use App\Models\Refacciones\Almacenes;
use App\Models\Refacciones\EstatusTraspaso;
use App\Models\Refacciones\ReEstatusTraspaso;
use App\Models\Refacciones\TraspasoProducto;
use App\Models\Refacciones\FoliosModel;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\DataTablesResource;
use Illuminate\Support\Facades\Http;
use App\Http\Requests\Traspasos\TraspasoUpdateRequest;
use App\Pdf\PdfInterface;
use App\Servicios\Refacciones\ServicioFolios;
use App\Models\Contabilidad\CatalogoProcesosModel;
use App\Http\Requests\Traspasos\StoreTraspasoRequest;
use App\Models\Refacciones\ProductosModel;
use App\Servicios\Inventario\CancelarTraspasoService;
use App\Servicios\Inventario\CancelarTraspasadoService;
use App\Servicios\Producto\CrearProductoService;
use Exception;
use Illuminate\Support\Facades\Log;
use Throwable;

class TraspasoController extends Controller
{
    /**
     * Mostramos la lista de traspasos
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $estatus = ReEstatusTraspaso::
        select([
            DB::raw('MAX(id) as id')
        ])
        ->groupBy('traspasos_id');

        $latestEstatus = ReEstatusTraspaso::
            joinSub($estatus, 'estatus', function($join){
                $join->on('re_estatus_traspaso.id', '=', 'estatus.id');
            })
            ->select([
                'traspasos_id',
                'estatus_id'
            ]);

        $productos = TraspasoProducto::
            groupBy('traspasos_id')
            ->select([
                'traspasos_id',
                DB::raw('SUM(cantidad) as cantidad')
            ]);

        $traspasos = Traspasos::join('almacen as origen', 'traspasos.origen_almacen_id', '=', 'origen.id')
        ->join('almacen as destino', 'traspasos.destino_almacen_id', '=', 'destino.id')
        ->leftJoinSub($latestEstatus, 'latestEstatus', function($join){
            $join->on('latestEstatus.traspasos_id', '=', 'traspasos.id');
        })
        ->leftJoin('estatus_traspaso', 'estatus_traspaso.id', 'latestEstatus.estatus_id')
        ->joinSub($productos, 'traspaso_productos', function($join){
            $join->on('traspasos.id', '=', 'traspaso_productos.traspasos_id');
        })
        ->select(
            'traspasos.id',
            'traspasos.folio',
            'traspasos.venta_id',
            'traspasos.traspaso_directo',
            'traspaso_productos.cantidad',
            'origen.ubicacion as origen',
            'destino.ubicacion as destino',
            'estatus_traspaso.nombre as estatus',
            'traspasos.created_at',
        )
        ->latest('traspasos.created_at')
        ->paginate();

        return response()->json(
            new DataTablesResource($traspasos)
        );
    }

    /**
     * Obtenemos el resumen de las refacciones por venta
     *
     * @return \App\Http\Resources\DataTablesResource
     */
    public function refacciones_ventas()
    {
        $traspasos = FoliosModel::
            join('venta_producto', 'folios.id', 'venta_producto.folio_id')
            ->select([
                DB::raw('MAX(venta_producto.venta_id) as venta_id'),
                'folios.folio',
                DB::raw('SUM(venta_producto.cantidad) as cantidad'),
            ])
            ->where('venta_producto.almacen_id', '<>', 1)
            ->groupBy('folios.id')
            ->orderBy('folios.id', 'desc')
            ->paginate();

        return response()->json(
            new DataTablesResource($traspasos)
        );
    }

    /**
     * Crea el traspaso pasando el id de la venta
     *
     * @param int $venta_id
     * @return array [id, message]
     */
    public function store($venta_id)
    {
        $productos = VentaProductoModel::
            join('producto', 'venta_producto.producto_id', 'producto.id')
            ->join('ventas', 'ventas.id', 'venta_producto.venta_id')
            ->join('folios', 'folios.id', 'ventas.folio_id')
            ->where('venta_producto.venta_id', $venta_id)
            ->where('venta_producto.almacen_id', '<>', 1)
            ->get([
                'folios.folio',
                'producto.no_identificacion',
                'venta_producto.cantidad',
                'venta_producto.valor_unitario',
            ]);
        
        // Si no encontramos traspasos devolvemos error
        if($productos->count() < 1)
        {
            return response()->json([
                'message' => 'No existen productos para realizar traspasos.'
            ], 400);
        }

        try{
            DB::beginTransaction();

            // Creamos el traspaso
            $traspaso = Traspasos::updateOrCreate([
                'folio' => $productos->first()->folio,
            ],
            [
                'folio' => $productos->first()->folio,
                'venta_id' => $venta_id,
                'origen_almacen_id' => env('ALMACEN_PRIMARIO') == "QUERETARO"? Almacenes::QUERETARO : Almacenes::SAN_JUAN,
                'destino_almacen_id' => env('ALMACEN_PRIMARIO') == "QUERETARO"? Almacenes::SAN_JUAN : Almacenes::QUERETARO,
            ]);
    
            // Guardamos todos los productos encontrados
            $traspaso->productos()->delete();
            foreach($productos as $producto)
            {
                $traspaso->productos()->create($producto->toArray());
            }

            // Cambiamos estatus
            $traspaso->estatus()->create([
                'estatus_id' => EstatusTraspaso::GENERADO,
            ]);

            DB::commit();

            return response()->json([
                'id' => $traspaso->id,
                'message' => 'Se agregaron los productos.'
            ], 201);
        } catch(\Throwable $e){
            DB::rollback();
        }

        return response()->json([
            'id' => null,
            'message' => 'No se agregaron los productos.'
        ], 400);
    }

    /**
     * Se crea el traspaso con los productos
     *
     * @param StoreRequest $request
     * @return void
     */
    public function storeTraspaso(StoreTraspasoRequest $request, ServicioFolios $servicio)
    {
        try
        {
            DB::beginTransaction();

            $folio = $servicio->generarFolio(CatalogoProcesosModel::PROCESO_TRASPASO);

            // Creamos el traspaso
            $traspaso = Traspasos::create([
                'folio' => $folio->folio,
                'venta_id' => null,
                'origen_almacen_id' => env('ALMACEN_PRIMARIO') == "QUERETARO"? Almacenes::QUERETARO : Almacenes::SAN_JUAN,
                'destino_almacen_id' => env('ALMACEN_PRIMARIO') == "QUERETARO"? Almacenes::SAN_JUAN : Almacenes::QUERETARO,
                'traspaso_directo' => true,
            ]);
    
            foreach($request->productos as $prod)
            {
                $producto = ProductosModel::find($prod['id']);

                $traspaso->productos()->create([
                    'traspasos_id' => $traspaso->id,
                    'no_identificacion' => $producto->no_identificacion,
                    'cantidad' => $prod['cantidad'],
                    'valor_unitario' => $producto->costo_promedio,
                ]);
            }

            // Cambiamos estatus
            $traspaso->estatus()->create([
                'estatus_id' => EstatusTraspaso::GENERADO,
            ]);

            DB::commit();

            return response()->json([
                'id' => $traspaso->id,
                'message' => 'Se agregaron los productos.'
            ], 201);

        } 
        catch(\Throwable $e)
        {
            DB::rollback();

            Log::warning("{$e->getMessage()}, {$e->getLine()}, {$e->getFile()}");
        }

        return response()->json([
            'id' => null,
            'message' => 'No se agregaron los productos.'
        ], 400);
    }

    /**
     * Envia traspasos a almacen secundario y realiza
     * notificacion a almacen secundario
     *
     * @param Traspasos $traspaso
     * @return array [$id, $message]
     */
    public function send(Request $request, Traspasos $traspaso)
    {
        $productos = $traspaso->productos()->get();
        $traspaso->firma_solicitud = $request->firma;

        $response = Http::acceptJson()->post(env('URL_ALMACEN_SECUNDARIO'). '/api/traspaso/solicitud', [
            'folio' => $traspaso->folio,
            'productos' => $productos->map(function($refaccion){
                return [
                    'no_identificacion' => $refaccion->producto->no_identificacion,
                    'grupo' => $refaccion->producto->gpo,
                    'prefijo' => $refaccion->producto->prefijo,
                    'basico' => $refaccion->producto->basico,
                    'sufijo' => $refaccion->producto->sufijo,
                    'cantidad' => $refaccion->cantidad,
                ];
            })
            ->toArray(),
            'firma' => $request->firma,
        ]);

        if($response->successful() && $traspaso->save())
        {

            $traspaso->estatus()->create([
                'estatus_id' => EstatusTraspaso::FIRMA_SOLICITADO,
            ]);

            return response()->json([
                'id' => $traspaso->id,
                'message' => 'Se realizo la peticion de traspaso de refacciones.'
            ]);
        }

        logger()->error('ERROR TRASPASO SOLICITUD', ['body' => $response->body()]);

        return response()->json([
            'message' => 'Ocurrio un error al realizar la peticion de traspasos.'
        ], 400);
    }

    /**
     * Crea solicitud de traspaso
     *
     * @param Request $request
     * @return array [ $message ]
     */
    public function solicitud(SolicitudTraspasoRequest $request)
    {
        try
        {
            DB::beginTransaction();
            
            $traspaso = Traspasos::updateOrCreate([
                'folio' => $request->folio,
            ],
            [
                'folio' => $request->folio,
                'origen_almacen_id' => env('ALMACEN_PRIMARIO') == "QUERETARO"? Almacenes::SAN_JUAN : Almacenes::QUERETARO,
                'destino_almacen_id' => env('ALMACEN_PRIMARIO') == "QUERETARO"? Almacenes::QUERETARO : Almacenes::SAN_JUAN,
                'firma_solicitud' => $request->firma,
            ]);
    
            $traspaso->productos()->delete();

            $productos = $request->productos?? [];
            
            // Fix: seleccionamos la cantidad que se va a entregar
            foreach($productos as &$producto)
            {
                $producto['cantidad_entregada'] = $producto['cantidad'];

                // Creamos refaccion en el catalogo del almacen si no existe
                $refaccion = CrearProductoService::create([
                    'no_identificacion' => $producto['no_identificacion'],
                    'grupo' => $producto['grupo'],
                    'prefijo' => $producto['prefijo'],
                    'basico' => $producto['basico'],
                    'sufijo' => $producto['sufijo'],
                ]);
            }

            $traspaso->productos()->createMany($productos);

            $traspaso->estatus()->create([
                'estatus_id' => EstatusTraspaso::FIRMA_SOLICITADO
            ]);

            DB::commit();
        } 
        catch(\Throwable $e)
        {
            DB::rollback();

            return response()->json([
                'message' => 'Error en la recepcion de la solicitud de traspaso.',
                'code' => $e->getMessage(),
            ], 400);
        }

        return response()->json([
            'message' => 'Recepcion correcta de solicitud.'
        ], 201);
    }

    /**
     * Finaliza traspaso en almacen primario
     *
     * @param Traspasos $traspaso
     * @return void
     */
    public function finalizar(Traspasos $traspaso)
    {
        $traspaso->estatus()->create([
            'estatus_id' => EstatusTraspaso::FINALIZADO,
        ]);

        return response()->json([
            'message' => 'Traspaso finalizado correctamente.'
        ]);
    }

    /**
     * Cancela traspaso y envía petición de cancelacion a almacen secundario
     *
     * @param Traspasos $traspaso
     * @return array [ $message ]
     */
    public function cancel(Traspasos $traspaso)
    {
        $response = Http::delete(env('URL_ALMACEN_SECUNDARIO'). '/api/traspaso/cancelacion-sucursal', [
            'folio' => $traspaso->folio,
        ]);

        if($response->successful())
        {
            $traspaso->estatus()->create([
                'estatus_id' => EstatusTraspaso::CANCELADO
            ]);

            return response()->json([
                'id' => $traspaso->id,
                'message' => 'Traspaso cancelado correctamente.'
            ]);
        }

        return response()->json([
            'message' => 'Ocurrio un error al cancelar el pedido.'
        ], 400);
    }

    /**
     * Cancela folio en almacen primario
     * petición viene del almacen secundario
     * 
     * @param Request $request
     * @return void
     */
    public function cancel_folio(Request $request)
    {
        $traspaso = Traspasos::where('folio', $request->folio)->firstOrFail();

        $traspaso->estatus()->create([
            'estatus_id' => EstatusTraspaso::CANCELADO
        ]);

        return response()->json([
            'message' => 'Se cancelo el traspaso correctamente.'
        ]);
    }

    /**
     * Envía la petición de traspaso a almacen secundario
     *
     * @param Traspasos $traspaso
     * @return array [ id, message]
     */
    public function enviado(Traspasos $traspaso)
    {
        try
        {
            // Inicio Transaccion
            DB::beginTransaction();

            $handler = new ValidarRefaccionesTraspaso;
            $handler->nextHandler(new AvisarAlmacenSecundario)
                ->nextHandler(new EnviarTraspasoContabilidad)
                ->nextHandler(new DescontarTraspaso)
                ->nextHandler(new TraspasoEnviado);

            // Capturamos la respuesta para poder realizar el commit de la transaccion
            $response = $handler->handle($traspaso);

            // Commit Transaccion
            DB::commit();

            return $response;
        } 
        catch(Exception $exception) 
        {
            DB::rollBack();

            logger()->warning("ERROR ENVIAR TRASPASO $traspaso->id", ['message' => $exception->getMessage(), 'file' => $exception->getFile(), 'line' => $exception->getFile()]);

            return response()->json(['message' => 'Ocurrio un error al enviar el traspaso.'], 400);
        }
    }

    /**
     * Se cambia estatus de traspaso a enviado
     * petición viene del almacen secundario
     * 
     * @param Request $request
     * @return void
     */
    public function enviado_folio(Request $request)
    {
        $traspaso = Traspasos::where('folio', $request->folio)->first();

        try
        {
            // Inicio Transaccion
            DB::beginTransaction();

            $handler = new AumentarTraspaso($request);
            $handler->nextHandler(new TraspasoEnviado);

            // Capturamos la respuesta para poder realizar el commit de la transaccion
            $response = $handler->handle($traspaso);

            // Commit Transaccion
            DB::commit();
            
            return $response;
        } 
        catch(Throwable $exception)
        {
            DB::rollBack();

            logger()->warning("ERROR ENVIADO FOLIO TRASPASO $traspaso->id", ['message' => $exception->getMessage(), 'file' => $exception->getFile(), 'line' => $exception->getFile()]);

            return response()->json(['message' => 'Ocurrio un error al enviar el traspaso.'], 400);
        }

    }

    /**
     * Inicia el proceso de devolución del traspaso y hace la notificacion a
     * la sucursal secundaria
     *
     * @param Traspasos $traspaso
     * @return array [ id, message ]
     */
    public function devolucion(Traspasos $traspaso)
    {
        $productos = $traspaso->productos()->get();

        $response = Http::post(env('URL_ALMACEN_SECUNDARIO'). '/api/traspaso/devolucion-sucursal', [
            'folio' => $traspaso->folio,
            'productos' => $productos, 
        ]);

        if($response->successful())
        {
            $traspaso->estatus()->create([
                'estatus_id' => EstatusTraspaso::ENVIO_DEVOLUCION
            ]);

            return response()->json([
                'id' => $traspaso->id,
                'message' => 'Se realizo petición de devolución.'
            ]);
        }

        return response()->json([
            'message' => 'No se realizo el cambio a devolución.'
        ], 400);
    }

    /**
     * Cambio de estatus en solicitud a envío en devolución
     * petición viene del almacen secundario
     * 
     * @param Request $request
     * @return array [ id, message ]
     */
    public function devolucion_folio(Request $request)
    {
        $traspaso = Traspasos::where('folio', $request->folio)->firstOrFail();
        
        $traspaso->estatus()->create([
            'estatus_id' => EstatusTraspaso::ENVIO_DEVOLUCION
        ]);

        $productos = $traspaso->productos()->get();
        $devuelto = collect($request->productos)->pluck('cantidad_devuelta', 'no_identificacion');

        foreach($productos as $producto)
        {
            $producto->cantidad_devuelta = $devuelto[$producto->no_identificacion];
            $producto->save();
        }

        return response()->json([
            'id' => $traspaso->id,
            'message' => 'Traspaso en devolucion.'
        ]);
    }

    /**
     * Se confirma la devolución del traspaso y se notifica al almacen secundario
     *
     * @param Traspasos $traspaso
     * @return array [ id, message ]
     */
    public function devuelto(Traspasos $traspaso)
    {
        $response = Http::post(env('URL_ALMACEN_SECUNDARIO'). '/api/traspaso/devuelto-sucursal', [
            'folio' => $traspaso->folio,
        ]);

        if($response->successful())
        {
            $traspaso->estatus()->create([
                'estatus_id' => EstatusTraspaso::DEVUELTO
            ]);

            $productos = $traspaso->productos()->get();

            foreach($productos as $producto)
            {
                if($producto->cantidad_devuelta > 0)
                {
                    $traspasado = new CancelarTraspasoService;
                    $traspasado->id($producto->id);
                    $traspasado->handle($producto->no_identificacion, $producto->cantidad_devuelta, $producto->valor_unitario);
                }
            }

            return response()->json([
                'id' => $traspaso->id,
                'message' => 'Traspaso devuelto.'
            ]);
        }

        return response()->json([
            'message' => 'No se realizo el cambio a devuelto.'
        ], 400);
    }

    /**
     * Se cambia estatus a devuelto petición viene del almacen secundario
     *
     * @param Request $request
     * @return array [ id, message ]
     */
    public function devuelto_folio(Request $request)
    {
        $traspaso = Traspasos::where('folio', $request->folio)->firstOrFail();
        $productos = $traspaso->productos()->get();
        
        $traspaso->estatus()->create([
            'estatus_id' => EstatusTraspaso::DEVUELTO
        ]);

        foreach($productos as $producto)
        {
            if($producto->cantidad_devuelta > 0)
            {
                $traspasar = new CancelarTraspasadoService;
                $traspasar->id($producto->id);
                $traspasar->handle($producto->no_identificacion, $producto->cantidad_devuelta, $producto->valor_unitario);
            }
        }

        return response()->json([
            'id' => $traspaso->id,
            'message' => 'Traspaso devuelto.'
        ]);
    }

    /**
     * Mostramos las refacciones del traspaso
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function refacciones($id)
    {
        $traspasos = Traspasos::
            join('traspaso_productos', 'traspasos.id', 'traspaso_productos.traspasos_id')
            ->join('producto', 'producto.no_identificacion', 'traspaso_productos.no_identificacion')
            ->where('traspasos.id', $id)
            ->select(
                'traspaso_productos.id', 
                'folio', 
                'producto.no_identificacion', 
                'traspaso_productos.cantidad', 
                'traspaso_productos.cantidad_entregada', 
                'traspaso_productos.cantidad_devuelta', 
                'traspaso_productos.valor_unitario', 
                'traspaso_directo',
                'venta_id', 
                'descripcion'
            )
            ->paginate();

        return response()->json(new DataTablesResource($traspasos));
    }

    /**
     * Se actualiza el pedido de traspaso
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TraspasoUpdateRequest $request, $id)
    {
        $traspaso = Traspasos::findOrFail($id);

        if($traspaso->update($request->validated()))
        {
            return response()->json([
                'id' => $traspaso->id,
                'message' => 'Se actualizo el traspaso',
            ], 200);    
        }

        return response()->json([
            'id' => $traspaso->id,
            'message' => 'Ocurrio un error al actualzar el traspaso',
        ], 200);
    }

    public function poliza_traspaso(Traspasos $traspaso, PdfInterface $pdf)
    {
        try{
            $productos = $traspaso->productos()->get();

            $html = view('polizas.traspasos', [
                'productos' => $productos,
                'traspaso' => $traspaso,
                'queretaro' => Almacenes::QUERETARO,
                'san_juan' => Almacenes::SAN_JUAN,
            ])
            ->render();
    
            $pdf->html($html);

        } catch(\Throwable $e) {
            return response()->json([
                'message' => 'Ocurrio un problema al generar la poliza de traspaso.',
                'code' => $e->getMessage(),
                'line' => $e->getLine(),
                'file' => $e->getFile(),
            ], 400);
        }

        return $pdf->output();
    }

    public function autorizar(Request $request, Traspasos $traspaso)
    {
        try{
            $traspaso->firma_autorizacion = $request->firma;

            $response = Http::post(env('URL_ALMACEN_SECUNDARIO'). '/api/traspaso/autorizado', [
                'folio' => $traspaso->folio,
                'firma' => $request->firma,
            ]);

            if($traspaso->save() && $response->successful()) 
            {
                $traspaso->estatus()->create([
                    'estatus_id' => EstatusTraspaso::FIRMA_AUTORIZADO
                ]);

                return response()->json([
                    'id' => $traspaso->id,
                    'message' => 'El traspaso fue autorizado.'
                ]);
            }

            return response()->json([
                'id' => $traspaso->id,
                'message' => 'El traspaso no fue autorizado'
            ], 400);
            
        } catch(\Throwable $e) {
            return response()->json([
                'message' => 'Ocurrio un error al autorizar el traspaso.'
            ], 400);
        }
    }

    public function autorizado(Request $request)
    {
        $traspaso = Traspasos::where('folio', $request->folio)->where(function($q){
            $q->whereNotNull('venta_id');
            $q->orWhere('traspaso_directo', true);
        })
        ->first();

        $traspaso->firma_autorizacion = $request->firma;

        if($traspaso->save())
        {
            $traspaso->estatus()->create([
                'estatus_id' => EstatusTraspaso::FIRMA_AUTORIZADO
            ]);

            return response()->json([
                'message' => 'El traspaso se registro como autorizado.'
            ]);
        }

        return response()->json([
            'message' => 'El traspaso no se registro como autorizado.'
        ], 400);
    }

    public function subir_evidencia(Request $request, Traspasos $traspaso)
    {
        $traspaso->evidencia = $request->file('archivo')->store('traspasos');

        if($traspaso->save())
        {
            $traspaso->estatus()->create([
                'estatus_id' => EstatusTraspaso::SUBIR_EVIDENCIA,
            ]);

            return response()->json([
                'message' => 'Evidencia guardada.',
            ]);
        }

        return response()->json([
            'message' => 'No se guardo la evidencia.'
        ], 400);
    }

    public function descargar_evidencia(Traspasos $traspaso)
    {
        return response()->download(storage_path("app/$traspaso->evidencia"));
    }

    public function disminuir($producto)
    {
        $producto = TraspasoProducto::find($producto);

        if($producto->cantidad <= 0)
        {
            return response()->json([
                'message' => 'La cantidad de producto disponible a descontar es de cero.'
            ], 400);
        }
        
        $producto->cantidad -= 1;
        
        if($producto->save())
        {
            return response()->json([
                'message' => 'Se disminuyo la cantidad correctamente.'
            ]);
        }

        return response()->json([
            'message' => 'Ocurrio un error al disminuir la refacción.'
        ], 400);
        
    }

    public function aumentar($id)
    {
        $producto = TraspasoProducto::find($id);
        $producto->cantidad += 1;
        
        if($producto->save())
        {
            return response()->json([
                'message' => 'Se aumento la cantidad correctamente.'
            ]);
        }

        return response()->json([
            'message' => 'Ocurrio un error al aumentar la refacción.'
        ], 400);   
    }

    public function disminuir_entregado($producto)
    {
        $producto = TraspasoProducto::find($producto);

        if($producto->cantidad_entregada <= 0)
        {
            return response()->json([
                'message' => 'La cantidad de producto disponible a descontar es de cero.'
            ], 400);
        }
        
        $producto->cantidad_entregada -= 1;
        
        if($producto->save())
        {
            return response()->json([
                'message' => 'Se disminuyo la cantidad correctamente.'
            ]);
        }

        return response()->json([
            'message' => 'Ocurrio un error al disminuir la refacción.'
        ], 400);
        
    }

    public function aumentar_entregado($id)
    {
        $producto = TraspasoProducto::find($id);
        $producto->cantidad_entregada += 1;
        
        if($producto->save())
        {
            return response()->json([
                'message' => 'Se aumento la cantidad correctamente.'
            ]);
        }

        return response()->json([
            'message' => 'Ocurrio un error al aumentar la refacción.'
        ], 400);   
    }

    public function disminuir_devuelto($producto)
    {
        $producto = TraspasoProducto::find($producto);

        if($producto->cantidad_devuelta <= 0)
        {
            return response()->json([
                'message' => 'La cantidad de producto disponible a descontar es de cero.'
            ], 400);
        }
        
        $producto->cantidad_devuelta -= 1;
        
        if($producto->save())
        {
            return response()->json([
                'message' => 'Se disminuyo la cantidad correctamente.'
            ]);
        }

        return response()->json([
            'message' => 'Ocurrio un error al disminuir la refacción.'
        ], 400);
        
    }

    public function aumentar_devuelto($id)
    {
        $producto = TraspasoProducto::find($id);
        $producto->cantidad_devuelta += 1;
        
        if($producto->save())
        {
            return response()->json([
                'message' => 'Se aumento la cantidad correctamente.'
            ]);
        }

        return response()->json([
            'message' => 'Ocurrio un error al aumentar la refacción.'
        ], 400);   
    }

    public function contabilidad(Traspasos $traspaso)
    {
        $productos = $traspaso->productos()->get();
        $total_costo_promedio = 0;
        // $suma_costos_promedio = 0;
        $total_cantidad = 0;

        // Envio a contabilidad de las refacciones
        $productos->each(function($refaccion) use($traspaso, $total_cantidad, $total_costo_promedio){
            $abono = new Abono($traspaso, [
                'num_pieza' => "{$refaccion->producto->no_identificacion} - {$refaccion->producto->descripcion}",
                'cantidad' => $refaccion->cantidad,
                'costo_promedio' => $refaccion->producto->costo_promedio,
                'cliente_nombre' => env('QUERETARO')? 'MYLSA QUERETARO' : 'MYLSA SAN JUAN',
            ]);
            $abono->make($refaccion->producto->costo_promedio * $refaccion->cantidad);

            $cargo = new Cargo($traspaso, [
                'num_pieza' => "{$refaccion->producto->no_identificacion} - {$refaccion->producto->descripcion}",
                'cantidad' => $refaccion->cantidad,
                'costo_promedio' => $refaccion->producto->costo_promedio,
                'cliente_nombre' => env('QUERETARO')? 'MYLSA QUERETARO' : 'MYLSA SAN JUAN',
            ]); 
            $cargo->make($refaccion->producto->costo_promedio * $refaccion->cantidad);

            $total_cantidad += $refaccion->cantidad;
            // $suma_costos_promedio += $refaccion->producto->costo_promedio;
            $total_costo_promedio += $refaccion->producto->costo_promedio * $refaccion->cantidad;

        });

        // Envío en asiento de los totales
        $abono = new Abono($traspaso, [
            'cantidad' => $total_cantidad,
            // 'costo_promedio' => $suma_costos_promedio,
            'cliente_nombre' => env('QUERETARO')? 'MYLSA QUERETARO' : 'MYLSA SAN JUAN',
        ]);
        $abono->make($total_costo_promedio);

        $cargo = new Cargo($traspaso, [
            'cantidad' => $total_cantidad,
            // 'costo_promedio' => $suma_costos_promedio,
            'cliente_nombre' => env('QUERETARO')? 'MYLSA QUERETARO' : 'MYLSA SAN JUAN',
        ]); 
        $cargo->make($total_costo_promedio);

        return response()->json([
            'message' => 'El envío a contabilidad de las refacciones realizado',
        ]);
    }
}
