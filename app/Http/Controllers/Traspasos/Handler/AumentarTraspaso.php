<?php

namespace App\Http\Controllers\Traspasos\Handler;

use App\Chain\AbstractHandler;
use App\Servicios\Inventario\TraspasadoService;
use Illuminate\Http\JsonResponse;

class AumentarTraspaso extends AbstractHandler
{
    protected $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function execute($traspaso): ?JsonResponse
    {
        $productos = $traspaso->productos()->get();
        $entregado = collect($this->request->productos)->pluck('cantidad_entregada', 'no_identificacion');

        foreach($productos as $producto)
        {
            $producto->cantidad_entregada = $entregado[$producto->no_identificacion];
            $producto->save();

            $traspasado = new TraspasadoService;
            $traspasado->id($producto->id);
            $traspasado->handle($producto->no_identificacion, $producto->cantidad_entregada, $producto->valor_unitario);
        }

        return null;
    }
}