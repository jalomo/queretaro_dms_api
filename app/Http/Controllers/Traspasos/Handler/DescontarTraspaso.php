<?php

namespace App\Http\Controllers\Traspasos\Handler;

use App\Chain\AbstractHandler;
use App\Servicios\Inventario\TraspasarService;
use Illuminate\Http\JsonResponse;

class DescontarTraspaso extends AbstractHandler
{
    public function execute($traspaso): ?JsonResponse
    {
        $productos = $traspaso->productos()->get();

        foreach($productos as $producto)
        {
            $traspasar = new TraspasarService;
            $traspasar->id($producto->id);
            $traspasar->handle($producto->no_identificacion, $producto->cantidad_entregada, $producto->valor_unitario);
        }

        return null;
    }
}