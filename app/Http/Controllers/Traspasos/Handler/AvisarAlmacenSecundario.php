<?php

namespace App\Http\Controllers\Traspasos\Handler;

use App\Chain\AbstractHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Http;

class AvisarAlmacenSecundario extends AbstractHandler
{
    public function execute($traspaso):? JsonResponse
    {
        $productos = $traspaso->productos()->get();

        $response = Http::post(env('URL_ALMACEN_SECUNDARIO'). '/api/traspaso/envio-sucursal', [
            'folio' => $traspaso->folio,
            'productos' => $productos, 
        ]);

        if(! $response->successful())
        {
            logger()->warning("ERROR TRASPASO {$traspaso->id}", ['response' => $response->body()]);

            return response()->json(['message' => __('Error al avisar a la sucursal secundaria.')], 400);
        }

        return null;
    }
}