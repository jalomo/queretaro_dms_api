<?php

namespace App\Http\Controllers\Traspasos\Handler;

use App\Chain\AbstractHandler;
use Illuminate\Http\JsonResponse;
use App\Models\Refacciones\EstatusTraspaso;

class TraspasoEnviado extends AbstractHandler
{
    public function execute($traspaso):? JsonResponse
    {
        $traspaso->estatus()->create([
            'estatus_id' => EstatusTraspaso::ENVIADO
        ]);

        return response()->json([
            'id' => $traspaso->id,
            'message' => 'Traspaso enviado correctamente.'
        ]);
    }
}