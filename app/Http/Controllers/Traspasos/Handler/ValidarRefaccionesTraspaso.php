<?php

namespace App\Http\Controllers\Traspasos\Handler;

use App\Chain\AbstractHandler;
use Illuminate\Http\JsonResponse;

class ValidarRefaccionesTraspaso extends AbstractHandler
{
    public function execute($traspaso):? JsonResponse
    {
        $productos = $traspaso->productos()->get();
        $existencia = [];

        foreach($productos as $refaccion)
        {
            // Vemos si existe inventario para cada refaccion de este traspaso
            if($refaccion->producto->cantidad < $refaccion->cantidad_entregada)
            {
                $existencia[] = "{$refaccion->producto->no_identificacion} sin existencia. para entregar: {$refaccion->cantidad_entregada} existencia: {$refaccion->producto->cantidad}";
                
            }
        }

        // Si exite mensaje de error para esta validacion lanzamos excepcion
        if(! empty($existencia))
        {
            return response()->json(['message' => 'Datos Incorretos.', 'errors' => $existencia], 422);
        }
        
        return null;
    }
}