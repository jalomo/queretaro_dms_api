<?php

namespace App\Http\Controllers\Traspasos\Handler;

use App\Chain\AbstractHandler;
use App\Http\Controllers\Traspasos\Asientos\Traspasos\Abono;
use App\Http\Controllers\Traspasos\Asientos\Traspasos\Cargo;
use Illuminate\Http\JsonResponse;

class EnviarTraspasoContabilidad extends AbstractHandler
 {
    public function execute($traspaso):? JsonResponse
    {
        $productos = $traspaso->productos()->get();

        // Envio a contabilidad de las refacciones
        $productos->each(function($refaccion) use($traspaso){
            $abono = new Abono($traspaso, [
                'num_pieza' => "{$refaccion->producto->no_identificacion} - {$refaccion->producto->descripcion}",
                'cantidad' => $refaccion->cantidad,
                'costo_promedio' => $refaccion->producto->costo_promedio,
                'cliente_nombre' => env('QUERETARO')? 'MYLSA QUERETARO' : 'MYLSA SAN JUAN',
            ]);
            $abono->make($refaccion->producto->costo_promedio * $refaccion->cantidad);

            $cargo = new Cargo($traspaso, [
                'num_pieza' => "{$refaccion->producto->no_identificacion} - {$refaccion->producto->descripcion}",
                'cantidad' => $refaccion->cantidad,
                'costo_promedio' => $refaccion->producto->costo_promedio,
                'cliente_nombre' => env('QUERETARO')? 'MYLSA QUERETARO' : 'MYLSA SAN JUAN',
            ]); 
            $cargo->make($refaccion->producto->costo_promedio * $refaccion->cantidad);
        });

        return null;
    }
 }