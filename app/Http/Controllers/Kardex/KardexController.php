<?php

namespace App\Http\Controllers\Kardex;

use App\Http\Controllers\Core\Controller;
use App\Http\Resources\DataTablesResource;
use App\Models\Refacciones\InventarioProductos;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\Reemplazo;
use Illuminate\Http\Request;

class KardexController extends Controller
{
    public function productos(Request $request)
    {
        $productos = ProductosModel::when($request->no_identificacion, function($q) use ($request){
            $q->where('no_identificacion', "like", "%{$request->no_identificacion}%");
        })
        ->paginate();

        return response()->json(
            new DataTablesResource($productos)
        );
    }

    public function detalles_producto(Request $request, ProductosModel $producto)
    {
        $productos = Reemplazo::where('producto_id', $producto->id)->get()->pluck('reemplazo_id');
        $productos[] = $producto->id;

        $inventario_productos = InventarioProductos::
            with('producto', 'tipo_movimiento')
            ->when($request->fecha_inicio, function($q) use($request) {
                $q->whereDate('created_at', '>=', $request->fecha_inicio);
            })
            ->when($request->fecha_termino, function($q) use($request) {
                $q->whereDate('created_at', '<=', $request->fecha_termino);
            })
            ->whereIn('producto_id', $productos)
            ->oldest('id')
            ->get();

        return response()->json([
            'producto' => $producto->load('desglose_producto', 'ubicacion'),
            'detalle' => $inventario_productos,
        ]);
    }
}