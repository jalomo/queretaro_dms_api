<?php

namespace App\Http\Controllers\Autos;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Models\Autos\AgendaSalidaModel;
use App\Servicios\Autos\ServicioAgendaSalida;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use Illuminate\Http\Request;
use Throwable;

class AgendaSalidaController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioAgendaSalida();
    }

    public function store(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
            $request->merge(['datetime_inicio' => $request->get('datetime_inicio') . ':00']);
            $modelo = $this->servicio->store($request);
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($modelo, 201, $mensaje);
        } catch (Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function borrarSalida(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, [AgendaSalidaModel::DATETIME_INICIO => 'required']);
            $search_by_date = $this->servicio->getEventoPorHora($request->all());
            return Respuesta::json($search_by_date, 204);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
