<?php

namespace App\Http\Controllers\Autos;

use App\Http\Controllers\Core\CrudController;
use App\Models\Autos\EstatusSalidaUnidadModel;
use App\Models\Autos\EstatusVentaAutosModel;
use App\Models\Autos\UnidadesNuevas\RemisionModel;
use App\Models\Autos\VentasAutosModel;
use App\Models\CuentasPorCobrar\AbonosModel;
use App\Models\CuentasPorCobrar\CuentasPorCobrarModel;
use App\Models\CuentasPorPagar\CatEstatusAbonoModel;
use App\Servicios\Autos\ServicioVentaAutos;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\CuentasPorCobrar\ServicioAbono;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VentaAutosController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioVentaAutos();
        $this->servicioAbono = new ServicioAbono();
        $this->modelocxc = new CuentasPorCobrarModel();
        $this->modeloAbonos = new AbonosModel();
    }

    public function store(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
            $modelo = $this->servicio->store($request->all());
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($modelo, 201, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getPrepedidos(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasFiltroPrepedido());
            return Respuesta::json($this->servicio->getPrepedidos($request->all()), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getPrepedidosById($id)
    {
        $modelo = $this->servicio->getPreventaById($id);
        return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
    }

    public function getPrepedidosSeminuevos(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasFiltroPrepedido());
            return Respuesta::json($this->servicio->getPrepedidosSeminuevos($request->all()), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getPrepedidosSeminuevosById($id)
    {
        $modelo = $this->servicio->getPreventaSeminuevoById($id);
        return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
    }

    public function index()
    {
        return Respuesta::json($this->servicio->getVentaAutos(), 200);
    }

    public function show($id)
    {
        $modelo = $this->servicio->getOne($id);
        return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
    }

    public function guardarformatosalida(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasformatosalida());
            return Respuesta::json($this->servicio->guardarFormatosalida($request->all()), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getdataformatosalida($id_venta_unidad)
    {
        try {
            return Respuesta::json($this->servicio->getdataformatosalida($id_venta_unidad), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getClienteVentaAuto()
    {
        try {
            return Respuesta::json($this->servicio->getClienteVentaAuto(), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getClienteVentaAutoById($id_preventa)
    {
        try {
            return Respuesta::json($this->servicio->getClienteVentaAutoById($id_preventa), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getsolicitudestransferencias()
    {
        try {
            return Respuesta::json($this->servicio->get_solicitud_transferencias([]), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getOnesolicitudestransferencia($id_cxc)
    {
        try {
            return Respuesta::json($this->servicio->get_solicitud_transferencias(['id_cxc' => $id_cxc]), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function autorizatransferencia(Request $request, $id_cxc)
    {
        try {
            ParametrosHttpValidador::validar($request, ['id_estatus_autorizado' => 'required']);
            return Respuesta::json($this->servicio->actualizarAutorizacion($id_cxc, $request->get('id_estatus_autorizado')), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function actualizarVentaSinUnidad(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->reglasVentaSinUnidad());

            $data_cxc = $this->modelocxc
                ->select(
                    CuentasPorCobrarModel::getTableName() . '.' . CuentasPorCobrarModel::ID . ' as cuenta_por_cobrar_id',
                    CuentasPorCobrarModel::getTableName() . '.' . CuentasPorCobrarModel::FOLIO_ID,
                    CuentasPorCobrarModel::getTableName() . '.' . CuentasPorCobrarModel::ESTATUS_CUENTA_ID,
                    AbonosModel::getTableName() . '.' . AbonosModel::ID . ' as abono_id',
                    AbonosModel::getTableName() . '.' . AbonosModel::TOTAL_ABONO,
                    AbonosModel::getTableName() . '.' . AbonosModel::TIPO_ABONO_ID,
                    AbonosModel::getTableName() . '.' . AbonosModel::ESTATUS_ABONO_ID,
                    AbonosModel::getTableName() . '.' . AbonosModel::CREATED_AT . ' as fecha_pago',
                )
                ->join(
                    AbonosModel::getTableName(),
                    AbonosModel::getTableName() . '.' . AbonosModel::CUENTA_POR_COBRAR_ID,
                    '=',
                    CuentasPorCobrarModel::getTableName() . '.' . CuentasPorCobrarModel::ID,
                )
                ->where(CuentasPorCobrarModel::getTableName() . '.' . CuentasPorCobrarModel::ID, $request->id)
                ->first();

            if ($data_cxc->estatus_abono_id == CatEstatusAbonoModel::PAGADO) {
                //Si ya se pago se aparta
                DB::table(RemisionModel::getTableName())
                    ->where(RemisionModel::ID,  $request->id_unidad)
                    ->update([
                        RemisionModel::ESTATUS_ID =>  EstatusSalidaUnidadModel::ESTATUS_SALIDA_APARTADO,
                        
                    ]);

                //Actualizamos el estatus
                DB::table(VentasAutosModel::getTableName())
                    ->where(VentasAutosModel::ID, $request->id)
                    ->update([
                        VentasAutosModel::ID_ESTATUS => EstatusVentaAutosModel::ESTATUS_AUTO_PENDIENTE,
                        VentasAutosModel::ID_UNIDAD =>  $request->id_unidad
                    ]);
            } else {
                // si no se pago y se asigna unidad, se asigna el estatus
                DB::table(VentasAutosModel::getTableName())
                    ->where(VentasAutosModel::ID, $request->id)
                    ->update([
                        VentasAutosModel::ID_ESTATUS => EstatusVentaAutosModel::ESTATUS_PRE_PEDIDO,
                        VentasAutosModel::ID_UNIDAD =>  $request->id_unidad
                    ]);
            }
            //TODO: validar los asientos -> valida_flujo_unidades_contado
            return Respuesta::json([], 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
