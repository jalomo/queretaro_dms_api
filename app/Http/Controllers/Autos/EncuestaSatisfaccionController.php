<?php

namespace App\Http\Controllers\Autos;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Autos\ServicioEncuestaSatisfaccion;
use App\Servicios\Core\Respuestas\Respuesta;
use Illuminate\Http\Request;

class EncuestaSatisfaccionController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioEncuestaSatisfaccion();
    }

    public function datosencuestaventa($id_venta)
    {
        try {
            $modelo = $this->servicio->getdatosencuestaventa($id_venta);
            return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
