<?php

namespace App\Http\Controllers\Autos\Inventario;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Autos\Inventario\ServicioInventario;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use Illuminate\Http\Request;

class InventarioController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioInventario();
    }

    public function storeInventarioCompleto(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->reglasStoreformulario());
            $modelo = $this->servicio->storeForumlario($request->all());
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($modelo, 201, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getformulario($id_inventario = 1)
    {
        try {
            $modelo = $this->servicio->getallformdata($id_inventario);
            return Respuesta::json($modelo);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
