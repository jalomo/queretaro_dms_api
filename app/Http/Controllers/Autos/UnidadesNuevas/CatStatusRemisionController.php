<?php

namespace App\Http\Controllers\Autos\UnidadesNuevas;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Autos\UnidadesNuevas\ServicioStatusRemision;
use Illuminate\Http\Request;

class CatStatusRemisionController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioStatusRemision();
    }
}
