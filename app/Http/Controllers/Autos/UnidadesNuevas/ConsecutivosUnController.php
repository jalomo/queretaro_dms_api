<?php

namespace App\Http\Controllers\Autos\UnidadesNuevas;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Autos\UnidadesNuevas\ServicioConsecutivosUN;
use Illuminate\Http\Request;
use App\Servicios\Core\Respuestas\Respuesta;

class ConsecutivosUnController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioConsecutivosUN();
    }
    public function getConsecutivosUnidades(Request $request){
        try {
            return ['data' => $this->servicio->getConsecutivo($request->all()), 200];
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
