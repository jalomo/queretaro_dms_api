<?php

namespace App\Http\Controllers\Financiamientos;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Financiamientos\ServicioCatTipoSeguro;
use Illuminate\Http\Request;

class catTipoSegurosController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioCatTipoSeguro();
    }
}
