<?php

namespace App\Http\Controllers\Estadisticas;

use App\Http\Controllers\Core\Controller;
use App\Http\Requests\Estadisticas\CompraRequest;
use App\Servicios\Ereact\Compras;

class ComprasController extends Controller
{
    public function index(CompraRequest $request, Compras $compra)
    {
        return $compra->json($request);
    }

    public function download(CompraRequest $request, Compras $compra)
    {
        return $compra->download($request);
    }
}