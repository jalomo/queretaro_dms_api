<?php 

namespace App\Http\Controllers\Estadisticas;

use App\Http\Controllers\Core\Controller;
use App\Http\Requests\Estadisticas\TraspasoRequest;
use App\Http\Resources\Estadisticas\TraspasoResource;
use App\Models\Refacciones\TraspasoProducto;
use App\Pdf\Pdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TraspasoController extends Controller
{
    public function index(TraspasoRequest $request)
    {
        // return response()->json($this->search($request));
        return response()->json(TraspasoResource::collection($this->search($request)));
    }

    public function download(Request $request)
    {
        $view = 'estadisticas.traspasos';

        if($request->tipo == 'orden')
            $view = 'estadisticas.traspasos_por_orden';
            
        $pdf = new Pdf;
        $pdf->html(
            view($view, [
                'traspasos' => $this->search($request),
                'fecha_inicio' => $request->fecha_inicio,
                'fecha_termino' => $request->fecha_termino,
            ])
            ->render()
        );

        return $pdf->output();
    }

    public function search(Request $request)
    {
        $pedidos = TraspasoProducto::with([
            'traspaso.almacen_origen', 
            'traspaso.almacen_destino',
            'traspaso',
            'producto',
            ])
            ->whereHas('traspaso', function($q) use ($request){
                $q->when($request->fecha_inicio, function($q) use ($request){
                    $q->whereDate('created_at', '>=', $request->fecha_inicio);
                })
                ->when($request->fecha_termino , function($q) use ($request){
                    $q->whereDate('created_at', '<=', $request->fecha_termino);
                });
            })
            ->when($request->producto, function($q) use ($request){
                $q->where('no_identificacion', 'like', "%$request->producto%");
            })
            ->when($request->tipo == 'orden', function($q){
                $q->groupBy('traspasos_id')
                ->select([
                    'traspasos_id',
                    DB::raw('MAX(id) as id'),
                    DB::raw('SUM(cantidad) as cantidad'),
                    DB::raw('SUM(cantidad_entregada) as cantidad_entregada'),
                    DB::raw('SUM(cantidad_devuelta) as cantidad_devuelta'),
                    DB::raw('SUM(valor_unitario * cantidad_entregada) as total_entregado'),
                    DB::raw('SUM(valor_unitario * cantidad_devuelta) as total_devuelto'),
                    DB::raw('SUM(valor_unitario * cantidad) as total'),
                ]);
            })
            ->latest('id')
            ->get();

        return $pedidos;
    }
}