<?php

namespace App\Http\Controllers\Estadisticas;

use App\Http\Controllers\Core\Controller;
use App\Http\Requests\Estadisticas\PedidoCargamentoRequest;
use App\Http\Resources\Estadisticas\PedidoCargamentoResource;
use App\Models\Refacciones\PedidosProductos;
use App\Pdf\Pdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PedidosController extends Controller
{
    //
    public function index(PedidoCargamentoRequest $request)
    {
        // return response()->json($this->search($request));
        return response()->json(PedidoCargamentoResource::collection($this->search($request)));
    }

    public function download(Request $request)
    {
        $view = 'estadisticas.pedido_cargamento';

        if($request->tipo == 'producto')
            $view = 'estadisticas.pedido_cargamento_por_producto';

        if($request->tipo == 'orden')
            $view = 'estadisticas.pedido_cargamento_por_pedido';
            
        $pdf = new Pdf;
        $pdf->html(
            view($view, [
                'productos' => $this->search($request),
                'fecha_inicio' => $request->fecha_inicio,
                'fecha_termino' => $request->fecha_termino,
            ])
            ->render()
        );

        return $pdf->output();
    }

    public function search(Request $request)
    {
        $pedidos = PedidosProductos::with([
            'producto', 
            'pedido',
            ])
            ->whereHas('pedido', function($q) use($request){
                $q->when($request->fecha_inicio, function($q) use ($request){
                    $q->whereDate('created_at', '>=', $request->fecha_inicio);
                })
                ->when($request->fecha_termino , function($q) use ($request){
                    $q->whereDate('created_at', '<=', $request->fecha_termino);
                })
                ->where('entrada', true);
            })
            ->whereHas('producto', function($q) use ($request){
                $q->when($request->producto, function($q) use ($request){
                    $q->where('no_identificacion', 'like', "%$request->producto%");
                });
            })
            ->when($request->tipo == 'producto', function($q){
                $q->groupBy('producto_id', 'precio')
                ->select([
                    DB::raw('MAX(id) as id'),
                    DB::raw('SUM(cantidad_entregada) as cantidad_entregada'),
                    DB::raw('SUM(total) as total'),
                    'precio',
                    'producto_id',
                    DB::raw('MAX(pedido_id) as pedido_id'),
                ]);
            })
            ->when($request->tipo == 'orden', function($q){
                $q->groupBy('pedido_id')
                ->select([
                    DB::raw('MAX(id) as id'),
                    DB::raw('SUM(cantidad_entregada) as cantidad_entregada'),
                    DB::raw('SUM(total) as total'),
                    'pedido_id',
                    DB::raw('MAX(proveedor) as proveedor'),
                    DB::raw('MAX(factura) as factura'),
                ]);
            })
            ->latest('id')
            ->get();

        return $pedidos;
    }
}
