<?php

namespace App\Http\Controllers\Estadisticas;

use App\Http\Controllers\Core\Controller;
use App\Servicios\Ereact\Services\Ereact;
use Illuminate\Http\Request;
use App\Models\Refacciones\HistorialEreact;

class EreactController extends Controller
{
    public function index(Request $request)
    {
        return response()->json(
            HistorialEreact::latest()->limit(100)->get()
        );
    }   

    public function download(Request $request, Ereact $ereact)
    {
        return $ereact->make($request->all());
    }
}