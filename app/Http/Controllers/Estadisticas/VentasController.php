<?php

namespace App\Http\Controllers\Estadisticas;

use App\Http\Controllers\Core\Controller;
use App\Http\Requests\Estadisticas\VentaRequest;
use App\Http\Resources\Estadisticas\VentaResource;
use App\Models\Refacciones\VentaProductoModel;
use App\Pdf\Pdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VentasController extends Controller
{
    //
    public function index(VentaRequest $request)
    {
        return response()->json(VentaResource::collection($this->search($request)));
    }

    public function download(Request $request)
    {
        $view = 'estadisticas.ventas';

        if($request->tipo == 'producto')
            $view = 'estadisticas.ventas_por_producto';

        if($request->tipo == 'orden')
            $view = 'estadisticas.ventas_por_orden';

        $pdf = new Pdf;
        $pdf->html(
            view($view, [
                'ventas' => $this->search($request),
                'fecha_inicio' => $request->fecha_inicio,
                'fecha_termino' => $request->fecha_termino,
            ])
            ->render()
        );

        return $pdf->output();
    }

    public function search(Request $request)
    {
        $ventas = VentaProductoModel::with([
            'venta', 
            'venta.cliente',
            'producto', 
            ])
            ->whereHas('venta', function($q) use($request){
                $q->when($request->fecha_inicio, function($q) use ($request){
                    $q->whereDate('created_at', '>=', $request->fecha_inicio);
                })
                ->when($request->fecha_termino , function($q) use ($request){
                    $q->whereDate('created_at', '<=', $request->fecha_termino);
                })
                ->when($request->tipo == 'mostrador', function($q){
                    $q->whereNull('numero_orden');
                })
                ->when($request->tipo == 'ventanilla', function($q){
                    $q->whereNotNull('numero_orden');
                });
            })
            ->whereHas('producto', function($q) use ($request){
                $q->when($request->producto, function($q) use ($request){
                    $q->where('no_identificacion', 'like', "%$request->producto%");
                });
            })
            ->where('estatus_producto_orden', '!=', 'B')
            ->when($request->tipo == 'producto', function($q){
                $q->groupBy('producto_id', 'valor_unitario')
                ->select([
                    DB::raw('MAX(id) as id'),
                    DB::raw('SUM(cantidad) as cantidad'),
                    'valor_unitario',
                    'producto_id',
                    DB::raw('MAX(venta_id) as venta_id'),
                ]);
            })
            ->when($request->tipo == 'orden', function($q){
                $q->groupBy('venta_id')
                ->select([
                    DB::raw('MAX(id) as id'),
                    DB::raw('SUM(cantidad) as cantidad'),
                    DB::raw('SUM(valor_unitario * cantidad) as total'),
                    'venta_id',
                ]);
            })
            ->latest('id')
            ->get();

        return $ventas;
    }
}
