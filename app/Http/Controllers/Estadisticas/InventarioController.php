<?php

namespace App\Http\Controllers\Estadisticas;

use App\Http\Controllers\Core\Controller;
use App\Http\Requests\Estadisticas\InventarioRequest;
use App\Servicios\Ereact\Inventario;

class InventarioController extends Controller
{
    public function index(InventarioRequest $request, Inventario $inventario)
    {
        return $inventario->json($request);
    }

    public function download(InventarioRequest $request, Inventario $inventario)
    {
        return $inventario->download($request);
    }
}