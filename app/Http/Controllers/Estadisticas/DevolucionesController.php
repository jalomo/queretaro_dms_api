<?php

namespace App\Http\Controllers\Estadisticas;

use App\Http\Controllers\Core\Controller;
use App\Http\Requests\Estadisticas\DevolucionRequest;
use App\Http\Resources\Estadisticas\DevolucionResource;
use App\Http\Resources\Estadisticas\DevolucionResourcePorOrden;
use App\Models\Refacciones\InventarioProductos;
use App\Pdf\Pdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DevolucionesController extends Controller
{
    //
    public function index(DevolucionRequest $request)
    {
        $resource = $request->tipo == 'orden'? 
            DevolucionResourcePorOrden::collection($this->search($request)) :
            DevolucionResource::collection($this->search($request));

        return response()->json($resource);
    }

    public function download(Request $request)
    {
        $view = 'estadisticas.devoluciones';

        if($request->tipo == 'producto')
            $view = 'estadisticas.devoluciones_por_producto';

        if($request->tipo == 'orden')
            $view = 'estadisticas.devoluciones_por_orden';

        $pdf = new Pdf;
        $pdf->html(
            view($view, [
                'devoluciones' => $this->search($request),
                'fecha_inicio' => $request->fecha_inicio,
                'fecha_termino' => $request->fecha_termino,
            ])
            ->render()
        );

        return $pdf->output();
    }

    public function search(Request $request)
    {
        $devoluciones = InventarioProductos::
        with('producto', 'venta_producto.venta', 'venta_producto.venta.cliente', 'venta_producto.venta.folio')
        ->join('venta_producto', function($join){
            $join->on(function($join){
                $join->on('inventario_productos.tipo_movimiento_id', '=', DB::raw(3));
                $join->orOn('inventario_productos.tipo_movimiento_id', '=', DB::raw(2));
                $join->orOn('inventario_productos.tipo_movimiento_id', '=', DB::raw(11));
                $join->orOn('inventario_productos.tipo_movimiento_id', '=', DB::raw(10));
            });
            $join->on('inventario_productos.model_id', 'venta_producto.id');
        })
        ->whereHas('venta', function($q) use($request){
            $q->when($request->fecha_inicio, function($q) use ($request){
                $q->whereDate('created_at', '>=', $request->fecha_inicio);
            })
            ->when($request->fecha_termino , function($q) use ($request){
                $q->whereDate('created_at', '<=', $request->fecha_termino);
            });
        })
        ->whereHas('producto', function($q) use ($request){
            $q->when($request->producto, function($q) use ($request){
                $q->where('no_identificacion', 'like', "%$request->producto%");
            });
        })
        ->groupBy('inventario_productos.producto_id', 'venta_producto.venta_id', 'inventario_productos.model_id')
        ->select([
            DB::raw('MAX(inventario_productos.id) as id'),
            DB::raw('SUM(CASE WHEN inventario_productos.tipo_movimiento_id = 2 OR inventario_productos.tipo_movimiento_id = 10 THEN inventario_productos.cantidad ELSE 0 END) as entradas'),
            DB::raw('SUM(CASE WHEN inventario_productos.tipo_movimiento_id = 3 OR inventario_productos.tipo_movimiento_id = 11 THEN inventario_productos.cantidad ELSE 0 END) as salidas'),
            DB::raw('SUM(CASE WHEN inventario_productos.tipo_movimiento_id = 3 OR inventario_productos.tipo_movimiento_id = 11 THEN inventario_productos.cantidad ELSE 0 END) - SUM(CASE WHEN inventario_productos.tipo_movimiento_id = 2 OR inventario_productos.tipo_movimiento_id = 10 THEN inventario_productos.cantidad ELSE 0 END) as devoluciones'),
            DB::raw('SUM(CASE WHEN inventario_productos.tipo_movimiento_id = 3 OR inventario_productos.tipo_movimiento_id = 11 THEN inventario_productos.cantidad * venta_producto.valor_unitario ELSE 0 END) - SUM(CASE WHEN inventario_productos.tipo_movimiento_id = 2 OR inventario_productos.tipo_movimiento_id = 10 THEN inventario_productos.cantidad * venta_producto.valor_unitario ELSE 0 END) as total'),
            DB::raw('AVG(venta_producto.valor_unitario) valor_unitario'),
            'venta_producto.venta_id',
            'inventario_productos.producto_id',
            'inventario_productos.model_id',
        ])
        ->having(DB::raw('SUM(CASE WHEN inventario_productos.tipo_movimiento_id = 3 OR inventario_productos.tipo_movimiento_id = 11 THEN inventario_productos.cantidad ELSE 0 END) - SUM(CASE WHEN inventario_productos.tipo_movimiento_id = 2 OR inventario_productos.tipo_movimiento_id = 10 THEN inventario_productos.cantidad ELSE 0 END)'), '>=', 0)
        ->latest('venta_producto.venta_id');
        
        if($request->tipo == 'orden')
        {
            $devoluciones = InventarioProductos::with('producto', 'venta', 'venta.cliente')->fromSub($devoluciones, 'devoluciones')
            ->groupBy('venta_id')
            ->select([
                DB::raw('MAX(id) as id'),
                DB::raw('MAX(model_id) AS model_id'),
                'venta_id',
                DB::raw('SUM(entradas) as entradas'),
                DB::raw('SUM(salidas) as salidas'),
                DB::raw('SUM ( CASE WHEN devoluciones > 0 THEN devoluciones WHEN devoluciones = 0 THEN entradas ELSE 0 END ) AS devoluciones'),
                DB::raw('SUM ( CASE WHEN devoluciones > 0 THEN devoluciones * valor_unitario WHEN devoluciones = 0 THEN entradas * valor_unitario ELSE 0 END ) AS total') 
            ]);
            // ->tosql();

            // dd($devoluciones);
        }

        return $devoluciones->get();
    }
}
