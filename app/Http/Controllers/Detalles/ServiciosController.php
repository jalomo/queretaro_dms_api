<?php

namespace App\Http\Controllers\Detalles;

use App\Http\Controllers\Core\Controller;
use App\Models\Refacciones\VentaServicioModel;
use App\Events\Detalles\DeleteServicioDetalleEvent;
use App\Events\Detalles\StoreServicioDetalleEvent;
use App\Events\Detalles\UpdateServicioDetalleEvent;
use App\Http\Requests\Ventas\Detalles\Servicios\StoreRequest;
use App\Http\Requests\Ventas\Detalles\Servicios\UpdateRequest;

class ServiciosController extends Controller
{
    /**
     * Guardar Servicio
     *
     * @param StoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $servicio = new VentaServicioModel($request->validated());

        if($servicio->save())
        {
            event(new StoreServicioDetalleEvent($servicio));

            return response()->json(['message' => __('El servicio se agrego correctamente.')], 201);
        }
        
        return response()->json(['message' => __('Ocurrio un error al agregar el servicio.')], 400);
    }

    /**
     * Actualizar Servicio
     *
     * @param VentaServicioModel $servicio
     * @param UpdateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update(VentaServicioModel $servicio, UpdateRequest $request)
    {
        if($servicio->update($request->validated()))
        {
            event(new UpdateServicioDetalleEvent($servicio));

            return response()->json(['message' => __('El servicio se actualizo correctamente.')]);
        }

        return response()->json(['message' => __('Ocurrio un error al actualizar el servicio.')], 400);
    }

    /**
     * Eliminar Servicio
     *
     * @param VentaServicioModel $servicio
     * @return \Illuminate\Http\Response
     */
    public function delete(VentaServicioModel $servicio)
    {
        if($servicio->delete())
        {
            event(new DeleteServicioDetalleEvent($servicio));

            return response()->json(['message' => __('El servicio se borro correctamente.')]);
        }

        return response()->json(['message' => __('Ocurrio un error al borrar el servicio.')], 400);
    }
}