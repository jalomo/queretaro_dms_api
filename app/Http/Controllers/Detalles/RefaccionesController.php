<?php

namespace App\Http\Controllers\Detalles;

use App\Http\Controllers\Core\Controller;
use App\Models\Refacciones\VentaProductoModel;
use App\Events\DeleteRefaccionDetalleEvent;
use App\Events\StoreRefaccionDetalleEvent;
use App\Events\UpdateRefaccionDetalleEvent;
use App\Http\Requests\Ventas\Detalles\DMS\DevolverTotalesContabilidadRequest;
use App\Http\Requests\Ventas\Detalles\DMS\StoreRequest;
use App\Http\Requests\Ventas\Detalles\DMS\UpdateRequest;
use App\Http\Requests\Ventas\Detalles\DMS\EntregarRefaccionRequest;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\VentasRealizadasModel;
use App\Servicios\Detalles\EntregarRefaccion;
use App\Servicios\Detalles\OrdenCerrada;
use App\Servicios\Detalles\ValidarCantidadEntregada;
use App\Servicios\Detalles\ValidarExistenciaRefaccion;
use App\Servicios\Detalles\ValidarExistenciaUpdateRefaccion;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class RefaccionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $productos = ProductosModel::when($request->no_identificacion, function($q) use ($request){
            $q->where('no_identificacion', 'like', "%$request->no_identificacion%");
        })
        ->limit(20)
        ->get();

        return response()->json($productos);
    }

    /**
     * Guardar Refaccion
     *
     * @param StoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        try{
            DB::beginTransaction();

            // Validamos que la orden no se encuentre cerrada en servicios
            $orden = new OrdenCerrada;
            $ordenCerrada = $orden->handle(VentasRealizadasModel::find($request->venta_id));

            if(! is_null($ordenCerrada))
            {
                return $ordenCerrada;
            }

            // Validamos que la existencia de las refacciones
            $handler = new ValidarExistenciaRefaccion;
            $validaciones = $handler->handle($request);
    
            if(! is_null($validaciones))
            {
                return $validaciones;
            }
    
            $refaccion = new VentaProductoModel($this->getStoreRequest($request));
    
            if($refaccion->save())
            {
                event(new StoreRefaccionDetalleEvent($refaccion));
                DB::commit();
                
                return response()->json(['message' => __('Se agrego la nueva refacción.')], 201);
            }

            DB::rollBack();
        } 
        catch(Exception $exception){
            DB::rollBack();

            logger()->warning($exception->getMessage(), [
                'file' => $exception->getFile(),
                'line' => $exception->getLine(),
            ]);
        }

        return response()->json(['message' => __('Ocurrio un errror al agregar la nueva refacción.')], 400);
    }

    /**
     * Actualizar Refaccion
     *
     * @param VentaProductoModel $refaccion
     * @param UpdateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update(VentaProductoModel $refaccion, UpdateRequest $request)
    {
        try{
            DB::beginTransaction();

            // Validamos que la orden no se encuentre cerrada en servicios
            $orden = new OrdenCerrada;
            $ordenCerrada = $orden->handle($refaccion->venta);

            if(! is_null($ordenCerrada))
            {
                return $ordenCerrada;
            }
            
            $handler = new ValidarExistenciaUpdateRefaccion($request);
            $validaciones = $handler->handle($refaccion);
    
            if(! is_null($validaciones))
            {
                return $validaciones;
            }
    
            if($refaccion->update($this->getUpdateRequest($request)))
            {
                event(new UpdateRefaccionDetalleEvent($refaccion));
                DB::commit();
    
                return response()->json(['message' => __('Se actualizo la refacción.')]);
            }

            DB::rollBack();
        } 
        catch(Exception $exception){
            DB::rollBack();
        }

        return response()->json(['message' => __('Ocurrio un error al actualizar la refaccion.')]);
    }

    /**
     * Eliminar Refaccion
     *
     * @param VentaProductoModel $refaccion
     * @return \Illuminate\Http\Response
     */
    public function delete(VentaProductoModel $refaccion)
    {
        try{
            DB::beginTransaction();

            // Validamos que la orden no se encuentre cerrada en servicios
            $orden = new OrdenCerrada;
            $ordenCerrada = $orden->handle($refaccion->venta);

            if(! is_null($ordenCerrada))
            {
                return $ordenCerrada;
            }

            if($refaccion->update(['estatus_producto_orden' => 'B']))
            {
                event(new DeleteRefaccionDetalleEvent($refaccion));
                DB::commit();

                return response()->json(['message' => __('Se elimino la refaccion correctamente.')]);
            }

            DB::rollBack();
        } 
        catch(Exception $exception){
            DB::rollBack();
        }

        return response()->json(['message' => __('Ocurrio un error al eliminarse la refaccion.')], 400);
    }

    /**
     * Realizar Entrega de Refacciones
     *
     * @param VentaProductoModel $refaccion
     * @param EntregarRefaccionRequest $request
     * @return \Illuminate\Http\Response
     */
    public function entregar(VentaProductoModel $refaccion, EntregarRefaccionRequest $request)
    {
        // Validamos la cantidad a entregar
        // Enviar Asiento a contabilidad
        // Realizamos la entrega
        $handler = new ValidarCantidadEntregada($request);
        $handler->nextHandler(new EntregarRefaccion($request));

        return $handler->handle($refaccion);
    }

    /**
     * Devuelve el total, costo promedio y cantidad de un numero de orden y num. pieza
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function refaccion_contabilidad(DevolverTotalesContabilidadRequest $request)
    {
        $producto = ProductosModel::where('no_identificacion', $request->no_identificacion)->first();
        $venta = VentasRealizadasModel::where('numero_orden', $request->numero_orden)->first();

        $refaccion = VentaProductoModel::where('producto_id', $producto->id)->where('venta_id', $venta->id)->first();

        if(is_null($refaccion))
        {
            throw ValidationException::withMessages(['no_identificacion' => 'No se encontro el número de refacción que coincida con el número de orden.']);
        }

        return response()->json([
            'total' => $refaccion->cantidad * $refaccion->valor_unitario, 
            'cantidad' => $refaccion->cantidad,
            'total_costo_promedio' => $refaccion->cantidad * $refaccion->costo_promedio,
            'costo_promedio' => $refaccion->costo_promedio,
        ]);
    }

    /**
     * Array para el guardado de la refaccion
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    protected function getStoreRequest($request)
    {
        $producto = ProductosModel::find($request->producto_id);

        return array_merge([
                'precio_id' => 1,
                'almacen_id' => 1,
                'venta_total' => $request->valor_unitario * $request->cantidad,
                'estatus_producto_orden' => 'O',
                'detalles' => true,
                'costo_promedio' => $producto->costo_promedio,
                'venta_servicio_id' => $request->venta_servicio_id,
                'afecta_paquete' => $request->venta_servicio_id ? true : false,
                'vendedor_id' => auth()->user()->id?? null,
            ], 
            $request->validated()
        );
    }

    /**
     * Array para la actualizacion de la refaccion
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    protected function getUpdateRequest($request)
    {
        $producto = ProductosModel::find($request->producto_id);

        return array_merge([
                'precio_id' => 1,
                'almacen_id' => 1,
                'venta_total' => $request->valor_unitario * $request->cantidad,
                'estatus_producto_orden' => 'O',
                'detalles' => true,
                'costo_promedio' => $producto->costo_promedio,
            ], 
            $request->validated()
        );
    }
}