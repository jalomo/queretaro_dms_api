<?php

namespace App\Http\Controllers\Detalles;

use App\Http\Controllers\Core\Controller;
use App\Http\Resources\Detalles\ResumenDetallesResource;
use App\Models\Refacciones\VentaProductoModel;
use App\Models\Refacciones\VentaServicioModel;
use App\Models\Refacciones\VentasRealizadasModel;
use App\Servicios\Refacciones\ServicioFolios;
use App\Servicios\Refacciones\ServicioVentasRealizadas;
use App\Http\Requests\Ventas\Detalles\StoreVentaRequest;
use App\Http\Resources\DataTablesResource;
use App\Models\Refacciones\ReVentasEstatusModel;
use App\Models\Refacciones\EstatusVentaModel;
use App\Models\Contabilidad\CatalogoProcesosModel;
use App\Servicios\Detalles\ActualizarOperaciones;
use App\Servicios\Detalles\OrdenCerrada;
use App\Servicios\Detalles\OrdenValidarSaldo;
use App\Servicios\Detalles\RecuperarRefaccionesPresupuesto;
use App\Servicios\Detalles\RefaccionesEntregadas;
use App\Servicios\Detalles\VentaEnProceso;
use App\Servicios\Detalles\VentaFinalizada;
use Illuminate\Support\Facades\DB;
use Throwable;
use VentasRealizadas;

class VentaDetallesController extends Controller
{
    /**
     * Devolver las operaciones y refacciones de una orden
     *
     * @param string $numero_orden
     * @return \Illuminate\Http\Response
     */
    public function index($numero_orden)
    {
        // Refacciones
        $productos = VentaProductoModel::whereHas(
            'venta', fn($q) => $q->where('numero_orden', $numero_orden)
        )
        ->where('estatus_producto_orden', '<>','B')
        ->orderBy('detalles')
        ->orderBy('presupuesto')
        ->orderBy('afecta_mano_obra')
        ->orderBy('afecta_paquete')
        ->orderBy('id')
        ->get();

        // Servicios u Operaciones
        $servicios = VentaServicioModel::whereHas(
            'venta', fn($q) => $q->where('numero_orden', $numero_orden)
        )
        ->orderBy('id')
        ->get();

        return response()->json(new ResumenDetallesResource([
            'servicios' => $servicios,
            'productos' => $productos,
        ]));
    }

    /**
     * Guardar nueva orden de venta
     *
     * @param StoreVentaRequest $request
     * @param ServicioFolios $servicioFolio
     * @return \Illuminate\Http\Response
     */
    public function store(StoreVentaRequest $request, ServicioFolios $servicioFolio)
    {
        try
        {
            DB::beginTransaction();

            $venta = VentasRealizadasModel::where('numero_orden', $request->numero_orden)->first();

            // Si existe calculamos el total 
            if(! is_null($venta))
            {
                (new ServicioVentasRealizadas)->calcularVentaTotal($venta->id);
            }

            // Si no existe la venta esta se crea
            if($venta == null)
            {
                $folio = $servicioFolio->generarFolio(CatalogoProcesosModel::PROCESO_SERVICIOS);
                $venta = VentasRealizadasModel::create($request->validated() + [ 'folio_id' => $folio->id]);

                // Si la api es llamada como usuario autenticado
                // guardamos el usuario
                if(optional(auth()->user())->id != null)
                {
                    ReVentasEstatusModel::create([
                        'ventas_id' => $venta->id,
                        'estatus_ventas_id' => EstatusVentaModel::ESTATUS_PROCESO,
                        'user_id' => auth()->user()->id,
                    ]);
                }

                // Actualizamos las operaciones
                (new ActualizarOperaciones)->execute($venta);
            }
                    
            DB::commit();

            return response()->json($venta, 201);
        } 
        catch(Throwable $exception)
        {
            DB::rollback();

            return response()->json(['message' => __('Ocurrio un error al guardar la orden.'), 'errors' => $exception->getMessage(), 'line' => $exception->getLine()], 400);
        }
    }

    /**
     * Crear Operacion desde api externa
     *
     * @param StoreVentaRequest $request
     * @param ServicioFolios $servicioFolio
     * @return \Illuminate\Http\Response
     */
    public function storeFromServicios(StoreVentaRequest $request, ServicioFolios $servicioFolio)
    {
        info("SE CREA VENTA DESDE SERVICIOS {$request->numero_orden}", $request->validated());

        return $this->store($request, $servicioFolio);
    }

    /**
     * Actualizar operaciones de una orden de venta
     *
     * @param string $numero_orden
     * @return \Illuminate\Http\Response
     */
    public function updateOperaciones($numero_orden)
    {
        try
        {
            DB::beginTransaction();

            // Buscamos la venta
            $venta = VentasRealizadasModel::where('numero_orden', $numero_orden)->first();

            // Actualizamos las operaciones
            (new ActualizarOperaciones)->execute($venta);

            DB::commit();
            
            return response()->json(['message' => __('Se actualizaron las operaciones para el número de orden :numero_orden correctamente.', ['numero_orden' => $numero_orden])]);
        } 
        catch(Throwable $exception)
        {
            DB::rollback();

            return response()->json(['message' => __('Ocurrio un error al actualizar las operaciones.'), 'errors' => $exception->getMessage()], 400);
        }
    }

    /**
     * Reabrir ordenes que no se encuentran cerradas en servicios
     *
     * @param VentasRealizadasModel $venta
     * @return \Illuminate\Http\Response
     */
    public function reabrir_orden_completada(VentasRealizadasModel $venta)
    {
        try 
        {   
            // Comprobamos si la orden ha sido cerrada
            // Cambiamos el estatus de la orden a en proceso
            $handler = new OrdenCerrada;
            $handler->nextHandler(new VentaEnProceso);

            return $handler->handle($venta);
        } 
        catch (Throwable $exception) 
        {
            return response()->json([
                'message' => 'Ocurrio un error al abrir la orden.',
                'errors' => $exception->getMessage(),
            ], 400);
        }
    }

    /**
     * Completar la orden de Venta
     *
     * @param VentasRealizadasModel $venta
     * @return \Illuminate\Http\Response
     */
    public function completar(VentasRealizadasModel $venta)
    {
        // Calcular el total de la venta al completarla
        (new ServicioVentasRealizadas)->calcularVentaTotal($venta->id);

        // Ejecutamos Validacion Refacciones Entregadas
        // Validamos el Saldo de la orden no sea Negativo
        // Finalizamos las Venta
        $handler = new RefaccionesEntregadas;
        $handler->nextHandler(new OrdenValidarSaldo)
            ->nextHandler(new VentaFinalizada);

        return $handler->handle($venta);
    }

    public function consultar_presupuesto($numero_orden)
    {
        try
        {
            $venta = VentasRealizadasModel::where('numero_orden', $numero_orden)->firstOrFail();
            $refacciones = (new RecuperarRefaccionesPresupuesto)->get($venta);

            return response()->json([ 'aaData' => $refacciones ]);
        }
        catch(Throwable $exception)
        {
            return response()->json([
                'message' => 'Ocurrio un error al consultar el presupuesto en servicios.',
                'errors' => $exception->getMessage(),
            ], 400);
        }
    }

    /**
     * Existen refacciones para firmar
     *
     * @param int $numero_orden
     * @return \Illuminate\Http\Response
     */
    public function refacciones_por_firmar($numero_orden)
    {
        $venta = VentasRealizadasModel::where('numero_orden', $numero_orden)->firstOrFail();

        $handler = new RefaccionesEntregadas;

        $entregar = $handler->handle($venta);

        return $entregar == null? response()->json(['message' => 'Refacciones Entregadas']) : $entregar;
    }

    public function orden_completa($numero_orden)
    {
        $venta = VentasRealizadasModel::where('numero_orden', $numero_orden)->firstOrFail();

        return $this->completar($venta);
    }

    /**
     * Regresar el tipo de cuenta folio contabilidad
     * 
     * 
     */
    public function venta_folio_contabilidad(string $numero_orden)
    {
        $numero_orden = preg_replace('/[^0-9]/', '', $numero_orden);

        $venta = VentasRealizadasModel::where('numero_orden', $numero_orden)->firstOrFail();

        return response()->json([
            'tipo_orden' => $venta->tipo_orden,
            'numero_orden' => $venta->numero_orden,
        ]);
    }
}