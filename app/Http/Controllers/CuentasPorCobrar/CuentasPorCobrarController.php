<?php

namespace App\Http\Controllers\CuentasPorCobrar;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\CuentasPorCobrar\ServicioCuentaPorCobrar;
use App\Servicios\CuentasPorCobrar\ServicioAbono;
use App\Servicios\Refacciones\ServicioCurl;
use App\Servicios\Refacciones\ServicioFolios;
use App\Servicios\Refacciones\ServicioClientes;
use App\Servicios\Refacciones\ServicioReVentasEstatus;
use App\Servicios\Ventas\ServicioPedidoDiaMostrador;
use App\Servicios\CuentasPorCobrar\ServicioAsientos;
use App\Servicios\Inventario\DevolverVentaService;
use App\Models\Usuarios\User as UsuarioModel;
use App\Models\CuentasPorCobrar\CuentasPorCobrarModel;
use App\Models\CuentasPorCobrar\EstatusCuentaModel;
use App\Models\CuentasPorCobrar\AsientoModel;
use App\Models\CuentasPorCobrar\AbonosModel;
use App\Models\CuentasPorCobrar\PagosFacturaModel;
use App\Models\Refacciones\VentasRealizadasModel;
use App\Models\Refacciones\VentaProductoModel;
use App\Models\Refacciones\ReVentasEstatusModel;
use App\Models\Refacciones\EstatusVentaModel;
use App\Models\Refacciones\CatTipoPagoModel;
use App\Models\Contabilidad\CatalogoProcesosModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Mail\MylsaEmail;
use App\Mail\EnviaFactura;
use App\Mail\CorreoApi;
use Illuminate\Support\Facades\Mail;
use App\Exceptions\ParametroHttpInvalidoException;
use App\Http\Resources\DataTablesResource;
use Throwable;
use Carbon\Carbon;

class CuentasPorCobrarController extends CrudController
{
	public function __construct()
	{
		$this->servicio = new ServicioCuentaPorCobrar();
		$this->servicioAbono = new ServicioAbono();
		$this->servicioCurl = new ServicioCurl();
		$this->servicioFolios = new ServicioFolios();
		$this->servicioClientes = new ServicioClientes();
		$this->servicioReVentasEstatus = new ServicioReVentasEstatus();
		$this->modelo_usuarios = new UsuarioModel();
		$this->modelo_ventas_realizadas = new VentasRealizadasModel();
		$this->modelo_venta_producto = new VentaProductoModel();
		$this->servicioAsientos = new ServicioAsientos();
		$this->model_pagos_factura = new PagosFacturaModel();
		$this->model_tipo_pago = new CatTipoPagoModel();
	}

	public function index()
	{
		try {
			return Respuesta::json($this->servicio->getAll(), 200);
		} catch (Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function show($id)
	{
		$modelo = $this->servicio->getByIdCuenta($id);
		return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
	}

	public function crearCuentaVenta(Request $request)
	{
		try {
			DB::beginTransaction();
			$ventas = $this->modelo_ventas_realizadas->where(VentasRealizadasModel::FOLIO_ID, $request->get(CuentasPorCobrarModel::FOLIO_ID))->first();
			$request->merge([CuentasPorCobrarModel::CLIENTE_ID => $ventas->cliente_id]);
			$request->merge([CuentasPorCobrarModel::TOTAL => $ventas->venta_total]);
			$request->merge([CuentasPorCobrarModel::IMPORTE => $ventas->venta_total]);
			$request->merge([CuentasPorCobrarModel::FECHA =>  $ventas->fecha_venta]);
			$request->merge([CuentasPorCobrarModel::TIPO_PAGO_ID => 22]); // Por definir
			$request->merge([CuentasPorCobrarModel::ESTATUS_CUENTA_ID => EstatusCuentaModel::ESTATUS_POR_EMPEZAR]);
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasCreateCuenta());
			$modelo = $this->servicio->crear($request->all());
			if ($modelo) {
				$this->servicioAbono->crearAbonosByCuentaId($modelo->id);
				DB::commit();
			}
			$mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);

			return Respuesta::json($modelo, 201, $mensaje);
		} catch (Throwable $e) {
			DB::rollback();
			return Respuesta::error($e);
		}
	}

	public function updateCuentaVenta(Request $request, $id)
	{
		DB::beginTransaction();
		try {

			$ventas = $this->modelo_ventas_realizadas->where(VentasRealizadasModel::FOLIO_ID, $request->get(CuentasPorCobrarModel::FOLIO_ID))->first();
			$cuenta_por_cobrar = $this->servicio->getById($id);
			if ($cuenta_por_cobrar->estatus_cuenta_id != 1) {
				throw new ParametroHttpInvalidoException([
					'mensaje' => 'La cuenta ya se encuentra en proceso de pago'
				]);
			}
			$request->merge([CuentasPorCobrarModel::CLIENTE_ID => $ventas->cliente_id]);
			$request->merge([CuentasPorCobrarModel::TOTAL => $ventas->venta_total]);
			$request->merge([CuentasPorCobrarModel::IMPORTE => $ventas->venta_total]);
			$request->merge([CuentasPorCobrarModel::FECHA =>  $ventas->fecha_venta]);

			ParametrosHttpValidador::validar($request, $this->servicio->getReglasUpdateCuenta());
			$request->merge([VentasRealizadasModel::VENTA_TOTAL => $ventas->venta_total]);
			$this->servicio->procesarCuentasPorCobrar($request->all());
			DB::commit();
		} catch (Throwable $e) {
			DB::rollback();
			return Respuesta::error($e);
		}
	}

	public function listado(Request $request)
	{
		try {
			$cuentas = $this->servicio->getAll($request);

			if (!isset($request->no_pagination)) {
				foreach ($cuentas as $cuenta) {
					$api = $this->servicioCurl->curlGet(env('URL_CONTABILIDAD') . 'asientos/api/detalle?folio=' . $cuenta->folio_id, false);
					$respuesta = json_decode($api);
					if (isset($respuesta) && $respuesta->data) {
						$cuenta->asientos = true;
					} else {
						$cuenta->asientos = false;
					}
				}
				return response()->json(
					new DataTablesResource($cuentas)
				);
			} else {
				return Respuesta::json($cuentas, 200);
			}
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function getKardexPagos(Request $request)
	{
		try {
			return Respuesta::json(array('data' => $this->servicio->getKardexPagos($request->all())), 200);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function getCierreCaja(Request $request)
	{
		try {
			return Respuesta::json($this->servicio->getCierreCaja($request->all()), 200);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function handleFactura($folio)
	{
		$detalle = $this->servicioAsientos->curl_asiento_api_detalle($folio, null);
		if (!$detalle) {
			throw new ParametroHttpInvalidoException([
				'msg' => "No se encontraron los asientos para la cuenta"
			]);
			return false;
		}
		if ($detalle->estatus_id != 'APLICADO' && $detalle->PolizaNomenclatura_id == 'QT') {
			$aplicar_folio = $this->servicioCurl->curlPost(env('URL_CONTABILIDAD') . 'asientos/api/aplicar_folio', [
				'folio' => $folio,
				'estatus' => 'APLICADO',
				AsientoModel::SUCURSAL_ID => env('SUCURSAL_DMS') == 'QUERETARO' ? AsientoModel::SUCURSAL_QUERETARO : AsientoModel::SUCURSAL_SANJUAN
			], false);

			if (json_decode($aplicar_folio)->status != 'success') {
				throw new ParametroHttpInvalidoException([
					'msg' => "Error al aplicar los asientos poliza"
				]);
				return false;
			}
			return true;
		}
	}

	public function aplicarFactura(Request $request)
	{
		try {
			DB::beginTransaction();

			ParametrosHttpValidador::validar_array([
				CuentasPorCobrarModel::CUENTA_POR_COBRAR_ID => $request->cuenta_por_cobrar_id
			], $this->servicio->getReglasByCuentaID());
			$tipo_factura = 0;

			$data = $this->servicio->getById($request->cuenta_por_cobrar_id);
			$this->handleFactura($data->folio_id);
			if (in_array($data->folio->tipo_proceso_id, [CatalogoProcesosModel::PROCESO_HOJALATERIA, CatalogoProcesosModel::PROCESO_SERVICIOS])) {
				$tipo_factura = 1;
			}
			$cliente = $this->servicioClientes->getById($data->cliente_id);
			if ($data->tipo_forma_pago_id == 1) {
				$abono = $this->servicioAbono->getWhere(CuentasPorCobrarModel::CUENTA_POR_COBRAR_ID, $request->cuenta_por_cobrar_id)->first();
			} else {
				if (!$request->abono_id) {
					throw new ParametroHttpInvalidoException([
						'mensaje' => 'El campo abono_id es requerido'
					]);
				}
			}
			$iva = $abono->total_abono * 0.16;
			$datos_factura = [
				'folio' => $data->folio_id,
				'forma_pago_id' => 1, //$data->tipo_forma_pago_id, 
				'metodo_pago_id' => $abono->tipo_pago_id,
				'sub_total' => $abono->total_abono - $iva,
				'total' => $abono->total_abono,
				'receptor_rfc' => $cliente->rfc ? $cliente->rfc : '',
				'receptor_nombre' => $cliente->nombre . ' ' . $cliente->apellido_paterno . ' ' . $cliente->apellido_materno,
				'receptor_uso_cfdi' => $abono->cfdi_id,
				'receptor_email' => $cliente->correo_electronico ? $cliente->correo_electronico : 'correo@sohex.com',
				'receptor_domicilio' => $cliente->direccion ?  $cliente->direccion . ' ' . $cliente->numero_ext . ' ' . $cliente->colonia . ' ' . $cliente->municipio . ' ' . $cliente->estado : 'S/D',
				'sucursal_id' => env('SUCURSAL_DMS') == 'QUERETARO' ? AsientoModel::SUCURSAL_QUERETARO : AsientoModel::SUCURSAL_SANJUAN,
				'tipo_factura' => $tipo_factura
			];
			if ($tipo_factura == 1) {
				$datos_factura['detalle_factura'] = isset($data->comentarios) ? $data->comentarios : null;
			}
			$contabilidad_api = $this->servicioCurl->curlPost(env('URL_CONTABILIDAD') . 'facturacion/api/crear_factura', $datos_factura, false);

			$respuesta = json_decode($contabilidad_api);
			if (isset($respuesta->status) && $respuesta->status == 'success') {
				$this->servicio->massUpdateWhereId('id', $request->cuenta_por_cobrar_id, [CuentasPorCobrarModel::ESTATUS_CUENTA_ID => EstatusCuentaModel::ESTATUS_FACTURADO]);
				if ($tipo_factura == 1) {
					$this->servicioCurl->curlGet(env('URL_CONTABILIDAD') . 'api/cambiar_situacion_orden/' . $data->numero_orden, false);
					// $respuesta = (json_decode($api));
					// if ($respuesta->exito) {
					// }
				}
				DB::commit();
				$mensaje = __(static::$I0014_FACTURA_APLICADA, ['recurso' => $this->servicio->getRecurso()]);
				return Respuesta::json($respuesta, 200, $mensaje);
			} else {
				$string_msg = [];
				$mensajillo = '';
				if (isset($respuesta) && $respuesta->message) {
					foreach ($respuesta->message as $msg) {
						$string_msg[] = $msg;
					}
					$mensajillo = implode(", ", $string_msg);
					throw new ParametroHttpInvalidoException([
						'mensaje' => 'Ocurrio un error al generar la factura. <br/>' . $mensajillo . ' ' . json_encode($datos_factura)
					]);
				} else {
					throw new ParametroHttpInvalidoException([
						'mensaje' => 'Fallo la conexión al generar la factura'
					]);
				}
				DB::rollback();
			}
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function aplicarFacturaMultiple(Request $request)
	{
		try {
			DB::beginTransaction();

			ParametrosHttpValidador::validar_array([
				'pagos_factura_id' => $request->pagos_factura_id
			], $this->servicio->getReglasFacturaMultipleCuenta());
			$tipo_factura = 0;
			$row = $this->model_pagos_factura->find($request->pagos_factura_id);
			$data = $this->servicio->getById($row->cuenta_por_cobrar_id);
			$cat = $this->model_tipo_pago->find($row->tipo_pago_id);
			$this->handleFactura($data->folio_id);
			if (in_array($data->folio->tipo_proceso_id, [CatalogoProcesosModel::PROCESO_HOJALATERIA, CatalogoProcesosModel::PROCESO_SERVICIOS])) {
				$tipo_factura = 1;
			}

			$cliente = $this->servicioClientes->getById($data->cliente_id);

			$iva = $row->importe - ($row->importe / 1.16);
			$subtotal = $row->importe - $iva;
			$conceptos[] = [
				'id_producto_servicio_interno' => $data->id,
				'concepto_cantidad' => 1,
				'unidad_interna' => 1,
				'concepto_nombre' => $data['concepto'],
				'concepto_precio' => round($subtotal, 2),
				'concepto_importe' => round($subtotal, 2),
				'concepto_NoIdentificacion' => $data->id . '_',
				'clave_sat' => '52141807',
				'unidad_sat' => 'P83',
				'Impuesto' => '002',
				'TipoFactor' => 'Tasa',
				'TasaOCuota' => '.16',
				'Base' => round($subtotal, 2),
				'Importe' => round($iva, 2),
			];
			$datos_factura = array(
				'Folio' => $data->folio_id . '-' . $row->id,
				'Serie' => '100',
				'fecha' => date('Y-m-d'),
				'FormaPago' => $cat->clave,
				'metodo_pago' => 'Pago en una sola exhibión',
				'subtotal' => round($subtotal, 2),
				'moneda' => 'MXN',
				'total' => round($row->importe, 2),
				'TipoDeComprobante' => 'I',
				'MetodoPago' => 'PUE',
				'LugarExpedicion' => '76046',
				'comentario' => 'sin comentarios',
				'iva' => round($iva, 2),
				'TipoCambio' => "1",
				'receptor_RFC' => $cliente->rfc ? $cliente->rfc : 'XAXX010101000',
				'receptor_nombre' => $cliente->nombre . ' ' . $cliente->apellido_paterno . ' ' . $cliente->apellido_materno,
				'receptor_uso_CFDI' => $row->cfdi_id,
				'receptor_email' => $cliente->correo_electronico ? $cliente->correo_electronico : 'correo@sohex.com', //$this->input->post('receptor_email'),
				'receptor_direccion' => $cliente->direccion ?  $cliente->direccion . ' ' . $cliente->numero_ext . ' ' . $cliente->colonia . ' ' . $cliente->municipio . ' ' . $cliente->estado : 'S/D',
				'conceptos' => $conceptos,
				'tipo_factura' => $tipo_factura,
			);
			if ($tipo_factura == 1) {
				$detalle_factura = json_decode($data->comentarios, true);
				$data_complementarios = [
					'Tipo' => (array_key_exists('tipo_vehiculo', $detalle_factura)) ? $detalle_factura['tipo_vehiculo'] : 'Automóvil',
					'Serie' => (array_key_exists('serie', $detalle_factura)) ? $detalle_factura['serie'] : '',
					'Modelo' => (array_key_exists('modelo', $detalle_factura)) ? $detalle_factura['modelo'] : '',
					'Placas' => (array_key_exists('placas', $detalle_factura)) ? $detalle_factura['placas'] : '',
					'Color' => (array_key_exists('color', $detalle_factura)) ? $detalle_factura['color'] : '',
					'KM' => (array_key_exists('km', $detalle_factura)) ? $detalle_factura['km'] : '',
					'Orden' => (array_key_exists('orden', $detalle_factura)) ? $detalle_factura['orden'] : '',
					'Version' => (array_key_exists('version', $detalle_factura)) ? $detalle_factura['version'] : '',
					'fecha_recepcion' => (array_key_exists('fecha_recepcion', $detalle_factura)) ? $detalle_factura['fecha_recepcion'] : '',
					'Asesor' => (array_key_exists('asesor', $detalle_factura)) ? $detalle_factura['asesor'] : '',
					'Transmision' => (array_key_exists('transmision', $detalle_factura)) ? $detalle_factura['transmision'] : ''
				];
				$datos_factura = array_merge($datos_factura, $data_complementarios);
			}
			$api_complemento = $this->servicioCurl->curlPostJson('https://mylsa.iplaneacion.com/dms/contabilidad_queretaro/index.php/facturacion/api_genera_factaura', @json_encode($datos_factura));
			$respuesta = json_decode($api_complemento);
			if (isset($respuesta) && $respuesta->error == 0) {
				$pagoFactura =  PagosFacturaModel::find($row->id);
				$pagoFactura->factura_id = $respuesta->factura;
				$pagoFactura->sat_uuid = $respuesta->sat_uuid;
				$pagoFactura->sat_pdf = $respuesta->pdf;
				$pagoFactura->facturado = 1;
				if ($pagoFactura->save()) {
					if ($data->estatus_cuenta_id != 8) {
						$this->servicio->massUpdateWhereId('id', $row->cuenta_por_cobrar_id, [CuentasPorCobrarModel::ESTATUS_CUENTA_ID => EstatusCuentaModel::ESTATUS_FACTURA_MULTIPLE]);
					}
					DB::commit();
					$mensaje = __(static::$I0014_FACTURA_APLICADA, ['recurso' => $this->servicio->getRecurso()]);
					return Respuesta::json($pagoFactura, 200, $mensaje);
				} else {
					throw new ParametroHttpInvalidoException([
						'mensaje' => 'Ocurrio un error al aplicar la actualización al registro'
					]);
				}
			} else {
				if (isset($respuesta) && $respuesta->error == 1) {
					throw new ParametroHttpInvalidoException([
						'mensaje' => 'Ocurrio un error al generar la factura <br/>' . $respuesta->error_mensaje
					]);
				} else {
					throw new ParametroHttpInvalidoException([
						'mensaje' => 'Fallo la conexión al generar la factura'
					]);
				}
				DB::rollback();
			}
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}


	public function aplicarFacturaCredito(Request $request)
	{
		try {
			DB::beginTransaction();

			ParametrosHttpValidador::validar_array([
				CuentasPorCobrarModel::CUENTA_POR_COBRAR_ID => $request->cuenta_por_cobrar_id
			], $this->servicio->getReglasByCuentaID());
			$tipo_factura = 0;
			$data = $this->servicio->getById($request->cuenta_por_cobrar_id);
			if (in_array($data->folio->tipo_proceso_id, [CatalogoProcesosModel::PROCESO_HOJALATERIA, CatalogoProcesosModel::PROCESO_SERVICIOS])) {
				$tipo_factura = 1;
			}
			$cliente = $this->servicioClientes->getById($data->cliente_id);
			if ($data->tipo_forma_pago_id != 2) {
				throw new ParametroHttpInvalidoException([
					'mensaje' => 'La cuenta no es tipo crédito-'
				]);
			}
			// $abono = $this->servicioAbono->getWhere(CuentasPorCobrarModel::CUENTA_POR_COBRAR_ID, $request->cuenta_por_cobrar_id)->first();
			$iva = $data->total * 0.16;
			$datos_factura = [
				'folio' => $data->folio_id,
				'forma_pago_id' => 2, // CREDITO
				'metodo_pago_id' => 23, // POR DEFINIR
				'sub_total' => $data->total - $iva,
				'total' => $data->total,
				'receptor_rfc' => $cliente->rfc ? $cliente->rfc : '',
				'receptor_nombre' => $cliente->nombre . ' ' . $cliente->apellido_paterno . ' ' . $cliente->apellido_materno,
				'receptor_uso_cfdi' => 3, //pendiente agregar modal	
				'receptor_email' => $cliente->correo_electronico ? $cliente->correo_electronico : 'correo@sohex.com',
				'receptor_domicilio' => $cliente->direccion ?  $cliente->direccion . ' ' . $cliente->numero_ext . ' ' . $cliente->colonia . ' ' . $cliente->municipio . ' ' . $cliente->estado : 'S/D',
				'sucursal_id' => env('SUCURSAL_DMS') == 'QUERETARO' ? AsientoModel::SUCURSAL_QUERETARO : AsientoModel::SUCURSAL_SANJUAN,
				'tipo_factura' => $tipo_factura
			];
			if ($tipo_factura == 1) {
				$datos_factura['detalle_factura'] = isset($data->comentarios) ? $data->comentarios : null;
			}
			$contabilidad_api = $this->servicioCurl->curlPost(env('URL_CONTABILIDAD') . 'facturacion/api/crear_factura', $datos_factura, false);
			$respuesta = json_decode($contabilidad_api);
			if (isset($respuesta->status) && $respuesta->status == 'success') {
				$this->servicio->massUpdateWhereId('id', $request->cuenta_por_cobrar_id, [CuentasPorCobrarModel::ESTATUS_CUENTA_ID => EstatusCuentaModel::ESTATUS_FACTURADO]);
			} else {
				$string_msg = [];
				$mensajillo = '';
				if (isset($respuesta) && $respuesta->message) {
					foreach ($respuesta->message as $msg) {
						$string_msg[] = $msg;
					}
					$mensajillo = implode(", ", $string_msg);
					throw new ParametroHttpInvalidoException([
						'mensaje' => 'Ocurrio un error al generar la factura <br/>' . $mensajillo
					]);
				} else {
					throw new ParametroHttpInvalidoException([
						'mensaje' => 'Fallo la conexión al generar la factura'
					]);
				}
				DB::rollback();
			}
			$mensaje = __(static::$I0014_FACTURA_APLICADA, ['recurso' => $this->servicio->getRecurso()]);
			DB::commit();
			return Respuesta::json($respuesta, 200, $mensaje);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function getComprobanteFiscal(Request $request)
	{
		$folio = $request->get('folio');
		$api =  $this->servicioCurl->curlGet(env('URL_CONTABILIDAD') . 'facturacion/api/getComprobanteFiscal?folio=' . $folio, false);
		$respuesta = (json_decode($api));
		if ($respuesta->status != "success") {
			$data = $this->servicio->getWhereInFirst(CuentasPorCobrarModel::FOLIO_ID, [$request->get('folio')]);
			if ($data->id) {

				$request->merge(['cuenta_por_cobrar_id' => $data->id]);
				$respuesta = $this->aplicarFactura($request);
			}
		}
		return Respuesta::json($respuesta, 200);
	}

	public function getDescargarComplemento(Request $request)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasDescargarComplemento());
			$request->merge(['sucursal_id' => env('SUCURSAL_DMS') == "QUERETARO" ?  1 : null]);
			$modelo = $this->servicio->getByIdCuenta($request->get('cuenta_por_cobrar_id'));
			$request->merge(['folio' => $modelo->folio_id]);
			$api = $this->servicioCurl->curlPost(env('URL_CONTABILIDAD') . 'facturacion/api/complementoFiscal', $request->toArray(), false);
			$respuesta = (json_decode($api));
			return Respuesta::json($respuesta, 200);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function showByFolioId($id)
	{
		$modelo = $this->servicio->getByFolioId($id);
		return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
	}

	public function showByFolioIdNumeroOrden(Request $request)
	{
		$modelo = $this->servicio->getByFolioNumeroOrden($request->get('folio_id'));
		return Respuesta::json($modelo, 200);
	}

	public function getVerificaCuentasMorosas(Request $request)
	{
		$fecha_actual = $request->get('fecha_actual') ? $request->get('fecha_actual') : date('Y-m-d');
		$cuentas_por_cobrar = $this->servicio->getCuentasEnProcesoPago();
		$data = [];
		if ($cuentas_por_cobrar && !empty($cuentas_por_cobrar)) {
			foreach ($cuentas_por_cobrar->toArray() as $cuenta) {
				$params = [
					'cuenta_por_cobrar_id' => $cuenta['id'],
					'fecha_actual' => $fecha_actual
				];
				$verificando = $this->servicioAbono->getVerificaAbonosPendientes($params);
				if (!empty($verificando)) {
					$data[] = $verificando;
				}
			}
		}
		return Respuesta::json($data, 200);
	}

	public function envioAvisoPago(Request $request)
	{

		$cuentas_por_cobrar = $this->servicio->getCuentasEnProcesoPago();
		$fecha_actual = $request->get('fecha_actual') ? $request->get('fecha_actual') : date('Y-m-d');
		$cobradores = $this->modelo_usuarios
			->select(UsuarioModel::TELEFONO)
			->where(UsuarioModel::ROL_ID, UsuarioModel::ROL_COBRANZA)->get();
		$data = [];
		if ($cuentas_por_cobrar && count($cuentas_por_cobrar) >= 1) {
			foreach ($cuentas_por_cobrar as $cuenta) {
				$params = [
					'cuenta_por_cobrar_id' => $cuenta->id,
					'fecha_actual' => $fecha_actual
				];
				$abonosAviso = $this->servicioAbono->getVerificaDiasParaPago($params);
				if (!empty($abonosAviso)) {
					$this->enviarSmsTexto($abonosAviso);
					Mail::to($abonosAviso->correo_electronico)->send(new MylsaEmail($abonosAviso));
					$this->enviarNotificacion($abonosAviso, $cobradores);
					$data[] = $abonosAviso;
				}
			}
		}
		return Respuesta::json($data, 200);
	}

	public function enviarSmsTexto($data)
	{
		$link = "https://sohexdms.net/cs/companycars/dms_companycars/caja/entradas/imprime_estado_cuenta?cuenta_id=" . base64_encode($data->cuenta_por_cobrar_id);
		$mensaje = $data->nombre . ' la factura con folio ' . $data->folio . ' vence el ' . date('d/m/y', strtotime($data->fecha_vencimiento)) . ' ' . $link;
		$this->servicioCurl->curlPost('https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_mensaje', [
			'celular' => $data->telefono,
			'mensaje' => $this->eliminar_tildes($mensaje)
		], false);
	}

	public function enviarNotificacion($data, $cobradores)
	{

		if (!empty($cobradores)) {
			foreach ($cobradores as $cobrador) {
				$mensaje =  'La factura con folio ' . $data->folio . ' del cliente ' . $data->nombre . ' vence el ' . date('d/m/y', strtotime($data->fecha_vencimiento));
				$this->servicioCurl->curlPost('https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_notificacion', [
					'celular' => $cobrador->telefono,
					'mensaje' => $this->eliminar_tildes($mensaje),
					'sucursal' => 'M2137'
				], false);
			}
		}
	}

	public function setNotificacion(Request $request)
	{

		ParametrosHttpValidador::validar($request, $this->servicio->getNotificacion());

		$dataNotificacion = $this->servicioCurl->curlPost('https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_notificacion', [
			'celular' => $request->get('telefono'),
			'mensaje' => $this->eliminar_tildes($request->get('mensaje')),
			'sucursal' => 'M2137'
		], false);

		return Respuesta::json($dataNotificacion, 200);
	}

	public function registrarCxcServicio(Request $request)
	{
		try {
			DB::beginTransaction();
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
			$modelo = $this->servicio->procesarCuentasPorCobrar($request->all());
			if ($modelo) {
				$mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
				if ($request->get(CuentasPorCobrarModel::TIPO_PROCESO) == 1) {
					$venta = $this->modelo_ventas_realizadas->where(VentasRealizadasModel::FOLIO_ID, $request->get(CuentasPorCobrarModel::FOLIO_ID))->first();

					$descontar_stock = $this->servicioReVentasEstatus->storeReVentasEstatus([
						ReVentasEstatusModel::VENTA_ID => $venta->id,
						ReVentasEstatusModel::ESTATUS_VENTA_ID => EstatusVentaModel::ESTATUS_FINALIZADA
					]);
					$pedido_del_dia = new ServicioPedidoDiaMostrador;

					if (!$pedido_del_dia->make($venta)) {
						DB::rollback();
						throw new ParametroHttpInvalidoException([
							'mensaje' => 'Ocurrio un error al afectar el efectuar el pedido'
						]);
					}

					if (!$descontar_stock) {
						DB::rollback();
						throw new ParametroHttpInvalidoException([
							'mensaje' => 'Ocurrio al descontar el stock'
						]);
					}
				}
				DB::commit();
				return Respuesta::json($modelo, 201, $mensaje);
			} else {
				throw new ParametroHttpInvalidoException([
					'mensaje' => 'Ocurrio un error al procesar la cuenta'
				]);
			}
		} catch (\Throwable $e) {
			DB::rollback();
			return Respuesta::error($e);
		}
	}

	public function setCxcComplemento(Request $request)
	{
		try {
			$request->merge([CuentasPorCobrarModel::FECHA =>  date('Y-m-d')]);
			$request->merge([CuentasPorCobrarModel::ESTATUS_CUENTA_ID => EstatusCuentaModel::ESTATUS_POR_EMPEZAR]);
			$request->merge([CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID => 1]); //contado
			$request->merge([CuentasPorCobrarModel::PLAZO_CREDITO_ID => 14]); // una sola exhibición
			$request->merge([CuentasPorCobrarModel::TIPO_PAGO_ID => 22]); // Por definir
			$request->merge([CuentasPorCobrarModel::IMPORTE => $request->get(CuentasPorCobrarModel::TOTAL)]);

			ParametrosHttpValidador::validar($request, $this->servicio->getReglasCreateCuenta());
			if (in_array($request->get('tipo_proceso'), [CatalogoProcesosModel::PROCESO_HOJALATERIA, CatalogoProcesosModel::PROCESO_SERVICIOS])) {
				ParametrosHttpValidador::validar_array(
					[
						CuentasPorCobrarModel::NUMERO_ORDEN => $request->get(CuentasPorCobrarModel::NUMERO_ORDEN),
						CuentasPorCobrarModel::TOTAL => $request->get(CuentasPorCobrarModel::TOTAL),
						CuentasPorCobrarModel::VENTA_TOTAL_DESCUENTO => $request->get(CuentasPorCobrarModel::VENTA_TOTAL_DESCUENTO),
					],
					$this->servicio->getReglasPolizaServicioHojalateria()
				);
				/*$apiTotalOrden = $this->servicioCurl->curlGet(env('URL_SERVICIOS') . 'citas/citas_queretaro/enlaces/dms/recuperar_operaciones?id_cita=' . $request->get(CuentasPorCobrarModel::NUMERO_ORDEN), false);
				$respuesta = json_decode($apiTotalOrden);
				if (!isset($respuesta->totales->general_costos)) {
					throw new ParametroHttpInvalidoException([
						'mensaje' => 'Error al obtener los totales API recuperar operaciones'
					]);
				}*/
				// dd($respuesta->totales->general_costos->costo_total_con_iva);
				/*if (!empty($request->get(CuentasPorCobrarModel::VENTA_TOTAL_DESCUENTO))) {
					if (round($respuesta->totales->general_costos->costo_total_con_iva, 2) != round($request->get(CuentasPorCobrarModel::VENTA_TOTAL_DESCUENTO), 2)) {
						if (round($respuesta->totales->presupuesto_costos->costo_total_con_iva, 2) != round($request->get(CuentasPorCobrarModel::VENTA_TOTAL_DESCUENTO), 2)) {
							throw new ParametroHttpInvalidoException([
								'mensaje' => 'El total de la orden ' . round($respuesta->totales->presupuesto_costos->costo_total_con_iva, 2)  . ' no coincide con el total enviado : ' . round($request->get(CuentasPorCobrarModel::VENTA_TOTAL_DESCUENTO), 2)
							]);
						}
					}
				} else {
					if (round($respuesta->totales->general_costos->costo_total_con_iva, 2) != round($request->get(CuentasPorCobrarModel::TOTAL), 2)) {
						if (round($respuesta->totales->presupuesto_costos->costo_total_con_iva, 2) != round($request->get(CuentasPorCobrarModel::TOTAL), 2)) {
							throw new ParametroHttpInvalidoException([
								'mensaje' => 'El total de la orden ' . round($respuesta->totales->presupuesto_costos->costo_total_con_iva, 2)  . ' no coincide con el total enviado: ' . round($request->get(CuentasPorCobrarModel::TOTAL), 2)
							]);
						}
					}
				}*/
				ParametrosHttpValidador::validar_array([CuentasPorCobrarModel::COMENTARIOS => $request->get(CuentasPorCobrarModel::COMENTARIOS)], $this->servicio->getReglasValidateJSON());
			}

			$this->validateCuentaPorCobrar($request);
			if (!isset($request['folio_id'])) {
				$folio = $this->servicioFolios->generarFolio($request['tipo_proceso']);
				if ($folio && $folio->id) {
					$request->merge([CuentasPorCobrarModel::FOLIO_ID => $folio->id]);
					$request->merge([VentasRealizadasModel::VENTA_TOTAL => $request->get(CuentasPorCobrarModel::TOTAL)]);
				}
			}
			$modelo = $this->servicio->procesarCuentasPorCobrar($request->all());
			$mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
			return Respuesta::json($modelo, 201, $mensaje);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	private function validateCuentaPorCobrar($request)
	{
		if (!$request['tipo_proceso']) {
			$mensaje = __(static::$E0013_ERROR_REGISTERING_RESOURCE, ['recurso' => $this->servicio->getRecurso()]);
			return Respuesta::noContent(400, $mensaje);
		}

		if (in_array($request['tipo_proceso'], [CatalogoProcesosModel::PROCESO_SERVICIOS, CatalogoProcesosModel::PROCESO_HOJALATERIA])) {
			$validar_campos_factura = ['serie', 'modelo', 'placas', 'color', 'km', 'orden', 'version', 'fecha_recepcion', 'asesor', 'trasmision', 'tipo_vehiculo'];

			if (!$request->get('comentarios')) {
				throw new ParametroHttpInvalidoException([
					'mensaje' => 'Falta indicar el campo comentarios en formato json con los datos: ' . implode(", ", $validar_campos_factura)
				]);
			} else {
				$campos_faltantes = [];
				$comentarios = $request->get('comentarios');
				$decode = (array)json_decode($comentarios);
				foreach ($validar_campos_factura as  $val) {
					if (!array_key_exists($val, $decode)) {
						$campos_faltantes[] = $val;
					}
				}
				if (is_array($campos_faltantes) && count($campos_faltantes) >= 1) {
					throw new ParametroHttpInvalidoException([
						'mensaje' => 'Falta indicar los datos de ' . implode(",", $campos_faltantes)
					]);
				}
			}
		}
		/*if ($request->get('concepto')) {
			$total_palabras = count(explode(" ", $request->get('concepto')));
			if ($total_palabras < 2) {
				throw new ParametroHttpInvalidoException([
					'mensaje' => 'Favor de mandar un concepto válido!!. Más de una palabra papa'
				]);
			}
		}*/
	}

	private function eliminar_tildes($cadena)
	{

		$cadena = ($cadena);

		//Ahora reemplazamos las letras
		$cadena = str_replace(
			array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
			array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
			$cadena
		);

		$cadena = str_replace(
			array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
			array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
			$cadena
		);

		$cadena = str_replace(
			array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
			array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
			$cadena
		);

		$cadena = str_replace(
			array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
			array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
			$cadena
		);

		$cadena = str_replace(
			array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
			array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
			$cadena
		);

		$cadena = str_replace(
			array('ñ', 'Ñ', 'ç', 'Ç'),
			array('n', 'N', 'c', 'C'),
			$cadena
		);

		return $cadena;
	}

	public function updatestatus($cxc = '', Request $request)
	{
		try {
			ParametrosHttpValidador::validar(
				$request,
				[
					'estatus_cuenta_id' => 'required'
				]
			);
			$modelo = $this->servicio->massUpdateWhereId('id', $cxc, $request->all());
			return Respuesta::json($modelo, 200);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function cancelarCuentaById(int $id)
	{
		try {
			DB::beginTransaction();
			ParametrosHttpValidador::validar_array([
				CuentasPorCobrarModel::CUENTA_POR_COBRAR_ID => $id
			], $this->servicio->getReglasByCuentaID());

			$data = $this->servicio->getById($id);
			if (in_array($data->estatus_cuenta_id, [EstatusCuentaModel::ESTATUS_TERMINADO, EstatusCuentaModel::ESTATUS_CANCELADO])) {
				throw new ParametroHttpInvalidoException([
					'mensaje' => self::$E0016_NOT_STATUS_CUENTA
				]);
			}
			$venta = $this->modelo_ventas_realizadas->where(VentasRealizadasModel::FOLIO_ID, $data->folio_id)->first();
			$venta_productos = $this->modelo_venta_producto->where(VentaProductoModel::VENTA_ID, $venta->id)->get();
			foreach ($venta_productos as $producto) {
				$devolucion = new DevolverVentaService;
				$devolucion->id($producto->id);
				$devolucion->handle($producto->producto_id, $producto->cantidad,$producto->valor_unitario);
			}
			$modelo = $this->servicio->massUpdateWhereId('id', $id, [CuentasPorCobrarModel::ESTATUS_CUENTA_ID => EstatusCuentaModel::ESTATUS_CANCELADO]);

			if ($modelo) {
				$contabilidad_api = $this->servicioCurl->curlPost(env('URL_CONTABILIDAD') . 'asientos/api/aplicar_folio', [
					'folio' => $data->folio_id,
					'estatus' => 'ANULADO',
					'sucursal_id' => env('SUCURSAL_DMS') == 'QUERETARO' ? AsientoModel::SUCURSAL_QUERETARO : AsientoModel::SUCURSAL_SANJUAN
				], false);

				$respuesta = json_decode($contabilidad_api);
				if ($respuesta) {
					DB::commit();
					$mensaje = __(static::$CUENTA_CANCELADA, ['recurso' => $this->servicio->getRecurso()]);
					return Respuesta::json($modelo, 200, $mensaje);
				}
			}
		} catch (\Throwable $e) {
			DB::rollback();
			return Respuesta::error($e);
		}
	}

	public function updateClienteByCuentaPorCobrar(int $cuenta_por_cobrar_id, Request $request)
	{
		try {
			ParametrosHttpValidador::validar_array([
				CuentasPorCobrarModel::CUENTA_POR_COBRAR_ID => $cuenta_por_cobrar_id,
				CuentasPorCobrarModel::CLIENTE_ID => $request->get(CuentasPorCobrarModel::CLIENTE_ID),
			], $this->servicio->getReglasByCuentaIDAndCliente());

			$modelo = $this->servicio->massUpdateWhereId(CuentasPorCobrarModel::ID, $cuenta_por_cobrar_id, $request->all());
			$mensaje = __(static::$I0004_RESOURCE_UPDATED, ['parametro' => $this->servicio->getRecurso()]);

			return Respuesta::json($modelo, 200, $mensaje);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function destroy(int $id)
	{
		try {
			ParametrosHttpValidador::validar_array([
				CuentasPorCobrarModel::CUENTA_POR_COBRAR_ID => $id,
			], $this->servicio->getReglasByCuentaID());

			$data = $this->servicio->getById($id);

			$api = $this->servicioCurl->curlGet(env('URL_CONTABILIDAD') . 'asientos/api/detalle?folio=' . $data->folio_id, false);
			$respuesta = json_decode($api);
			$tiene_asientos = $respuesta->data ? $respuesta->data[0] : [];

			if ($tiene_asientos) {
				$contabilidad_api = $this->servicioCurl->curlPost(env('URL_CONTABILIDAD') . 'asientos/api/aplicar_folio', [
					'folio' => $data->folio_id,
					'estatus' => 'ANULADO',
					'sucursal_id' => env('SUCURSAL_DMS') == 'QUERETARO' ? AsientoModel::SUCURSAL_QUERETARO : AsientoModel::SUCURSAL_SANJUAN
				], false);
				$respuesta = json_decode($contabilidad_api);
				if ($respuesta->status != 'error') {
					$this->elimina_abonos($id);
					return Respuesta::noContent(204);
				} else {
					throw new ParametroHttpInvalidoException([
						'mensaje' => self::$E0016_NOT_STATUS_CUENTA
					]);
				}
			} else {
				$this->elimina_abonos($id);
				return Respuesta::noContent(204);
			}
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function eliminaNumeroOrden(Request $request)
	{
		try {
			ParametrosHttpValidador::validar(
				$request,
				$this->servicio->getReglasNumeroOrden()
			);

			$data = $this->servicio->getWhereInFirst(CuentasPorCobrarModel::NUMERO_ORDEN, [$request->get(CuentasPorCobrarModel::NUMERO_ORDEN)]);

			if (!$data) {
				throw new ParametroHttpInvalidoException([
					'mensaje' => 'No se encontró el número de orden'
				]);
			}
			if ($data['estatus_cuenta_id'] == 1) {
				$api = $this->servicioCurl->curlGet(env('URL_CONTABILIDAD') . 'asientos/api/detalle?folio=' . $data->folio_id, false);
				$respuesta = json_decode($api);
				$tiene_asientos = $respuesta->data ? $respuesta->data[0] : [];
				$mensaje = "Cuenta eliminada correctamente";
				if ($tiene_asientos) {
					$contabilidad_api = $this->servicioCurl->curlPost(env('URL_CONTABILIDAD') . 'asientos/api/aplicar_folio', [
						'folio' => $data->folio_id,
						'estatus' => 'ANULADO',
						'sucursal_id' => env('SUCURSAL_DMS') == 'QUERETARO' ? AsientoModel::SUCURSAL_QUERETARO : AsientoModel::SUCURSAL_SANJUAN
					], false);
					$respuesta = json_decode($contabilidad_api);
					if ($respuesta->status != 'error') {
						$this->elimina_abonos($data->id);
						return Respuesta::json($data, 200, $mensaje);
					} else {
						throw new ParametroHttpInvalidoException([
							'mensaje' => self::$E0016_NOT_STATUS_CUENTA
						]);
					}
				} else {
					$this->elimina_abonos($data->id);
					return Respuesta::json($data, 200, $mensaje);
				}
			} else {
				throw new ParametroHttpInvalidoException([
					'mensaje' => 'Ya existe un pago en proceso para este número de orden, Favor de pasar a Cajas'
				]);
			}
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function elimina_abonos($id)
	{
		$abonos = $this->servicioAbono->getWhere(CuentasPorCobrarModel::CUENTA_POR_COBRAR_ID, $id);
		if ($abonos) {
			foreach ($abonos as $abono) {
				$this->servicioAbono->eliminar($abono->id);
			}
		}
		$this->servicio->eliminar($id);
	}

	public function showByNumeroOrden($numero_orden)
	{
		$modelo = $this->servicio->getByNumeroOrden($numero_orden);
		return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
	}

	public function getByNumeroOrden($numero_orden)
	{
		$modelo = $this->servicio->getDataNumeroOrden($numero_orden);
		return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
	}

	public function showDate()
	{
		dd(date('Y-m-d H:m:i'));
	}

	public function sendComprobanteFiscal(Request $request)
	{
		try {
			ParametrosHttpValidador::validar_array([
				CuentasPorCobrarModel::CUENTA_POR_COBRAR_ID => $request->get('cuenta_por_cobrar_id')
			], $this->servicio->getReglasByCuentaID());
			$data = $this->servicio->getById($request->get('cuenta_por_cobrar_id'));
			$api =  $this->servicioCurl->curlGet(env('URL_CONTABILIDAD') . 'facturacion/api/getComprobanteFiscal?folio=' . $data->folio_id, false);
			$respuesta = (json_decode($api));
			$data->url_pdf = $respuesta->data->pdf;
			if (!empty($data->cliente->correo_electronico)) {
				Mail::to($data->cliente->correo_electronico)->send(new EnviaFactura($data));
				return Respuesta::json($data, 200);
			} else {
				throw new ParametroHttpInvalidoException([
					'mensaje' => 'NO SE TIENE CORREO ELECTRÓNICO DEL CLIENTE'
				]);
			}
			return Respuesta::json($data, 200);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function generaFacturaComplemento(Request $request)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasFacturaComplemento());
			$modelo = $this->servicio->getByIdCuenta($request->get('cuenta_por_cobrar_id'));
			$folio = $modelo->folio_id;
			$api =  $this->servicioCurl->curlGet(env('URL_CONTABILIDAD') . 'facturacion/api/getComprobanteFiscal?folio=' . $folio, false);
			$response_factura = (json_decode($api));
			if (!$response_factura->data) {
				throw new ParametroHttpInvalidoException([
					'mensaje' => 'No se pudo obtener el SAT_UUID'
				]);
			} else {
				$abonos_pendientes = $this->servicioAbono->getAbonosPorPagar(['orden_entrada_id' => $modelo->id, 'estatus_abono_id' => 1]);
				$monto_pendiente = ($this->procesar_total_abonos($abonos_pendientes));
				$abono_actual = $this->servicioAbono->getById($request->get('abono_id'));
				$saldo_actual =  $monto_pendiente['total_pendiente'] +  $abono_actual->total_pago;
				$nuevo_saldo = $saldo_actual - $abono_actual->total_pago;
				$cliente = $this->servicioClientes->getById($modelo->cliente_id);
				$newDateTime = Carbon::now()->subHours(2)->format('Y-m-d H:i:s');
				$newDateTime2 = Carbon::now()->subHours(1)->format('Y-m-d H:i:s');
				$fechahora = (str_replace(' ', 'T', $newDateTime));
				$fechahora2 = (str_replace(' ', 'T', $newDateTime2));
				$dataFolio = $this->servicioFolios->getById($folio);


				$arrayVar = [
					"uuid" => $response_factura->data->sat_uuid,
					"seriep" => $abono_actual['id'],
					"foliop" => $dataFolio->folio . '-' . $request->get('parcialidad'),
					"moneda_pe" => "MXN",
					"tipo_cambio_p" => "1",
					"metodo_pago_p" => "PPD",
					"numero_parcial" => $request->get('parcialidad'),
					"deuda_pagar" => '' . $saldo_actual . '',
					"importe_pagado" => '' . $abono_actual->total_pago . '',
					"nuevo_saldo" => '' . $nuevo_saldo . '',
					"receptor_nombre" => $cliente->nombre . ' ' . $cliente->apellido_paterno . ' ' .  $cliente->apellido_materno,
					"lugarExpedicion" => $cliente->codigo_postal,
					"fecha_pago" => $fechahora,
					"receptor_email" => $cliente->correo_electronico ? strtolower($cliente->correo_electronico) : 'notiecorreo@sohex.com',
					"factura_folio" => $dataFolio->folio . '-' . $request->get('parcialidad'),
					"factura_serie" => $abono_actual['id'],
					"comentario" => "comentarios",
					"receptorRFC" => $cliente->rfc,
					"complemento_fecha" => $fechahora2,
					"complemento_forma_pago" => $abono_actual['clave_pago'],
					"complemento_tipoCambio" => "1",
					"complemento_totalPago" => '' . $abono_actual->total_pago . '',
					"complemento_no_operacion" => "-",
					"complemento_banco_ordenante" => "-",
					"complemento_cta_ordenante" => "-",
					"complemento_rfc_beneficiario" => "-",
					"complemento_cta_beneficiario" => "-",
					"complemento_rfc_ordenante" => "-",
				];
				ParametrosHttpValidador::validar_array(
					$arrayVar,
					$this->servicio->getReglasValidateComplementoPago()
				);
				$api_complemento = $this->servicioCurl->curlPostJson('https://mylsa.iplaneacion.com/dms/contabilidad_queretaro/index.php/complemento_pago/api_genera_complemento', @json_encode($arrayVar));
				$response_complemento = json_decode($api_complemento);
				// dd(@json_encode($arrayVar),$response_complemento);

				if (isset($response_complemento) && $response_complemento->error == '0') {
					$data = [
						'sat_uuid' => $response_complemento->sat_uuid,
						'folio' => $folio,
						'abono_id' => $request->get('abono_id'),
						'sucursal_id' =>  env('SUCURSAL_DMS') == "QUERETARO" ?  1 : null,
						'respuesta_api' => @json_encode($response_complemento),
						'pdf' => $response_complemento->pdf,
					];
					$api_save_complemento = $this->servicioCurl->curlPost(env('URL_CONTABILIDAD') . 'facturacion/api/save_complemento', $data, false);
					$response_save = (json_decode($api_save_complemento));
					if ($response_save->status == 'success') {
						$this->servicioAbono->massUpdateWhereId('id', $request->get('abono_id'),  [AbonosModel::ESTATUS_ABONO_ID => 4]); //PAGADO
					}
					return Respuesta::json($response_complemento, 200);
				} else {
					if (isset($response_complemento) && $response_complemento->error_mensaje) {
						throw new ParametroHttpInvalidoException([
							'mensaje' => $response_complemento->error_mensaje
						]);
					} else {
						throw new ParametroHttpInvalidoException([
							'mensaje' => 'Ocurrio un error al generar la factura'
						]);
					}
				}
			}
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	private function procesar_enganche($enganches)
	{
		$total_enganche = 0;
		if (isset($enganches) && is_array($enganches)) {
			foreach ($enganches as $enganche) {
				if ($enganche->estatus_abono_id != 3) {
					$total_enganche += $enganche->total_pago;
				}
			}
		}
		return $total_enganche;
	}

	private function procesar_total_abonos($abonos)
	{
		$total_abono = 0;
		$total_pago = 0;
		if (isset($abonos) && ($abonos)) {
			foreach ($abonos as $abono) {
				$total_pago += $abono->total_pago;
				$total_abono += $abono->total_abono;
			}
		}
		return [
			'total_pendiente' => $total_abono,
			'total_pagado' => $total_pago
		];
	}

	public function envioCorreo(Request $request)
	{
		try {

			ParametrosHttpValidador::validar($request, [
				'correo_electronico' => 'required',
				'titulo' => 'required|string',
				'cuerpo' => 'required',
				'nombre_completo' => 'required'
			]);
			$correos = explode(",", trim($request->correo_electronico));
			if ($correos) {
				foreach ($correos as $correo) {
					Mail::to(trim($correo))->send(new CorreoApi($request));
				}
			}
			return Respuesta::json(true, 200, 'Correo electrónico enviado correctamente');
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function showDataByNumeroOrden(Request $request)
	{
		try {
			$servicio = $this->servicio->getAll($request);
			$respuesta = json_decode($servicio);
			return Respuesta::json($respuesta, 200);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function getDataEreactByNumeroOrden(Request $request)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasNumeroOrden());
			$servicio = $this->servicio->getDataByNumeroOrden($request);
			return Respuesta::json($servicio[0] ?? [], 200);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function cancelarFactura(Request $request)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasCancelarFactura());
			$data = $this->servicio->getById($request->cuenta_por_cobrar_id);
			// $api =  $this->servicioCurl->curlGet(env('URL_CONTABILIDAD') . 'facturacion/api/getComprobanteFiscal?folio=' . $data->folio_id, false);
			$api =  $this->servicioCurl->curlGet('https://mylsa.iplaneacion.com/dms/contabilidad_queretaro/index.php/facturacion/api/getComprobanteFiscal?folio=' . $data->folio_id, false);
			$respuesta = (json_decode($api));
			if ($respuesta->status == "success" && $respuesta->data->factura_id) {
				$api_cancela_factura = $this->servicioCurl->curlGet('https://mylsa.iplaneacion.com/dms/contabilidad_queretaro/index.php/facturacion/cancelar_factura/' . $respuesta->data->factura_id, false);
				$response_cancela_factura = json_decode($api_cancela_factura);
				if (isset($response_cancela_factura->cancelar_EstatusUUID)) {
					$this->servicio->massUpdateWhereId('id', $request->cuenta_por_cobrar_id, [CuentasPorCobrarModel::ESTATUS_CUENTA_ID => EstatusCuentaModel::ESTATUS_FACTURA_CANCELADO]);
					return Respuesta::json($respuesta, 200);
				} else {
					throw new ParametroHttpInvalidoException([
						'mensaje' => 'Ocurrio un error al cancelar la factura'
					]);
				}
			}
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}
}
