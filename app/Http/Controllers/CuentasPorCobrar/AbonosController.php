<?php

namespace App\Http\Controllers\CuentasPorCobrar;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\CuentasPorCobrar\ServicioAbono;
use App\Servicios\Core\Respuestas\Respuesta;
use Illuminate\Http\Request;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\CuentasPorCobrar\ServicioAsientos;
use App\Servicios\Refacciones\ServicioCurl;
use App\Mail\EmailAbonoRealizado;
use App\Models\CuentasPorCobrar\AbonosModel;
use Illuminate\Support\Facades\Mail;
use App\Exceptions\ParametroHttpInvalidoException;


class AbonosController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioAbono();
        $this->servicioAsientos = new ServicioAsientos();
        $this->servicioCurl = new ServicioCurl();
    }

    public function showByIdOrdenEntrada(Request $request)
    {
        try {

            $modelo = $this->servicio->getAbonosPorPagar($request->toArray());
            return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function listadoAbonosByOrdenEntrada(Request $request)
    {
        try {
            $modelo = $this->servicio->getAbonosPorPagar($request->toArray());
            return Respuesta::json(['data' => $modelo], empty($modelo) ? 404 : 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getVerificaAbonosPendientes(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasVerificaAbonosPendientes());
            $modelo = $this->servicio->getVerificaAbonosPendientes($request->toArray());
            return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasUpdate());
            $movimientos = json_encode($this->servicio->set_movimiento($request->toArray()));
            if ($movimientos) {
                $modelo = $this->servicio->actualiza_abonos($request, $id);
                $mensaje = __(static::$I0004_RESOURCE_UPDATED, ['parametro' => $this->servicio->getRecurso()]);
                return Respuesta::json($modelo, 200, $mensaje);
            } else {
                throw new ParametroHttpInvalidoException([
                    'msg' => "Error al crear la poliza en caja."
                ]);
            }
            //Mail::to($modelo->correo_electronico)->send(new EmailAbonoRealizado($modelo));
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function recalcularAbonos($cuenta_por_cobrar_id)
    {
        try {
            $modelo = $this->servicio->recalcularAbonos($cuenta_por_cobrar_id);
            $mensaje = __(static::$I0004_RESOURCE_UPDATED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($modelo, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function updateEstatus(Request $request, $id)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasEstatus());
            $mensaje = __(static::$I0004_RESOURCE_UPDATED, ['parametro' => $this->servicio->getRecurso()]);
            $modelo = $this->servicio->changeEstatus($request, $id);
            return Respuesta::json($modelo, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getAbonoByFolio(Request $request)
    {
        try {

            $modelo = $this->servicio->getAbonoByFolio($request);
            return Respuesta::json($modelo);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getAbonosPagadosPorFecha(Request $request)
    {
        try {
            $modelo = $this->servicio->getAbonosPagadosPorFecha($request);
            return Respuesta::json($modelo, empty($modelo) ? 204 : 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
