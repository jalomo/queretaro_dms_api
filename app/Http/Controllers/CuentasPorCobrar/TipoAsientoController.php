<?php

namespace App\Http\Controllers\CuentasPorCobrar;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\CuentasPorCobrar\ServicioTipoAsiento;

class TipoAsientoController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioTipoAsiento();
    }
}
