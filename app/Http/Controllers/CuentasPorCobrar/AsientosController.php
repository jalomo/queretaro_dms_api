<?php

namespace App\Http\Controllers\CuentasPorCobrar;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\CuentasPorCobrar\ServicioAsientos;
use App\Servicios\CuentasPorCobrar\ServicioCuentaPorCobrar;
use App\Servicios\Refacciones\ServicioReVentasEstatus;
use App\Servicios\Polizas\ServicioMovimientos;
use App\Models\CuentasPorCobrar\CuentasPorCobrarModel;
use App\Models\CuentasPorCobrar\EstatusCuentaModel;
use App\Models\Refacciones\VentasRealizadasModel;
use App\Models\Refacciones\VentaProductoModel;
use App\Models\Refacciones\ReVentasEstatusModel;
use App\Models\Refacciones\EstatusVentaModel;
use App\Servicios\Inventario\DevolverVentaService;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Exceptions\ParametroHttpInvalidoException;
use Illuminate\Http\Request;


class AsientosController extends CrudController
{
	public function __construct()
	{
		$this->servicio = new ServicioAsientos();
		$this->servicioCuentasPorCobrar = new ServicioCuentaPorCobrar();
		$this->modelo_ventas_realizadas = new VentasRealizadasModel();
		$this->modelo_venta_producto = new VentaProductoModel();
		$this->servicioMovimientos = new ServicioMovimientos();
		$this->servicioReVentasEstatus = new ServicioReVentasEstatus();
	}

	public function setAsientoContabilidad(Request $request)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
			$modelo = $this->servicio->curl_asiento_api($request->all());
			$respuesta = (json_decode($modelo));
			if ($respuesta->status == 'success') {
				$mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
				return Respuesta::json($respuesta, 201, $mensaje);
			} else {
				throw new ParametroHttpInvalidoException([
					'msg' => "Ocurrio un error al crear el asiento"
				]);
			}
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function registrarPolizaHojalateria(Request $request)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasPolizaHojalateria());
			$modelo = $this->servicio->servicioPolizaHojalateria($request->toArray());
			$mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
			return Respuesta::json($modelo, 201, $mensaje);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function registrarPolizaServicio(Request $request)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasPolizaServicio());
			$modelo = $this->servicio->servicioPolizaServicios($request->toArray());
			if ($modelo) {
				$mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
				return Respuesta::json($modelo, 201, $mensaje);
			} else {
				throw new ParametroHttpInvalidoException([
					'msg' => "Ocurrio un error al procesar la información, Intente nuevamente !"
				]);
			}
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}
	public function registrarActualizaPoliza(Request $request)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasActualizaPolizaServicio());
			$modelo = $this->servicio->servicioActualizaPoliza($request->get('cuenta_por_cobrar_id'), $request->get('tipo_descuento'));
			$mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
			return Respuesta::json($modelo, 201, $mensaje);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function registrarPolizaVentaMostrador(Request $request)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasPolizaVentaMostrador());
			$modelo = $this->servicio->servicioPolizaVentaMostrador($request->toArray());
			$mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
			return Respuesta::json($modelo, 201, $mensaje);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function registrarPolizaVentaMostradorCredito(Request $request)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasPolizaVentaMostradorCredito());
			$modelo = $this->servicio->servicioPolizaVentaMostradorCredito($request->toArray());
			$mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
			return Respuesta::json($modelo, 201, $mensaje);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function registrarPolizaGarantias(Request $request)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasPolizaGarantias());
			$modelo = $this->servicio->servicioPolizaGarantias($request->toArray());
			$mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
			return Respuesta::json($modelo, 201, $mensaje);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function registrarPolizaServicioInterno(Request $request)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasPolizaServicioInterno());
			$modelo = $this->servicio->servicioPolizaServicioInterno($request->toArray());
			$mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
			return Respuesta::json($modelo, 201, $mensaje);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function registrarPolizaRefaccionTaller(Request $request)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasPolizaRefaccionTaller());
			$modelo = $this->servicio->servicioPolizaRefaccionTaller($request->toArray());
			$mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
			return Respuesta::json($modelo, 201, $mensaje);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function registrarPolizaCompraFordPlanta(Request $request)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasPolizaCompraFordPlanta());
			$modelo = $this->servicio->servicioPolizaCompraFordPlanta($request->toArray());
			$mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
			return Respuesta::json($modelo, 201, $mensaje);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function registrarPolizaCompraProveedorExterno(Request $request)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasPolizaCompraProveedorExterno());
			$modelo = $this->servicio->servicioPolizaCompraProveedorExterno($request->toArray());
			$mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
			return Respuesta::json($modelo, 201, $mensaje);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function registrarPolizaDevolucionServicio(Request $request)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasPolizaDevolucionServicio());
			$modelo = $this->servicio->servicioPolizaDevolucionServicios($request->toArray());
			if ($modelo) {
				$this->servicioCuentasPorCobrar->massUpdateWhereId('id', $request->cuenta_por_cobrar_id, [CuentasPorCobrarModel::ESTATUS_CUENTA_ID => EstatusCuentaModel::ESTATUS_DEVOLUCION]);
				$mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
				return Respuesta::json($modelo, 201, $mensaje);
			} else {
				throw new ParametroHttpInvalidoException([
					'msg' => "Ocurrio un error al procesar la información, Intente nuevamente !"
				]);
			}
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function registrarPolizaDevolucionMostrador(Request $request)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasPolizaDevolucionMostrador());
			$data = $this->servicioMovimientos->getTipoCuentaxCobrar($request['cuenta_por_cobrar_id']);
			$modelo = $this->servicio->servicioPolizaDevolucionMostrador($data->toArray());
			if ($modelo) {
				$servicioCuenta = $this->servicioCuentasPorCobrar->massUpdateWhereId('id', $request->cuenta_por_cobrar_id, [CuentasPorCobrarModel::ESTATUS_CUENTA_ID => EstatusCuentaModel::ESTATUS_DEVOLUCION]);
				if ($servicioCuenta) {
					$venta = $this->modelo_ventas_realizadas->where(VentasRealizadasModel::FOLIO_ID, $data->folio_id)->first();
					$venta_productos = $this->modelo_venta_producto->where(VentaProductoModel::VENTA_ID, $venta->id)->get();

					foreach ($venta_productos as $producto) {
						$devolucion = new DevolverVentaService;
						$devolucion->id($producto->id);
						$devolucion->handle($producto->producto_id, $producto->cantidad,$producto->valor_unitario);
					}

					$this->servicioReVentasEstatus->storeReVentasEstatus([
						ReVentasEstatusModel::VENTA_ID => $venta->id,
						ReVentasEstatusModel::ESTATUS_VENTA_ID => EstatusVentaModel::ESTATUS_DEVOLUCION
					]);

					$mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
					return Respuesta::json($modelo, 201, $mensaje);
				} else {
					throw new ParametroHttpInvalidoException([
						'msg' => "Ocurrio un error al cancelar la cuenta !"
					]);
				}
			} else {
				throw new ParametroHttpInvalidoException([
					'msg' => "Ocurrio un error al procesar la información, Intente nuevamente !"
				]);
			}
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function aplicaAsientosCuentas(Request $request)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasAplicarCuentas());
			$movimientos = json_encode($this->servicio->aplicaAsientosCuentas($request->toArray()));
			if ($movimientos) {
				return Respuesta::json($movimientos, 200);
			} else {
				throw new ParametroHttpInvalidoException([
					'msg' => "Error al crear la poliza en caja."
				]);
			}
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}
}
