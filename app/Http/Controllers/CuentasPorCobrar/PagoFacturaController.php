<?php

namespace App\Http\Controllers\CuentasPorCobrar;

use App\Http\Controllers\Core\CrudController;
use App\Http\Requests\CuentasPorCobrar\PagoFacturaActualizarRequest;
use App\Http\Requests\CuentasPorCobrar\PagoFacturadoRequest;
use App\Http\Requests\CuentasPorCobrar\PagoFacturaGuardarRequest;
use App\Models\CuentasPorCobrar\CuentasPorCobrarModel;
use App\Models\CuentasPorCobrar\PagosFacturaModel;
class PagoFacturaController extends CrudController
{
    public $cuenta_por_cobrar_id;
    public function __construct()
    {
        $this->modelo = new PagosFacturaModel();
        $this->cuenta_por_cobrar_id = null;
    }
    public function guardar(PagoFacturaGuardarRequest $request)
    {
        try {
            $this->cuenta_por_cobrar_id = $request->cuenta_por_cobrar_id;
            $importeValido = $this->validarImporte($request->importe);
            if (!$importeValido) {
                $respuesta = [
                    'status' => "error",
                    'message' => "Importe mayor supera el total de la cuenta",
                    'data' => [],
                ];
                return response()->json($respuesta);
            }
            $pago = new PagosFacturaModel();
            $pago->cuenta_por_cobrar_id = $request->cuenta_por_cobrar_id;
            $pago->tipo_pago_id = $request->tipo_pago_id;
            $pago->cfdi_id = $request->cfdi_id;
            $pago->importe = $request->importe;
            $pago->regimen_fiscal = $request->regimen_fiscal;
            $pago->facturado = $request->facturado ? $request->facturado : 0;
            $pago->save();
            $respuesta = [
                'status' => "success",
                'message' => "Pago guardado correctamente",
                'data' => $pago,
            ];
            return response()->json($respuesta);
        } catch (\Throwable $e) {
            $respuesta = [
                'status' => "error",
                'message' => "Pago no guardado",
                'data' => $e,
            ];
            return response()->json($respuesta);
        }
    }

    public function actualizar(PagoFacturaActualizarRequest $request, $id)
    {
        try {
            $importeValido = $this->validarImporte($request->importe);
            if (!$importeValido) {
                $respuesta = [
                    'status' => "error",
                    'message' => "Importe mayor supera el total de la cuenta",
                    'data' => [],
                ];
                return response()->json($respuesta);
            }
            $pago =  PagosFacturaModel::find($id);
            $pago->cuenta_por_cobrar_id = $request->cuenta_por_cobrar_id;
            $pago->tipo_pago_id = $request->tipo_pago_id;
            $pago->cfdi_id = $request->cfdi_id;
            $pago->importe = $request->importe;
            $pago->regimen_fiscal = $request->regimen_fiscal;
            $pago->facturado = $request->facturado ? $request->facturado : 0;
            $pago->save();
            $respuesta = [
                'status' => "success",
                'message' => "Pago actualizado correctamente",
                'data' => $pago,
            ];
            return response()->json($respuesta);
        } catch (\Throwable $e) {
            $respuesta = [
                'status' => "error",
                'message' => "Pago no actualizado",
                'data' => $e,
            ];
            return response()->json($respuesta);
        }
    }

    public function eliminar($id)
    {
        try {
            $pago = PagosFacturaModel::find($id);
            if ($pago->delete()) {
                $respuesta = [
                    'status' => "success",
                    'message' => "Pago eliminado correctamente",
                    'data' => $pago,
                ];
                return response()->json($respuesta);
            } else {
                $respuesta = [
                    'status' => "error",
                    'message' => "Pago no eliminado",
                    'data' => $pago,
                ];
                return response()->json($respuesta);
            }
        } catch (\Throwable $th) {
            $respuesta = [
                'status' => "error",
                'message' => "Pago no eliminado",
                'data' => $th,
            ];
            return response()->json($respuesta);
        }
    }
    public function GetByCuentaPorCobrarId($id)
    {
        $data = $this->modelo->select(
            'pagos_factura.*', 'cat_tipo_pago.nombre as tipo_pago', 'catalogo_cfdi.descripcion as cfdi_descripcion'
        )
            ->join('cat_tipo_pago', 'pagos_factura.tipo_pago_id', '=', 'cat_tipo_pago.id')
            ->join('catalogo_cfdi', 'pagos_factura.cfdi_id', '=', 'catalogo_cfdi.id')
            ->where('cuenta_por_cobrar_id', $id)->get();
        return response()->json($data);
    }

    public function facturado(PagoFacturadoRequest $request)
    {

        if ($request->id) {
            $pagoFactura =  PagosFacturaModel::find($request->id);
            $pagoFactura->factura_id = $request->factura_id;
            $pagoFactura->sat_uuid = $request->sat_uuid;
            $pagoFactura->sat_pdf = $request->sat_pdf;
            $pagoFactura->facturado = 1;
        }

        if ($pagoFactura->save()) {
            $respuesta = [
                'status' => "success",
                'message' => "Factura generada correctamente",
                'data' => $pagoFactura,
            ];
            return response()->json($respuesta);
        } else {
            $respuesta = [
                'status' => "error",
                'message' => "No se genero la factura",
                'data' => $pagoFactura,
            ];
            return response()->json($respuesta);
        }
    }

    public static function getPagosByCauntaPorCobrarId($id)
    {
        $pagos = PagosFacturaModel::where(PagosFacturaModel::CUENTA_POR_COBRAR_ID, $id)->get();
        return $pagos;
    }

    public static function getPagoByCuentaPorCobrar($cuenta_por_cobrar_id)
    {
        return PagosFacturaModel::where(PagosFacturaModel::CUENTA_POR_COBRAR_ID, $cuenta_por_cobrar_id)->get();
    }

    public function validarImporte($importe = null)
    {
        $cuentaPorCobrar = self::getCuentaPorCobrarById($this->cuenta_por_cobrar_id);
        $pagos = self::getPagoByCuentaPorCobrar($this->cuenta_por_cobrar_id);
        $totalPagado = 0;
        $totPagado = 0;
        foreach ($pagos as $pago) {
            $totalPagado += $pago->importe;
        }
        $totPagado = $totalPagado + $importe;
        $importeValido = false;

        if ($totPagado <= $cuentaPorCobrar->total) {
            $importeValido = true;
        }
        return $importeValido;
    }

    public static function getCuentaPorCobrarById($cuenta_por_cobrar_id)
    {
        return CuentasPorCobrarModel::whereId($cuenta_por_cobrar_id)->first();
    }
}
