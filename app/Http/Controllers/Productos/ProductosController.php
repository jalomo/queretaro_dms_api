<?php

namespace App\Http\Controllers\Productos;

use App\Http\Controllers\Core\Controller;
use App\Http\Requests\Productos\ShowRequest;
use App\Http\Resources\Productos\ProductoResource;
use App\Models\Refacciones\ProductosModel;

class ProductosController extends Controller
{
    public function show(ShowRequest $request)
    {
        $producto = ProductosModel::with('ubicacion', 'desglose_producto')->where('no_identificacion', mb_strtoupper($request->no_identificacion))->first();

        if(is_null($producto))
        {
            return response()->json(['message' => __('La refacción buscada :no_identificacion no existe.', ['no_identificacion' => $request->no_identificacion ])], 400);
        }

        return response()->json(new ProductoResource($producto));
    }
}