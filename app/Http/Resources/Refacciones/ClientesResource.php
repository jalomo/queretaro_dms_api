<?php

namespace App\Http\Resources\Refacciones;

use Illuminate\Http\Resources\Json\JsonResource;

class ClientesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nombre_cliente' => $this->nombre_cliente,
            'nombre' => $this->nombre,
            'nombre_empresa' => $this->nombre_empresa,
            'numero_cliente' => $this->numero_cliente,
            'direccion_completa' => $this->direccion_completa,
            'telefono' => $this->telefono,
            'aplica_credito' => $this->aplica_credito,
            'limite_credito' => $this->limite_credito,
        ];
    }
}
