<?php

namespace App\Http\Resources\Productos;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'no_identificacion' => $this->no_identificacion,
            'prefijo' => $this->prefijo,
            'basico' => $this->basico,
            'sufijo' => $this->sufijo,
            'descripcion' => $this->descripcion,
            'valor_unitario' => $this->valor_unitario,
            'costo_promedio' => $this->costo_promedio,
            'ubicacion' => $this->ubicacion->nombre?? 'GEN.',
            'existencia' => $this->desglose_producto->cantidad_actual,
        ];
    }
}
