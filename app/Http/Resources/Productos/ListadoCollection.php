<?php

namespace App\Http\Resources\Productos;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ListadoCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'iTotalRecords' => $this->perPage(),
            'iTotalDisplayRecords' => $this->total(), 
            'aaData' => ListadoResource::collection($this->collection),
            'draw' => request()->input('draw'),
        ];
    }
}
