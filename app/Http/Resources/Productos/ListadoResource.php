<?php

namespace App\Http\Resources\Productos;

use Illuminate\Http\Resources\Json\JsonResource;

class ListadoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'no_identificacion' => $this->no_identificacion,
            'descripcion' => $this->descripcion,
            'grupo' => $this->gpo,
            'basico' => $this->basico,
            'prefijo' => $this->prefijo,
            'sufijo' => $this->sufijo,
            'ubicacion' => $this->ubicacion->nombre?? 'GEN.',
            'proveedor' => 'FORD',
        ];
    }
}
