<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class DataTablesResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'iTotalRecords' => $this->perPage(),
            'iTotalDisplayRecords' => $this->total(), 
            'aaData' => $this->collection,
            'draw' => request()->input('draw'),
        ];
    }
}
