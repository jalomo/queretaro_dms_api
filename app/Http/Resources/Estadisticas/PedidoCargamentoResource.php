<?php

namespace App\Http\Resources\Estadisticas;

use Illuminate\Http\Resources\Json\JsonResource;

class PedidoCargamentoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'folio' => $this->pedido->id,
            'proveedor' => $this->proveedor?? 'Sin Proveedor',
            'factura' => $this->factura?? 'Sin Factura', 
            'no_identificacion' => $this->producto->no_identificacion ?? 'No aplica',
            'descripcion' => $this->producto->descripcion ?? 'No aplica',
            'cantidad' => $this->cantidad_entregada,
            'valor_unitario' => $this->total? (double) $this->total : (double) $this->precio,
            'iva' => $this->total * 0.16,
            'total' => $this->total * 1.16,
            'created_at' => $this->pedido->created_at,
        ];
    }
}
