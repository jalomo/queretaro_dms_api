<?php

namespace App\Http\Resources\Estadisticas;

use Illuminate\Http\Resources\Json\JsonResource;

class TraspasoProductosResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'no_identificacion' => $this->no_identificacion,
            'descripcion' => new ProductoDescripcion($this->producto),
            'cantidad' => $this->cantidad,
            'cantidad_entregada' => $this->cantidad_entregada,
            'cantidad_devuelta' => $this->cantidad_devuelta,
            'valor_unitario' => $this->valor_unitario,
        ];
    }
}
