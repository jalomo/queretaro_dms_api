<?php

namespace App\Http\Resources\Estadisticas;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductoDescripcion extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->descripcion;
    }
}
