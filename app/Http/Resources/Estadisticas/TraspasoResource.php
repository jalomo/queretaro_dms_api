<?php

namespace App\Http\Resources\Estadisticas;

use Illuminate\Http\Resources\Json\JsonResource;

class TraspasoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->traspaso->id,
            'almacen_origen' => $this->traspaso->almacen_origen->nombre,
            'almacen_destino' => $this->traspaso->almacen_destino->nombre,
            'no_identificacion' => $this->no_identificacion ?? 'No aplica',
            'descripcion' => $this->producto->descripcion ?? 'No aplica',
            'cantidad' => $this->cantidad,
            'cantidad_entregada' => $this->cantidad_entregada,
            'cantidad_devuelta' => $this->cantidad_devuelta,
            'valor_unitario' => $this->total? $this->total_entregado - $this->total_devuelto : $this->valor_unitario,
            'created_at' => $this->traspaso->created_at,
        ];
    }
}
