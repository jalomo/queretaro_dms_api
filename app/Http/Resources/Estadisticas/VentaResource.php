<?php

namespace App\Http\Resources\Estadisticas;

use Illuminate\Http\Resources\Json\JsonResource;

class VentaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'cliente' => $this->venta->cliente != null ? "{$this->venta->cliente->nombre} {$this->venta->cliente->apellido_paterno} {$this->venta->cliente->apellido_materno}" : 'Sin Cliente',
            'folio' => $this->venta->numero_orden?? $this->venta->folio->folio,
            'no_identificacion' => $this->producto->no_identificacion?? 'No aplica',
            'descripcion' => $this->producto->descripcion?? 'No aplica',
            'cantidad' => $this->cantidad,
            'tipo_venta' => $this->venta->numero_orden? 'Venta Detalles' : 'Venta Mostrador',
            'valor_unitario' => $this->total?? (double) $this->valor_unitario,
            'iva' => $this->total? $this->total * 0.16 : ($this->venta->numero_orden? $this->valor_unitario * $this->cantidad * 0.16 : ($this->valor_unitario * $this->cantidad) - ($this->valor_unitario * $this->cantidad / 1.16)),
            'total' => $this->total? $this->total * 1.16 : $this->valor_unitario * $this->cantidad,
            'created_at' => $this->venta->created_at,
        ];
    }
}
