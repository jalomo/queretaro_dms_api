<?php

namespace App\Http\Resources\Estadisticas;

use Illuminate\Http\Resources\Json\JsonResource;

class DevolucionResourcePorOrden extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'cliente' => $this->venta != null && $this->venta->venta->cliente != null ? "{$this->venta->venta->cliente->nombre} {$this->venta->venta->cliente->apellido_paterno} {$this->venta->venta->cliente->apellido_materno}" : 'Sin Cliente',
            'folio' => $this->venta != null? ($this->venta->venta->numero_orden?? $this->venta->venta->folio->folio) : 'No aplica',
            'no_identificacion' => 'No aplica',
            'descripcion' => 'No aplica',
            'cantidad' => ($this->entradas - $this->salidas) == 0? $this->entradas : $this->devoluciones,
            'entradas' => $this->entradas,
            'salidas' => $this->salidas,
            'devoluciones' => $this->devoluciones,
            'tipo_venta' => $this->venta != null && $this->venta->venta->numero_orden? 'Venta Detalles' : 'Venta Mostrador',
            'valor_unitario' => abs($this->total),
            'iva' => abs($this->total) * 0.16,
            'total' => abs($this->total) * 1.16,
            'created_at' => $this->venta->created_at?? $this->created_at,
        ];
    }
}
