<?php

namespace App\Http\Resources\Cuentas;

use App\Servicios\Refacciones\ServicioCatalogoClaveCliente;
use App\Servicios\Contabilidad\ServicioProcesos;
use App\Servicios\Usuarios\ServicioUsuarios;
use Illuminate\Http\Resources\Json\JsonResource;

class CuentasResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        // $this->servicioCatalogoClaveCliente->getById($this->cliente->tipo_registro);
        $this->servicioCatalogoClaveCliente = new ServicioCatalogoClaveCliente();
        $this->servicioProcesos = new ServicioProcesos();
        $this->servicioUsuarios = new ServicioUsuarios();
        $name_cliente = explode(' ', $this->cliente->nombre)[1] ?? null;
        return [
            'numero_orden' => $this->numero_orden ?? null,
            'numero_factura' => $this->folio->folio ?? null,
            'fecha_venta' => $this->created_at ?? null,
            'apellido' => $this->cliente->apellido_paterno ?? null,
            'direccion' => $this->cliente->direccion ?? null,
            'direccion2' => '',
            'ciudad' => $this->cliente->municipio ?? null,
            'estado' => 'QE', //$this->cliente->estado ?? null, -- TO DO OBTENER LA SIGLA DESDE EL CATALOGO
            'codigo_postal' => $this->cliente->codigo_postal ?? null,
            'tipo_cliente' => $this->cliente->tipo_registro ? $this->servicioCatalogoClaveCliente->getById($this->cliente->tipo_registro)->nombre ?? null : null,
            'clave_cliente' => $this->cliente->rfc ?? null,
            'impuestos' => '',
            'seguro_social_vendedor' => '',
            'identificador_vendedor_distribuidora' => '',
            'identificador_vendedor_stars' => '',
            'nombre' => $this->cliente->nombre ?? null,
            'inicial_segundo_nombre' =>  substr($name_cliente,0,1) ?? null,
            'codigo_pais_cliente' => 'MEX', // $this->cliente->pais ?? null -- TO DO OBTENER SIGLA DE MÉXICO O RESTRINGIR AL MISMO PAIS,
            'telefono_principal' => $this->cliente->telefono ?? null,
            'telefono_secundario' => $this->cliente->telefono_secundario ?? null,
            'telefono_adicional' => $this->cliente->telefono_oficina ?? null,
            'correo_electronico' => $this->cliente->correo_electronico ?? null,
            'monto_descuento' => '',
            'monto_traslado' => '',
            'tipo_venta' => $this->folio->tipo_proceso_id ? $this->servicioProcesos->getById($this->folio->tipo_proceso_id)->descripcion ?? null : null,
            'descripcion_tipo_venta' => $this->concepto ?? null,
            'direccion3' => '',
            'descripcion_compania' => '',
            'correo_electronico_empresa' => '',
            'correo_electronico2' => '',
            'correo_electronico3' => '',
            'numero_miembro_programa_lealtad' => '',
            'numero_cliente' => $this->cliente->numero_cliente ?? null,
        ];
    }
}
