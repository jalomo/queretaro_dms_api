<?php

namespace App\Http\Resources\Pedidos;

use Illuminate\Http\Resources\Json\JsonResource;

class FacturaByFolio extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'folio' => env('QUERETARO') == 'QUERETARO' ? "QRT-PEDIDO-{$this->id}" : "SNJ-PEDIDO-{$this->id}",
            'factura' => optional($this->piezas)->first()->factura,
        ];
    }
}
