<?php

namespace App\Http\Resources\Pedidos;

use Illuminate\Http\Resources\Json\ResourceCollection;

class VentaProductosCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'iTotalRecords' => $this->perPage(),
            'iTotalDisplayRecords' => $this->total(), 
            'aaData' => VentaProductosResource::collection($this->collection),
            'draw' => request()->input('draw'),
        ];
    }
}
