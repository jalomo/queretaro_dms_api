<?php

namespace App\Http\Resources\Pedidos;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductosContabilidadResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->pedido->id,
            'producto_id' => $this->id,
            'cantidad' => $this->pedido->cantidad_entregada,
            'no_identificacion' => $this->no_identificacion,
            'ubicacion' => is_null($this->ubicacion)? 'GEN.' : $this->ubicacion->nombre,
            'descripcion' => $this->descripcion,
            'created_at' => $this->created_at,
            'factura' => $this->pedido->factura,
            'deleted_at' => $this->deleted_at,
            'valor_unitario' => $this->pedido->precio,
            'iva' => $this->pedido->precio * 0.16,
            'total' => $this->pedido->total,
        ];
    }
}
