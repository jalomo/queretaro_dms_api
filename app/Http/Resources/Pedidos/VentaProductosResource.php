<?php

namespace App\Http\Resources\Pedidos;

use Illuminate\Http\Resources\Json\JsonResource;

class VentaProductosResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->producto->id,
            'cantidad' => $this->cantidad,
            'no_identificacion' => $this->producto->no_identificacion,
            'descripcion' => $this->producto->descripcion,
        ];
    }
}
