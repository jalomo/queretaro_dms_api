<?php

namespace App\Http\Resources\Pedidos;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductosResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'producto_id' => $this->producto_id,
            'cantidad' => $this->cantidad,
            'cantidad_entregada' => $this->cantidad_entregada,
            'no_identificacion' => $this->no_identificacion,
            'ubicacion' => $this->ubicacion?? 'GEN.',
            'descripcion' => $this->descripcion,
            'created_at' => $this->created_at,
            'origen' =>  $this->numero_orden? $this->numero_orden : $this->folio,
            'proveedor' => $this->proveedor,
            'total' => $this->total,
            'factura' => $this->factura? true : false,
            'deleted_at' => $this->deleted_at,
        ];
    }
}
