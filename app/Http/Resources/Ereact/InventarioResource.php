<?php

namespace App\Http\Resources\Ereact;

use Illuminate\Http\Resources\Json\JsonResource;

class InventarioResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'no_identificacion' => $this->producto->no_identificacion,
            'prefijo' => $this->producto->prefijo,
            'basico' => $this->producto->basico,
            'sufijo' => $this->producto->sufijo,
            'descripcion' => $this->producto->descripcion,
            'ubicacion' => $this->producto->ubicacion == null? 'GEN.' : $this->producto->ubicacion->nombre,
            'mes_1' => $this->mes_1,
            'mes_2' => $this->mes_2,
            'mes_3' => $this->mes_3,
            'mes_4' => $this->mes_4,
            'mes_5' => $this->mes_5,
            'mes_6' => $this->mes_6,
            'mes_7' => $this->mes_7,
            'mes_8' => $this->mes_8,
            'mes_9' => $this->mes_9,
            'mes_10' => $this->mes_10,
            'mes_11' => $this->mes_11,
            'mes_12' => $this->mes_12,
            'total' => $this->cantidad,
            'existencia' => $this->producto->desglose_producto->cantidad_actual,
        ];
    }
}
