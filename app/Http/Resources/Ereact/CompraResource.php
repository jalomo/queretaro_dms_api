<?php

namespace App\Http\Resources\Ereact;

use Illuminate\Http\Resources\Json\JsonResource;

class CompraResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'no_identificacion' => $this->producto->no_identificacion,
            'prefijo' => $this->producto->prefijo,
            'basico' => $this->producto->basico,
            'sufijo' => $this->producto->sufijo,
            'descripcion' => $this->producto->descripcion,
            'proveedor' => $this->proveedor,
            'rfc' => $this->rfc?? 'AAAA999999XXX',
            'tipo_venta' => $this->factura?? 'FO',
            'total' => number_format($this->total * 1.16, 2),
            'cantidad' => $this->cantidad,
            'iva' => number_format($this->total * 0.16, 2),
            'created_at' => $this->created_at,
        ];
    }
}
