<?php

namespace App\Http\Resources\Ereact;

use Illuminate\Http\Resources\Json\JsonResource;

class VentasResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'numero_parte' => $this->validarEspacios('DRE' . $this['numero_parte'], 75, '*', 1),
            'prefijo' => $this->validarEspacios($this['prefijo'], 25, '*', 1),
            'basico' => $this->validarEspacios($this['basico'], 25, '*', 1),
            'sufijo' => $this->validarEspacios($this['sufijo'], 25, ' ', 1),
            'descripcion' => $this->validarEspacios($this['descripcion'], 10, ' ', 1),
            'clave_star' => $this->validarEspacios($this['clave_star'], 10, ' ', 1),
            'nombre_vendedor' => $this->validarEspacios($this['nombre_vendedor'], 100, ' ', 1),
            'clave_cliente' => $this->validarEspacios($this['clave_cliente'], 10, ' ', 1),
            'nombre_cliente' => $this->validarEspacios($this['nombre_cliente'], 100, ' ', 1),
            'rfc_cliente' => $this->validarEspacios($this['rfc_cliente'], 15, ' ', 1),
            'cantidad' => $this->validarEspacios($this['cantidad'], 9, 0, 2),
            'precio_venta' => $this->validarEspacios(number_format($this['precio_venta'], 2), 11, 0, 2),
            'costo' => $this->validarEspacios(number_format($this['costo'], 2), 11, 0, 2),
            'factura_folio' => $this->validarEspacios($this['factura_folio'], 10, ' ', 1),
            'tipo_venta' => $this->validarEspacios($this['tipo_venta'] ?? 1, 1, ' ', 1),
            'iva' => $this->validarEspacios(number_format($this['precio_venta'] * 0.16, 2), 12, ' ', 1),
            'codigo_postal' => $this->validarEspacios($this['codigo_postal'], 5, ' ', 1)
        ];
    }

    public function validarEspacios($value, $cantidad_espacios, $elemento_remplazo = '*', $antes_despues = 2)
    {
        return strlen($value) <= $cantidad_espacios ? ($antes_despues == 1 ? $value . str_repeat($elemento_remplazo,  $cantidad_espacios - strlen($value)) : str_repeat($elemento_remplazo,  $cantidad_espacios - strlen($value)) . $value)  :  substr($value, 0, $cantidad_espacios);
    }
}
