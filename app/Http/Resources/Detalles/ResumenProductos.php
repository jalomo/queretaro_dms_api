<?php

namespace App\Http\Resources\Detalles;

use Illuminate\Http\Resources\Json\JsonResource;

class ResumenProductos extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'producto_id' => $this->producto->id,
            'no_identificacion' => $this->producto->no_identificacion,
            'descripcion' => $this->producto->descripcion,
            'ubicacion' => $this->ubicacion->nombre ?? 'GEN.',
            'cantidad' => $this->cantidad,
            'valor_unitario' => $this->valor_unitario,
            'total_sin_iva' => $this->venta_total,
            'iva' => $this->venta_total * 0.16,
            'total' => $this->venta_total * 1.16,
            'servicio' => false,
            'prepiking' => $this->prepiking,
            'afecta_paquete' => $this->afecta_paquete,
            'afecta_mano_obra' => $this->afecta_mano_obra,
            'preventivo' => $this->preventivo,
            'presupuesto' => $this->presupuesto,
            'detalles' => $this->detalles,
            'costo_mano_obra' => $this->costo_mano_obra / 1.16,
            'costo_promedio' => $this->producto->costo_promedio,
            'core' => $this->core,
            'venta_servicio_id' => $this->venta_servicio_id,
            'created_at' => $this->created_at,
        ];
    }
}
