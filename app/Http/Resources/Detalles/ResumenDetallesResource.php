<?php

namespace App\Http\Resources\Detalles;

use Illuminate\Http\Resources\Json\JsonResource;

class ResumenDetallesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'aaData' => array_merge(
                ResumenServicios::collection($this->resource['servicios'])->resolve(),
                ResumenProductos::collection($this->resource['productos'])->resolve(), 
            )
        ];
    }
}
