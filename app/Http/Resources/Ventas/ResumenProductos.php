<?php

namespace App\Http\Resources\Ventas;

use Illuminate\Http\Resources\Json\JsonResource;

class ResumenProductos extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'no_indentificacion' => $this->producto->no_identificacion,
            'descripcion' => $this->producto->descripcion,
            'ubicacion' => $this->ubicacion->nombre ?? 'GEN.',
            'cantidad' => $this->cantidad,
            'cantidad_entregada' => $this->cantidad_entregada,
            'precio_unitario' => $this->valor_unitario,
            'costo_mo' => $this->costo_mano_obra,
            'total_sin_iva' => $this->venta_total,
            'iva' => $this->venta_total * 0.16,
            'total' => $this->venta_total * 1.16,
            'servicio' => false,
            'prepiking' => $this->prepiking,
            'afecta_paquete' => $this->afecta_paquete,
            'afecta_mano_obra' => $this->afecta_mano_obra,
            'preventivo' => $this->preventivo,
            'presupuesto' => $this->presupuesto,
            'detalles' => $this->detalles,
            'costo_mano_obra' => $this->costo_mano_obra / 1.16,
            'core' => $this->core,
            'venta_servicio_id' => $this->venta_servicio_id,
        ];
    }
}
