<?php

namespace App\Http\Resources\Ventas;

use Illuminate\Http\Resources\Json\JsonResource;

class ResumenResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(
            ResumenProductos::collection($this->resource['productos'])->resolve(), 
            ResumenServicios::collection($this->resource['servicios'])->resolve(),
        );
    }
}
