<?php

namespace App\Http\Resources\Ventas;

use Illuminate\Http\Resources\Json\JsonResource;

class ResumenServicios extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'no_indentificacion' => $this->no_identificacion,
            'descripcion' => $this->nombre_servicio,
            'cantidad' => $this->cantidad,
            'precio_unitario' => 0,
            'costo_mo' => $this->costo_mo,
            'costo_mano_obra' => $this->costo_mo / 1.16,
            'total_sin_iva' => $this->costo_mo - $this->iva,
            'iva' => $this->iva,
            'total' => $this->costo_mo,
            'servicio' => true,
            'prepiking' => true,
            'afecta_mano_obra' => false,
            'preventivo' => false,
            'presupuesto' => false,
            'detalles' => false,
            'refacciones' => $this->refacciones,
        ];
    }
}
