<?php

namespace App\Http\Resources\Ventas;

use App\Models\Refacciones\VentaServicioModel;
use Illuminate\Http\Resources\Json\JsonResource;

class RefaccionesOperacionesLista extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if($this->resource instanceof VentaServicioModel)
            return [
                'id' => $this->id,
                'venta_id' => $this->venta_id,
                'no_identificacion' => $this->no_identificacion,
                'descripcion_producto' => $this->nombre_servicio,
                'valor_unitario' => $this->precio,
                'cantidad' => $this->cantidad,
                'costo_mo' => $this->costo_mo / 1.16,
                'iva' => $this->iva,
                'refacciones' => $this->refacciones,
                'precio' => $this->precio / 1.16,
                'estatus_ventas_id' => $this->estatus_ventas_id,
                'venta_total' => $this->venta_total,
                'servicio' => true,
                'preventivo' => $this->preventivo,
                'presupuesto' => $this->presupuesto,
                'bodyshop' => $this->bodyshop,
            ];

        return [
            'id' => $this->id,
            'producto_id' => $this->producto_id,
            'precio_id' => $this->precio_id,
            'cliente_id' => $this->cliente_id,
            'folio_id' => $this->folio_id,
            'cantidad' => $this->cantidad,
            'venta_total' => ($this->valor_unitario * $this->cantidad) + ($this->costo_mano_obra / 1.16),
            'valor_unitario' => $this->valor_unitario,
            'venta_id' => $this->venta_id,
            'orden_original' => $this->orden_original,
            'estatus_producto_' => $this->estatus_producto_orden,
            'descontado_almacen' => $this->descontado_almacen,
            'id_index_requisicion' => $this->id_index_requisicion,
            'precio_manual' => $this->precio_manual,
            'almacen_id' => $this->almacen_id,
            'prepiking' => $this->prepiking,
            'afecta_mano_obra' => $this->afecta_mano_obra,
            'tipo_descuento_id' =>  $this->tipo_descuento_id,
            'preventivo' =>  $this->preventivo,
            'costo_adicional' => $this->costo_adicional,
            'cantidad_entregada' => $this->cantidad_entregada,
            'presupuesto' =>  $this->presupuesto,
            'detalles' => $this->detalles,
            'bodyshop' => $this->bodyshop,
            'costo_mano_obra' =>  $this->costo_mano_obra / 1.16,
            'afecta_paquete' => $this->afecta_paquete,
            'core' =>  $this->core,

            'no_identificacion' => $this->producto->no_identificacion,
            'costo_promedio' => $this->producto->costo_promedio,
            'descripcion_producto' => $this->producto->descripcion,
            'unidad' => 'PIEZA',
            'precio_publico' => $this->precio_publico,
            'estatus_venta' => $this->estatus_venta,
            'ubicacion_producto' => $this->producto->ubicacion->nombre?? 'GEN.',
            'estatus_ventas_id' => $this->estatus_ventas_id,
            'stock' => $this->stock,
            'reemplazo_no_identificacion' => $this->producto->reemplazo->no_identificacion?? '',
            'reemplazo_producto_id' => $this->producto->reemplazo->id?? null,
        ];
    }
}
