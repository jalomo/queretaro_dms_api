<?php

namespace App\Http\Resources\Orden;

use Illuminate\Http\Resources\Json\JsonResource;

class RefaccionesFacturadasResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'numero_orden' => $this->numero_orden, 
            'basico' => $this->basico, 
            'sufijo' => $this->sufijo, 
            'gpo' => $this->gpo, 
            'prefijo' => $this->prefijo, 
            'cantidad' => $this->cantidad, 
            'precio_venta' => $this->venta_total, 
            'costo_refaccion' => $this->valor_unitario, 
            'descripcion' => $this->descripcion
        ];
    }
}
