<?php

namespace App\Http\Requests\OrdenCompra;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\Response;
use App\Servicios\Core\MensajesConstantes;

class UploadCsvRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'proveedor_id' => 'required|numeric',
			'fecha_pago' => 'required|date',
			'fecha' => 'required|date',
			'observaciones' => 'nullable',
			'factura_id' => 'nullable',
			'factura' => 'nullable',
            'archivo_csv' => 'required|mimetypes:text/plain',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json(
            ['message' => 'Los datos son invalidos'], Response::HTTP_BAD_REQUEST, 
            ['X-Message' => json_encode($validator->errors()->messages())]
        ));
    }

    protected function prepareForValidation()
    {
        $all = $this->all();
        $rules = array_keys($this->rules());

        foreach ($all as $key => $value) {
            if (! in_array($key, $rules)) {
                throw new HttpResponseException(response()->json(
                    [ $key => str_replace(':parametro', $key, MensajesConstantes::$E0031_ERROR_UNEXPECTED_PARAMETER)], Response::HTTP_BAD_REQUEST
                ));
            }
        }
    }
}
