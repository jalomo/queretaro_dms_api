<?php

namespace App\Http\Requests\Pedido;

use App\Http\Requests\CustomValidation;
use Illuminate\Foundation\Http\FormRequest;

class StoreCargamentoRequest extends FormRequest
{
    use CustomValidation;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'productos' => 'required',
            'productos.*.id' => 'nullable|required_without_all:productos.*.grupo,productos.*.basico,productos.*.prefijo,productos.*.sufijo',
            'productos.*.grupo' => 'nullable',
            'productos.*.basico' => 'nullable|required_without:productos.*.prefijo',
            'productos.*.prefijo' => 'nullable|required_without:productos.*.basico',
            'productos.*.sufijo' => 'nullable',
            'productos.*.cantidad' => 'required|min:1',
            'productos.*.valor_unitario' => 'required|min:1',
            'productos.*.factura' => 'required|string',
            'productos.*.proveedor' => 'required|string',
            'productos.*.descripcion' => 'required|string',
            'productos.*.clave_sat' => 'nullable|string',
            'productos.*.unidad_sat' => 'nullable|string',
            'ford' => 'required|in:FM,MC,NF,VA,LL',
        ];
    }
}
