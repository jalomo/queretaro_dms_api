<?php

namespace App\Http\Requests\CuentasPorCobrar;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\Response;

class PagoFacturaActualizarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cuenta_por_cobrar_id' => 'required|exists:pagos_factura,cuenta_por_cobrar_id',
            'tipo_pago_id' => 'required',
            'cfdi_id' => 'required',
            'importe' => 'required',
        ];
    }
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json(
            ['message' => 'Los datos son invalidos'], Response::HTTP_BAD_REQUEST, 
            ['X-Message' => json_encode($validator->errors()->messages())]
        ));
    }
}
