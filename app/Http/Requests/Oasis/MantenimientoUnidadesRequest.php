<?php

namespace App\Http\Requests\Oasis;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\Response;

class MantenimientoUnidadesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fecha_mantenimiento' => 'required',
            'temperatura_normal' => 'required',
            'estereo_apagado' => 'required',
            'aire_apagado' => 'required',
            'cambios_transmision' => 'required',
            'rotacion_llantas' => 'required',
            'estado_elevadores' => 'required',
            'fugas_fluidos' => 'required',
            'protectores_asientos' => 'required',
            'presion_llanta_dd' => 'required',
            'presion_llanta_di' => 'required',
            'presion_llanta_ti' => 'required',
            'presion_llanta_td' => 'required',
            'caducidad_wrap' => 'required',
            'lavado_ford' => 'required',
            'estado_pintura' => 'required',
            'cera_ford' => 'required',
            'descontaminacion_pintura' => 'required',
            'gomas_limpiaparabrisas' => 'required',
            'nivel_gasolina' => 'required',
            'parte_baja_vieculo' => 'required',
            'nivel_anticongelante' => 'required',
            'nivel_aceite_motor' => 'required',
            'nivel_liquido_frenos' => 'required',
            'nivel_aceite_transmicion' => 'required',
            'nivel_chisgueteros' => 'required',
            'nivel_aceite_dieccion' => 'required',
            'luces_delanteras' => 'required',
            'direccionales' => 'required',
            'luces_traceras' => 'required',
            'luz_toldo' => 'required',
            'luz_emergencia' => 'required',
            'luz_vanidad' => 'required',
            'luz_frenos' => 'required',
            'luz_reversa' => 'required',
            'luz_placa' => 'required',
            'luz_guantera' => 'required',
        ];
    }
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json(
            ['message' => 'Los datos son invalidos'], Response::HTTP_BAD_REQUEST, 
            ['X-Message' => json_encode($validator->errors()->messages())]
        ));
    }
}
