<?php

namespace App\Http\Requests\Productos;

use App\Http\Requests\CustomValidation;
use Illuminate\Foundation\Http\FormRequest;

class UpdateProductoRequest extends FormRequest
{
    use CustomValidation;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'descripcion' => 'required|string',
            'unidad' => 'required|string',
            'gpo' => 'required|string',
            'prefijo' => 'nullable|string',
            'sufijo' => 'nullable|string',
            'basico' => 'required|string',
            'valor_unitario' => 'required|numeric|min:0',
            'costo_promedio' => 'required|numeric|min:0',
            'inventariable' => 'required|integer',
            'existencia' => 'integer',
            'taller_id' => 'required|exists:taller,id',
            'almacen_id' => 'required|exists:almacen,id',
            'precio_id' => 'nullable|exists:precio,id',
            'precio_factura' => 'required|integer',
            'activo' => 'integer',
            'cantidad' => 'required|integer|min:0',
            'clave_prod_serv' => '',
            'clave_unidad' => '',
            'ubicacion_producto_id' => 'required|exists:catalogo_ubicaciones_producto,id',
            'no_identificacion' => "required|unique:producto,no_identificacion,{$this->producto}"
        ];
    }

    public function validationData()
    {
        return $this->all() + ['no_identificacion' => $this->gpo . $this->prefijo . $this->basico . $this->sufijo];
    }
}
