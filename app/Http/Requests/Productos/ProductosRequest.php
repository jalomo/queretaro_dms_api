<?php

namespace App\Http\Requests\Productos;

use Illuminate\Foundation\Http\FormRequest;

class ProductosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'no_identificacion' => 'nullable|string',
            'descripcion' => 'nullable|string',
            'producto_id' => 'nullable|integer',
            'cantidad_actual' => 'nullable|numeric',
            'venta_id' => 'sometimes|required|integer',
            'traspaso_id' => 'sometimes|required|integer',
        ];
    }
}
