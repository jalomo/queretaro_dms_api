<?php

namespace App\Http\Requests\Traspasos;

use Illuminate\Foundation\Http\FormRequest;

class TraspasoUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'almacen_origen_id' => 'required|exists:almacen,id',
            'almacen_destino_id' => 'required|exists:almacen,id',
            'folio' => 'required|string',
            'venta_id' => 'required|exists:ventas,id' 
        ];
    }
}
