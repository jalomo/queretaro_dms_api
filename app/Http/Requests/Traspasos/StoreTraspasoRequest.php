<?php

namespace App\Http\Requests\Traspasos;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\Response;

class StoreTraspasoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'productos' => 'required|array',
            'productos.*.id' => 'required|exists:producto,id',
            'productos.*cantidad' => 'required|integer|min:1',
            'venta_id' => 'sometimes|exists:ventas,id'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json(
            ['message' => 'Los datos son invalidos'], Response::HTTP_BAD_REQUEST, 
            ['X-Message' => json_encode($validator->errors()->messages())]
        ));
    }
}
