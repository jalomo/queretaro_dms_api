<?php

namespace App\Http\Requests\Traspasos;

use Illuminate\Foundation\Http\FormRequest;

class SolicitudTraspasoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'folio' => 'required|string',
            'firma' => 'required',
            'productos' => 'required|array',
            'productos.*.no_identificacion' => 'required|string',
            'productos.*.grupo' => 'required|string',
            'productos.*.prefijo' => 'nullable|string',
            'productos.*.basico' => 'required|string',
            'productos.*.sufijo' => 'nullable|string',
            'productos.*.cantidad' => 'required|integer|min:1',
        ];
    }
}
