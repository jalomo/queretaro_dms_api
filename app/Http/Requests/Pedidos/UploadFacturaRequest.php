<?php

namespace App\Http\Requests\Pedidos;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\CustomValidation;

class UploadFacturaRequest extends FormRequest
{
    use CustomValidation;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'factura' => 'required|mimes:pdf,xml',
        ];
    }
}
