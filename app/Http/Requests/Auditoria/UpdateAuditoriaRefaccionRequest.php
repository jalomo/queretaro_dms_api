<?php

namespace App\Http\Requests\Auditoria;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAuditoriaRefaccionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'producto_id' => 'required|integer|exists:producto,id',
            'existencia_sistema' => 'required|numeric',
            'existencia_fisico' => 'required|numeric',
            'ubicacion' => 'required|string|max:10',
        ];
    }
}
