<?php

namespace App\Http\Requests\Refacciones;

use Illuminate\Foundation\Http\FormRequest;

class VentaUpdatePresupuestoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'precio_id' => 'required|exists:precio,id',
            'almacen_id' => 'required|exists:almacen,id',
            'producto_id' => 'required|exists:producto,id',
            'no_identificacion' => 'required|string',
            'cantidad' => 'required|numeric|min:1',
            'valor_unitario' => 'required|numeric',
            'es_total' => 'nullable|boolean',
            'afecta_paquete' => 'nullable|boolean',
            'afecta_mano_obra' => 'nullable|boolean',
            'presupuesto' => 'nullable|boolean',
            'usuario' => 'required|string',
            'password' => 'required|string',
            'nuevo' => 'required|boolean',
        ];
    }
}
