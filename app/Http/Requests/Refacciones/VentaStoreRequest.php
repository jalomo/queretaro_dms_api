<?php

namespace App\Http\Requests\Refacciones;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Refacciones\VentasRealizadasModel;
use App\Models\Refacciones\VentaProductoModel;

class VentaStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            VentasRealizadasModel::CLIENTE_ID => 'required|exists:clientes,id',
            VentasRealizadasModel::VENTA_TOTAL => 'required|numeric',
            VentasRealizadasModel::SUBTOTAL => 'nullable|numeric',
            VentasRealizadasModel::IVA => 'nullable|numeric',
            VentasRealizadasModel::TIPO_VENTA => 'required|numeric',
            VentasRealizadasModel::REL_PRODUCTO_ID => 'required|exists:producto,id',
            VentasRealizadasModel::PRECIO_ID => 'required|exists:precio,id',
            VentaProductoModel::CANTIDAD => 'required|numeric',
            VentasRealizadasModel::VENDEDOR_ID => 'required|numeric',
            VentaProductoModel::VALOR_UNITARIO => 'required|numeric',
            VentasRealizadasModel::ALMACEN_ID => 'required|numeric',
            VentasRealizadasModel::TIPO_PRECIO_ID => 'required|numeric|exists:cat_tipo_precio,id'
        ];
    }
}
