<?php

namespace App\Http\Requests\Refacciones;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\UploadFileStockRule;

class StockRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $max_size = (int) ini_get('upload_max_filesize') * 1024;

        return [
            'last' => 'required|boolean',
            'archivo' => 'required|file|max:'. $max_size,
            'uuid' => ['nullable', new UploadFileStockRule]
        ];
    }
}
