<?php

namespace App\Http\Requests\Estadisticas;

use Illuminate\Foundation\Http\FormRequest;

class DevolucionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'fecha_inicio' => 'nullable|date',
            'fecha_termino' => 'nullable|date|after_or_equal:fecha_inicio',
            'producto' => 'nullable|string',
        ];
    }
}
