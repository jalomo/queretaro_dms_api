<?php

namespace App\Http\Requests\Ventas;

use Illuminate\Foundation\Http\FormRequest;

class FinalizarVenta extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'plazo_credito_id' => 'nullable|integer|exists:plazos_credito,id',
            'cliente_id' => 'required|exists:clientes,id',
            'tipo_forma_pago_id' => 'required|integer|exists:cat_tipo_precio,id',
            'tipo_pago_id' => 'required|numeric|exists:cat_tipo_pago,id',
            'estatus_cuenta_id' => 'nullable|integer|exists:estatus_cuenta,id',
            'enganche' => 'required|numeric',
            'numero_orden' => 'nullable|string',
            'concepto' => 'required|string',
            'tasa_interes' => 'integer',
            'fecha' => 'nullable|date',
        ];
    }
}
