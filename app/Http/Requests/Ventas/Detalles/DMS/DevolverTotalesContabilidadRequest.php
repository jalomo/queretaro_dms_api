<?php

namespace App\Http\Requests\Ventas\Detalles\DMS;

use Illuminate\Foundation\Http\FormRequest;

class DevolverTotalesContabilidadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'no_identificacion' => 'required|string|exists:producto,no_identificacion',
            'numero_orden' => 'required|string|exists:ventas,numero_orden',
        ];
    }
}
