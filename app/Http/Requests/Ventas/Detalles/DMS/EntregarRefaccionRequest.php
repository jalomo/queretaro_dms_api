<?php

namespace App\Http\Requests\Ventas\Detalles\DMS;

use Illuminate\Foundation\Http\FormRequest;

class EntregarRefaccionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cantidad_entregada' => 'required|integer|min:1',
            'cliente_id' => 'required',
            'cliente_nombre' => 'required',
            'cliente_apellido_paterno' => 'required',
            'cliente_apellido_materno' => 'nullable',
            'fecha' => 'required|date',
        ];
    }
}
