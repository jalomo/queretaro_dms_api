<?php

namespace App\Http\Requests\Ventas\Detalles\DMS;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'producto_id' => 'required|exists:producto,id',
            'folio_id' => 'integer|exists:folios,id',
            'venta_id' => 'required|exists:ventas,id',
            'valor_unitario' => 'required',
            'precio_manual' => 'required|min:0|max:100',
            'cantidad' => 'required|numeric',
            'afecta_paquete' => 'nullable|boolean',
            'afecta_mano_obra' => 'nullable|boolean',
            'core' => 'required|numeric',
            'venta_servicio_id' => 'nullable|exists:venta_servicio,id',
        ];
    }
}
