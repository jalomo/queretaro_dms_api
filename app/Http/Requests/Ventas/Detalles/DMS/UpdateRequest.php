<?php

namespace App\Http\Requests\Ventas\Detalles\DMS;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'producto_id' => 'required|exists:producto,id',
            'valor_unitario' => 'required',
            'precio_manual' => 'required|min:0|max:100',
            'cantidad' => 'required|numeric',
            // 'afecta_paquete' => 'nullable|boolean',
            // 'afecta_mano_obra' => 'nullable|boolean',
            'core' => 'required|numeric',
        ];
    }
}
