<?php

namespace App\Http\Requests\Ventas\Detalles;

use Illuminate\Foundation\Http\FormRequest;

class StoreVentaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'numero_orden' => 'required|integer',
            'cliente_id' => 'required|exists:clientes,id',
            'venta_total' => 'required|numeric',
            'tipo_venta_id' => 'required|integer',
            'almacen_id' => 'required|exists:almacen,id',
            'tipo_precio_id' => 'required|exists:cat_tipo_precio,id',
            'precio_id' => 'required|exists:precio,id',
            'tipo_orden' => 'sometimes|string',
        ];
    }
}
