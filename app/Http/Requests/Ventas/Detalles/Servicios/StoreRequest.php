<?php

namespace App\Http\Requests\Ventas\Detalles\Servicios;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'venta_id' => 'integer|exists:ventas,id',
            'nombre_servicio' => 'required|string',
            'no_identificacion' => 'required|string',
            'cantidad' => 'required|integer|min:1',
            'precio' => 'required|numeric|min:0',
            'costo_mo' => 'required|numeric|min:0',
            'refacciones' => 'required|numeric|min:0',
            'preventivo' => 'nullable|boolean',
            'presupuesto' => 'nullable|boolean',
            'bodyshop' => 'nullable|boolean',
        ];
    }
}
