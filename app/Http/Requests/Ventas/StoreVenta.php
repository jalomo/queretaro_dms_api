<?php

namespace App\Http\Requests\Ventas;

use Illuminate\Foundation\Http\FormRequest;

class StoreVenta extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //

            'almacen_id' => 'required|integer|exists:almacen,id',
            'cantidad' => 'required|integer',
            'cliente_id' => 'required|exists:clientes,id',
            'precio_id' => 'required|integer|exists:cat_tipo_precio,id',
            'producto_id' => 'required|integer|exists:producto,id',
            'tipo_precio_id' => 'required|integer|exists:cat_tipo_precio,id',
            'tipo_venta_id' => 'required|integer',
            'valor_unitario' => 'required|numeric',
            'vendedor_id' => 'required|integer|exists:usuarios,id',
        ];
    }
}
