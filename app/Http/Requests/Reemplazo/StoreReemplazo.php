<?php

namespace App\Http\Requests\Reemplazo;

use App\Http\Requests\CustomValidation;
use Illuminate\Foundation\Http\FormRequest;

class StoreReemplazo extends FormRequest
{
    use CustomValidation;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'producto' => 'required|exists:producto,no_identificacion',
            'reemplazo' => 'required|exists:producto,no_identificacion',
        ];
    }
}
