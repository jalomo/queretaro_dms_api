<?php

namespace App\Events;

use App\Models\Refacciones\VentaProductoModel;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UpdateRefaccionDetalleEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $refaccion;
    
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(VentaProductoModel $venta_producto)
    {
        $this->refaccion = $venta_producto;
    }
}
