<?php

namespace App\Events\Refacciones;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class IncrementStockEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $compras;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($compras)
    {
        //
        $this->compras = $compras;
    }
}
