<?php

namespace App\Events\Detalles;

use App\Models\Refacciones\VentaServicioModel;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class DeleteServicioDetalleEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $servicio;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(VentaServicioModel $servicio)
    {
        $this->servicio = $servicio;
    }
}
