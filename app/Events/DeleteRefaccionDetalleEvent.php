<?php

namespace App\Events;

use App\Models\Refacciones\VentaProductoModel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class DeleteRefaccionDetalleEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $refaccion;
    
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(VentaProductoModel $venta_producto)
    {
        //
        $this->refaccion = $venta_producto;
    }
}
