<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Http;
use App\Servicios\Asientos\Devoluciones\Ventas\Detalles\Abono as AbonoDevolucion;
use App\Servicios\Asientos\Devoluciones\Ventas\Detalles\Cargo as CargoDevolucion;

class DevolucionRefaccionPreventivoListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        // Si se encuentran entregadas todas las refacciones realizamos la devolucion
        if($event->refaccion->cantidad_entregada == $event->refaccion->cantidad)
        {
            // Enviamos a contabilidad el abono y cargo de la devolucion
            // Sin el IVA
            $abono = new AbonoDevolucion($event->refaccion->venta, [
                'cantidad' => $event->refaccion->cantidad,
                'num_pieza' => $event->refaccion->producto->no_identificacion,
            ]);
            $abono->make($event->refaccion->costo_promedio * $event->refaccion->cantidad);

            $cargo = new CargoDevolucion($event->refaccion->venta, [
                'cantidad' => $event->refaccion->cantidad,
                'num_pieza' => $event->refaccion->producto->no_identificacion,
            ]);
            $cargo->make($event->refaccion->costo_promedio * $event->refaccion->cantidad);

            // Disminuimos el total de las refacciones entregadas
            $event->refaccion->cantidad_entregada -= $event->refaccion->cantidad;
            $event->refaccion->save();

            logger()->info("EVENTO DEVOLUCION REFACCION CONTABILIDAD {$event->refaccion->venta->numero_orden}, SALIDA: {$event->refaccion->cantidad}", (array) $event->refaccion);
        }

        // Si hay parte de las refacciones entregadas realizamos la devolucion
        if($event->refaccion->cantidad_entregada > 0 && $event->refaccion->cantidad_entregada < $event->refaccion->cantidad)
        {
            // Enviamos a contabilidad el abono y cargo de la devolucion
            // Sin el IVA
            $abono = new AbonoDevolucion($event->refaccion->venta, [
                'cantidad' => $event->refaccion->cantidad,
                'num_pieza' => $event->refaccion->producto->no_identificacion,
            ]);
            $abono->make($event->refaccion->costo_promedio * $event->refaccion->cantidad_entregada);

            $cargo = new CargoDevolucion($event->refaccion->venta, [
                'cantidad' => $event->refaccion->cantidad,
                'num_pieza' => $event->refaccion->producto->no_identificacion,
            ]);
            $cargo->make($event->refaccion->costo_promedio * $event->refaccion->cantidad_entregada);

            // Disminuimos la cantidad de refacciones entregadas
            $event->refaccion->cantidad_entregada -= $event->refaccion->cantidad_entregada;
            $event->refaccion->save();

            logger()->info("EVENTO DEVOLUCION REFACCION CONTABILIDAD {$event->refaccion->venta->numero_orden}, SALIDA: {$event->refaccion->cantidad_entregada}", (array) $event->refaccion);
        }
    }
}
