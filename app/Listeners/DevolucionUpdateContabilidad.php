<?php

namespace App\Listeners;

use App\Models\Refacciones\InventarioProductos;
use App\Models\Refacciones\TipoMovimiento;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Servicios\Asientos\Devoluciones\Ventas\Detalles\Abono as AbonoDevolucion;
use App\Servicios\Asientos\Devoluciones\Ventas\Detalles\Cargo as CargoDevolucion;

class DevolucionUpdateContabilidad
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $cantidad_venta = InventarioProductos::where('model_id', $event->refaccion->id)->where('tipo_movimiento_id', TipoMovimiento::VENTA_DETALLES)->latest()->sum('cantidad');
        $cantidad_devolucion = InventarioProductos::where('model_id', $event->refaccion->id)->where('tipo_movimiento_id', TipoMovimiento::DEVOLUCION_VENTA_DETALLES)->latest()->sum('cantidad');
        $cantidad_inventario = $cantidad_venta - $cantidad_devolucion;
        $cantidad = 0;

        // Si tenemos entregadas mas piezas al disminuir por actualizacion
        // entonces devolvemos la diferencia y disminuimos la cantidad de refacciones 
        // entregadas
        // 3 < 4
        if($event->refaccion->cantidad_entregada >  0 && $cantidad_inventario < $event->refaccion->cantidad_entregada)
        {
            $cantidad = $event->refaccion->cantidad_entregada - $cantidad_inventario;

            // Enviamos a contabilidad el abono y cargo de la devolucion
            // Sin el IVA
            $abono = new AbonoDevolucion($event->refaccion->venta, [
                'cantidad' => $event->refaccion->cantidad,
                'num_pieza' => "{$event->refaccion->producto->no_identificacion} - {$event->refaccion->producto->descripcion}",
                'costo_promedio' => $event->refaccion->costo_promedio * $cantidad,
            ]);
            $abono->make($event->refaccion->costo_promedio * $cantidad);

            $cargo = new CargoDevolucion($event->refaccion->venta, [
                'cantidad' => $event->refaccion->cantidad,
                'num_pieza' => "{$event->refaccion->producto->no_identificacion} - {$event->refaccion->producto->descripcion}",
                'costo_promedio' => $event->refaccion->costo_promedio * $cantidad,
            ]);
            $cargo->make($event->refaccion->costo_promedio * $cantidad);

            // Disminuimos la cantidad de refacciones entregadas
            $event->refaccion->cantidad_entregada -= $cantidad;
            $event->refaccion->save();

            logger()->info("EVENTO ACTUALIZACION DEVOLUCION CONTABILIDAD {$event->refaccion->venta->numero_orden}, SALIDA: $cantidad", (array) $event->refaccion);
        }
    }
}
