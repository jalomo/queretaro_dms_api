<?php

namespace App\Listeners;

use App\Events\DeleteRefaccionDetalleEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Http;
use App\Servicios\Asientos\Devoluciones\Ventas\Detalles\Abono as AbonoDevolucion;
use App\Servicios\Asientos\Devoluciones\Ventas\Detalles\Cargo as CargoDevolucion;

class DevolucionRefaccionListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(DeleteRefaccionDetalleEvent $event)
    {
        //
        $response = Http::get(env('URL_SERVICIOS'). 'citas/citas_queretaro/enlaces/dms/verifica_firma_tecnico', [
            'id_cita' => $event->refaccion->venta->numero_orden,
        ]);

        $firma = $response->object();

        logger()->info("EJECUTO EVENTO DEVOLUCION CONTABILIDAD REFACCION PRESUPUESTOS {$event->refaccion->venta->numero_orden}", (array) $firma);

        // Si la llamada a la api es exitosa y
        // se encuentra firmada realizamos la devolucion
        if($response->successful() && (optional($firma->data)->firma_presupuesto === true || optional($firma->data)->firma_presupuesto_garantias === true))
        {
            // Enviamos a contabilidad el abono y cargo de la devolucion
            // Sin el IVA
            $abono = new AbonoDevolucion($event->refaccion->venta);
            $abono->make($event->refaccion->valor_unitario * $event->refaccion->cantidad);

            $cargo = new CargoDevolucion($event->refaccion->venta);
            $cargo->make($event->refaccion->valor_unitario * $event->refaccion->cantidad);
        }
    }
}
