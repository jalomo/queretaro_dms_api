<?php

namespace App\Listeners\Detalles;

use App\Events\StoreRefaccionDetalleEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Http;

class StorePreventivoListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(StoreRefaccionDetalleEvent $event)
    {
        // Ejecutamos el evento si la refaccion va con afectar paquete o mano de obra
        if($event->refaccion->afecta_paquete || $event->refaccion->afecta_mano_obra)
        {
            $response = Http::withBody(
                json_encode([[
                    'codigo' => $event->refaccion->producto->no_identificacion,
                    'descripcion' => $event->refaccion->producto->descripcion,
                    'precio' => $event->refaccion->valor_unitario,
                    'cantidad' => $event->refaccion->cantidad,
                    'tiempo' => 0,
                    'afecta_paquete' => $event->refaccion->afecta_paquete,
                    'afecta_mano_obra' => $event->refaccion->afecta_mano_obra,
                    'presupuesto' => $event->refaccion->presupuesto,
                ]]),
                'json'
            )
            ->post(
                env('URL_SERVICIOS') . "api/addPiezasFromDMS/{$event->refaccion->venta->numero_orden}"
            );
    
            if ($response->successful() && optional($response->object())->exito == true) {
                
                $event->refaccion->id_index_requisicion = collect($response->object()->dataUpdated)->firstWhere('codigo', $codigo)->id;
                $event->refaccion->save();
            }

            info("EVENTO AGREGAR PREVENTIVO {$event->refaccion->venta->numero_orden}", (array) $response->object());
        }
    }
}