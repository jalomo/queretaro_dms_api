<?php

namespace App\Listeners\Detalles;

use App\Events\DeleteRefaccionDetalleEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Http;

class DeletePreventivoListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(DeleteRefaccionDetalleEvent $event)
    {
        // Ejecutamos el evento si la refaccion va con afectar paquete o mano de obra
        if($event->refaccion->afecta_paquete || $event->refaccion->afecta_mano_obra)
        {
            // Eliminamos de preventivo
            $response = Http::withBody(
                json_encode([
                    'id' => $id,
                ]),
                'json'
            )
            ->post(
                env('URL_SERVICIOS') . "api/deletePiezasFromDMS/{$venta->numero_orden}"
            );

            info("EVENTO ELIMINAR PREVENTIVO {$event->refaccion->venta->numero_orden}", (array) $response->object());
        }
    }
}