<?php

namespace App\Listeners\Detalles;

use App\Events\UpdateRefaccionDetalleEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Http;

class UpdatePreventivoListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UpdateRefaccionDetalleEvent $event)
    {
        // Ejecutamos el evento si la refaccion va con afectar paquete o mano de obra
        if($event->refaccion->afecta_paquete || $event->refaccion->afecta_mano_obra)
        {
            $response = Http::withBody(
                json_encode([[
                    'id' => $event->refaccion->id_index_requisicion,
                    'codigo' => $event->refaccion->producto->no_identificacion,
                    'descripcion' => $event->refaccion->producto->descripcion,
                    'precio' => $event->refaccion->valor_unitario,
                    'cantidad' => $event->refaccion->cantidad,
                    'tiempo' => 0,
                    'afecta_paquete' => $event->refaccion->afecta_paquete,
                    'afecta_mano_obra' => $event->refaccion->afecta_mano_obra,
                    'presupuesto' => $event->refaccion->presupuesto,
                ]]),
                'json'
            )
            ->post(
                env('URL_SERVICIOS') . "api/updatePaqueteFromDMS/{$event->refaccion->venta->numero_orden}"
            );

            info("EVENTO ACTUALIZAR PREVENTIVO {$event->refaccion->venta->numero_orden}", (array) $response->object());
        }
    }
}