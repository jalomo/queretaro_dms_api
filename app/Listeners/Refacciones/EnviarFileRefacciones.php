<?php

namespace App\Listeners\Refacciones;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Storage;

class EnviarFileRefacciones implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $venta = $event->refaccion->venta;
        $refacciones = $venta->detalle_venta()->where('estatus_producto_orden', '<>','B')->get();

        // Nombre del archivo
        $filename = "R_{$venta->numero_orden}";

        $total = $refacciones->reduce(function($total, $refaccion){
            return $total += $refaccion->cantidad * $refaccion->valor_unitario;
        }, 0);

        $saved = Storage::disk('ftp')->put('Dealers/Service Excellence/' . $filename . '.txt', $total);

        info("ARCHIVO FTP: R_{$event->refaccion->venta->numero_orden}", [ 'saved' => $saved, 'total' => $total ]);
    }
}
