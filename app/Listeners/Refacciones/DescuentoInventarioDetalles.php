<?php

namespace App\Listeners\Refacciones;

use App\Servicios\Inventario\VenderDetallesService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class DescuentoInventarioDetalles
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        // Devolvemos las piezas al inventario
        $devolucion = new VenderDetallesService;
        $devolucion->id($event->refaccion->id);
        $devolucion->handle($event->refaccion->producto_id, $event->refaccion->cantidad, $event->refaccion->valor_unitario);

        logger()->info("EJECUTO EVENTO DESCUENTO INVENTARIO DETALLES {$event->refaccion->venta->numero_orden}, SALIDA: {$event->refaccion->cantidad}", (array) $event->refaccion);
    }
}
