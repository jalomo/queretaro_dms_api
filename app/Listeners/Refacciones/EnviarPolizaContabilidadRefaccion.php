<?php

namespace App\Listeners\Refacciones;

use App\Servicios\Detalles\ClienteNumeroOrden;
use App\Servicios\Detalles\EnviarAsientoContabilidad;
use DateTime;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class EnviarPolizaContabilidadRefaccion
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        // Recuperamos el cliente de servicios
        $cliente = (new ClienteNumeroOrden)->get($event->refaccion->venta->numero_orden);
        // Convertimos a collection array
        $request = (object) [
            'cliente_id' => $cliente->id_cliente,
            'cliente_nombre' => $cliente->cliente_nombre,
            'cliente_apellido_paterno' => $cliente->cliente_apellido_paterno,
            'cliente_apellido_materno' => $cliente->cliente_apellido_materno,
            'fecha' => (new DateTime)->format('Y-m-d H:i:s'),
            'cantidad_entregada' => $event->refaccion->cantidad,
        ];

        // Se envían los datos necesarios para el envío de los asientos a contabilidad
        $handler = new EnviarAsientoContabilidad($request);

        $handler->handle($event->refaccion);

        logger()->info("EJECUTO EVENTO ASIENTOS CONTABILIDAD AL AGREGAR REFACCION {$event->refaccion->venta->numero_orden}");
    }
}
