<?php

namespace App\Listeners\Refacciones;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\Refacciones\DecrementStockEvent;
use App\Models\Refacciones\StockProductosModel;

class DecrementStockListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(DecrementStockEvent $event)
    {
        // Cuando se crea una nueva venta en VentaProductoModel 
        // Buscamos todos los productos de esta venta
        // descontamos del stock la cantidad vendida
        foreach($event->ventas as $venta)
        {
            $stock = StockProductosModel::where('producto_id', $venta->producto_id)->first();
            $stock->cantidad_almacen_primario -= $venta->cantidad;
            $stock->cantidad_actual -= $venta->cantidad;
            $stock->save();
        }
        
    }
}
