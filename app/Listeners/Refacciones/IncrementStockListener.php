<?php

namespace App\Listeners\Refacciones;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\Refacciones\IncrementStockEvent;
use App\Models\Refacciones\StockProductosModel;

class IncrementStockListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(IncrementStockEvent $event)
    {
        // Cuando se crea una nueva venta en VentaProductoModel 
        // Buscamos todos los productos de esta venta
        // descontamos del stock la cantidad vendida
        foreach($event->compras as $compra)
        {
            $stock = StockProductosModel::find($compra->producto_id);
            $stock->cantidad_almacen_primario += $compra->cantidad;
            $stock->cantidad_actual += $compra->cantidad;
            $stock->save();
        }
    }
}
