<?php

namespace App\Listeners\Refacciones;

use App\Models\Refacciones\InventarioProductos;
use App\Models\Refacciones\TipoMovimiento;
use App\Servicios\Inventario\DevolverVentaDetallesService;
use App\Servicios\Inventario\VenderDetallesService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Inventario;

class DescuentoIncrementoInventarioDetalles
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $cantidad_venta = InventarioProductos::where('model_id', $event->refaccion->id)->where('tipo_movimiento_id', TipoMovimiento::VENTA_DETALLES)->latest()->sum('cantidad');
        $cantidad_devolucion = InventarioProductos::where('model_id', $event->refaccion->id)->where('tipo_movimiento_id', TipoMovimiento::DEVOLUCION_VENTA_DETALLES)->latest()->sum('cantidad');
        $cantidad_inventario = $cantidad_venta - $cantidad_devolucion;
        $cantidad = 0;

        // 3 > 2
        if($cantidad_inventario > $event->refaccion->cantidad)
        {
            $cantidad = $cantidad_inventario - $event->refaccion->cantidad;

            // Devolvemos las piezas al inventario
            $devolucion = new DevolverVentaDetallesService;
            $devolucion->id($event->refaccion->id);
            $devolucion->handle($event->refaccion->producto_id, $cantidad, $event->refaccion->valor_unitario); 

            logger()->info("EVENTO ACTUALIZACION INVENTARIO DETALLES {$event->refaccion->venta->numero_orden}, ENTRADA: $cantidad", (array) $event->refaccion);
        }

        // 3 < 4
        if($cantidad_inventario < $event->refaccion->cantidad)
        {
            $cantidad = $event->refaccion->cantidad - $cantidad_inventario;

            // Descontamos las piezas al inventario
            $devolucion = new VenderDetallesService;
            $devolucion->id($event->refaccion->id);
            $devolucion->handle($event->refaccion->producto_id, $cantidad, $event->refaccion->valor_unitario);

            logger()->info("EVENTO ACTUALIZACION INVENTARIO DETALLES {$event->refaccion->venta->numero_orden}, SALIDA: $cantidad", (array) $event->refaccion);
        }
    }
}
