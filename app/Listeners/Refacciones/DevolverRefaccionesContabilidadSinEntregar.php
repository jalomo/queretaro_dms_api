<?php

namespace App\Listeners\Refacciones;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Servicios\Asientos\Devoluciones\Ventas\Detalles\Abono as AbonoDevolucion;
use App\Servicios\Asientos\Devoluciones\Ventas\Detalles\Cargo as CargoDevolucion;

class DevolverRefaccionesContabilidadSinEntregar
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $costo_promedio_total = $event->refaccion->cantidad * $event->refaccion->costo_promedio;
        // Enviamos a contabilidad el abono y cargo de la devolucion
        // Sin el IVA
        $abono = new AbonoDevolucion($event->refaccion->venta, [
            'cantidad' => $event->refaccion->cantidad,
            'num_pieza' => $event->refaccion->producto->no_identificacion,
            'costo_promedio' => $costo_promedio_total,
            'vendedor' => $event->refaccion->vendedor_id,
            'cantidad' => $event->refaccion->cantidad,
        ]);
        $abono->make($costo_promedio_total);

        $cargo = new CargoDevolucion($event->refaccion->venta, [
            'cantidad' => $event->refaccion->cantidad,
            'num_pieza' => $event->refaccion->producto->no_identificacion,
            'costo_promedio' => $costo_promedio_total,
            'vendedor' => $event->refaccion->vendedor_id,
            'cantidad' => $event->refaccion->cantidad,
        ]);
        $cargo->make($costo_promedio_total);

        // Disminuimos el total de las refacciones entregadas
        $event->refaccion->cantidad_entregada = 0;
        $event->refaccion->save();

        logger()->info("EJECUTO EVENTO DEVOLUCION CONTABILIDAD AL ELIMINAR REFACCION {$event->refaccion->venta->numero_orden}, SALIDA: {$event->refaccion->cantidad}", (array) $event->refaccion);
    }
}