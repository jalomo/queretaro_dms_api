<?php

namespace App\Exceptions;

use Exception;

class ParametroHttpInvalidoException extends Exception
{
    protected $code = 400;
    
    public function __construct(array $errors)
    {
        $this->errors = $errors;
    }
    
    public function report()
    {
        // parent::report($this->errors);
    }

    public function getErrors()
    {
        return $this->errors;
    }
}
