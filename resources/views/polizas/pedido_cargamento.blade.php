<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Entrega Pedido</title>
    <style>
        
    </style>
    <link rel="stylesheet" media="print" href="{{ public_path("css/print.css") }}">
</head>
<body>
    <htmlpageheader name="header">
        <img class="mylsa" src="{{ public_path("images_correo/ford.png") }}" />
        <h1 class="text-center text-header pt-2"> Entrada de Pedido </h1>
    </htmlpageheader>

    <htmlpagefooter name="footer">
        <div class="text-right"><small>Página. {PAGENO} de {nbpg}<small></div>
    </htmlpagefooter>
        
    <div class="text-left">
        <div>
            <b>Fecha de Registro:</b>
            <b>{{ $pedido->created_at->format('d\/m\/Y H:m:s') }}</b>
        </div>
        <div>
            <b>Proveedor: </b> 
            {{ $pedido->ford? 'FORD' : $productos->first()->pedido->proveedor }}
        </div>
        <div>
            <b>Factura: </b> 
            {{ $productos->first()->pedido->factura }}
        </div>

    </div>

    <h3 class="text-center uppercase">Refacciones</h3>

    @php $total = 0 @endphp
    <table class="table table-striped">
        <tr class="text-left">
            <th align="left">No. de parte</th>
            <th align="left">Descripción</th>
            <th align="left">Cantidad</th>
            <th align="right">Valor Unitario</th>
            <th align="right">Importe Total</th>
        </tr>
        <tbody>
            @foreach ($productos as $producto)
            {{ $producto }}
            <tr class="@if($loop->even) even @endif">
                <td>
                    {{ $producto->no_identificacion }}
                </td>
                <td>
                    {{ $producto->descripcion }}
                </td>
                <td>
                    {{ $producto->pedido->cantidad_entregada }}
                </td>
                <td align="right">
                    $ {{ number_format($producto->pedido->precio, 2) }}
                </td>
                <td align="right">
                    $ {{ number_format($producto->pedido->total, 2) }}
                </td>
            </tr>
            @php $total += $producto->pedido->precio * $producto->pedido->cantidad_entregada @endphp
            @endforeach
            <tr class="total">
                <td colspan="3"></td>
                <td align="right">SUBTOTAL</td>
                <td align="right">
                    <b>${{ number_format($total, 2) }}</b>
                </td>
            </tr>
            <tr class="total">
                <td colspan="3"></td>
                <td align="right">IVA</td>
                <td align="right">
                    <b>${{ number_format($total * 0.16, 2) }}</b>
                </td>
            </tr>
            <tr class="total">
                <td colspan="3"></td>
                <td align="right">TOTAL</td>
                <td align="right">
                    <b>${{ number_format($total * 1.16, 2) }}</b>
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>