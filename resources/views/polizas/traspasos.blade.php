<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Poliza del Traspaso</title>
    <style>
        
    </style>
    <link rel="stylesheet" media="print" href="{{ public_path("css/print.css") }}">
</head>
<body>
    <htmlpageheader name="header">
        <img class="mylsa" src="{{ public_path("images_correo/ford.png") }}" />
        <h1 class="text-center text-header pt-2"> Poliza de Traspaso </h1>
    </htmlpageheader>

    <htmlpagefooter name="footer">
        <div class="text-right"><small>Página. {PAGENO} de {nbpg}<small></div>
    </htmlpagefooter>
        
    <div class="text-left">
        <div>
            <b>Folio:</b>
            <b>{{ $traspaso->folio }}</b>
        </div>
        <div class="w-full">
            <div class="w-50">
                <b>Origen: </b>{{ $traspaso->almacen_origen->nombre }}
            </div>
            <div class="w-50">
                <b>Destino:</b> {{ $traspaso->almacen_destino->nombre }}
            </div>
        </div>
    </div>

    <h3 class="text-center uppercase">Refacciones</h3>

    @php $total = 0 @endphp
    <table class="table table-striped">
        <tr class="text-left">
            <th align="left">No. de parte</th>
            <th align="left">Descripción</th>
            <th align="left">Cantidad</th>
            <th align="right">Valor Unitario</th>
        </tr>
        <tbody>
            @foreach ($productos as $producto)
            <tr class="@if($loop->even) even @endif">
                <td>
                    {{ $producto->no_identificacion }}
                </td>
                <td>
                    {{ $producto->producto->descripcion }}
                </td>
                <td>
                    {{ $producto->cantidad }}
                </td>
                <td align="right">
                    $ {{ number_format($producto->valor_unitario, 2) }}
                </td>
            </tr>
            @php $total += $producto->valor_unitario * $producto->cantidad @endphp
            @endforeach
            <tr class="total">
                <td align="right" colspan="4">
                    <b>${{ number_format($total, 2) }}</b>
                </td>
            </tr>
        </tbody>
    </table>

    <table class="table">
        <tr class="">
            @if(env('QUERETARO') == 'TRUE')
            <td class="w-50 text-center bold uppercase" style="width:50%; vertical-align:bottom">
                <img height="3cm" src="{{ is_resource($traspaso->firma_solicitud)? stream_get_contents($traspaso->firma_solicitud) : $traspaso->firma_solicitud }}"/>
                ______________________________________________<br>
                @if($traspaso->origen_almacen_id == $queretaro) 
                    Alfonso Cardoso Rosa Ventura
                    <br>
                    Firma Solicitud de Traspaso
                @else
                    Jorge Martinez Magdalena Dionicio
                    <br>
                    Firma Autorización de Traspaso
                @endif
                
            </td>
            <td class="w-50 text-center bold uppercase" style="width:50%; vertical-align:bottom">
                <img height="3cm" src="{{ is_resource($traspaso->firma_autorizacion)? stream_get_contents($traspaso->firma_autorizacion) : $traspaso->firma_autorizacion}}" />
                ______________________________________________<br>
                @if($traspaso->destino_almacen_id == $san_juan) 
                    Jorge Martinez Magdalena Dionicio    
                    <br>
                    Firma Autorización de Traspaso
                @else
                    Alfonso Cardoso Rosa Ventura
                    <br>
                    Firma Solicitud de Traspaso
                @endif
            </td>
            @else
            <td class="w-50 text-center bold uppercase" style="width:50%; vertical-align:bottom">
                <img height="3cm" src="{{ is_resource($traspaso->firma_autorizacion)? stream_get_contents($traspaso->firma_autorizacion) : $traspaso->firma_autorizacion}}" />
                ______________________________________________<br>
                @if($traspaso->destino_almacen_id == $san_juan) 
                    Jorge Martinez Magdalena Dionicio    
                    <br>
                    Firma Autorización de Traspaso
                @else
                    Alfonso Cardoso Rosa Ventura
                    <br>
                    Firma Solicitud de Traspaso
                @endif
            </td>
            <td class="w-50 text-center bold uppercase" style="width:50%; vertical-align:bottom">
                <img height="3cm" src="{{ is_resource($traspaso->firma_solicitud)? stream_get_contents($traspaso->firma_solicitud) : $traspaso->firma_solicitud }}"/>
                ______________________________________________<br>
                @if($traspaso->origen_almacen_id == $queretaro) 
                    Alfonso Cardoso Rosa Ventura
                    <br>
                    Firma Solicitud de Traspaso
                @else
                    Jorge Martinez Magdalena Dionicio
                    <br>
                    Firma Autorización de Traspaso
                @endif
                
            </td>
            @endif
        </tr>
        <tr class="">
            <td class="w-50 text-center bold uppercase" uppercase style="width:50%; height:4cm; vertical-align:bottom">
                
                <br>
                ______________________________________________<br>
                Nombre y Firma<br> Transportista
            </td>
            <td class="w-50 text-center bold uppercase" style="width:50%; height:5cm; vertical-align:bottom">
                
                <br>
                ______________________________________________<br>
                Nombre y Firma<br> Quién Recibe
            </td>
        </tr>
        <tr class="">
            <td class="w-50 text-center bold uppercase" uppercase style="width:50%; height:4cm; vertical-align:bottom">
                
                <br>
                ______________________________________________<br>
                Nombre y Firma<br> Vigilancia Salida
            </td>
            <td class="w-50 text-center bold uppercase" style="width:50%; height:5cm; vertical-align:bottom">
                
                <br>
                ______________________________________________<br>
                Nombre y Firma<br> Vigilancia Entrada
            </td>
        </tr>
        
    </table>
</body>
</html>