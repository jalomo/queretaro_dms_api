<html lang="es">

<head>
    <meta charset="UTF-8">
    <title>Reporte venta mostrador</title>
    <style>
        @page {
            header: header;
            footer: footer;
            margin-top: 30mm;
            margin-bottom: 15mm;
        }

        body {
            line-height: 1.5em;
            font-family: Arial;
            font-size: 12px;
        }

        .text-center {
            text-align: center
        }

        .text-right {
            text-align: right
        }

        .text-left {
            text-align: left;
        }

        .text-justify {
            text-align: justify;
        }

        .mylsa {
            width: 5cm;
            float: left;
        }

        .text-header {
            float: right;
            margin-top: 0.5cm;
        }

        .uppercase {
            text-transform: uppercase;
        }

        .bold {
            font-weight: bold;
        }

        .page-break {
            page-break-before: always;
        }

        .table {
            width: 100%;
            margin-bottom: 1rem;
            background-color: transparent;
            border-collapse: collapse;
        }

        .table th {
            padding: 0.5rem;
            background: #0E185F;
            color: #ececec;
        }

        .table td,
        th {
            padding: 0.5rem;
        }

        .table tr {
            border: 1px solid #ededed;
        }

        .table-striped tr.even {
            background-color: #ececec;
        }

        .table tr.total {
            border: 0;
        }

        .w-full {
            width: 100%;
        }

        .w-50 {
            width: 50%;
        }

        .pt-2 {
            padding-top: 0.5rem;
        }

        .mt-2 {
            padding-top: 0.5rem;
        }
    </style>
    <!-- <link rel="stylesheet" media="print" href="{{ public_path('css/print.css') }}">  -->
</head>

<body>
    <htmlpageheader name="header">
        <img class="mylsa" src="{{ public_path('images_correo/ford.png') }}" />
        <h1 class="text-center text-header pt-2"> Venta Mostrador </h1>
    </htmlpageheader>

    <htmlpagefooter name="footer">
        <div class="text-right"><small>Página. {PAGENO} de {nbpg}<small></div>
    </htmlpagefooter>

    <div class="text-left">
        <h3 style="margin-top:0px">Filtros aplicados</h4>
            <div>
                <b>Fecha inicio: </b> {{ $fecha_inicio }} <br />
                <b>Fecha fin: </b> {{ $fecha_fin }} <br />
                <b>Vendedor: </b> {{ $vendedor }} <br />
                <b>Fecha elaboración reporte: </b> {{ $fecha_elaboracion }} <br />
            </div>
    </div>
    <br />
    <table class="table table-striped mt-2">
        <tr class="text-left">
            <th align="left">Cantidad</th>
            <th align="left">Clave producto</th>
            <th align="left">Descripción</th>
            <th align="left">Precio Unitario</th>
            <th align="left">Importe</th>
            <th align="left">Costo P</th>
            <th align="left">Costo total</th>
        </tr>
        <tbody>
            @php 
            $subtotal_general = 0; $iva_general = 0; $venta_total_general = 0; $costo_promedio_total_general = 0; $utilidad_general = 0;
            @endphp

            @foreach($ventas as $venta)
            <tr class="even">
                <td>
                    <b>{{ $venta['folio'] }}</b>
                </td>
                <td colspan="3">
                    <b>Cliente:</b> {{ $venta['cliente'] }}
                </td>
                <td colspan="2">
                    <b>Vendedor:</b> {{ $venta['vendedor'] }}
                </td>
                <td>
                    {{ Carbon\Carbon::parse($venta['created_at'])->format('d-m-Y') }}
                </td>
            </tr>
            @php
            $detalle_venta = $venta['detalle_venta'] ?? [];
            $costo_promedio_venta = 0;
            $costo_promedio_total = 0;
            $utilidad = 0;
            @endphp
            @if ($detalle_venta)
            @foreach($detalle_venta as $venta_producto)
            <tr>
                <td>
                    {{ $venta_producto['cantidad'] }}
                </td>
                <td>
                    {{ $venta_producto['producto']['no_identificacion']}}
                </td>
                <td>
                    {{ $venta_producto['producto']['descripcion'] }}
                </td>
                <td>
                    ${{ number_format($venta_producto['valor_unitario'], 2) }}
                </td>
                <td>
                    ${{ number_format($venta_producto['venta_total'], 2) }}
                </td>
                <td>
                    ${{ number_format($venta_producto['producto']['costo_promedio'], 2) }}
                </td>
                <td>
                    @php
                    $costo_promedio_venta = $venta_producto['producto']['costo_promedio'] * $venta_producto['cantidad'];
                    $costo_promedio_total += $costo_promedio_venta;
                    $utilidad = $venta['subtotal'] - $costo_promedio_total;
                    @endphp
                    ${{ number_format($costo_promedio_venta,2) }}
                </td>
            </tr>
            @endforeach
            @endif
            @php

            $subtotal_general = $venta['subtotal'] + $subtotal_general;
            $iva_general = $venta['iva'] + $iva_general;
            $venta_total_general = $venta['venta_total'] + $venta_total_general;
            $costo_promedio_total_general = $costo_promedio_total + $costo_promedio_total_general;
            $utilidad_general = $utilidad + $utilidad_general;
            @endphp
            <tr>
                <td colspan="2">
                    <b>Sub-Total: </b> ${{ number_format($venta['subtotal'], 2) }}
                </td>
                <td>
                    <b>I.V.A:</b> ${{ number_format($venta['iva'], 2) }}
                </td>
                <td>
                    <b>Total:</b> ${{ number_format($venta['venta_total'], 2) }}
                </td>
                <td colspan="2">
                    <b>Costo Total:</b> ${{ number_format($costo_promedio_total, 2) }}
                </td>
                <td>
                    <b>Utilidad:</b> ${{ number_format($utilidad, 2) }}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <table class="table table-striped mt-2">
        <tr class="">
            <th align="left" colspan="5">Totales Generales</th>
        </tr>
        <tr>
            <td>
                <b>Sub-Total: </b> ${{ number_format($subtotal_general, 2) }}
            </td>
            <td>
                <b>I.V.A:</b> ${{ number_format($iva_general, 2) }}
            </td>
            <td>
                <b>Total:</b> ${{ number_format($venta_total_general, 2) }}
            </td>
            <td>
                <b>Costo Total:</b> ${{ number_format($costo_promedio_total_general, 2) }}
            </td>
            <td>
                <b>Utilidad:</b> ${{ number_format($utilidad_general, 2) }}
            </td>
        </tr>
    </table>
</body>

</html>