<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Devolución</title>
    <style>
        
    </style>
    <link rel="stylesheet" media="print" href="{{ public_path("css/print.css") }}">
</head>
<body>
    <htmlpageheader name="header">
        <img class="mylsa" src="{{ public_path("images_correo/ford.png") }}" />
        <h1 class="text-center text-header pt-2"> Devoluciones </h1>
    </htmlpageheader>

    <htmlpagefooter name="footer">
        <div class="text-right"><small>Página. {PAGENO} de {nbpg}<small></div>
    </htmlpagefooter>
        
    <div class="text-left">
        <div>
            <b>Fechas de Reporte:</b>
            <b>{{ $fecha_inicio }}</b> a 
            <b>{{ $fecha_termino }}</b>
        </div>
    </div>

    <h3 class="text-center uppercase">Refacciones</h3>

    @php $total = 0; @endphp
    <table class="table table-striped">
        <tr class="text-left">
            <th align="left">No. de parte</th>
            <th align="left">Descripción</th>
            <th align="left">Cantidad</th>
            <th align="right">Valor Unitario</th>
            <th align="right">IVA</th>
            <th align="right">Importe Total</th>
        </tr>
        <tbody>
            @foreach ($devoluciones as $devolucion)
            <tr>
                <td>
                    {{ $devolucion->producto->no_identificacion }}
                </td>
                <td>
                    {{ $devolucion->producto->descripcion }}
                </td>
                <td>
                    {{ $devolucion->cantidad }}
                </td>
                <td align="right">
                    ${{ number_format($devolucion->valor_unitario, 2) }}
                </td>
                <td align="right">
                    ${{ number_format($devolucion->valor_unitario * $devolucion->cantidad * 0.16, 2) }}
                </td>
                <td align="right">
                    ${{ number_format($devolucion->valor_unitario * $devolucion->cantidad * 1.16, 2) }}
                </td>
            </tr>
            @php $total += $devolucion->valor_unitario * $devolucion->cantidad; @endphp
            @endforeach
            <tr>
                <td colspan="5">
                    <b>TOTAL</b>
                </td>
                <td align="right">
                    ${{ number_format($total, 2) }}
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>