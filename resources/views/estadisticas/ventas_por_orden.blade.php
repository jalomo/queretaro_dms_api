<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Ventas</title>
    <style>
        
    </style>
    <link rel="stylesheet" media="print" href="{{ public_path("css/print.css") }}">
</head>
<body>
    <htmlpageheader name="header">
        <img class="mylsa" src="{{ public_path("images_correo/ford.png") }}" />
        <h1 class="text-center text-header pt-2"> Ventas </h1>
    </htmlpageheader>

    <htmlpagefooter name="footer">
        <div class="text-right"><small>Página. {PAGENO} de {nbpg}<small></div>
    </htmlpagefooter>
        
    <div class="text-left">
        <div>
            <b>Fechas de Reporte:</b>
            <b>{{ $fecha_inicio }}</b> a 
            <b>{{ $fecha_termino }}</b>
        </div>
    </div>

    <h3 class="text-center uppercase">Refacciones</h3>

    @php $total = $total_orden = 0; $orden = ''; @endphp
    <table class="table table-striped">
        <tr class="text-left">
            <th align="left">Cliente</th>
            <th align="left">Cantidad</th>
            <th align="right">Valor Unitario</th>
            <th align="right">IVA</th>
            <th align="right">Importe Total</th>
        </tr>
        <tbody>
            @foreach ($ventas as $venta)
            @if($venta->venta->numero_orden == null && $venta->venta->folio->folio != $orden)
            <tr class="even">
                <td colspan="6">
                    <b>{{ $venta->venta->folio->folio }}</b> |
                    {{ $venta->venta->created_at->format('d \/ M \/ Y') }} 
                </td>
                @php $orden = $venta->venta->folio->folio; @endphp
            </tr>
            @endif
            @if($venta->venta->numero_orden != null && $venta->venta->numero_orden != $orden )
            <tr class="even">
                <td colspan="6">
                    <b>{{ $venta->venta->numero_orden }}</b> |
                    {{ $venta->venta->created_at->format('d \/ M \/ Y') }} 
                </td>
                @php $orden = $venta->venta->numero_orden; @endphp
            </tr>
            @endif
            <tr>
                <td>
                    {{ $venta->venta->cliente != null? "{$venta->venta->cliente->nombre} {$venta->venta->cliente->apellido_paterno} {$venta->venta->cliente->apellido_materno}" : 'Sin cliente' }}
                </td>
                <td>
                    {{ $venta->cantidad }}
                </td>
                <td align="right">
                    ${{ number_format($venta->total, 2) }}
                </td>
                <td align="right">
                    ${{ number_format($venta->total * 0.16, 2) }}
                </td>
                <td align="right">
                    ${{ number_format($venta->total * 1.16, 2) }}
                </td>
            </tr>
            @php $total += $venta->total * 1.16; @endphp
            @endforeach
            <tr>
                <td colspan="4">
                    <b>TOTAL</b>
                </td>
                <td align="right">
                    ${{ number_format($total, 2) }}
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>