<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Traspasos</title>
    <style>
        
    </style>
    <link rel="stylesheet" media="print" href="{{ public_path("css/print.css") }}">
</head>
<body>
    <htmlpageheader name="header">
        <img class="mylsa" src="{{ public_path("images_correo/ford.png") }}" />
        <h1 class="text-center text-header pt-2"> Traspasos </h1>
    </htmlpageheader>

    <htmlpagefooter name="footer">
        <div class="text-right"><small>Página. {PAGENO} de {nbpg}<small></div>
    </htmlpagefooter>
        
    <div class="text-left">
        <div>
            <b>Fechas de Reporte:</b>
            <b>{{ $fecha_inicio }}</b> a 
            <b>{{ $fecha_termino }}</b>
        </div>
    </div>

    <h3 class="text-center uppercase">Refacciones</h3>

    @php $total = 0; $id = null; @endphp
    <table class="table table-striped">
        <tr class="text-left">
            <th align="left">Cantidad Solicitada</th>
            <th align="left">Cantidad Entregada</th>
            <th align="left">Cantidad Devuelta</th>
            <th align="right">Total</th>
        </tr>
        <tbody>
            @foreach ($traspasos as $traspaso)
            @if($traspaso->traspaso->id != $id)
            <tr class="even">
                <td colspan="7">
                    <b>{{ $traspaso->traspaso->id }}</b> | <b>Origen:</b> {{ $traspaso->traspaso->almacen_origen->nombre }} | <b>Destino:</b> {{ $traspaso->traspaso->almacen_destino->nombre }} <br>
                    Fecha: {{ $traspaso->traspaso->created_at->format('d \/ M \/ Y') }}
                </td>
            </tr>
            @php $id = $traspaso->traspaso->id; @endphp
            @endif

            <tr>
                <td>
                    {{ $traspaso->cantidad }}
                </td>
                <td>
                    {{ $traspaso->cantidad_entregada }}
                </td>
                <td>
                    {{ $traspaso->cantidad_devuelta }}
                </td>
                <td align="right">
                    $ {{ number_format($traspaso->total_entregado - $traspaso->total_devuelto, 2) }}
                </td>
            </tr>
            @php $total += $traspaso->total_entregado - $traspaso->total_devuelto; @endphp
            
            @endforeach
            <tr>
                <td colspan="3">
                    <b>TOTAL</b>
                </td>
                <td align="right">
                    ${{ number_format($total, 2) }}
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>