<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Traspasos</title>
    <style>
        
    </style>
    <link rel="stylesheet" media="print" href="{{ public_path("css/print.css") }}">
</head>
<body>
    <htmlpageheader name="header">
        <img class="mylsa" src="{{ public_path("images_correo/ford.png") }}" />
        <h1 class="text-center text-header pt-2"> Traspasos </h1>
    </htmlpageheader>

    <htmlpagefooter name="footer">
        <div class="text-right"><small>Página. {PAGENO} de {nbpg}<small></div>
    </htmlpagefooter>
        
    <div class="text-left">
        <div>
            <b>Fechas de Reporte:</b>
            <b>{{ $fecha_inicio }}</b> a 
            <b>{{ $fecha_termino }}</b>
        </div>
    </div>

    <h3 class="text-center uppercase">Refacciones</h3>

    @php $total = $total_orden = 0; $id = null; @endphp
    <table class="table table-striped">
        <tr class="text-left">
            <th align="left">No. de parte</th>
            <th align="left">Descripción</th>
            <th align="left">Cantidad Solicitada</th>
            <th align="left">Cantidad Entregada</th>
            <th align="left">Cantidad Devuelta</th>
            <th align="right">Costo</th>
            <th align="right">Total</th>
        </tr>
        <tbody>
            @foreach ($traspasos as $traspaso)
            @if($traspaso->traspaso->id != $id)
            @if(! $loop->first)
                <tr>
                    <td colspan="5">
                        Costo Total
                    </td>
                    <td colspan="2" align="right">
                        ${{ number_format($total_orden, 2) }}
                    </td>
                </tr>
                @php $total_orden = 0; @endphp
            @endif
            <tr class="even">
                <td colspan="7">
                    <b>{{ $traspaso->traspaso->id }}</b> | <b>Origen:</b> {{ $traspaso->traspaso->almacen_origen->nombre }} | <b>Destino:</b> {{ $traspaso->traspaso->almacen_destino->nombre }} <br>
                    Fecha: {{ $traspaso->traspaso->created_at->format('d \/ M \/ Y') }}
                </td>
            </tr>
            @php $id = $traspaso->traspaso->id; @endphp
            @endif

            <tr>
                <td>
                    {{ $traspaso->no_identificacion }}
                </td>
                <td>
                    {{ $traspaso->producto->descripcion ?? '' }}
                </td>
                <td>
                    {{ $traspaso->cantidad }}
                </td>
                <td>
                    {{ $traspaso->cantidad_entregada }}
                </td>
                <td>
                    {{ $traspaso->cantidad_devuelta }}
                </td>
                <td align="right">
                    ${{number_format($traspaso->valor_unitario, 2) }}
                </td>
                <td align="right">
                    ${{ number_format($traspaso->valor_unitario * ($traspaso->cantidad_entregada - $traspaso->cantidad_devuelta), 2) }}
                </td>
            </tr>
            @php $total += $traspaso->valor_unitario * ($traspaso->cantidad_entregada - $traspaso->cantidad_devuelta); @endphp
            @php $total_orden += $traspaso->valor_unitario * ($traspaso->cantidad_entregada - $traspaso->cantidad_devuelta); @endphp

            @endforeach
            <tr>
                <td colspan="6">
                    <b>TOTAL</b>
                </td>
                <td align="right">
                    ${{ number_format($total, 2) }}
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>